package fl.controls
{
   import fl.core.*;
   import fl.events.*;
   import flash.display.DisplayObjectContainer;
   import flash.events.*;
   
   public class ScrollBar extends UIComponent
   {
      
      public static const WIDTH:Number = 15;
      
      private static var defaultStyles:Object = {
         "downArrowDisabledSkin":"ScrollArrowDown_disabledSkin",
         "downArrowDownSkin":"ScrollArrowDown_downSkin",
         "downArrowOverSkin":"ScrollArrowDown_overSkin",
         "downArrowUpSkin":"ScrollArrowDown_upSkin",
         "thumbDisabledSkin":"ScrollThumb_upSkin",
         "thumbDownSkin":"ScrollThumb_downSkin",
         "thumbOverSkin":"ScrollThumb_overSkin",
         "thumbUpSkin":"ScrollThumb_upSkin",
         "trackDisabledSkin":"ScrollTrack_skin",
         "trackDownSkin":"ScrollTrack_skin",
         "trackOverSkin":"ScrollTrack_skin",
         "trackUpSkin":"ScrollTrack_skin",
         "upArrowDisabledSkin":"ScrollArrowUp_disabledSkin",
         "upArrowDownSkin":"ScrollArrowUp_downSkin",
         "upArrowOverSkin":"ScrollArrowUp_overSkin",
         "upArrowUpSkin":"ScrollArrowUp_upSkin",
         "thumbIcon":"ScrollBar_thumbIcon",
         "repeatDelay":500,
         "repeatInterval":35
      };
      
      protected static const DOWN_ARROW_STYLES:Object = {
         "disabledSkin":"downArrowDisabledSkin",
         "downSkin":"downArrowDownSkin",
         "overSkin":"downArrowOverSkin",
         "upSkin":"downArrowUpSkin",
         "repeatDelay":"repeatDelay",
         "repeatInterval":"repeatInterval"
      };
      
      protected static const THUMB_STYLES:Object = {
         "disabledSkin":"thumbDisabledSkin",
         "downSkin":"thumbDownSkin",
         "overSkin":"thumbOverSkin",
         "upSkin":"thumbUpSkin",
         "icon":"thumbIcon",
         "textPadding":0
      };
      
      protected static const TRACK_STYLES:Object = {
         "disabledSkin":"trackDisabledSkin",
         "downSkin":"trackDownSkin",
         "overSkin":"trackOverSkin",
         "upSkin":"trackUpSkin",
         "repeatDelay":"repeatDelay",
         "repeatInterval":"repeatInterval"
      };
      
      protected static const UP_ARROW_STYLES:Object = {
         "disabledSkin":"upArrowDisabledSkin",
         "downSkin":"upArrowDownSkin",
         "overSkin":"upArrowOverSkin",
         "upSkin":"upArrowUpSkin",
         "repeatDelay":"repeatDelay",
         "repeatInterval":"repeatInterval"
      };
       
      
      private var _pageSize:Number = 10;
      
      private var _pageScrollSize:Number = 0;
      
      private var _lineScrollSize:Number = 1;
      
      private var _minScrollPosition:Number = 0;
      
      private var _maxScrollPosition:Number = 0;
      
      private var _scrollPosition:Number = 0;
      
      private var _direction:String = "vertical";
      
      private var thumbScrollOffset:Number;
      
      protected var inDrag:Boolean = false;
      
      protected var upArrow:BaseButton;
      
      protected var downArrow:BaseButton;
      
      protected var thumb:LabelButton;
      
      protected var track:BaseButton;
      
      public function ScrollBar()
      {
         super();
         this.setStyles();
         focusEnabled = false;
      }
      
      public static function getStyleDefinition() : Object
      {
         return defaultStyles;
      }
      
      override public function setSize(width:Number, height:Number) : void
      {
         if(this._direction == ScrollBarDirection.HORIZONTAL)
         {
            super.setSize(height,width);
         }
         else
         {
            super.setSize(width,height);
         }
      }
      
      override public function get width() : Number
      {
         return this._direction == ScrollBarDirection.HORIZONTAL ? super.height : super.width;
      }
      
      override public function get height() : Number
      {
         return this._direction == ScrollBarDirection.HORIZONTAL ? super.width : super.height;
      }
      
      override public function get enabled() : Boolean
      {
         return super.enabled;
      }
      
      override public function set enabled(value:Boolean) : void
      {
         super.enabled = value;
         this.downArrow.enabled = this.track.enabled = this.thumb.enabled = this.upArrow.enabled = this.enabled && this._maxScrollPosition > this._minScrollPosition;
         this.updateThumb();
      }
      
      public function setScrollProperties(pageSize:Number, minScrollPosition:Number, maxScrollPosition:Number, pageScrollSize:Number = 0) : void
      {
         this.pageSize = pageSize;
         this._minScrollPosition = minScrollPosition;
         this._maxScrollPosition = maxScrollPosition;
         if(pageScrollSize >= 0)
         {
            this._pageScrollSize = pageScrollSize;
         }
         this.enabled = this._maxScrollPosition > this._minScrollPosition;
         this.setScrollPosition(this._scrollPosition,false);
         this.updateThumb();
      }
      
      public function get scrollPosition() : Number
      {
         return this._scrollPosition;
      }
      
      public function set scrollPosition(newScrollPosition:Number) : void
      {
         this.setScrollPosition(newScrollPosition,true);
      }
      
      public function get minScrollPosition() : Number
      {
         return this._minScrollPosition;
      }
      
      public function set minScrollPosition(value:Number) : void
      {
         this.setScrollProperties(this._pageSize,value,this._maxScrollPosition);
      }
      
      public function get maxScrollPosition() : Number
      {
         return this._maxScrollPosition;
      }
      
      public function set maxScrollPosition(value:Number) : void
      {
         this.setScrollProperties(this._pageSize,this._minScrollPosition,value);
      }
      
      public function get pageSize() : Number
      {
         return this._pageSize;
      }
      
      public function set pageSize(value:Number) : void
      {
         if(value > 0)
         {
            this._pageSize = value;
         }
      }
      
      public function get pageScrollSize() : Number
      {
         return this._pageScrollSize == 0 ? Number(this._pageSize) : Number(this._pageScrollSize);
      }
      
      public function set pageScrollSize(value:Number) : void
      {
         if(value >= 0)
         {
            this._pageScrollSize = value;
         }
      }
      
      public function get lineScrollSize() : Number
      {
         return this._lineScrollSize;
      }
      
      public function set lineScrollSize(value:Number) : void
      {
         if(value > 0)
         {
            this._lineScrollSize = value;
         }
      }
      
      public function get direction() : String
      {
         return this._direction;
      }
      
      public function set direction(value:String) : void
      {
         if(this._direction == value)
         {
            return;
         }
         this._direction = value;
         if(isLivePreview)
         {
            return;
         }
         setScaleY(1);
         var horizontal:* = this._direction == ScrollBarDirection.HORIZONTAL;
         if(horizontal && componentInspectorSetting)
         {
            if(rotation == 90)
            {
               return;
            }
            setScaleX(-1);
            rotation = -90;
         }
         if(!componentInspectorSetting)
         {
            if(horizontal && rotation == 0)
            {
               rotation = -90;
               setScaleX(-1);
            }
            else if(!horizontal && rotation == -90)
            {
               rotation = 0;
               setScaleX(1);
            }
         }
         invalidate(InvalidationType.SIZE);
      }
      
      override protected function configUI() : void
      {
         super.configUI();
         this.track = new BaseButton();
         this.track.move(0,14);
         this.track.useHandCursor = false;
         this.track.autoRepeat = true;
         this.track.focusEnabled = false;
         addChild(this.track);
         this.thumb = new LabelButton();
         this.thumb.label = "";
         this.thumb.setSize(WIDTH,15);
         this.thumb.move(0,15);
         this.thumb.focusEnabled = false;
         addChild(this.thumb);
         this.downArrow = new BaseButton();
         this.downArrow.setSize(WIDTH,14);
         this.downArrow.autoRepeat = true;
         this.downArrow.focusEnabled = false;
         addChild(this.downArrow);
         this.upArrow = new BaseButton();
         this.upArrow.setSize(WIDTH,14);
         this.upArrow.move(0,0);
         this.upArrow.autoRepeat = true;
         this.upArrow.focusEnabled = false;
         addChild(this.upArrow);
         this.upArrow.addEventListener(ComponentEvent.BUTTON_DOWN,this.scrollPressHandler,false,0,true);
         this.downArrow.addEventListener(ComponentEvent.BUTTON_DOWN,this.scrollPressHandler,false,0,true);
         this.track.addEventListener(ComponentEvent.BUTTON_DOWN,this.scrollPressHandler,false,0,true);
         this.thumb.addEventListener(MouseEvent.MOUSE_DOWN,this.thumbPressHandler,false,0,true);
         this.enabled = false;
      }
      
      override protected function draw() : void
      {
         var h:Number = NaN;
         if(isInvalid(InvalidationType.SIZE))
         {
            h = super.height;
            this.downArrow.move(0,Math.max(this.upArrow.height,h - Number(this.downArrow.height)));
            this.track.setSize(WIDTH,Math.max(0,h - (this.downArrow.height + this.upArrow.height)));
            this.updateThumb();
         }
         if(isInvalid(InvalidationType.STYLES,InvalidationType.STATE))
         {
            this.setStyles();
         }
         this.downArrow.drawNow();
         this.upArrow.drawNow();
         this.track.drawNow();
         this.thumb.drawNow();
         validate();
      }
      
      protected function scrollPressHandler(event:ComponentEvent) : void
      {
         var mousePosition:Number = NaN;
         var pgScroll:Number = NaN;
         event.stopImmediatePropagation();
         if(event.currentTarget == this.upArrow)
         {
            this.setScrollPosition(Number(this._scrollPosition) - Number(this._lineScrollSize));
         }
         else if(event.currentTarget == this.downArrow)
         {
            this.setScrollPosition(this._scrollPosition + this._lineScrollSize);
         }
         else
         {
            mousePosition = Number(this.track.mouseY) / Number(this.track.height) * (Number(this._maxScrollPosition) - Number(this._minScrollPosition)) + this._minScrollPosition;
            pgScroll = this.pageScrollSize == 0 ? this.pageSize : this.pageScrollSize;
            if(this._scrollPosition < mousePosition)
            {
               this.setScrollPosition(Math.min(mousePosition,this._scrollPosition + pgScroll));
            }
            else if(this._scrollPosition > mousePosition)
            {
               this.setScrollPosition(Math.max(mousePosition,Number(this._scrollPosition) - pgScroll));
            }
         }
      }
      
      protected function thumbPressHandler(event:MouseEvent) : void
      {
         this.inDrag = true;
         this.thumbScrollOffset = mouseY - Number(this.thumb.y);
         this.thumb.mouseStateLocked = true;
         mouseChildren = false;
         var myForm:DisplayObjectContainer = focusManager.form;
         myForm.addEventListener(MouseEvent.MOUSE_MOVE,this.handleThumbDrag,false,0,true);
         myForm.addEventListener(MouseEvent.MOUSE_UP,this.thumbReleaseHandler,false,0,true);
      }
      
      protected function handleThumbDrag(event:MouseEvent) : void
      {
         var pos:Number = Math.max(0,Math.min(Number(this.track.height) - Number(this.thumb.height),mouseY - Number(this.track.y) - Number(this.thumbScrollOffset)));
         this.setScrollPosition(pos / (Number(this.track.height) - Number(this.thumb.height)) * (Number(this._maxScrollPosition) - Number(this._minScrollPosition)) + this._minScrollPosition);
      }
      
      protected function thumbReleaseHandler(event:MouseEvent) : void
      {
         this.inDrag = false;
         mouseChildren = true;
         this.thumb.mouseStateLocked = false;
         var myForm:DisplayObjectContainer = focusManager.form;
         myForm.removeEventListener(MouseEvent.MOUSE_MOVE,this.handleThumbDrag);
         myForm.removeEventListener(MouseEvent.MOUSE_UP,this.thumbReleaseHandler);
      }
      
      public function setScrollPosition(newScrollPosition:Number, fireEvent:Boolean = true) : void
      {
         var oldPosition:Number = this.scrollPosition;
         this._scrollPosition = Math.max(this._minScrollPosition,Math.min(this._maxScrollPosition,newScrollPosition));
         if(oldPosition == this._scrollPosition)
         {
            return;
         }
         if(fireEvent)
         {
            dispatchEvent(new ScrollEvent(this._direction,this.scrollPosition - oldPosition,this.scrollPosition));
         }
         this.updateThumb();
      }
      
      protected function setStyles() : void
      {
         copyStylesToChild(this.downArrow,DOWN_ARROW_STYLES);
         copyStylesToChild(this.thumb,THUMB_STYLES);
         copyStylesToChild(this.track,TRACK_STYLES);
         copyStylesToChild(this.upArrow,UP_ARROW_STYLES);
      }
      
      protected function updateThumb() : void
      {
         var per:Number = Number(this._maxScrollPosition) - Number(this._minScrollPosition) + this._pageSize;
         if(this.track.height <= 12 || this._maxScrollPosition <= this._minScrollPosition || (per == 0 || Boolean(isNaN(per))))
         {
            this.thumb.height = 12;
            this.thumb.visible = false;
         }
         else
         {
            this.thumb.height = Math.max(13,Number(this._pageSize) / per * Number(this.track.height));
            this.thumb.y = this.track.y + (Number(this.track.height) - Number(this.thumb.height)) * ((Number(this._scrollPosition) - Number(this._minScrollPosition)) / (Number(this._maxScrollPosition) - Number(this._minScrollPosition)));
            this.thumb.visible = this.enabled;
         }
      }
   }
}
