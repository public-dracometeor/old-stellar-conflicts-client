package fl.controls
{
   import fl.core.*;
   import fl.events.*;
   import fl.managers.*;
   import flash.display.DisplayObject;
   import flash.events.*;
   import flash.text.*;
   import flash.ui.*;
   
   public class TextInput extends UIComponent implements IFocusManagerComponent
   {
      
      private static var defaultStyles:Object = {
         "upSkin":"TextInput_upSkin",
         "disabledSkin":"TextInput_disabledSkin",
         "focusRectSkin":null,
         "focusRectPadding":null,
         "textFormat":null,
         "disabledTextFormat":null,
         "textPadding":0,
         "embedFonts":false
      };
      
      public static var createAccessibilityImplementation:Function;
       
      
      public var textField:TextField;
      
      protected var _editable:Boolean = true;
      
      protected var background:DisplayObject;
      
      protected var _html:Boolean = false;
      
      protected var _savedHTML:String;
      
      public function TextInput()
      {
         super();
      }
      
      public static function getStyleDefinition() : Object
      {
         return defaultStyles;
      }
      
      public function get text() : String
      {
         return this.textField.text;
      }
      
      public function set text(value:String) : void
      {
         this.textField.text = value;
         this._html = false;
         invalidate(InvalidationType.DATA);
         invalidate(InvalidationType.STYLES);
      }
      
      override public function get enabled() : Boolean
      {
         return super.enabled;
      }
      
      override public function set enabled(value:Boolean) : void
      {
         super.enabled = value;
         this.updateTextFieldType();
      }
      
      public function get imeMode() : String
      {
         return _imeMode;
      }
      
      public function set imeMode(value:String) : void
      {
         _imeMode = value;
      }
      
      public function get alwaysShowSelection() : Boolean
      {
         return this.textField.alwaysShowSelection;
      }
      
      public function set alwaysShowSelection(value:Boolean) : void
      {
         this.textField.alwaysShowSelection = value;
      }
      
      override public function drawFocus(draw:Boolean) : void
      {
         if(focusTarget != null)
         {
            focusTarget.drawFocus(draw);
            return;
         }
         super.drawFocus(draw);
      }
      
      public function get editable() : Boolean
      {
         return this._editable;
      }
      
      public function set editable(value:Boolean) : void
      {
         this._editable = value;
         this.updateTextFieldType();
      }
      
      public function get horizontalScrollPosition() : int
      {
         return this.textField.scrollH;
      }
      
      public function set horizontalScrollPosition(value:int) : void
      {
         this.textField.scrollH = value;
      }
      
      public function get maxHorizontalScrollPosition() : int
      {
         return this.textField.maxScrollH;
      }
      
      public function get length() : int
      {
         return this.textField.length;
      }
      
      public function get maxChars() : int
      {
         return this.textField.maxChars;
      }
      
      public function set maxChars(value:int) : void
      {
         this.textField.maxChars = value;
      }
      
      public function get displayAsPassword() : Boolean
      {
         return this.textField.displayAsPassword;
      }
      
      public function set displayAsPassword(value:Boolean) : void
      {
         this.textField.displayAsPassword = value;
      }
      
      public function get restrict() : String
      {
         return this.textField.restrict;
      }
      
      public function set restrict(value:String) : void
      {
         if(componentInspectorSetting && value == "")
         {
            value = null;
         }
         this.textField.restrict = value;
      }
      
      public function get selectionBeginIndex() : int
      {
         return this.textField.selectionBeginIndex;
      }
      
      public function get selectionEndIndex() : int
      {
         return this.textField.selectionEndIndex;
      }
      
      public function get condenseWhite() : Boolean
      {
         return this.textField.condenseWhite;
      }
      
      public function set condenseWhite(value:Boolean) : void
      {
         this.textField.condenseWhite = value;
      }
      
      public function get htmlText() : String
      {
         return this.textField.htmlText;
      }
      
      public function set htmlText(value:String) : void
      {
         if(value == "")
         {
            this.text = "";
            return;
         }
         this._html = true;
         this._savedHTML = value;
         this.textField.htmlText = value;
         invalidate(InvalidationType.DATA);
         invalidate(InvalidationType.STYLES);
      }
      
      public function get textHeight() : Number
      {
         return this.textField.textHeight;
      }
      
      public function get textWidth() : Number
      {
         return this.textField.textWidth;
      }
      
      public function setSelection(beginIndex:int, endIndex:int) : void
      {
         this.textField.setSelection(beginIndex,endIndex);
      }
      
      public function getLineMetrics(index:int) : TextLineMetrics
      {
         return this.textField.getLineMetrics(index);
      }
      
      public function appendText(text:String) : void
      {
         this.textField.appendText(text);
      }
      
      protected function updateTextFieldType() : void
      {
         this.textField.type = this.enabled && this.editable ? TextFieldType.INPUT : TextFieldType.DYNAMIC;
         this.textField.selectable = this.enabled;
      }
      
      protected function handleKeyDown(event:KeyboardEvent) : void
      {
         if(event.keyCode == Keyboard.ENTER)
         {
            dispatchEvent(new ComponentEvent(ComponentEvent.ENTER,true));
         }
      }
      
      protected function handleChange(event:Event) : void
      {
         event.stopPropagation();
         dispatchEvent(new Event(Event.CHANGE,true));
      }
      
      protected function handleTextInput(event:TextEvent) : void
      {
         event.stopPropagation();
         dispatchEvent(new TextEvent(TextEvent.TEXT_INPUT,true,false,event.text));
      }
      
      protected function setEmbedFont() : *
      {
         var embed:Object = getStyleValue("embedFonts");
         if(embed != null)
         {
            this.textField.embedFonts = embed;
         }
      }
      
      override protected function draw() : void
      {
         var embed:Object = null;
         if(isInvalid(InvalidationType.STYLES,InvalidationType.STATE))
         {
            this.drawTextFormat();
            this.drawBackground();
            embed = getStyleValue("embedFonts");
            if(embed != null)
            {
               this.textField.embedFonts = embed;
            }
            invalidate(InvalidationType.SIZE,false);
         }
         if(isInvalid(InvalidationType.SIZE))
         {
            this.drawLayout();
         }
         super.draw();
      }
      
      protected function drawBackground() : void
      {
         var bg:DisplayObject = this.background;
         var styleName:String = this.enabled ? "upSkin" : "disabledSkin";
         this.background = getDisplayObjectInstance(getStyleValue(styleName));
         if(this.background == null)
         {
            return;
         }
         addChildAt(this.background,0);
         if(bg != null && bg != this.background && contains(bg))
         {
            removeChild(bg);
         }
      }
      
      protected function drawTextFormat() : void
      {
         var uiStyles:Object = UIComponent.getStyleDefinition();
         var defaultTF:TextFormat = this.enabled ? uiStyles.defaultTextFormat as TextFormat : uiStyles.defaultDisabledTextFormat as TextFormat;
         this.textField.setTextFormat(defaultTF);
         var tf:TextFormat = getStyleValue(this.enabled ? "textFormat" : "disabledTextFormat") as TextFormat;
         if(tf != null)
         {
            this.textField.setTextFormat(tf);
         }
         else
         {
            tf = defaultTF;
         }
         this.textField.defaultTextFormat = tf;
         this.setEmbedFont();
         if(this._html)
         {
            this.textField.htmlText = this._savedHTML;
         }
      }
      
      protected function drawLayout() : void
      {
         var txtPad:Number = Number(getStyleValue("textPadding"));
         if(this.background != null)
         {
            this.background.width = width;
            this.background.height = height;
         }
         this.textField.width = width - 2 * txtPad;
         this.textField.height = height - 2 * txtPad;
         this.textField.x = this.textField.y = txtPad;
      }
      
      override protected function configUI() : void
      {
         super.configUI();
         tabChildren = true;
         this.textField = new TextField();
         addChild(this.textField);
         this.updateTextFieldType();
         this.textField.addEventListener(TextEvent.TEXT_INPUT,this.handleTextInput,false,0,true);
         this.textField.addEventListener(Event.CHANGE,this.handleChange,false,0,true);
         this.textField.addEventListener(KeyboardEvent.KEY_DOWN,this.handleKeyDown,false,0,true);
      }
      
      override public function setFocus() : void
      {
         stage.focus = this.textField;
      }
      
      override protected function isOurFocus(target:DisplayObject) : Boolean
      {
         return target == this.textField || super.isOurFocus(target);
      }
      
      override protected function focusInHandler(event:FocusEvent) : void
      {
         if(event.target == this)
         {
            stage.focus = this.textField;
         }
         var fm:IFocusManager = focusManager;
         if(this.editable && Boolean(fm))
         {
            fm.showFocusIndicator = true;
            if(this.textField.selectable && this.textField.selectionBeginIndex == this.textField.selectionBeginIndex)
            {
               this.setSelection(0,this.textField.length);
            }
         }
         super.focusInHandler(event);
         if(this.editable)
         {
            setIMEMode(true);
         }
      }
      
      override protected function focusOutHandler(event:FocusEvent) : void
      {
         super.focusOutHandler(event);
         if(this.editable)
         {
            setIMEMode(false);
         }
      }
   }
}
