package fl.controls
{
   import fl.core.*;
   import fl.events.ScrollEvent;
   import flash.display.DisplayObject;
   import flash.events.*;
   
   public class UIScrollBar extends ScrollBar
   {
      
      private static var defaultStyles:Object = {};
       
      
      protected var _scrollTarget:DisplayObject;
      
      protected var inEdit:Boolean = false;
      
      protected var inScroll:Boolean = false;
      
      protected var _targetScrollProperty:String;
      
      protected var _targetMaxScrollProperty:String;
      
      public function UIScrollBar()
      {
         super();
      }
      
      public static function getStyleDefinition() : Object
      {
         return UIComponent.mergeStyles(defaultStyles,ScrollBar.getStyleDefinition());
      }
      
      override public function set minScrollPosition(minScrollPosition:Number) : void
      {
         super.minScrollPosition = minScrollPosition < 0 ? 0 : minScrollPosition;
      }
      
      override public function set maxScrollPosition(maxScrollPosition:Number) : void
      {
         var maxScrollPos:Number = maxScrollPosition;
         if(this._scrollTarget != null)
         {
            maxScrollPos = Math.min(maxScrollPos,this._scrollTarget[this._targetMaxScrollProperty]);
         }
         super.maxScrollPosition = maxScrollPos;
      }
      
      public function get scrollTarget() : DisplayObject
      {
         return this._scrollTarget;
      }
      
      public function set scrollTarget(target:DisplayObject) : void
      {
         var blockProg:String;
         var textDir:String;
         var hasPixelVS:Boolean;
         var scrollHoriz:Boolean;
         var rot:Number;
         if(this._scrollTarget != null)
         {
            this._scrollTarget.removeEventListener(Event.CHANGE,this.handleTargetChange,false);
            this._scrollTarget.removeEventListener(TextEvent.TEXT_INPUT,this.handleTargetChange,false);
            this._scrollTarget.removeEventListener(Event.SCROLL,this.handleTargetScroll,false);
         }
         this._scrollTarget = target;
         blockProg = null;
         textDir = null;
         hasPixelVS = false;
         if(this._scrollTarget != null)
         {
            try
            {
               if(this._scrollTarget.hasOwnProperty("blockProgression"))
               {
                  blockProg = this._scrollTarget["blockProgression"];
               }
               if(this._scrollTarget.hasOwnProperty("direction"))
               {
                  textDir = this._scrollTarget["direction"];
               }
               if(this._scrollTarget.hasOwnProperty("pixelScrollV"))
               {
                  hasPixelVS = true;
               }
            }
            catch(e:Error)
            {
               blockProg = null;
               textDir = null;
            }
         }
         scrollHoriz = this.direction == ScrollBarDirection.HORIZONTAL;
         rot = Math.abs(this.rotation);
         if(scrollHoriz && (blockProg == "rl" || textDir == "rtl"))
         {
            if(getScaleY() > 0 && rotation == 90)
            {
               x += width;
            }
            setScaleY(-1);
         }
         else if(!scrollHoriz && blockProg == "rl" && textDir == "rtl")
         {
            if(getScaleY() > 0 && rotation != 90)
            {
               y += height;
            }
            setScaleY(-1);
         }
         else
         {
            if(getScaleY() < 0)
            {
               if(scrollHoriz)
               {
                  if(rotation == 90)
                  {
                     x -= width;
                  }
               }
               else if(rotation != 90)
               {
                  y -= height;
               }
            }
            setScaleY(1);
         }
         this.setTargetScrollProperties(scrollHoriz,blockProg,hasPixelVS);
         if(this._scrollTarget != null)
         {
            this._scrollTarget.addEventListener(Event.CHANGE,this.handleTargetChange,false,0,true);
            this._scrollTarget.addEventListener(TextEvent.TEXT_INPUT,this.handleTargetChange,false,0,true);
            this._scrollTarget.addEventListener(Event.SCROLL,this.handleTargetScroll,false,0,true);
         }
         invalidate(InvalidationType.DATA);
      }
      
      public function get scrollTargetName() : String
      {
         return this._scrollTarget.name;
      }
      
      public function set scrollTargetName(target:String) : void
      {
         try
         {
            this.scrollTarget = parent.getChildByName(target);
         }
         catch(error:Error)
         {
            throw new Error("ScrollTarget not found, or is not a valid target");
         }
      }
      
      override public function get direction() : String
      {
         return super.direction;
      }
      
      override public function set direction(dir:String) : void
      {
         var cacheScrollTarget:DisplayObject = null;
         if(isLivePreview)
         {
            return;
         }
         if(!componentInspectorSetting && this._scrollTarget != null)
         {
            cacheScrollTarget = this._scrollTarget;
            this.scrollTarget = null;
         }
         super.direction = dir;
         if(cacheScrollTarget != null)
         {
            this.scrollTarget = cacheScrollTarget;
         }
         else
         {
            this.updateScrollTargetProperties();
         }
      }
      
      public function update() : void
      {
         this.inEdit = true;
         this.updateScrollTargetProperties();
         this.inEdit = false;
      }
      
      override protected function draw() : void
      {
         if(isInvalid(InvalidationType.DATA))
         {
            this.updateScrollTargetProperties();
         }
         super.draw();
      }
      
      protected function updateScrollTargetProperties() : void
      {
         var local_tlf_internal:*;
         var blockProg:String = null;
         var hasPixelVS:Boolean = false;
         var pageSize:Number = NaN;
         var minScroll:Number = NaN;
         var minScrollVValue:* = undefined;
         if(this._scrollTarget == null)
         {
            this.setScrollProperties(pageSize,minScrollPosition,maxScrollPosition);
            scrollPosition = 0;
         }
         else
         {
            blockProg = null;
            hasPixelVS = false;
            try
            {
               if(this._scrollTarget.hasOwnProperty("blockProgression"))
               {
                  blockProg = this._scrollTarget["blockProgression"];
               }
               if(this._scrollTarget.hasOwnProperty("pixelScrollV"))
               {
                  hasPixelVS = true;
               }
            }
            catch(e1:Error)
            {
            }
            this.setTargetScrollProperties(this.direction == ScrollBarDirection.HORIZONTAL,blockProg,hasPixelVS);
            if(this._targetScrollProperty == "scrollH")
            {
               minScroll = 0;
               try
               {
                  if(Boolean(this._scrollTarget.hasOwnProperty("controller")) && Boolean(this._scrollTarget["controller"].hasOwnProperty("compositionWidth")))
                  {
                     pageSize = Number(this._scrollTarget["controller"]["compositionWidth"]);
                  }
                  else
                  {
                     pageSize = Number(this._scrollTarget.width);
                  }
               }
               catch(e2:Error)
               {
                  pageSize = Number(_scrollTarget.width);
               }
            }
            else
            {
               try
               {
                  if(blockProg != null)
                  {
                     minScrollVValue = this._scrollTarget["pixelMinScrollV"];
                     if(minScrollVValue is int)
                     {
                        minScroll = minScrollVValue;
                     }
                     else
                     {
                        minScroll = 1;
                     }
                  }
                  else
                  {
                     minScroll = 1;
                  }
               }
               catch(e3:Error)
               {
                  minScroll = 1;
               }
               pageSize = 10;
            }
            this.setScrollProperties(pageSize,minScroll,this.scrollTarget[this._targetMaxScrollProperty]);
            scrollPosition = this._scrollTarget[this._targetScrollProperty];
         }
      }
      
      override public function setScrollProperties(pageSize:Number, minScrollPosition:Number, maxScrollPosition:Number, pageScrollSize:Number = 0) : void
      {
         var maxScrollPos:Number = maxScrollPosition;
         var minScrollPos:Number = minScrollPosition < 0 ? 0 : minScrollPosition;
         if(this._scrollTarget != null)
         {
            maxScrollPos = Math.min(maxScrollPosition,this._scrollTarget[this._targetMaxScrollProperty]);
         }
         super.setScrollProperties(pageSize,minScrollPos,maxScrollPos,pageScrollSize);
      }
      
      override public function setScrollPosition(scrollPosition:Number, fireEvent:Boolean = true) : void
      {
         super.setScrollPosition(scrollPosition,fireEvent);
         if(!this._scrollTarget)
         {
            this.inScroll = false;
            return;
         }
         this.updateTargetScroll();
      }
      
      protected function updateTargetScroll(event:ScrollEvent = null) : void
      {
         if(this.inEdit)
         {
            return;
         }
         this._scrollTarget[this._targetScrollProperty] = scrollPosition;
      }
      
      protected function handleTargetChange(event:Event) : void
      {
         this.inEdit = true;
         this.setScrollPosition(this._scrollTarget[this._targetScrollProperty],true);
         this.updateScrollTargetProperties();
         this.inEdit = false;
      }
      
      protected function handleTargetScroll(event:Event) : void
      {
         if(inDrag)
         {
            return;
         }
         if(!enabled)
         {
            return;
         }
         this.inEdit = true;
         this.updateScrollTargetProperties();
         scrollPosition = this._scrollTarget[this._targetScrollProperty];
         this.inEdit = false;
      }
      
      private function setTargetScrollProperties(scrollHoriz:Boolean, blockProg:String, hasPixelVS:Boolean = false) : void
      {
         if(blockProg == "rl")
         {
            if(scrollHoriz)
            {
               this._targetScrollProperty = hasPixelVS ? "pixelScrollV" : "scrollV";
               this._targetMaxScrollProperty = hasPixelVS ? "pixelMaxScrollV" : "maxScrollV";
            }
            else
            {
               this._targetScrollProperty = "scrollH";
               this._targetMaxScrollProperty = "maxScrollH";
            }
         }
         else if(scrollHoriz)
         {
            this._targetScrollProperty = "scrollH";
            this._targetMaxScrollProperty = "maxScrollH";
         }
         else
         {
            this._targetScrollProperty = hasPixelVS ? "pixelScrollV" : "scrollV";
            this._targetMaxScrollProperty = hasPixelVS ? "pixelMaxScrollV" : "maxScrollV";
         }
      }
   }
}
