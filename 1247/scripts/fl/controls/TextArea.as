package fl.controls
{
   import fl.core.*;
   import fl.events.*;
   import fl.managers.*;
   import flash.display.DisplayObject;
   import flash.events.*;
   import flash.system.*;
   import flash.text.*;
   import flash.ui.*;
   
   public class TextArea extends UIComponent implements IFocusManagerComponent
   {
      
      private static var defaultStyles:Object = {
         "upSkin":"TextArea_upSkin",
         "disabledSkin":"TextArea_disabledSkin",
         "focusRectSkin":null,
         "focusRectPadding":null,
         "textFormat":null,
         "disabledTextFormat":null,
         "textPadding":3,
         "embedFonts":false
      };
      
      protected static const SCROLL_BAR_STYLES:Object = {
         "downArrowDisabledSkin":"downArrowDisabledSkin",
         "downArrowDownSkin":"downArrowDownSkin",
         "downArrowOverSkin":"downArrowOverSkin",
         "downArrowUpSkin":"downArrowUpSkin",
         "upArrowDisabledSkin":"upArrowDisabledSkin",
         "upArrowDownSkin":"upArrowDownSkin",
         "upArrowOverSkin":"upArrowOverSkin",
         "upArrowUpSkin":"upArrowUpSkin",
         "thumbDisabledSkin":"thumbDisabledSkin",
         "thumbDownSkin":"thumbDownSkin",
         "thumbOverSkin":"thumbOverSkin",
         "thumbUpSkin":"thumbUpSkin",
         "thumbIcon":"thumbIcon",
         "trackDisabledSkin":"trackDisabledSkin",
         "trackDownSkin":"trackDownSkin",
         "trackOverSkin":"trackOverSkin",
         "trackUpSkin":"trackUpSkin",
         "repeatDelay":"repeatDelay",
         "repeatInterval":"repeatInterval"
      };
      
      public static var createAccessibilityImplementation:Function;
       
      
      public var textField:TextField;
      
      protected var _editable:Boolean = true;
      
      protected var _wordWrap:Boolean = true;
      
      protected var _horizontalScrollPolicy:String = "auto";
      
      protected var _verticalScrollPolicy:String = "auto";
      
      protected var _horizontalScrollBar:UIScrollBar;
      
      protected var _verticalScrollBar:UIScrollBar;
      
      protected var background:DisplayObject;
      
      protected var _html:Boolean = false;
      
      protected var _savedHTML:String;
      
      protected var textHasChanged:Boolean = false;
      
      public function TextArea()
      {
         super();
      }
      
      public static function getStyleDefinition() : Object
      {
         return UIComponent.mergeStyles(defaultStyles,ScrollBar.getStyleDefinition());
      }
      
      public function get horizontalScrollBar() : UIScrollBar
      {
         return this._horizontalScrollBar;
      }
      
      public function get verticalScrollBar() : UIScrollBar
      {
         return this._verticalScrollBar;
      }
      
      override public function get enabled() : Boolean
      {
         return super.enabled;
      }
      
      override public function set enabled(value:Boolean) : void
      {
         super.enabled = value;
         mouseChildren = this.enabled;
         invalidate(InvalidationType.STATE);
      }
      
      public function get text() : String
      {
         return this.textField.text;
      }
      
      public function set text(value:String) : void
      {
         if(componentInspectorSetting && value == "")
         {
            return;
         }
         this.textField.text = value;
         this._html = false;
         invalidate(InvalidationType.DATA);
         invalidate(InvalidationType.STYLES);
         this.textHasChanged = true;
      }
      
      public function get htmlText() : String
      {
         return this.textField.htmlText;
      }
      
      public function set htmlText(value:String) : void
      {
         if(componentInspectorSetting && value == "")
         {
            return;
         }
         if(value == "")
         {
            this.text = "";
            return;
         }
         this._html = true;
         this._savedHTML = value;
         this.textField.htmlText = value;
         invalidate(InvalidationType.DATA);
         invalidate(InvalidationType.STYLES);
         this.textHasChanged = true;
      }
      
      public function get condenseWhite() : Boolean
      {
         return this.textField.condenseWhite;
      }
      
      public function set condenseWhite(value:Boolean) : void
      {
         this.textField.condenseWhite = value;
         invalidate(InvalidationType.DATA);
      }
      
      public function get horizontalScrollPolicy() : String
      {
         return this._horizontalScrollPolicy;
      }
      
      public function set horizontalScrollPolicy(value:String) : void
      {
         this._horizontalScrollPolicy = value;
         invalidate(InvalidationType.SIZE);
      }
      
      public function get verticalScrollPolicy() : String
      {
         return this._verticalScrollPolicy;
      }
      
      public function set verticalScrollPolicy(value:String) : void
      {
         this._verticalScrollPolicy = value;
         invalidate(InvalidationType.SIZE);
      }
      
      public function get horizontalScrollPosition() : Number
      {
         return this.textField.scrollH;
      }
      
      public function set horizontalScrollPosition(value:Number) : void
      {
         drawNow();
         this.textField.scrollH = value;
      }
      
      public function get verticalScrollPosition() : Number
      {
         return this.textField.scrollV;
      }
      
      public function set verticalScrollPosition(value:Number) : void
      {
         drawNow();
         this.textField.scrollV = value;
      }
      
      public function get textWidth() : Number
      {
         drawNow();
         return this.textField.textWidth;
      }
      
      public function get textHeight() : Number
      {
         drawNow();
         return this.textField.textHeight;
      }
      
      public function get length() : Number
      {
         return this.textField.text.length;
      }
      
      public function get restrict() : String
      {
         return this.textField.restrict;
      }
      
      public function set restrict(value:String) : void
      {
         if(componentInspectorSetting && value == "")
         {
            value = null;
         }
         this.textField.restrict = value;
      }
      
      public function get maxChars() : int
      {
         return this.textField.maxChars;
      }
      
      public function set maxChars(value:int) : void
      {
         this.textField.maxChars = value;
      }
      
      public function get maxHorizontalScrollPosition() : int
      {
         return this.textField.maxScrollH;
      }
      
      public function get maxVerticalScrollPosition() : int
      {
         return this.textField.maxScrollV;
      }
      
      public function get wordWrap() : Boolean
      {
         return this._wordWrap;
      }
      
      public function set wordWrap(value:Boolean) : void
      {
         this._wordWrap = value;
         invalidate(InvalidationType.STATE);
      }
      
      public function get selectionBeginIndex() : int
      {
         return this.textField.selectionBeginIndex;
      }
      
      public function get selectionEndIndex() : int
      {
         return this.textField.selectionEndIndex;
      }
      
      public function get displayAsPassword() : Boolean
      {
         return this.textField.displayAsPassword;
      }
      
      public function set displayAsPassword(value:Boolean) : void
      {
         this.textField.displayAsPassword = value;
      }
      
      public function get editable() : Boolean
      {
         return this._editable;
      }
      
      public function set editable(value:Boolean) : void
      {
         this._editable = value;
         invalidate(InvalidationType.STATE);
      }
      
      public function get imeMode() : String
      {
         return IME.conversionMode;
      }
      
      public function set imeMode(value:String) : void
      {
         _imeMode = value;
      }
      
      public function get alwaysShowSelection() : Boolean
      {
         return this.textField.alwaysShowSelection;
      }
      
      public function set alwaysShowSelection(value:Boolean) : void
      {
         this.textField.alwaysShowSelection = value;
      }
      
      override public function drawFocus(draw:Boolean) : void
      {
         if(focusTarget != null)
         {
            focusTarget.drawFocus(draw);
            return;
         }
         super.drawFocus(draw);
      }
      
      public function getLineMetrics(lineIndex:int) : TextLineMetrics
      {
         return this.textField.getLineMetrics(lineIndex);
      }
      
      public function setSelection(setSelection:int, endIndex:int) : void
      {
         this.textField.setSelection(setSelection,endIndex);
      }
      
      public function appendText(text:String) : void
      {
         this.textField.appendText(text);
         invalidate(InvalidationType.DATA);
      }
      
      override protected function configUI() : void
      {
         super.configUI();
         tabChildren = true;
         this.textField = new TextField();
         addChild(this.textField);
         this.updateTextFieldType();
         this._verticalScrollBar = new UIScrollBar();
         this._verticalScrollBar.name = "V";
         this._verticalScrollBar.visible = false;
         this._verticalScrollBar.focusEnabled = false;
         copyStylesToChild(this._verticalScrollBar,SCROLL_BAR_STYLES);
         this._verticalScrollBar.addEventListener(ScrollEvent.SCROLL,this.handleScroll,false,0,true);
         addChild(this._verticalScrollBar);
         this._horizontalScrollBar = new UIScrollBar();
         this._horizontalScrollBar.name = "H";
         this._horizontalScrollBar.visible = false;
         this._horizontalScrollBar.focusEnabled = false;
         this._horizontalScrollBar.direction = ScrollBarDirection.HORIZONTAL;
         copyStylesToChild(this._horizontalScrollBar,SCROLL_BAR_STYLES);
         this._horizontalScrollBar.addEventListener(ScrollEvent.SCROLL,this.handleScroll,false,0,true);
         addChild(this._horizontalScrollBar);
         this.textField.addEventListener(TextEvent.TEXT_INPUT,this.handleTextInput,false,0,true);
         this.textField.addEventListener(Event.CHANGE,this.handleChange,false,0,true);
         this.textField.addEventListener(KeyboardEvent.KEY_DOWN,this.handleKeyDown,false,0,true);
         this._horizontalScrollBar.scrollTarget = this.textField;
         this._verticalScrollBar.scrollTarget = this.textField;
         addEventListener(MouseEvent.MOUSE_WHEEL,this.handleWheel,false,0,true);
      }
      
      protected function updateTextFieldType() : void
      {
         this.textField.type = this.enabled && Boolean(this._editable) ? TextFieldType.INPUT : TextFieldType.DYNAMIC;
         this.textField.selectable = this.enabled;
         this.textField.wordWrap = this._wordWrap;
         this.textField.multiline = true;
      }
      
      protected function handleKeyDown(event:KeyboardEvent) : void
      {
         if(event.keyCode == Keyboard.ENTER)
         {
            dispatchEvent(new ComponentEvent(ComponentEvent.ENTER,true));
         }
      }
      
      protected function handleChange(event:Event) : void
      {
         event.stopPropagation();
         dispatchEvent(new Event(Event.CHANGE,true));
         invalidate(InvalidationType.DATA);
      }
      
      protected function handleTextInput(event:TextEvent) : void
      {
         event.stopPropagation();
         dispatchEvent(new TextEvent(TextEvent.TEXT_INPUT,true,false,event.text));
      }
      
      protected function handleScroll(event:ScrollEvent) : void
      {
         dispatchEvent(event);
      }
      
      protected function handleWheel(event:MouseEvent) : void
      {
         if(!this.enabled || !this._verticalScrollBar.visible)
         {
            return;
         }
         this._verticalScrollBar.scrollPosition -= event.delta * Number(this._verticalScrollBar.lineScrollSize);
         dispatchEvent(new ScrollEvent(ScrollBarDirection.VERTICAL,event.delta * Number(this._verticalScrollBar.lineScrollSize),this._verticalScrollBar.scrollPosition));
      }
      
      protected function setEmbedFont() : *
      {
         var embed:Object = getStyleValue("embedFonts");
         if(embed != null)
         {
            this.textField.embedFonts = embed;
         }
      }
      
      override protected function draw() : void
      {
         if(isInvalid(InvalidationType.STATE))
         {
            this.updateTextFieldType();
         }
         if(isInvalid(InvalidationType.STYLES))
         {
            this.setStyles();
            this.setEmbedFont();
         }
         if(isInvalid(InvalidationType.STYLES,InvalidationType.STATE))
         {
            this.drawTextFormat();
            this.drawBackground();
            invalidate(InvalidationType.SIZE,false);
         }
         if(isInvalid(InvalidationType.SIZE,InvalidationType.DATA))
         {
            this.drawLayout();
         }
         super.draw();
      }
      
      protected function setStyles() : void
      {
         copyStylesToChild(this._verticalScrollBar,SCROLL_BAR_STYLES);
         copyStylesToChild(this._horizontalScrollBar,SCROLL_BAR_STYLES);
      }
      
      protected function drawTextFormat() : void
      {
         var uiStyles:Object = UIComponent.getStyleDefinition();
         var defaultTF:TextFormat = this.enabled ? uiStyles.defaultTextFormat as TextFormat : uiStyles.defaultDisabledTextFormat as TextFormat;
         this.textField.setTextFormat(defaultTF);
         var tf:TextFormat = getStyleValue(this.enabled ? "textFormat" : "disabledTextFormat") as TextFormat;
         if(tf != null)
         {
            this.textField.setTextFormat(tf);
         }
         else
         {
            tf = defaultTF;
         }
         this.textField.defaultTextFormat = tf;
         this.setEmbedFont();
         if(this._html)
         {
            this.textField.htmlText = this._savedHTML;
         }
      }
      
      protected function drawBackground() : void
      {
         var bg:DisplayObject = this.background;
         var styleName:String = this.enabled ? "upSkin" : "disabledSkin";
         this.background = getDisplayObjectInstance(getStyleValue(styleName));
         if(this.background != null)
         {
            addChildAt(this.background,0);
         }
         if(bg != null && bg != this.background && contains(bg))
         {
            removeChild(bg);
         }
      }
      
      protected function drawLayout() : void
      {
         var txtPad:Number = Number(getStyleValue("textPadding"));
         this.textField.x = this.textField.y = txtPad;
         this.background.width = width;
         this.background.height = height;
         var availHeight:Number = height;
         var vScrollBar:Boolean = Boolean(this.needVScroll());
         var availWidth:Number = width - (vScrollBar ? this._verticalScrollBar.width : 0);
         var hScrollBar:Boolean = Boolean(this.needHScroll());
         if(hScrollBar)
         {
            availHeight -= this._horizontalScrollBar.height;
         }
         this.setTextSize(availWidth,availHeight,txtPad);
         if(hScrollBar && !vScrollBar && Boolean(this.needVScroll()))
         {
            vScrollBar = true;
            availWidth -= this._verticalScrollBar.width;
            this.setTextSize(availWidth,availHeight,txtPad);
         }
         if(vScrollBar)
         {
            this._verticalScrollBar.visible = true;
            this._verticalScrollBar.x = width - Number(this._verticalScrollBar.width);
            this._verticalScrollBar.height = availHeight;
            this._verticalScrollBar.visible = true;
            this._verticalScrollBar.enabled = this.enabled;
         }
         else
         {
            this._verticalScrollBar.visible = false;
         }
         if(hScrollBar)
         {
            this._horizontalScrollBar.visible = true;
            this._horizontalScrollBar.y = height - Number(this._horizontalScrollBar.height);
            this._horizontalScrollBar.width = availWidth;
            this._horizontalScrollBar.visible = true;
            this._horizontalScrollBar.enabled = this.enabled;
         }
         else
         {
            this._horizontalScrollBar.visible = false;
         }
         this.updateScrollBars();
         addEventListener(Event.ENTER_FRAME,this.delayedLayoutUpdate,false,0,true);
      }
      
      protected function delayedLayoutUpdate(event:Event) : void
      {
         if(this.textHasChanged)
         {
            this.textHasChanged = false;
            this.drawLayout();
            return;
         }
         removeEventListener(Event.ENTER_FRAME,this.delayedLayoutUpdate);
      }
      
      protected function updateScrollBars() : *
      {
         this._horizontalScrollBar.update();
         this._verticalScrollBar.update();
         this._verticalScrollBar.enabled = this.enabled;
         this._horizontalScrollBar.enabled = this.enabled;
         this._horizontalScrollBar.drawNow();
         this._verticalScrollBar.drawNow();
      }
      
      protected function needVScroll() : Boolean
      {
         if(this._verticalScrollPolicy == ScrollPolicy.OFF)
         {
            return false;
         }
         if(this._verticalScrollPolicy == ScrollPolicy.ON)
         {
            return true;
         }
         return this.textField.maxScrollV > 1;
      }
      
      protected function needHScroll() : Boolean
      {
         if(this._horizontalScrollPolicy == ScrollPolicy.OFF)
         {
            return false;
         }
         if(this._horizontalScrollPolicy == ScrollPolicy.ON)
         {
            return true;
         }
         return this.textField.maxScrollH > 0;
      }
      
      protected function setTextSize(width:Number, height:Number, padding:Number) : void
      {
         var w:Number = width - padding * 2;
         var h:Number = height - padding * 2;
         if(w != this.textField.width)
         {
            this.textField.width = w;
         }
         if(h != this.textField.height)
         {
            this.textField.height = h;
         }
      }
      
      override protected function isOurFocus(target:DisplayObject) : Boolean
      {
         return target == this.textField || super.isOurFocus(target);
      }
      
      override protected function focusInHandler(event:FocusEvent) : void
      {
         setIMEMode(true);
         if(event.target == this)
         {
            stage.focus = this.textField;
         }
         var fm:IFocusManager = focusManager;
         if(fm)
         {
            if(this.editable)
            {
               fm.showFocusIndicator = true;
            }
            fm.defaultButtonEnabled = false;
         }
         super.focusInHandler(event);
         if(this.editable)
         {
            setIMEMode(true);
         }
      }
      
      override protected function focusOutHandler(event:FocusEvent) : void
      {
         var fm:IFocusManager = focusManager;
         if(fm)
         {
            fm.defaultButtonEnabled = true;
         }
         this.setSelection(0,0);
         super.focusOutHandler(event);
         if(this.editable)
         {
            setIMEMode(false);
         }
      }
   }
}
