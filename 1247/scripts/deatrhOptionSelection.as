package
{
   import flash.display.*;
   import flash.text.*;
   
   public dynamic class deatrhOptionSelection extends MovieClip
   {
       
      
      public var actionButton:MovieClip;
      
      public var xspot:Object;
      
      public var yspot:Object;
      
      public var respawnMessage:TextField;
      
      public function deatrhOptionSelection()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      internal function frame1() : *
      {
         this.actionButton.gotoAndStop(1);
      }
   }
}
