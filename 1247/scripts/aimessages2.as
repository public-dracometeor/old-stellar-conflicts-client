package
{
   import flash.display.*;
   import flash.events.*;
   import flash.text.*;
   import flash.utils.*;
   
   public dynamic class aimessages2 extends MovieClip
   {
       
      
      public var textBackGround:MovieClip;
      
      public var DisplayingMessage:TextField;
      
      public var timeUntilHide:Object;
      
      public var cancel_button:SimpleButton;
      
      public var NewsPictureWindow:MovieClip;
      
      public function aimessages2()
      {
         super();
         addFrameScript(0,this.frame1,2,this.frame3);
      }
      
      internal function frame1() : *
      {
         this.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN,function(event:MouseEvent):void
         {
            gotoAndStop(1);
         });
         this.visible = false;
         stop();
      }
      
      internal function frame3() : *
      {
         if(this.timeUntilHide > getTimer())
         {
            gotoAndPlay(2);
         }
         else
         {
            gotoAndStop(1);
         }
      }
   }
}
