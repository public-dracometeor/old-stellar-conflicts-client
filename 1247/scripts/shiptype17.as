package
{
   import flash.display.*;
   
   public dynamic class shiptype17 extends MovieClip
   {
       
      
      public var Eng1:MovieClip;
      
      public var Eng2:MovieClip;
      
      public var Eng3:MovieClip;
      
      public function shiptype17()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      internal function frame1() : *
      {
      }
      
      public function setSpeedRatio(param1:*) : *
      {
         this.Eng1.setSpeedRatio(param1);
         this.Eng2.setSpeedRatio(param1);
         this.Eng3.setSpeedRatio(param1);
      }
   }
}
