package
{
   import flash.display.*;
   import flash.events.*;
   import flash.text.*;
   import flash.utils.*;
   
   public dynamic class chatdialogue extends MovieClip
   {
       
      
      public var ChatterTimer:Timer;
      
      public var miscOutPut:TextField;
      
      public var MiscChatt:Object;
      
      public var chatInput:TextField;
      
      public var textdisplay:TextField;
      
      public var sendButton:SimpleButton;
      
      public var textoutline:MovieClip;
      
      public function chatdialogue()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function func_displayChatter() : *
      {
         var _loc_1:* = "";
         _loc_1 += this.MiscChatt[0] + "\r";
         _loc_1 += this.MiscChatt[1] + "\r";
         _loc_1 += this.MiscChatt[2] + "\r";
         _loc_1 += this.MiscChatt[3] + "\r";
         this.miscOutPut.text = _loc_1;
         this.ChatterTimer.stop();
         this.ChatterTimer.start();
      }
      
      internal function frame1() : *
      {
         this.miscOutPut.text = "";
         this.MiscChatt = new Array();
         this.MiscChatt[0] = "";
         this.MiscChatt[1] = "";
         this.MiscChatt[2] = "";
         this.MiscChatt[3] = "";
         this.ChatterTimer = new Timer(5000);
         this.ChatterTimer.addEventListener(TimerEvent.TIMER,this.ChatterTimerHandler);
         stop();
      }
      
      public function ChatterTimerHandler(event:TimerEvent) : void
      {
         this.func_addMiscChatter("");
      }
      
      public function func_removekeys(param1:*) : *
      {
         var _loc_3:* = undefined;
         var _loc_2:* = 0;
         while(_loc_2 < param1.length)
         {
            if(param1.charAt(_loc_2) == "`" || param1.charAt(_loc_2) == "~")
            {
               _loc_3 = " ";
               param1 = param1.substr(0,_loc_2) + _loc_3 + param1.substr(_loc_2 + 1);
            }
            _loc_2 += 1;
         }
         return param1;
      }
      
      public function func_addMiscChatter(param1:*) : *
      {
         this.MiscChatt[0] = this.MiscChatt[1];
         this.MiscChatt[1] = this.MiscChatt[2];
         this.MiscChatt[2] = this.MiscChatt[3];
         this.MiscChatt[3] = param1;
         this.func_displayChatter();
      }
   }
}
