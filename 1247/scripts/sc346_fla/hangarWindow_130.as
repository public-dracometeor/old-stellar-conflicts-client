package sc346_fla
{
   import flash.display.*;
   import flash.events.*;
   import flash.text.*;
   import flash.utils.*;
   
   public dynamic class hangarWindow_130 extends MovieClip
   {
       
      
      public var PurchasePriceOfShip:Object;
      
      public var questionBox:MovieClip;
      
      public var energygenerators:Object;
      
      public var shipDetails1:TextField;
      
      public var shipDetails2:TextField;
      
      public var changeShip:MovieClip;
      
      public var cancel_button:MovieClip;
      
      public var gamesetting:Object;
      
      public var missile:Object;
      
      public var maxShipsToOwn:Object;
      
      public var shieldgenerators:Object;
      
      public var playerscurrentextrashipno:Object;
      
      public var sellShip:MovieClip;
      
      public var currentlySelectedExtraSlot:Object;
      
      public var DisplayScreen:MovieClip;
      
      public var OwnedShipsDisplay:Object;
      
      public var BuyQuestionBox:MovieClip;
      
      public var BuyShip:MovieClip;
      
      public var playershipstatus:Object;
      
      public var DisplayBox:Class;
      
      public var AvailableShipsDisplay:Object;
      
      public var gameSounds:Object;
      
      public var blankersdafadsfdsaf:String;
      
      public var specialshipitems:Object;
      
      public var extraplayerships:Object;
      
      public var capShipsStartAt:Object;
      
      public var exit_button:MovieClip;
      
      public var SalePriceOfShip:Object;
      
      public var shiptype:Object;
      
      public var upgradeShip:MovieClip;
      
      public var energycapacitors:Object;
      
      public var guntype:Object;
      
      public var currentlySelectedShip:Object;
      
      public function hangarWindow_130()
      {
         super();
         addFrameScript(0,this.frame1,6,this.frame7,34,this.frame35);
      }
      
      public function func_AcceptShipPurchase(event:MouseEvent) : void
      {
         var _loc_2:* = this.currentlySelectedExtraSlot;
         this.extraplayerships[_loc_2] = new Array();
         this.extraplayerships[_loc_2][0] = this.currentlySelectedShip;
         var _loc_3:* = 0;
         this.extraplayerships[_loc_2][4] = new Array();
         while(_loc_3 < this.shiptype[this.currentlySelectedShip][2].length)
         {
            this.extraplayerships[_loc_2][4][_loc_3] = 3;
            _loc_3 += 1;
         }
         var _loc_4:* = 0;
         this.extraplayerships[_loc_2][5] = new Array();
         while(_loc_4 < this.shiptype[this.currentlySelectedShip][5].length)
         {
            this.extraplayerships[_loc_2][5][_loc_4] = 2;
            _loc_4 += 1;
         }
         this.extraplayerships[_loc_2][11] = new Array();
         this.extraplayerships[_loc_2][11][1] = new Array();
         this.extraplayerships[_loc_2][1] = 3;
         this.extraplayerships[_loc_2][2] = 3;
         this.extraplayerships[_loc_2][3] = 3;
         this.playershipstatus[3][1] -= this.PurchasePriceOfShip;
         this.func_playRegularClick();
         gotoAndStop("extraships");
      }
      
      public function func_CancelShipPurchase(event:MouseEvent) : void
      {
         this.func_playRegularClick();
         this.BuyQuestionBox.visible = false;
      }
      
      internal function frame1() : *
      {
         this.blankersdafadsfdsaf = "";
         this.changeShip.actionlabel.text = "Set Current Ship";
         this.sellShip.actionlabel.text = "Sell Ship";
         this.exit_button.actionlabel.text = "Exit";
         this.changeShip.gotoAndStop("2");
         this.sellShip.gotoAndStop("2");
         this.exit_button.gotoAndStop("1");
         this.questionBox.visible = false;
         this.DisplayBox = getDefinitionByName("Shipitembox") as Class;
         this.currentlySelectedExtraSlot = -1;
         this.capShipsStartAt = 16;
      }
      
      public function func_ResetButtonsToOff() : *
      {
      }
      
      public function func_InitShipsDisplay() : *
      {
         this.currentlySelectedExtraSlot = this.playerscurrentextrashipno;
         this.questionBox.visible = false;
         this.DisplayScreen.func_clearAllItems();
         this.func_InitCurrentShipsDisplay();
         this.shipDetails1.text = "";
         this.shipDetails2.text = "";
         this.displayShipDetail(this.currentlySelectedExtraSlot);
         this.func_initLeftSideMenu(this.currentlySelectedExtraSlot);
      }
      
      public function displayShipDetail(param1:*) : *
      {
         var _loc_8:* = undefined;
         var _loc_9:* = undefined;
         this.shipDetails1.text = "";
         var _loc_2:* = "";
         var _loc_3:* = this.extraplayerships[param1][0];
         _loc_2 += "Ship Name: " + this.shiptype[_loc_3][0] + "\r\r";
         _loc_2 += "Current Setup:\r";
         var _loc_4:* = this.extraplayerships[param1][1];
         _loc_2 += "Shield Generator: " + this.shieldgenerators[_loc_4][4] + "\r";
         _loc_2 += "  Max Shield Strength: " + this.shieldgenerators[_loc_4][0] + " \r";
         var _loc_5:* = this.extraplayerships[param1][2];
         _loc_2 += "Energy Generator: " + this.energygenerators[_loc_5][1] + "\r";
         _loc_2 += "  Charge Rate:" + this.energygenerators[_loc_5][0] + "\r";
         var _loc_6:* = this.extraplayerships[param1][3];
         _loc_2 += "Energy Capacitor: " + this.energycapacitors[_loc_6][1] + "\r";
         _loc_2 += "  Max Stored Energy:" + this.energycapacitors[_loc_6][0] + "\r";
         _loc_2 += "\rGuns:\r";
         var _loc_7:* = 0;
         while(_loc_7 < this.extraplayerships[param1][4].length)
         {
            _loc_8 = this.extraplayerships[param1][4][_loc_7];
            if(!isNaN(_loc_8))
            {
               _loc_2 += "Gun " + (_loc_7 + 1) + ": " + this.guntype[_loc_8][6] + "\r";
            }
            else
            {
               _loc_2 += "Gun " + (_loc_7 + 1) + ": None\r";
            }
            _loc_7 += 1;
         }
         if(this.extraplayerships[param1][5].length > 0)
         {
            _loc_2 += "Turrets:\r";
            _loc_7 = 0;
            while(_loc_7 < this.extraplayerships[param1][5].length)
            {
               _loc_9 = this.extraplayerships[param1][5][_loc_7];
               if(!isNaN(_loc_9))
               {
                  _loc_2 += "Turret " + (_loc_7 + 1) + ": " + this.guntype[_loc_9][6] + "\r";
               }
               else
               {
                  _loc_2 += "Turret " + (_loc_7 + 1) + ": None\r";
               }
               _loc_7 += 1;
            }
         }
         this.shipDetails1.text = _loc_2;
         this.shipDetails2.text = this.displayMaxStatsDetail(_loc_3);
      }
      
      public function func_PurchaseNewShip(event:MouseEvent) : void
      {
         var _loc_2:* = Number(event.target.parent.name);
         this.currentlySelectedExtraSlot = _loc_2;
         this.func_playRegularClick();
         gotoAndStop("newShip");
      }
      
      public function func_ShipBuyButton(event:MouseEvent) : void
      {
         if(this.currentlySelectedShip >= 0)
         {
            this.PurchasePriceOfShip = this.shiptype[this.currentlySelectedShip][1];
            if(this.PurchasePriceOfShip <= this.playershipstatus[3][1])
            {
               this.BuyQuestionBox.visible = true;
               this.BuyQuestionBox.question.text = "Buy " + this.shiptype[this.currentlySelectedShip][0] + " \r For: " + this.PurchasePriceOfShip;
               this.func_playRegularClick();
            }
         }
      }
      
      public function func_InitAvailShipsDisplay() : *
      {
         var _loc_2:* = undefined;
         var _loc_3:* = undefined;
         var _loc_4:* = undefined;
         var _loc_5:* = undefined;
         var _loc_6:* = undefined;
         var _loc_7:* = undefined;
         this.AvailableShipsDisplay = new Array();
         var _loc_1:* = 0;
         while(_loc_1 < this.shiptype.length)
         {
            if(this.shiptype[_loc_1] != null)
            {
               if(this.shiptype[_loc_1][0] != null)
               {
                  _loc_2 = _loc_1;
                  _loc_3 = this.AvailableShipsDisplay.length;
                  this.AvailableShipsDisplay[_loc_3] = new Object();
                  this.AvailableShipsDisplay[_loc_3].DisplayBox = new this.DisplayBox() as MovieClip;
                  this.AvailableShipsDisplay[_loc_3].DisplayBox.name = _loc_2;
                  this.AvailableShipsDisplay[_loc_3].DisplayBox.current.text = "";
                  this.AvailableShipsDisplay[_loc_3].DisplayBox.itemname.text = this.shiptype[_loc_2][0];
                  this.AvailableShipsDisplay[_loc_3].DisplayBox.itemspecs.text = "";
                  this.AvailableShipsDisplay[_loc_3].DisplayBox.current.text = "Cost: " + this.FormatNumber(this.shiptype[_loc_2][1]);
                  this.AvailableShipsDisplay[_loc_3].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_BuyingAShip);
                  this.DisplayScreen.func_addItem(this.AvailableShipsDisplay[_loc_3].DisplayBox);
                  _loc_4 = new this.shiptype[_loc_2][10]() as MovieClip;
                  _loc_4.x = Number(this.AvailableShipsDisplay[_loc_3].DisplayBox.width) / 2;
                  _loc_4.y = Number(this.AvailableShipsDisplay[_loc_3].DisplayBox.height) / 2;
                  this.AvailableShipsDisplay[_loc_3].DisplayBox.addChild(_loc_4);
                  _loc_5 = Number(this.AvailableShipsDisplay[_loc_3].DisplayBox.width) * 0.8;
                  _loc_6 = false;
                  if(_loc_4.width > _loc_5)
                  {
                     _loc_7 = _loc_5 / Number(_loc_4.width);
                     _loc_4.scaleX = _loc_7;
                     _loc_4.scaleY = _loc_7;
                  }
               }
            }
            _loc_1 += 1;
         }
      }
      
      public function func_CancelShipSale(event:MouseEvent) : void
      {
         this.func_playRegularClick();
         this.questionBox.visible = false;
      }
      
      public function func_playRegularClick() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.regularClick.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      public function FormatNumber(param1:*) : String
      {
         param1 = Number(param1);
         if(isNaN(param1))
         {
            param1 = 0;
         }
         param1 = Math.round(param1);
         var _loc_2:* = false;
         var _loc_3:* = "";
         if(param1 < 0)
         {
            _loc_2 = true;
         }
         param1 = Math.abs(param1);
         param1 = String(param1);
         if(param1.length > 3)
         {
            _loc_3 = param1.substr(Number(param1.length) - 3);
            param1 = Number(param1);
            while(param1 > 999)
            {
               if(param1 > 999)
               {
                  param1 = Math.floor(Number(param1) / 1000);
                  param1 = String(param1);
                  if(param1.length > 3)
                  {
                     _loc_3 = param1.substr(Number(param1.length) - 3) + "," + _loc_3;
                  }
                  else
                  {
                     _loc_3 = param1 + "," + _loc_3;
                  }
                  param1 = Number(param1);
               }
            }
         }
         else
         {
            _loc_3 = param1;
         }
         if(_loc_2 == true)
         {
            _loc_3 = "-" + _loc_3;
         }
         return _loc_3;
      }
      
      public function displayMaxStatsDetail(param1:*) : *
      {
         var _loc_2:* = "";
         _loc_2 += "Top Setup Available:\r";
         _loc_2 += "Max Structure: " + this.shiptype[param1][3][3] + "\r";
         var _loc_3:* = this.shiptype[param1][6][0];
         if(_loc_3 > this.shieldgenerators.length)
         {
            _loc_3 = this.shieldgenerators.length - 1;
         }
         _loc_2 += "Shield Generator: " + this.shieldgenerators[_loc_3][4] + "\r";
         _loc_2 += "  Max Shield Strength: " + this.shieldgenerators[_loc_3][0] + "\r";
         var _loc_4:* = this.shiptype[param1][6][2];
         if(this.shiptype[param1][6][2] > this.energygenerators.length)
         {
            _loc_4 = this.energygenerators.length - 1;
         }
         _loc_2 += "Energy Generator: " + this.energygenerators[_loc_4][1] + "\r";
         _loc_2 += "  Charge Rate: " + this.energygenerators[_loc_4][0] + "\r";
         var _loc_5:* = this.shiptype[param1][6][1];
         if(this.shiptype[param1][6][1] > this.energycapacitors.length)
         {
            _loc_5 = this.energycapacitors.length - 1;
         }
         _loc_2 += "Energy Capacitor: " + this.energycapacitors[_loc_5][1] + "\r";
         _loc_2 += "  Max Energy: " + this.energycapacitors[_loc_5][0] + "\r";
         _loc_2 += "Guns: " + this.shiptype[param1][2].length + "\r";
         _loc_2 += "Turrets: " + this.shiptype[param1][5].length + "\r";
         var _loc_6:* = this.shiptype[param1][6][3];
         if(this.shiptype[param1][6][3] > this.guntype.length)
         {
            _loc_6 = this.guntype.length - 1;
         }
         _loc_2 += "Best Gun: " + this.guntype[_loc_6][6] + "\r";
         _loc_2 += "Missile Banks: " + this.shiptype[param1][7].length + "\r";
         _loc_2 += "Maximum Specials: " + this.shiptype[param1][3][7] + "\r";
         _loc_2 += "Unique Specials: \r";
         if(this.shiptype[param1][3][8])
         {
            _loc_2 += "  Cloak\r";
         }
         if(this.shiptype[param1][3][12])
         {
            _loc_2 += "  Rapid Missiles\r";
         }
         return _loc_2;
      }
      
      public function func_initLeftSideMenu(param1:*) : *
      {
         if(param1 == this.playerscurrentextrashipno)
         {
            this.changeShip.gotoAndStop("2");
            this.sellShip.gotoAndStop("2");
            this.upgradeShip.visible = true;
         }
         else if(this.WillNotHaveANonCapLeft(param1))
         {
            this.changeShip.gotoAndStop("1");
            this.sellShip.gotoAndStop("2");
            this.upgradeShip.visible = false;
         }
         else
         {
            this.changeShip.gotoAndStop("1");
            this.sellShip.gotoAndStop("1");
            this.upgradeShip.visible = false;
         }
      }
      
      public function func_SellCurrShip(event:MouseEvent) : void
      {
         var _loc_2:* = undefined;
         var _loc_3:* = undefined;
         var _loc_4:* = undefined;
         if(this.playerscurrentextrashipno != this.currentlySelectedExtraSlot)
         {
            _loc_2 = 0;
            _loc_2 += Number(this.shiptype[this.extraplayerships[this.currentlySelectedExtraSlot][0]][1]) / 1.5;
            _loc_3 = 0;
            while(_loc_3 < this.shiptype[this.extraplayerships[this.currentlySelectedExtraSlot][0]][2].length)
            {
               if(isNaN(Number(this.extraplayerships[this.currentlySelectedExtraSlot][4][_loc_3])))
               {
               }
               _loc_3 += 1;
            }
            _loc_4 = 0;
            while(_loc_4 < this.shiptype[this.extraplayerships[this.currentlySelectedExtraSlot][0]][5].length)
            {
               if(isNaN(Number(this.extraplayerships[this.currentlySelectedExtraSlot][5][_loc_4])))
               {
               }
               _loc_4 += 1;
            }
            _loc_2 = Math.round(_loc_2);
            if(!isNaN(_loc_2))
            {
               this.SalePriceOfShip = _loc_2;
            }
            else
            {
               this.SalePriceOfShip = 0;
            }
            this.questionBox.question.text = "Sell " + this.shiptype[this.extraplayerships[this.currentlySelectedExtraSlot][0]][0] + "\r For " + this.SalePriceOfShip + "?";
            this.questionBox.visible = true;
            this.func_playRegularClick();
         }
      }
      
      public function displayBuyingShipDetail(param1:*) : *
      {
         this.currentlySelectedShip = param1;
         this.shipDetails1.text = "";
         var _loc_2:* = "";
         _loc_2 += "Ship Name: " + this.shiptype[param1][0] + "\r";
         _loc_2 += "Cost: " + this.shiptype[param1][1] + "\r\r";
         _loc_2 += "Rotation: " + this.shiptype[param1][3][2] + " Degrees / Second \r";
         _loc_2 += "Max Speed: " + this.shiptype[param1][3][1] + " Feet / Second \r";
         _loc_2 += "Boosted Speed: " + this.shiptype[param1][3][6] + " Feet / Second \r";
         _loc_2 += "Acceleration: " + this.shiptype[param1][3][0] + " Feet / Second \r\r";
         _loc_2 += "Maximum Structure: " + this.shiptype[param1][3][3] + " \r";
         _loc_2 += "Maximum Cargo: " + this.shiptype[param1][4] + " \r";
         this.shipDetails1.text = _loc_2;
         this.shipDetails2.text = this.displayMaxStatsDetail(param1);
      }
      
      public function func_AcceptShipSale(event:MouseEvent) : void
      {
         this.playershipstatus[3][1] += this.SalePriceOfShip;
         this.extraplayerships[this.currentlySelectedExtraSlot] = new Array();
         this.extraplayerships[this.currentlySelectedExtraSlot][0] = null;
         this.func_playRegularClick();
         this.func_InitShipsDisplay();
      }
      
      public function func_InitBuyShipsDisplay() : *
      {
         this.BuyQuestionBox.visible = false;
         this.DisplayScreen.func_clearAllItems();
         this.func_InitAvailShipsDisplay();
         this.shipDetails1.text = "";
         this.shipDetails2.text = "";
      }
      
      internal function frame35() : *
      {
         this.exit_button.visible = false;
         this.BuyShip.gotoAndStop("1");
         this.cancel_button.gotoAndStop("1");
         this.BuyShip.actionlabel.text = "Buy Ship";
         this.BuyShip.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ShipBuyButton);
         this.cancel_button.actionlabel.text = "Cancel";
         this.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_CancelShipBuy);
         this.AvailableShipsDisplay = new Array();
         this.PurchasePriceOfShip = 0;
         this.currentlySelectedShip = -1;
         this.func_InitBuyShipsDisplay();
         this.BuyQuestionBox.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_CancelShipPurchase);
         this.BuyQuestionBox.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_AcceptShipPurchase);
      }
      
      public function func_InitCurrentShipsDisplay() : *
      {
         var _loc_2:* = undefined;
         var _loc_3:* = undefined;
         var _loc_4:* = undefined;
         var _loc_5:* = undefined;
         var _loc_6:* = undefined;
         var _loc_7:* = undefined;
         var _loc_1:* = 0;
         while(_loc_1 < this.maxShipsToOwn)
         {
            this.OwnedShipsDisplay[_loc_1] = new Object();
            this.OwnedShipsDisplay[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
            this.OwnedShipsDisplay[_loc_1].DisplayBox.name = _loc_1;
            this.OwnedShipsDisplay[_loc_1].DisplayBox.current.text = "";
            _loc_2 = true;
            if(this.extraplayerships[_loc_1] != null)
            {
               if(this.extraplayerships[_loc_1][0] != null)
               {
                  _loc_2 = false;
                  _loc_3 = this.extraplayerships[_loc_1][0];
                  this.OwnedShipsDisplay[_loc_1].DisplayBox.itemname.text = this.shiptype[_loc_3][0];
                  this.OwnedShipsDisplay[_loc_1].DisplayBox.itemspecs.text = "";
                  this.OwnedShipsDisplay[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_extraShipSelected);
                  if(_loc_1 == this.playerscurrentextrashipno)
                  {
                     this.OwnedShipsDisplay[_loc_1].DisplayBox.current.text = "CURRENT SHIP";
                     this.OwnedShipsDisplay[_loc_1].DisplayBox.gotoAndStop(2);
                  }
                  _loc_4 = new this.shiptype[_loc_3][10]() as MovieClip;
                  _loc_4.x = Number(this.OwnedShipsDisplay[_loc_1].DisplayBox.width) / 2;
                  _loc_4.y = Number(this.OwnedShipsDisplay[_loc_1].DisplayBox.height) / 2;
                  this.OwnedShipsDisplay[_loc_1].DisplayBox.addChild(_loc_4);
                  _loc_5 = Number(this.OwnedShipsDisplay[_loc_1].DisplayBox.width) * 0.8;
                  _loc_6 = false;
                  if(_loc_4.width > _loc_5)
                  {
                     _loc_7 = _loc_5 / Number(_loc_4.width);
                     _loc_4.scaleX = _loc_7;
                     _loc_4.scaleY = _loc_7;
                  }
               }
            }
            if(_loc_2)
            {
               this.OwnedShipsDisplay[_loc_1].DisplayBox.itemname.text = "Empty Spot";
               this.OwnedShipsDisplay[_loc_1].DisplayBox.itemspecs.text = "Ship Slot \r Available";
               this.OwnedShipsDisplay[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_PurchaseNewShip);
            }
            this.DisplayScreen.func_addItem(this.OwnedShipsDisplay[_loc_1].DisplayBox);
            _loc_1 += 1;
         }
      }
      
      public function WillNotHaveANonCapLeft(param1:*) : *
      {
         var _loc_2:* = undefined;
         var _loc_3:* = undefined;
         if(this.extraplayerships[param1][0] >= this.capShipsStartAt)
         {
            trace("ran" + this.extraplayerships[param1][0] + "`" + this.capShipsStartAt);
            return false;
         }
         _loc_2 = false;
         _loc_3 = 0;
         while(_loc_3 < this.maxShipsToOwn)
         {
            if(this.extraplayerships[_loc_3] != null)
            {
               if(this.extraplayerships[_loc_3][0] != null)
               {
                  if(this.extraplayerships[_loc_3][0] < this.capShipsStartAt)
                  {
                     if(_loc_2)
                     {
                        trace("ran2" + this.extraplayerships[param1][0] + "`" + this.capShipsStartAt);
                        return false;
                     }
                     _loc_2 = true;
                     trace("ranholder" + _loc_3);
                  }
               }
            }
            _loc_3 += 1;
         }
         trace("no run");
         return true;
      }
      
      public function func_BuyingAShip(event:MouseEvent) : void
      {
         var _loc_2:* = Number(event.target.parent.name);
         this.func_playRegularClick();
         var _loc_3:* = 0;
         while(_loc_3 < this.AvailableShipsDisplay.length)
         {
            this.AvailableShipsDisplay[_loc_3].DisplayBox.gotoAndStop(1);
            _loc_3 += 1;
         }
         event.target.parent.gotoAndStop(2);
         this.displayBuyingShipDetail(_loc_2);
      }
      
      public function func_ChangeCurrShip(event:MouseEvent) : void
      {
         this.playerscurrentextrashipno = this.currentlySelectedExtraSlot;
         Object(root).func_checkForShipChange();
         this.func_playRegularClick();
         this.func_InitShipsDisplay();
      }
      
      internal function frame7() : *
      {
         this.exit_button.visible = true;
         this.changeShip.actionlabel.text = "Set Current Ship";
         this.sellShip.actionlabel.text = "Sell Ship";
         this.exit_button.actionlabel.text = "Exit";
         this.upgradeShip.actionlabel.text = "Upgrade Ship";
         this.changeShip.gotoAndStop("2");
         this.sellShip.gotoAndStop("2");
         this.exit_button.gotoAndStop("1");
         this.upgradeShip.gotoAndStop("1");
         this.questionBox.visible = false;
         this.upgradeShip.addEventListener(MouseEvent.MOUSE_DOWN,this.func_upgradeShip);
         this.DisplayScreen.func_clearAllItems();
         this.OwnedShipsDisplay = new Array();
         this.SalePriceOfShip = 0;
         this.func_InitShipsDisplay();
         this.changeShip.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ChangeCurrShip);
         this.sellShip.addEventListener(MouseEvent.MOUSE_DOWN,this.func_SellCurrShip);
         this.questionBox.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_CancelShipSale);
         this.questionBox.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_AcceptShipSale);
         stop();
      }
      
      public function func_extraShipSelected(event:MouseEvent) : void
      {
         var _loc_2:* = Number(event.target.parent.name);
         var _loc_3:* = 0;
         while(_loc_3 < this.OwnedShipsDisplay.length)
         {
            this.OwnedShipsDisplay[_loc_3].DisplayBox.gotoAndStop(1);
            _loc_3 += 1;
         }
         event.target.parent.gotoAndStop(2);
         this.currentlySelectedExtraSlot = _loc_2;
         this.displayShipDetail(this.currentlySelectedExtraSlot);
         this.func_initLeftSideMenu(this.currentlySelectedExtraSlot);
         this.func_playRegularClick();
      }
      
      public function func_CancelShipBuy(event:MouseEvent) : void
      {
         this.func_playRegularClick();
         gotoAndStop("extraships");
      }
      
      public function func_upgradeShip(event:MouseEvent) : void
      {
         Object(root).gotoAndStop("ShipHardware");
      }
   }
}
