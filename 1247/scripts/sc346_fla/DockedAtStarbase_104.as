package sc346_fla
{
   import flash.display.*;
   
   public dynamic class DockedAtStarbase_104 extends MovieClip
   {
       
      
      public var game_options_button:SimpleButton;
      
      public var base_exit_button:SimpleButton;
      
      public var zoneinterface:MovieClip;
      
      public var trageGoodsSelector:SimpleButton;
      
      public var hangarButton:SimpleButton;
      
      public var base_hardware_button:SimpleButton;
      
      public var game_missions_button:SimpleButton;
      
      public function DockedAtStarbase_104()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      internal function frame1() : *
      {
         stop();
      }
   }
}
