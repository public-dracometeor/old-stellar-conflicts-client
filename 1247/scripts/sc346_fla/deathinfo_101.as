package sc346_fla
{
   import flash.display.*;
   import flash.events.*;
   import flash.text.*;
   import flash.utils.*;
   
   public dynamic class deathinfo_101 extends MovieClip
   {
       
      
      public var capShipDeathPenalty:Object;
      
      public var insult:TextField;
      
      public var playershipstatus:Object;
      
      public var blankersdafadsfdsaf:String;
      
      public var Insults:Object;
      
      public var optionSelection:Object;
      
      public var playersshiptype:Object;
      
      public var isCapShip:Object;
      
      public var otherplayership:Object;
      
      public var optionsAvail:Object;
      
      public var playersMaxRepairCost:Object;
      
      public var shiptype:Object;
      
      public var reapirCost:Object;
      
      public function deathinfo_101()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function FormatNumber(param1:*) : String
      {
         param1 = Number(param1);
         if(isNaN(param1))
         {
            param1 = 0;
         }
         param1 = Math.round(param1);
         var _loc_2:* = false;
         var _loc_3:* = "";
         if(param1 < 0)
         {
            _loc_2 = true;
         }
         param1 = Math.abs(param1);
         param1 = String(param1);
         if(param1.length > 3)
         {
            _loc_3 = param1.substr(Number(param1.length) - 3);
            param1 = Number(param1);
            while(param1 > 999)
            {
               if(param1 > 999)
               {
                  param1 = Math.floor(Number(param1) / 1000);
                  param1 = String(param1);
                  if(param1.length > 3)
                  {
                     _loc_3 = param1.substr(Number(param1.length) - 3) + "," + _loc_3;
                  }
                  else
                  {
                     _loc_3 = param1 + "," + _loc_3;
                  }
                  param1 = Number(param1);
               }
            }
         }
         else
         {
            _loc_3 = param1;
         }
         if(_loc_2 == true)
         {
            _loc_3 = "-" + _loc_3;
         }
         return _loc_3;
      }
      
      public function func_AcceptRepairs_Click(event:MouseEvent) : void
      {
         Object(root).playershipstatus[3][1] = Number(Object(root).playershipstatus[3][1]) - Number(this.reapirCost);
         this.func_gotoDockedScreen();
      }
      
      internal function frame1() : *
      {
         this.blankersdafadsfdsaf = "";
         this.Insults = new Array();
         this.Insults[this.Insults.length] = "Well... Someone has to win and someone has too loose.";
         this.Insults[this.Insults.length] = "Cheer up, scrap prices are on the rise!";
         this.Insults[this.Insults.length] = "Someday you might be good, someday...";
         this.insult.text = this.Insults[Math.round(Math.random() * (this.Insults.length - 1))];
         this.optionSelection = getDefinitionByName("deatrhOptionSelection") as Class;
         this.playersshiptype = this.playershipstatus[5][0];
         this.isCapShip = this.shiptype[this.playersshiptype][3][10];
         this.capShipDeathPenalty = 0.1;
         this.reapirCost = 0;
         if(this.isCapShip)
         {
            this.reapirCost = Math.floor(Number(this.shiptype[this.playersshiptype][1]) * Number(this.capShipDeathPenalty));
            this.playersMaxRepairCost = Math.floor(Number(this.playershipstatus[3][1]) * Number(this.capShipDeathPenalty));
            if(this.reapirCost > this.playersMaxRepairCost)
            {
               this.reapirCost = this.playersMaxRepairCost;
            }
         }
         this.optionsAvail = new Array();
         addEventListener(Event.ENTER_FRAME,this.func_OnDeathFrame);
         stop();
      }
      
      public function func_OnDeathFrame(event:Event) : *
      {
         var _loc_2:* = undefined;
         var _loc_3:* = undefined;
         _loc_2 = 0;
         while(_loc_2 < this.optionsAvail.length)
         {
            this.removeChild(this.optionsAvail[_loc_2]);
            _loc_2 += 1;
         }
         this.optionsAvail = new Array();
         if(this.isCapShip)
         {
            this.optionsAvail[this.optionsAvail.length] = new this.optionSelection() as MovieClip;
            this.optionsAvail[this.optionsAvail.length - 1].respawnMessage.text = "Dock and Repair for:" + this.FormatNumber(this.reapirCost);
            this.optionsAvail[this.optionsAvail.length - 1].actionButton.actionlabel.text = "Repair";
            this.optionsAvail[this.optionsAvail.length - 1].actionButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_AcceptRepairs_Click);
            this.optionsAvail[this.optionsAvail.length - 1].y = (this.optionsAvail.length - 1) * Number(this.optionsAvail[this.optionsAvail.length - 1].height);
            this.addChild(this.optionsAvail[this.optionsAvail.length - 1]);
         }
         else
         {
            this.optionsAvail[this.optionsAvail.length] = new this.optionSelection() as MovieClip;
            this.optionsAvail[this.optionsAvail.length - 1].respawnMessage.text = "Last Docked Base/Planet";
            this.optionsAvail[this.optionsAvail.length - 1].actionButton.actionlabel.text = "Respawn";
            this.optionsAvail[this.optionsAvail.length - 1].actionButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_AcceptDeathButton_Click);
            this.optionsAvail[this.optionsAvail.length - 1].y = (this.optionsAvail.length - 1) * Number(this.optionsAvail[this.optionsAvail.length - 1].height);
            this.addChild(this.optionsAvail[this.optionsAvail.length - 1]);
            this.otherplayership = Object(root).otherplayership;
            _loc_2 = 0;
            while(_loc_2 < this.otherplayership.length)
            {
               if(this.otherplayership[_loc_2] != null)
               {
                  if(this.playershipstatus[3][0] != this.otherplayership[_loc_2][0])
                  {
                     if(this.otherplayership[_loc_2][56] == "friend")
                     {
                        _loc_3 = this.otherplayership[_loc_2][11];
                        if(this.shiptype[_loc_3][3][11])
                        {
                           this.optionsAvail[this.optionsAvail.length] = new this.optionSelection() as MovieClip;
                           this.optionsAvail[this.optionsAvail.length - 1].name = "1";
                           this.optionsAvail[this.optionsAvail.length - 1].respawnMessage.text = "At Carrier:" + Object(root).func_getPlayersName(this.otherplayership[_loc_2][0]);
                           this.optionsAvail[this.optionsAvail.length - 1].xspot = this.otherplayership[_loc_2][1];
                           this.optionsAvail[this.optionsAvail.length - 1].yspot = this.otherplayership[_loc_2][2];
                           this.optionsAvail[this.optionsAvail.length - 1].actionButton.actionlabel.text = "Carrier Spawn";
                           this.optionsAvail[this.optionsAvail.length - 1].actionButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_RespawnAtCarrier_Click);
                           this.optionsAvail[this.optionsAvail.length - 1].y = (this.optionsAvail.length - 1) * Number(this.optionsAvail[this.optionsAvail.length - 1].height);
                           this.addChild(this.optionsAvail[this.optionsAvail.length - 1]);
                        }
                     }
                  }
               }
               _loc_2 += 1;
            }
         }
      }
      
      public function func_RespawnAtCarrier_Click(event:MouseEvent) : void
      {
         removeEventListener(Event.ENTER_FRAME,this.func_OnDeathFrame);
         Object(root).shipcoordinatex = event.target.parent.parent.xspot;
         Object(root).shipcoordinatey = event.target.parent.parent.yspot;
         Object(root).gotoAndStop("respawnTransition");
      }
      
      public function func_AcceptDeathButton_Click(event:MouseEvent) : void
      {
         this.func_gotoDockedScreen();
      }
      
      public function func_gotoDockedScreen() : *
      {
         removeEventListener(Event.ENTER_FRAME,this.func_OnDeathFrame);
         Object(root).gotoAndStop("dockedscreen");
      }
   }
}
