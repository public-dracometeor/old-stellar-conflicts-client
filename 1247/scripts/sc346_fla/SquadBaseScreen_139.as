package sc346_fla
{
   import flash.display.*;
   import flash.events.*;
   import flash.text.*;
   import flash.utils.*;
   
   public dynamic class SquadBaseScreen_139 extends MovieClip
   {
       
      
      public var BaseLevel:Object;
      
      public var s0:MovieClip;
      
      public var gamesetting:Object;
      
      public var mysocket:Object;
      
      public var buyweapon:MovieClip;
      
      public var squadoptions:MovieClip;
      
      public var shieldgenerators:Object;
      
      public var baseimage:MovieClip;
      
      public var gunBeingBought:Object;
      
      public var squadbaseinfo:Object;
      
      public var shipstopgun:Object;
      
      public var l0:MovieClip;
      
      public var MaxBaseLevel:Object;
      
      public var BaseTurrets:Object;
      
      public var currentvalueofgenerator:Object;
      
      public var blankersdafadsfdsaf:String;
      
      public var gameoptions:MovieClip;
      
      public var currBaseInfo:TextField;
      
      public var upgBaseInfo:TextField;
      
      public var exit_button:MovieClip;
      
      public var guntype:Object;
      
      public var MaxGunLevel:Object;
      
      public var baseStructure:Object;
      
      public var currentlySelectedShieldGen:Object;
      
      public var g0:MovieClip;
      
      public var g1:MovieClip;
      
      public var g2:MovieClip;
      
      public var g3:MovieClip;
      
      public var g4:MovieClip;
      
      public var DisplayScreen:MovieClip;
      
      public var CurrentActionDoing:Object;
      
      public var DisplayBox:Class;
      
      public var GunsDisplayed:Object;
      
      public var maxGenerator:Object;
      
      public var h0:MovieClip;
      
      public var gameSounds:Object;
      
      public var playershipstatus:Object;
      
      public var CurrentSpotPurchased:Object;
      
      public var BaseGenerator:Object;
      
      public var MaxbaseStructure:Object;
      
      public var GunHolder:Object;
      
      public var ShieldGenTypeBeingBought:Object;
      
      public var ShieldGensDisplayed:Object;
      
      public var UpgradeCost:Object;
      
      public function SquadBaseScreen_139()
      {
         super();
         addFrameScript(0,this.frame1,10,this.frame11);
      }
      
      public function func_setButtons() : *
      {
         var _loc_1:* = 0;
         _loc_1 = 0;
         while(_loc_1 < 5)
         {
            this["g" + _loc_1].gotoAndStop(1);
            if(_loc_1 < this.BaseTurrets.length)
            {
               this["g" + _loc_1].itemText.text = _loc_1 + 1 + " " + this.guntype[this.BaseTurrets[_loc_1]][6];
               this["g" + _loc_1].visible = true;
            }
            else
            {
               this["g" + _loc_1].visible = false;
            }
            _loc_1 += 1;
         }
         this.s0.gotoAndStop(1);
         this.s0.itemText.text = this.shieldgenerators[this.BaseGenerator][4];
         this.h0.gotoAndStop(1);
         this.h0.itemText.text = "Repair: " + this.FormatNumber(Number(this.MaxbaseStructure) - Number(this.baseStructure));
         this.l0.gotoAndStop(1);
         this.l0.itemText.text = "Base Level: " + (this.BaseLevel + 1);
         this.baseimage.gotoAndStop(this.BaseLevel + 1);
      }
      
      public function func_getBaseDetail() : *
      {
         var _loc_1:* = "SQUADB`STATS`" + this.playershipstatus[5][10] + "`" + "~";
         this.mysocket.send(_loc_1);
         trace("sent base load for " + this.playershipstatus[5][10]);
      }
      
      public function FormatNumber(param1:*) : String
      {
         param1 = Number(param1);
         if(isNaN(param1))
         {
            param1 = 0;
         }
         param1 = Math.round(param1);
         var _loc_2:* = false;
         var _loc_3:* = "";
         if(param1 < 0)
         {
            _loc_2 = true;
         }
         param1 = Math.abs(param1);
         param1 = String(param1);
         if(param1.length > 3)
         {
            _loc_3 = param1.substr(Number(param1.length) - 3);
            param1 = Number(param1);
            while(param1 > 999)
            {
               if(param1 > 999)
               {
                  param1 = Math.floor(Number(param1) / 1000);
                  param1 = String(param1);
                  if(param1.length > 3)
                  {
                     _loc_3 = param1.substr(Number(param1.length) - 3) + "," + _loc_3;
                  }
                  else
                  {
                     _loc_3 = param1 + "," + _loc_3;
                  }
                  param1 = Number(param1);
               }
            }
         }
         else
         {
            _loc_3 = param1;
         }
         if(_loc_2 == true)
         {
            _loc_3 = "-" + _loc_3;
         }
         return _loc_3;
      }
      
      public function func_setButtonsListeners() : *
      {
         var _loc_1:* = 0;
         _loc_1 = 0;
         while(_loc_1 < 5)
         {
            this["g" + _loc_1].addEventListener(MouseEvent.MOUSE_DOWN,this.func_ListItemClick);
            _loc_1 += 1;
         }
         this.s0.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ListItemClick);
         this.h0.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ListItemClick);
         this.l0.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ListItemClick);
      }
      
      internal function frame11() : *
      {
         this.ShieldGensDisplayed = new Array();
         this.currentlySelectedShieldGen = 0;
         this.currentvalueofgenerator = 0;
         this.ShieldGenTypeBeingBought = -1;
         this.DisplayScreen.visible = false;
         this.buyweapon.visible = false;
         this.GunsDisplayed = new Array();
         this.GunHolder = new Array();
         this.shipstopgun = 0;
         this.gunBeingBought = -1;
         this.buyweapon.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.Accept_Current_Purchase);
         this.buyweapon.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelGunPurchase);
         this.BaseTurrets = new Array();
         this.MaxGunLevel = 5;
         this.BaseGenerator = 0;
         this.maxGenerator = 6;
         this.baseStructure = 0;
         this.MaxbaseStructure = 350000;
         this.BaseLevel = 0;
         this.MaxBaseLevel = this.squadbaseinfo.length - 1;
         this.func_getBaseDetail();
         this.func_setButtons();
         this.func_DisplayBaseStats();
         this.func_setButtonsListeners();
         this.CurrentSpotPurchased = 0;
         this.CurrentActionDoing = "";
         this.UpgradeCost = 0;
         stop();
      }
      
      internal function frame1() : *
      {
         this.blankersdafadsfdsaf = "";
         this.DisplayBox = getDefinitionByName("hardwareitembox") as Class;
         this.exit_button.actionlabel.text = "Exit Base";
         this.exit_button.gotoAndStop(1);
      }
      
      public function func_InitGunsDisplay() : *
      {
         var _loc_2:* = undefined;
         var _loc_1:* = 0;
         while(_loc_1 < this.shipstopgun)
         {
            if(this.guntype[_loc_1] != null)
            {
               _loc_2 = _loc_1;
               this.GunsDisplayed[_loc_1] = new Object();
               this.GunsDisplayed[_loc_1].gunNumber = _loc_2;
               this.GunsDisplayed[_loc_1].cost = this.guntype[_loc_2][5];
               this.GunsDisplayed[_loc_1].gunName = this.guntype[_loc_2][6];
               this.GunsDisplayed[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
               this.GunsDisplayed[_loc_1].DisplayBox.itemname.text = this.guntype[_loc_2][6];
               this.GunsDisplayed[_loc_1].DisplayBox.itemcost.text = "Cost:" + this.FormatNumber(this.guntype[_loc_2][5]);
               this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text = "Shld/Struct/Pierce\r  ";
               this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text += this.guntype[_loc_2][4] + " / " + this.guntype[_loc_2][7] + " / " + this.guntype[_loc_2][8] + " \r";
               this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text += "Energy Drain: N/A \r";
               this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text += "Life Time: " + this.guntype[_loc_2][1] + "/s \r";
               this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text += "Rel:" + this.guntype[_loc_2][2] + "/s  \r";
               this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text += "Spd:" + this.guntype[_loc_2][0] + "/s ";
               this.GunsDisplayed[_loc_1].DisplayBox.name = _loc_2;
               this.GunsDisplayed[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_BuyNewWeapon);
               this.DisplayScreen.func_addItem(this.GunsDisplayed[_loc_1].DisplayBox);
            }
            _loc_1 += 1;
         }
      }
      
      public function func_InitShieldGensDisplay() : *
      {
         var _loc_3:* = undefined;
         this.DisplayScreen.func_clearAllItems();
         this.DisplayScreen.visible = true;
         this.ShieldGensDisplayed = new Array();
         var _loc_1:* = 0;
         var _loc_2:* = this.maxGenerator;
         while(_loc_1 < _loc_2)
         {
            _loc_3 = _loc_1;
            this.ShieldGensDisplayed[_loc_1] = new Object();
            this.ShieldGensDisplayed[_loc_1].genNumber = _loc_3;
            this.ShieldGensDisplayed[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
            this.ShieldGensDisplayed[_loc_1].DisplayBox.itemname.text = this.shieldgenerators[_loc_3][4];
            this.ShieldGensDisplayed[_loc_1].DisplayBox.itemcost.text = "Cost:" + this.FormatNumber(Number(this.shieldgenerators[_loc_3][5]) - Number(this.currentvalueofgenerator));
            this.ShieldGensDisplayed[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_BuyShieldGen);
            this.ShieldGensDisplayed[_loc_1].DisplayBox.name = _loc_3;
            this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text = "Recharge: " + this.shieldgenerators[_loc_3][1] + " \r";
            this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text += "Energy Drain/second: \r";
            this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text += "Unit: " + this.shieldgenerators[_loc_3][2] + " \r";
            this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text += "Full Charge: " + this.shieldgenerators[_loc_3][3] + " \r";
            this.DisplayScreen.func_addItem(this.ShieldGensDisplayed[_loc_1].DisplayBox);
            _loc_1 += 1;
         }
      }
      
      public function initGunSelection() : *
      {
         this.shipstopgun = this.MaxGunLevel;
         this.DisplayScreen.func_clearAllItems();
         this.func_InitGunsDisplay();
         this.DisplayScreen.visible = true;
      }
      
      public function func_acceptGunPurchase(event:MouseEvent) : void
      {
         this.func_playRegularClick();
         this.endGunSelection();
      }
      
      public function Accept_Current_Purchase(event:MouseEvent) : void
      {
         var _loc_2:* = undefined;
         this.buyweapon.visible = false;
         this.playershipstatus[3][1] -= this.UpgradeCost;
         if(this.CurrentActionDoing == "g")
         {
            _loc_2 = "SQUADB`UPG`" + this.playershipstatus[5][10] + "`GUN`" + this.CurrentSpotPurchased + "`" + this.gunBeingBought + "~";
            this.mysocket.send(_loc_2);
         }
         if(this.CurrentActionDoing == "s")
         {
            _loc_2 = "SQUADB`UPG`" + this.playershipstatus[5][10] + "`SH`" + this.ShieldGenTypeBeingBought + "~";
            this.mysocket.send(_loc_2);
         }
         if(this.CurrentActionDoing == "h")
         {
            _loc_2 = "SQUADB`UPG`" + this.playershipstatus[5][10] + "`STR`" + "~";
            this.mysocket.send(_loc_2);
         }
         if(this.CurrentActionDoing == "l")
         {
            _loc_2 = "SQUADB`UPG`" + this.playershipstatus[5][10] + "`LVL``" + "~";
            this.mysocket.send(_loc_2);
         }
      }
      
      public function endGunSelection() : *
      {
         this.DisplayScreen.visible = false;
         this.buyweapon.visible = false;
      }
      
      public function func_BuyNewWeapon(event:MouseEvent) : void
      {
         this.gunBeingBought = Number(event.target.parent.name);
         if(this.guntype[this.gunBeingBought][5] <= this.playershipstatus[3][1])
         {
            this.buyweapon.question.text = "Buy: " + this.guntype[this.gunBeingBought][6] + " \r" + "For: " + this.FormatNumber(this.guntype[this.gunBeingBought][5]);
            this.buyweapon.visible = true;
            this.UpgradeCost = this.guntype[this.gunBeingBought][5];
            this.func_playRegularClick();
         }
      }
      
      public function func_playRegularClick() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.regularClick.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      public function func_cancelGunPurchase(event:MouseEvent) : void
      {
         this.func_playRegularClick();
         this.buyweapon.visible = false;
      }
      
      public function func_ListItemClick(event:MouseEvent) : void
      {
         this.endGunSelection();
         var _loc_2:* = String(event.target.parent.name);
         var _loc_3:* = _loc_2.substr(0,1);
         var _loc_4:* = Number(_loc_2.substr(1,1));
         this.CurrentSpotPurchased = _loc_4;
         trace(_loc_2);
         this.CurrentActionDoing = _loc_3;
         if(_loc_3 == "g")
         {
            this.initGunSelection();
         }
         else if(_loc_3 == "s")
         {
            this.func_InitShieldGensDisplay();
         }
         else if(_loc_3 == "h")
         {
            this.UpgradeCost = Math.ceil((Number(this.MaxbaseStructure) - Number(this.baseStructure)) / 5);
            if(this.UpgradeCost <= this.playershipstatus[3][1])
            {
               this.buyweapon.question.text = "Fully Reapair Base \r" + "For: " + this.FormatNumber(this.UpgradeCost);
               this.buyweapon.visible = true;
               this.func_playRegularClick();
            }
         }
         else if(_loc_3 == "l")
         {
            if(this.BaseLevel < this.MaxBaseLevel)
            {
               this.UpgradeCost = this.squadbaseinfo[this.BaseLevel + 1].upgradeToCost;
               if(this.UpgradeCost <= this.playershipstatus[3][1])
               {
                  this.gunBeingBought = Number(event.target.parent.name);
                  this.buyweapon.question.text = "Buy: Base Level " + (this.BaseLevel + 2) + " \r" + "For: " + this.FormatNumber(this.UpgradeCost);
                  this.buyweapon.visible = true;
                  this.func_playRegularClick();
               }
            }
         }
      }
      
      public function func_DisplayBaseStats() : *
      {
         var _loc_1:* = "";
         if(this.BaseTurrets.length < 1)
         {
            this.currBaseInfo.text = "Loading...";
            this.upgBaseInfo.text = "Loading...";
         }
         else
         {
            _loc_1 = "Current Level: " + (this.BaseLevel + 1);
            _loc_1 += "\rTop Generator: " + this.shieldgenerators[this.maxGenerator][4];
            _loc_1 += "\rTop Weapon: " + this.guntype[this.MaxGunLevel][6];
            _loc_1 += "\rStructure: " + this.FormatNumber(Math.floor(this.baseStructure) + " / " + this.MaxbaseStructure);
            _loc_1 += "\rIncome: " + this.FormatNumber(this.squadbaseinfo[this.BaseLevel].HourlyIncome) + " /hr ";
            this.currBaseInfo.text = _loc_1;
            _loc_1 = "Not Programmed Yet";
            if(this.BaseLevel >= this.MaxBaseLevel)
            {
               _loc_1 = "No More Levels";
            }
            else
            {
               _loc_1 = "Next Level: " + (this.BaseLevel + 2) + " (" + this.FormatNumber(this.squadbaseinfo[this.BaseLevel + 1].upgradeToCost) + ")";
               _loc_1 += "\rTop Generator: " + this.shieldgenerators[this.squadbaseinfo[this.BaseLevel + 1].MaxShieldGen][4];
               _loc_1 += "\rTop Weapon: " + this.guntype[this.squadbaseinfo[this.BaseLevel + 1].MaxGun][6];
               _loc_1 += "\rMax Structure: " + this.FormatNumber(this.squadbaseinfo[this.BaseLevel + 1].MaxStructure);
               _loc_1 += "\rIncome: " + this.FormatNumber(this.squadbaseinfo[this.BaseLevel + 1].HourlyIncome) + " /hr ";
            }
            this.upgBaseInfo.text = _loc_1;
            this.MaxGunLevel = this.squadbaseinfo[this.BaseLevel].MaxGun;
            this.maxGenerator = this.squadbaseinfo[this.BaseLevel].MaxShieldGen;
            this.MaxbaseStructure = this.squadbaseinfo[this.BaseLevel].MaxStructure;
         }
      }
      
      public function parseInBaseStats(param1:*) : *
      {
         var _loc_3:* = undefined;
         var _loc_4:* = undefined;
         var _loc_5:* = undefined;
         var _loc_6:* = undefined;
         trace("Inc SQB Data:" + param1);
         var _loc_2:* = param1;
         if(1 == 1)
         {
            this.BaseLevel = Number(_loc_2[4]);
            this.baseStructure = Number(_loc_2[5]);
            this.BaseGenerator = Number(_loc_2[6]);
            _loc_3 = _loc_2[7];
            this.BaseTurrets = new Array();
            _loc_4 = _loc_3.split("b");
            _loc_5 = 0;
            while(_loc_5 < _loc_4.length)
            {
               _loc_6 = _loc_4[_loc_5].split("a");
               this.BaseTurrets[_loc_5] = Number(_loc_6[0]);
               _loc_5 += 1;
            }
            trace("TurretsBase:" + this.BaseTurrets.length);
            this.MaxGunLevel = this.squadbaseinfo[this.BaseLevel].MaxGun;
            this.maxGenerator = this.squadbaseinfo[this.BaseLevel].MaxShieldGen;
            this.MaxbaseStructure = this.squadbaseinfo[this.BaseLevel].MaxStructure;
         }
         this.func_DisplayBaseStats();
         this.func_setButtons();
      }
      
      public function func_BuyShieldGen(event:MouseEvent) : void
      {
         this.ShieldGenTypeBeingBought = Number(event.target.parent.name);
         var _loc_2:* = Number(this.shieldgenerators[this.ShieldGenTypeBeingBought][5]) - Number(this.currentvalueofgenerator);
         if(_loc_2 <= this.playershipstatus[3][1])
         {
            this.buyweapon.question.text = "Buy: " + this.shieldgenerators[this.ShieldGenTypeBeingBought][4] + " \r";
            this.buyweapon.question.text += "For: " + this.FormatNumber(_loc_2);
            this.buyweapon.visible = true;
            this.func_playRegularClick();
         }
         else
         {
            this.buyweapon.visible = false;
         }
      }
   }
}
