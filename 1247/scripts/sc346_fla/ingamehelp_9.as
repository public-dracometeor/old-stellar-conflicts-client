package sc346_fla
{
   import flash.display.*;
   import flash.events.*;
   
   public dynamic class ingamehelp_9 extends MovieClip
   {
       
      
      public var butt3:MovieClip;
      
      public var butt4:MovieClip;
      
      public var butt6:MovieClip;
      
      public var butt7:MovieClip;
      
      public var butt8:MovieClip;
      
      public var butt9:MovieClip;
      
      public var butt5:MovieClip;
      
      public var minimizeButton:SimpleButton;
      
      public var butt1:MovieClip;
      
      public var butt2:MovieClip;
      
      public function ingamehelp_9()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2,3,this.frame4,4,this.frame5,5,this.frame6,6,this.frame7,7,this.frame8,8,this.frame9,9,this.frame10,10,this.frame11,11,this.frame12);
      }
      
      internal function frame12() : *
      {
         stop();
      }
      
      internal function frame10() : *
      {
         stop();
      }
      
      internal function frame11() : *
      {
         stop();
      }
      
      public function func_GotToPressedHelpScreen(event:MouseEvent) : void
      {
         var _loc_2:* = event.target.parent.ActionType;
         trace("Help Screen:" + _loc_2);
         this.HelpScreenSelected(_loc_2);
      }
      
      internal function frame1() : *
      {
         this.butt1.ActionLabel.text = "In Game Help";
         this.butt1.ActionType = "ingame";
         this.butt1.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GotToPressedHelpScreen);
         this.butt2.ActionLabel.text = "Trade Goods";
         this.butt2.ActionType = "tradegoods1";
         this.butt2.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GotToPressedHelpScreen);
         this.butt3.ActionLabel.text = "Ship Hardware";
         this.butt3.ActionType = "hardware1";
         this.butt3.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GotToPressedHelpScreen);
         this.butt4.ActionLabel.text = "Buying Ship";
         this.butt4.ActionType = "ship1";
         this.butt4.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GotToPressedHelpScreen);
         this.butt5.ActionLabel.text = "Docked";
         this.butt5.ActionType = "starbase1";
         this.butt5.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GotToPressedHelpScreen);
         this.butt6.ActionLabel.text = "Missions";
         this.butt6.ActionType = "mission1";
         this.butt6.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GotToPressedHelpScreen);
         this.butt7.ActionLabel.text = "Hangar";
         this.butt7.ActionType = "extraships1";
         this.butt7.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GotToPressedHelpScreen);
         this.butt8.ActionLabel.text = "Preferences";
         this.butt8.ActionType = "preferences1";
         this.butt8.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GotToPressedHelpScreen);
         this.butt9.ActionLabel.text = "Mod Commands";
         this.butt9.ActionType = "adminCommands";
         this.butt9.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GotToPressedHelpScreen);
         this.HelpScreenSelected("overview");
      }
      
      internal function frame2() : *
      {
         gotoAndPlay(1);
      }
      
      internal function frame4() : *
      {
         stop();
      }
      
      public function HelpScreenSelected(param1:*) : *
      {
         if(param1 == "ingame")
         {
            gotoAndStop("ingame");
         }
         if(param1 == "tradegoods1")
         {
            gotoAndStop("tradegoods1");
         }
         if(param1 == "hardware1")
         {
            gotoAndStop("hardware1");
         }
         if(param1 == "ship1")
         {
            gotoAndStop("ship1");
         }
         if(param1 == "starbase1")
         {
            gotoAndStop("starbase1");
         }
         if(param1 == "mission1")
         {
            gotoAndStop("mission1");
         }
         if(param1 == "extraships1")
         {
            gotoAndStop("extraships1");
         }
         if(param1 == "preferences1")
         {
            gotoAndStop("preferences1");
         }
         if(param1 == "overview")
         {
            gotoAndStop("overview");
         }
         if(param1 == "adminCommands")
         {
            gotoAndStop("adminCommands");
         }
      }
      
      internal function frame6() : *
      {
         stop();
      }
      
      internal function frame7() : *
      {
         stop();
      }
      
      internal function frame8() : *
      {
         stop();
      }
      
      internal function frame9() : *
      {
         stop();
      }
      
      internal function frame5() : *
      {
         stop();
      }
   }
}
