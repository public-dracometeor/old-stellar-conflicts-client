package sc346_fla
{
   import flash.display.*;
   
   public dynamic class loginscreen_50 extends MovieClip
   {
       
      
      public var gamesetting:Object;
      
      public var counter:Object;
      
      public var playershipstatus:Object;
      
      public var PlayerInConquestZone:Object;
      
      public var gameSounds:Object;
      
      public var mov_login:MovieClip;
      
      public var mysocket:Object;
      
      public function loginscreen_50()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2,2,this.frame3,15,this.frame16,64,this.frame65);
      }
      
      public function func_playOpeningTitleSound() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.openingTitleSound.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      internal function frame3() : *
      {
         var _loc_1:* = this;
         var _loc_2:* = this.counter + 1;
         _loc_1.counter = _loc_2;
         gotoAndPlay(2);
      }
      
      internal function frame65() : *
      {
         this.mov_login.mysocket = this.mysocket;
         this.mov_login.playershipstatus = this.playershipstatus;
         this.mov_login.PlayerInConquestZone = this.PlayerInConquestZone;
         this.mov_login.gameSounds = this.gameSounds;
         this.mov_login.gamesetting = this.gamesetting;
         stop();
      }
      
      internal function frame1() : *
      {
         this.counter = 101;
         this.scaleX = 0.6;
         this.scaleY = 0.6;
      }
      
      internal function frame2() : *
      {
         if(this.counter > 100)
         {
            this.mysocket.send("GAMEINFO`~");
            this.counter = 0;
         }
      }
      
      internal function frame16() : *
      {
         this.func_playOpeningTitleSound();
      }
      
      public function func_playRegularClick() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.regularClick.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
   }
}
