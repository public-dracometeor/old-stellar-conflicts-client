package sc346_fla
{
   import flash.display.*;
   import flash.text.*;
   
   public dynamic class specialitems_letter_95 extends MovieClip
   {
       
      
      public var setting:Object;
      
      public var button:TextField;
      
      public function specialitems_letter_95()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2,2,this.frame3);
      }
      
      internal function frame1() : *
      {
         this.setting = "OFF";
         stop();
      }
      
      internal function frame2() : *
      {
         this.setting = "ON";
         stop();
      }
      
      internal function frame3() : *
      {
         this.setting = "RELOAD";
         stop();
      }
   }
}
