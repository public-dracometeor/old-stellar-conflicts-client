package sc346_fla
{
   import flash.display.*;
   import flash.events.*;
   
   public dynamic class SelectionWindow_117 extends MovieClip
   {
       
      
      public var sliderscale:Object;
      
      public var initialSlideBarY:Object;
      
      public var MaxBarY:Object;
      
      public var leftSideSlide:Object;
      
      public var slider:MovieClip;
      
      public var risghtSideSLide:Object;
      
      public var SlidingBar:Object;
      
      public var screen:MovieClip;
      
      public var mouseXoffset:Object;
      
      public var MinBarX:Object;
      
      public var screenWidth:Object;
      
      public var originalSLiderWidth:Object;
      
      public var SliderToScreenDIfference:Object;
      
      public function SelectionWindow_117()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function StartDrag(event:MouseEvent) : void
      {
         this.SlidingBar = true;
         this.mouseXoffset = mouseX - Number(this.leftSideSlide) - (Number(this.slider.sliderBar.x) - Number(this.leftSideSlide));
      }
      
      public function func_addItem(param1:*) : *
      {
         var _loc_2:* = this.screen.width;
         this.screen.addChild(param1);
         param1.x = _loc_2;
         this.func_calcSlideBarScale();
         this.screen.x = 0;
      }
      
      internal function frame1() : *
      {
         this.screenWidth = 545;
         this.leftSideSlide = 15;
         this.risghtSideSLide = 550 - 15;
         this.slider.sliderBar.scaleX = 1;
         this.sliderscale = 1;
         this.SlidingBar = true;
         this.initialSlideBarY = this.slider.sliderBar.y;
         this.originalSLiderWidth = this.slider.sliderBar.width;
         this.mouseXoffset = 0;
         this.SliderToScreenDIfference = Number(this.originalSLiderWidth) / Number(this.screenWidth);
         this.MinBarX = this.leftSideSlide;
         this.func_calcSlideBarScale();
         this.slider.sliderBar.addEventListener(MouseEvent.MOUSE_DOWN,this.StartDrag);
         Object(root).addEventListener(MouseEvent.MOUSE_UP,this.StopSliderDrag);
         addEventListener(Event.ENTER_FRAME,this.func_MoveSLider);
      }
      
      public function func_clearAllItems() : *
      {
         while(this.screen.numChildren > 0)
         {
            this.screen.removeChildAt(0);
         }
         this.func_calcSlideBarScale();
      }
      
      public function func_calcSlideBarScale() : *
      {
         if(this.screen.width > 0)
         {
            this.sliderscale = Number(this.screenWidth) / this.screen.width;
         }
         else
         {
            this.sliderscale = 1;
            this.slider.visible = false;
         }
         if(this.sliderscale > 1)
         {
            this.sliderscale = 1;
            this.slider.visible = false;
         }
         if(this.sliderscale < 1)
         {
            this.slider.sliderBar.scaleX = this.sliderscale;
            this.slider.sliderBar.x = this.leftSideSlide;
            this.MaxBarY = Number(this.risghtSideSLide) - Number(this.originalSLiderWidth) * Number(this.sliderscale);
            this.slider.visible = true;
         }
         else
         {
            this.slider.alpha = 50;
         }
      }
      
      public function func_MoveSLider(event:Event) : *
      {
         if(this.SlidingBar)
         {
            this.slider.sliderBar.y = this.initialSlideBarY;
            this.slider.sliderBar.x = mouseX - Number(this.mouseXoffset);
            if(this.slider.sliderBar.x < this.leftSideSlide)
            {
               this.slider.sliderBar.x = this.leftSideSlide;
            }
            else if(this.slider.sliderBar.x > this.MaxBarY)
            {
               this.slider.sliderBar.x = this.MaxBarY;
            }
            this.screen.x = -(Number(this.slider.sliderBar.x) - Number(this.leftSideSlide)) / (Number(this.sliderscale) * Number(this.SliderToScreenDIfference)) + 1;
         }
      }
      
      public function StopSliderDrag(event:MouseEvent) : void
      {
         this.SlidingBar = false;
      }
   }
}
