package sc346_fla
{
   import flash.display.*;
   import flash.events.*;
   import flash.text.*;
   import flash.utils.*;
   
   public dynamic class login_m_login_connect_65 extends MovieClip
   {
       
      
      public var Racedesc:TextField;
      
      public var lastrefresh:Object;
      
      public var zone200:MovieClip;
      
      public var ZoneSelected:Object;
      
      public var zone400:MovieClip;
      
      public var zone600:MovieClip;
      
      public var PlayerInConquestZone:Object;
      
      public var TutorialPlayerZoneInfo:SimpleButton;
      
      public var mysocket:Object;
      
      public var changeRace:MovieClip;
      
      public var zone175:MovieClip;
      
      public var gamesetting:Object;
      
      public var currentstatus:TextField;
      
      public var ZoneInformation:TextField;
      
      public var resfreshingtext:Object;
      
      public var resfreshinginfo:TextField;
      
      public var zone100:MovieClip;
      
      public var newPlayerZoneInfo:SimpleButton;
      
      public var playershipstatus:Object;
      
      public var currentrace:Object;
      
      public var refreshrate:Object;
      
      public var ConquestZoneInfo:SimpleButton;
      
      public var totalRaces:Object;
      
      public var zone150:MovieClip;
      
      public var isplayeraguest:Object;
      
      public var gameSounds:Object;
      
      public var DeathMatchZoneInfo:SimpleButton;
      
      public var StandardZoneInfo:SimpleButton;
      
      public var acceptRace:MovieClip;
      
      public function login_m_login_connect_65()
      {
         super();
         addFrameScript(0,this.frame1,9,this.frame10,22,this.frame23,23,this.frame24,25,this.frame26,29,this.frame30);
      }
      
      public function func_playlogin_but_press() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.login_but_pressSound.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      public function manageMouseOver_STANDARD(event:MouseEvent) : void
      {
         this.ZoneInformation.text = "Free play zone with regular AI Ships, Squad Bases and other events.";
      }
      
      internal function frame23() : *
      {
      }
      
      public function func_incominglisting(param1:*) : *
      {
         if(param1[1] == 150)
         {
            this.zone150.players.text = param1[3] + "/" + param1[4];
            this.zone150.zonename.text = "Zone 150, < " + Math.ceil(int(param1[5])) + " score";
            this.zone150.enterzonebutton.visible = true;
            if(this.playershipstatus[5][9] >= Number(param1[5]))
            {
               if(this.playershipstatus[5][12] == "")
               {
                  this.zone150.enterzonebutton.visible = false;
               }
            }
         }
         if(param1[1] == 175)
         {
            this.zone175.players.text = param1[3] + "/" + param1[4];
            this.zone175.zonename.text = "Zone 175, <" + Math.ceil(int(param1[5])) + " score";
            this.zone175.enterzonebutton.visible = true;
            if(this.playershipstatus[5][9] >= Number(param1[5]))
            {
               if(this.playershipstatus[5][12] == "")
               {
                  this.zone175.enterzonebutton.visible = false;
               }
            }
         }
         if(param1[1] == 200)
         {
            this.zone200.players.text = param1[3] + "/" + param1[4];
            this.zone200.enterzonebutton.visible = true;
         }
         if(param1[1] == 100)
         {
            this.zone100.players.text = param1[3] + "/" + param1[4];
            this.zone100.enterzonebutton.visible = true;
         }
         if(param1[1] == 400)
         {
            this.zone400.players.text = param1[3] + "/" + param1[4];
            this.zone400.enterzonebutton.visible = true;
         }
         if(param1[1] == 600)
         {
            this.zone600.players.text = param1[3] + "/" + param1[4];
            this.zone600.enterzonebutton.visible = true;
         }
         this.resfreshinginfo.text = "";
      }
      
      public function myFunction(event:Event) : *
      {
         if(getTimer() > this.lastrefresh)
         {
            this.lastrefresh = getTimer() + this.refreshrate;
            this.mysocket.send("LISTING~");
            this.resfreshinginfo.text = this.resfreshingtext;
         }
      }
      
      internal function frame30() : *
      {
         removeEventListener(Event.ENTER_FRAME,this.myFunction);
         stop();
      }
      
      public function func_playRegularClick() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.regularClick.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      public function zoneEntranceSelected(event:MouseEvent) : void
      {
         this.func_playRegularClick();
         this.ZoneSelected = String(event.target.parent.name).substr(4);
         trace(this.ZoneSelected);
         this.playershipstatus[5][2] = Math.round(Math.random() * 2);
         if(this.ZoneSelected.substr(0,1) == "4")
         {
            this.playershipstatus[5][2] = "-1";
         }
         if(this.ZoneSelected.substr(0,1) == "6")
         {
            gotoAndStop("raceSelection");
         }
         else
         {
            this.mysocket.send("ZONE`" + this.ZoneSelected + "~");
         }
      }
      
      internal function frame10() : *
      {
      }
      
      public function AcceptRaceSelected(event:MouseEvent) : void
      {
         trace("accepted click");
         this.playershipstatus[5][2] = this.currentrace;
         this.PlayerInConquestZone = true;
         this.mysocket.send("ZONE`" + this.ZoneSelected + "`~");
      }
      
      public function manageMouseOver_CQ(event:MouseEvent) : void
      {
         this.ZoneInformation.text = "Conquest Zone where factions battle for control of the map. Bases are capturable.";
      }
      
      internal function frame1() : *
      {
      }
      
      public function func_playConnectingSound() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.connectingSound.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      public function func_playlogin_but_pops() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.login_but_popsSound.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      public function manageMouseOver_TUT(event:MouseEvent) : void
      {
         this.ZoneInformation.text = "Tutorial of the game";
      }
      
      internal function frame26() : *
      {
         removeEventListener(Event.ENTER_FRAME,this.myFunction);
         this.currentrace = -1;
         this.totalRaces = 2;
         this.changeRaceSelection();
         this.changeRace.addEventListener(MouseEvent.MOUSE_DOWN,this.ChangeRaceSelected);
         this.changeRace.buttonText.text = "Change Race";
         this.acceptRace.addEventListener(MouseEvent.MOUSE_DOWN,this.AcceptRaceSelected);
         this.acceptRace.buttonText.text = "Accept Race";
         stop();
      }
      
      public function manageMouseOver_DM(event:MouseEvent) : void
      {
         this.ZoneInformation.text = "Death Match Zone where two teams battle head to head trying to kill their opponents base.";
      }
      
      public function manageMouseOut(event:MouseEvent) : void
      {
         this.ZoneInformation.text = "";
      }
      
      public function changeRaceSelection() : *
      {
         var _loc_2:* = this;
         var _loc_3:* = this.currentrace + 1;
         _loc_2.currentrace = _loc_3;
         var _loc_1:* = "";
         if(this.currentrace == this.totalRaces)
         {
            this.currentrace = 0;
         }
         if(this.currentrace == 0)
         {
            _loc_1 = "The Human Race. \r\r ";
            _loc_1 += "Humans are a more intelligent race, crafty, and diverse in their skills. ";
            _loc_1 += "In combat, they prefer to be the more agile hunter, out maneuverung thier opponent.\r\r";
            _loc_1 += "In general, Humans have ships that are more nimble, agile, and harder to hit.";
         }
         else if(this.currentrace == 1)
         {
            _loc_1 = "The Tuskann Race. \r\r";
            _loc_1 += "A race that is more brawn than brains. This race prefers to have power and strength, rather then agility and skill. ";
            _loc_1 += "With more powerful weapons, and tougher ships, they are a race designed for dominance by power.\r\r";
            _loc_1 += "Tuskan ships can carry tougher guns, and can take more damage than other race\'s ships.";
         }
         else
         {
            _loc_1 = "You have found the secret broken race!";
         }
         this.Racedesc.text = _loc_1;
      }
      
      public function manageMouseOver_NEWP(event:MouseEvent) : void
      {
         this.ZoneInformation.text = "Free play zone for new players, weaker AI ships, meant for learning the game.";
      }
      
      public function ChangeRaceSelected(event:MouseEvent) : void
      {
         trace("Change click");
         this.changeRaceSelection();
      }
      
      internal function frame24() : *
      {
         this.isplayeraguest = false;
         this.ZoneInformation.text = "";
         this.zone100.players.text = "LOAD";
         this.zone100.zonename.text = "Zone 100";
         this.zone100.enterzonebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.zoneEntranceSelected);
         this.zone100.enterzonebutton.visible = false;
         this.zone150.players.text = "LOAD";
         this.zone150.zonename.text = "Zone 150";
         this.zone150.enterzonebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.zoneEntranceSelected);
         this.zone150.enterzonebutton.visible = false;
         this.zone175.players.text = "LOAD";
         this.zone175.zonename.text = "Zone 175";
         this.zone175.enterzonebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.zoneEntranceSelected);
         this.zone175.enterzonebutton.visible = false;
         this.zone200.players.text = "LOAD";
         this.zone200.zonename.text = "Zone 200";
         this.zone200.enterzonebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.zoneEntranceSelected);
         this.zone200.enterzonebutton.visible = false;
         this.zone400.players.text = "LOAD";
         this.zone400.zonename.text = "Zone 400";
         this.zone400.enterzonebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.zoneEntranceSelected);
         this.zone400.enterzonebutton.visible = false;
         this.zone600.players.text = "LOAD";
         this.zone600.zonename.text = "Zone 600";
         this.zone600.enterzonebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.zoneEntranceSelected);
         this.zone600.enterzonebutton.visible = false;
         if(!this.isplayeraguest)
         {
         }
         this.mysocket.send("LISTING~");
         this.resfreshingtext = "(REFRESHING)";
         this.resfreshinginfo.text = this.resfreshingtext;
         this.refreshrate = 15000;
         this.lastrefresh = getTimer() + this.refreshrate;
         addEventListener(Event.ENTER_FRAME,this.myFunction);
         this.TutorialPlayerZoneInfo.addEventListener(MouseEvent.ROLL_OUT,this.manageMouseOut,false,0,true);
         this.newPlayerZoneInfo.addEventListener(MouseEvent.ROLL_OUT,this.manageMouseOut,false,0,true);
         this.StandardZoneInfo.addEventListener(MouseEvent.ROLL_OUT,this.manageMouseOut,false,0,true);
         this.DeathMatchZoneInfo.addEventListener(MouseEvent.ROLL_OUT,this.manageMouseOut,false,0,true);
         this.ConquestZoneInfo.addEventListener(MouseEvent.ROLL_OUT,this.manageMouseOut,false,0,true);
         this.TutorialPlayerZoneInfo.addEventListener(MouseEvent.ROLL_OVER,this.manageMouseOver_TUT,false,0,true);
         this.newPlayerZoneInfo.addEventListener(MouseEvent.ROLL_OVER,this.manageMouseOver_NEWP,false,0,true);
         this.StandardZoneInfo.addEventListener(MouseEvent.ROLL_OVER,this.manageMouseOver_STANDARD,false,0,true);
         this.DeathMatchZoneInfo.addEventListener(MouseEvent.ROLL_OVER,this.manageMouseOver_DM,false,0,true);
         this.ConquestZoneInfo.addEventListener(MouseEvent.ROLL_OVER,this.manageMouseOver_CQ,false,0,true);
         stop();
         stop();
      }
   }
}
