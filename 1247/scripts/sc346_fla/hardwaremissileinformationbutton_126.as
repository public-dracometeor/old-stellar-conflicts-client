package sc346_fla
{
   import flash.display.*;
   import flash.events.*;
   import flash.text.*;
   
   public dynamic class hardwaremissileinformationbutton_126 extends MovieClip
   {
       
      
      public var MissilePrice:Object;
      
      public var maxpurchase:TextField;
      
      public var banknumberdisp:TextField;
      
      public var quantity:TextField;
      
      public var CurrentMissiles:Object;
      
      public var costdisp:TextField;
      
      public var nobutton:SimpleButton;
      
      public var missilecostdisp:TextField;
      
      public var maxMissiles:Object;
      
      public var yesbutton:SimpleButton;
      
      public function hardwaremissileinformationbutton_126()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function func_OnGameFrame(event:Event) : *
      {
         var _loc_2:* = undefined;
         if(this.visible)
         {
            _loc_2 = Number(this.quantity.text);
            if(!isNaN(_loc_2))
            {
               _loc_2 = Math.round(_loc_2);
            }
            if(_loc_2 < 0)
            {
               this.quantity.text = "0";
            }
            else if(isNaN(_loc_2))
            {
               this.quantity.text = "0";
            }
            else if(_loc_2 > this.maxMissiles)
            {
               this.quantity.text = String(this.maxMissiles);
            }
            this.costdisp.text = "Cost: " + Number(this.quantity.text) * Number(this.MissilePrice);
         }
      }
      
      internal function frame1() : *
      {
         this.visible = false;
         this.maxMissiles = 0;
         this.CurrentMissiles = 0;
         this.MissilePrice = 0;
         this.quantity.text = "0";
         addEventListener(Event.ENTER_FRAME,this.func_OnGameFrame);
         stop();
      }
   }
}
