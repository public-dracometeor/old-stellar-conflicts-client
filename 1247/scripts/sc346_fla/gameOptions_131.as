package sc346_fla
{
   import flash.display.*;
   import flash.events.*;
   import flash.text.*;
   
   public dynamic class gameOptions_131 extends MovieClip
   {
       
      
      public var FieldPressed:Object;
      
      public var bck_off:MovieClip;
      
      public var latency_delay:TextField;
      
      public var sectorinformation:Object;
      
      public var key_accel:TextField;
      
      public var gameTips_off:MovieClip;
      
      public var music_vol_but:MovieClip;
      
      public var BaseLocationHeader:TextField;
      
      public var newGameSettings:Object;
      
      public var sound_on:MovieClip;
      
      public var key_dock:TextField;
      
      public var BaseMessageBox:TextField;
      
      public var key_guns:TextField;
      
      public var combat_vol:TextField;
      
      public var key_help:TextField;
      
      public var squadoptions:MovieClip;
      
      public var key_deaccel:TextField;
      
      public var nameLabel:TextField;
      
      public var mysocket:Object;
      
      public var menu_vol_but:MovieClip;
      
      public var music_on:MovieClip;
      
      public var squadBaseButton:MovieClip;
      
      public var sound_off:MovieClip;
      
      public var chat_Small:MovieClip;
      
      public var squadAction_button:MovieClip;
      
      public var key_right:TextField;
      
      public var messageBox:TextField;
      
      public var loc:Object;
      
      public var sectormapitems:Object;
      
      public var music_off:MovieClip;
      
      public var tot_stars_but:MovieClip;
      
      public var squadbase_info:TextField;
      
      public var passLabel:TextField;
      
      public var chat_med:MovieClip;
      
      public var squadsNameDisp:TextField;
      
      public var isplayeraguest:Object;
      
      public var squadAction_buttontwo:MovieClip;
      
      public var gameoptions:MovieClip;
      
      public var TheSquadPass:Object;
      
      public var exit_button:MovieClip;
      
      public var baseXgrid:TextField;
      
      public var changeSquadMessage:MovieClip;
      
      public var MemberList:TextField;
      
      public var key_map:TextField;
      
      public var accept_changes:MovieClip;
      
      public var SquadMessage:TextField;
      
      public var scroll_bck_off:MovieClip;
      
      public var chat_large:MovieClip;
      
      public var initialGameSettings:Object;
      
      public var BaseLocationDetails:TextField;
      
      public var cancel_key_changes:MovieClip;
      
      public var squadmembers:Object;
      
      public var key_shift:TextField;
      
      public var latency_delay_set:MovieClip;
      
      public var scroll_bck_on:MovieClip;
      
      public var squadBaseTwoButton:MovieClip;
      
      public var combat_sound_off:MovieClip;
      
      public var total_stars:TextField;
      
      public var music_vol:TextField;
      
      public var membersTitle:TextField;
      
      public var TheSquadName:Object;
      
      public var playershipstatus:Object;
      
      public var combat_vol_but:MovieClip;
      
      public var reset_changes:MovieClip;
      
      public var SquadName:Object;
      
      public var replacewithchar:Object;
      
      public var gameSounds:Object;
      
      public var key_targeter:TextField;
      
      public var key_left:TextField;
      
      public var squadsPassDisp:TextField;
      
      public var baseYgrid:TextField;
      
      public var menu_vol:TextField;
      
      public var bck_on:MovieClip;
      
      public var SquadMessageInfoBox:TextField;
      
      public var combat_sound_on:MovieClip;
      
      public var gameTips_on:MovieClip;
      
      public var key_missiles:TextField;
      
      public function gameOptions_131()
      {
         super();
         addFrameScript(0,this.frame1,9,this.frame10,40,this.frame41);
      }
      
      public function SquadFunction(event:MouseEvent) : void
      {
         this.func_playRegularClick();
         var _loc_2:* = String(event.target.text);
         if(_loc_2 == "Join A Squad")
         {
            this.squadAction_button.actionlabel.text = "Submit Squad Join";
            this.squadAction_buttontwo.actionlabel.text = "Cancel Squad Join";
            this.nameLabel.text = "Enter Squad Name:";
            this.squadsNameDisp.text = "";
            this.passLabel.text = "Enter Squad Pass:";
            this.squadsPassDisp.text = "";
         }
         if(_loc_2 == "Cancel Squad Join")
         {
            this.mainSquadDisplay();
         }
         if(_loc_2 == "Submit Squad Join")
         {
            this.func_TrySquadJoin(this.squadsNameDisp.text,this.squadsPassDisp.text);
         }
         if(_loc_2 == "Create A Squad")
         {
            this.squadAction_button.visible = true;
            this.squadAction_button.actionlabel.text = "Submit Squad Create";
            this.squadAction_buttontwo.visible = true;
            this.squadAction_buttontwo.actionlabel.text = "Cancel Squad Create";
            this.nameLabel.text = "Enter Squad Name:";
            this.squadsNameDisp.text = "";
            this.passLabel.text = "Enter Squad Pass:";
            this.squadsPassDisp.text = "";
         }
         if(_loc_2 == "Cancel Squad Create")
         {
            this.mainSquadDisplay();
         }
         if(_loc_2 == "Submit Squad Create")
         {
            this.func_TrySquadCreation(this.squadsNameDisp.text,this.squadsPassDisp.text);
         }
         if(_loc_2 == "Leave Squad")
         {
            this.squadAction_button.visible = true;
            this.squadAction_button.actionlabel.text = "Confirm Leave";
            this.squadAction_buttontwo.visible = true;
            this.squadAction_buttontwo.actionlabel.text = "Cancel Leave";
         }
         if(_loc_2 == "Cancel Leave")
         {
            this.mainSquadDisplay();
         }
         if(_loc_2 == "Confirm Leave")
         {
            this.messageBox.text = "Leaving Squad";
            this.squadAction_button.visible = false;
            this.squadAction_buttontwo.visible = false;
            this.mysocket.send("SQUAD`LEAVESQ`~");
         }
         if(_loc_2 == "Dissolve Squad")
         {
            this.squadAction_button.visible = true;
            this.squadAction_button.actionlabel.text = "Confirm Dissolve";
            this.squadAction_buttontwo.visible = true;
            this.squadAction_buttontwo.actionlabel.text = "Cancel Dissolve";
         }
         if(_loc_2 == "Cancel Dissolve")
         {
            this.mainSquadDisplay();
         }
         if(_loc_2 == "Confirm Dissolve")
         {
            this.messageBox.text = "Dissolving Squad";
            this.squadAction_button.visible = false;
            this.squadAction_buttontwo.visible = false;
            this.mysocket.send("SQUAD`DESTROYSQ`~");
         }
         if(_loc_2 == "Create A Base")
         {
            this.squadBaseButton.visible = true;
            this.squadBaseButton.actionlabel.text = "Create Base";
            this.squadBaseTwoButton.visible = true;
            this.squadBaseTwoButton.actionlabel.text = "Cancel Base";
            this.BaseLocationDetails.visible = true;
            this.baseXgrid.visible = true;
            this.baseXgrid.text = "";
            this.baseYgrid.visible = true;
            this.baseYgrid.text = "";
         }
         if(_loc_2 == "Cancel Base")
         {
            this.mainSquadDisplay();
         }
         if(_loc_2 == "Create Base")
         {
            this.func_trytocreateBase(this.baseXgrid.text,this.baseYgrid.text);
         }
         if(_loc_2 == "Destroy Base")
         {
            this.squadBaseButton.visible = true;
            this.squadBaseButton.actionlabel.text = "Confirm Destruction";
            this.squadBaseTwoButton.visible = true;
            this.squadBaseTwoButton.actionlabel.text = "Cancel Destruction";
         }
         if(_loc_2 == "Cancel Destructio")
         {
            this.mainSquadDisplay();
         }
         if(_loc_2 == "Confirm Destruction")
         {
            this.mysocket.send("SQUAD`DESTROYBASE`~");
            this.mainSquadDisplay();
         }
         if(_loc_2 == "Set Message")
         {
            this.func_ChangeSquadMessage();
         }
      }
      
      public function DisplayNoSquad() : *
      {
         this.nameLabel.text = "Squad Name:";
         this.passLabel.text = "Squad Password:";
         this.squadsPassDisp.text = "Not In A Squad";
         this.squadAction_button.visible = true;
         this.squadAction_button.actionlabel.text = "Create A Squad";
         this.squadAction_buttontwo.visible = true;
         this.squadAction_buttontwo.actionlabel.text = "Join A Squad";
         this.squadBaseButton.visible = false;
         this.squadBaseTwoButton.visible = false;
         this.squadsNameDisp.text = "Not In A Squad";
      }
      
      public function windowActive(event:FocusEvent) : void
      {
         Object(root).TypingInTextField = true;
      }
      
      public function displayInSquad() : *
      {
         this.nameLabel.text = "Squad Name:";
         this.squadsNameDisp.text = this.SquadName;
         this.passLabel.text = "Squad Password:";
         this.squadsPassDisp.text = this.playershipstatus[5][13];
         this.squadAction_button.visible = false;
         this.squadAction_buttontwo.visible = false;
         if(this.playershipstatus[5][11] == false)
         {
            this.squadAction_buttontwo.visible = true;
            this.squadAction_buttontwo.actionlabel.text = "Leave Squad";
         }
         else
         {
            this.squadAction_buttontwo.visible = true;
            this.squadAction_buttontwo.actionlabel.text = "Dissolve Squad";
         }
      }
      
      public function func_squadOptionsClick(event:MouseEvent) : void
      {
         if(!this.isplayeraguest)
         {
            this.gameoptions.gotoAndStop(2);
            this.squadoptions.gotoAndStop(1);
            this.exit_button.gotoAndStop(2);
            Object(root).removeEventListener(KeyboardEvent.KEY_DOWN,this.OptionsKeyListener);
            this.func_playRegularClick();
            gotoAndStop("squadsettings");
         }
      }
      
      public function func_receivedsquadleaving(param1:*) : *
      {
         if(param1 == "left")
         {
            this.playershipstatus[5][10] = "NONE";
            this.mainSquadDisplay();
         }
         else
         {
            this.messageBox.text = "Error Leaving!";
            this.squadAction_button.visible = true;
            this.squadAction_buttontwo.visible = true;
         }
      }
      
      public function func_cancelKeyChange(event:MouseEvent) : void
      {
         this.func_playRegularClick();
         this.func_cancelAnyKeyChange();
      }
      
      public function RevertChanged(event:MouseEvent) : void
      {
         this.func_cancelAnyKeyChange();
      }
      
      public function func_receivedsquadjoinage(param1:*) : *
      {
         if(param1 == "joined")
         {
            this.playershipstatus[5][10] = this.TheSquadName;
            this.playershipstatus[5][13] = this.TheSquadPass;
            this.playershipstatus[5][11] = false;
            this.mainSquadDisplay();
         }
         else if(param1 == "toomany")
         {
            this.messageBox.text = "Squad is Full!";
            this.squadAction_button.visible = true;
            this.squadAction_buttontwo.visible = true;
         }
         else if(param1 == "failedjoin")
         {
            this.messageBox.text = "Wrong Information!";
            this.squadAction_button.visible = true;
            this.squadAction_buttontwo.visible = true;
         }
         else
         {
            this.messageBox.text = "Invalid Message!";
            this.squadAction_button.visible = true;
            this.squadAction_buttontwo.visible = true;
         }
      }
      
      public function func_cancelAnyKeyChange() : *
      {
         Object(root).removeEventListener(KeyboardEvent.KEY_DOWN,this.OptionsKeyListener);
         this.FieldPressed = "";
         this.cancel_key_changes.visible = false;
         this.func_displayKeySetup();
      }
      
      internal function frame10() : *
      {
         this.newGameSettings = this.initialGameSettings;
         trace("name:" + this.playershipstatus[3][2]);
         this.reset_changes.actionlabel.text = "Reset Changes";
         this.accept_changes.actionlabel.text = "Save Changes";
         this.sound_on.actionlabel.text = "On";
         this.sound_off.actionlabel.text = "Off";
         this.combat_sound_on.actionlabel.text = "On";
         this.combat_sound_off.actionlabel.text = "Off";
         this.music_on.actionlabel.text = "On";
         this.music_off.actionlabel.text = "Off";
         this.bck_on.actionlabel.text = "On";
         this.bck_off.actionlabel.text = "Off";
         this.scroll_bck_on.actionlabel.text = "On";
         this.scroll_bck_off.actionlabel.text = "Off";
         this.gameTips_on.actionlabel.text = "On";
         this.gameTips_off.actionlabel.text = "Off";
         this.cancel_key_changes.actionlabel.text = "Cancel Change";
         this.chat_Small.actionlabel.text = "Small";
         this.chat_med.actionlabel.text = "Med.";
         this.chat_large.actionlabel.text = "Large";
         this.menu_vol_but.actionlabel.text = "Set";
         this.menu_vol_but.gotoAndStop(1);
         this.combat_vol_but.actionlabel.text = "Set";
         this.combat_vol_but.gotoAndStop(1);
         this.music_vol_but.actionlabel.text = "Set";
         this.music_vol_but.gotoAndStop(1);
         this.tot_stars_but.actionlabel.text = "Set";
         this.tot_stars_but.gotoAndStop(1);
         this.latency_delay_set.actionlabel.text = "Set";
         this.latency_delay_set.gotoAndStop(1);
         this.func_displayKeySetup();
         this.FieldPressed = "";
         this.cancel_key_changes.visible = false;
         this.key_accel.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_deaccel.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_left.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_right.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_shift.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_dock.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_guns.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_missiles.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_targeter.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_map.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.key_help.addEventListener(MouseEvent.MOUSE_DOWN,this.keyTogglePress);
         this.cancel_key_changes.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelKeyChange);
         this.sound_on.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.sound_off.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.bck_on.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.bck_off.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.scroll_bck_on.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.scroll_bck_off.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.chat_Small.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.chat_med.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.chat_large.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.music_on.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.music_off.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.combat_sound_on.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.combat_sound_off.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.latency_delay_set.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.gameTips_on.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.gameTips_off.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.menu_vol_but.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.combat_vol_but.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.music_vol_but.addEventListener(MouseEvent.MOUSE_DOWN,this.buttonTogglePress);
         this.reset_changes.addEventListener(MouseEvent.MOUSE_DOWN,this.RevertChanged);
         this.accept_changes.addEventListener(MouseEvent.MOUSE_DOWN,this.SaveChanges);
      }
      
      public function func_preferences_membersofsquad(param1:*) : *
      {
         var _loc_6:* = undefined;
         var _loc_7:* = undefined;
         var _loc_8:* = undefined;
         var _loc_9:* = undefined;
         var _loc_10:* = undefined;
         var _loc_11:* = undefined;
         var _loc_12:* = undefined;
         var _loc_13:* = undefined;
         var _loc_14:* = undefined;
         var _loc_15:* = undefined;
         trace("prefdata rec" + param1);
         var _loc_2:* = param1.split("`");
         var _loc_3:* = "";
         var _loc_4:* = "";
         this.SquadMessageInfoBox.text = "";
         var _loc_5:* = 0;
         while(_loc_5 < _loc_2.length - 1)
         {
            _loc_6 = _loc_2[_loc_5].split(":");
            if(_loc_6[0] == "MESS")
            {
               this.SquadMessage.htmlText = _loc_6[1];
            }
            else if(_loc_6[0] == "CR")
            {
               _loc_4 = _loc_6[1];
            }
            else if(_loc_6[0] == "P")
            {
               if(_loc_4 != _loc_6[1])
               {
                  _loc_3 += " " + _loc_6[1] + "\r";
               }
            }
            else if(_loc_6[0] == "PASS")
            {
               this.squadsPassDisp.text = _loc_6[1];
            }
            else if(_loc_6[0] == "B")
            {
               _loc_7 = this.sectorinformation[1][0];
               _loc_8 = this.sectorinformation[1][1];
               if(_loc_6[1] == "N/A")
               {
                  this.squadbase_info.text = "No Squad Base";
                  this.squadBaseButton.visible = true;
                  this.squadBaseButton.actionlabel.text = "Create A Base";
               }
               else
               {
                  _loc_9 = Number(_loc_6[1]);
                  _loc_10 = Number(_loc_6[2]);
                  _loc_11 = Math.ceil(_loc_9 / _loc_7) + 1;
                  _loc_12 = Math.ceil(_loc_10 / _loc_8) + 1;
                  _loc_13 = _loc_6[3];
                  _loc_14 = "Grid X: " + _loc_11 + ", Y: " + _loc_12;
                  _loc_15 = "Zone: " + _loc_13;
                  this.squadbase_info.text = _loc_15 + " " + _loc_14;
                  this.squadBaseButton.visible = true;
                  this.squadBaseButton.actionlabel.text = "Destroy Base";
               }
            }
            _loc_5 += 1;
         }
         this.MemberList.text = "Owner:\r " + _loc_4 + "\rMembers:\r" + _loc_3;
      }
      
      internal function frame1() : *
      {
         this.gameoptions.addEventListener(MouseEvent.MOUSE_DOWN,this.func_gameOptionsClick);
         this.squadoptions.addEventListener(MouseEvent.MOUSE_DOWN,this.func_squadOptionsClick);
         this.gameoptions.actionlabel.text = "Game Options";
         this.squadoptions.actionlabel.text = "Squad Setup";
         this.exit_button.actionlabel.text = "Exit";
         this.gameoptions.gotoAndStop(1);
         this.squadoptions.gotoAndStop(2);
         this.exit_button.gotoAndStop(2);
         gotoAndStop("settings");
      }
      
      public function func_trytocreateBase(param1:*, param2:*) : *
      {
         var _loc_5:* = undefined;
         var _loc_6:* = undefined;
         var _loc_7:* = undefined;
         var _loc_8:* = undefined;
         var _loc_9:* = undefined;
         var _loc_10:* = undefined;
         var _loc_11:* = undefined;
         var _loc_12:* = undefined;
         var _loc_13:* = undefined;
         var _loc_14:* = undefined;
         var _loc_15:* = undefined;
         var _loc_3:* = this.sectorinformation[0][0] - 1;
         var _loc_4:* = this.sectorinformation[0][1] - 1;
         if(!isNaN(param1) && !isNaN(param2))
         {
            if(param1 > 0 && param2 > 0 && param1 <= _loc_3 && param2 <= _loc_4)
            {
               this.BaseMessageBox.text = "Trying to Create";
               _loc_5 = this.playershipstatus[5][1];
               _loc_6 = this.sectorinformation[1][0];
               _loc_7 = this.sectorinformation[1][1];
               _loc_8 = this.SquadName;
               _loc_9 = Math.round(param1) - 1;
               _loc_10 = Math.round(param2) - 1;
               _loc_11 = false;
               _loc_12 = _loc_6 * _loc_9 - Math.round(_loc_6 / 2);
               _loc_13 = _loc_7 * _loc_10 - Math.round(_loc_7 / 2);
               _loc_14 = 0;
               while(_loc_14 < this.sectormapitems.length)
               {
                  if(this.sectormapitems[_loc_14] != null)
                  {
                     trace(this.sectormapitems[_loc_14]);
                     trace(this.sectormapitems[_loc_14][10] + "`" + _loc_9);
                     trace(this.sectormapitems[_loc_14][11] + "`" + _loc_10);
                     if(_loc_9 == this.sectormapitems[_loc_14][10] && _loc_10 == this.sectormapitems[_loc_14][11])
                     {
                        _loc_11 = true;
                        break;
                     }
                  }
                  _loc_14 += 1;
               }
               if(_loc_11 == true)
               {
                  trace("taken");
                  this.BaseMessageBox.text = "Something is already in that grid!";
               }
               else if(param1 != "" && param2 != "")
               {
                  _loc_15 = "SQUADB`CREATING`" + _loc_8 + "`" + _loc_12 + "`" + _loc_13 + "~";
                  this.mysocket.send(_loc_15);
                  this.squadBaseButton.visible = false;
                  this.squadBaseTwoButton.visible = false;
                  this.mainSquadDisplay();
               }
            }
            else
            {
               this.BaseMessageBox.text = "Xgrid:  1 to " + _loc_3 + ", Ygrid: 1 to " + _loc_4;
            }
         }
      }
      
      public function keyTogglePress(event:MouseEvent) : void
      {
         this.func_cancelAnyKeyChange();
         this.func_displayKeySetup();
         this.FieldPressed = String(event.target.name);
         Object(root).addEventListener(KeyboardEvent.KEY_DOWN,this.OptionsKeyListener);
         if(this.FieldPressed == "key_accel")
         {
            this.key_accel.text = "Press A Key";
         }
         if(this.FieldPressed == "key_deaccel")
         {
            this.key_deaccel.text = "Press A Key";
         }
         if(this.FieldPressed == "key_left")
         {
            this.key_left.text = "Press A Key";
         }
         if(this.FieldPressed == "key_right")
         {
            this.key_right.text = "Press A Key";
         }
         if(this.FieldPressed == "key_shift")
         {
            this.key_shift.text = "Press A Key";
         }
         if(this.FieldPressed == "key_dock")
         {
            this.key_dock.text = "Press A Key";
         }
         if(this.FieldPressed == "key_guns")
         {
            this.key_guns.text = "Press A Key";
         }
         if(this.FieldPressed == "key_missiles")
         {
            this.key_missiles.text = "Press A Key";
         }
         if(this.FieldPressed == "key_targeter")
         {
            this.key_targeter.text = "Press A Key";
         }
         if(this.FieldPressed == "key_map")
         {
            this.key_map.text = "Press A Key";
         }
         if(this.FieldPressed == "key_help")
         {
            this.key_help.text = "Press A Key";
         }
         this.cancel_key_changes.visible = true;
         this.func_playRegularClick();
      }
      
      public function SaveChanges(event:MouseEvent) : void
      {
         this.func_cancelAnyKeyChange();
      }
      
      public function func_checkoutgoingcharacters(param1:*) : *
      {
         var _loc_3:* = undefined;
         var _loc_4:* = undefined;
         var _loc_5:* = undefined;
         var _loc_2:* = 0;
         while(_loc_2 < param1.length)
         {
            _loc_3 = 0;
            while(_loc_3 < this.replacewithchar.length)
            {
               if(param1.substr(_loc_2,this.replacewithchar[_loc_3][1].length) == this.replacewithchar[_loc_3][1])
               {
                  trace("FOUND:" + this.replacewithchar[_loc_3][1]);
                  _loc_4 = param1.substr(0,_loc_2);
                  _loc_5 = param1.substr(_loc_2 + this.replacewithchar[_loc_3][1].length);
                  param1 = _loc_4 + this.replacewithchar[_loc_3][0] + _loc_5;
                  _loc_3 = 999;
               }
               _loc_3 += 1;
            }
            _loc_2 += 1;
         }
         return param1;
      }
      
      public function mainSquadDisplay() : *
      {
         this.BaseLocationHeader.text = "Base Location";
         this.SquadName = this.playershipstatus[5][10];
         this.messageBox.text = "";
         this.BaseLocationDetails.visible = false;
         this.baseXgrid.visible = false;
         this.baseYgrid.visible = false;
         this.squadBaseTwoButton.visible = false;
         this.BaseMessageBox.text = "";
         this.SquadMessageInfoBox.text = "";
         if(this.SquadName != "NONE")
         {
            this.mysocket.send("SQUAD`PREFSQUADINFO`~");
            this.squadbase_info.text = "Loading";
            this.MemberList.text = "Loading";
            this.SquadMessage.text = "Loading";
            this.changeSquadMessage.visible = true;
            this.displayInSquad();
         }
         else
         {
            this.MemberList.text = "Not in a Squad";
            this.squadbase_info.text = "Not in a Squad";
            this.changeSquadMessage.visible = false;
            this.SquadMessage.text = "";
            this.DisplayNoSquad();
         }
      }
      
      public function func_TrySquadCreation(param1:*, param2:*) : *
      {
         var _loc_3:* = undefined;
         param1 = param1.toUpperCase();
         param2 = param2.toUpperCase();
         if(param1.length > 13 || param2.length > 13)
         {
            this.messageBox.text = "12 Characters Max (Name And Password)";
         }
         else if(param1.length < 3 || param2.length < 3)
         {
            this.messageBox.text = "3 Characters Min (Name And Password)";
         }
         else if(this.checknameforlegitcharacters(param1))
         {
            this.messageBox.text = "Letters or numbers! (Name)";
         }
         else if(this.checknameforlegitcharacters(param2))
         {
            this.messageBox.text = "Letters or numbers! (Password)";
         }
         else
         {
            this.messageBox.text = "Waiting For Result";
            this.TheSquadName = param1;
            this.TheSquadPass = param2;
            _loc_3 = "SQUAD`CREATESQUAD`" + param1 + "`" + param2 + "~";
            trace(_loc_3);
            this.squadAction_button.visible = false;
            this.squadAction_buttontwo.visible = false;
            this.mysocket.send(_loc_3);
         }
      }
      
      public function func_receivedsquadcreation(param1:*) : *
      {
         if(param1 == "created")
         {
            this.playershipstatus[5][10] = this.TheSquadName;
            this.playershipstatus[5][11] = true;
            this.playershipstatus[5][13] = this.TheSquadPass;
            this.mainSquadDisplay();
            this.messageBox.text = "Squad Created";
         }
         else if(param1 == "exists")
         {
            this.messageBox.text = "Squad Name Exists!";
            this.squadAction_button.visible = true;
            this.squadAction_buttontwo.visible = true;
         }
         else
         {
            this.messageBox.text = "Error Creating";
            this.squadAction_button.visible = true;
            this.squadAction_buttontwo.visible = true;
         }
      }
      
      public function windowNotActive(event:FocusEvent) : void
      {
         Object(root).TypingInTextField = false;
      }
      
      internal function frame41() : *
      {
         stop();
         this.TheSquadName = "";
         this.TheSquadPass = "";
         this.replacewithchar = new Array();
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&apos;";
         this.replacewithchar[this.loc][1] = "\'";
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&quot;";
         this.replacewithchar[this.loc][1] = "\"";
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&gt;";
         this.replacewithchar[this.loc][1] = ">";
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&lt;";
         this.replacewithchar[this.loc][1] = "<";
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&#126;";
         this.replacewithchar[this.loc][1] = "~";
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&#96;";
         this.replacewithchar[this.loc][1] = "`";
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&#37;";
         this.replacewithchar[this.loc][1] = "%";
         this.mainSquadDisplay();
         this.squadAction_button.gotoAndStop(1);
         this.squadAction_buttontwo.gotoAndStop(1);
         this.squadBaseButton.gotoAndStop(1);
         this.squadBaseTwoButton.gotoAndStop(1);
         this.changeSquadMessage.gotoAndStop(1);
         this.squadAction_button.addEventListener(MouseEvent.MOUSE_DOWN,this.SquadFunction);
         this.squadAction_buttontwo.addEventListener(MouseEvent.MOUSE_DOWN,this.SquadFunction);
         this.squadBaseButton.addEventListener(MouseEvent.MOUSE_DOWN,this.SquadFunction);
         this.squadBaseTwoButton.addEventListener(MouseEvent.MOUSE_DOWN,this.SquadFunction);
         this.changeSquadMessage.addEventListener(MouseEvent.MOUSE_DOWN,this.SquadFunction);
         this.changeSquadMessage.actionlabel.text = "Set Message";
         this.squadsNameDisp.addEventListener(FocusEvent.FOCUS_OUT,this.windowNotActive);
         this.squadsNameDisp.addEventListener(FocusEvent.FOCUS_IN,this.windowActive);
         this.squadsPassDisp.addEventListener(FocusEvent.FOCUS_OUT,this.windowNotActive);
         this.squadsPassDisp.addEventListener(FocusEvent.FOCUS_IN,this.windowActive);
         this.SquadMessage.addEventListener(FocusEvent.FOCUS_OUT,this.windowNotActive);
         this.SquadMessage.addEventListener(FocusEvent.FOCUS_IN,this.windowActive);
      }
      
      public function func_gameOptionsClick(event:MouseEvent) : void
      {
         this.gameoptions.gotoAndStop(1);
         this.squadoptions.gotoAndStop(2);
         this.exit_button.gotoAndStop(2);
         Object(root).removeEventListener(KeyboardEvent.KEY_DOWN,this.OptionsKeyListener);
         this.func_playRegularClick();
         gotoAndStop("settings");
      }
      
      public function func_incomingSquadBaseInfo(param1:*) : *
      {
      }
      
      public function func_playRegularClick() : *
      {
         if(this.newGameSettings.soundvolume)
         {
            this.initialGameSettings.MenuChannelSound = this.gameSounds.regularClick.play();
            this.initialGameSettings.MenuChannelSound.soundTransform = this.initialGameSettings.MenuSoundTransform;
         }
      }
      
      public function buttonTogglePress(event:MouseEvent) : void
      {
         var _loc_3:* = undefined;
         var _loc_4:* = undefined;
         var _loc_5:* = undefined;
         var _loc_6:* = undefined;
         var _loc_7:* = undefined;
         this.func_playRegularClick();
         var _loc_2:* = String(event.target.parent.name);
         trace(_loc_2);
         if(_loc_2 == "sound_on")
         {
            this.newGameSettings.soundvolume = true;
         }
         if(_loc_2 == "sound_off")
         {
            this.newGameSettings.soundvolume = false;
            this.newGameSettings.gameMusic = false;
            this.newGameSettings.CombatSounds = false;
         }
         if(_loc_2 == "bck_on")
         {
            this.newGameSettings.showbckgrnd = true;
         }
         if(_loc_2 == "bck_off")
         {
            this.newGameSettings.showbckgrnd = false;
         }
         if(_loc_2 == "gameTips_on")
         {
            this.newGameSettings.GameTips = true;
         }
         else if(_loc_2 == "gameTips_off")
         {
            this.newGameSettings.GameTips = false;
         }
         if(_loc_2 == "scroll_bck_on")
         {
            this.newGameSettings.scrollingbckgrnd = true;
         }
         if(_loc_2 == "scroll_bck_off")
         {
            this.newGameSettings.scrollingbckgrnd = false;
         }
         if(_loc_2 == "chat_Small")
         {
            this.newGameSettings.chatTextSize = "S";
         }
         if(_loc_2 == "chat_med")
         {
            this.newGameSettings.chatTextSize = "M";
         }
         if(_loc_2 == "chat_large")
         {
            this.newGameSettings.chatTextSize = "L";
         }
         if(_loc_2 == "music_on")
         {
            this.newGameSettings.gameMusic = true;
         }
         if(_loc_2 == "music_off")
         {
            this.newGameSettings.gameMusic = false;
         }
         if(_loc_2 == "combat_sound_on")
         {
            this.newGameSettings.CombatSounds = true;
         }
         if(_loc_2 == "combat_sound_off")
         {
            this.newGameSettings.CombatSounds = false;
         }
         if(_loc_2 == "menu_vol_but")
         {
            _loc_3 = Number(this.menu_vol.text);
            if(!isNaN(_loc_3))
            {
               if(_loc_3 < 0)
               {
                  _loc_3 = 0;
               }
               else if(_loc_3 > 300)
               {
                  _loc_3 = 300;
               }
               this.newGameSettings.MenuVolume = Math.floor(_loc_3) * 0.01;
            }
         }
         if(_loc_2 == "combat_vol_but")
         {
            _loc_4 = Number(this.combat_vol.text);
            if(!isNaN(_loc_4))
            {
               if(_loc_4 < 0)
               {
                  _loc_4 = 0;
               }
               else if(_loc_4 > 300)
               {
                  _loc_4 = 300;
               }
               this.newGameSettings.IngameVolume = Math.floor(_loc_4) * 0.01;
            }
         }
         if(_loc_2 == "music_vol_but")
         {
            _loc_5 = Number(this.music_vol.text);
            if(!isNaN(_loc_5))
            {
               if(_loc_5 < 0)
               {
                  _loc_5 = 0;
               }
               else if(_loc_5 > 300)
               {
                  _loc_5 = 300;
               }
               this.newGameSettings.MusicVolume = Math.floor(_loc_5) * 0.01;
            }
         }
         if(_loc_2 == "tot_stars_but")
         {
            _loc_6 = Number(this.total_stars.text);
            if(!isNaN(_loc_6))
            {
               if(_loc_6 < 0)
               {
                  _loc_6 = 0;
               }
               else if(_loc_6 > 30)
               {
                  _loc_6 = 30;
               }
               this.newGameSettings.totalstars = _loc_6;
            }
         }
         if(_loc_2 == "latency_delay_set")
         {
            _loc_7 = Number(this.latency_delay.text);
            if(!isNaN(_loc_7))
            {
               if(_loc_7 < 80)
               {
                  _loc_7 = 80;
               }
               else if(_loc_7 > 350)
               {
                  _loc_7 = 350;
               }
               Object(root).shiplag = _loc_7;
            }
         }
         Object(root).gamesetting = this.newGameSettings;
         Object(root).func_checkMusicSettings();
         Object(root).func_refreshMenuVolumeSetting();
         Object(root).func_refreshIngameVolumeSetting();
         this.func_cancelAnyKeyChange();
      }
      
      public function returnKeyValueOfCode(param1:*) : *
      {
         var _loc_2:* = "";
         if(param1 == 65)
         {
            _loc_2 = "A";
         }
         else if(param1 == 66)
         {
            _loc_2 = "B";
         }
         else if(param1 == 67)
         {
            _loc_2 = "C";
         }
         else if(param1 == 68)
         {
            _loc_2 = "D";
         }
         else if(param1 == 69)
         {
            _loc_2 = "E";
         }
         else if(param1 == 70)
         {
            _loc_2 = "F";
         }
         else if(param1 == 71)
         {
            _loc_2 = "G";
         }
         else if(param1 == 72)
         {
            _loc_2 = "H";
         }
         else if(param1 == 73)
         {
            _loc_2 = "I";
         }
         else if(param1 == 74)
         {
            _loc_2 = "J";
         }
         else if(param1 == 75)
         {
            _loc_2 = "K";
         }
         else if(param1 == 76)
         {
            _loc_2 = "L";
         }
         else if(param1 == 77)
         {
            _loc_2 = "M";
         }
         else if(param1 == 78)
         {
            _loc_2 = "N";
         }
         else if(param1 == 79)
         {
            _loc_2 = "O";
         }
         else if(param1 == 80)
         {
            _loc_2 = "P";
         }
         else if(param1 == 81)
         {
            _loc_2 = "Q";
         }
         else if(param1 == 82)
         {
            _loc_2 = "R";
         }
         else if(param1 == 83)
         {
            _loc_2 = "S";
         }
         else if(param1 == 84)
         {
            _loc_2 = "T";
         }
         else if(param1 == 85)
         {
            _loc_2 = "U";
         }
         else if(param1 == 86)
         {
            _loc_2 = "V";
         }
         else if(param1 == 87)
         {
            _loc_2 = "W";
         }
         else if(param1 == 88)
         {
            _loc_2 = "X";
         }
         else if(param1 == 89)
         {
            _loc_2 = "Y";
         }
         else if(param1 == 90)
         {
            _loc_2 = "Z";
         }
         else if(param1 == 48)
         {
            _loc_2 = "0";
         }
         else if(param1 == 49)
         {
            _loc_2 = "1";
         }
         else if(param1 == 50)
         {
            _loc_2 = "2";
         }
         else if(param1 == 51)
         {
            _loc_2 = "3";
         }
         else if(param1 == 52)
         {
            _loc_2 = "4";
         }
         else if(param1 == 53)
         {
            _loc_2 = "5";
         }
         else if(param1 == 54)
         {
            _loc_2 = "6";
         }
         else if(param1 == 55)
         {
            _loc_2 = "7";
         }
         else if(param1 == 56)
         {
            _loc_2 = "8";
         }
         else if(param1 == 57)
         {
            _loc_2 = "9";
         }
         else if(param1 == 8)
         {
            _loc_2 = "Backspace";
         }
         else if(param1 == 9)
         {
            _loc_2 = "Tab";
         }
         else if(param1 == 13)
         {
            _loc_2 = "Enter";
         }
         else if(param1 == 16)
         {
            _loc_2 = "Shift";
         }
         else if(param1 == 17)
         {
            _loc_2 = "Control";
         }
         else if(param1 == 20)
         {
            _loc_2 = "Caps Lock";
         }
         else if(param1 == 27)
         {
            _loc_2 = "Escape";
         }
         else if(param1 == 32)
         {
            _loc_2 = "Space Bar";
         }
         else if(param1 == 33)
         {
            _loc_2 = "Page Up";
         }
         else if(param1 == 34)
         {
            _loc_2 = "Page Down";
         }
         else if(param1 == 35)
         {
            _loc_2 = "End";
         }
         else if(param1 == 36)
         {
            _loc_2 = "Home";
         }
         else if(param1 == 37)
         {
            _loc_2 = "Left Arrow";
         }
         else if(param1 == 38)
         {
            _loc_2 = "Up Arrow";
         }
         else if(param1 == 39)
         {
            _loc_2 = "Right Arrow";
         }
         else if(param1 == 40)
         {
            _loc_2 = "Down Arrow";
         }
         else if(param1 == 45)
         {
            _loc_2 = "Insert";
         }
         else if(param1 == 46)
         {
            _loc_2 = "Delete";
         }
         else if(param1 == 144)
         {
            _loc_2 = "Num Lock";
         }
         else if(param1 == 145)
         {
            _loc_2 = "ScrLck";
         }
         else if(param1 == 19)
         {
            _loc_2 = "Pause/Break";
         }
         else if(param1 == 186)
         {
            _loc_2 = "; or :";
         }
         else if(param1 == 187)
         {
            _loc_2 = "= or +";
         }
         else if(param1 == 189)
         {
            _loc_2 = "_ or -";
         }
         else if(param1 == 191)
         {
            _loc_2 = "/ or ?";
         }
         else if(param1 == 192)
         {
            _loc_2 = "` or ~";
         }
         else if(param1 == 219)
         {
            _loc_2 = "[ or {";
         }
         else if(param1 == 220)
         {
            _loc_2 = " or |";
         }
         else if(param1 == 221)
         {
            _loc_2 = "] or }";
         }
         else if(param1 == 222)
         {
            _loc_2 = "\" of \'";
         }
         else if(param1 == 188)
         {
            _loc_2 = ", or <";
         }
         else if(param1 == 190)
         {
            _loc_2 = ". or >";
         }
         else if(param1 == 191)
         {
            _loc_2 = "/ or ?";
         }
         else if(param1 == 112)
         {
            _loc_2 = "F1";
         }
         else if(param1 == 113)
         {
            _loc_2 = "F2";
         }
         else if(param1 == 114)
         {
            _loc_2 = "F3";
         }
         else if(param1 == 115)
         {
            _loc_2 = "F4";
         }
         else if(param1 == 116)
         {
            _loc_2 = "F5";
         }
         else if(param1 == 117)
         {
            _loc_2 = "F6";
         }
         else if(param1 == 118)
         {
            _loc_2 = "F7";
         }
         else if(param1 == 119)
         {
            _loc_2 = "F8";
         }
         else if(param1 == 120)
         {
            _loc_2 = "F9";
         }
         else if(param1 == 121)
         {
            _loc_2 = "F10";
         }
         else if(param1 == 122)
         {
            _loc_2 = "F11";
         }
         else if(param1 == 123)
         {
            _loc_2 = "F12";
         }
         return _loc_2;
      }
      
      public function func_displayKeySetup() : *
      {
         this.key_accel.text = this.returnKeyValueOfCode(this.newGameSettings.accelkey);
         this.key_deaccel.text = this.returnKeyValueOfCode(this.newGameSettings.deaccelkey);
         this.key_left.text = this.returnKeyValueOfCode(this.newGameSettings.turnleftkey);
         this.key_right.text = this.returnKeyValueOfCode(this.newGameSettings.turnrightkey);
         this.key_shift.text = this.returnKeyValueOfCode(this.newGameSettings.afterburnerskey);
         this.key_dock.text = this.returnKeyValueOfCode(this.newGameSettings.dockkey);
         this.key_guns.text = this.returnKeyValueOfCode(this.newGameSettings.gunskey);
         this.key_missiles.text = this.returnKeyValueOfCode(this.newGameSettings.missilekey);
         this.key_targeter.text = this.returnKeyValueOfCode(this.newGameSettings.targeterkey);
         this.total_stars.text = this.newGameSettings.totalstars;
         this.latency_delay.text = Object(root).shiplag;
         this.key_map.text = this.returnKeyValueOfCode(this.newGameSettings.MapKey);
         this.key_help.text = this.returnKeyValueOfCode(this.newGameSettings.HelpKey);
         if(this.newGameSettings.showbckgrnd)
         {
            this.bck_on.gotoAndStop(1);
            this.bck_off.gotoAndStop(2);
            if(this.newGameSettings.scrollingbckgrnd)
            {
               this.scroll_bck_on.gotoAndStop(1);
               this.scroll_bck_off.gotoAndStop(2);
            }
            else
            {
               this.scroll_bck_on.gotoAndStop(2);
               this.scroll_bck_off.gotoAndStop(1);
            }
         }
         else
         {
            this.bck_on.gotoAndStop(2);
            this.bck_off.gotoAndStop(1);
            this.scroll_bck_on.gotoAndStop(2);
            this.scroll_bck_off.gotoAndStop(2);
         }
         if(this.newGameSettings.soundvolume)
         {
            this.sound_on.gotoAndStop(1);
            this.sound_off.gotoAndStop(2);
         }
         else
         {
            this.sound_on.gotoAndStop(2);
            this.sound_off.gotoAndStop(1);
         }
         if(this.newGameSettings.gameMusic)
         {
            this.music_on.gotoAndStop(1);
            this.music_off.gotoAndStop(2);
         }
         else
         {
            this.music_on.gotoAndStop(2);
            this.music_off.gotoAndStop(1);
         }
         if(this.newGameSettings.chatTextSize == "S")
         {
            this.chat_Small.gotoAndStop(1);
            this.chat_med.gotoAndStop(2);
            this.chat_large.gotoAndStop(2);
         }
         else if(this.newGameSettings.chatTextSize == "M")
         {
            this.chat_Small.gotoAndStop(2);
            this.chat_med.gotoAndStop(1);
            this.chat_large.gotoAndStop(2);
         }
         else
         {
            this.chat_Small.gotoAndStop(2);
            this.chat_med.gotoAndStop(2);
            this.chat_large.gotoAndStop(1);
         }
         if(this.newGameSettings.CombatSounds)
         {
            this.combat_sound_on.gotoAndStop(1);
            this.combat_sound_off.gotoAndStop(2);
         }
         else
         {
            this.combat_sound_on.gotoAndStop(2);
            this.combat_sound_off.gotoAndStop(1);
         }
         if(this.newGameSettings.GameTips)
         {
            this.gameTips_on.gotoAndStop(1);
            this.gameTips_off.gotoAndStop(2);
         }
         else
         {
            this.gameTips_on.gotoAndStop(2);
            this.gameTips_off.gotoAndStop(1);
         }
         this.menu_vol.text = String(Number(this.newGameSettings.MenuVolume) * 100);
         this.combat_vol.text = String(Number(this.newGameSettings.IngameVolume) * 100);
         this.music_vol.text = String(Number(this.newGameSettings.MusicVolume) * 100);
      }
      
      public function func_ChangeSquadMessage() : *
      {
         var _loc_1:* = this.SquadMessage.text.substr(0,200);
         _loc_1 = this.func_checkoutgoingcharacters(_loc_1);
         trace(_loc_1);
         var _loc_2:* = "SQUAD`INFO`MESS`" + _loc_1 + "`~";
         this.SquadMessageInfoBox.text = "Updating Message";
         this.mysocket.send(_loc_2);
      }
      
      public function checknameforlegitcharacters(param1:*) : *
      {
         var _loc_4:* = undefined;
         var _loc_5:* = undefined;
         var _loc_2:* = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
         var _loc_3:* = 0;
         _loc_3 = 0;
         while(_loc_3 < param1.length)
         {
            _loc_4 = true;
            _loc_5 = 0;
            _loc_5 = 0;
            while(_loc_5 < _loc_2.length)
            {
               if(param1.charAt(_loc_3) == _loc_2.charAt(_loc_5))
               {
                  _loc_4 = false;
               }
               _loc_5 += 1;
            }
            if(_loc_4 == true)
            {
               return true;
            }
            _loc_3 += 1;
         }
      }
      
      public function OptionsKeyListener(event:KeyboardEvent) : void
      {
         var _loc_2:* = event.keyCode;
         trace("Press:" + _loc_2);
         this.func_keyChangeSelected(_loc_2);
         Object(root).removeEventListener(KeyboardEvent.KEY_DOWN,this.OptionsKeyListener);
      }
      
      public function func_TrySquadJoin(param1:*, param2:*) : *
      {
         var _loc_3:* = undefined;
         param1 = param1.toUpperCase();
         param2 = param2.toUpperCase();
         if(param1.length > 13 || param2.length > 13)
         {
            this.messageBox.text = "12 Characters Max (Name And Password)";
         }
         else if(param1.length < 3 || param2.length < 3)
         {
            this.messageBox.text = "3 Characters Min (Name And Password)";
         }
         else if(this.checknameforlegitcharacters(param1))
         {
            this.messageBox.text = "Letters or numbers! (Name)";
         }
         else if(this.checknameforlegitcharacters(param2))
         {
            this.messageBox.text = "Letters or numbers! (Password)";
         }
         else
         {
            this.messageBox.text = "Waiting For Joining Result";
            this.TheSquadName = param1;
            this.TheSquadPass = param2;
            _loc_3 = "SQUAD`JOINSQ`" + param1 + "`" + param2 + "~";
            trace(_loc_3);
            this.squadAction_button.visible = false;
            this.squadAction_buttontwo.visible = false;
            this.mysocket.send(_loc_3);
         }
      }
      
      public function func_keyChangeSelected(param1:*) : *
      {
         if(Number(param1) != 13)
         {
            if(this.FieldPressed == "key_accel")
            {
               this.newGameSettings.accelkey = param1;
            }
            if(this.FieldPressed == "key_deaccel")
            {
               this.newGameSettings.deaccelkey = param1;
            }
            if(this.FieldPressed == "key_left")
            {
               this.newGameSettings.turnleftkey = param1;
            }
            if(this.FieldPressed == "key_right")
            {
               this.newGameSettings.turnrightkey = param1;
            }
            if(this.FieldPressed == "key_shift")
            {
               this.newGameSettings.afterburnerskey = param1;
            }
            if(this.FieldPressed == "key_dock")
            {
               this.newGameSettings.dockkey = param1;
            }
            if(this.FieldPressed == "key_guns")
            {
               this.newGameSettings.gunskey = param1;
            }
            if(this.FieldPressed == "key_missiles")
            {
               this.newGameSettings.missilekey = param1;
            }
            if(this.FieldPressed == "key_targeter")
            {
               this.newGameSettings.targeterkey = param1;
            }
            if(this.FieldPressed == "key_map")
            {
               this.newGameSettings.MapKey = param1;
            }
            if(this.FieldPressed == "key_help")
            {
               this.newGameSettings.HelpKey = param1;
            }
            this.func_playRegularClick();
         }
         this.func_cancelAnyKeyChange();
      }
   }
}
