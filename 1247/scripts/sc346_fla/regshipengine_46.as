package sc346_fla
{
   import flash.display.*;
   
   public dynamic class regshipengine_46 extends MovieClip
   {
       
      
      public function regshipengine_46()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2);
      }
      
      internal function frame1() : *
      {
      }
      
      internal function frame2() : *
      {
         stop();
      }
      
      public function setSpeedRatio(param1:*) : *
      {
         var _loc_2:* = Math.ceil(param1 * 10 / 2) + 2;
         if(_loc_2 > 8)
         {
            _loc_2 = 8;
         }
         this.gotoAndStop(_loc_2);
      }
   }
}
