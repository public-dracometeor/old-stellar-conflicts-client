package sc346_fla
{
   import flash.display.*;
   
   public dynamic class aimessagepicturebox_25 extends MovieClip
   {
       
      
      public var PictureToShow:Object;
      
      public var staticscreen:MovieClip;
      
      public function aimessagepicturebox_25()
      {
         super();
         addFrameScript(0,this.frame1,38,this.frame39,39,this.frame40,40,this.frame41,41,this.frame42,42,this.frame43);
      }
      
      internal function frame41() : *
      {
         stop();
      }
      
      internal function frame42() : *
      {
         stop();
      }
      
      internal function frame39() : *
      {
         if(this.PictureToShow == "INCOME")
         {
            gotoAndStop("INCOME");
         }
         else if(this.PictureToShow == "NEWS")
         {
            gotoAndStop("NEWS");
         }
         else if(this.PictureToShow == "INFO")
         {
            gotoAndStop("INFO");
         }
         else
         {
            stop();
         }
      }
      
      internal function frame1() : *
      {
      }
      
      internal function frame40() : *
      {
         stop();
      }
      
      internal function frame43() : *
      {
         stop();
      }
   }
}
