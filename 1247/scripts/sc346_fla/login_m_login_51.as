package sc346_fla
{
   import flash.display.*;
   import flash.events.*;
   import flash.net.*;
   import flash.text.*;
   
   public dynamic class login_m_login_51 extends MovieClip
   {
       
      
      public var PlayerInConquestZone:Object;
      
      public var memberpassword1:Object;
      
      public var mysocket:Object;
      
      public var mov_signup:MovieClip;
      
      public var gamesetting:Object;
      
      public var mov_guests:MovieClip;
      
      public var emailtoSub:Object;
      
      public var memberpassword:Object;
      
      public var logindisplay:MovieClip;
      
      public var isAGuest:Object;
      
      public var membernameinput:Object;
      
      public var memberpassword2:Object;
      
      public var mov_player_signup:MovieClip;
      
      public var playershipstatus:Object;
      
      public var returnpassbutt:SimpleButton;
      
      public var gameSounds:Object;
      
      public var curentinputfield:Object;
      
      public var RelayMessage:TextField;
      
      public var hascreationbeensubmited:Object;
      
      public var mov_members:MovieClip;
      
      public var SentGuestRequest:Object;
      
      public var hasloginbeensubmited:Object;
      
      public var emailRetrieval:TextField;
      
      public function login_m_login_51()
      {
         super();
         addFrameScript(0,this.frame1,2,this.frame3,9,this.frame10,16,this.frame17,29,this.frame30,30,this.frame31,35,this.frame36,38,this.frame39,39,this.frame40,40,this.frame41,45,this.frame46,48,this.frame49,50,this.frame51,51,this.frame52,56,this.frame57,59,this.frame60,60,this.frame61);
      }
      
      public function func_playlogin_but_press() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.login_but_pressSound.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      internal function frame10() : *
      {
         this.func_playlogin_but_pops();
      }
      
      public function func_initializeCreateACcount() : *
      {
         addEventListener(KeyboardEvent.KEY_DOWN,this.myCreationListener);
      }
      
      public function myListener(event:KeyboardEvent) : void
      {
         if(event.keyCode == 13)
         {
            this.func_submit();
         }
      }
      
      internal function frame60() : *
      {
         this.isAGuest = true;
         this.SentGuestRequest = false;
         this.mov_guests.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN,function(event:MouseEvent):void
         {
            func_playRegularClick();
            gotoAndPlay(1);
         });
         this.mov_guests.submitButton.addEventListener(MouseEvent.MOUSE_DOWN,function(event:MouseEvent):void
         {
            func_playRegularClick();
            func_Guestsubmit();
         });
         this.func_Guestinitialize();
         stop();
      }
      
      internal function frame31() : *
      {
      }
      
      public function accountcreation(param1:*, param2:*, param3:*) : *
      {
         this.mysocket.send("ACCT`CREATE`" + param1 + "`" + param2 + "`" + param3 + "`~");
      }
      
      public function func_trytologin(param1:*, param2:*) : *
      {
         param1 = param1.toUpperCase();
         param2 = param2.toUpperCase();
         if(param1.length > 11 || param1.length < 3)
         {
            this.func_RelayingMessage("Name must be at least 3 and no more than 11 Characters");
            this.func_playErrorSound();
         }
         else if(param2.length > 11 || param2.length < 3)
         {
            this.func_RelayingMessage("Password must be at least 3 and no more than 11 Characters");
            this.func_playErrorSound();
         }
         else if(this.checknameforlegitcharacters(param1))
         {
            this.func_RelayingMessage("Enter a Name that consists of only letters a-Z,and numbers");
            this.func_playErrorSound();
         }
         else if(this.checknameforlegitcharacters(param2))
         {
            this.func_RelayingMessage("Enter a Passwrod that consists of only letters a-Z,and numbers");
            this.func_playErrorSound();
         }
         else if(this.hasloginbeensubmited != true)
         {
            this.hasloginbeensubmited = true;
            this.playershipstatus[3][2] = param1;
            this.mysocket.send("ACCT`LOGIN`" + param1 + "`" + param2 + "`~");
            this.func_RelayingMessage("Sending Login Request");
            this.func_playRegularClick();
         }
         else
         {
            this.func_RelayingMessage("Still Trying to Login");
            this.func_playErrorSound();
         }
      }
      
      internal function frame36() : *
      {
         this.mov_members.gotoAndStop(2);
      }
      
      internal function frame30() : *
      {
         this.mov_members.thebutton.addEventListener(MouseEvent.MOUSE_UP,function(event:MouseEvent):void
         {
            func_playlogin_but_press();
            gotoAndPlay("members");
         });
         this.mov_player_signup.thebutton.addEventListener(MouseEvent.MOUSE_DOWN,function(event:MouseEvent):void
         {
            func_playlogin_but_press();
            gotoAndPlay("signup");
         });
         this.mov_guests.thebutton.addEventListener(MouseEvent.MOUSE_DOWN,function(event:MouseEvent):void
         {
            func_playlogin_but_press();
            gotoAndPlay("guests");
         });
         this.emailRetrieval.text = "";
         this.returnpassbutt.addEventListener(MouseEvent.MOUSE_DOWN,this.func_retrieve_pass);
         stop();
      }
      
      internal function frame39() : *
      {
         this.isAGuest = false;
         this.mov_members.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN,function(event:MouseEvent):void
         {
            func_playRegularClick();
            gotoAndPlay(1);
         });
         this.mov_members.submitButton.addEventListener(MouseEvent.MOUSE_DOWN,function(event:MouseEvent):void
         {
            func_playRegularClick();
            func_submit();
         });
         this.curentinputfield = "name";
         this.func_initialize();
         this.memberpassword = "";
         stop();
      }
      
      public function myGuestistener(event:KeyboardEvent) : void
      {
         if(event.keyCode == 13)
         {
            this.func_Guestsubmit();
         }
      }
      
      internal function frame61() : *
      {
         this.logindisplay.playershipstatus = this.playershipstatus;
         this.logindisplay.PlayerInConquestZone = this.PlayerInConquestZone;
         this.logindisplay.gameSounds = this.gameSounds;
         this.logindisplay.gamesetting = this.gamesetting;
         this.logindisplay.mysocket = this.mysocket;
         stop();
      }
      
      public function func_playErrorSound() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.errorClick.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      internal function frame41() : *
      {
      }
      
      public function func_zoneconnectionbeenmade() : *
      {
         this.func_playConnectingSound();
         this.logindisplay.gotoAndStop("beginentering");
      }
      
      public function func_login(param1:*) : *
      {
         param1 = param1.toUpperCase();
         if(this.SentGuestRequest)
         {
            this.func_RelayingMessage("Already Logging in as Guest");
         }
         else
         {
            this.func_RelayingMessage("Logging in as Guest");
            this.SentGuestRequest = true;
            this.mysocket.send("ACCT`LOGIN`" + param1 + "`" + "#GUEST" + "~");
            this.func_playRegularClick();
         }
      }
      
      internal function frame46() : *
      {
         this.mov_signup.gotoAndStop(2);
      }
      
      internal function frame40() : *
      {
         this.mov_members.gotoAndStop(3);
         this.logindisplay.mysocket = this.mysocket;
         this.logindisplay.playershipstatus = this.playershipstatus;
         this.logindisplay.PlayerInConquestZone = this.PlayerInConquestZone;
         this.logindisplay.gameSounds = this.gameSounds;
         this.logindisplay.gamesetting = this.gamesetting;
         stop();
      }
      
      public function func_failedLogin() : *
      {
         this.mov_members.username.text = "";
         this.mov_members.userpass.text = "";
         this.hasloginbeensubmited = false;
         this.func_RelayingMessage("Login Failed\rPlease try again.");
      }
      
      internal function frame49() : *
      {
         this.isAGuest = false;
         this.hascreationbeensubmited = false;
         this.mov_signup.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN,function(event:MouseEvent):void
         {
            func_playRegularClick();
            gotoAndPlay(1);
         });
         this.mov_signup.submitButton.addEventListener(MouseEvent.MOUSE_DOWN,function(event:MouseEvent):void
         {
            func_playRegularClick();
            func_submitCreatAccount();
         });
         this.func_initializeCreateACcount();
         stop();
      }
      
      public function func_failedCreate(param1:*) : *
      {
         this.hasloginbeensubmited = false;
         this.func_RelayingMessage("Login Failed\rPlease try again.");
         this.func_playErrorSound();
      }
      
      public function func_playRegularClick() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.regularClick.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      public function func_NameExists() : *
      {
         this.hascreationbeensubmited = false;
         this.func_RelayingMessage("Name Exists, Try Another");
         this.func_playErrorSound();
      }
      
      public function func_NameCreated() : *
      {
         this.func_RelayingMessage("Name Created, Please Wait");
         this.func_playRegularClick();
      }
      
      public function checknameforlegitcharacters(param1:*) : *
      {
         var _loc_4:* = undefined;
         var _loc_5:* = undefined;
         var _loc_2:* = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
         var _loc_3:* = 0;
         while(_loc_3 < param1.length)
         {
            _loc_4 = true;
            _loc_5 = 0;
            while(_loc_5 < _loc_2.length)
            {
               if(param1.charAt(_loc_3) == _loc_2.charAt(_loc_5))
               {
                  _loc_4 = false;
               }
               _loc_5 += 1;
            }
            if(_loc_4 == true)
            {
               return true;
            }
            _loc_3 += 1;
         }
         return false;
      }
      
      internal function frame51() : *
      {
         stop();
      }
      
      public function Incoming_Creation_Data(param1:*) : *
      {
         var _loc_2:* = String(param1);
         if(_loc_2 == "nameexists")
         {
            this.func_RelayingMessage("Name Already Owned, Please Try A Different Name");
            this.hascreationbeensubmited = false;
            this.func_playErrorSound();
         }
         else if(_loc_2 == "namecreated")
         {
            this.func_RelayingMessage("Account Created, Logging in");
            this.mysocket.send("ACCT`LOGIN`" + this.membernameinput + "`" + this.memberpassword1 + "`~");
            this.func_playRegularClick();
         }
      }
      
      public function func_loadForums(event:MouseEvent) : void
      {
         var _loc_2:* = new URLRequest("http://forum.gecko-games.com");
         navigateToURL(_loc_2,"_blank");
         this.func_playRegularClick();
      }
      
      public function func_loginaccepted() : *
      {
         this.func_RelayingMessage("");
         this.func_playlogin_but_press();
         gotoAndStop("memberconenct");
      }
      
      internal function frame52() : *
      {
      }
      
      internal function frame17() : *
      {
         this.func_playlogin_but_pops();
      }
      
      public function func_playlogin_but_pops() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.login_but_popsSound.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      internal function frame3() : *
      {
         this.func_playlogin_but_pops();
      }
      
      internal function frame57() : *
      {
         this.mov_guests.gotoAndStop(2);
      }
      
      public function func_playConnectingSound() : *
      {
         if(this.gamesetting.soundvolume)
         {
            this.gamesetting.MenuChannelSound = this.gameSounds.connectingSound.play();
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
         }
      }
      
      public function func_initialize() : *
      {
         addEventListener(KeyboardEvent.KEY_DOWN,this.myListener);
      }
      
      public function func_Guestsubmit() : *
      {
         var _loc_1:* = this.mov_guests.GuestName.text;
         if(_loc_1 != "")
         {
            this.func_login(_loc_1);
         }
      }
      
      public function func_submit() : *
      {
         this.membernameinput = this.mov_members.username.text;
         this.memberpassword = this.mov_members.userpass.text;
         if(this.membernameinput != "")
         {
            if(this.memberpassword != "")
            {
               this.func_trytologin(this.membernameinput,this.memberpassword);
            }
            else
            {
               this.func_RelayingMessage("Password Required");
               this.func_playErrorSound();
            }
         }
         else
         {
            this.func_RelayingMessage("Name Required");
            this.func_playErrorSound();
         }
      }
      
      public function checkEmail(param1:String) : Boolean
      {
         if(param1.indexOf(" ") > 0)
         {
            return false;
         }
         var _loc_2:* = param1.split("@");
         if(_loc_2.length != 2 || _loc_2[0].length == 0 || _loc_2[1].length == 0)
         {
            return false;
         }
         var _loc_3:* = _loc_2[1].split(".");
         if(_loc_3.length < 2)
         {
            return false;
         }
         var _loc_4:* = 0;
         while(_loc_4 < _loc_3.length)
         {
            if(_loc_3[_loc_4].length < 1)
            {
               return false;
            }
            _loc_4 += 1;
         }
         var _loc_5:* = _loc_3[_loc_3.length - 1];
         if(_loc_5.length < 2 || _loc_5.length > 3)
         {
            return false;
         }
         return true;
      }
      
      public function func_createaccount(param1:*, param2:*, param3:*, param4:*) : *
      {
         param1 = param1.toUpperCase();
         param2 = param2.toUpperCase();
         param3 = param3.toUpperCase();
         var _loc_5:* = param4.toLowerCase();
         var _loc_6:* = true;
         if(_loc_5.length > 0)
         {
            _loc_6 = this.checkEmail(_loc_5);
         }
         if(param1.length > 11 || param1.length < 3)
         {
            this.func_RelayingMessage("Name must be at least 3 and no more than 11 Characters");
            this.func_playErrorSound();
         }
         else if(param1 == "HOST")
         {
            this.func_RelayingMessage("Illegal Name");
            this.func_playErrorSound();
         }
         else if(this.checknameforlegitcharacters(param1))
         {
            this.func_RelayingMessage("Enter a Name that consists of only letters a-Z, and numbers");
            this.func_playErrorSound();
         }
         else if(param2.length > 10 || param2.length < 3)
         {
            this.func_RelayingMessage("Passwords must be at least 3 characters and no more than 10 characters");
            this.func_playErrorSound();
         }
         else if(this.checknameforlegitcharacters(param2))
         {
            this.func_RelayingMessage("Passwords consist of only letters a-Z, and numbers");
            this.func_playErrorSound();
         }
         else if(param3.toUpperCase() != param2.toUpperCase())
         {
            this.func_RelayingMessage("Passwords do not match");
            this.func_playErrorSound();
         }
         else if(!_loc_6)
         {
            this.func_RelayingMessage("Non-ValidEmail");
            this.func_playErrorSound();
         }
         else
         {
            this.func_RelayingMessage("Creating Account");
            if(this.hascreationbeensubmited != true)
            {
               this.hascreationbeensubmited = true;
               this.accountcreation(param1,param2,param4);
               this.func_playRegularClick();
            }
            else
            {
               this.func_RelayingMessage("Still Creating Your Account, Please Wait");
               this.func_playErrorSound();
            }
         }
      }
      
      public function func_submitCreatAccount() : *
      {
         this.membernameinput = this.mov_signup.UserName.text;
         this.memberpassword1 = this.mov_signup.password1.text;
         this.memberpassword2 = this.mov_signup.password2.text;
         this.emailtoSub = this.mov_signup.UserEmail.text;
         if(this.membernameinput != "")
         {
            if(this.memberpassword1 != "")
            {
               if(this.memberpassword2 != "")
               {
                  this.func_createaccount(this.membernameinput,this.memberpassword1,this.memberpassword2,this.emailtoSub);
               }
               else
               {
                  this.func_RelayingMessage("Verify Password.");
                  this.func_playErrorSound();
               }
            }
            else
            {
               this.func_RelayingMessage("Enter a Password.");
               this.func_playErrorSound();
            }
         }
         else
         {
            this.func_RelayingMessage("Enter a Name.");
            this.func_playErrorSound();
         }
      }
      
      public function func_retrieve_pass(event:MouseEvent) : void
      {
         var _loc_2:* = this.emailRetrieval.text;
         this.emailRetrieval.text = "";
         var _loc_3:* = new URLRequest("http://www.gecko-games.com/stellar/accounts.php?mode=sendpass&email=" + _loc_2);
         navigateToURL(_loc_3,"_blank");
      }
      
      public function func_Guestinitialize() : *
      {
         addEventListener(KeyboardEvent.KEY_DOWN,this.myGuestistener);
      }
      
      public function func_RelayingMessage(param1:*) : *
      {
         this.RelayMessage.visible = true;
         this.RelayMessage.text = param1;
         this.hascreationbeensubmited = false;
      }
      
      internal function frame1() : *
      {
         this.SentGuestRequest = false;
         this.hascreationbeensubmited = false;
         this.RelayMessage.text = "";
         this.hasloginbeensubmited = false;
         this.isAGuest = false;
      }
      
      public function myCreationListener(event:KeyboardEvent) : void
      {
         if(event.keyCode == 13)
         {
            this.func_submitCreatAccount();
         }
      }
   }
}
