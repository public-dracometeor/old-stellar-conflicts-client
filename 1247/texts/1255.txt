Welcome to Stellar Conflicts,
--- RECORDSEPARATOR ---
A futuristic space based multiplayer game where players fight each 
--- RECORDSEPARATOR ---
other with ships, do missions or trade goods from base to base and 
--- RECORDSEPARATOR ---
earn funds. You can use your funds to upgrade your ship, buy a new 
--- RECORDSEPARATOR ---
ship, and equip it with specials and missiles.  This game does take 
--- RECORDSEPARATOR ---
some time to fullly learn how to play, and has many features to 
--- RECORDSEPARATOR ---
offer.  Don't expect to be an ace pilot in one day!
--- RECORDSEPARATOR ---
IMPORTANT
--- RECORDSEPARATOR ---
:
--- RECORDSEPARATOR ---
  Game controls below
--- RECORDSEPARATOR ---
 
--- RECORDSEPARATOR ---
Movement: Arrow keys      Guns: Control     Afterburners: Shift 
--- RECORDSEPARATOR ---
MIssiles: Space (you need to buy in ship hardware first)
--- RECORDSEPARATOR ---
Docking: Be on a base or planet fly slow and press D
--- RECORDSEPARATOR ---
To change your targeted ship : T 
--- RECORDSEPARATOR ---
To Change teams: When docked (At a starbase or planet screen.) 
--- RECORDSEPARATOR ---
Go into preferences and and you can change your team in there.  If 
--- RECORDSEPARATOR ---
you want to fight with the Ai join team 0 or 1.
--- RECORDSEPARATOR ---
There is an in-game help. Click on the ? button or press F12
--- RECORDSEPARATOR ---
You can also message online staff by typing ?help and then your 
--- RECORDSEPARATOR ---
message.  The help contains information on the MANY different 
--- RECORDSEPARATOR ---
features in this game.
--- RECORDSEPARATOR ---
Notes:
--- RECORDSEPARATOR ---
Ai ships can't be locked on with seeker Missiles or specials
--- RECORDSEPARATOR ---
Ai Ships are on teams 0 and 1, if you find them tough you may want 
--- RECORDSEPARATOR ---
to join thier team