﻿package 
{
    import flash.display.*;

    dynamic public class shiptype17 extends MovieClip
    {
        public var Eng1:MovieClip;
        public var Eng2:MovieClip;
        public var Eng3:MovieClip;

        public function shiptype17()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        function frame1()
        {
            return;
        }// end function

        public function setSpeedRatio(param1)
        {
            this.Eng1.setSpeedRatio(param1);
            this.Eng2.setSpeedRatio(param1);
            this.Eng3.setSpeedRatio(param1);
            return;
        }// end function

    }
}
