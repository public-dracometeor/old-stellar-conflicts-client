﻿package 
{
    import flash.display.*;
    import flash.text.*;
    import flash.utils.*;

    dynamic public class aimessages2 extends MovieClip
    {
        public var timetoclear:Object;
        public var ainame:TextField;
        public var timestarted:Object;
        public var aipicture:MovieClip;
        public var timetokeepit:Object;
        public var aimessage:TextField;

        public function aimessages2()
        {
            addFrameScript(0, this.frame1, 1, this.frame2, 3, this.frame4, 4, this.frame5);
            return;
        }// end function

        function frame1()
        {
            stop();
            return;
        }// end function

        function frame4()
        {
            if (this.timetoclear > getTimer())
            {
                gotoAndPlay(3);
            }
            return;
        }// end function

        function frame5()
        {
            return;
        }// end function

        function frame2()
        {
            this.timestarted = getTimer();
            this.timetokeepit = 15000;
            this.timetoclear = this.timestarted + this.timetokeepit;
            return;
        }// end function

    }
}
