﻿package 
{
    import flash.display.*;

    dynamic public class shiphealthbar extends MovieClip
    {
        public var structperc:MovieClip;
        public var shieldperc:MovieClip;

        public function shiphealthbar()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        function frame1()
        {
            this.visible = false;
            stop();
            return;
        }// end function

        public function func_setLifeSettings(param1, param2)
        {
            this.visible = true;
            if (param2 >= 1)
            {
                param2 = 1;
            }
            else if (param2 < 0)
            {
                param2 = 0;
            }
            this.shieldperc.scaleX = Math.floor(param2 * 100) / 100;
            this.structperc.scaleX = Math.floor(param1 * 100) / 100;
            return;
        }// end function

    }
}
