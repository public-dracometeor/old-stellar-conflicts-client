﻿package 
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import flash.utils.*;

    dynamic public class ingamemap extends MovieClip
    {
        public var gridsize_x:Object;
        public var gridsize_y:Object;
        public var sectorinformation:Object;
        public var rangetotarget:TextField;
        public var MapIsVisible:Object;
        public var destination:TextField;
        public var MapGridLabelImage:Class;
        public var SectorItemImages:Object;
        public var MapGridXLabels:Object;
        public var lastTeamPositionSent:Object;
        public var ysecwidth:Object;
        public var gridxindent:Object;
        public var mysocket:Object;
        public var playersdestination:Object;
        public var ingamexwidthofasec:Object;
        public var xsecwidth:Object;
        public var PlayerLocater:Object;
        public var DesiredHeading:Object;
        public var noofysectors:Object;
        public var settingjumplocation:Object;
        public var noofxsectors:Object;
        public var xratio:Object;
        public var yratio:Object;
        public var informationtodisplay:TextField;
        public var StarBaseImages:Object;
        public var SectorSquare:Object;
        public var sectormapitems:Object;
        public var xrange:Object;
        public var gamemapscripting:MovieClip;
        public var MapGridYLabels:Object;
        public var starbaselocation:Object;
        public var lastTeamPositionSentinterval:Object;
        public var SectorSquareImage:Class;
        public var SquadBaseImages:Object;
        public var yrange:Object;
        public var gridyindent:Object;
        public var minimizeButton:SimpleButton;
        public var ingameywidthofasec:Object;
        public var PlayerLocaterImage:Class;

        public function ingamemap()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        public function func_setupthewaypoints()
        {
            var _loc_1:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = null;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            this.StarBaseImages = new Array();
            this.SectorItemImages = new Array();
            this.SquadBaseImages = new Array();
            var _loc_2:* = getDefinitionByName("ingamemapstarbasemarker") as Class;
            _loc_3 = 0;
            while (_loc_3 < this.starbaselocation.length)
            {
                
                this.StarBaseImages[_loc_3] = new _loc_2 as MovieClip;
                this.StarBaseImages[_loc_3].x = this.starbaselocation[_loc_3][1] * this.xratio + this.gridxindent + this.xsecwidth;
                this.StarBaseImages[_loc_3].y = this.starbaselocation[_loc_3][2] * this.yratio + this.gridyindent + this.ysecwidth;
                this.addChild(this.StarBaseImages[_loc_3]);
                if (this.starbaselocation[_loc_3][0].substr(0, 2) == "PL")
                {
                    _loc_4 = getDefinitionByName("planettype" + this.starbaselocation[_loc_3][3] + "image") as Class;
                    this.StarBaseImages[_loc_3].label.text = this.starbaselocation[_loc_3][0].substr(2);
                }
                else
                {
                    _loc_4 = getDefinitionByName("starbasetype" + this.starbaselocation[_loc_3][3] + "image") as Class;
                    this.StarBaseImages[_loc_3].label.text = this.starbaselocation[_loc_3][0];
                }
                _loc_5 = new _loc_4 as MovieClip;
                _loc_5.width = this.xsecwidth * 0.75;
                _loc_5.height = this.ysecwidth * 0.8;
                this.StarBaseImages[_loc_3].addChild(_loc_5);
                _loc_6 = 0;
                while (_loc_6 < this.sectormapitems.length)
                {
                    
                    if (this.sectormapitems[_loc_6] != null)
                    {
                        if (this.sectormapitems[_loc_6][0] == this.starbaselocation[_loc_3][0])
                        {
                            this.StarBaseImages[_loc_3].name = _loc_6;
                            this.StarBaseImages[_loc_3].addEventListener(MouseEvent.MOUSE_DOWN, this.func_SetHeadingDirection);
                        }
                    }
                    _loc_6 = _loc_6 + 1;
                }
                _loc_3 = _loc_3 + 1;
            }
            _loc_2 = getDefinitionByName("ingamemapnavpoint") as Class;
            _loc_3 = 0;
            while (_loc_3 < this.sectormapitems.length)
            {
                
                if (this.sectormapitems[_loc_3] != null)
                {
                    if (this.sectormapitems[_loc_3][0].substr(0, 2) == "TB")
                    {
                        _loc_1 = this.SectorItemImages.length;
                        this.SectorItemImages[_loc_1] = new _loc_2 as MovieClip;
                        this.SectorItemImages[_loc_1].x = this.sectormapitems[_loc_3][1] * this.xratio + this.gridxindent + this.xsecwidth;
                        this.SectorItemImages[_loc_1].y = this.sectormapitems[_loc_3][2] * this.yratio + this.gridyindent + this.ysecwidth;
                        this.SectorItemImages[_loc_1].label.text = this.sectormapitems[_loc_3][0];
                        this.addChild(this.SectorItemImages[_loc_1]);
                        this.SectorItemImages[_loc_1].name = _loc_3;
                        this.SectorItemImages[_loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_SetHeadingDirection);
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            _loc_2 = getDefinitionByName("ingamemapnavpoint") as Class;
            _loc_3 = 0;
            while (_loc_3 < this.sectormapitems.length)
            {
                
                if (this.sectormapitems[_loc_3] != null)
                {
                    if (this.sectormapitems[_loc_3][0].substr(0, 2) == "NP")
                    {
                        _loc_1 = this.SectorItemImages.length;
                        this.SectorItemImages[_loc_1] = new _loc_2 as MovieClip;
                        this.SectorItemImages[_loc_1].x = this.sectormapitems[_loc_3][1] * this.xratio + this.gridxindent + this.xsecwidth;
                        this.SectorItemImages[_loc_1].y = this.sectormapitems[_loc_3][2] * this.yratio + this.gridyindent + this.ysecwidth;
                        this.SectorItemImages[_loc_1].label.text = "Nav " + this.sectormapitems[_loc_3][0];
                        this.addChild(this.SectorItemImages[_loc_1]);
                        this.SectorItemImages[_loc_1].name = _loc_3;
                        this.SectorItemImages[_loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_SetHeadingDirection);
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            return;
        }// end function

        public function func_SetHeadingDirection(event:MouseEvent) : void
        {
            this.DesiredHeading = Number(event.target.parent.name);
            return;
        }// end function

        public function func_drawSquadbases()
        {
            return;
        }// end function

        public function func_initMap()
        {
            trace(this.sectorinformation[0][0]);
            this.noofxsectors = this.sectorinformation[0][0];
            this.noofysectors = this.sectorinformation[0][1];
            this.xsecwidth = this.gridsize_x / (this.noofxsectors + 1);
            this.ysecwidth = this.gridsize_y / (this.noofysectors + 1);
            this.ingamexwidthofasec = this.sectorinformation[1][0];
            this.ingameywidthofasec = this.sectorinformation[1][1];
            this.xratio = this.xsecwidth / this.ingamexwidthofasec;
            this.yratio = this.ysecwidth / this.ingameywidthofasec;
            this.drawgrid();
            this.func_setupthewaypoints();
            this.addChild(this.PlayerLocater);
            return;
        }// end function

        public function func_PlayerMapLocation(param1, param2, param3)
        {
            this.PlayerLocater.x = param1 * this.xratio + this.gridxindent + this.xsecwidth;
            this.PlayerLocater.y = param2 * this.yratio + this.gridyindent + this.ysecwidth;
            this.PlayerLocater.rotation = param3;
            return this.DesiredHeading;
        }// end function

        function frame1()
        {
            this.MapIsVisible = false;
            this.StarBaseImages = new Array();
            this.SectorItemImages = new Array();
            this.SquadBaseImages = new Array();
            this.DesiredHeading = -1;
            this.informationtodisplay.text = "N/A";
            this.destination.text = "None";
            this.rangetotarget.text = "RANGE: No Dest.";
            this.MapGridLabelImage = getDefinitionByName("ingamemapgridlabel") as Class;
            this.MapGridXLabels = new Array();
            this.MapGridYLabels = new Array();
            this.SectorSquareImage = getDefinitionByName("ingamemapsecsquare") as Class;
            this.PlayerLocaterImage = getDefinitionByName("ingamemapplayership") as Class;
            this.PlayerLocater = new this.PlayerLocaterImage() as MovieClip;
            this.addChild(this.PlayerLocater);
            this.SectorSquare = new Array();
            this.lastTeamPositionSent = 0;
            this.lastTeamPositionSentinterval = 1500;
            this.settingjumplocation = false;
            this.gridxindent = 50;
            this.gridyindent = 70;
            this.gridsize_x = 500;
            this.gridsize_y = 375;
            this.xrange = 0;
            this.yrange = 0;
            stop();
            return;
        }// end function

        public function drawgrid()
        {
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_1:* = 0;
            while (_loc_1 <= this.noofxsectors)
            {
                
                _loc_3 = this.xsecwidth * _loc_1 + this.gridxindent + this.xsecwidth / 2;
                _loc_4 = this.gridyindent - 10;
                this.MapGridXLabels[_loc_1] = new this.MapGridLabelImage() as MovieClip;
                this.MapGridXLabels[_loc_1].x = _loc_3;
                this.MapGridXLabels[_loc_1].y = _loc_4;
                this.addChild(this.MapGridXLabels[_loc_1]);
                this.MapGridXLabels[_loc_1].GridLabel.text = _loc_1 + 1;
                _loc_1 = _loc_1 + 1;
            }
            var _loc_2:* = 0;
            while (_loc_2 <= this.noofxsectors)
            {
                
                _loc_4 = this.ysecwidth * _loc_2 + this.gridyindent + this.ysecwidth / 2;
                _loc_3 = this.gridxindent - 20;
                this.MapGridYLabels[_loc_2] = new this.MapGridLabelImage() as MovieClip;
                this.MapGridYLabels[_loc_2].x = _loc_3;
                this.MapGridYLabels[_loc_2].y = _loc_4;
                this.addChild(this.MapGridYLabels[_loc_2]);
                this.MapGridYLabels[_loc_2].GridLabel.text = _loc_2 + 1;
                _loc_2 = _loc_2 + 1;
            }
            _loc_1 = 0;
            while (_loc_1 <= this.noofxsectors)
            {
                
                _loc_3 = this.xsecwidth * _loc_1 + this.gridxindent;
                this.SectorSquare[_loc_1] = new Array();
                _loc_2 = 0;
                while (_loc_2 <= this.noofysectors)
                {
                    
                    _loc_4 = this.ysecwidth * _loc_2 + this.gridyindent;
                    this.SectorSquare[_loc_1][_loc_2] = new this.SectorSquareImage() as MovieClip;
                    this.SectorSquare[_loc_1][_loc_2].x = _loc_3;
                    this.SectorSquare[_loc_1][_loc_2].y = _loc_4;
                    this.SectorSquare[_loc_1][_loc_2].width = this.xsecwidth;
                    this.SectorSquare[_loc_1][_loc_2].height = this.ysecwidth;
                    this.addChild(this.SectorSquare[_loc_1][_loc_2]);
                    _loc_2 = _loc_2 + 1;
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

    }
}
