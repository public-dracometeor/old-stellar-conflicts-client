﻿package sc313_fla
{
    import flash.display.*;

    dynamic public class loginscreen_35 extends MovieClip
    {
        public var counter:Object;
        public var mov_login:MovieClip;
        public var mysocket:Object;

        public function loginscreen_35()
        {
            addFrameScript(0, this.frame1, 1, this.frame2, 2, this.frame3, 16, this.frame17, 64, this.frame65);
            return;
        }// end function

        function frame1()
        {
            this.counter = 101;
            this.scaleX = 0.6;
            this.scaleY = 0.6;
            return;
        }// end function

        function frame2()
        {
            if (this.counter > 100)
            {
                this.mysocket.send("GAMEINFO`~");
                this.counter = 0;
            }
            return;
        }// end function

        function frame3()
        {
            var _loc_1:* = this;
            var _loc_2:* = this.counter + 1;
            _loc_1.counter = _loc_2;
            gotoAndPlay(2);
            return;
        }// end function

        function frame65()
        {
            this.mov_login.mysocket = this.mysocket;
            stop();
            return;
        }// end function

        function frame17()
        {
            return;
        }// end function

    }
}
