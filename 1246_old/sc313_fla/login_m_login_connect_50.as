﻿package sc313_fla
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import flash.utils.*;

    dynamic public class login_m_login_connect_50 extends MovieClip
    {
        public var ZoneInformation:TextField;
        public var resfreshingtext:Object;
        public var resfreshinginfo:TextField;
        public var lastrefresh:Object;
        public var zone400:MovieClip;
        public var zone600:MovieClip;
        public var zone200:MovieClip;
        public var refreshrate:Object;
        public var mysocket:Object;
        public var zone150:MovieClip;
        public var isplayeraguest:Object;
        public var zone175:MovieClip;
        public var DeathMatchZoneInfo:SimpleButton;
        public var currentstatus:TextField;
        public var StandardZoneInfo:SimpleButton;
        public var newPlayerZoneInfo:SimpleButton;

        public function login_m_login_connect_50()
        {
            addFrameScript(0, this.frame1, 9, this.frame10, 22, this.frame23, 23, this.frame24, 25, this.frame26, 29, this.frame30);
            return;
        }// end function

        function frame30()
        {
            removeEventListener(Event.ENTER_FRAME, this.myFunction);
            stop();
            return;
        }// end function

        function frame10()
        {
            return;
        }// end function

        function frame24()
        {
            this.isplayeraguest = false;
            this.ZoneInformation.text = "";
            this.zone150.players.text = "LOAD";
            this.zone150.zonename.text = "Zone 150";
            this.zone150.zoneselected = "150";
            this.zone150.mysocket = this.mysocket;
            this.zone150.enterzonebutton.visible = true;
            this.zone175.players.text = "LOAD";
            this.zone175.zonename.text = "Zone 175";
            this.zone175.zoneselected = "175";
            this.zone175.mysocket = this.mysocket;
            this.zone175.enterzonebutton.visible = true;
            this.zone200.players.text = "LOAD";
            this.zone200.zonename.text = "Zone 200";
            this.zone200.zoneselected = "200";
            this.zone200.mysocket = this.mysocket;
            this.zone400.players.text = "LOAD";
            this.zone400.zonename.text = "Zone 400";
            this.zone400.zoneselected = "400";
            this.zone400.mysocket = this.mysocket;
            this.zone600.players.text = "LOAD";
            this.zone600.zonename.text = "Zone 600";
            this.zone600.zoneselected = "600";
            this.zone600.mysocket = this.mysocket;
            if (this.isplayeraguest)
            {
                this.zone400.enterzonebutton.gotoAndStop(2);
                this.zone600.enterzonebutton.gotoAndStop(2);
            }
            this.mysocket.send("LISTING");
            this.resfreshingtext = "(REFRESHING)";
            this.resfreshinginfo.text = this.resfreshingtext;
            this.refreshrate = 15000;
            this.lastrefresh = getTimer() + this.refreshrate;
            addEventListener(Event.ENTER_FRAME, this.myFunction);
            this.newPlayerZoneInfo.addEventListener(MouseEvent.ROLL_OUT, this.manageMouseOut, false, 0, true);
            this.StandardZoneInfo.addEventListener(MouseEvent.ROLL_OUT, this.manageMouseOut, false, 0, true);
            this.DeathMatchZoneInfo.addEventListener(MouseEvent.ROLL_OUT, this.manageMouseOut, false, 0, true);
            this.newPlayerZoneInfo.addEventListener(MouseEvent.ROLL_OVER, this.manageMouseOver_NEWP, false, 0, true);
            this.StandardZoneInfo.addEventListener(MouseEvent.ROLL_OVER, this.manageMouseOver_STANDARD, false, 0, true);
            this.DeathMatchZoneInfo.addEventListener(MouseEvent.ROLL_OVER, this.manageMouseOver_DM, false, 0, true);
            stop();
            return;
        }// end function

        public function myFunction(event:Event)
        {
            if (getTimer() > this.lastrefresh)
            {
                this.lastrefresh = getTimer() + this.refreshrate;
                this.mysocket.send("LISTING");
                this.resfreshinginfo.text = this.resfreshingtext;
            }
            return;
        }// end function

        function frame23()
        {
            return;
        }// end function

        public function manageMouseOver_NEWP(event:MouseEvent) : void
        {
            this.ZoneInformation.text = "Free play zone for new players, weaker AI ships";
            return;
        }// end function

        public function func_incominglisting(param1)
        {
            if (param1[1] == 150)
            {
                this.zone150.players.text = param1[3] + "/" + param1[4];
                this.zone150.zonename.text = "Zone 150, < " + Math.ceil(int(param1[5]) / 50) + " score";
            }
            if (param1[1] == 175)
            {
                this.zone175.players.text = param1[3] + "/" + param1[4];
                this.zone175.zonename.text = "Zone 175, < " + Math.ceil(int(param1[5]) / 50) + " score";
            }
            if (param1[1] == 200)
            {
                this.zone200.players.text = param1[3] + "/" + param1[4];
            }
            if (param1[1] == 400)
            {
                this.zone400.players.text = param1[3] + "/" + param1[4];
            }
            if (param1[1] == 600)
            {
                this.zone600.players.text = param1[3] + "/" + param1[4];
            }
            this.resfreshinginfo.text = "";
            return;
        }// end function

        public function manageMouseOver_STANDARD(event:MouseEvent) : void
        {
            this.ZoneInformation.text = "Free play zone with regular AI Ships, Squad Bases and other events.";
            return;
        }// end function

        function frame26()
        {
            removeEventListener(Event.ENTER_FRAME, this.myFunction);
            stop();
            return;
        }// end function

        function frame1()
        {
            return;
        }// end function

        public function manageMouseOver_DM(event:MouseEvent) : void
        {
            this.ZoneInformation.text = "Death Match Zone where two teams battle head to head trying to kill their opponents base.";
            return;
        }// end function

        public function manageMouseOut(event:MouseEvent) : void
        {
            this.ZoneInformation.text = "";
            return;
        }// end function

    }
}
