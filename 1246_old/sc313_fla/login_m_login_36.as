﻿package sc313_fla
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;

    dynamic public class login_m_login_36 extends MovieClip
    {
        public var memberpassword1:Object;
        public var memberpassword2:Object;
        public var mysocket:Object;
        public var mov_signup:MovieClip;
        public var mov_guests:MovieClip;
        public var emailtoSub:Object;
        public var memberpassword:Object;
        public var logindisplay:MovieClip;
        public var isAGuest:Object;
        public var membernameinput:Object;
        public var mov_player_signup:MovieClip;
        public var returnpassbutt:SimpleButton;
        public var RelayMessage:TextField;
        public var curentinputfield:Object;
        public var hascreationbeensubmited:Object;
        public var mov_members:MovieClip;
        public var SentGuestRequest:Object;
        public var hasloginbeensubmited:Object;

        public function login_m_login_36()
        {
            addFrameScript(0, this.frame1, 4, this.frame5, 11, this.frame12, 19, this.frame20, 29, this.frame30, 30, this.frame31, 35, this.frame36, 38, this.frame39, 39, this.frame40, 40, this.frame41, 45, this.frame46, 48, this.frame49, 50, this.frame51, 51, this.frame52, 56, this.frame57, 59, this.frame60);
            return;
        }// end function

        public function myListener(event:KeyboardEvent) : void
        {
            if (event.keyCode == 13)
            {
                this.func_submit();
            }
            return;
        }// end function

        public function func_initializeCreateACcount()
        {
            addEventListener(KeyboardEvent.KEY_DOWN, this.myCreationListener);
            return;
        }// end function

        function frame31()
        {
            return;
        }// end function

        public function accountcreation(param1, param2, param3)
        {
            this.mysocket.send("ACCT`CREATE`" + param1 + "`" + param2 + "`" + param3 + "`~");
            return;
        }// end function

        public function func_trytologin(param1, param2)
        {
            param1 = param1.toUpperCase();
            param2 = param2.toUpperCase();
            if (param1.length > 11 || param1.length < 3)
            {
                this.func_RelayingMessage("Name must be at least 3 and no more than 11 Characters");
            }
            else if (param2.length > 11 || param2.length < 3)
            {
                this.func_RelayingMessage("Password must be at least 3 and no more than 11 Characters");
            }
            else if (this.checknameforlegitcharacters(param1))
            {
                this.func_RelayingMessage("Enter a Name that consists of only letters a-Z,and numbers");
            }
            else if (this.checknameforlegitcharacters(param2))
            {
                this.func_RelayingMessage("Enter a Passwrod that consists of only letters a-Z,and numbers");
            }
            else if (this.hasloginbeensubmited != true)
            {
                this.hasloginbeensubmited = true;
                this.mysocket.send("ACCT`LOGIN`" + param1 + "`" + param2 + "`~");
                this.func_RelayingMessage("Sending Login Request");
            }
            else
            {
                this.func_RelayingMessage("Still Trying to Login");
            }
            return;
        }// end function

        function frame36()
        {
            this.mov_members.gotoAndStop(2);
            return;
        }// end function

        function frame30()
        {
            this.mov_members.thebutton.addEventListener(MouseEvent.MOUSE_UP, function (event:MouseEvent) : void
            {
                gotoAndPlay("members");
                return;
            }// end function
            );
            this.mov_player_signup.thebutton.addEventListener(MouseEvent.MOUSE_UP, function (event:MouseEvent) : void
            {
                gotoAndPlay("signup");
                return;
            }// end function
            );
            this.mov_guests.thebutton.addEventListener(MouseEvent.MOUSE_UP, function (event:MouseEvent) : void
            {
                gotoAndPlay("guests");
                return;
            }// end function
            );
            stop();
            return;
        }// end function

        function frame39()
        {
            this.isAGuest = false;
            this.mov_members.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN, function (event:MouseEvent) : void
            {
                gotoAndPlay(1);
                return;
            }// end function
            );
            this.curentinputfield = "name";
            this.func_initialize();
            this.memberpassword = "";
            stop();
            return;
        }// end function

        public function myGuestistener(event:KeyboardEvent) : void
        {
            if (event.keyCode == 13)
            {
                this.func_Guestsubmit();
                trace("RAN");
            }
            return;
        }// end function

        function frame41()
        {
            return;
        }// end function

        public function func_zoneconnectionbeenmade()
        {
            this.logindisplay.gotoAndStop("beginentering");
            return;
        }// end function

        public function func_login(param1)
        {
            param1 = param1.toUpperCase();
            if (this.SentGuestRequest)
            {
                this.func_RelayingMessage("Already Logging in as Guest");
            }
            else
            {
                this.func_RelayingMessage("Logging in as Guest");
                this.SentGuestRequest = true;
                this.mysocket.send("ACCT`LOGIN`" + param1 + "`" + "#GUEST" + "~");
            }
            return;
        }// end function

        function frame46()
        {
            this.mov_signup.gotoAndStop(2);
            return;
        }// end function

        function frame40()
        {
            this.mov_members.gotoAndStop(3);
            this.logindisplay.mysocket = this.mysocket;
            stop();
            return;
        }// end function

        public function func_failedLogin()
        {
            this.mov_members.username.text = "";
            this.mov_members.userpass.text = "";
            this.hasloginbeensubmited = false;
            this.func_RelayingMessage("Login Failed\rPlease try again.");
            return;
        }// end function

        function frame49()
        {
            this.isAGuest = false;
            this.hascreationbeensubmited = false;
            this.mov_signup.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN, function (event:MouseEvent) : void
            {
                gotoAndPlay(1);
                return;
            }// end function
            );
            this.func_initializeCreateACcount();
            stop();
            return;
        }// end function

        public function func_failedCreate(param1)
        {
            this.hasloginbeensubmited = false;
            this.func_RelayingMessage("Login Failed\rPlease try again.");
            return;
        }// end function

        public function func_NameExists()
        {
            this.hascreationbeensubmited = false;
            this.func_RelayingMessage("Name Exists, Try Another");
            return;
        }// end function

        public function func_NameCreated()
        {
            this.func_RelayingMessage("Name Created, Please Wait");
            return;
        }// end function

        function frame52()
        {
            return;
        }// end function

        public function checknameforlegitcharacters(param1)
        {
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_2:* = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var _loc_3:* = 0;
            while (_loc_3 < param1.length)
            {
                
                _loc_4 = true;
                _loc_5 = 0;
                while (_loc_5 < _loc_2.length)
                {
                    
                    if (param1.charAt(_loc_3) == _loc_2.charAt(_loc_5))
                    {
                        _loc_4 = false;
                    }
                    _loc_5 = _loc_5 + 1;
                }
                if (_loc_4 == true)
                {
                    return true;
                }
                _loc_3 = _loc_3 + 1;
            }
            return false;
        }// end function

        function frame12()
        {
            return;
        }// end function

        function frame57()
        {
            this.mov_guests.gotoAndStop(2);
            return;
        }// end function

        function frame51()
        {
            stop();
            return;
        }// end function

        public function Incoming_Creation_Data(param1)
        {
            var _loc_2:* = String(param1);
            if (_loc_2 == "nameexists")
            {
                this.func_RelayingMessage("Name Already Owned, Please Try A Different Name");
                this.hascreationbeensubmited = false;
            }
            else if (_loc_2 == "namecreated")
            {
                this.func_RelayingMessage("Account Created, Logging in");
                this.mysocket.send("ACCT`LOGIN`" + this.membernameinput + "`" + this.memberpassword1 + "`~");
            }
            return;
        }// end function

        public function func_loginaccepted()
        {
            this.func_RelayingMessage("");
            gotoAndStop("memberconenct");
            return;
        }// end function

        public function func_Guestinitialize()
        {
            addEventListener(KeyboardEvent.KEY_DOWN, this.myGuestistener);
            return;
        }// end function

        function frame60()
        {
            this.isAGuest = true;
            this.SentGuestRequest = false;
            this.mov_guests.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN, function (event:MouseEvent) : void
            {
                gotoAndPlay(1);
                return;
            }// end function
            );
            this.func_Guestinitialize();
            stop();
            return;
        }// end function

        function frame5()
        {
            return;
        }// end function

        public function func_initialize()
        {
            addEventListener(KeyboardEvent.KEY_DOWN, this.myListener);
            return;
        }// end function

        public function func_Guestsubmit()
        {
            var _loc_1:* = this.mov_guests.GuestName.text;
            if (_loc_1 != "")
            {
                this.func_login(_loc_1);
            }
            return;
        }// end function

        public function func_submit()
        {
            this.membernameinput = this.mov_members.username.text;
            this.memberpassword = this.mov_members.userpass.text;
            if (this.membernameinput != "")
            {
                if (this.memberpassword != "")
                {
                    this.func_trytologin(this.membernameinput, this.memberpassword);
                }
                else
                {
                    this.func_RelayingMessage("Password Required");
                }
            }
            else
            {
                this.func_RelayingMessage("Name Required");
            }
            return;
        }// end function

        public function checkEmail(param1:String) : Boolean
        {
            if (param1.indexOf(" ") > 0)
            {
                return false;
            }
            var _loc_2:* = param1.split("@");
            if (_loc_2.length != 2 || _loc_2[0].length == 0 || _loc_2[1].length == 0)
            {
                return false;
            }
            var _loc_3:* = _loc_2[1].split(".");
            if (_loc_3.length < 2)
            {
                return false;
            }
            var _loc_4:* = 0;
            while (_loc_4 < _loc_3.length)
            {
                
                if (_loc_3[_loc_4].length < 1)
                {
                    return false;
                }
                _loc_4 = _loc_4 + 1;
            }
            var _loc_5:* = _loc_3[(_loc_3.length - 1)];
            if (_loc_5.length < 2 || _loc_5.length > 3)
            {
                return false;
            }
            return true;
        }// end function

        public function func_createaccount(param1, param2, param3, param4)
        {
            param1 = param1.toUpperCase();
            param2 = param2.toUpperCase();
            param3 = param3.toUpperCase();
            var _loc_5:* = param4.toLowerCase();
            var _loc_6:* = true;
            if (_loc_5.length > 0)
            {
                _loc_6 = this.checkEmail(_loc_5);
            }
            if (param1.length > 11 || param1.length < 3)
            {
                this.func_RelayingMessage("Name must be at least 3 and no more than 11 Characters");
            }
            else if (param1 == "HOST")
            {
                this.func_RelayingMessage("Illegal Name");
            }
            else if (this.checknameforlegitcharacters(param1))
            {
                this.func_RelayingMessage("Enter a Name that consists of only letters a-Z, and numbers");
            }
            else if (param2.length > 10 || param2.length < 3)
            {
                this.func_RelayingMessage("Passwords must be at least 3 characters and no more than 10 characters");
            }
            else if (this.checknameforlegitcharacters(param2))
            {
                this.func_RelayingMessage("Passwords consist of only letters a-Z, and numbers");
            }
            else if (param3.toUpperCase() != param2.toUpperCase())
            {
                this.func_RelayingMessage("Passwords do not match");
            }
            else if (!_loc_6)
            {
                this.func_RelayingMessage("Non-ValidEmail");
            }
            else
            {
                this.func_RelayingMessage("Creating Account");
                if (this.hascreationbeensubmited != true)
                {
                    this.hascreationbeensubmited = true;
                    this.accountcreation(param1, param2, param4);
                }
                else
                {
                    this.func_RelayingMessage("Still Creating Your Account, Please Wait");
                }
            }
            return;
        }// end function

        public function func_submitCreatAccount()
        {
            this.membernameinput = this.mov_signup.UserName.text;
            this.memberpassword1 = this.mov_signup.password1.text;
            this.memberpassword2 = this.mov_signup.password2.text;
            this.emailtoSub = this.mov_signup.UserEmail.text;
            if (this.membernameinput != "")
            {
                if (this.memberpassword1 != "")
                {
                    if (this.memberpassword2 != "")
                    {
                        this.func_createaccount(this.membernameinput, this.memberpassword1, this.memberpassword2, this.emailtoSub);
                    }
                    else
                    {
                        this.func_RelayingMessage("Verify Password.");
                    }
                }
                else
                {
                    this.func_RelayingMessage("Enter a Password.");
                }
            }
            else
            {
                this.func_RelayingMessage("Enter a Name.");
            }
            return;
        }// end function

        function frame20()
        {
            return;
        }// end function

        public function func_RelayingMessage(param1)
        {
            this.RelayMessage.visible = true;
            this.RelayMessage.text = param1;
            this.hascreationbeensubmited = false;
            return;
        }// end function

        function frame1()
        {
            this.SentGuestRequest = false;
            this.hascreationbeensubmited = false;
            this.RelayMessage.text = "";
            this.hasloginbeensubmited = false;
            this.isAGuest = false;
            return;
        }// end function

        public function myCreationListener(event:KeyboardEvent) : void
        {
            if (event.keyCode == 13)
            {
                this.func_submitCreatAccount();
            }
            return;
        }// end function

    }
}
