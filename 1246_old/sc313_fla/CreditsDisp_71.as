﻿package sc313_fla
{
    import flash.display.*;
    import flash.text.*;

    dynamic public class CreditsDisp_71 extends MovieClip
    {
        public var credits:TextField;

        public function CreditsDisp_71()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        public function func_displayCredits(param1)
        {
            this.credits.text = "Credits: " + param1;
            return;
        }// end function

        function frame1()
        {
            this.credits.text = "";
            stop();
            return;
        }// end function

    }
}
