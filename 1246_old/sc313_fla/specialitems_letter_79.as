﻿package sc313_fla
{
    import flash.display.*;
    import flash.text.*;

    dynamic public class specialitems_letter_79 extends MovieClip
    {
        public var setting:Object;
        public var button:TextField;

        public function specialitems_letter_79()
        {
            addFrameScript(0, this.frame1, 1, this.frame2, 2, this.frame3);
            return;
        }// end function

        function frame1()
        {
            this.setting = "OFF";
            stop();
            return;
        }// end function

        function frame2()
        {
            this.setting = "ON";
            stop();
            return;
        }// end function

        function frame3()
        {
            this.setting = "RELOAD";
            stop();
            return;
        }// end function

    }
}
