﻿package sc313_fla
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;

    dynamic public class login_m_zonesection_52 extends MovieClip
    {
        public var zoneselected:Object;
        public var players:TextField;
        public var enterzonebutton:MovieClip;
        public var zonename:TextField;
        public var mysocket:Object;

        public function login_m_zonesection_52()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        function frame1()
        {
            this.enterzonebutton.addEventListener(MouseEvent.MOUSE_UP, function (event:MouseEvent) : void
            {
                trace("test" + zoneselected + "`" + mysocket);
                mysocket.send("ZONE`" + zoneselected + "~");
                return;
            }// end function
            );
            stop();
            return;
        }// end function

    }
}
