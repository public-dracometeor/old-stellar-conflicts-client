﻿package sc313_fla
{
    import flash.display.*;
    import flash.text.*;

    dynamic public class scoreDisp_72 extends MovieClip
    {
        public var score:TextField;

        public function scoreDisp_72()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        public function func_displayScore(param1)
        {
            this.score.text = "Score: " + param1;
            return;
        }// end function

        function frame1()
        {
            this.score.text = "";
            stop();
            return;
        }// end function

    }
}
