package
{
   import flash.display.MovieClip;
   import flash.display.SimpleButton;
   import flash.text.TextField;
   
   public dynamic class hardwareitembox extends MovieClip
   {
       
      
      public var itemcost:TextField;
      
      public var itemspecs:TextField;
      
      public var buybuttonforhardware:SimpleButton;
      
      public var itemname:TextField;
      
      public function hardwareitembox()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      internal function frame1() : *
      {
         stop();
      }
   }
}
