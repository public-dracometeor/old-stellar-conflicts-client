package
{
   import flash.display.MovieClip;
   import flash.text.TextField;
   
   public dynamic class hardwaremissileinformationbutton extends MovieClip
   {
       
      
      public var maxpurchase:TextField;
      
      public var banknumberdisp:TextField;
      
      public var quantity:TextField;
      
      public var costdisp:TextField;
      
      public var missilecostdisp:TextField;
      
      public function hardwaremissileinformationbutton()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      internal function frame1() : *
      {
         stop();
      }
   }
}
