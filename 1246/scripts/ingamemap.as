package
{
   import flash.display.MovieClip;
   import flash.display.SimpleButton;
   import flash.events.MouseEvent;
   import flash.text.TextField;
   import flash.utils.getDefinitionByName;
   
   public dynamic class ingamemap extends MovieClip
   {
       
      
      public var gridsize_x;
      
      public var gridsize_y;
      
      public var sectorinformation;
      
      public var rangetotarget:TextField;
      
      public var MapIsVisible;
      
      public var destination:TextField;
      
      public var MapGridLabelImage:Class;
      
      public var SectorItemImages;
      
      public var MapGridXLabels;
      
      public var lastTeamPositionSent;
      
      public var ysecwidth;
      
      public var gridxindent;
      
      public var mysocket;
      
      public var playersdestination;
      
      public var ingamexwidthofasec;
      
      public var xsecwidth;
      
      public var PlayerLocater;
      
      public var DesiredHeading;
      
      public var noofysectors;
      
      public var settingjumplocation;
      
      public var noofxsectors;
      
      public var xratio;
      
      public var yratio;
      
      public var informationtodisplay:TextField;
      
      public var StarBaseImages;
      
      public var SectorSquare;
      
      public var sectormapitems;
      
      public var xrange;
      
      public var gamemapscripting:MovieClip;
      
      public var MapGridYLabels;
      
      public var starbaselocation;
      
      public var lastTeamPositionSentinterval;
      
      public var SectorSquareImage:Class;
      
      public var SquadBaseImages;
      
      public var yrange;
      
      public var gridyindent;
      
      public var minimizeButton:SimpleButton;
      
      public var ingameywidthofasec;
      
      public var PlayerLocaterImage:Class;
      
      public function ingamemap()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function func_setupthewaypoints() : *
      {
         var _loc1_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:Class = null;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         this.StarBaseImages = new Array();
         this.SectorItemImages = new Array();
         this.SquadBaseImages = new Array();
         var _loc2_:Class = getDefinitionByName("ingamemapstarbasemarker") as Class;
         _loc3_ = 0;
         while(_loc3_ < this.starbaselocation.length)
         {
            this.StarBaseImages[_loc3_] = new _loc2_() as MovieClip;
            this.StarBaseImages[_loc3_].x = Number(this.starbaselocation[_loc3_][1]) * this.xratio + this.gridxindent + this.xsecwidth;
            this.StarBaseImages[_loc3_].y = Number(this.starbaselocation[_loc3_][2]) * this.yratio + this.gridyindent + this.ysecwidth;
            this.addChild(this.StarBaseImages[_loc3_]);
            if(this.starbaselocation[_loc3_][0].substr(0,2) == "PL")
            {
               _loc4_ = getDefinitionByName("planettype" + this.starbaselocation[_loc3_][3] + "image") as Class;
               this.StarBaseImages[_loc3_].label.text = this.starbaselocation[_loc3_][0].substr(2);
            }
            else
            {
               _loc4_ = getDefinitionByName("starbasetype" + this.starbaselocation[_loc3_][3] + "image") as Class;
               this.StarBaseImages[_loc3_].label.text = this.starbaselocation[_loc3_][0];
            }
            (_loc5_ = new _loc4_() as MovieClip).width = this.xsecwidth * 0.75;
            _loc5_.height = this.ysecwidth * 0.8;
            this.StarBaseImages[_loc3_].addChild(_loc5_);
            _loc6_ = 0;
            while(_loc6_ < this.sectormapitems.length)
            {
               if(this.sectormapitems[_loc6_] != null)
               {
                  if(this.sectormapitems[_loc6_][0] == this.starbaselocation[_loc3_][0])
                  {
                     this.StarBaseImages[_loc3_].name = _loc6_;
                     this.StarBaseImages[_loc3_].addEventListener(MouseEvent.MOUSE_DOWN,this.func_SetHeadingDirection);
                  }
               }
               _loc6_++;
            }
            _loc3_++;
         }
         _loc2_ = getDefinitionByName("ingamemapnavpoint") as Class;
         _loc3_ = 0;
         while(_loc3_ < this.sectormapitems.length)
         {
            if(this.sectormapitems[_loc3_] != null)
            {
               if(this.sectormapitems[_loc3_][0].substr(0,2) == "TB")
               {
                  _loc1_ = this.SectorItemImages.length;
                  this.SectorItemImages[_loc1_] = new _loc2_() as MovieClip;
                  this.SectorItemImages[_loc1_].x = Number(this.sectormapitems[_loc3_][1]) * this.xratio + this.gridxindent + this.xsecwidth;
                  this.SectorItemImages[_loc1_].y = Number(this.sectormapitems[_loc3_][2]) * this.yratio + this.gridyindent + this.ysecwidth;
                  this.SectorItemImages[_loc1_].label.text = this.sectormapitems[_loc3_][0];
                  this.addChild(this.SectorItemImages[_loc1_]);
                  this.SectorItemImages[_loc1_].name = _loc3_;
                  this.SectorItemImages[_loc1_].addEventListener(MouseEvent.MOUSE_DOWN,this.func_SetHeadingDirection);
               }
            }
            _loc3_++;
         }
         _loc2_ = getDefinitionByName("ingamemapnavpoint") as Class;
         _loc3_ = 0;
         while(_loc3_ < this.sectormapitems.length)
         {
            if(this.sectormapitems[_loc3_] != null)
            {
               if(this.sectormapitems[_loc3_][0].substr(0,2) == "NP")
               {
                  _loc1_ = this.SectorItemImages.length;
                  this.SectorItemImages[_loc1_] = new _loc2_() as MovieClip;
                  this.SectorItemImages[_loc1_].x = Number(this.sectormapitems[_loc3_][1]) * this.xratio + this.gridxindent + this.xsecwidth;
                  this.SectorItemImages[_loc1_].y = Number(this.sectormapitems[_loc3_][2]) * this.yratio + this.gridyindent + this.ysecwidth;
                  this.SectorItemImages[_loc1_].label.text = "Nav " + this.sectormapitems[_loc3_][0];
                  this.addChild(this.SectorItemImages[_loc1_]);
                  this.SectorItemImages[_loc1_].name = _loc3_;
                  this.SectorItemImages[_loc1_].addEventListener(MouseEvent.MOUSE_DOWN,this.func_SetHeadingDirection);
               }
            }
            _loc3_++;
         }
      }
      
      public function func_SetHeadingDirection(param1:MouseEvent) : void
      {
         this.DesiredHeading = Number(param1.target.parent.name);
      }
      
      public function func_drawSquadbases() : *
      {
      }
      
      public function func_initMap() : *
      {
         trace(this.sectorinformation[0][0]);
         this.noofxsectors = this.sectorinformation[0][0];
         this.noofysectors = this.sectorinformation[0][1];
         this.xsecwidth = this.gridsize_x / (this.noofxsectors + 1);
         this.ysecwidth = this.gridsize_y / (this.noofysectors + 1);
         this.ingamexwidthofasec = this.sectorinformation[1][0];
         this.ingameywidthofasec = this.sectorinformation[1][1];
         this.xratio = this.xsecwidth / this.ingamexwidthofasec;
         this.yratio = this.ysecwidth / this.ingameywidthofasec;
         this.drawgrid();
         this.func_setupthewaypoints();
         this.addChild(this.PlayerLocater);
      }
      
      public function func_PlayerMapLocation(param1:*, param2:*, param3:*) : *
      {
         this.PlayerLocater.x = param1 * this.xratio + this.gridxindent + this.xsecwidth;
         this.PlayerLocater.y = param2 * this.yratio + this.gridyindent + this.ysecwidth;
         this.PlayerLocater.rotation = param3;
         return this.DesiredHeading;
      }
      
      internal function frame1() : *
      {
         this.MapIsVisible = false;
         this.StarBaseImages = new Array();
         this.SectorItemImages = new Array();
         this.SquadBaseImages = new Array();
         this.DesiredHeading = -1;
         this.informationtodisplay.text = "N/A";
         this.destination.text = "None";
         this.rangetotarget.text = "RANGE: No Dest.";
         this.MapGridLabelImage = getDefinitionByName("ingamemapgridlabel") as Class;
         this.MapGridXLabels = new Array();
         this.MapGridYLabels = new Array();
         this.SectorSquareImage = getDefinitionByName("ingamemapsecsquare") as Class;
         this.PlayerLocaterImage = getDefinitionByName("ingamemapplayership") as Class;
         this.PlayerLocater = new this.PlayerLocaterImage() as MovieClip;
         this.addChild(this.PlayerLocater);
         this.SectorSquare = new Array();
         this.lastTeamPositionSent = 0;
         this.lastTeamPositionSentinterval = 1500;
         this.settingjumplocation = false;
         this.gridxindent = 50;
         this.gridyindent = 70;
         this.gridsize_x = 500;
         this.gridsize_y = 375;
         this.xrange = 0;
         this.yrange = 0;
         stop();
      }
      
      public function drawgrid() : *
      {
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc1_:* = 0;
         while(_loc1_ <= this.noofxsectors)
         {
            _loc3_ = this.xsecwidth * _loc1_ + this.gridxindent + this.xsecwidth / 2;
            _loc4_ = this.gridyindent - 10;
            this.MapGridXLabels[_loc1_] = new this.MapGridLabelImage() as MovieClip;
            this.MapGridXLabels[_loc1_].x = _loc3_;
            this.MapGridXLabels[_loc1_].y = _loc4_;
            this.addChild(this.MapGridXLabels[_loc1_]);
            this.MapGridXLabels[_loc1_].GridLabel.text = _loc1_ + 1;
            _loc1_++;
         }
         var _loc2_:* = 0;
         while(_loc2_ <= this.noofxsectors)
         {
            _loc4_ = this.ysecwidth * _loc2_ + this.gridyindent + this.ysecwidth / 2;
            _loc3_ = this.gridxindent - 20;
            this.MapGridYLabels[_loc2_] = new this.MapGridLabelImage() as MovieClip;
            this.MapGridYLabels[_loc2_].x = _loc3_;
            this.MapGridYLabels[_loc2_].y = _loc4_;
            this.addChild(this.MapGridYLabels[_loc2_]);
            this.MapGridYLabels[_loc2_].GridLabel.text = _loc2_ + 1;
            _loc2_++;
         }
         _loc1_ = 0;
         while(_loc1_ <= this.noofxsectors)
         {
            _loc3_ = this.xsecwidth * _loc1_ + this.gridxindent;
            this.SectorSquare[_loc1_] = new Array();
            _loc2_ = 0;
            while(_loc2_ <= this.noofysectors)
            {
               _loc4_ = this.ysecwidth * _loc2_ + this.gridyindent;
               this.SectorSquare[_loc1_][_loc2_] = new this.SectorSquareImage() as MovieClip;
               this.SectorSquare[_loc1_][_loc2_].x = _loc3_;
               this.SectorSquare[_loc1_][_loc2_].y = _loc4_;
               this.SectorSquare[_loc1_][_loc2_].width = this.xsecwidth;
               this.SectorSquare[_loc1_][_loc2_].height = this.ysecwidth;
               this.addChild(this.SectorSquare[_loc1_][_loc2_]);
               _loc2_++;
            }
            _loc1_++;
         }
      }
   }
}
