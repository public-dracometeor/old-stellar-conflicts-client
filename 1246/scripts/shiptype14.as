package
{
   import flash.display.MovieClip;
   
   public dynamic class shiptype14 extends MovieClip
   {
       
      
      public var Eng1:MovieClip;
      
      public var Eng2:MovieClip;
      
      public function shiptype14()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      internal function frame1() : *
      {
      }
      
      public function setSpeedRatio(param1:*) : *
      {
         this.Eng1.setSpeedRatio(param1);
         this.Eng2.setSpeedRatio(param1);
      }
   }
}
