package
{
   import flash.display.MovieClip;
   import flash.text.TextField;
   import flash.utils.getTimer;
   
   public dynamic class aimessages2 extends MovieClip
   {
       
      
      public var timetoclear;
      
      public var ainame:TextField;
      
      public var timestarted;
      
      public var aipicture:MovieClip;
      
      public var timetokeepit;
      
      public var aimessage:TextField;
      
      public function aimessages2()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2,3,this.frame4,4,this.frame5);
      }
      
      internal function frame1() : *
      {
         stop();
      }
      
      internal function frame4() : *
      {
         if(this.timetoclear > getTimer())
         {
            gotoAndPlay(3);
         }
      }
      
      internal function frame5() : *
      {
      }
      
      internal function frame2() : *
      {
         this.timestarted = getTimer();
         this.timetokeepit = 15000;
         this.timetoclear = this.timestarted + this.timetokeepit;
      }
   }
}
