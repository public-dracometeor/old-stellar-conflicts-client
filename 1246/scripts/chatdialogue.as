package
{
   import flash.display.MovieClip;
   import flash.events.TimerEvent;
   import flash.text.TextField;
   import flash.utils.Timer;
   
   public dynamic class chatdialogue extends MovieClip
   {
       
      
      public var ChatterTimer:Timer;
      
      public var miscOutPut:TextField;
      
      public var MiscChatt;
      
      public var chatInput:TextField;
      
      public var textdisplay:TextField;
      
      public var textoutline:MovieClip;
      
      public function chatdialogue()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function func_displayChatter() : *
      {
         var _loc1_:* = "";
         _loc1_ += this.MiscChatt[0] + "\r";
         _loc1_ += this.MiscChatt[1] + "\r";
         _loc1_ += this.MiscChatt[2] + "\r";
         _loc1_ += this.MiscChatt[3] + "\r";
         this.miscOutPut.text = _loc1_;
         this.ChatterTimer.stop();
         this.ChatterTimer.start();
      }
      
      internal function frame1() : *
      {
         this.miscOutPut.text = "";
         this.MiscChatt = new Array();
         this.MiscChatt[0] = "";
         this.MiscChatt[1] = "";
         this.MiscChatt[2] = "";
         this.MiscChatt[3] = "";
         this.ChatterTimer = new Timer(5000);
         this.ChatterTimer.addEventListener(TimerEvent.TIMER,this.ChatterTimerHandler);
         stop();
      }
      
      public function ChatterTimerHandler(param1:TimerEvent) : void
      {
         this.func_addMiscChatter("");
      }
      
      public function func_removekeys(param1:*) : *
      {
         var _loc3_:* = undefined;
         var _loc2_:* = 0;
         while(_loc2_ < param1.length)
         {
            if(param1.charAt(_loc2_) == "`" || param1.charAt(_loc2_) == "~")
            {
               _loc3_ = " ";
               param1 = param1.substr(0,_loc2_) + _loc3_ + param1.substr(_loc2_ + 1);
            }
            _loc2_++;
         }
         return param1;
      }
      
      public function func_addMiscChatter(param1:*) : *
      {
         this.MiscChatt[0] = this.MiscChatt[1];
         this.MiscChatt[1] = this.MiscChatt[2];
         this.MiscChatt[2] = this.MiscChatt[3];
         this.MiscChatt[3] = param1;
         this.func_displayChatter();
      }
   }
}
