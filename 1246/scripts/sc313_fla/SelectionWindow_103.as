package sc313_fla
{
   import flash.display.MovieClip;
   import flash.events.Event;
   import flash.events.MouseEvent;
   
   public dynamic class SelectionWindow_103 extends MovieClip
   {
       
      
      public var sliderscale;
      
      public var initialSlideBarY;
      
      public var MaxBarY;
      
      public var leftSideSlide;
      
      public var slider:MovieClip;
      
      public var risghtSideSLide;
      
      public var SlidingBar;
      
      public var screen:MovieClip;
      
      public var mouseXoffset;
      
      public var MinBarX;
      
      public var screenWidth;
      
      public var originalSLiderWidth;
      
      public var SliderToScreenDIfference;
      
      public function SelectionWindow_103()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function StartDrag(param1:MouseEvent) : void
      {
         this.SlidingBar = true;
         this.mouseXoffset = mouseX - this.leftSideSlide - (Number(this.slider.sliderBar.x) - this.leftSideSlide);
      }
      
      public function func_addItem(param1:*) : *
      {
         var _loc2_:* = this.screen.width;
         this.screen.addChild(param1);
         param1.x = _loc2_;
         this.func_calcSlideBarScale();
         this.screen.x = 0;
      }
      
      internal function frame1() : *
      {
         this.screenWidth = 545;
         this.leftSideSlide = 15;
         this.risghtSideSLide = 550 - 15;
         this.slider.sliderBar.scaleX = 1;
         this.sliderscale = 1;
         this.SlidingBar = true;
         this.initialSlideBarY = this.slider.sliderBar.y;
         this.originalSLiderWidth = this.slider.sliderBar.width;
         this.mouseXoffset = 0;
         this.SliderToScreenDIfference = this.originalSLiderWidth / this.screenWidth;
         this.MinBarX = this.leftSideSlide;
         this.func_calcSlideBarScale();
         this.slider.sliderBar.addEventListener(MouseEvent.MOUSE_DOWN,this.StartDrag);
         stage.addEventListener(MouseEvent.MOUSE_UP,this.StopSliderDrag);
         addEventListener(Event.ENTER_FRAME,this.func_MoveSLider);
      }
      
      public function func_clearAllItems() : *
      {
         while(this.screen.numChildren > 0)
         {
            this.screen.removeChildAt(0);
         }
         this.func_calcSlideBarScale();
      }
      
      public function func_calcSlideBarScale() : *
      {
         if(this.screen.width > 0)
         {
            this.sliderscale = this.screenWidth / this.screen.width;
         }
         else
         {
            this.sliderscale = 1;
            this.slider.visible = false;
         }
         if(this.sliderscale > 1)
         {
            this.sliderscale = 1;
            this.slider.visible = false;
         }
         if(this.sliderscale < 1)
         {
            this.slider.sliderBar.scaleX = this.sliderscale;
            this.slider.sliderBar.x = this.leftSideSlide;
            this.MaxBarY = this.risghtSideSLide - this.originalSLiderWidth * this.sliderscale;
            this.slider.visible = true;
         }
         else
         {
            this.slider.alpha = 50;
         }
      }
      
      public function func_MoveSLider(param1:Event) : *
      {
         if(this.SlidingBar)
         {
            this.slider.sliderBar.y = this.initialSlideBarY;
            this.slider.sliderBar.x = mouseX - this.mouseXoffset;
            if(this.slider.sliderBar.x < this.leftSideSlide)
            {
               this.slider.sliderBar.x = this.leftSideSlide;
            }
            else if(this.slider.sliderBar.x > this.MaxBarY)
            {
               this.slider.sliderBar.x = this.MaxBarY;
            }
            this.screen.x = -(Number(this.slider.sliderBar.x) - this.leftSideSlide) / (this.sliderscale * this.SliderToScreenDIfference) + 1;
         }
      }
      
      public function StopSliderDrag(param1:MouseEvent) : void
      {
         this.SlidingBar = false;
      }
   }
}
