package sc313_fla
{
   import flash.display.MovieClip;
   import flash.display.SimpleButton;
   import flash.events.MouseEvent;
   import flash.text.TextField;
   
   public dynamic class onlineplayersListing_2 extends MovieClip
   {
       
      
      public var changeWindow:MovieClip;
      
      public var onlinePlayersListing:TextField;
      
      public var playershipstatus;
      
      public var minimizeButton:SimpleButton;
      
      public var onlineListDetailsone:TextField;
      
      public var onlineListDetailstwo:TextField;
      
      public var teamNo:TextField;
      
      public var mysocket;
      
      public function onlineplayersListing_2()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2);
      }
      
      public function func_ChangeToTeam(param1:MouseEvent) : void
      {
         var _loc3_:* = undefined;
         var _loc2_:* = Number(this.teamNo.text);
         if(!isNaN(_loc2_))
         {
            _loc2_ = Math.round(_loc2_);
            if(_loc2_ < 1)
            {
               _loc2_ = 0;
            }
            _loc3_ = "TC`" + this.playershipstatus[3][0] + "`TM`" + _loc2_ + "`~";
            this.mysocket.send(_loc3_);
            this.teamNo.text = "Change Sent";
         }
      }
      
      internal function frame1() : *
      {
         stop();
      }
      
      public function func_ChangeToSoloPlayer(param1:MouseEvent) : void
      {
         var _loc2_:* = "TC`" + this.playershipstatus[3][0] + "`TM`-1`~";
         this.mysocket.send(_loc2_);
         this.teamNo.text = "Change Sent";
      }
      
      internal function frame2() : *
      {
         this.changeWindow.changeTeam.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ChangeToTeam);
         this.changeWindow.SoloPlayer.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ChangeToSoloPlayer);
         stop();
      }
   }
}
