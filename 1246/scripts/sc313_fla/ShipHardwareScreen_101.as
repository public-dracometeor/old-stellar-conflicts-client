package sc313_fla
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import flash.utils.getDefinitionByName;
   
   public dynamic class ShipHardwareScreen_101 extends MovieClip
   {
       
      
      public var buyenergygen:MovieClip;
      
      public var missiles:MovieClip;
      
      public var buyweapon:MovieClip;
      
      public var hardPointBox:Class;
      
      public var shieldgenerators;
      
      public var TurrethardPointHolders;
      
      public var informationWindow:MovieClip;
      
      public var selectedSpot;
      
      public var EnergyGenPointHolders;
      
      public var shipstopgun;
      
      public var EnergyGenTypeBeingBought;
      
      public var energycap:MovieClip;
      
      public var MissileDataHolders;
      
      public var BluePrint:Class;
      
      public var currentvalueofgenerator;
      
      public var numberOfTurrets;
      
      public var numberOfMissilepoints;
      
      public var specialshipitems;
      
      public var q;
      
      public var guntype;
      
      public var buyenergycap:MovieClip;
      
      public var specialsDisplay:MovieClip;
      
      public var numberOfEnergyCapPoints;
      
      public var energygenerators;
      
      public var EnergyGenDataHolders;
      
      public var sellingTurretPointBox;
      
      public var availableitems;
      
      public var currentlySelectedShieldGen;
      
      public var numberOfHardpoints;
      
      public var MissilePointHolders;
      
      public var TurretsDisplayed;
      
      public var currentlyDraggingGun;
      
      public var GunsDisplayed;
      
      public var playershipstatus;
      
      public var EnergyCapDataHolders;
      
      public var turretCostModifier;
      
      public var missilepurchasewindow:MovieClip;
      
      public var specials:MovieClip;
      
      public var missilelocationmultiplier;
      
      public var ShieldGenTypeBeingBought;
      
      public var missileTypeBeingBought;
      
      public var GunHolder;
      
      public var TurretsHolder;
      
      public var SpecialsDisplayed;
      
      public var hardPointHolders;
      
      public var missile;
      
      public var gunBeingBought;
      
      public var guns:MovieClip;
      
      public var MissilesDisplayed;
      
      public var EnergyCapsDisplayed;
      
      public var EnergyGensDisplayed;
      
      public var ShieldGenPointHolders;
      
      public var shieldgen:MovieClip;
      
      public var playershiptype;
      
      public var TurretBeingBought;
      
      public var SpecialListHolder;
      
      public var shiptype;
      
      public var energycapacitors;
      
      public var buyshieldgen:MovieClip;
      
      public var buyspecial:MovieClip;
      
      public var turrets:MovieClip;
      
      public var currentlyDraggingTurret;
      
      public var EnergyCapTypeBeingBought;
      
      public var currentlySeelctedMIssileBank;
      
      public var currentvalueofEnergycap;
      
      public var sellingHardPointBox;
      
      public var numberOfEnergyGenPoints;
      
      public var maxBumberOfEnergySpecials;
      
      public var DisplayScreen:MovieClip;
      
      public var ShieldGenDataHolders;
      
      public var EnergyCapPointHolders;
      
      public var buyingwindowaction;
      
      public var energygen:MovieClip;
      
      public var DisplayBox:Class;
      
      public var numberOfShieldGenPoints;
      
      public var shipstopgunTurret;
      
      public var shipblueprint;
      
      public var exit_but:MovieClip;
      
      public var currentvalueofEnergygenerator;
      
      public var sellweapon:MovieClip;
      
      public var ShieldGensDisplayed;
      
      public function ShipHardwareScreen_101()
      {
         super();
         addFrameScript(0,this.frame1,7,this.frame8,18,this.frame19,28,this.frame29,44,this.frame45,58,this.frame59,76,this.frame77,94,this.frame95);
      }
      
      public function func_ResetButtonsToOff() : *
      {
         this.guns.gotoAndStop(2);
         this.missiles.gotoAndStop(2);
         this.shieldgen.gotoAndStop(2);
         this.energygen.gotoAndStop(2);
         this.energycap.gotoAndStop(2);
         this.specials.gotoAndStop(2);
         this.exit_but.gotoAndStop(2);
         this.turrets.gotoAndStop(2);
         try
         {
            stage.removeEventListener(MouseEvent.MOUSE_UP,this.func_SelectedHardpointReleased);
         }
         catch(error:Error)
         {
         }
      }
      
      public function func_drawShieldGenDisplays() : *
      {
         this.buyshieldgen.visible = false;
         this.DisplayScreen.func_clearAllItems();
         this.func_InitShieldGensDisplay();
         this.func_ClearBlueBrint();
         this.func_drawShieldGenBrackets();
         this.func_drawShieldGenImages();
      }
      
      public function func_drawEnergyCapDisplays() : *
      {
         this.buyenergycap.visible = false;
         this.DisplayScreen.func_clearAllItems();
         this.func_InitEnergyCapsDisplay();
         this.func_ClearBlueBrint();
         this.func_drawEnergyCapBrackets();
         this.func_drawEnergyCapImages();
      }
      
      public function func_buyenergycap(param1:MouseEvent) : void
      {
         this.EnergyCapTypeBeingBought = Number(param1.target.parent.name);
         var _loc2_:* = Number(this.energycapacitors[this.EnergyCapTypeBeingBought][2]) - this.currentvalueofEnergycap;
         if(_loc2_ <= this.playershipstatus[3][1])
         {
            this.buyenergycap.question.text = "Buy: " + this.energycapacitors[this.EnergyCapTypeBeingBought][1] + " \r";
            this.buyenergycap.question.text += "For: " + _loc2_;
            this.buyenergycap.visible = true;
         }
         else
         {
            this.buyenergycap.visible = false;
         }
      }
      
      public function func_SelectedHardpointDrag(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(param1.target.parent.name);
         if(!isNaN(_loc2_))
         {
            this.currentlyDraggingGun = _loc2_;
            this.GunHolder[_loc2_].startDrag(true);
         }
      }
      
      public function func_acceptEnergyGenPurchase(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(this.energygenerators[this.EnergyGenTypeBeingBought][2]) - this.currentvalueofEnergygenerator;
         this.playershipstatus[3][1] -= _loc2_;
         this.playershipstatus[1][0] = this.EnergyGenTypeBeingBought;
         this.func_drawEnergyGenDisplays();
      }
      
      public function func_cancelSale(param1:MouseEvent) : void
      {
         this.func_drawGunDisplays();
      }
      
      public function func_DisplayBankInfo(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         this.missilepurchasewindow.visible = false;
         if(param1 < 0)
         {
            trace("1");
            this.informationWindow.information.text = "Select a Missile Bank";
         }
         else
         {
            trace("2");
            _loc2_ = "Selected Missile Bank: " + param1 + "\r";
            _loc3_ = "";
            _loc4_ = 0;
            _loc5_ = 0;
            while(_loc5_ < this.missile.length)
            {
               if(this.playershipstatus[7][param1][4][_loc5_] > 0)
               {
                  _loc3_ += this.missile[_loc5_][6] + "  (" + this.playershipstatus[7][param1][4][_loc5_] + ")\r";
                  _loc4_ += this.playershipstatus[7][param1][4][_loc5_];
               }
               _loc5_++;
            }
            _loc6_ = (_loc6_ = (_loc6_ = _loc2_) + ("No. of Missiles (" + _loc4_ + "/" + this.playershipstatus[7][param1][5] + ")\r\r")) + ("Current Missiles Loaded\r" + _loc3_);
            this.informationWindow.information.text = _loc6_;
         }
      }
      
      internal function frame19() : *
      {
         this.turretCostModifier = 2;
         this.DisplayScreen.func_clearAllItems();
         this.buyweapon.visible = false;
         this.sellweapon.visible = false;
         this.func_ClearBlueBrint();
         this.TurretsDisplayed = new Array();
         this.TurretsHolder = new Array();
         this.shipstopgunTurret = this.shiptype[this.playershiptype][6][3] + 1;
         this.currentlyDraggingTurret = -1;
         this.func_InitTurretsDisplay();
         this.numberOfTurrets = this.shiptype[this.playershiptype][5].length;
         this.TurrethardPointHolders = new Array();
         this.sellingTurretPointBox = new this.hardPointBox() as MovieClip;
         this.sellingTurretPointBox.hardpointname.text = "Sell Weapon";
         this.func_drawTurretDisplays();
         stage.addEventListener(MouseEvent.MOUSE_UP,this.func_SelectedTurretReleased);
         this.sellweapon.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_acceptTurretSale);
         this.sellweapon.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelTurretSale);
         this.TurretBeingBought = -1;
         this.buyweapon.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_acceptTurretPurchase);
         this.buyweapon.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelTurretPurchase);
      }
      
      public function func_MissilesSelectionMade(param1:MouseEvent) : void
      {
         this.func_ResetButtonsToOff();
         this.missiles.gotoAndStop(1);
         gotoAndStop("missiles");
      }
      
      public function func_TurretsHolderImages() : *
      {
         var _loc3_:Class = null;
         this.TurretsHolder = new Array();
         var _loc1_:* = this.shiptype[this.playershiptype][3][4];
         var _loc2_:* = 0;
         while(_loc2_ < this.numberOfTurrets)
         {
            if(!isNaN(this.playershipstatus[8][_loc2_][0]))
            {
               _loc3_ = getDefinitionByName("hardwarescreengunbracketguntype" + this.playershipstatus[8][_loc2_][0]) as Class;
            }
            else
            {
               _loc3_ = getDefinitionByName("hardwarescreengunbracketguntypenone") as Class;
            }
            this.TurretsHolder[_loc2_] = new _loc3_() as MovieClip;
            this.TurretsHolder[_loc2_].x = Number(this.shiptype[this.playershiptype][5][_loc2_][0]) * _loc1_;
            this.TurretsHolder[_loc2_].y = Number(this.shiptype[this.playershiptype][5][_loc2_][1]) * _loc1_;
            this.TurretsHolder[_loc2_].scaleX = 0.75;
            this.TurretsHolder[_loc2_].scaleY = 0.75;
            if(!isNaN(this.playershipstatus[8][_loc2_][0]))
            {
               this.TurretsHolder[_loc2_].itemname.text = this.guntype[this.playershipstatus[8][_loc2_][0]][6];
            }
            this.TurretsHolder[_loc2_].name = _loc2_;
            this.shipblueprint.addChild(this.TurretsHolder[_loc2_]);
            _loc2_++;
         }
      }
      
      public function func_drawEnergyGenImages() : *
      {
         var _loc2_:Class = null;
         this.EnergyGenDataHolders = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.numberOfEnergyGenPoints)
         {
            _loc2_ = getDefinitionByName("hardwarescreengunbracketenergygen0") as Class;
            this.EnergyGenDataHolders[_loc1_] = new _loc2_() as MovieClip;
            this.EnergyGenDataHolders[_loc1_].x = 0;
            this.EnergyGenDataHolders[_loc1_].y = 0;
            this.EnergyGenDataHolders[_loc1_].scaleX = 0.75;
            this.EnergyGenDataHolders[_loc1_].scaleY = 0.75;
            this.EnergyGenDataHolders[_loc1_].itemname.text = this.energygenerators[this.playershipstatus[1][0]][1];
            this.EnergyGenDataHolders[_loc1_].name = _loc1_;
            this.shipblueprint.addChild(this.EnergyGenDataHolders[_loc1_]);
            _loc1_++;
         }
      }
      
      public function func_InitTurretsDisplay() : *
      {
         var _loc2_:* = undefined;
         var _loc1_:* = 0;
         while(_loc1_ < this.shipstopgunTurret)
         {
            if(this.guntype[_loc1_] != null)
            {
               _loc2_ = _loc1_;
               this.TurretsDisplayed[_loc1_] = new Object();
               this.TurretsDisplayed[_loc1_].gunNumber = _loc2_;
               this.TurretsDisplayed[_loc1_].cost = Number(this.guntype[_loc2_][5]) * this.turretCostModifier;
               this.TurretsDisplayed[_loc1_].gunName = this.guntype[_loc2_][6];
               this.TurretsDisplayed[_loc1_].DisplayBox = new this.DisplayBox() as MovieClip;
               this.TurretsDisplayed[_loc1_].DisplayBox.itemname.text = this.guntype[_loc2_][6];
               this.TurretsDisplayed[_loc1_].DisplayBox.itemcost.text = "Cost:" + Number(this.guntype[_loc2_][5]) * this.turretCostModifier;
               this.TurretsDisplayed[_loc1_].DisplayBox.itemspecs.text = "Shld/Struct/Pierce\r  ";
               this.TurretsDisplayed[_loc1_].DisplayBox.itemspecs.text += this.guntype[_loc2_][4] + " / " + this.guntype[_loc2_][7] + " / " + this.guntype[_loc2_][8] + " \r";
               this.TurretsDisplayed[_loc1_].DisplayBox.itemspecs.text += "Energy Drain: " + this.guntype[_loc2_][3] + " \r";
               this.TurretsDisplayed[_loc1_].DisplayBox.itemspecs.text += "Life Time: " + this.guntype[_loc2_][1] + "/s \r";
               this.TurretsDisplayed[_loc1_].DisplayBox.itemspecs.text += "Rel:" + this.guntype[_loc2_][2] + "/s  \r";
               this.TurretsDisplayed[_loc1_].DisplayBox.itemspecs.text += "Spd:" + this.guntype[_loc2_][0] + "/s ";
               this.TurretsDisplayed[_loc1_].DisplayBox.name = _loc2_;
               this.TurretsDisplayed[_loc1_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_BuyNewlTurret);
               this.DisplayScreen.func_addItem(this.TurretsDisplayed[_loc1_].DisplayBox);
            }
            _loc1_++;
         }
      }
      
      public function func_InitEnergyCapsDisplay() : *
      {
         var _loc3_:* = undefined;
         this.currentvalueofEnergycap = Math.round(Number(this.energycapacitors[this.playershipstatus[1][5]][2]) / 2);
         this.EnergyCapsDisplayed = new Array();
         var _loc1_:* = 0;
         var _loc2_:* = this.shiptype[this.playershiptype][6][1] + 1;
         while(_loc1_ < _loc2_)
         {
            _loc3_ = _loc1_;
            this.EnergyCapsDisplayed[_loc1_] = new Object();
            this.EnergyCapsDisplayed[_loc1_].CapNumber = _loc3_;
            this.EnergyCapsDisplayed[_loc1_].DisplayBox = new this.DisplayBox() as MovieClip;
            this.EnergyCapsDisplayed[_loc1_].DisplayBox.itemname.text = this.energycapacitors[_loc3_][1];
            if(this.playershipstatus[1][5] == _loc3_)
            {
               this.EnergyCapsDisplayed[_loc1_].DisplayBox.itemcost.text = "Current";
            }
            else
            {
               this.EnergyCapsDisplayed[_loc1_].DisplayBox.itemcost.text = "Cost:" + (Number(this.energycapacitors[_loc3_][2]) - this.currentvalueofEnergycap);
               this.EnergyCapsDisplayed[_loc1_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_buyenergycap);
            }
            this.EnergyCapsDisplayed[_loc1_].DisplayBox.name = _loc3_;
            this.EnergyCapsDisplayed[_loc1_].DisplayBox.itemspecs.text = "Max Charge: " + this.energycapacitors[_loc3_][0] + " \r";
            this.DisplayScreen.func_addItem(this.EnergyCapsDisplayed[_loc1_].DisplayBox);
            _loc1_++;
         }
      }
      
      public function func_InitShieldGensDisplay() : *
      {
         var _loc3_:* = undefined;
         this.currentvalueofgenerator = Math.round(Number(this.shieldgenerators[this.playershipstatus[2][0]][5]) / 2);
         this.ShieldGensDisplayed = new Array();
         var _loc1_:* = 0;
         var _loc2_:* = this.shiptype[this.playershiptype][6][0] + 1;
         while(_loc1_ < _loc2_)
         {
            _loc3_ = _loc1_;
            this.ShieldGensDisplayed[_loc1_] = new Object();
            this.ShieldGensDisplayed[_loc1_].genNumber = _loc3_;
            this.ShieldGensDisplayed[_loc1_].DisplayBox = new this.DisplayBox() as MovieClip;
            this.ShieldGensDisplayed[_loc1_].DisplayBox.itemname.text = this.shieldgenerators[_loc3_][4];
            if(this.playershipstatus[2][0] == _loc3_)
            {
               this.ShieldGensDisplayed[_loc1_].DisplayBox.itemcost.text = "Current";
            }
            else
            {
               this.ShieldGensDisplayed[_loc1_].DisplayBox.itemcost.text = "Cost:" + (Number(this.shieldgenerators[_loc3_][5]) - this.currentvalueofgenerator);
               this.ShieldGensDisplayed[_loc1_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_BuyShieldGen);
            }
            this.ShieldGensDisplayed[_loc1_].DisplayBox.name = _loc3_;
            this.ShieldGensDisplayed[_loc1_].DisplayBox.itemspecs.text = "Recharge: " + this.shieldgenerators[_loc3_][1] + " \r";
            this.ShieldGensDisplayed[_loc1_].DisplayBox.itemspecs.text += "Energy Drain/second: \r";
            this.ShieldGensDisplayed[_loc1_].DisplayBox.itemspecs.text += "Unit: " + this.shieldgenerators[_loc3_][2] + " \r";
            this.ShieldGensDisplayed[_loc1_].DisplayBox.itemspecs.text += "Full Charge: " + this.shieldgenerators[_loc3_][3] + " \r";
            this.DisplayScreen.func_addItem(this.ShieldGensDisplayed[_loc1_].DisplayBox);
            _loc1_++;
         }
      }
      
      internal function frame1() : *
      {
         this.DisplayBox = getDefinitionByName("hardwareitembox") as Class;
         this.hardPointBox = getDefinitionByName("hardwarescreengunbracket") as Class;
         this.playershiptype = this.playershipstatus[5][0];
         this.turrets.addEventListener(MouseEvent.MOUSE_DOWN,this.func_TurretsSelectionWasMade);
         this.guns.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GunsSelectionMade);
         this.missiles.addEventListener(MouseEvent.MOUSE_DOWN,this.func_MissilesSelectionMade);
         this.shieldgen.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ShieldSelectionMade);
         this.energygen.addEventListener(MouseEvent.MOUSE_DOWN,this.func_EnergyGenSelectionMade);
         this.energycap.addEventListener(MouseEvent.MOUSE_DOWN,this.func_EnergyCapSelectionMade);
         this.specials.addEventListener(MouseEvent.MOUSE_DOWN,this.func_SpecialsSelectionMade);
         this.turrets.label.text = "Turrets";
         this.guns.label.text = "Guns";
         this.missiles.label.text = "Missiles";
         this.shieldgen.label.text = "Shield Generators";
         this.energygen.label.text = "Energy Generators";
         this.energycap.label.text = "Energy Capacitors";
         this.specials.label.text = "Specials";
         this.exit_but.label.text = "Exit";
         this.func_ResetButtonsToOff();
         this.func_GunsSelectionWasMade();
         this.BluePrint = getDefinitionByName("shiptype" + this.playershiptype + "blueprint") as Class;
         this.shipblueprint = new this.BluePrint() as MovieClip;
         addChild(this.shipblueprint);
         this.shipblueprint.x = 450;
         this.shipblueprint.y = 300;
         if(this.shiptype[this.playershiptype][5].length < 1)
         {
            this.turrets.visible = false;
         }
         stop();
      }
      
      public function func_drawEnergyGenBrackets() : *
      {
         this.EnergyGenPointHolders = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.numberOfEnergyGenPoints)
         {
            this.EnergyGenPointHolders[_loc1_] = new this.hardPointBox() as MovieClip;
            this.EnergyGenPointHolders[_loc1_].x = 0;
            this.EnergyGenPointHolders[_loc1_].y = 0;
            this.EnergyGenPointHolders[_loc1_].hardpointname.text = "Generator";
            this.EnergyGenPointHolders[_loc1_].scaleX = 0.75;
            this.EnergyGenPointHolders[_loc1_].scaleY = 0.75;
            this.EnergyGenPointHolders[_loc1_].name = _loc1_;
            this.shipblueprint.addChild(this.EnergyGenPointHolders[_loc1_]);
            _loc1_++;
         }
      }
      
      public function func_acceptShiledGenPurchase(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(this.shieldgenerators[this.ShieldGenTypeBeingBought][5]) - this.currentvalueofgenerator;
         this.playershipstatus[3][1] -= _loc2_;
         this.playershipstatus[2][0] = this.ShieldGenTypeBeingBought;
         this.func_drawShieldGenDisplays();
      }
      
      internal function frame29() : *
      {
         this.DisplayScreen.func_clearAllItems();
         this.func_ClearBlueBrint();
         this.MissilesDisplayed = new Array();
         this.currentlySeelctedMIssileBank = 0;
         this.func_InitMissilesDisplay();
         this.missilelocationmultiplier = this.shiptype[this.playershiptype][3][4];
         this.numberOfMissilepoints = this.shiptype[this.playershiptype][7].length;
         this.MissilePointHolders = new Array();
         this.MissileDataHolders = new Array();
         this.func_drawMissileDisplays();
         this.missileTypeBeingBought = -1;
         this.missilepurchasewindow.yesbutton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_acceptMissilePurchase);
         this.missilepurchasewindow.nobutton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelMissilePurchase);
      }
      
      public function func_GunsSelectionWasMade() : *
      {
         this.func_ResetButtonsToOff();
         this.guns.gotoAndStop(1);
         gotoAndStop("guns");
      }
      
      internal function frame8() : *
      {
         this.DisplayScreen.func_clearAllItems();
         this.buyweapon.visible = false;
         this.sellweapon.visible = false;
         this.func_ClearBlueBrint();
         this.GunsDisplayed = new Array();
         this.GunHolder = new Array();
         this.shipstopgun = this.shiptype[this.playershiptype][6][3] + 1;
         this.currentlyDraggingGun = -1;
         this.func_InitGunsDisplay();
         this.numberOfHardpoints = this.shiptype[this.playershiptype][2].length;
         this.hardPointHolders = new Array();
         this.sellingHardPointBox = new this.hardPointBox() as MovieClip;
         this.sellingHardPointBox.hardpointname.text = "Sell Weapon";
         this.func_drawGunDisplays();
         stage.addEventListener(MouseEvent.MOUSE_UP,this.func_SelectedHardpointReleased);
         this.sellweapon.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_acceptSale);
         this.sellweapon.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelSale);
         this.gunBeingBought = -1;
         this.buyweapon.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_acceptGunPurchase);
         this.buyweapon.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelGunPurchase);
      }
      
      public function func_ClearBlueBrint() : *
      {
         while(this.shipblueprint.numChildren > 1)
         {
            this.shipblueprint.removeChildAt(1);
         }
      }
      
      public function func_acceptTurretSale(param1:MouseEvent) : void
      {
         this.playershipstatus[3][1] += Math.round(Number(this.guntype[this.playershipstatus[8][this.currentlyDraggingTurret][0]][5]) / 2) * this.turretCostModifier;
         this.playershipstatus[8][this.currentlyDraggingTurret][0] = "none";
         this.func_drawTurretDisplays();
      }
      
      public function func_drawShieldGenImages() : *
      {
         var _loc2_:Class = null;
         this.ShieldGenDataHolders = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.numberOfShieldGenPoints)
         {
            _loc2_ = getDefinitionByName("hardwarescreengunbracketshieldtype0") as Class;
            this.ShieldGenDataHolders[_loc1_] = new _loc2_() as MovieClip;
            this.ShieldGenDataHolders[_loc1_].x = 0;
            this.ShieldGenDataHolders[_loc1_].y = 0;
            this.ShieldGenDataHolders[_loc1_].scaleX = 0.75;
            this.ShieldGenDataHolders[_loc1_].scaleY = 0.75;
            this.ShieldGenDataHolders[_loc1_].itemname.text = this.shieldgenerators[this.playershipstatus[2][0]][4];
            this.ShieldGenDataHolders[_loc1_].name = _loc1_;
            this.shipblueprint.addChild(this.ShieldGenDataHolders[_loc1_]);
            _loc1_++;
         }
      }
      
      public function func_acceptEnergyCapPurchase(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(this.energycapacitors[this.EnergyCapTypeBeingBought][2]) - this.currentvalueofEnergycap;
         this.playershipstatus[3][1] -= _loc2_;
         this.playershipstatus[1][5] = this.EnergyCapTypeBeingBought;
         this.func_drawEnergyCapDisplays();
      }
      
      public function func_InitMissilesDisplay() : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc1_:* = 0;
         while(_loc1_ < this.missile.length)
         {
            _loc2_ = _loc1_;
            this.MissilesDisplayed[_loc1_] = new Object();
            this.GunsDisplayed[_loc1_] = new Object();
            this.MissilesDisplayed[_loc1_].missileNumber = _loc2_;
            this.MissilesDisplayed[_loc1_].DisplayBox = new this.DisplayBox() as MovieClip;
            this.MissilesDisplayed[_loc1_].DisplayBox.itemname.text = this.missile[_loc2_][6];
            this.MissilesDisplayed[_loc1_].DisplayBox.itemcost.text = "Cost:" + this.missile[_loc2_][5];
            this.MissilesDisplayed[_loc1_].DisplayBox.name = _loc2_;
            this.MissilesDisplayed[_loc1_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_BuyMissiles);
            if(this.missile[_loc2_][8] == "EMP" || this.missile[_loc2_][8] == "DISRUPTER")
            {
               if(this.missile[_loc2_][8] == "EMP")
               {
                  _loc3_ = "E.M.P.";
               }
               else if(this.missile[_loc2_][8] == "DISRUPTER")
               {
                  _loc3_ = "Disrupter";
               }
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text = "Reload :" + Number(this.missile[_loc2_][3]) / 1000 + "/s \r";
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text += "Life Time: " + Number(this.missile[_loc2_][2]) / 1000 + "/s \r";
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text += "Speed :" + this.missile[_loc2_][0] + "/s \r";
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text += "Duration: " + Math.floor(Number(this.missile[_loc2_][9]) / 1000) + " \r";
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text += "Guidance: " + _loc3_;
            }
            if(this.missile[_loc2_][8] == "DUMBFIRE" || this.missile[_loc2_][8] == "SEEKER")
            {
               if(this.missile[_loc2_][8] == "DUMBFIRE")
               {
                  _loc3_ = "Dumbfire";
               }
               else if(this.missile[_loc2_][8] == "SEEKER")
               {
                  _loc3_ = "Seeker";
               }
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text = "Damage: " + this.missile[_loc2_][4] + " \r";
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text += "Reload :" + Number(this.missile[_loc2_][3]) / 1000 + "/s \r";
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text += "Life Time: " + Number(this.missile[_loc2_][2]) / 1000 + "/s \r";
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text += "Speed :" + this.missile[_loc2_][0] + "/s \r";
               this.MissilesDisplayed[_loc1_].DisplayBox.itemspecs.text += "Guidance: " + _loc3_;
            }
            this.DisplayScreen.func_addItem(this.MissilesDisplayed[_loc1_].DisplayBox);
            _loc1_++;
         }
      }
      
      public function func_SpecialsSelectionMade(param1:MouseEvent) : void
      {
         this.func_ResetButtonsToOff();
         this.specials.gotoAndStop(1);
         gotoAndStop("Specials");
      }
      
      public function func_EnergyCapSelectionMade(param1:MouseEvent) : void
      {
         this.func_ResetButtonsToOff();
         this.energycap.gotoAndStop(1);
         gotoAndStop("EnergyCapacitors");
      }
      
      public function func_SelectedHardpointReleased(param1:MouseEvent) : void
      {
         var TemGunHolder:* = undefined;
         var event:MouseEvent = param1;
         var gunReleasedon:* = "test";
         try
         {
            gunReleasedon = Number(event.target.parent.name);
            if(this.currentlyDraggingGun != -1)
            {
               if(!isNaN(gunReleasedon))
               {
                  trace(gunReleasedon);
                  if(gunReleasedon == -1)
                  {
                     if(!isNaN(this.playershipstatus[0][this.currentlyDraggingGun][0]))
                     {
                        this.sellweapon.visible = true;
                        this.sellweapon.question.text = "Sell: " + this.guntype[this.playershipstatus[0][this.currentlyDraggingGun][0]][6] + " \r" + "For: " + Math.round(Number(this.guntype[this.playershipstatus[0][this.currentlyDraggingGun][0]][5]) / 2);
                        this.GunHolder[this.currentlyDraggingGun].stopDrag();
                     }
                     else
                     {
                        gunReleasedon = "";
                     }
                  }
                  else if(this.currentlyDraggingGun != gunReleasedon)
                  {
                     TemGunHolder = this.playershipstatus[0][this.currentlyDraggingGun];
                     this.playershipstatus[0][this.currentlyDraggingGun] = this.playershipstatus[0][gunReleasedon];
                     this.playershipstatus[0][gunReleasedon] = TemGunHolder;
                  }
               }
            }
            if(gunReleasedon != -1)
            {
               this.func_drawGunDisplays();
            }
         }
         catch(error:Error)
         {
         }
      }
      
      public function func_SelectedTurretDrag(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(param1.target.parent.name);
         if(!isNaN(_loc2_))
         {
            this.currentlyDraggingTurret = _loc2_;
            this.TurretsHolder[_loc2_].startDrag(true);
         }
      }
      
      public function func_drawTurretDisplays() : *
      {
         this.func_ClearBlueBrint();
         this.func_TurretsHolderImages();
         this.func_drawTurretBrackets();
         this.currentlyDraggingTurret = -1;
         this.sellweapon.visible = false;
      }
      
      public function func_cancelGunPurchase(param1:MouseEvent) : void
      {
         this.buyweapon.visible = false;
      }
      
      internal function frame59() : *
      {
         this.EnergyGensDisplayed = new Array();
         this.currentvalueofEnergygenerator = 0;
         this.numberOfEnergyGenPoints = 1;
         this.EnergyGenPointHolders = new Array();
         this.EnergyGenDataHolders = new Array();
         this.func_drawEnergyGenDisplays();
         this.EnergyGenTypeBeingBought = -1;
         this.buyenergygen.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_acceptEnergyGenPurchase);
         this.buyenergygen.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelEnergyGenPurchase);
      }
      
      internal function frame45() : *
      {
         this.ShieldGensDisplayed = new Array();
         this.currentlySelectedShieldGen = 0;
         this.currentvalueofgenerator = 0;
         this.numberOfShieldGenPoints = 1;
         this.ShieldGenPointHolders = new Array();
         this.ShieldGenDataHolders = new Array();
         this.func_drawShieldGenDisplays();
         this.ShieldGenTypeBeingBought = -1;
         this.buyshieldgen.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_acceptShiledGenPurchase);
         this.buyshieldgen.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelShiledGenPurchase);
      }
      
      public function func_acceptMissilePurchase(param1:MouseEvent) : void
      {
         var _loc2_:* = 0;
         var _loc3_:* = Number(this.missilepurchasewindow.quantity.text);
         var _loc4_:* = this.missile[this.missileTypeBeingBought][5];
         trace(_loc3_ + "`" + _loc4_);
         if(!isNaN(_loc3_) && !isNaN(_loc4_))
         {
            trace("RAN");
            this.playershipstatus[3][1] -= _loc4_;
            this.playershipstatus[7][this.currentlySeelctedMIssileBank][4][this.missileTypeBeingBought] += _loc3_;
         }
         this.func_drawMissileDisplays();
      }
      
      public function func_drawSpecialsDisplays() : *
      {
         while(this.specialsDisplay.numChildren > 2)
         {
            this.specialsDisplay.removeChildAt(2);
         }
         this.buyspecial.visible = false;
         this.DisplayScreen.func_clearAllItems();
         this.func_InitSpecialsDisplay();
         this.func_ClearBlueBrint();
         this.func_drawSpecialsOnShip();
      }
      
      public function func_cancelEnergyGenPurchase(param1:MouseEvent) : void
      {
         this.func_drawEnergyGenDisplays();
      }
      
      public function func_drawEnergyCapBrackets() : *
      {
         this.EnergyCapPointHolders = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.numberOfEnergyCapPoints)
         {
            this.EnergyCapPointHolders[_loc1_] = new this.hardPointBox() as MovieClip;
            this.EnergyCapPointHolders[_loc1_].x = 0;
            this.EnergyCapPointHolders[_loc1_].y = 0;
            this.EnergyCapPointHolders[_loc1_].hardpointname.text = "Capacitor";
            this.EnergyCapPointHolders[_loc1_].scaleX = 0.75;
            this.EnergyCapPointHolders[_loc1_].scaleY = 0.75;
            this.EnergyCapPointHolders[_loc1_].name = _loc1_;
            this.shipblueprint.addChild(this.EnergyCapPointHolders[_loc1_]);
            _loc1_++;
         }
      }
      
      public function func_drawShieldGenBrackets() : *
      {
         this.ShieldGenPointHolders = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.numberOfShieldGenPoints)
         {
            this.ShieldGenPointHolders[_loc1_] = new this.hardPointBox() as MovieClip;
            this.ShieldGenPointHolders[_loc1_].x = 0;
            this.ShieldGenPointHolders[_loc1_].y = 0;
            this.ShieldGenPointHolders[_loc1_].hardpointname.text = "Generator";
            this.ShieldGenPointHolders[_loc1_].scaleX = 0.75;
            this.ShieldGenPointHolders[_loc1_].scaleY = 0.75;
            this.ShieldGenPointHolders[_loc1_].name = _loc1_;
            this.shipblueprint.addChild(this.ShieldGenPointHolders[_loc1_]);
            _loc1_++;
         }
      }
      
      public function func_BuyShieldGen(param1:MouseEvent) : void
      {
         this.ShieldGenTypeBeingBought = Number(param1.target.parent.name);
         var _loc2_:* = Number(this.shieldgenerators[this.ShieldGenTypeBeingBought][5]) - this.currentvalueofgenerator;
         if(_loc2_ <= this.playershipstatus[3][1])
         {
            this.buyshieldgen.question.text = "Buy: " + this.shieldgenerators[this.ShieldGenTypeBeingBought][4] + " \r";
            this.buyshieldgen.question.text += "For: " + _loc2_;
            this.buyshieldgen.visible = true;
         }
         else
         {
            this.buyshieldgen.visible = false;
         }
      }
      
      internal function frame77() : *
      {
         this.EnergyCapsDisplayed = new Array();
         this.currentvalueofEnergycap = 0;
         this.numberOfEnergyCapPoints = 1;
         this.EnergyCapPointHolders = new Array();
         this.EnergyCapDataHolders = new Array();
         this.func_drawEnergyCapDisplays();
         this.EnergyCapTypeBeingBought = -1;
         this.buyenergycap.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_acceptEnergyCapPurchase);
         this.buyenergycap.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelEnergyCapPurchase);
      }
      
      public function func_gunHolderImages() : *
      {
         var _loc3_:Class = null;
         this.GunHolder = new Array();
         var _loc1_:* = this.shiptype[this.playershiptype][3][4];
         var _loc2_:* = 0;
         while(_loc2_ < this.numberOfHardpoints)
         {
            if(!isNaN(this.playershipstatus[0][_loc2_][0]))
            {
               _loc3_ = getDefinitionByName("hardwarescreengunbracketguntype" + this.playershipstatus[0][_loc2_][0]) as Class;
            }
            else
            {
               _loc3_ = getDefinitionByName("hardwarescreengunbracketguntypenone") as Class;
            }
            this.GunHolder[_loc2_] = new _loc3_() as MovieClip;
            this.GunHolder[_loc2_].x = Number(this.shiptype[this.playershiptype][2][_loc2_][0]) * _loc1_;
            this.GunHolder[_loc2_].y = Number(this.shiptype[this.playershiptype][2][_loc2_][1]) * _loc1_;
            this.GunHolder[_loc2_].scaleX = 0.75;
            this.GunHolder[_loc2_].scaleY = 0.75;
            if(!isNaN(this.playershipstatus[0][_loc2_][0]))
            {
               this.GunHolder[_loc2_].itemname.text = this.guntype[this.playershipstatus[0][_loc2_][0]][6];
            }
            this.GunHolder[_loc2_].name = _loc2_;
            this.shipblueprint.addChild(this.GunHolder[_loc2_]);
            _loc2_++;
         }
      }
      
      public function func_MissileBankSelected(param1:MouseEvent) : void
      {
         var bankSelected:* = undefined;
         var event:MouseEvent = param1;
         try
         {
            bankSelected = Number(event.target.parent.name);
            this.func_DisplayBankInfo(bankSelected);
            this.currentlySeelctedMIssileBank = bankSelected;
         }
         catch(error:Error)
         {
         }
      }
      
      public function func_cancelMissilePurchase(param1:MouseEvent) : void
      {
         this.func_drawMissileDisplays();
      }
      
      public function func_InitGunsDisplay() : *
      {
         var _loc2_:* = undefined;
         var _loc1_:* = 0;
         while(_loc1_ < this.shipstopgun)
         {
            if(this.guntype[_loc1_] != null)
            {
               _loc2_ = _loc1_;
               this.GunsDisplayed[_loc1_] = new Object();
               this.GunsDisplayed[_loc1_].gunNumber = _loc2_;
               this.GunsDisplayed[_loc1_].cost = this.guntype[_loc2_][5];
               this.GunsDisplayed[_loc1_].gunName = this.guntype[_loc2_][6];
               this.GunsDisplayed[_loc1_].DisplayBox = new this.DisplayBox() as MovieClip;
               this.GunsDisplayed[_loc1_].DisplayBox.itemname.text = this.guntype[_loc2_][6];
               this.GunsDisplayed[_loc1_].DisplayBox.itemcost.text = "Cost:" + this.guntype[_loc2_][5];
               this.GunsDisplayed[_loc1_].DisplayBox.itemspecs.text = "Shld/Struct/Pierce\r  ";
               this.GunsDisplayed[_loc1_].DisplayBox.itemspecs.text += this.guntype[_loc2_][4] + " / " + this.guntype[_loc2_][7] + " / " + this.guntype[_loc2_][8] + " \r";
               this.GunsDisplayed[_loc1_].DisplayBox.itemspecs.text += "Energy Drain: " + this.guntype[_loc2_][3] + " \r";
               this.GunsDisplayed[_loc1_].DisplayBox.itemspecs.text += "Life Time: " + this.guntype[_loc2_][1] + "/s \r";
               this.GunsDisplayed[_loc1_].DisplayBox.itemspecs.text += "Rel:" + this.guntype[_loc2_][2] + "/s  \r";
               this.GunsDisplayed[_loc1_].DisplayBox.itemspecs.text += "Spd:" + this.guntype[_loc2_][0] + "/s ";
               this.GunsDisplayed[_loc1_].DisplayBox.name = _loc2_;
               this.GunsDisplayed[_loc1_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_BuyNewWeapon);
               this.DisplayScreen.func_addItem(this.GunsDisplayed[_loc1_].DisplayBox);
            }
            _loc1_++;
         }
      }
      
      public function func_cancelShiledGenPurchase(param1:MouseEvent) : void
      {
         this.func_drawShieldGenDisplays();
      }
      
      internal function frame95() : *
      {
         this.SpecialsDisplayed = new Array();
         this.availableitems = new Array();
         this.q = 0;
         while(this.q < this.specialshipitems.length)
         {
            if(this.specialshipitems[this.q][5] == "CLOAK")
            {
               if(this.shiptype[this.playershiptype][3][8] == true)
               {
                  this.availableitems[this.availableitems.length] = this.specialshipitems[this.q];
                  this.availableitems[this.availableitems.length - 1][50] = this.q;
               }
            }
            else if(this.specialshipitems[this.q][5] == "RAPIDMISSILE")
            {
               if(this.shiptype[this.playershiptype][3][12] == true)
               {
                  this.availableitems[this.availableitems.length] = this.specialshipitems[this.q];
                  this.availableitems[this.availableitems.length - 1][50] = this.q;
               }
            }
            else if(this.specialshipitems[this.q][5] == "INVULNSHIELD")
            {
               if(this.playershiptype > 3 && this.playershiptype < 8)
               {
                  this.availableitems[this.availableitems.length] = this.specialshipitems[this.q];
                  this.availableitems[this.availableitems.length - 1][50] = this.q;
               }
            }
            else if(this.specialshipitems[this.q][5] == "HSHIELD")
            {
               if(this.shiptype[this.playershiptype][3][10])
               {
                  this.availableitems[this.availableitems.length] = this.specialshipitems[this.q];
                  this.availableitems[this.availableitems.length - 1][50] = this.q;
               }
            }
            else if(this.specialshipitems[this.q][5] == "JUMPDRIVE")
            {
               if(this.shiptype[this.playershiptype][3][13] == true)
               {
                  this.availableitems[this.availableitems.length] = this.specialshipitems[this.q];
                  this.availableitems[this.availableitems.length - 1][50] = this.q;
               }
            }
            else
            {
               this.availableitems[this.availableitems.length] = this.specialshipitems[this.q];
               this.availableitems[this.availableitems.length - 1][50] = this.q;
            }
            ++this.q;
         }
         this.maxBumberOfEnergySpecials = this.shiptype[this.playershiptype][3][7];
         this.SpecialListHolder = new Array();
         this.buyingwindowaction = "";
         this.selectedSpot = -1;
         this.func_drawSpecialsDisplays();
         this.buyspecial.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_acceptQuestion);
         this.buyspecial.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_cancelQuestion);
      }
      
      public function func_drawHardpointBrackets() : *
      {
         this.hardPointHolders = new Array();
         var _loc1_:* = this.shiptype[this.playershiptype][3][4];
         var _loc2_:* = 0;
         while(_loc2_ < this.numberOfHardpoints)
         {
            this.hardPointHolders[_loc2_] = new this.hardPointBox() as MovieClip;
            this.hardPointHolders[_loc2_].x = Number(this.shiptype[this.playershiptype][2][_loc2_][0]) * _loc1_;
            this.hardPointHolders[_loc2_].y = Number(this.shiptype[this.playershiptype][2][_loc2_][1]) * _loc1_;
            this.hardPointHolders[_loc2_].hardpointname.text = "Hardpoint " + _loc2_;
            this.hardPointHolders[_loc2_].scaleX = 0.75;
            this.hardPointHolders[_loc2_].scaleY = 0.75;
            this.hardPointHolders[_loc2_].name = _loc2_;
            this.hardPointHolders[_loc2_].addEventListener(MouseEvent.MOUSE_DOWN,this.func_SelectedHardpointDrag);
            this.hardPointHolders[_loc2_].addEventListener(MouseEvent.MOUSE_UP,this.func_SelectedHardpointReleased);
            this.shipblueprint.addChild(this.hardPointHolders[_loc2_]);
            _loc2_++;
         }
         this.shipblueprint.addChild(this.sellingHardPointBox);
         this.sellingHardPointBox.x = 250;
         this.sellingHardPointBox.y = 0;
         this.sellingHardPointBox.scaleX = 0.75;
         this.sellingHardPointBox.scaleY = 0.75;
         this.sellingHardPointBox.name = -1;
         this.sellingHardPointBox.addEventListener(MouseEvent.MOUSE_UP,this.func_SelectedHardpointReleased);
      }
      
      public function func_SpecialSelling(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(param1.target.parent.parent.name);
         trace(param1.target.parent.parent.name);
         this.buyspecial.visible = true;
         this.buyingwindowaction = this.SpecialListHolder[_loc2_].action.actionToDo.text;
         trace(this.buyingwindowaction);
         this.selectedSpot = _loc2_;
         var _loc3_:* = this.playershipstatus[11][1][this.selectedSpot][0];
         var _loc4_:* = this.specialshipitems[_loc3_][0];
         if(this.buyingwindowaction == "SELL")
         {
            this.buyspecial.question.text = "Sell: " + _loc4_ + " \r For" + Number(this.specialshipitems[_loc3_][6]) / 2;
         }
         else
         {
            this.buyspecial.question.text = "Drop: " + _loc4_ + "? ";
         }
      }
      
      public function func_InitSpecialsDisplay() : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         this.SpecialsDisplayed = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.availableitems.length)
         {
            _loc2_ = _loc1_;
            this.SpecialsDisplayed[_loc1_] = new Object();
            this.SpecialsDisplayed[_loc1_].DisplayBox = new this.DisplayBox() as MovieClip;
            this.SpecialsDisplayed[_loc1_].DisplayBox.itemname.text = this.availableitems[_loc1_][0];
            this.SpecialsDisplayed[_loc1_].DisplayBox.itemcost.text = "Cost: " + this.availableitems[_loc1_][6];
            this.SpecialsDisplayed[_loc1_].DisplayBox.name = this.availableitems[_loc1_][50];
            _loc3_ = "";
            if(this.availableitems[_loc1_][5] == "FLARE")
            {
               _loc3_ += "Energy Drain: " + this.availableitems[_loc1_][2] + " \r";
               _loc3_ += "Reload :" + Number(this.availableitems[_loc1_][4]) / 1000 + "/s \r";
               _loc3_ += "Life Time: " + Number(this.availableitems[_loc1_][3]) / 1000 + "/s \r";
               _loc3_ += "%Chance :" + Number(this.availableitems[_loc1_][10]) * 100 + "% \r";
            }
            else if(this.availableitems[_loc1_][5] == "MINES")
            {
               _loc3_ += "Reload :" + Number(this.availableitems[_loc1_][4]) / 1000 + "/s \r";
               _loc3_ += "Life Time: " + Number(this.availableitems[_loc1_][3]) / 1000 + "/s \r";
               _loc3_ += "Damage :" + this.availableitems[_loc1_][12] + " \r";
               _loc3_ += "Not Effective on Ai";
            }
            else if(this.availableitems[_loc1_][5] == "DETECTOR" || this.availableitems[_loc1_][5] == "STEALTH" || this.availableitems[_loc1_][5] == "CLOAK")
            {
               _loc3_ += "Energy Drain: " + this.availableitems[_loc1_][2] + " \r";
               if(this.availableitems[_loc1_][5] == "DETECTOR")
               {
                  _loc3_ += "Detect Hide Levels: \r " + this.availableitems[_loc1_][10] + " and under \r ";
                  _loc3_ += "Interval: " + Number(this.availableitems[_loc1_][11]) / 1000 + "/s";
               }
               else
               {
                  _loc3_ += "Hide Level: " + this.availableitems[_loc1_][10] + "\r";
               }
               if(this.availableitems[_loc1_][5] == "CLOAK")
               {
                  _loc3_ += "C/D: " + Number(this.availableitems[_loc1_][11]) / 1000 + "/s";
               }
            }
            else if(this.availableitems[_loc1_][5] == "PULSAR")
            {
               _loc3_ += "Energy Need: " + this.availableitems[_loc1_][2] + " \r";
               _loc3_ += "Shutdown Time: " + Number(this.availableitems[_loc1_][11]) / 1000 + " s\r";
               _loc3_ += "Radius :" + this.availableitems[_loc1_][12] + " \r";
               _loc3_ += "QTY: " + this.availableitems[_loc1_][1];
            }
            else if(this.availableitems[_loc1_][5] == "HSHIELD")
            {
               _loc3_ += "Energy Need: " + this.availableitems[_loc1_][2] + " \r";
               _loc3_ += "ReCharge: " + Number(this.availableitems[_loc1_][4]) / 1000 + " s\r";
               _loc3_ += "Duration: " + Number(this.availableitems[_loc1_][3]) / 1000 + "/s \r";
               _loc3_ += "Damage :" + this.availableitems[_loc1_][12] + "/s \r";
            }
            else if(this.availableitems[_loc1_][5] == "INVULNSHIELD")
            {
               _loc3_ += "Energy Need: " + this.availableitems[_loc1_][2] + " \r";
               _loc3_ += "ReCharge: " + Number(this.availableitems[_loc1_][4]) / 1000 + " s\r";
               _loc3_ += "Duration :" + this.availableitems[_loc1_][3] + "/s \r";
            }
            else if(this.availableitems[_loc1_][5] == "RECHARGESHIELD")
            {
               _loc3_ += "Energy Conversion: \r 1 Energy:" + this.availableitems[_loc1_][2] + " Shield \r";
               _loc3_ += "ReCharge :" + Number(this.availableitems[_loc1_][4]) / 1000 + "/s \r";
            }
            else if(this.availableitems[_loc1_][5] == "RECHARGESTRUCT")
            {
               _loc3_ += "Energy Conversion: \r 1 Energy:" + this.availableitems[_loc1_][2] + " Struct \r";
               _loc3_ += "ReCharge :" + Number(this.availableitems[_loc1_][4]) / 1000 + "/s \r";
            }
            else if(this.availableitems[_loc1_][5] == "RAPIDMISSILE")
            {
               _loc3_ += "Energy Used:" + this.availableitems[_loc1_][2] + "\r";
               _loc3_ += "Reload: " + (1 - Number(this.availableitems[_loc1_][11])) * 100 + "% Faster\r";
               _loc3_ += "Dumbfire Missiles\rOnly";
            }
            else if(this.availableitems[_loc1_][5] == "JUMPDRIVE")
            {
               _loc3_ += "Energy:" + this.availableitems[_loc1_][2] + "\r";
               _loc3_ += "Reload: " + Number(this.availableitems[_loc1_][4]) / 1000 + "s\r";
               _loc3_ += "Set Location\rOn Map";
            }
            else if(this.availableitems[_loc1_][5] == "WINGMAN")
            {
               _loc3_ += "Energy:" + this.availableitems[_loc1_][2] + "\r";
               _loc3_ += "Reload: " + Number(this.availableitems[_loc1_][4]) / 1000 + "s\r";
               _loc3_ += "Max Wingmen\rCarriers:3\rOthers:1";
            }
            this.SpecialsDisplayed[_loc1_].DisplayBox.itemspecs.text = _loc3_;
            this.SpecialsDisplayed[_loc1_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_SpecialBuying);
            this.DisplayScreen.func_addItem(this.SpecialsDisplayed[_loc1_].DisplayBox);
            _loc1_++;
         }
      }
      
      public function func_drawMissileDisplays() : *
      {
         this.func_ClearBlueBrint();
         this.func_drawMissilePointBrackets();
         this.func_MissileHolderImages();
         this.func_DisplayBankInfo(this.currentlySeelctedMIssileBank);
      }
      
      public function func_cancelQuestion(param1:MouseEvent) : void
      {
         this.func_drawSpecialsDisplays();
      }
      
      public function func_SpecialBuying(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(param1.target.parent.name);
         this.buyingwindowaction = "BUY";
         this.selectedSpot = _loc2_;
         var _loc3_:* = this.specialshipitems[this.selectedSpot][6];
         var _loc4_:* = this.specialshipitems[this.selectedSpot][0];
         if(this.playershipstatus[11][1].length < this.shiptype[this.playershiptype][3][7])
         {
            if(_loc3_ <= this.playershipstatus[3][1])
            {
               this.buyspecial.visible = true;
               this.buyspecial.question.text = "Buy: " + _loc4_ + " \r For" + _loc3_;
            }
         }
      }
      
      public function func_drawEnergyCapImages() : *
      {
         var _loc2_:Class = null;
         this.EnergyCapDataHolders = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.numberOfEnergyCapPoints)
         {
            _loc2_ = getDefinitionByName("hardwarescreengunbracketenergycap0") as Class;
            this.EnergyCapDataHolders[_loc1_] = new _loc2_() as MovieClip;
            this.EnergyCapDataHolders[_loc1_].x = 0;
            this.EnergyCapDataHolders[_loc1_].y = 0;
            this.EnergyCapDataHolders[_loc1_].scaleX = 0.75;
            this.EnergyCapDataHolders[_loc1_].scaleY = 0.75;
            this.EnergyCapDataHolders[_loc1_].itemname.text = this.energycapacitors[this.playershipstatus[1][5]][1];
            this.EnergyCapDataHolders[_loc1_].name = _loc1_;
            this.shipblueprint.addChild(this.EnergyCapDataHolders[_loc1_]);
            _loc1_++;
         }
      }
      
      public function func_MissileHolderImages() : *
      {
         var _loc2_:Class = null;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         this.MissileDataHolders = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.numberOfMissilepoints)
         {
            _loc2_ = getDefinitionByName("hardwarescreenmissilebankicon") as Class;
            this.MissileDataHolders[_loc1_] = new _loc2_() as MovieClip;
            this.MissileDataHolders[_loc1_].x = Number(this.shiptype[this.playershiptype][7][_loc1_][2]) * this.missilelocationmultiplier;
            this.MissileDataHolders[_loc1_].y = Number(this.shiptype[this.playershiptype][7][_loc1_][3]) * this.missilelocationmultiplier;
            this.MissileDataHolders[_loc1_].scaleX = 0.75;
            this.MissileDataHolders[_loc1_].scaleY = 0.75;
            this.MissileDataHolders[_loc1_].itemname.text = "Bank " + _loc1_;
            this.MissileDataHolders[_loc1_].name = _loc1_;
            _loc3_ = 0;
            _loc4_ = _loc1_;
            _loc5_ = 0;
            while(_loc5_ < this.missile.length)
            {
               if(this.playershipstatus[7][_loc4_][4][_loc5_] > 0)
               {
                  _loc3_ += this.playershipstatus[7][_loc4_][4][_loc5_];
               }
               _loc5_++;
            }
            this.shipblueprint.addChild(this.MissileDataHolders[_loc1_]);
            _loc6_ = this.playershipstatus[7][_loc4_][5];
            this.MissileDataHolders[_loc1_].qty.text = "Missiles\r(" + _loc3_ + "/" + _loc6_ + ")";
            this.MissileDataHolders[_loc1_].addEventListener(MouseEvent.MOUSE_DOWN,this.func_MissileBankSelected);
            this.shipblueprint.addChild(this.MissileDataHolders[_loc1_]);
            _loc1_++;
         }
      }
      
      public function func_GunsSelectionMade(param1:MouseEvent) : void
      {
         this.func_GunsSelectionWasMade();
      }
      
      public function func_drawMissilePointBrackets() : *
      {
         this.MissilePointHolders = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.numberOfMissilepoints)
         {
            this.MissilePointHolders[_loc1_] = new this.hardPointBox() as MovieClip;
            this.MissilePointHolders[_loc1_].x = Number(this.shiptype[this.playershiptype][7][_loc1_][2]) * this.missilelocationmultiplier;
            this.MissilePointHolders[_loc1_].y = Number(this.shiptype[this.playershiptype][7][_loc1_][3]) * this.missilelocationmultiplier;
            this.MissilePointHolders[_loc1_].maxMissiles = this.playershipstatus[7][_loc1_][5];
            this.MissilePointHolders[_loc1_].hardpointname.text = "Bank " + _loc1_;
            this.MissilePointHolders[_loc1_].scaleX = 0.75;
            this.MissilePointHolders[_loc1_].scaleY = 0.75;
            this.MissilePointHolders[_loc1_].name = _loc1_;
            this.shipblueprint.addChild(this.MissilePointHolders[_loc1_]);
            _loc1_++;
         }
      }
      
      public function func_TurretsSelectionWasMade(param1:MouseEvent) : void
      {
         if(this.shiptype[this.playershiptype][5].length > 0)
         {
            this.func_ResetButtonsToOff();
            this.turrets.gotoAndStop(1);
            gotoAndStop("Turrets");
         }
      }
      
      public function func_BuyEnergyGen(param1:MouseEvent) : void
      {
         this.EnergyGenTypeBeingBought = Number(param1.target.parent.name);
         var _loc2_:* = Number(this.energygenerators[this.EnergyGenTypeBeingBought][2]) - this.currentvalueofEnergygenerator;
         if(_loc2_ <= this.playershipstatus[3][1])
         {
            this.buyenergygen.question.text = "Buy: " + this.energygenerators[this.EnergyGenTypeBeingBought][1] + " \r";
            this.buyenergygen.question.text += "For: " + _loc2_;
            this.buyenergygen.visible = true;
         }
         else
         {
            this.buyenergygen.visible = false;
         }
      }
      
      public function func_acceptGunPurchase(param1:MouseEvent) : void
      {
         var _loc2_:* = 0;
         while(_loc2_ < this.playershipstatus[0].length)
         {
            if(this.playershipstatus[0][_loc2_][0] == "none")
            {
               this.playershipstatus[0][_loc2_][0] = this.gunBeingBought;
               this.playershipstatus[3][1] -= this.guntype[this.gunBeingBought][5];
               break;
            }
            _loc2_++;
         }
         this.buyweapon.visible = false;
         this.func_drawGunDisplays();
      }
      
      public function func_cancelTurretSale(param1:MouseEvent) : void
      {
         this.func_drawTurretDisplays();
      }
      
      public function func_cancelEnergyCapPurchase(param1:MouseEvent) : void
      {
         this.func_drawEnergyCapDisplays();
      }
      
      public function func_BuyMissiles(param1:MouseEvent) : void
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         this.missileTypeBeingBought = Number(param1.target.parent.name);
         if(this.currentlySeelctedMIssileBank >= 0)
         {
            _loc2_ = this.missile[this.missileTypeBeingBought][5];
            if(_loc2_ <= this.playershipstatus[3][1])
            {
               _loc3_ = 0;
               _loc4_ = this.MissilePointHolders[this.currentlySeelctedMIssileBank].maxMissiles;
               _loc3_ = 0;
               while(_loc3_ < this.missile.length)
               {
                  if(this.playershipstatus[7][this.currentlySeelctedMIssileBank][4][_loc3_] > 0)
                  {
                     _loc4_ -= this.playershipstatus[7][this.currentlySeelctedMIssileBank][4][_loc3_];
                  }
                  _loc3_++;
               }
               if(_loc4_ > 0)
               {
                  this.missilepurchasewindow.visible = true;
                  this.missilepurchasewindow.maxMissiles = _loc4_;
                  if(_loc4_ * _loc2_ > this.playershipstatus[3][1])
                  {
                     this.missilepurchasewindow.maxMissiles = Math.floor(Number(this.playershipstatus[3][1]) / _loc2_);
                  }
                  this.missilepurchasewindow.MissilePrice = _loc2_;
                  this.missilepurchasewindow.quantity.text = 0;
                  this.missilepurchasewindow.banknumberdisp.text = "Bank: " + this.currentlySeelctedMIssileBank;
                  this.missilepurchasewindow.missilecostdisp.text = this.missile[this.missileTypeBeingBought][6] + " at " + _loc2_ + "each";
                  this.missilepurchasewindow.maxpurchase.text = "Max Qty of: " + this.missilepurchasewindow.maxMissiles + " missiles";
               }
            }
         }
      }
      
      public function func_drawTurretBrackets() : *
      {
         this.TurrethardPointHolders = new Array();
         var _loc1_:* = this.shiptype[this.playershiptype][3][4];
         var _loc2_:* = 0;
         while(_loc2_ < this.numberOfTurrets)
         {
            this.TurrethardPointHolders[_loc2_] = new this.hardPointBox() as MovieClip;
            this.TurrethardPointHolders[_loc2_].x = Number(this.shiptype[this.playershiptype][5][_loc2_][0]) * _loc1_;
            this.TurrethardPointHolders[_loc2_].y = Number(this.shiptype[this.playershiptype][5][_loc2_][1]) * _loc1_;
            this.TurrethardPointHolders[_loc2_].hardpointname.text = "Turret " + _loc2_;
            this.TurrethardPointHolders[_loc2_].scaleX = 0.75;
            this.TurrethardPointHolders[_loc2_].scaleY = 0.75;
            this.TurrethardPointHolders[_loc2_].name = _loc2_;
            this.TurrethardPointHolders[_loc2_].addEventListener(MouseEvent.MOUSE_DOWN,this.func_SelectedTurretDrag);
            this.TurrethardPointHolders[_loc2_].addEventListener(MouseEvent.MOUSE_UP,this.func_SelectedTurretReleased);
            this.shipblueprint.addChild(this.TurrethardPointHolders[_loc2_]);
            _loc2_++;
         }
         this.shipblueprint.addChild(this.sellingTurretPointBox);
         this.sellingTurretPointBox.x = 250;
         this.sellingTurretPointBox.y = 0;
         this.sellingTurretPointBox.scaleX = 0.75;
         this.sellingTurretPointBox.scaleY = 0.75;
         this.sellingTurretPointBox.name = -1;
         this.sellingTurretPointBox.addEventListener(MouseEvent.MOUSE_UP,this.func_SelectedTurretReleased);
      }
      
      public function func_drawEnergyGenDisplays() : *
      {
         this.buyenergygen.visible = false;
         this.DisplayScreen.func_clearAllItems();
         this.func_InitEnergyGensDisplay();
         this.func_ClearBlueBrint();
         this.func_drawEnergyGenBrackets();
         this.func_drawEnergyGenImages();
      }
      
      public function func_BuyNewlTurret(param1:MouseEvent) : void
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         this.TurretBeingBought = Number(param1.target.parent.name);
         if(this.guntype[this.TurretBeingBought][5] <= this.playershipstatus[3][1])
         {
            _loc2_ = 0;
            _loc3_ = false;
            _loc4_ = -1;
            while(_loc2_ < this.playershipstatus[8].length)
            {
               if(this.playershipstatus[8][_loc2_][0] == "none")
               {
                  _loc3_ = true;
                  _loc4_ = _loc2_;
                  break;
               }
               _loc2_++;
            }
            if(_loc3_ == true)
            {
               this.buyweapon.visible = true;
               this.buyweapon.question.text = "Buy: " + this.guntype[this.TurretBeingBought][6] + " \r" + "For: " + Number(this.guntype[this.TurretBeingBought][5]) * this.turretCostModifier;
            }
         }
      }
      
      public function func_BuyNewWeapon(param1:MouseEvent) : void
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         this.gunBeingBought = Number(param1.target.parent.name);
         if(this.guntype[this.gunBeingBought][5] <= this.playershipstatus[3][1])
         {
            _loc2_ = 0;
            _loc3_ = false;
            _loc4_ = -1;
            while(_loc2_ < this.playershipstatus[0].length)
            {
               if(this.playershipstatus[0][_loc2_][0] == "none")
               {
                  _loc3_ = true;
                  _loc4_ = _loc2_;
                  break;
               }
               _loc2_++;
            }
            if(_loc3_ == true)
            {
               this.buyweapon.visible = true;
               this.buyweapon.question.text = "Buy: " + this.guntype[this.gunBeingBought][6] + " \r" + "For: " + this.guntype[this.gunBeingBought][5];
            }
         }
      }
      
      public function func_ShieldSelectionMade(param1:MouseEvent) : void
      {
         this.func_ResetButtonsToOff();
         this.shieldgen.gotoAndStop(1);
         gotoAndStop("shieldgenerators");
      }
      
      public function func_SelectedTurretReleased(param1:MouseEvent) : void
      {
         var TemTurretsHolder:* = undefined;
         var event:MouseEvent = param1;
         var gunReleasedon:* = "test";
         try
         {
            gunReleasedon = Number(event.target.parent.name);
            if(this.currentlyDraggingTurret != -1)
            {
               if(!isNaN(gunReleasedon))
               {
                  trace(gunReleasedon);
                  if(gunReleasedon == -1)
                  {
                     if(!isNaN(this.playershipstatus[8][this.currentlyDraggingTurret][0]))
                     {
                        this.sellweapon.visible = true;
                        this.sellweapon.question.text = "Sell: " + this.guntype[this.playershipstatus[8][this.currentlyDraggingTurret][0]][6] + " \r" + "For: " + Math.round(Number(this.guntype[this.playershipstatus[8][this.currentlyDraggingTurret][0]][5]) / 2) * this.turretCostModifier;
                        this.TurretsHolder[this.currentlyDraggingTurret].stopDrag();
                     }
                     else
                     {
                        gunReleasedon = "";
                     }
                  }
                  else if(this.currentlyDraggingTurret != gunReleasedon)
                  {
                     TemTurretsHolder = this.playershipstatus[8][this.currentlyDraggingTurret];
                     this.playershipstatus[8][this.currentlyDraggingTurret] = this.playershipstatus[8][gunReleasedon];
                     this.playershipstatus[8][gunReleasedon] = TemTurretsHolder;
                  }
               }
            }
            if(gunReleasedon != -1)
            {
               this.func_drawTurretDisplays();
            }
         }
         catch(error:Error)
         {
         }
      }
      
      public function func_acceptSale(param1:MouseEvent) : void
      {
         this.playershipstatus[3][1] += Math.round(Number(this.guntype[this.playershipstatus[0][this.currentlyDraggingGun][0]][5]) / 2);
         this.playershipstatus[0][this.currentlyDraggingGun][0] = "none";
         this.func_drawGunDisplays();
      }
      
      public function func_cancelTurretPurchase(param1:MouseEvent) : void
      {
         this.buyweapon.visible = false;
      }
      
      public function func_acceptTurretPurchase(param1:MouseEvent) : void
      {
         var _loc2_:* = 0;
         while(_loc2_ < this.playershipstatus[8].length)
         {
            if(this.playershipstatus[8][_loc2_][0] == "none")
            {
               this.playershipstatus[8][_loc2_][0] = this.TurretBeingBought;
               this.playershipstatus[3][1] -= Number(this.guntype[this.TurretBeingBought][5]) * this.turretCostModifier;
               break;
            }
            _loc2_++;
         }
         this.buyweapon.visible = false;
         this.func_drawTurretDisplays();
      }
      
      public function func_acceptQuestion(param1:MouseEvent) : void
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         if(this.buyingwindowaction == "BUY")
         {
            _loc2_ = this.specialshipitems[this.selectedSpot][6];
            _loc3_ = this.specialshipitems[this.selectedSpot][1];
            this.playershipstatus[3][1] -= _loc2_;
            if(this.playershipstatus[11][1].length == 0)
            {
               this.playershipstatus[11][1] = new Array();
            }
            _loc4_ = this.playershipstatus[11][1].length;
            this.playershipstatus[11][1][_loc4_] = new Array();
            this.playershipstatus[11][1][_loc4_][0] = this.selectedSpot;
            this.playershipstatus[11][1][_loc4_][1] = _loc3_;
         }
         else if(this.buyingwindowaction == "SELL")
         {
            _loc5_ = this.playershipstatus[11][1][this.selectedSpot][0];
            _loc6_ = Number(this.specialshipitems[_loc5_][6]) / 2;
            this.playershipstatus[3][1] += _loc6_;
            this.playershipstatus[11][1].splice(this.selectedSpot,1);
         }
         else
         {
            this.playershipstatus[11][1].splice(this.selectedSpot,1);
         }
         this.func_drawSpecialsDisplays();
      }
      
      public function func_InitEnergyGensDisplay() : *
      {
         var _loc3_:* = undefined;
         this.currentvalueofEnergygenerator = Math.round(Number(this.energygenerators[this.playershipstatus[1][0]][2]) / 2);
         this.EnergyGensDisplayed = new Array();
         var _loc1_:* = 0;
         var _loc2_:* = this.shiptype[this.playershiptype][6][2] + 1;
         while(_loc1_ < _loc2_)
         {
            _loc3_ = _loc1_;
            this.EnergyGensDisplayed[_loc1_] = new Object();
            this.EnergyGensDisplayed[_loc1_].genNumber = _loc3_;
            this.EnergyGensDisplayed[_loc1_].DisplayBox = new this.DisplayBox() as MovieClip;
            this.EnergyGensDisplayed[_loc1_].DisplayBox.itemname.text = this.energygenerators[_loc3_][1];
            if(this.playershipstatus[1][0] == _loc3_)
            {
               this.EnergyGensDisplayed[_loc1_].DisplayBox.itemcost.text = "Current";
            }
            else
            {
               this.EnergyGensDisplayed[_loc1_].DisplayBox.itemcost.text = "Cost:" + (Number(this.energygenerators[_loc3_][2]) - this.currentvalueofEnergygenerator);
               this.EnergyGensDisplayed[_loc1_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_BuyEnergyGen);
            }
            this.EnergyGensDisplayed[_loc1_].DisplayBox.name = _loc3_;
            this.EnergyGensDisplayed[_loc1_].DisplayBox.itemspecs.text = "Recharge: " + this.energygenerators[_loc3_][0] + "/sec \r";
            this.DisplayScreen.func_addItem(this.EnergyGensDisplayed[_loc1_].DisplayBox);
            _loc1_++;
         }
      }
      
      public function func_drawGunDisplays() : *
      {
         this.func_ClearBlueBrint();
         this.func_gunHolderImages();
         this.func_drawHardpointBrackets();
         this.currentlyDraggingGun = -1;
         this.sellweapon.visible = false;
      }
      
      public function func_EnergyGenSelectionMade(param1:MouseEvent) : void
      {
         this.func_ResetButtonsToOff();
         this.energygen.gotoAndStop(1);
         gotoAndStop("EnergyGenerators");
      }
      
      public function func_drawSpecialsOnShip() : *
      {
         var _loc1_:Class = null;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         this.SpecialListHolder = new Array();
         this.specialsDisplay.titledisp.text = "No. of Specials (" + this.playershipstatus[11][1].length + "/" + this.shiptype[this.playershiptype][3][7] + ")";
         _loc1_ = getDefinitionByName("hardwarescreenspecialsinfoitemdisp") as Class;
         var _loc2_:* = 0;
         _loc2_ = 0;
         while(_loc2_ < this.playershipstatus[11][1].length)
         {
            this.SpecialListHolder[_loc2_] = new _loc1_() as MovieClip;
            _loc3_ = this.playershipstatus[11][1][_loc2_][0];
            _loc4_ = this.specialshipitems[_loc3_][0];
            _loc5_ = this.playershipstatus[11][1][_loc2_][1];
            this.SpecialListHolder[_loc2_].x = 0;
            this.SpecialListHolder[_loc2_].y = Number(this.SpecialListHolder[_loc2_].height) * _loc2_;
            this.SpecialListHolder[_loc2_].name = _loc2_;
            this.SpecialListHolder[_loc2_].label.text = _loc2_ + ": " + _loc4_;
            if(_loc5_ >= 0)
            {
               this.SpecialListHolder[_loc2_].label.text = " (" + _loc5_ + ")";
               this.SpecialListHolder[_loc2_].action.actionToDo.text = "DROP";
            }
            else
            {
               this.SpecialListHolder[_loc2_].action.actionToDo.text = "SELL";
            }
            this.SpecialListHolder[_loc2_].action.addEventListener(MouseEvent.MOUSE_DOWN,this.func_SpecialSelling);
            this.specialsDisplay.addChild(this.SpecialListHolder[_loc2_]);
            _loc2_++;
         }
      }
   }
}
