package sc313_fla
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import flash.text.TextField;
   import flash.utils.getDefinitionByName;
   
   public dynamic class hangarWindow_115 extends MovieClip
   {
       
      
      public var PurchasePriceOfShip;
      
      public var questionBox:MovieClip;
      
      public var energygenerators;
      
      public var shipDetails1:TextField;
      
      public var shipDetails2:TextField;
      
      public var changeShip:MovieClip;
      
      public var cancel_button:MovieClip;
      
      public var maxShipsToOwn;
      
      public var playerscurrentextrashipno;
      
      public var shieldgenerators;
      
      public var missile;
      
      public var currentlySelectedExtraSlot;
      
      public var sellShip:MovieClip;
      
      public var OwnedShipsDisplay;
      
      public var DisplayScreen:MovieClip;
      
      public var BuyQuestionBox:MovieClip;
      
      public var BuyShip:MovieClip;
      
      public var DisplayBox:Class;
      
      public var playershipstatus;
      
      public var AvailableShipsDisplay;
      
      public var currentlySelectedShip;
      
      public var specialshipitems;
      
      public var extraplayerships;
      
      public var capShipsStartAt;
      
      public var exit_button:MovieClip;
      
      public var SalePriceOfShip;
      
      public var shiptype;
      
      public var energycapacitors;
      
      public var guntype;
      
      public function hangarWindow_115()
      {
         super();
         addFrameScript(0,this.frame1,6,this.frame7,34,this.frame35);
      }
      
      public function func_AcceptShipPurchase(param1:MouseEvent) : void
      {
         var _loc2_:* = this.currentlySelectedExtraSlot;
         this.extraplayerships[_loc2_] = new Array();
         this.extraplayerships[_loc2_][0] = this.currentlySelectedShip;
         var _loc3_:* = 0;
         this.extraplayerships[_loc2_][4] = new Array();
         while(_loc3_ < this.shiptype[this.currentlySelectedShip][2].length)
         {
            this.extraplayerships[_loc2_][4][_loc3_] = 0;
            _loc3_++;
         }
         var _loc4_:* = 0;
         this.extraplayerships[_loc2_][5] = new Array();
         while(_loc4_ < this.shiptype[this.currentlySelectedShip][5].length)
         {
            this.extraplayerships[_loc2_][5][_loc4_] = 0;
            _loc4_++;
         }
         this.extraplayerships[_loc2_][11] = new Array();
         this.extraplayerships[_loc2_][11][1] = new Array();
         this.extraplayerships[_loc2_][1] = 0;
         this.extraplayerships[_loc2_][2] = 0;
         this.extraplayerships[_loc2_][3] = 0;
         this.playershipstatus[3][1] -= this.PurchasePriceOfShip;
         gotoAndStop("extraships");
      }
      
      public function func_CancelShipPurchase(param1:MouseEvent) : void
      {
         this.BuyQuestionBox.visible = false;
      }
      
      internal function frame1() : *
      {
         this.changeShip.actionlabel.text = "Set Current Ship";
         this.sellShip.actionlabel.text = "Sell Ship";
         this.exit_button.actionlabel.text = "Exit";
         this.changeShip.gotoAndStop("2");
         this.sellShip.gotoAndStop("2");
         this.exit_button.gotoAndStop("1");
         this.questionBox.visible = false;
         this.DisplayBox = getDefinitionByName("Shipitembox") as Class;
         this.currentlySelectedExtraSlot = -1;
         this.maxShipsToOwn = 5;
         this.capShipsStartAt = 16;
         gotoAndStop("extraships");
         stop();
      }
      
      public function func_ResetButtonsToOff() : *
      {
      }
      
      public function displayShipDetail(param1:*) : *
      {
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         this.shipDetails1.text = "";
         var _loc2_:* = "";
         var _loc3_:* = this.extraplayerships[param1][0];
         _loc2_ += "Ship Name:\r  " + this.shiptype[_loc3_][0] + "\r";
         _loc2_ += "Current Setup:\r";
         var _loc4_:* = this.extraplayerships[param1][1];
         _loc2_ += "Shield Gen:" + this.shieldgenerators[_loc4_][4] + "\r";
         _loc2_ += "  Max Shield:" + this.shieldgenerators[_loc4_][0] + "\r";
         var _loc5_:* = this.extraplayerships[param1][2];
         _loc2_ += "Energy Gen:" + this.energygenerators[_loc5_][1] + "\r";
         _loc2_ += "  Charge Rate:" + this.energygenerators[_loc5_][0] + "\r";
         var _loc6_:* = this.extraplayerships[param1][3];
         _loc2_ += "Energy Cap:" + this.energycapacitors[_loc6_][1] + "\r";
         _loc2_ += "  Max Energy:" + this.energycapacitors[_loc6_][0] + "\r";
         _loc2_ += "Guns:\r";
         var _loc7_:* = 0;
         while(_loc7_ < this.extraplayerships[param1][4].length)
         {
            _loc8_ = this.extraplayerships[param1][4][_loc7_];
            if(!isNaN(_loc8_))
            {
               _loc2_ += "Hardpoint " + _loc7_ + ": " + this.guntype[_loc8_][6] + "\r";
            }
            else
            {
               _loc2_ += "Hardpoint " + _loc7_ + ": None\r";
            }
            _loc7_++;
         }
         _loc2_ += "Turrets:\r";
         _loc7_ = 0;
         while(_loc7_ < this.extraplayerships[param1][5].length)
         {
            _loc9_ = this.extraplayerships[param1][5][_loc7_];
            if(!isNaN(_loc9_))
            {
               _loc2_ += "Turret " + _loc7_ + ": " + this.guntype[_loc9_][6] + "\r";
            }
            else
            {
               _loc2_ += "Turret " + _loc7_ + ": None\r";
            }
            _loc7_++;
         }
         this.shipDetails1.text = _loc2_;
         this.shipDetails2.text = this.displayMaxStatsDetail(_loc3_);
      }
      
      public function func_PurchaseNewShip(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(param1.target.parent.name);
         this.currentlySelectedExtraSlot = _loc2_;
         gotoAndStop("newShip");
      }
      
      public function func_ShipBuyButton(param1:MouseEvent) : void
      {
         if(this.currentlySelectedShip >= 0)
         {
            this.PurchasePriceOfShip = this.shiptype[this.currentlySelectedShip][1];
            if(this.PurchasePriceOfShip <= this.playershipstatus[3][1])
            {
               this.BuyQuestionBox.visible = true;
               this.BuyQuestionBox.question.text = "Buy " + this.shiptype[this.currentlySelectedShip][0] + " \r For: " + this.PurchasePriceOfShip;
            }
         }
      }
      
      public function func_BuyingAShip(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(param1.target.parent.name);
         var _loc3_:* = 0;
         while(_loc3_ < this.AvailableShipsDisplay.length)
         {
            this.AvailableShipsDisplay[_loc3_].DisplayBox.gotoAndStop(1);
            _loc3_++;
         }
         param1.target.parent.gotoAndStop(2);
         this.displayBuyingShipDetail(_loc2_);
      }
      
      public function func_InitBuyShipsDisplay() : *
      {
         this.BuyQuestionBox.visible = false;
         this.DisplayScreen.func_clearAllItems();
         this.func_InitAvailShipsDisplay();
         this.shipDetails1.text = "";
         this.shipDetails2.text = "";
      }
      
      public function displayMaxStatsDetail(param1:*) : *
      {
         trace(param1);
         var _loc2_:* = "";
         _loc2_ += "Top Setup Available:\r";
         _loc2_ += "Max Structure: " + this.shiptype[param1][3][3] + "\r";
         var _loc3_:* = this.shiptype[param1][6][0];
         if(_loc3_ > this.shieldgenerators.length)
         {
            _loc3_ = this.shieldgenerators.length - 1;
         }
         _loc2_ += "Shield Gen:" + this.shieldgenerators[_loc3_][4] + "\r";
         _loc2_ += "  Max Shield:" + this.shieldgenerators[_loc3_][0] + "\r";
         var _loc4_:*;
         if((_loc4_ = this.shiptype[param1][6][2]) > this.energygenerators.length)
         {
            _loc4_ = this.energygenerators.length - 1;
         }
         _loc2_ += "Energy Gen:" + this.energygenerators[_loc4_][1] + "\r";
         _loc2_ += "  Charge Rate:" + this.energygenerators[_loc4_][0] + "\r";
         var _loc5_:*;
         if((_loc5_ = this.shiptype[param1][6][1]) > this.energycapacitors.length)
         {
            _loc5_ = this.energycapacitors.length - 1;
         }
         _loc2_ += "Energy Cap:" + this.energycapacitors[_loc5_][1] + "\r";
         _loc2_ += "  Max Energy:" + this.energycapacitors[_loc5_][0] + "\r";
         _loc2_ += "Guns:\r";
         _loc2_ += "  Number: " + this.shiptype[param1][2].length + "\r";
         var _loc6_:*;
         if((_loc6_ = this.shiptype[param1][6][3]) > this.guntype.length)
         {
            _loc6_ = this.guntype.length - 1;
         }
         _loc2_ += "  Type: " + this.guntype[_loc6_][6] + "\r";
         _loc2_ += "Specials Available:\r";
         _loc2_ += "  Total Spots: " + this.shiptype[param1][3][7] + "\r";
         _loc2_ += "  Can Cloak: " + this.shiptype[param1][3][8] + "\r";
         _loc2_ += "  Rapid Missiles: " + this.shiptype[param1][3][12] + "\r";
         if(param1 > 3 && param1 < 8)
         {
            _loc2_ += "  Invulnerability: true\r";
         }
         else
         {
            _loc2_ += "  Invulnerability: false\r";
         }
         _loc2_ += "  Hard Shields: " + this.shiptype[param1][3][10] + "\r";
         return _loc2_ + ("  Jump Drive: " + this.shiptype[param1][3][13] + "\r");
      }
      
      public function func_InitAvailShipsDisplay() : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         this.AvailableShipsDisplay = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.shiptype.length)
         {
            if(this.shiptype[_loc1_] != null)
            {
               if(this.shiptype[_loc1_][0] != null)
               {
                  _loc2_ = _loc1_;
                  _loc3_ = this.AvailableShipsDisplay.length;
                  this.AvailableShipsDisplay[_loc3_] = new Object();
                  this.AvailableShipsDisplay[_loc3_].DisplayBox = new this.DisplayBox() as MovieClip;
                  this.AvailableShipsDisplay[_loc3_].DisplayBox.name = _loc2_;
                  this.AvailableShipsDisplay[_loc3_].DisplayBox.current.text = "";
                  this.AvailableShipsDisplay[_loc3_].DisplayBox.itemname.text = this.shiptype[_loc2_][0];
                  this.AvailableShipsDisplay[_loc3_].DisplayBox.itemspecs.text = "";
                  this.AvailableShipsDisplay[_loc3_].DisplayBox.current.text = "Cost: " + this.shiptype[_loc2_][1];
                  this.AvailableShipsDisplay[_loc3_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_BuyingAShip);
                  this.DisplayScreen.func_addItem(this.AvailableShipsDisplay[_loc3_].DisplayBox);
                  (_loc4_ = new this.shiptype[_loc2_][10]() as MovieClip).x = Number(this.AvailableShipsDisplay[_loc3_].DisplayBox.width) / 2;
                  _loc4_.y = Number(this.AvailableShipsDisplay[_loc3_].DisplayBox.height) / 2;
                  this.AvailableShipsDisplay[_loc3_].DisplayBox.addChild(_loc4_);
                  _loc5_ = Number(this.AvailableShipsDisplay[_loc3_].DisplayBox.width) * 0.8;
                  _loc6_ = false;
                  if(_loc4_.width > _loc5_)
                  {
                     _loc7_ = _loc5_ / Number(_loc4_.width);
                     _loc4_.scaleX = _loc7_;
                     _loc4_.scaleY = _loc7_;
                  }
               }
            }
            _loc1_++;
         }
      }
      
      public function func_CancelShipSale(param1:MouseEvent) : void
      {
         this.questionBox.visible = false;
      }
      
      public function func_SellCurrShip(param1:MouseEvent) : void
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         if(this.playerscurrentextrashipno != this.currentlySelectedExtraSlot)
         {
            _loc2_ = 0;
            _loc2_ += Number(this.shiptype[this.extraplayerships[this.currentlySelectedExtraSlot][0]][1]) / 2;
            _loc2_ += Number(this.shieldgenerators[this.extraplayerships[this.currentlySelectedExtraSlot][1]][5]) / 4;
            _loc2_ += Number(this.energygenerators[this.extraplayerships[this.currentlySelectedExtraSlot][2]][2]) / 4;
            _loc2_ += Number(this.energycapacitors[this.extraplayerships[this.currentlySelectedExtraSlot][3]][2]) / 4;
            _loc3_ = 0;
            while(_loc3_ < this.shiptype[this.extraplayerships[this.currentlySelectedExtraSlot][0]][2].length)
            {
               if(!isNaN(Number(this.extraplayerships[this.currentlySelectedExtraSlot][4][_loc3_])))
               {
                  _loc2_ += Number(this.guntype[this.extraplayerships[this.currentlySelectedExtraSlot][4][_loc3_]][5]) / 4;
               }
               _loc3_++;
            }
            _loc4_ = 0;
            while(_loc4_ < this.shiptype[this.extraplayerships[this.currentlySelectedExtraSlot][0]][5].length)
            {
               if(!isNaN(Number(this.extraplayerships[this.currentlySelectedExtraSlot][5][_loc4_])))
               {
                  _loc2_ += Number(this.guntype[this.extraplayerships[this.currentlySelectedExtraSlot][5][_loc4_]][5]) / 2;
               }
               _loc4_++;
            }
            _loc2_ = Math.round(_loc2_);
            if(!isNaN(_loc2_))
            {
               this.SalePriceOfShip = _loc2_;
            }
            else
            {
               this.SalePriceOfShip = 0;
            }
            this.questionBox.question.text = "Sell " + this.shiptype[this.currentlySelectedExtraSlot][0] + "\r For " + this.SalePriceOfShip + "?";
            this.questionBox.visible = true;
         }
      }
      
      internal function frame35() : *
      {
         this.exit_button.visible = false;
         this.BuyShip.gotoAndStop("1");
         this.cancel_button.gotoAndStop("1");
         this.BuyShip.actionlabel.text = "Buy Ship";
         this.BuyShip.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ShipBuyButton);
         this.cancel_button.actionlabel.text = "Cancel";
         this.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_CancelShipBuy);
         this.AvailableShipsDisplay = new Array();
         this.PurchasePriceOfShip = 0;
         this.currentlySelectedShip = -1;
         this.func_InitBuyShipsDisplay();
         this.BuyQuestionBox.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_CancelShipPurchase);
         this.BuyQuestionBox.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_AcceptShipPurchase);
      }
      
      public function func_initLeftSideMenu(param1:*) : *
      {
         if(param1 == this.playerscurrentextrashipno)
         {
            this.changeShip.gotoAndStop("2");
            this.sellShip.gotoAndStop("2");
         }
         else if(this.WillNotHaveANonCapLeft(param1))
         {
            this.changeShip.gotoAndStop("2");
            this.sellShip.gotoAndStop("2");
         }
         else
         {
            this.changeShip.gotoAndStop("1");
            this.sellShip.gotoAndStop("1");
         }
      }
      
      public function displayBuyingShipDetail(param1:*) : *
      {
         this.currentlySelectedShip = param1;
         this.shipDetails1.text = "";
         var _loc2_:* = "";
         _loc2_ += "Ship Name:\r  " + this.shiptype[param1][0] + "\r";
         _loc2_ += "Cost: " + this.shiptype[param1][1] + "\r";
         _loc2_ += "Rotation Degrees / Second \r";
         _loc2_ += "  When Stationary: " + this.shiptype[param1][3][2] + " \r";
         _loc2_ += "  Loss at Full Speed:" + "\r" + "   %" + Number(this.shiptype[param1][3][5]) * 100 + " \r";
         _loc2_ += "  When Stationary: " + this.shiptype[param1][3][2] + " \r";
         _loc2_ += "Max Speed: " + this.shiptype[param1][3][1] + "/ sec \r";
         _loc2_ += "Aft. Speed: " + this.shiptype[param1][3][6] + "/ sec \r";
         _loc2_ += "Acceleration: " + this.shiptype[param1][3][0] + "/ sec \r";
         _loc2_ += "Max Structure: " + this.shiptype[param1][3][3] + " \r";
         _loc2_ += "Max Cargo: " + this.shiptype[param1][4] + " \r";
         this.shipDetails1.text = _loc2_;
         this.shipDetails2.text = this.displayMaxStatsDetail(param1);
      }
      
      public function func_AcceptShipSale(param1:MouseEvent) : void
      {
         this.playershipstatus[3][1] += this.SalePriceOfShip;
         this.extraplayerships[this.currentlySelectedExtraSlot] = new Array();
         this.extraplayerships[this.currentlySelectedExtraSlot][0] = null;
         this.func_InitShipsDisplay();
      }
      
      public function func_InitCurrentShipsDisplay() : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc1_:* = 0;
         while(_loc1_ < this.maxShipsToOwn)
         {
            this.OwnedShipsDisplay[_loc1_] = new Object();
            this.OwnedShipsDisplay[_loc1_].DisplayBox = new this.DisplayBox() as MovieClip;
            this.OwnedShipsDisplay[_loc1_].DisplayBox.name = _loc1_;
            this.OwnedShipsDisplay[_loc1_].DisplayBox.current.text = "";
            _loc2_ = true;
            if(this.extraplayerships[_loc1_] != null)
            {
               if(this.extraplayerships[_loc1_][0] != null)
               {
                  _loc2_ = false;
                  _loc3_ = this.extraplayerships[_loc1_][0];
                  this.OwnedShipsDisplay[_loc1_].DisplayBox.itemname.text = this.shiptype[_loc3_][0];
                  this.OwnedShipsDisplay[_loc1_].DisplayBox.itemspecs.text = "";
                  this.OwnedShipsDisplay[_loc1_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_extraShipSelected);
                  if(_loc1_ == this.playerscurrentextrashipno)
                  {
                     this.OwnedShipsDisplay[_loc1_].DisplayBox.current.text = "CURRENT SHIP";
                     this.OwnedShipsDisplay[_loc1_].DisplayBox.gotoAndStop(2);
                  }
                  (_loc4_ = new this.shiptype[_loc3_][10]() as MovieClip).x = Number(this.OwnedShipsDisplay[_loc1_].DisplayBox.width) / 2;
                  _loc4_.y = Number(this.OwnedShipsDisplay[_loc1_].DisplayBox.height) / 2;
                  this.OwnedShipsDisplay[_loc1_].DisplayBox.addChild(_loc4_);
                  _loc5_ = Number(this.OwnedShipsDisplay[_loc1_].DisplayBox.width) * 0.8;
                  _loc6_ = false;
                  if(_loc4_.width > _loc5_)
                  {
                     _loc7_ = _loc5_ / Number(_loc4_.width);
                     _loc4_.scaleX = _loc7_;
                     _loc4_.scaleY = _loc7_;
                  }
               }
            }
            if(_loc2_)
            {
               this.OwnedShipsDisplay[_loc1_].DisplayBox.itemname.text = "Empty Spot";
               this.OwnedShipsDisplay[_loc1_].DisplayBox.itemspecs.text = "Ship Slot \r Available";
               this.OwnedShipsDisplay[_loc1_].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN,this.func_PurchaseNewShip);
            }
            this.DisplayScreen.func_addItem(this.OwnedShipsDisplay[_loc1_].DisplayBox);
            _loc1_++;
         }
      }
      
      public function WillNotHaveANonCapLeft(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         if(this.extraplayerships[param1][0] >= this.capShipsStartAt)
         {
            trace("ran" + this.extraplayerships[param1][0] + "`" + this.capShipsStartAt);
            return false;
         }
         _loc2_ = false;
         _loc3_ = 0;
         while(_loc3_ < this.maxShipsToOwn)
         {
            if(this.extraplayerships[_loc3_] != null)
            {
               if(this.extraplayerships[_loc3_][0] != null)
               {
                  if(this.extraplayerships[_loc3_][0] < this.capShipsStartAt)
                  {
                     if(_loc2_)
                     {
                        trace("ran2" + this.extraplayerships[param1][0] + "`" + this.capShipsStartAt);
                        return false;
                     }
                     _loc2_ = true;
                     trace("ranholder" + _loc3_);
                  }
               }
            }
            _loc3_++;
         }
         trace("no run");
         return true;
      }
      
      public function func_ChangeCurrShip(param1:MouseEvent) : void
      {
         this.playerscurrentextrashipno = this.currentlySelectedExtraSlot;
         this.func_InitShipsDisplay();
      }
      
      internal function frame7() : *
      {
         this.exit_button.visible = true;
         this.changeShip.actionlabel.text = "Set Current Ship";
         this.sellShip.actionlabel.text = "Sell Ship";
         this.exit_button.actionlabel.text = "Exit";
         this.changeShip.gotoAndStop("2");
         this.sellShip.gotoAndStop("2");
         this.exit_button.gotoAndStop("1");
         this.questionBox.visible = false;
         this.DisplayScreen.func_clearAllItems();
         this.OwnedShipsDisplay = new Array();
         this.SalePriceOfShip = 0;
         this.func_InitShipsDisplay();
         this.changeShip.addEventListener(MouseEvent.MOUSE_DOWN,this.func_ChangeCurrShip);
         this.sellShip.addEventListener(MouseEvent.MOUSE_DOWN,this.func_SellCurrShip);
         this.questionBox.no_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_CancelShipSale);
         this.questionBox.yes_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_AcceptShipSale);
      }
      
      public function func_extraShipSelected(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(param1.target.parent.name);
         var _loc3_:* = 0;
         while(_loc3_ < this.OwnedShipsDisplay.length)
         {
            this.OwnedShipsDisplay[_loc3_].DisplayBox.gotoAndStop(1);
            _loc3_++;
         }
         param1.target.parent.gotoAndStop(2);
         this.currentlySelectedExtraSlot = _loc2_;
         this.displayShipDetail(this.currentlySelectedExtraSlot);
         this.func_initLeftSideMenu(this.currentlySelectedExtraSlot);
      }
      
      public function func_CancelShipBuy(param1:MouseEvent) : void
      {
         gotoAndStop("extraships");
      }
      
      public function func_InitShipsDisplay() : *
      {
         this.currentlySelectedExtraSlot = this.playerscurrentextrashipno;
         this.questionBox.visible = false;
         this.DisplayScreen.func_clearAllItems();
         this.func_InitCurrentShipsDisplay();
         this.shipDetails1.text = "";
         this.shipDetails2.text = "";
         this.displayShipDetail(this.currentlySelectedExtraSlot);
         this.func_initLeftSideMenu(this.currentlySelectedExtraSlot);
      }
   }
}
