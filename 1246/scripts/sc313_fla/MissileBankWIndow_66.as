package sc313_fla
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import flash.utils.getDefinitionByName;
   import flash.utils.getTimer;
   
   public dynamic class MissileBankWIndow_66 extends MovieClip
   {
       
      
      public var playersMissileBanks;
      
      public var BankHolderImage:Class;
      
      public var firingStyle;
      
      public var totalMissileTypes;
      
      public var fireStyle:MovieClip;
      
      public var SingleFireBank;
      
      public var BankDisplays;
      
      public var holderDisplay:bankDisplayInfo;
      
      public var changingBankMissileType;
      
      public var missile;
      
      public function MissileBankWIndow_66()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function func_searchForNextMissile(param1:*, param2:*) : *
      {
         var _loc3_:* = 0;
         _loc3_ = param2 + 1;
         while(_loc3_ != param2)
         {
            if(_loc3_ >= this.totalMissileTypes)
            {
               _loc3_ = -1;
            }
            else if(this.playersMissileBanks[param1][4][_loc3_] > 0)
            {
               this.playersMissileBanks[param1][0] = _loc3_;
               break;
            }
            _loc3_++;
         }
      }
      
      public function func_buildDisplay(param1:*, param2:*, param3:*) : *
      {
         this.BankHolderImage = getDefinitionByName("bankDisplayInfo") as Class;
         this.playersMissileBanks = param1;
         this.SingleFireBank = param2;
         this.missile = param3;
         this.totalMissileTypes = this.missile.length;
         this.BankDisplays = new Array();
         this.changingBankMissileType = new Array();
         var _loc4_:* = 0;
         _loc4_ = 0;
         while(_loc4_ < this.playersMissileBanks.length)
         {
            this.changingBankMissileType[_loc4_] = false;
            this.BankDisplays[_loc4_] = new Object();
            this.BankDisplays[_loc4_] = new this.BankHolderImage() as MovieClip;
            this.BankDisplays[_loc4_].x = 0;
            this.BankDisplays[_loc4_].y = this.holderDisplay.height * _loc4_ * 0.5;
            this.BankDisplays[_loc4_].name = _loc4_;
            this.BankDisplays[_loc4_].changeButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_MissileTypeChange);
            this.BankDisplays[_loc4_].missilereadiness.addEventListener(MouseEvent.MOUSE_DOWN,this.func_MissileSingleBankChange);
            addChild(this.BankDisplays[_loc4_]);
            _loc4_++;
         }
      }
      
      public function func_MissileSingleBankChange(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(param1.target.parent.name);
         this.SingleFireBank = _loc2_;
      }
      
      public function func_RefreshDisplay(param1:*, param2:*, param3:*) : *
      {
         var _loc6_:* = undefined;
         var _loc4_:* = getTimer();
         if(this.SingleFireBank != param2)
         {
            param2 = this.SingleFireBank;
         }
         this.playersMissileBanks = param1;
         if(param3 == "Single")
         {
            this.fireStyle.gotoAndStop(1);
         }
         else
         {
            this.fireStyle.gotoAndStop(2);
         }
         var _loc5_:* = 0;
         _loc5_ = 0;
         while(_loc5_ < this.playersMissileBanks.length)
         {
            if(this.playersMissileBanks[_loc5_][4][this.playersMissileBanks[_loc5_][0]] < 1 || Boolean(this.changingBankMissileType[_loc5_]))
            {
               this.changingBankMissileType[_loc5_] = false;
               this.func_searchForNextMissile(_loc5_,this.playersMissileBanks[_loc5_][0]);
            }
            _loc6_ = param1[_loc5_][4][this.playersMissileBanks[_loc5_][0]];
            this.BankDisplays[_loc5_].bankdisplay.text = _loc5_ + ": " + this.missile[this.playersMissileBanks[_loc5_][0]][7] + "(" + _loc6_ + ")";
            this.BankDisplays[_loc5_].missilereadiness.gotoAndStop("ready");
            if(_loc6_ < 1)
            {
               this.BankDisplays[_loc5_].missilereadiness.gotoAndStop("noammo");
            }
            else if(this.playersMissileBanks[_loc5_][1] > _loc4_)
            {
               this.BankDisplays[_loc5_].missilereadiness.gotoAndStop("reload");
            }
            else if(param3 == "Single")
            {
               if(param2 != _loc5_)
               {
                  this.BankDisplays[_loc5_].missilereadiness.gotoAndStop("noton");
               }
            }
            _loc5_++;
         }
         return param2;
      }
      
      internal function frame1() : *
      {
         this.holderDisplay.visible = false;
         stop();
      }
      
      public function func_MissileTypeChange(param1:MouseEvent) : void
      {
         var _loc2_:* = Number(param1.target.parent.name);
         trace(_loc2_);
         trace(this.changingBankMissileType);
         trace(this.changingBankMissileType[_loc2_]);
         this.changingBankMissileType[_loc2_] = true;
      }
   }
}
