package sc313_fla
{
   import flash.display.MovieClip;
   import flash.display.SimpleButton;
   import flash.events.KeyboardEvent;
   import flash.events.MouseEvent;
   import flash.text.TextField;
   
   public dynamic class login_m_login_36 extends MovieClip
   {
       
      
      public var memberpassword1;
      
      public var memberpassword2;
      
      public var mysocket;
      
      public var mov_signup:MovieClip;
      
      public var mov_guests:MovieClip;
      
      public var emailtoSub;
      
      public var memberpassword;
      
      public var logindisplay:MovieClip;
      
      public var isAGuest;
      
      public var membernameinput;
      
      public var mov_player_signup:MovieClip;
      
      public var returnpassbutt:SimpleButton;
      
      public var RelayMessage:TextField;
      
      public var curentinputfield;
      
      public var hascreationbeensubmited;
      
      public var mov_members:MovieClip;
      
      public var SentGuestRequest;
      
      public var hasloginbeensubmited;
      
      public function login_m_login_36()
      {
         super();
         addFrameScript(0,this.frame1,4,this.frame5,11,this.frame12,19,this.frame20,29,this.frame30,30,this.frame31,35,this.frame36,38,this.frame39,39,this.frame40,40,this.frame41,45,this.frame46,48,this.frame49,50,this.frame51,51,this.frame52,56,this.frame57,59,this.frame60);
      }
      
      public function myListener(param1:KeyboardEvent) : void
      {
         if(param1.keyCode == 13)
         {
            this.func_submit();
         }
      }
      
      public function func_initializeCreateACcount() : *
      {
         addEventListener(KeyboardEvent.KEY_DOWN,this.myCreationListener);
      }
      
      internal function frame31() : *
      {
      }
      
      public function accountcreation(param1:*, param2:*, param3:*) : *
      {
         this.mysocket.send("ACCT`CREATE`" + param1 + "`" + param2 + "`" + param3 + "`~");
      }
      
      public function func_trytologin(param1:*, param2:*) : *
      {
         param1 = param1.toUpperCase();
         param2 = param2.toUpperCase();
         if(param1.length > 11 || param1.length < 3)
         {
            this.func_RelayingMessage("Name must be at least 3 and no more than 11 Characters");
         }
         else if(param2.length > 11 || param2.length < 3)
         {
            this.func_RelayingMessage("Password must be at least 3 and no more than 11 Characters");
         }
         else if(this.checknameforlegitcharacters(param1))
         {
            this.func_RelayingMessage("Enter a Name that consists of only letters a-Z,and numbers");
         }
         else if(this.checknameforlegitcharacters(param2))
         {
            this.func_RelayingMessage("Enter a Passwrod that consists of only letters a-Z,and numbers");
         }
         else if(this.hasloginbeensubmited != true)
         {
            this.hasloginbeensubmited = true;
            this.mysocket.send("ACCT`LOGIN`" + param1 + "`" + param2 + "`~");
            this.func_RelayingMessage("Sending Login Request");
         }
         else
         {
            this.func_RelayingMessage("Still Trying to Login");
         }
      }
      
      internal function frame36() : *
      {
         this.mov_members.gotoAndStop(2);
      }
      
      internal function frame30() : *
      {
         this.mov_members.thebutton.addEventListener(MouseEvent.MOUSE_UP,function(param1:MouseEvent):void
         {
            gotoAndPlay("members");
         });
         this.mov_player_signup.thebutton.addEventListener(MouseEvent.MOUSE_UP,function(param1:MouseEvent):void
         {
            gotoAndPlay("signup");
         });
         this.mov_guests.thebutton.addEventListener(MouseEvent.MOUSE_UP,function(param1:MouseEvent):void
         {
            gotoAndPlay("guests");
         });
         stop();
      }
      
      internal function frame39() : *
      {
         this.isAGuest = false;
         this.mov_members.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:MouseEvent):void
         {
            gotoAndPlay(1);
         });
         this.curentinputfield = "name";
         this.func_initialize();
         this.memberpassword = "";
         stop();
      }
      
      public function myGuestistener(param1:KeyboardEvent) : void
      {
         if(param1.keyCode == 13)
         {
            this.func_Guestsubmit();
            trace("RAN");
         }
      }
      
      internal function frame41() : *
      {
      }
      
      public function func_zoneconnectionbeenmade() : *
      {
         this.logindisplay.gotoAndStop("beginentering");
      }
      
      public function func_login(param1:*) : *
      {
         param1 = param1.toUpperCase();
         if(this.SentGuestRequest)
         {
            this.func_RelayingMessage("Already Logging in as Guest");
         }
         else
         {
            this.func_RelayingMessage("Logging in as Guest");
            this.SentGuestRequest = true;
            this.mysocket.send("ACCT`LOGIN`" + param1 + "`" + "#GUEST" + "~");
         }
      }
      
      internal function frame46() : *
      {
         this.mov_signup.gotoAndStop(2);
      }
      
      internal function frame40() : *
      {
         this.mov_members.gotoAndStop(3);
         this.logindisplay.mysocket = this.mysocket;
         stop();
      }
      
      public function func_failedLogin() : *
      {
         this.mov_members.username.text = "";
         this.mov_members.userpass.text = "";
         this.hasloginbeensubmited = false;
         this.func_RelayingMessage("Login Failed\rPlease try again.");
      }
      
      internal function frame49() : *
      {
         this.isAGuest = false;
         this.hascreationbeensubmited = false;
         this.mov_signup.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:MouseEvent):void
         {
            gotoAndPlay(1);
         });
         this.func_initializeCreateACcount();
         stop();
      }
      
      public function func_failedCreate(param1:*) : *
      {
         this.hasloginbeensubmited = false;
         this.func_RelayingMessage("Login Failed\rPlease try again.");
      }
      
      public function func_NameExists() : *
      {
         this.hascreationbeensubmited = false;
         this.func_RelayingMessage("Name Exists, Try Another");
      }
      
      public function func_NameCreated() : *
      {
         this.func_RelayingMessage("Name Created, Please Wait");
      }
      
      internal function frame52() : *
      {
      }
      
      public function checknameforlegitcharacters(param1:*) : *
      {
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc2_:* = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
         var _loc3_:* = 0;
         while(_loc3_ < param1.length)
         {
            _loc4_ = true;
            _loc5_ = 0;
            while(_loc5_ < _loc2_.length)
            {
               if(param1.charAt(_loc3_) == _loc2_.charAt(_loc5_))
               {
                  _loc4_ = false;
               }
               _loc5_++;
            }
            if(_loc4_ == true)
            {
               return true;
            }
            _loc3_++;
         }
         return false;
      }
      
      internal function frame12() : *
      {
      }
      
      internal function frame57() : *
      {
         this.mov_guests.gotoAndStop(2);
      }
      
      internal function frame51() : *
      {
         stop();
      }
      
      public function Incoming_Creation_Data(param1:*) : *
      {
         var _loc2_:* = String(param1);
         if(_loc2_ == "nameexists")
         {
            this.func_RelayingMessage("Name Already Owned, Please Try A Different Name");
            this.hascreationbeensubmited = false;
         }
         else if(_loc2_ == "namecreated")
         {
            this.func_RelayingMessage("Account Created, Logging in");
            this.mysocket.send("ACCT`LOGIN`" + this.membernameinput + "`" + this.memberpassword1 + "`~");
         }
      }
      
      public function func_loginaccepted() : *
      {
         this.func_RelayingMessage("");
         gotoAndStop("memberconenct");
      }
      
      public function func_Guestinitialize() : *
      {
         addEventListener(KeyboardEvent.KEY_DOWN,this.myGuestistener);
      }
      
      internal function frame60() : *
      {
         this.isAGuest = true;
         this.SentGuestRequest = false;
         this.mov_guests.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:MouseEvent):void
         {
            gotoAndPlay(1);
         });
         this.func_Guestinitialize();
         stop();
      }
      
      internal function frame5() : *
      {
      }
      
      public function func_initialize() : *
      {
         addEventListener(KeyboardEvent.KEY_DOWN,this.myListener);
      }
      
      public function func_Guestsubmit() : *
      {
         var _loc1_:* = this.mov_guests.GuestName.text;
         if(_loc1_ != "")
         {
            this.func_login(_loc1_);
         }
      }
      
      public function func_submit() : *
      {
         this.membernameinput = this.mov_members.username.text;
         this.memberpassword = this.mov_members.userpass.text;
         if(this.membernameinput != "")
         {
            if(this.memberpassword != "")
            {
               this.func_trytologin(this.membernameinput,this.memberpassword);
            }
            else
            {
               this.func_RelayingMessage("Password Required");
            }
         }
         else
         {
            this.func_RelayingMessage("Name Required");
         }
      }
      
      public function checkEmail(param1:String) : Boolean
      {
         if(param1.indexOf(" ") > 0)
         {
            return false;
         }
         var _loc2_:Array = param1.split("@");
         if(_loc2_.length != 2 || _loc2_[0].length == 0 || _loc2_[1].length == 0)
         {
            return false;
         }
         var _loc3_:Array = _loc2_[1].split(".");
         if(_loc3_.length < 2)
         {
            return false;
         }
         var _loc4_:Number = 0;
         while(_loc4_ < _loc3_.length)
         {
            if(_loc3_[_loc4_].length < 1)
            {
               return false;
            }
            _loc4_++;
         }
         var _loc5_:*;
         if((_loc5_ = _loc3_[_loc3_.length - 1]).length < 2 || _loc5_.length > 3)
         {
            return false;
         }
         return true;
      }
      
      public function func_createaccount(param1:*, param2:*, param3:*, param4:*) : *
      {
         param1 = param1.toUpperCase();
         param2 = param2.toUpperCase();
         param3 = param3.toUpperCase();
         var _loc5_:* = param4.toLowerCase();
         var _loc6_:* = true;
         if(_loc5_.length > 0)
         {
            _loc6_ = this.checkEmail(_loc5_);
         }
         if(param1.length > 11 || param1.length < 3)
         {
            this.func_RelayingMessage("Name must be at least 3 and no more than 11 Characters");
         }
         else if(param1 == "HOST")
         {
            this.func_RelayingMessage("Illegal Name");
         }
         else if(this.checknameforlegitcharacters(param1))
         {
            this.func_RelayingMessage("Enter a Name that consists of only letters a-Z, and numbers");
         }
         else if(param2.length > 10 || param2.length < 3)
         {
            this.func_RelayingMessage("Passwords must be at least 3 characters and no more than 10 characters");
         }
         else if(this.checknameforlegitcharacters(param2))
         {
            this.func_RelayingMessage("Passwords consist of only letters a-Z, and numbers");
         }
         else if(param3.toUpperCase() != param2.toUpperCase())
         {
            this.func_RelayingMessage("Passwords do not match");
         }
         else if(!_loc6_)
         {
            this.func_RelayingMessage("Non-ValidEmail");
         }
         else
         {
            this.func_RelayingMessage("Creating Account");
            if(this.hascreationbeensubmited != true)
            {
               this.hascreationbeensubmited = true;
               this.accountcreation(param1,param2,param4);
            }
            else
            {
               this.func_RelayingMessage("Still Creating Your Account, Please Wait");
            }
         }
      }
      
      public function func_submitCreatAccount() : *
      {
         this.membernameinput = this.mov_signup.UserName.text;
         this.memberpassword1 = this.mov_signup.password1.text;
         this.memberpassword2 = this.mov_signup.password2.text;
         this.emailtoSub = this.mov_signup.UserEmail.text;
         if(this.membernameinput != "")
         {
            if(this.memberpassword1 != "")
            {
               if(this.memberpassword2 != "")
               {
                  this.func_createaccount(this.membernameinput,this.memberpassword1,this.memberpassword2,this.emailtoSub);
               }
               else
               {
                  this.func_RelayingMessage("Verify Password.");
               }
            }
            else
            {
               this.func_RelayingMessage("Enter a Password.");
            }
         }
         else
         {
            this.func_RelayingMessage("Enter a Name.");
         }
      }
      
      internal function frame20() : *
      {
      }
      
      public function func_RelayingMessage(param1:*) : *
      {
         this.RelayMessage.visible = true;
         this.RelayMessage.text = param1;
         this.hascreationbeensubmited = false;
      }
      
      internal function frame1() : *
      {
         this.SentGuestRequest = false;
         this.hascreationbeensubmited = false;
         this.RelayMessage.text = "";
         this.hasloginbeensubmited = false;
         this.isAGuest = false;
      }
      
      public function myCreationListener(param1:KeyboardEvent) : void
      {
         if(param1.keyCode == 13)
         {
            this.func_submitCreatAccount();
         }
      }
   }
}
