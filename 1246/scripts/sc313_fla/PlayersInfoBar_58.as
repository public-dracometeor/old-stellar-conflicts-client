package sc313_fla
{
   import flash.display.MovieClip;
   
   public dynamic class PlayersInfoBar_58 extends MovieClip
   {
       
      
      public var structurebar:MovieClip;
      
      public var playersmaxstructure;
      
      public var energy;
      
      public var shieldbar:MovieClip;
      
      public var energybar:MovieClip;
      
      public var maxshieldstrength;
      
      public var currentvelocity;
      
      public var maxvelocity;
      
      public var velocitybar:MovieClip;
      
      public var shield;
      
      public var maxenergy;
      
      public var structure;
      
      public function PlayersInfoBar_58()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function showenergy(param1:*) : *
      {
         var _loc2_:* = Math.ceil(param1 / this.maxenergy * 28);
         if(_loc2_ < 1)
         {
            _loc2_ = 1;
         }
         this.energybar.gotoAndStop(_loc2_);
      }
      
      public function func_showStruct(param1:*) : *
      {
         var _loc2_:* = Math.ceil(param1 / this.playersmaxstructure * 28);
         if(_loc2_ < 1)
         {
            _loc2_ = 1;
         }
         this.structurebar.gotoAndStop(_loc2_);
      }
      
      public function func_showVelocity(param1:*) : *
      {
         var _loc2_:* = undefined;
         if(param1 <= 0.4)
         {
            this.velocitybar.gotoAndStop(1);
         }
         else if(param1 <= this.maxvelocity)
         {
            _loc2_ = Math.ceil(param1 / this.maxvelocity * 24);
            this.velocitybar.gotoAndStop(_loc2_);
         }
         else
         {
            this.velocitybar.gotoAndStop(25);
         }
         if(param1 < 0)
         {
         }
      }
      
      internal function frame1() : *
      {
         this.energybar.gotoAndStop(28);
         this.energy = 1;
         this.shield = 1;
         this.shieldbar.gotoAndStop(28);
         this.structure = 1;
         this.structurebar.gotoAndStop(28);
         this.currentvelocity = 0;
         this.velocitybar.gotoAndStop(1);
      }
      
      public function showshield(param1:*) : *
      {
         var _loc2_:* = Math.ceil(param1 / this.maxshieldstrength * 28);
         if(_loc2_ < 1)
         {
            _loc2_ = 1;
         }
         this.shieldbar.gotoAndStop(_loc2_);
      }
      
      public function func_displaystats(param1:*, param2:*, param3:*, param4:*) : *
      {
         this.showenergy(param1);
         this.showshield(param2);
         this.func_showStruct(param3);
         this.func_showVelocity(param4);
      }
   }
}
