package sc313_fla
{
   import flash.display.MovieClip;
   
   public dynamic class regcapshipengine_156 extends MovieClip
   {
       
      
      public function regcapshipengine_156()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2);
      }
      
      internal function frame1() : *
      {
      }
      
      internal function frame2() : *
      {
         stop();
      }
      
      public function setSpeedRatio(param1:*) : *
      {
         var _loc2_:* = Math.ceil(param1 * 10 / 2) + 2;
         if(_loc2_ > 8)
         {
            _loc2_ = 8;
         }
         this.gotoAndStop(_loc2_);
      }
   }
}
