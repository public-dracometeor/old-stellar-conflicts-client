package sc313_fla
{
   import flash.display.MovieClip;
   import flash.display.SimpleButton;
   import flash.text.TextField;
   
   public dynamic class login_m_guestlogin_46 extends MovieClip
   {
       
      
      public var GuestName:TextField;
      
      public var mov_name:MovieClip;
      
      public var cancel_button:SimpleButton;
      
      public var thebutton:SimpleButton;
      
      public function login_m_guestlogin_46()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2);
      }
      
      internal function frame2() : *
      {
         this.GuestName.text = "GUEST" + Math.round(Math.random() * 9999);
      }
      
      internal function frame1() : *
      {
         stop();
      }
   }
}
