package sc313_fla
{
   import flash.display.MovieClip;
   import flash.text.TextField;
   
   public dynamic class scoreDisp_72 extends MovieClip
   {
       
      
      public var score:TextField;
      
      public function scoreDisp_72()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function func_displayScore(param1:*) : *
      {
         this.score.text = "Score: " + param1;
      }
      
      internal function frame1() : *
      {
         this.score.text = "";
         stop();
      }
   }
}
