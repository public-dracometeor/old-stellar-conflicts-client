package sc313_fla
{
   import flash.display.MovieClip;
   import flash.text.TextField;
   
   public dynamic class CreditsDisp_71 extends MovieClip
   {
       
      
      public var credits:TextField;
      
      public function CreditsDisp_71()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function func_displayCredits(param1:*) : *
      {
         this.credits.text = "Credits: " + param1;
      }
      
      internal function frame1() : *
      {
         this.credits.text = "";
         stop();
      }
   }
}
