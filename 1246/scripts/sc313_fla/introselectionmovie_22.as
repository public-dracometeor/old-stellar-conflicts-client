package sc313_fla
{
   import flash.display.MovieClip;
   import flash.display.SimpleButton;
   import flash.events.MouseEvent;
   import flash.text.TextField;
   
   public dynamic class introselectionmovie_22 extends MovieClip
   {
       
      
      public var deaccel:TextField;
      
      public var back_button:SimpleButton;
      
      public var target:TextField;
      
      public var playGameButton:SimpleButton;
      
      public var continuebutton:SimpleButton;
      
      public var oldkeys:SimpleButton;
      
      public var gamesetting;
      
      public var zoomout:TextField;
      
      public var newkeys:SimpleButton;
      
      public var missile:TextField;
      
      public var afterburn:TextField;
      
      public var tright:TextField;
      
      public var dock:TextField;
      
      public var guns:TextField;
      
      public var isFinished;
      
      public var zoomin:TextField;
      
      public var help:TextField;
      
      public var forumsButton:SimpleButton;
      
      public var chat:TextField;
      
      public var map:TextField;
      
      public var currkeystyle:TextField;
      
      public var accel:TextField;
      
      public var highsetting:SimpleButton;
      
      public var medsettings:SimpleButton;
      
      public var playersList:TextField;
      
      public var tleft:TextField;
      
      public var lowsettings:SimpleButton;
      
      public function introselectionmovie_22()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2,2,this.frame3,3,this.frame4,4,this.frame5,5,this.frame6,6,this.frame7);
      }
      
      internal function frame5() : *
      {
         this.back_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_PreviousFrame);
         this.continuebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_NextFrame);
         stop();
      }
      
      public function func_PlayGame(param1:MouseEvent) : void
      {
         this.isFinished = true;
      }
      
      internal function frame3() : *
      {
         Object(root).gamesetting = this.gamesetting;
         this.currkeystyle.text = "New Style";
         this.displaykeys();
         stop();
         this.newkeys.addEventListener(MouseEvent.MOUSE_DOWN,this.func_SetToNew);
         this.oldkeys.addEventListener(MouseEvent.MOUSE_DOWN,this.func_SetToOld);
         this.continuebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_NextFrame);
         this.playGameButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_PlayGame);
         this.forumsButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_loadForums);
      }
      
      public function func_HighSettings(param1:MouseEvent) : void
      {
         this.gamesetting.scrollingbckgrnd = true;
         this.gamesetting.showbckgrnd = true;
         this.gamesetting.totalstars = 20;
         this.gamesetting.soundvolume = "on";
         nextFrame();
      }
      
      public function func_SetToNew(param1:MouseEvent) : void
      {
         this.currkeystyle.text = "New Style";
         Object(root).func_setToNewDefaultGameKeys();
         this.displaykeys();
      }
      
      public function func_SetToOld(param1:MouseEvent) : void
      {
         this.currkeystyle.text = "Old Style";
         Object(root).func_setToOldGameKeys();
         this.displaykeys();
      }
      
      public function returnKeyValueOfCode(param1:*) : *
      {
         var _loc2_:* = "";
         if(param1 == 65)
         {
            _loc2_ = "A";
         }
         else if(param1 == 66)
         {
            _loc2_ = "B";
         }
         else if(param1 == 67)
         {
            _loc2_ = "C";
         }
         else if(param1 == 68)
         {
            _loc2_ = "D";
         }
         else if(param1 == 69)
         {
            _loc2_ = "E";
         }
         else if(param1 == 70)
         {
            _loc2_ = "F";
         }
         else if(param1 == 71)
         {
            _loc2_ = "G";
         }
         else if(param1 == 72)
         {
            _loc2_ = "H";
         }
         else if(param1 == 73)
         {
            _loc2_ = "I";
         }
         else if(param1 == 74)
         {
            _loc2_ = "J";
         }
         else if(param1 == 75)
         {
            _loc2_ = "K";
         }
         else if(param1 == 76)
         {
            _loc2_ = "L";
         }
         else if(param1 == 77)
         {
            _loc2_ = "M";
         }
         else if(param1 == 78)
         {
            _loc2_ = "N";
         }
         else if(param1 == 79)
         {
            _loc2_ = "O";
         }
         else if(param1 == 80)
         {
            _loc2_ = "P";
         }
         else if(param1 == 81)
         {
            _loc2_ = "Q";
         }
         else if(param1 == 82)
         {
            _loc2_ = "R";
         }
         else if(param1 == 83)
         {
            _loc2_ = "S";
         }
         else if(param1 == 84)
         {
            _loc2_ = "T";
         }
         else if(param1 == 85)
         {
            _loc2_ = "U";
         }
         else if(param1 == 86)
         {
            _loc2_ = "V";
         }
         else if(param1 == 87)
         {
            _loc2_ = "W";
         }
         else if(param1 == 88)
         {
            _loc2_ = "X";
         }
         else if(param1 == 89)
         {
            _loc2_ = "Y";
         }
         else if(param1 == 90)
         {
            _loc2_ = "Z";
         }
         else if(param1 == 48)
         {
            _loc2_ = "0";
         }
         else if(param1 == 49)
         {
            _loc2_ = "1";
         }
         else if(param1 == 50)
         {
            _loc2_ = "2";
         }
         else if(param1 == 51)
         {
            _loc2_ = "3";
         }
         else if(param1 == 52)
         {
            _loc2_ = "4";
         }
         else if(param1 == 53)
         {
            _loc2_ = "5";
         }
         else if(param1 == 54)
         {
            _loc2_ = "6";
         }
         else if(param1 == 55)
         {
            _loc2_ = "7";
         }
         else if(param1 == 56)
         {
            _loc2_ = "8";
         }
         else if(param1 == 57)
         {
            _loc2_ = "9";
         }
         else if(param1 == 8)
         {
            _loc2_ = "Backspace";
         }
         else if(param1 == 9)
         {
            _loc2_ = "Tab";
         }
         else if(param1 == 13)
         {
            _loc2_ = "Enter";
         }
         else if(param1 == 16)
         {
            _loc2_ = "Shift";
         }
         else if(param1 == 17)
         {
            _loc2_ = "Control";
         }
         else if(param1 == 20)
         {
            _loc2_ = "Caps Lock";
         }
         else if(param1 == 27)
         {
            _loc2_ = "Escape";
         }
         else if(param1 == 32)
         {
            _loc2_ = "Space Bar";
         }
         else if(param1 == 33)
         {
            _loc2_ = "Page Up";
         }
         else if(param1 == 34)
         {
            _loc2_ = "Page Down";
         }
         else if(param1 == 35)
         {
            _loc2_ = "End";
         }
         else if(param1 == 36)
         {
            _loc2_ = "Home";
         }
         else if(param1 == 37)
         {
            _loc2_ = "Left Arrow";
         }
         else if(param1 == 38)
         {
            _loc2_ = "Up Arrow";
         }
         else if(param1 == 39)
         {
            _loc2_ = "Right Arrow";
         }
         else if(param1 == 40)
         {
            _loc2_ = "Down Arrow";
         }
         else if(param1 == 45)
         {
            _loc2_ = "Insert";
         }
         else if(param1 == 46)
         {
            _loc2_ = "Delete";
         }
         else if(param1 == 144)
         {
            _loc2_ = "Num Lock";
         }
         else if(param1 == 145)
         {
            _loc2_ = "ScrLck";
         }
         else if(param1 == 19)
         {
            _loc2_ = "Pause/Break";
         }
         else if(param1 == 186)
         {
            _loc2_ = "; or :";
         }
         else if(param1 == 187)
         {
            _loc2_ = "= or +";
         }
         else if(param1 == 189)
         {
            _loc2_ = "_ or -";
         }
         else if(param1 == 191)
         {
            _loc2_ = "/ or ?";
         }
         else if(param1 == 192)
         {
            _loc2_ = "` or ~";
         }
         else if(param1 == 219)
         {
            _loc2_ = "[ or {";
         }
         else if(param1 == 220)
         {
            _loc2_ = " or |";
         }
         else if(param1 == 221)
         {
            _loc2_ = "] or }";
         }
         else if(param1 == 222)
         {
            _loc2_ = "\" of \'";
         }
         else if(param1 == 188)
         {
            _loc2_ = ", or <";
         }
         else if(param1 == 190)
         {
            _loc2_ = ". or >";
         }
         else if(param1 == 191)
         {
            _loc2_ = "/ or ?";
         }
         return _loc2_;
      }
      
      public function SetKeyConfiguration(param1:*) : *
      {
         this.displaykeys();
      }
      
      public function displaykeys() : *
      {
         this.gamesetting = Object(root).gamesetting;
         this.accel.text = this.returnKeyValueOfCode(this.gamesetting.accelkey);
         this.tright.text = this.returnKeyValueOfCode(this.gamesetting.turnrightkey);
         this.tleft.text = this.returnKeyValueOfCode(this.gamesetting.turnleftkey);
         this.deaccel.text = this.returnKeyValueOfCode(this.gamesetting.deaccelkey);
         this.afterburn.text = this.returnKeyValueOfCode(this.gamesetting.afterburnerskey);
         this.guns.text = this.returnKeyValueOfCode(this.gamesetting.gunskey);
         this.missile.text = this.returnKeyValueOfCode(this.gamesetting.missilekey);
         this.dock.text = this.returnKeyValueOfCode(this.gamesetting.dockkey);
         this.target.text = this.returnKeyValueOfCode(this.gamesetting.targeterkey);
         this.map.text = this.returnKeyValueOfCode(this.gamesetting.MapKey);
         this.playersList.text = "O";
         this.chat.text = "ENTER";
         this.zoomin.text = ", or < ";
         this.zoomout.text = ". or > ";
         this.help.text = "F12";
      }
      
      public function func_loadForums(param1:MouseEvent) : void
      {
      }
      
      public function func_PreviousFrame(param1:MouseEvent) : void
      {
         prevFrame();
      }
      
      public function func_NextFrame(param1:MouseEvent) : void
      {
         nextFrame();
      }
      
      internal function frame2() : *
      {
         nextFrame();
      }
      
      internal function frame4() : *
      {
         this.back_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_PreviousFrame);
         this.continuebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_NextFrame);
         stop();
      }
      
      internal function frame6() : *
      {
         this.back_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_PreviousFrame);
         this.continuebutton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_NextFrame);
         stop();
      }
      
      internal function frame7() : *
      {
         this.back_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_PreviousFrame);
         this.playGameButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_PlayGame);
         stop();
      }
      
      internal function frame1() : *
      {
         this.isFinished = false;
         this.gamesetting = Object(root).gamesetting;
         this.highsetting.addEventListener(MouseEvent.MOUSE_DOWN,this.func_HighSettings);
         this.medsettings.addEventListener(MouseEvent.MOUSE_DOWN,this.func_MedSettings);
         this.lowsettings.addEventListener(MouseEvent.MOUSE_DOWN,this.func_LowSettings);
         stop();
      }
      
      public function func_MedSettings(param1:MouseEvent) : void
      {
         this.gamesetting.scrollingbckgrnd = false;
         this.gamesetting.showbckgrnd = true;
         this.gamesetting.totalstars = 15;
         this.gamesetting.soundvolume = "on";
         nextFrame();
      }
      
      public function func_LowSettings(param1:MouseEvent) : void
      {
         this.gamesetting.scrollingbckgrnd = false;
         this.gamesetting.showbckgrnd = false;
         this.gamesetting.totalstars = 5;
         this.gamesetting.soundvolume = "off";
         nextFrame();
      }
   }
}
