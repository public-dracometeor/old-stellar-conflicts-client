package sc313_fla
{
   import flash.display.MovieClip;
   
   public dynamic class loginscreen_35 extends MovieClip
   {
       
      
      public var counter;
      
      public var mov_login:MovieClip;
      
      public var mysocket;
      
      public function loginscreen_35()
      {
         super();
         addFrameScript(0,this.frame1,1,this.frame2,2,this.frame3,16,this.frame17,64,this.frame65);
      }
      
      internal function frame1() : *
      {
         this.counter = 101;
         this.scaleX = 0.6;
         this.scaleY = 0.6;
      }
      
      internal function frame2() : *
      {
         if(this.counter > 100)
         {
            this.mysocket.send("GAMEINFO`~");
            this.counter = 0;
         }
      }
      
      internal function frame3() : *
      {
         ++this.counter;
         gotoAndPlay(2);
      }
      
      internal function frame65() : *
      {
         this.mov_login.mysocket = this.mysocket;
         stop();
      }
      
      internal function frame17() : *
      {
      }
   }
}
