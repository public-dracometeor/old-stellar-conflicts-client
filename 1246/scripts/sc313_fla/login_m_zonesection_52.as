package sc313_fla
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import flash.text.TextField;
   
   public dynamic class login_m_zonesection_52 extends MovieClip
   {
       
      
      public var zoneselected;
      
      public var players:TextField;
      
      public var enterzonebutton:MovieClip;
      
      public var zonename:TextField;
      
      public var mysocket;
      
      public function login_m_zonesection_52()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      internal function frame1() : *
      {
         this.enterzonebutton.addEventListener(MouseEvent.MOUSE_UP,function(param1:MouseEvent):void
         {
            trace("test" + zoneselected + "`" + mysocket);
            mysocket.send("ZONE`" + zoneselected + "~");
         });
         stop();
      }
   }
}
