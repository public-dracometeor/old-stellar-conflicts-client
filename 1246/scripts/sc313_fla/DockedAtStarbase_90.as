package sc313_fla
{
   import flash.display.MovieClip;
   
   public dynamic class DockedAtStarbase_90 extends MovieClip
   {
       
      
      public var hangarButton:MovieClip;
      
      public var base_hardware_button:MovieClip;
      
      public var base_exit_button:MovieClip;
      
      public var zoneinterface:MovieClip;
      
      public function DockedAtStarbase_90()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      internal function frame1() : *
      {
         stop();
      }
   }
}
