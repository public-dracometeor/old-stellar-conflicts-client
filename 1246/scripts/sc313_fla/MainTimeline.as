package sc313_fla
{
   import flash.display.MovieClip;
   import flash.display.SimpleButton;
   import flash.events.DataEvent;
   import flash.events.Event;
   import flash.events.FocusEvent;
   import flash.events.IOErrorEvent;
   import flash.events.KeyboardEvent;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.net.XMLSocket;
   import flash.text.TextField;
   import flash.utils.Timer;
   import flash.utils.getDefinitionByName;
   import flash.utils.getTimer;
   
   public dynamic class MainTimeline extends MovieClip
   {
       
      
      public var gunShotBufferData;
      
      public var gameSettingScreen:MovieClip;
      
      public var maxextraships;
      
      public var playerfunds;
      
      public var gameMap:ingamemap;
      
      public var isPlayerChatting;
      
      public var mysocket;
      
      public var credDisplayTimer:Timer;
      
      public var baseshieldgendrain;
      
      public var speedratio;
      
      public var shieldgenerators;
      
      public var systemchattextcolor;
      
      public var missiontoactualscoremodifier;
      
      public var gameerrordoneby;
      
      public var PlayerStatDisp:MovieClip;
      
      public var currentPIpacket;
      
      public var squadchattextcolor;
      
      public var TargetTimer:Timer;
      
      public var controlledserverclose;
      
      public var starbasestayhostiletime;
      
      public var lastMissileSeek;
      
      public var halfgameareaheight;
      
      public var halfgameareawidth;
      
      public var isplayeremp;
      
      public var currentip;
      
      public var InfoResendDelay;
      
      public var hasClockBeenSet;
      
      public var playerrotationdegredation;
      
      public var otherplayerdockedon;
      
      public var SpecialsTimer:Timer;
      
      public var HalfBackgroundWidth;
      
      public var tradegoods;
      
      public var playerjustloggedin;
      
      public var guntype;
      
      public var shipDeadImage;
      
      public var spacekeyjustpressed;
      
      public var backgroundstar;
      
      public var playershipvelocity;
      
      public var energygenerators;
      
      public var othershipbuffer;
      
      public var remoteupdate;
      
      public var isshiftkeypressed;
      
      public var versionno;
      
      public var playerShipSpeedRatio;
      
      public var playersSessionScoreStart;
      
      public var datatosend;
      
      public var refreshchatdisplay;
      
      public var keywaspressed;
      
      public var timebannedfor;
      
      public var LeftKey;
      
      public var xwidthofasector;
      
      public var maxdockingvelocity;
      
      public var currentStarFrame;
      
      public var updateStarInterval;
      
      public var squadbasedockedat;
      
      public var currenthelpframedisplayed;
      
      public var starbaselocation;
      
      public var lastplayerssavedinfo;
      
      public var isdkeypressed;
      
      public var playerrankings;
      
      public var energydrainedbyshieldgenatfull;
      
      public var maxenergy;
      
      public var publicteams;
      
      public var lastshipcoordinatex;
      
      public var currentplayershotsfired;
      
      public var starbasepricechanges;
      
      public var lastshipcoordinatey;
      
      public var lastpingcheck;
      
      public var GamePingTimer:TextField;
      
      public var pulsarseldammodier;
      
      public var shipTargetDisplay;
      
      public var gamedisplayarea:MovieClip;
      
      public var framestobuffer;
      
      public var missile;
      
      public var timePlayerCanExit;
      
      public var playerempend;
      
      public var gameError:MovieClip;
      
      public var shipHealthBar;
      
      public var maxshieldstrength;
      
      public var sectormapitems;
      
      public var shipYmovement;
      
      public var NavigationImageToAttach:Class;
      
      public var gameerror;
      
      public var privatechattextcolor;
      
      public var extraplayerships;
      
      public var loginmovie:MovieClip;
      
      public var playersquadbases;
      
      public var NavigationTimer:Timer;
      
      public var playershotsfired;
      
      public var stafchattextcolor;
      
      public var ForwardKey;
      
      public var staffhelpchattextcolor;
      
      public var currentport;
      
      public var doublegameareawidth;
      
      public var curentGametime;
      
      public var playershipmaxvelocity;
      
      public var justenteredgame;
      
      public var currentmissile;
      
      public var lastdamagetobase;
      
      public var isrightkeypressed;
      
      public var currentOnlineListdisplay;
      
      public var playershipacceleration;
      
      public var gameplaystatus;
      
      public var turretCrosshairs:MovieClip;
      
      public var arrayZoomFactors;
      
      public var timetillusercanjump;
      
      public var afterburnerinuse;
      
      public var timeintervalcheck;
      
      public var ClockCheckstodo;
      
      public var timetillnexClocktcheck;
      
      public var badwords;
      
      public var AnglePlayerShipFacing;
      
      public var shipositiondelay;
      
      public var targetinfo;
      
      public var shipRelationIDDisplay;
      
      public var TargetDisplay:MovieClip;
      
      public var OnlinePLayerList:MovieClip;
      
      public var deathmenu:MovieClip;
      
      public var playersworthtobtymodifire;
      
      public var shiplag;
      
      public var MissileTimer:Timer;
      
      public var schange;
      
      public var inGameSoundButton:MovieClip;
      
      public var teamdeathmatch;
      
      public var ShipHardwareScreen:MovieClip;
      
      public var specialsingame:Specialsingame;
      
      public var CurenntClockCheckNumber;
      
      public var hangarWindow:MovieClip;
      
      public var shieldrechargerate;
      
      public var gamechatinfo;
      
      public var secondlastturning;
      
      public var shiptimeoutime;
      
      public var BackgroundMaxHeight;
      
      public var myLoadVars;
      
      public var sectorinformation;
      
      public var counterForBaseLives;
      
      public var DeathMatchTimer:Timer;
      
      public var EnterChatKey;
      
      public var basesstartat;
      
      public var playerdiruptend;
      
      public var iscontrolkeypressed;
      
      public var playerSpecialsSettings;
      
      public var bountychanging;
      
      public var squadwarinfo;
      
      public var shipDockingImage;
      
      public var playersFakeVelocity;
      
      public var IsSocketConnected;
      
      public var BackgroundMaxWidth;
      
      public var isplayerdisrupt;
      
      public var gameRadar:MovieClip;
      
      public var specialItemno;
      
      public var gamesetting;
      
      public var shipcoordinatex;
      
      public var shipcoordinatey;
      
      public var chatDisplay:chatdialogue;
      
      public var shipXmovement;
      
      public var arenachattextcolor;
      
      public var creditBar:MovieClip;
      
      public var clocktimediff;
      
      public var ClockIntervalCheckTime;
      
      public var playershipafterburnerspeed;
      
      public var currentotherplayshot;
      
      public var pulsarsinzone;
      
      public var specialshipitems;
      
      public var turretcontrol:MovieClip;
      
      public var isplayeraguest;
      
      public var missileShotBuuferData;
      
      public var tb;
      
      public var othergunfire;
      
      public var teambasetypes;
      
      public var energyrechargerate;
      
      public var scoreratiomodifier;
      
      public var inGameHelp:MovieClip;
      
      public var gamebackground;
      
      public var isdownkeypressed;
      
      public var inGameMapButton:SimpleButton;
      
      public var NextShipTimeResend;
      
      public var keypressdelay;
      
      public var teamchattextcolor;
      
      public var RightKey;
      
      public var playershipfacing;
      
      public var HalfBackgroundMaxHeight;
      
      public var playershipstatus;
      
      public var isgamerunningfromremote;
      
      public var currenttimechangeratio;
      
      public var currZoomFactor;
      
      public var playersexitdocktimewait;
      
      public var lastturning;
      
      public var shipJumpImage;
      
      public var playershiprotating;
      
      public var ywidthofasector;
      
      public var PlayersShipImage:MovieClip;
      
      public var missileBankWindow:MovieClip;
      
      public var playershiprotation;
      
      public var playersdestination;
      
      public var currentonlineplayers;
      
      public var playersshiptype;
      
      public var playerscurrentextrashipno;
      
      public var pingintervalcheck;
      
      public var main_docked_screen:MovieClip;
      
      public var scoreDISP:MovieClip;
      
      public var gunfirekey;
      
      public var shipNameTag;
      
      public var playersmaxstructure;
      
      public var playerBeingSeekedByMissile;
      
      public var currentgamestatustext;
      
      public var dockkey;
      
      public var loc;
      
      public var othermissilefire;
      
      public var totalsmallships;
      
      public var MapTimer:Timer;
      
      public var BackwardsKey;
      
      public var shiptype;
      
      public var doublegameareaheight;
      
      public var energycapacitors;
      
      public var MapKey;
      
      public var NavigationImage:MovieClip;
      
      public var missilekey;
      
      public var currentothermissileshot;
      
      public var baseidnumberstart;
      
      public var TargetShipKey;
      
      public var teamdeathmatchinfo;
      
      public var teambases;
      
      public var MissileSeekDelay;
      
      public var PlayersShipShieldImage:MovieClip;
      
      public var ClockCheckTimesReceived;
      
      public var afterburnerspeed;
      
      public var otherplayership;
      
      public var RadarBlot;
      
      public var isaracealteredZone;
      
      public var savestatus;
      
      public var totalstars;
      
      public var TurretMouseDown;
      
      public var LastFrameTime;
      
      public var afterburnerkey;
      
      public var replacewithchar;
      
      public var AvailableListDisplay;
      
      public var isupkeypressed;
      
      public var inGameHelpButton:SimpleButton;
      
      public var isleftkeypressed;
      
      public var lastplayerssavedinfosent;
      
      public var regularchattextcolor;
      
      public var isspacekeypressed;
      
      public function MainTimeline()
      {
         super();
         addFrameScript(0,this.frame1,3,this.frame4,10,this.frame11,13,this.frame14,15,this.frame16,16,this.frame17,17,this.frame18,25,this.frame26,38,this.frame39,59,this.frame60,72,this.frame73,87,this.frame88,146,this.frame147);
      }
      
      public function firingbulletstartlocation(param1:*, param2:*, param3:*) : *
      {
         var _loc4_:* = Math.sqrt(param1 * param1 + param2 * param2);
         var _loc5_:* = Math.asin(param1 / _loc4_) / (Math.PI / 180);
         var _loc6_:* = 0;
         if(param1 >= 0)
         {
            if(param2 >= 0)
            {
               _loc6_ = param3 + (90 - _loc5_);
               param1 = _loc4_ * Math.cos(Math.PI / 180 * _loc6_);
               param2 = _loc4_ * Math.sin(Math.PI / 180 * _loc6_);
            }
            if(param2 < 0)
            {
               _loc6_ = param3 + (270 + _loc5_);
               param1 = _loc4_ * Math.cos(Math.PI / 180 * _loc6_);
               param2 = _loc4_ * Math.sin(Math.PI / 180 * _loc6_);
            }
         }
         if(param1 < 0)
         {
            if(param2 >= 0)
            {
               _loc6_ = param3 + (90 - _loc5_);
               param1 = _loc4_ * Math.cos(Math.PI / 180 * _loc6_);
               param2 = _loc4_ * Math.sin(Math.PI / 180 * _loc6_);
            }
            if(param2 < 0)
            {
               _loc6_ = param3 + (270 + _loc5_);
               param1 = _loc4_ * Math.cos(Math.PI / 180 * _loc6_);
               param2 = _loc4_ * Math.sin(Math.PI / 180 * _loc6_);
            }
         }
         var _loc7_:*;
         (_loc7_ = new Array())[0] = param1;
         _loc7_[1] = param2;
         return _loc7_;
      }
      
      public function func_MoveGunShots(param1:*, param2:*) : *
      {
         var currenttimechangeratio:* = param1;
         var curentGametime:* = param2;
         var currentnumber:* = 0;
         while(currentnumber < this.playershotsfired.length)
         {
            if(this.playershotsfired[currentnumber][10])
            {
               this.playershotsfired[currentnumber][10] = false;
            }
            else if(this.playershotsfired[currentnumber][5] < curentGametime)
            {
               this.func_removePlayersShot(currentnumber);
               currentnumber--;
            }
            else
            {
               this.playershotsfired[currentnumber][1] += Number(this.playershotsfired[currentnumber][3]) * currenttimechangeratio;
               this.playershotsfired[currentnumber][2] += Number(this.playershotsfired[currentnumber][4]) * currenttimechangeratio;
               try
               {
                  this.playershotsfired[currentnumber][9].x = Number(this.playershotsfired[currentnumber][1]) - this.shipcoordinatex;
                  this.playershotsfired[currentnumber][9].y = Number(this.playershotsfired[currentnumber][2]) - this.shipcoordinatey;
               }
               catch(error:Error)
               {
                  trace("Error moving player gunfire: " + error);
               }
            }
            currentnumber++;
         }
         this.othergunfiremovement(currenttimechangeratio);
      }
      
      public function func_RemoveRadarDot(param1:*) : *
      {
         var ChildName:* = param1;
         try
         {
            this.gameRadar.radarScreen.removeChild(ChildName);
         }
         catch(error:Error)
         {
            trace("Raddar Remove Error: " + error);
         }
      }
      
      public function func_SubmitChat() : *
      {
         var _loc1_:* = undefined;
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         if(this.chatDisplay.visible)
         {
            _loc1_ = this.chatDisplay.chatInput.text;
            _loc2_ = "";
            if(_loc1_ != "")
            {
               _loc3_ = _loc1_.toUpperCase();
               _loc4_ = "";
               _loc5_ = "";
               if(_loc3_ != "?SOUND=ON")
               {
                  if(_loc3_ != "?SOUND=OFF")
                  {
                     if(_loc3_ == "?KFLAG")
                     {
                        this.func_enterintochat("Not Enabled Yet",this.systemchattextcolor);
                     }
                     else if(_loc3_.substr(0,5) == "?HELP")
                     {
                        if((_loc4_ = _loc1_.substr(6)) != "")
                        {
                           _loc5_ = "CHT~CH`" + this.playershipstatus[3][0] + "`STAFFH`" + _loc4_ + "~";
                           this.mysocket.send(_loc5_);
                           this.func_enterintochat("Sent Message To Staff : " + _loc4_,this.systemchattextcolor);
                        }
                     }
                     else if(_loc3_.substr(0,8) != "?GETRANK")
                     {
                        if(_loc3_.substr(0,6) == "?NEWS=")
                        {
                           if(this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "ADMIN")
                           {
                              _loc5_ = "ADMIN`NEWS`MES`" + _loc1_.substr(6) + "~";
                              this.mysocket.send(_loc5_);
                           }
                        }
                        else if(_loc3_.substr(0,3) == "/W ")
                        {
                           _loc6_ = 3;
                           while(_loc6_ < _loc1_.length)
                           {
                              if(_loc1_.charAt(_loc6_) != " ")
                              {
                                 _loc9_ = _loc6_;
                                 break;
                              }
                              _loc6_++;
                           }
                           _loc6_ = _loc9_ + 1;
                           while(_loc6_ < _loc1_.length)
                           {
                              if(_loc1_.charAt(_loc6_) == " ")
                              {
                                 _loc10_ = _loc6_;
                                 break;
                              }
                              _loc6_++;
                           }
                           _loc7_ = null;
                           _loc8_ = _loc1_.substr(_loc9_,_loc10_ - _loc9_);
                           if((_loc4_ = _loc1_.substr(_loc10_ + 1)).length > 0)
                           {
                              _loc3_ = "CHT~CH`" + Number(this.playershipstatus[3][0]) + "`PM`" + _loc4_ + "`" + _loc8_.toUpperCase() + "~";
                              if(this.gamechatinfo[2][2] == false)
                              {
                                 this.mysocket.send(_loc3_);
                                 if(_loc7_ != this.playershipstatus[3][0])
                                 {
                                 }
                              }
                              else
                              {
                                 this.func_enterintochat(" You Are Muted ",this.systemchattextcolor);
                              }
                           }
                        }
                        else if(_loc1_.substr(0,7).toUpperCase() == "?IGNORE")
                        {
                           _loc2_ = _loc1_.substr(8).toUpperCase();
                           this.func_ignorelist(_loc2_);
                        }
                        else if(_loc1_.substr(0,8).toUpperCase() != "?CHEATER")
                        {
                           if(_loc3_.substr(0,2) == "//" && this.playershipstatus[5][2] != "N/A")
                           {
                              _loc3_ = "CHT~CH`" + this.playershipstatus[3][0] + "`TM`" + _loc1_.substr(2) + "~";
                              if(this.gamechatinfo[2][2] == false)
                              {
                                 this.mysocket.send(_loc3_);
                              }
                              else
                              {
                                 this.func_enterintochat(" You Are Muted ",this.systemchattextcolor);
                              }
                           }
                           else if(_loc1_.substr(0,1) == " ".substr(0,1) && (this.playershipstatus[5][12] == "MOD" || this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "ADMIN"))
                           {
                              _loc3_ = "CHT~CH`" + this.playershipstatus[3][0] + "`STF`" + _loc1_.substr(1) + "~";
                              this.mysocket.send(_loc3_);
                           }
                           else if(_loc1_.charAt(0) == ";" && this.playershipstatus[5][10] != "NONE")
                           {
                              _loc3_ = "CHT~CH`" + this.playershipstatus[3][0] + "`SM`" + _loc1_.substr(1) + "~";
                              this.mysocket.send(_loc3_);
                           }
                           else if(_loc1_.substr(0,2) == "/*" && (this.playershipstatus[5][12] == "MOD" || this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "ADMIN"))
                           {
                              this.func_admincommands(_loc1_);
                           }
                           else
                           {
                              _loc3_ = "CHT~CH`" + this.playershipstatus[3][0] + "`M`" + this.badwordfiltering(_loc1_) + "~";
                              if(this.gamechatinfo[2][2] == false)
                              {
                                 this.mysocket.send(_loc3_);
                              }
                              else
                              {
                                 this.func_enterintochat(" You Are Muted ",this.systemchattextcolor);
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      
      public function func_otherPLayerLogin(param1:*) : *
      {
         var _loc2_:* = param1[1];
         var _loc3_:* = String(param1[2]);
         var _loc4_:* = false;
         var _loc5_:* = 0;
         while(_loc5_ < this.currentonlineplayers.length)
         {
            if(String(this.currentonlineplayers[_loc5_][0]) == String(_loc2_) || String(this.currentonlineplayers[_loc5_][1]) == String(_loc3_))
            {
               this.currentonlineplayers.splice(_loc5_,1);
               _loc5_--;
            }
            _loc5_++;
         }
         var _loc6_:* = this.currentonlineplayers.length;
         this.currentonlineplayers[_loc6_] = new Array();
         this.currentonlineplayers[_loc6_][0] = param1[1];
         this.currentonlineplayers[_loc6_][1] = param1[2];
         this.currentonlineplayers[_loc6_][2] = param1[3];
         this.currentonlineplayers[_loc6_][3] = param1[4];
         this.currentonlineplayers[_loc6_][4] = param1[5];
         if(isNaN(this.currentonlineplayers[_loc6_][4]))
         {
            this.currentonlineplayers[_loc6_][4] = -1;
         }
         this.currentonlineplayers[_loc6_][5] = int(param1[6]);
         this.currentonlineplayers[_loc6_][6] = int(param1[7]);
         this.currentonlineplayers[_loc6_][7] = int(param1[8]);
         this.currentonlineplayers[_loc6_][8] = int(param1[9]);
         if(param1[2] == this.playershipstatus[3][2] && this.playershipstatus[3][2] == this.playershipstatus[3][0])
         {
            this.playershipstatus[3][0] = param1[1];
         }
         this.func_refreshCurrentOnlineList();
         this.func_AddChatter(param1[2] + " has entered");
      }
      
      public function func_DisplayError(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         if(param1 == "failedtologin")
         {
            this.gameError.errormessage.text = " Failed to Login \r" + "Probable Reasons For This - \r" + "1. Server is down, or could not connect to server \r" + "2. You are not connected to the internet or information cannot  \r" + "Pass through a firewall\r" + "3. Failed Login  \r\r" + "Post on the Forums if this continues to happen  \r";
         }
         else if(param1 == "hostclosedconnection")
         {
            this.gameError.errormessage.text = " Connection Has Closed \r" + "You Have lost the connection to the server \r" + "Probable Reasons For This - \r" + "1. Server shut down \r" + "2. Your connection to the internet was broken \r" + "3. Server Full, come back later";
         }
         else if(param1 == "toomanyplayers")
         {
            this.gameError.errormessage.text = " Too Many Players \r" + "Probable Reasons For This - \r" + "1. There are too many players, Try again later \r";
         }
         else if(param1 == "servershutdown")
         {
            this.gameError.errormessage.text = " The server was shut down \r" + "Probable Reasons For This - \r" + "1. An Admin needed to restart the server. \r" + "It will take a few minuites for the server to reload \r" + "You need to close this window and try reloading form the Homepage.";
         }
         else if(param1 == "nameinuse")
         {
            this.gameError.errormessage.text = " Your Name is Already in Use \r" + "Probable Reasons For This - \r" + "1. Your name is already in use in this zone. \r" + "It will take a few minuites for you to time out if disconnected  \r";
         }
         else if(param1 == "kicked")
         {
            this.gameError.errormessage.text = " You were booted by " + this.gameerrordoneby + "\r" + "Probable Reasons For This - \r" + "1. You Have been a very naughty person \r" + "2. Game Error \r";
         }
         else if(param1 == "toomanyipplayers")
         {
            this.gameError.errormessage.text = " Too Many Players using this Internet Connection  \r" + "Probable Reasons For This - \r" + "1. Too many players on your network \r" + "2. Too many names logged in \r";
         }
         else if(param1 == "banned")
         {
            _loc2_ = Number(this.timebannedfor);
            _loc3_ = _loc2_ * 60;
            _loc4_ = Math.floor(_loc3_ / 60 / 60 / 24);
            _loc3_ -= _loc4_ * 60 * 60 * 24;
            _loc5_ = Math.floor(_loc3_ / 60 / 60);
            _loc3_ -= _loc5_ * 60 * 60;
            _loc6_ = Math.floor(_loc3_ / 60);
            _loc3_ -= _loc6_ * 60;
            _loc7_ = _loc3_;
            _loc2_ = _loc4_ + " Days, " + _loc5_ + " Hours, " + _loc6_ + " Minutes";
            this.gameError.errormessage.text = " Your Have Been Banned for " + _loc2_ + " by " + this.gameerrordoneby + "\r" + "Probable Reasons For This - \r" + "1. You are probably not welcomed here. \r" + "2. This Point of Access is banned. \r" + "3. In case of an error you can post in the forum. \r";
         }
         else if(param1 == "fpsstopped")
         {
            this.gameError.errormessage.text = " Your Frame Rate is Too Slow! \r" + "Probable Reasons For This - \r" + "1. You computer was running to slow \r" + "2. You tried to cheat \r";
         }
         else if(param1 == "savefailure")
         {
            this.gameError.errormessage.text = " Your Game Failed to Save! \r" + "Probable Reasons For This - \r" + "1. Your Save Transmission Failed \r" + "2. You are Playing With This Account in Another Zone at the Same Time \r" + "\r" + "If this continues you may end up being banned, /r and you will have to contact the webmaster to become unbanned.";
         }
         else if(param1 == "FLOODING")
         {
            this.gameError.errormessage.text = " Your Must Stop Flooding \r" + "Probable Reasons For This - \r" + "1. Your Are Flooding the Chat \r" + "\r" + "If this continues you may end up being reported and being banned.";
         }
         else if(param1 == "endplayersgame")
         {
            this.gameError.errormessage.text = " You have ended your game \r" + "Thanks for playing!.";
         }
         else
         {
            this.gameError.errormessage.text = " Unknown Error Occured";
         }
      }
      
      public function func_specialsinfo(param1:*, param2:*, param3:*, param4:*) : *
      {
         if(param2 == "RELOAD")
         {
            this.specialsingame["sp" + param4].specialbutton.reloadtime = param3;
            this.func_displayspecials(param4);
            this.specialsingame["sp" + param4].specialbutton.gotoAndStop("RELOAD");
            this.specialsingame["sp" + param4].specialinfodata.text = "RELOAD";
         }
         if(param2 == "FAILED")
         {
            this.specialsingame["sp" + param4].specialbutton.reloadtime = param3;
            this.specialsingame["sp" + param4].specialinfodata.text = "FAILED";
            this.func_displayspecials(param4);
            this.specialsingame["sp" + param4].specialbutton.gotoAndStop("RELOAD");
         }
         if(param2 == "ON")
         {
            this.specialsingame["sp" + param4].specialbutton.gotoAndStop("ON");
            this.specialsingame["sp" + param4].specialinfodata.text = "ON";
            this.func_displayspecials(param4);
         }
         if(param2 == "OFF")
         {
            this.func_displayspecials(param4);
            this.specialsingame["sp" + param4].specialbutton.gotoAndStop("OFF");
            this.specialsingame["sp" + param4].specialinfodata.text = "";
         }
      }
      
      public function func_stealthplayership(param1:*, param2:*, param3:*) : *
      {
         this.playershipstatus[5][15] = "S" + param2;
         this.playerSpecialsSettings.isStealthed = true;
         this.playerSpecialsSettings.StealthEnergy = param1;
         this.playerSpecialsSettings.StealthLocation = param3;
         this.func_StealthTheRadar();
      }
      
      public function func_InGameMap_Click(param1:MouseEvent) : void
      {
         this.func_TriggerGameMap();
      }
      
      public function func_KeyReleaseWhileChatting() : *
      {
         if(this.chatDisplay.chatInput.text == "::")
         {
            if(this.gamechatinfo[5] != "")
            {
               this.chatDisplay.chatInput.text = "/w " + this.gamechatinfo[5] + " ";
            }
         }
         else if(this.chatDisplay.chatInput.text.length > 120)
         {
            this.chatDisplay.chatInput.text = this.chatDisplay.chatInput.text.substr(0,120);
         }
      }
      
      public function func_loadSectorItemsIntoBackgroup() : *
      {
         var teambasename:* = undefined;
         var basename:* = undefined;
         var extracount:* = 0;
         var ii:* = 0;
         while(ii < this.sectormapitems.length)
         {
            if(this.sectormapitems[ii] != null)
            {
               if(this.sectormapitems[ii][0].substr(0,2) == "TB")
               {
                  teambasename = this.sectormapitems[ii][0];
                  extracount = 0;
                  while(extracount < this.teambases.length)
                  {
                     if(this.teambases[extracount][0] == teambasename)
                     {
                        this.gamebackground.addChild(this.teambases[extracount][21]);
                        this.teambases[extracount][21].x = this.sectormapitems[ii][1];
                        this.teambases[extracount][21].y = this.sectormapitems[ii][2];
                        try
                        {
                           this.teambases[extracount][21].gotoAndStop(extracount + 1);
                        }
                        catch(error:Error)
                        {
                           trace("TeamBaseFrameNotThere");
                        }
                        this.teambases[extracount][21].baseiddisp.text = teambasename.substr(2);
                        this.func_AddRadarDot(this.teambases[extracount][22],"BASE");
                        this.teambases[extracount][22].x = this.sectormapitems[ii][1];
                        this.teambases[extracount][22].y = this.sectormapitems[ii][2];
                        this.func_updateTameBaseHealthBars(extracount);
                     }
                     extracount++;
                  }
               }
               if(this.sectormapitems[ii][0].substr(0,2) == "SB" || this.sectormapitems[ii][0].substr(0,2) == "PL")
               {
                  basename = this.sectormapitems[ii][0];
                  extracount = 0;
                  while(extracount < this.starbaselocation.length)
                  {
                     if(this.starbaselocation[extracount][0] == basename)
                     {
                        this.gamebackground.addChild(this.starbaselocation[extracount][21]);
                        this.starbaselocation[extracount][21].x = this.sectormapitems[ii][1];
                        this.starbaselocation[extracount][21].y = this.sectormapitems[ii][2];
                        this.starbaselocation[extracount][22].x = this.sectormapitems[ii][1];
                        this.starbaselocation[extracount][22].y = this.sectormapitems[ii][2];
                        this.func_AddRadarDot(this.starbaselocation[extracount][22],"BASE");
                     }
                     extracount++;
                  }
               }
               if(this.sectormapitems[ii][0].substr(0,2) == "NP")
               {
                  this.gamebackground.addChild(this.sectormapitems[ii][21]);
                  this.sectormapitems[ii][21].x = this.sectormapitems[ii][1];
                  this.sectormapitems[ii][21].y = this.sectormapitems[ii][2];
                  this.func_AddRadarDot(this.sectormapitems[ii][22],"NP");
                  this.sectormapitems[ii][22].x = this.sectormapitems[ii][1];
                  this.sectormapitems[ii][22].y = this.sectormapitems[ii][2];
               }
            }
            ii++;
         }
      }
      
      public function makebackgroundstars() : *
      {
         var _loc1_:* = undefined;
         var _loc2_:Class = null;
         var _loc3_:* = undefined;
         this.backgroundstar = new Array();
         _loc1_ = 0;
         while(_loc1_ < this.gamesetting.totalstars)
         {
            this.backgroundstar[_loc1_] = new Array(4);
            this.backgroundstar[_loc1_][0] = Math.random() * this.doublegameareawidth - Math.round(this.doublegameareawidth / 2);
            this.backgroundstar[_loc1_][1] = Math.random() * this.doublegameareaheight - Math.round(this.doublegameareaheight / 2);
            _loc2_ = getDefinitionByName("backgroundstar") as Class;
            this.backgroundstar[_loc1_][2] = new _loc2_() as MovieClip;
            this.gamedisplayarea.addChild(this.backgroundstar[_loc1_][2]);
            this.backgroundstar[_loc1_][2].x = this.backgroundstar[_loc1_][0];
            this.backgroundstar[_loc1_][2].y = this.backgroundstar[_loc1_][1];
            _loc3_ = Math.round(Math.random() * 3) + 1;
            if(_loc3_ != 1)
            {
               this.backgroundstar[_loc1_][2].gotoAndStop(1);
            }
            else
            {
               this.backgroundstar[_loc1_][2].gotoAndStop(1);
            }
            this.backgroundstar[_loc1_][3] = _loc3_;
            _loc1_++;
         }
      }
      
      public function GameKeyListenerKeyRelease(param1:KeyboardEvent) : void
      {
         var _loc2_:* = false;
         var _loc3_:* = param1.keyCode;
         if(_loc3_ == this.EnterChatKey)
         {
            this.func_EnableChatPress();
         }
         else if(this.isPlayerChatting)
         {
            this.func_KeyReleaseWhileChatting();
         }
         else if(_loc3_ == this.gamesetting.accelkey)
         {
            this.isupkeypressed = _loc2_;
            this.keywaspressed = true;
         }
         else if(_loc3_ == this.gamesetting.turnleftkey)
         {
            this.isleftkeypressed = _loc2_;
            this.keywaspressed = true;
         }
         else if(_loc3_ == this.gamesetting.turnrightkey)
         {
            this.isrightkeypressed = _loc2_;
            this.keywaspressed = true;
         }
         else if(_loc3_ == this.gamesetting.deaccelkey)
         {
            this.isdownkeypressed = _loc2_;
            this.keywaspressed = true;
         }
         else if(_loc3_ == this.gamesetting.afterburnerskey)
         {
            this.isshiftkeypressed = _loc2_;
            this.keywaspressed = true;
         }
         else if(_loc3_ == this.gamesetting.gunskey)
         {
            this.iscontrolkeypressed = _loc2_;
         }
         else if(_loc3_ == this.gamesetting.missilekey)
         {
            this.isspacekeypressed = _loc2_;
         }
         else if(_loc3_ == this.gamesetting.dockkey)
         {
            this.isdkeypressed = _loc2_;
         }
         else if(_loc3_ == this.gamesetting.MapKey)
         {
            this.func_TriggerGameMap();
         }
         else if(_loc3_ == this.gamesetting.targeterkey)
         {
            this.func_selectShipTarget();
         }
         else if(_loc3_ == this.gamesetting.OnlineListKey)
         {
            this.fund_OnlineListToggled();
         }
         else if(_loc3_ == this.gamesetting.HelpKey)
         {
            this.func_TriggerGameHelp();
         }
      }
      
      internal function frame11() : *
      {
         this.gameMap.visible = false;
         this.inGameMapButton.visible = false;
         this.MapTimer = new Timer(100);
         this.MapTimer.addEventListener(TimerEvent.TIMER,this.MapTimerHandler);
         this.playersdestination = new Array();
         this.playersdestination[0] = -1;
         this.inGameMapButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_InGameMap_Click);
         this.gameMap.minimizeButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_InGameMap_Click);
         this.inGameHelp.visible = false;
         this.inGameHelpButton.visible = false;
         this.inGameHelpButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_InGameHelp_Click);
         this.inGameHelp.minimizeButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_InGameHelp_Click);
         this.gameplaystatus = new Array();
         this.gameplaystatus[1] = new Array();
         this.pingintervalcheck = 20000;
         this.lastpingcheck = getTimer() + 10000;
         this.shiplag = 150;
         this.remoteupdate = false;
         this.GamePingTimer.text = "";
         addEventListener(Event.ENTER_FRAME,this.pingTimerScript);
         this.hasClockBeenSet = false;
         this.ClockIntervalCheckTime = 2000;
         this.timetillnexClocktcheck = getTimer() + 400;
         this.CurenntClockCheckNumber = 0;
         this.ClockCheckstodo = 2;
         this.ClockCheckTimesReceived = new Array();
         this.clocktimediff = 0;
         this.OnlinePLayerList.onlinePlayersListing.text = "";
         this.OnlinePLayerList.visible = true;
         this.currentonlineplayers = new Array();
         this.currentOnlineListdisplay = 0;
         this.AvailableListDisplay = new Array();
         this.AvailableListDisplay[0] = "s/b";
         this.AvailableListDisplay[1] = "k/d";
         this.AvailableListDisplay[2] = "both";
         this.specialshipitems = new Array();
         this.pulsarseldammodier = 1;
         this.specialItemno = 0;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Flare 1";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 200;
         this.specialshipitems[this.specialItemno][3] = 10000;
         this.specialshipitems[this.specialItemno][4] = 3000;
         this.specialshipitems[this.specialItemno][5] = "FLARE";
         this.specialshipitems[this.specialItemno][6] = 100000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "specialflare1";
         this.specialshipitems[this.specialItemno][10] = 30 / 100;
         this.specialshipitems[this.specialItemno][11] = 1000;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Flare 2";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 400;
         this.specialshipitems[this.specialItemno][3] = 10000;
         this.specialshipitems[this.specialItemno][4] = 4000;
         this.specialshipitems[this.specialItemno][5] = "FLARE";
         this.specialshipitems[this.specialItemno][6] = 280000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "specialflare2";
         this.specialshipitems[this.specialItemno][10] = 45 / 100;
         this.specialshipitems[this.specialItemno][11] = 1500;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Flare 3";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 600;
         this.specialshipitems[this.specialItemno][3] = 10000;
         this.specialshipitems[this.specialItemno][4] = 5000;
         this.specialshipitems[this.specialItemno][5] = "FLARE";
         this.specialshipitems[this.specialItemno][6] = 750000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "specialflare3";
         this.specialshipitems[this.specialItemno][10] = 60 / 100;
         this.specialshipitems[this.specialItemno][11] = 2000;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Detector 1";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 100;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "DETECTOR";
         this.specialshipitems[this.specialItemno][6] = 350000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "";
         this.specialshipitems[this.specialItemno][10] = 1;
         this.specialshipitems[this.specialItemno][11] = 6000;
         this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
         this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Detector 2";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 220;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "DETECTOR";
         this.specialshipitems[this.specialItemno][6] = 750000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "";
         this.specialshipitems[this.specialItemno][10] = 2;
         this.specialshipitems[this.specialItemno][11] = 5500;
         this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
         this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Detector 3";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 325;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "DETECTOR";
         this.specialshipitems[this.specialItemno][6] = 2150000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "";
         this.specialshipitems[this.specialItemno][10] = 3;
         this.specialshipitems[this.specialItemno][11] = 5000;
         this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
         this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Stealth 1";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 75;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "STEALTH";
         this.specialshipitems[this.specialItemno][6] = 250000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "";
         this.specialshipitems[this.specialItemno][10] = 1;
         this.specialshipitems[this.specialItemno][11] = 5000;
         this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
         this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Stealth 2";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 175;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "STEALTH";
         this.specialshipitems[this.specialItemno][6] = 1250000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "";
         this.specialshipitems[this.specialItemno][10] = 2;
         this.specialshipitems[this.specialItemno][11] = 7500;
         this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
         this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Stealth 3";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 250;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "STEALTH";
         this.specialshipitems[this.specialItemno][6] = 2500000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "";
         this.specialshipitems[this.specialItemno][10] = 3;
         this.specialshipitems[this.specialItemno][11] = 10000;
         this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
         this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Cloak+Stealth 1";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 150;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "CLOAK";
         this.specialshipitems[this.specialItemno][6] = 500000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "";
         this.specialshipitems[this.specialItemno][10] = 1;
         this.specialshipitems[this.specialItemno][11] = 5000;
         this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
         this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Cloak+Stealth 2";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 350;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "CLOAK";
         this.specialshipitems[this.specialItemno][6] = 1750000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "";
         this.specialshipitems[this.specialItemno][10] = 2;
         this.specialshipitems[this.specialItemno][11] = 7500;
         this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
         this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Cloak+Stealth 3";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 650;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "CLOAK";
         this.specialshipitems[this.specialItemno][6] = 4200000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "";
         this.specialshipitems[this.specialItemno][10] = 3;
         this.specialshipitems[this.specialItemno][11] = 10000;
         this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
         this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Pulsar-1";
         this.specialshipitems[this.specialItemno][1] = 3;
         this.specialshipitems[this.specialItemno][2] = 400;
         this.specialshipitems[this.specialItemno][3] = 1000;
         this.specialshipitems[this.specialItemno][4] = 5000;
         this.specialshipitems[this.specialItemno][5] = "PULSAR";
         this.specialshipitems[this.specialItemno][6] = 50000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "pulsar1";
         this.specialshipitems[this.specialItemno][10] = 95 / 100;
         this.specialshipitems[this.specialItemno][11] = 4000;
         this.specialshipitems[this.specialItemno][12] = 150;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Pulsar-2";
         this.specialshipitems[this.specialItemno][1] = 2;
         this.specialshipitems[this.specialItemno][2] = 600;
         this.specialshipitems[this.specialItemno][3] = 1500;
         this.specialshipitems[this.specialItemno][4] = 7000;
         this.specialshipitems[this.specialItemno][5] = "PULSAR";
         this.specialshipitems[this.specialItemno][6] = 125000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "pulsar2";
         this.specialshipitems[this.specialItemno][10] = 95 / 100;
         this.specialshipitems[this.specialItemno][11] = 7000;
         this.specialshipitems[this.specialItemno][12] = 225;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Pulsar-3";
         this.specialshipitems[this.specialItemno][1] = 1;
         this.specialshipitems[this.specialItemno][2] = 750;
         this.specialshipitems[this.specialItemno][3] = 2000;
         this.specialshipitems[this.specialItemno][4] = 15000;
         this.specialshipitems[this.specialItemno][5] = "PULSAR";
         this.specialshipitems[this.specialItemno][6] = 250000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "pulsar3";
         this.specialshipitems[this.specialItemno][10] = 95 / 100;
         this.specialshipitems[this.specialItemno][11] = 9500;
         this.specialshipitems[this.specialItemno][12] = 300;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Re-Shield-1";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 5;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = 15000;
         this.specialshipitems[this.specialItemno][5] = "RECHARGESHIELD";
         this.specialshipitems[this.specialItemno][6] = 650000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "reshield1";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0;
         this.specialshipitems[this.specialItemno][12] = 0;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Re-Shield-2";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 12;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = 25000;
         this.specialshipitems[this.specialItemno][5] = "RECHARGESHIELD";
         this.specialshipitems[this.specialItemno][6] = 1250000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "reshield2";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0;
         this.specialshipitems[this.specialItemno][12] = 0;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Re-Shield-3";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 20;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = 50000;
         this.specialshipitems[this.specialItemno][5] = "RECHARGESHIELD";
         this.specialshipitems[this.specialItemno][6] = 2500000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "reshield3";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0;
         this.specialshipitems[this.specialItemno][12] = 0;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Re-Struct-1";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 5;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = 25000;
         this.specialshipitems[this.specialItemno][5] = "RECHARGESTRUCT";
         this.specialshipitems[this.specialItemno][6] = 1250000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "restruct1";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0;
         this.specialshipitems[this.specialItemno][12] = 0;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Re-Struct-2";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 12;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = 35000;
         this.specialshipitems[this.specialItemno][5] = "RECHARGESTRUCT";
         this.specialshipitems[this.specialItemno][6] = 2750000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "restruct2";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0;
         this.specialshipitems[this.specialItemno][12] = 0;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Re-Struct-3";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 20;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = 50000;
         this.specialshipitems[this.specialItemno][5] = "RECHARGESTRUCT";
         this.specialshipitems[this.specialItemno][6] = 5890000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "restruct3";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0;
         this.specialshipitems[this.specialItemno][12] = 0;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Mine-1";
         this.specialshipitems[this.specialItemno][1] = 15;
         this.specialshipitems[this.specialItemno][2] = 0;
         this.specialshipitems[this.specialItemno][3] = 5000;
         this.specialshipitems[this.specialItemno][4] = 3000;
         this.specialshipitems[this.specialItemno][5] = "MINES";
         this.specialshipitems[this.specialItemno][6] = 50000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "mine1";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 2000;
         this.specialshipitems[this.specialItemno][12] = 5000;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Mine-2";
         this.specialshipitems[this.specialItemno][1] = 10;
         this.specialshipitems[this.specialItemno][2] = 0;
         this.specialshipitems[this.specialItemno][3] = 5000;
         this.specialshipitems[this.specialItemno][4] = 5500;
         this.specialshipitems[this.specialItemno][5] = "MINES";
         this.specialshipitems[this.specialItemno][6] = 95000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "mine2";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 2000;
         this.specialshipitems[this.specialItemno][12] = 10000;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Mine-3";
         this.specialshipitems[this.specialItemno][1] = 5;
         this.specialshipitems[this.specialItemno][2] = 0;
         this.specialshipitems[this.specialItemno][3] = 5000;
         this.specialshipitems[this.specialItemno][4] = 8000;
         this.specialshipitems[this.specialItemno][5] = "MINES";
         this.specialshipitems[this.specialItemno][6] = 135000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "mine3";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 2000;
         this.specialshipitems[this.specialItemno][12] = 25000;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "HardShield";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 1000;
         this.specialshipitems[this.specialItemno][3] = 7000;
         this.specialshipitems[this.specialItemno][4] = 14000;
         this.specialshipitems[this.specialItemno][5] = "HSHIELD";
         this.specialshipitems[this.specialItemno][6] = 535000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "hardshield1";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0;
         this.specialshipitems[this.specialItemno][12] = 25000;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "InvulnShield";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 1000;
         this.specialshipitems[this.specialItemno][3] = 7000;
         this.specialshipitems[this.specialItemno][4] = 30000;
         this.specialshipitems[this.specialItemno][5] = "INVULNSHIELD";
         this.specialshipitems[this.specialItemno][6] = 535000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "invulnshield1";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0;
         this.specialshipitems[this.specialItemno][12] = 0;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Rap. Missile 1";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 300;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "RAPIDMISSILE";
         this.specialshipitems[this.specialItemno][6] = 750000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "rapmis1";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0.7;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Rap. Missile 2";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 550;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "RAPIDMISSILE";
         this.specialshipitems[this.specialItemno][6] = 2750000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "rapmis2";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0.5;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Rap. Missile 3";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 850;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = -1;
         this.specialshipitems[this.specialItemno][5] = "RAPIDMISSILE";
         this.specialshipitems[this.specialItemno][6] = 4750000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "rapmis3";
         this.specialshipitems[this.specialItemno][10] = 100 / 100;
         this.specialshipitems[this.specialItemno][11] = 0.35;
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Jump Drive";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 1200;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = 70000;
         this.specialshipitems[this.specialItemno][5] = "JUMPDRIVE";
         this.specialshipitems[this.specialItemno][6] = 1500000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "Jump Drive";
         ++this.specialItemno;
         this.specialshipitems[this.specialItemno] = new Array();
         this.specialshipitems[this.specialItemno][0] = "Wing Man";
         this.specialshipitems[this.specialItemno][1] = -1;
         this.specialshipitems[this.specialItemno][2] = 1200;
         this.specialshipitems[this.specialItemno][3] = -1;
         this.specialshipitems[this.specialItemno][4] = 10000;
         this.specialshipitems[this.specialItemno][5] = "WINGMAN";
         this.specialshipitems[this.specialItemno][6] = 100000;
         this.specialshipitems[this.specialItemno][7] = this.specialItemno;
         this.specialshipitems[this.specialItemno][8] = "Wing Man";
         this.shipDockingImage = getDefinitionByName("shiptypedock") as Class;
         this.shipDeadImage = getDefinitionByName("shiptypedead") as Class;
         this.shipJumpImage = getDefinitionByName("shiptypejump") as Class;
         this.shipNameTag = getDefinitionByName("shipnametag") as Class;
         this.shipHealthBar = getDefinitionByName("shiphealthbar") as Class;
         this.shipRelationIDDisplay = getDefinitionByName("othershipiderDisplay") as Class;
         this.shipTargetDisplay = getDefinitionByName("shiptargetedDisplay") as Class;
         this.shiptype = new Array();
         this.totalsmallships = 16;
         this.guntype = new Array();
         this.shieldgenerators = new Array();
         this.missile = new Array();
         this.basesstartat = 4000;
         this.baseidnumberstart = 4000;
         this.isaracealteredZone = false;
         this.versionno = "2.00.205b";
         this.squadbasedockedat = null;
         this.otherplayerdockedon = null;
         this.lastdamagetobase = -55;
         this.playerrankings = new Array();
         this.playersSessionScoreStart = 0;
         this.starbasepricechanges = new Array();
         this.keypressdelay = 0;
         this.shipositiondelay = 220;
         this.scoreratiomodifier = 50;
         this.missiontoactualscoremodifier = 10;
         this.publicteams = 20;
         this.lastplayerssavedinfo = "";
         this.isplayeremp = false;
         this.playerempend = 0;
         this.playerdiruptend = 0;
         this.isplayerdisrupt = false;
         this.isplayeraguest = false;
         this.timetillusercanjump = 0;
         this.playersworthtobtymodifire = 1000;
         this.energygenerators = new Array();
         this.energygenerators[0] = new Array();
         this.energygenerators[0][0] = 150;
         this.energygenerators[0][1] = "Level 1";
         this.energygenerators[0][2] = 1000;
         this.energygenerators[1] = new Array();
         this.energygenerators[1][0] = 225;
         this.energygenerators[1][1] = "Level 2";
         this.energygenerators[1][2] = 1000;
         this.energygenerators[2] = new Array();
         this.energygenerators[2][0] = 300;
         this.energygenerators[2][1] = "Level 3";
         this.energygenerators[2][2] = 22000;
         this.energygenerators[3] = new Array();
         this.energygenerators[3][0] = 375;
         this.energygenerators[3][1] = "Level 4";
         this.energygenerators[3][2] = 35000;
         this.energygenerators[4] = new Array();
         this.energygenerators[4][0] = 450;
         this.energygenerators[4][1] = "Level 5";
         this.energygenerators[4][2] = 51000;
         this.energygenerators[5] = new Array();
         this.energygenerators[5][0] = 637.5;
         this.energygenerators[5][1] = "Level 6";
         this.energygenerators[5][2] = 159000;
         this.energygenerators[6] = new Array();
         this.energygenerators[6][0] = 810;
         this.energygenerators[6][1] = "Level 7";
         this.energygenerators[6][2] = 335500;
         this.energygenerators[7] = new Array();
         this.energygenerators[7][0] = 1035;
         this.energygenerators[7][1] = "Level 8";
         this.energygenerators[7][2] = 535500;
         this.energygenerators[8] = new Array();
         this.energygenerators[8][0] = 1215;
         this.energygenerators[8][1] = "Level 9";
         this.energygenerators[8][2] = 735500;
         this.energygenerators[9] = new Array();
         this.energygenerators[9][0] = 1455;
         this.energygenerators[9][1] = "Level 10";
         this.energygenerators[9][2] = 935500;
         this.energygenerators[10] = new Array();
         this.energygenerators[10][0] = 1732.5;
         this.energygenerators[10][1] = "Level 11";
         this.energygenerators[10][2] = 935500;
         this.energycapacitors = new Array();
         this.energycapacitors[0] = new Array();
         this.energycapacitors[0][0] = 600;
         this.energycapacitors[0][1] = "Level 1";
         this.energycapacitors[0][2] = 1500;
         this.energycapacitors[1] = new Array();
         this.energycapacitors[1][0] = 900;
         this.energycapacitors[1][1] = "Level 2";
         this.energycapacitors[1][2] = 6000;
         this.energycapacitors[2] = new Array();
         this.energycapacitors[2][0] = 1200;
         this.energycapacitors[2][1] = "Level 3";
         this.energycapacitors[2][2] = 13000;
         this.energycapacitors[3] = new Array();
         this.energycapacitors[3][0] = 1500;
         this.energycapacitors[3][1] = "Level 4";
         this.energycapacitors[3][2] = 21000;
         this.energycapacitors[4] = new Array();
         this.energycapacitors[4][0] = 2100;
         this.energycapacitors[4][1] = "Level 5";
         this.energycapacitors[4][2] = 83000;
         this.energycapacitors[5] = new Array();
         this.energycapacitors[5][0] = 2600;
         this.energycapacitors[5][1] = "Level 6";
         this.energycapacitors[5][2] = 235000;
         this.energycapacitors[6] = new Array();
         this.energycapacitors[6][0] = 3450;
         this.energycapacitors[6][1] = "Level 7";
         this.energycapacitors[6][2] = 385000;
         this.energycapacitors[7] = new Array();
         this.energycapacitors[7][0] = 4150;
         this.energycapacitors[7][1] = "Level 8";
         this.energycapacitors[7][2] = 535000;
         this.energycapacitors[8] = new Array();
         this.energycapacitors[8][0] = 5250;
         this.energycapacitors[8][1] = "Level 9";
         this.energycapacitors[8][2] = 755000;
         this.energycapacitors[9] = new Array();
         this.energycapacitors[9][0] = 6450;
         this.energycapacitors[9][1] = "Level 10";
         this.energycapacitors[9][2] = 1435000;
         this.tradegoods = new Array();
         this.tradegoods[0] = new Array();
         this.tradegoods[0][0] = "Food";
         this.tradegoods[1] = new Array();
         this.tradegoods[1][0] = "Wood";
         this.tradegoods[2] = new Array();
         this.tradegoods[2][0] = "Iron";
         this.tradegoods[3] = new Array();
         this.tradegoods[3][0] = "Machinery";
         this.tradegoods[4] = new Array();
         this.tradegoods[4][0] = "Tools";
         this.tradegoods[5] = new Array();
         this.tradegoods[5][0] = "Spare Parts";
         this.tradegoods[6] = new Array();
         this.tradegoods[6][0] = "Liquor";
         this.currentmissile = 0;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 120;
         this.missile[this.currentmissile][1] = 0;
         this.missile[this.currentmissile][2] = 8000;
         this.missile[this.currentmissile][3] = 3000;
         this.missile[this.currentmissile][4] = 1250;
         this.missile[this.currentmissile][5] = 500;
         this.missile[this.currentmissile][6] = "Torp 1";
         this.missile[this.currentmissile][7] = "Torp 1";
         this.missile[this.currentmissile][8] = "DUMBFIRE";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 110;
         this.missile[this.currentmissile][1] = 0;
         this.missile[this.currentmissile][2] = 11000;
         this.missile[this.currentmissile][3] = 3500;
         this.missile[this.currentmissile][4] = 2200;
         this.missile[this.currentmissile][5] = 1500;
         this.missile[this.currentmissile][6] = "Torp 2";
         this.missile[this.currentmissile][7] = "Torp 2";
         this.missile[this.currentmissile][8] = "DUMBFIRE";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 100;
         this.missile[this.currentmissile][1] = 0;
         this.missile[this.currentmissile][2] = 12000;
         this.missile[this.currentmissile][3] = 4000;
         this.missile[this.currentmissile][4] = 3400;
         this.missile[this.currentmissile][5] = 2750;
         this.missile[this.currentmissile][6] = "Torp 3";
         this.missile[this.currentmissile][7] = "Torp 3";
         this.missile[this.currentmissile][8] = "DUMBFIRE";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 90;
         this.missile[this.currentmissile][1] = 0;
         this.missile[this.currentmissile][2] = 15000;
         this.missile[this.currentmissile][3] = 5000;
         this.missile[this.currentmissile][4] = 5500;
         this.missile[this.currentmissile][5] = 4500;
         this.missile[this.currentmissile][6] = "Torp 4";
         this.missile[this.currentmissile][7] = "Torp 4";
         this.missile[this.currentmissile][8] = "DUMBFIRE";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 120;
         this.missile[this.currentmissile][1] = 0;
         this.missile[this.currentmissile][2] = 12000;
         this.missile[this.currentmissile][3] = 4000;
         this.missile[this.currentmissile][4] = 4500;
         this.missile[this.currentmissile][5] = 4250;
         this.missile[this.currentmissile][6] = "Stealth";
         this.missile[this.currentmissile][7] = "Stealth";
         this.missile[this.currentmissile][8] = "DUMBFIRE";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 100;
         this.missile[this.currentmissile][1] = 0;
         this.missile[this.currentmissile][2] = 7000;
         this.missile[this.currentmissile][3] = 4000;
         this.missile[this.currentmissile][4] = 2000;
         this.missile[this.currentmissile][5] = 1400;
         this.missile[this.currentmissile][6] = "Mirv 1";
         this.missile[this.currentmissile][7] = "Mirv 1";
         this.missile[this.currentmissile][8] = "DUMBFIRE";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 85;
         this.missile[this.currentmissile][1] = 0;
         this.missile[this.currentmissile][2] = 8000;
         this.missile[this.currentmissile][3] = 6000;
         this.missile[this.currentmissile][4] = 4000;
         this.missile[this.currentmissile][5] = 4000;
         this.missile[this.currentmissile][6] = "Mirv 2";
         this.missile[this.currentmissile][7] = "Mirv 2";
         this.missile[this.currentmissile][8] = "DUMBFIRE";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 70;
         this.missile[this.currentmissile][1] = 0;
         this.missile[this.currentmissile][2] = 9000;
         this.missile[this.currentmissile][3] = 7500;
         this.missile[this.currentmissile][4] = 6500;
         this.missile[this.currentmissile][5] = 8500;
         this.missile[this.currentmissile][6] = "Mirv 3";
         this.missile[this.currentmissile][7] = "Mirv 3";
         this.missile[this.currentmissile][8] = "DUMBFIRE";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 70;
         this.missile[this.currentmissile][1] = 65;
         this.missile[this.currentmissile][2] = 8000;
         this.missile[this.currentmissile][3] = 6500;
         this.missile[this.currentmissile][4] = 2500;
         this.missile[this.currentmissile][5] = 3500;
         this.missile[this.currentmissile][6] = "Seek 1";
         this.missile[this.currentmissile][7] = "Seek 1";
         this.missile[this.currentmissile][8] = "SEEKER";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 60;
         this.missile[this.currentmissile][1] = 55;
         this.missile[this.currentmissile][2] = 10000;
         this.missile[this.currentmissile][3] = 7500;
         this.missile[this.currentmissile][4] = 3950;
         this.missile[this.currentmissile][5] = 5500;
         this.missile[this.currentmissile][6] = "Seek 2";
         this.missile[this.currentmissile][7] = "Seek 2";
         this.missile[this.currentmissile][8] = "SEEKER";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 50;
         this.missile[this.currentmissile][1] = 35;
         this.missile[this.currentmissile][2] = 13000;
         this.missile[this.currentmissile][3] = 10000;
         this.missile[this.currentmissile][4] = 5300;
         this.missile[this.currentmissile][5] = 8500;
         this.missile[this.currentmissile][6] = "Seek 3";
         this.missile[this.currentmissile][7] = "Seek 3";
         this.missile[this.currentmissile][8] = "SEEKER";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 55;
         this.missile[this.currentmissile][1] = 45;
         this.missile[this.currentmissile][2] = 10000;
         this.missile[this.currentmissile][3] = 9000;
         this.missile[this.currentmissile][4] = 3500;
         this.missile[this.currentmissile][5] = 7500;
         this.missile[this.currentmissile][6] = "Seek ST";
         this.missile[this.currentmissile][7] = "Seek ST";
         this.missile[this.currentmissile][8] = "SEEKER";
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 65;
         this.missile[this.currentmissile][1] = 65;
         this.missile[this.currentmissile][2] = 15000;
         this.missile[this.currentmissile][3] = 6000;
         this.missile[this.currentmissile][4] = 0;
         this.missile[this.currentmissile][5] = 4000;
         this.missile[this.currentmissile][6] = "EMP 1";
         this.missile[this.currentmissile][7] = "EMP 1";
         this.missile[this.currentmissile][8] = "EMP";
         this.missile[this.currentmissile][9] = 3000;
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 60;
         this.missile[this.currentmissile][1] = 60;
         this.missile[this.currentmissile][2] = 12000;
         this.missile[this.currentmissile][3] = 7000;
         this.missile[this.currentmissile][4] = 0;
         this.missile[this.currentmissile][5] = 6000;
         this.missile[this.currentmissile][6] = "EMP 2";
         this.missile[this.currentmissile][7] = "EMP 2";
         this.missile[this.currentmissile][8] = "EMP";
         this.missile[this.currentmissile][9] = 6000;
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 50;
         this.missile[this.currentmissile][1] = 53;
         this.missile[this.currentmissile][2] = 10000;
         this.missile[this.currentmissile][3] = 9000;
         this.missile[this.currentmissile][4] = 0;
         this.missile[this.currentmissile][5] = 8000;
         this.missile[this.currentmissile][6] = "EMP 3";
         this.missile[this.currentmissile][7] = "EMP 3";
         this.missile[this.currentmissile][8] = "EMP";
         this.missile[this.currentmissile][9] = 10000;
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 60;
         this.missile[this.currentmissile][1] = 55;
         this.missile[this.currentmissile][2] = 10000;
         this.missile[this.currentmissile][3] = 9000;
         this.missile[this.currentmissile][4] = 0;
         this.missile[this.currentmissile][5] = 8000;
         this.missile[this.currentmissile][6] = "Disrupter 1";
         this.missile[this.currentmissile][7] = "Disrupter 1";
         this.missile[this.currentmissile][8] = "DISRUPTER";
         this.missile[this.currentmissile][9] = 2000;
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 50;
         this.missile[this.currentmissile][1] = 50;
         this.missile[this.currentmissile][2] = 10000;
         this.missile[this.currentmissile][3] = 11000;
         this.missile[this.currentmissile][4] = 0;
         this.missile[this.currentmissile][5] = 14000;
         this.missile[this.currentmissile][6] = "Disrupter 2";
         this.missile[this.currentmissile][7] = "Disrupter 2";
         this.missile[this.currentmissile][8] = "DISRUPTER";
         this.missile[this.currentmissile][9] = 4000;
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.missile[this.currentmissile] = new Array();
         this.missile[this.currentmissile][0] = 45;
         this.missile[this.currentmissile][1] = 45;
         this.missile[this.currentmissile][2] = 10000;
         this.missile[this.currentmissile][3] = 15000;
         this.missile[this.currentmissile][4] = 0;
         this.missile[this.currentmissile][5] = 20000;
         this.missile[this.currentmissile][6] = "Disrupter 3";
         this.missile[this.currentmissile][7] = "Disrupter 3";
         this.missile[this.currentmissile][8] = "DISRUPTER";
         this.missile[this.currentmissile][9] = 5500;
         this.missile[this.currentmissile][10] = getDefinitionByName("missile" + this.currentmissile + "fire") as Class;
         ++this.currentmissile;
         this.playerfunds = 0;
         this.extraplayerships = new Array();
         this.maxextraships = 3;
         this.playerscurrentextrashipno = 0;
         this.playershipstatus = new Array();
         this.playershipstatus[1] = new Array();
         this.playershipstatus[2] = new Array();
         this.playershipstatus[3] = new Array();
         this.playershipstatus[4] = new Array();
         this.playershipstatus[4][1] = new Array();
         this.playershipstatus[5] = new Array();
         this.playershipstatus[5][4] = "";
         this.playershipstatus[7] = new Array();
         this.playershipstatus[8] = new Array();
         this.playershipstatus[10] = new Array();
         this.playershipstatus[10][0] = 0;
         this.playershipstatus[10][1] = "Single";
         this.playershipstatus[11] = new Array();
         this.playershipstatus[11][1] = new Array();
         this.playershipstatus[5][20] = true;
         this.playershipstatus[5][21] = getTimer() + 7000;
         if(this.playershipstatus[6] == null)
         {
            this.playershipstatus[6] = new Array();
         }
         this.playershipstatus[6][0] = 0;
         this.playershipstatus[6][1] = 0;
         this.starbasestayhostiletime = 15000;
         this.teamdeathmatch = false;
         this.sectormapitems = new Array();
         this.starbaselocation = new Array();
         this.teambases = new Array();
         this.teambasetypes = new Array();
         this.playersquadbases = new Array();
         this.pulsarsinzone = false;
         this.sectorinformation = new Array();
         this.sectorinformation[1] = new Array();
         this.sectorinformation[1][0] = 1000;
         this.sectorinformation[1][1] = 1000;
         this.gamesetting = new Object();
         this.gamesetting.scrollingbckgrnd = true;
         this.gamesetting.showbckgrnd = true;
         this.gamesetting.totalstars = 20;
         this.gamesetting.MapKey = 77;
         this.gamesetting.OnlineListKey = 79;
         this.gamesetting.HelpKey = 72;
         this.gamesetting.newdefault = new Object();
         this.gamesetting.newdefault.accelkey = 87;
         this.gamesetting.newdefault.deaccelkey = 83;
         this.gamesetting.newdefault.turnleftkey = 65;
         this.gamesetting.newdefault.turnrightkey = 68;
         this.gamesetting.newdefault.missilekey = 76;
         this.gamesetting.newdefault.afterburnerskey = 74;
         this.gamesetting.newdefault.dockkey = 88;
         this.gamesetting.newdefault.gunskey = 75;
         this.gamesetting.newdefault.targeterkey = 84;
         this.gamesetting.newdefault.zoominkey = 188;
         this.gamesetting.newdefault.zoomoutkey = 190;
         this.gamesetting.olddefault = new Object();
         this.gamesetting.olddefault.accelkey = 38;
         this.gamesetting.olddefault.deaccelkey = 40;
         this.gamesetting.olddefault.turnleftkey = 37;
         this.gamesetting.olddefault.turnrightkey = 39;
         this.gamesetting.olddefault.missilekey = 32;
         this.gamesetting.olddefault.afterburnerskey = 16;
         this.gamesetting.olddefault.dockkey = 68;
         this.gamesetting.olddefault.gunskey = 17;
         this.gamesetting.olddefault.targeterkey = 84;
         this.gamesetting.olddefault.zoominkey = 188;
         this.gamesetting.olddefault.zoomoutkey = 190;
         this.func_setToNewDefaultGameKeys();
         this.targetinfo = new Array();
         this.targetinfo[0] = "None";
         this.shipcoordinatex = 0;
         this.shipcoordinatey = 0;
         this.playerjustloggedin = true;
         this.maxdockingvelocity = 20;
         this.playershiprotating = 0;
         this.halfgameareawidth = Math.round(1000 / 2);
         this.halfgameareaheight = Math.round(700 / 2);
         this.playershotsfired = new Array();
         this.playerBeingSeekedByMissile = false;
         this.currenttimechangeratio = 0;
         this.xwidthofasector = 1000;
         this.ywidthofasector = 1000;
         this.secondlastturning = 0;
         this.lastturning = 0;
         this.currentplayershotsfired = 0;
         this.currenthelpframedisplayed = 0;
         this.isupkeypressed = false;
         this.isdownkeypressed = false;
         this.isleftkeypressed = false;
         this.isrightkeypressed = false;
         this.iscontrolkeypressed = false;
         this.isdkeypressed = false;
         this.isshiftkeypressed = false;
         this.isspacekeypressed = false;
         this.keywaspressed = false;
         this.playershipafterburnerspeed = 120;
         this.playerrotationdegredation = 0.25;
         this.afterburnerinuse = false;
         this.spacekeyjustpressed = false;
         this.AnglePlayerShipFacing = 0;
         this.afterburnerspeed = 0;
         this.playersFakeVelocity = 0;
         this.shipXmovement = 0;
         this.shipYmovement = 0;
         this.playerShipSpeedRatio = 0;
         this.currentPIpacket = 0;
         this.gunShotBufferData = "";
         this.missileShotBuuferData = "";
         this.lastshipcoordinatex = 0;
         this.lastshipcoordinatey = 0;
         this.NextShipTimeResend = 0;
         this.InfoResendDelay = 5000;
         this.currentotherplayshot = 0;
         this.currentothermissileshot = 0;
         this.lastMissileSeek = getTimer();
         this.MissileSeekDelay = 500;
         stage.addEventListener(KeyboardEvent.KEY_DOWN,this.GameKeyListenerKeyPress);
         stage.addEventListener(KeyboardEvent.KEY_UP,this.GameKeyListenerKeyRelease);
         this.isPlayerChatting = false;
         this.ForwardKey = 87;
         this.LeftKey = 65;
         this.BackwardsKey = 83;
         this.RightKey = 68;
         this.EnterChatKey = 13;
         this.afterburnerkey = 74;
         this.gunfirekey = 75;
         this.missilekey = 76;
         this.dockkey = 88;
         this.MapKey = 77;
         this.TargetShipKey = 84;
         this.RadarBlot = getDefinitionByName("radarblot") as Class;
         this.teamdeathmatchinfo = new Array();
         this.teamdeathmatchinfo[0] = 300000;
         this.teamdeathmatchinfo[1] = 100000;
         this.teambasetypes[0] = new Array();
         this.teambasetypes[0][0] = 300000;
         this.teambasetypes[0][1] = 800;
         this.teambasetypes[0][2] = 10000;
         this.teambasetypes[0][3] = 300000;
         this.teambasetypes[0][4] = 20000;
         this.squadwarinfo = new Array();
         this.squadwarinfo[0] = false;
         this.squadwarinfo[1] = new Array();
         this.savestatus = "Save Game";
         this.lastplayerssavedinfo = "";
         this.lastplayerssavedinfosent = "";
      }
      
      public function setMouseIsDown(param1:MouseEvent) : void
      {
         this.TurretMouseDown = true;
      }
      
      public function func_sendstatuscheck(param1:*) : *
      {
         if(!isNaN(param1))
         {
            this.datatosend = "STATS`TGT`GET`" + this.playershipstatus[3][0] + "`" + param1 + "~";
            this.mysocket.send(this.datatosend);
         }
      }
      
      public function SpecialsTimerHandler(param1:TimerEvent) : void
      {
         var event:TimerEvent = param1;
         try
         {
            this.func_updateSpecialsDisplay();
         }
         catch(error:Error)
         {
         }
      }
      
      internal function frame1() : *
      {
         this.playerSpecialsSettings = new Object();
         this.playerSpecialsSettings.isCloaked = false;
         this.playerSpecialsSettings.CloakEnergy = 0;
         this.playerSpecialsSettings.CloakLocation = 0;
         this.playerSpecialsSettings.isStealthed = false;
         this.playerSpecialsSettings.StealthEnergy = 0;
         this.playerSpecialsSettings.StealthLocation = 0;
      }
      
      internal function frame16() : *
      {
         stop();
         try
         {
         }
         catch(error:Error)
         {
            trace("Error: " + error);
         }
         this.IsSocketConnected = false;
         this.timebannedfor = 0;
         this.controlledserverclose = false;
         this.gameerror = "hostclosedconnection";
         this.gameerrordoneby = "";
         this.isgamerunningfromremote = false;
         this.framestobuffer = 4;
         this.othershipbuffer = this.framestobuffer;
         this.shiptimeoutime = 10000;
         this.timeintervalcheck = getTimer();
         this.otherplayership = new Array();
         this.currentotherplayshot = 0;
         this.othergunfire = new Array();
         this.othermissilefire = new Array();
         this.justenteredgame = true;
         this.mysocket = new XMLSocket();
         this.currentgamestatustext = "";
         this.currentgamestatustext += "Connecting to Game Server \r";
         this.currentgamestatustext += "Logging In \r";
         this.currentip = "";
         this.currentport = 2151;
         if(this.isgamerunningfromremote == true)
         {
            this.currentip = "127.0.0.1";
            this.ClockCheckstodo = 2;
            this.mysocket.connect(this.currentip,this.currentport);
         }
         else
         {
            this.ClockCheckstodo = 8;
            this.currentip = "";
            this.mysocket.connect(this.currentip,this.currentport);
         }
         this.mysocket.addEventListener(DataEvent.DATA,this.ReceivedData);
         this.mysocket.addEventListener(Event.CONNECT,this.onConnect);
         this.mysocket.addEventListener(Event.CLOSE,this.onClose);
         this.mysocket.addEventListener(IOErrorEvent.IO_ERROR,this.onSocketError);
      }
      
      public function func_setHeadingLocation() : *
      {
         if(this.playersdestination[0] > -1)
         {
            this.playersdestination[1] = this.sectormapitems[this.playersdestination[0]][1];
            this.playersdestination[2] = this.sectormapitems[this.playersdestination[0]][2];
            this.playersdestination[4] = this.sectormapitems[this.playersdestination[0]][0];
         }
      }
      
      internal function frame26() : *
      {
         this.turretcontrol.auto.addEventListener(MouseEvent.MOUSE_DOWN,this.autoTurretsSelected);
         this.turretcontrol.manual.addEventListener(MouseEvent.MOUSE_DOWN,this.manualTurretsSelected);
         this.turretcontrol.off.addEventListener(MouseEvent.MOUSE_DOWN,this.offTurretsSelected);
         this.turretCrosshairs.visible = false;
         this.TurretMouseDown = false;
         this.func_setTurrets("off");
         this.turretCrosshairs.addEventListener(MouseEvent.MOUSE_DOWN,this.setMouseIsDown);
         this.turretCrosshairs.addEventListener(MouseEvent.MOUSE_UP,this.setMouseIsUp);
         stop();
         this.playerSpecialsSettings.isCloaked = false;
         this.playerSpecialsSettings.CloakEnergy = 0;
         this.playerSpecialsSettings.CloakLocation = 0;
         this.playerSpecialsSettings.isStealthed = false;
         this.playerSpecialsSettings.StealthEnergy = 0;
         this.playerSpecialsSettings.StealthLocation = 0;
         this.func_resetSpecials();
         this.func_displayallspecials();
         this.SpecialsTimer = new Timer(500);
         this.SpecialsTimer.addEventListener(TimerEvent.TIMER,this.SpecialsTimerHandler);
         this.SpecialsTimer.start();
         stop();
         this.NavigationTimer = new Timer(350);
         this.NavigationTimer.addEventListener(TimerEvent.TIMER,this.NavigationTimerHandler);
         this.NavigationTimer.start();
         this.TargetDisplay.visible = false;
         this.targetinfo[0] = "";
         this.targetinfo[4] = 99999999999999;
         this.TargetTimer = new Timer(350);
         this.TargetTimer.addEventListener(TimerEvent.TIMER,this.TargetTimerHandler);
         this.TargetTimer.start();
         this.missileBankWindow.fireStyle.addEventListener(MouseEvent.MOUSE_DOWN,this.MissileFireStyleChanged);
         if(this.playershipstatus[10][0] > this.playershipstatus[7].length)
         {
            this.playershipstatus[10][0] = 0;
         }
         this.missileBankWindow.func_buildDisplay(this.playershipstatus[7],this.playershipstatus[10][0],this.missile);
         this.missileBankWindow.func_RefreshDisplay(this.playershipstatus[7],this.playershipstatus[10][0],this.playershipstatus[10][1]);
         this.MissileTimer = new Timer(350);
         this.MissileTimer.addEventListener(TimerEvent.TIMER,this.MissileTimerHandler);
         this.MissileTimer.start();
         this.setRadarScale();
         this.speedratio = 0;
         this.playersshiptype = this.playershipstatus[5][0];
         this.totalstars = 30;
         this.backgroundstar = new Array();
         this.doublegameareawidth = 1600;
         this.doublegameareaheight = 1000;
         this.BackgroundMaxWidth = 1000 * Number(this.sectorinformation[0][0]);
         this.HalfBackgroundWidth = this.BackgroundMaxWidth / 2;
         this.BackgroundMaxHeight = 1000 * Number(this.sectorinformation[0][1]);
         this.HalfBackgroundMaxHeight = this.BackgroundMaxHeight / 2;
         this.otherplayership = new Array();
         this.othergunfire = new Array();
         this.playershotsfired = new Array();
         this.arrayZoomFactors = new Array();
         this.arrayZoomFactors[0] = 100;
         this.arrayZoomFactors[1] = 90;
         this.arrayZoomFactors[2] = 80;
         this.arrayZoomFactors[3] = 70;
         this.arrayZoomFactors[4] = 60;
         this.arrayZoomFactors[5] = 55;
         this.currZoomFactor = 0;
         this.currentStarFrame = 100;
         this.updateStarInterval = 0;
         this.curentGametime = getTimer();
         this.LastFrameTime = getTimer();
         if(!this.gamesetting.showbckgrnd)
         {
            this.gamedisplayarea.backgroundImage.visible = false;
         }
         this.func_InitPlayerAfterDock();
         addEventListener(Event.ENTER_FRAME,this.func_OnGameFrame);
         this.gamebackground = new MovieClip();
         this.gamedisplayarea.addChild(this.gamebackground);
         this.makebackgroundstars();
         this.func_loadSectorItemsIntoBackgroup();
         this.NavigationImageToAttach = getDefinitionByName("NavigationDirectorImage") as Class;
         this.NavigationImage = new this.NavigationImageToAttach() as MovieClip;
         this.gamedisplayarea.addChild(this.NavigationImage);
         this.NavigationImage.visible = false;
         this.PlayersShipImage = new this.shiptype[this.playersshiptype][10]() as MovieClip;
         this.PlayersShipImage.name = "PlayersShipImage";
         this.gamedisplayarea.addChild(this.PlayersShipImage);
         this.PlayersShipShieldImage = new this.shiptype[this.playersshiptype][11]() as MovieClip;
         this.gamedisplayarea.addChild(this.PlayersShipShieldImage);
         this.PlayersShipShieldImage.gotoAndStop(4);
         this.PlayersShipImage.gotoAndStop(1);
         this.PlayersShipImage.x = 0;
         this.PlayersShipImage.y = 0;
      }
      
      internal function frame17() : *
      {
         this.gameSettingScreen.addEventListener(MouseEvent.MOUSE_UP,this.func_GameSettingScreenPressed);
         stop();
      }
      
      public function func_OnGameFrame(param1:Event) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         this.curentGametime = getTimer();
         _loc2_ = (this.curentGametime - this.LastFrameTime) * 0.001;
         this.LastFrameTime = this.curentGametime;
         _loc2_ = _loc2_;
         this.Proccess_Energies(_loc2_);
         _loc3_ = this.gunShotBufferData;
         this.func_MoveGunShots(_loc2_,this.curentGametime);
         this.othermissilefiremovement(_loc2_);
         if(_loc3_ != this.gunShotBufferData)
         {
            this.gunShotBufferData += "LFE`" + this.playershipstatus[3][0] + "`" + this.playershipstatus[2][5] + "`" + this.playershipstatus[2][1] + "`~";
         }
         this.func_RunOtherSips(_loc2_);
         this.shipXmovement = this.shipcoordinatex - this.lastshipcoordinatex;
         this.shipYmovement = this.shipcoordinatey - this.lastshipcoordinatey;
         this.lastshipcoordinatex = this.shipcoordinatex;
         this.lastshipcoordinatey = this.shipcoordinatey;
         this.func_MoveBackGroundStars();
         if(this.gamesetting.showbckgrnd)
         {
            if(this.gamesetting.scrollingbckgrnd)
            {
               this.func_MoveBackground();
            }
         }
         this.gamebackground.x = -this.shipcoordinatex;
         this.gamebackground.y = -this.shipcoordinatey;
         this.func_UpdateRadarPosition(this.shipcoordinatex,this.shipcoordinatey);
         _loc4_ = "";
         _loc4_ = this.PlayerControlsScript(_loc2_);
         this.func_TurretControlScript(this.curentGametime);
         if(_loc4_ != "")
         {
            _loc4_ += this.gunShotBufferData;
            this.mysocket.send(_loc4_);
            _loc4_ = "";
            this.gunShotBufferData = "";
         }
         else if(this.gunShotBufferData != "")
         {
            _loc5_ = "PI`FALSE`~" + this.gunShotBufferData;
            this.gunShotBufferData = "";
            this.mysocket.send(_loc5_);
         }
         this.PlayersShipShieldImage.rotation = this.PlayersShipImage.rotation;
      }
      
      internal function frame4() : *
      {
         if(framesLoaded == totalFrames)
         {
            play();
         }
         else
         {
            gotoAndPlay(1);
         }
      }
      
      public function PlayerControlsScript(param1:*) : *
      {
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         this.playerShipSpeedRatio = this.playershipvelocity / this.playershipmaxvelocity;
         var _loc2_:* = "";
         var _loc3_:* = this.playershipstatus[5][0];
         var _loc4_:* = this.shiptype[_loc3_][3][2];
         this.secondlastturning = 0;
         this.lastturning = 0;
         var _loc5_:* = this.sectorinformation[1][0];
         var _loc6_:* = this.sectorinformation[1][1];
         var _loc7_:* = 0;
         var _loc8_:* = getTimer();
         var _loc9_:* = new Array();
         if(this.playershipstatus[5][4] == "alive")
         {
            if(this.iscontrolkeypressed)
            {
               this.func_runFireScript(_loc8_);
            }
            if(this.isspacekeypressed)
            {
               this.func_runMissileFireScript(_loc8_);
            }
            if(this.isshiftkeypressed == true)
            {
               this.playersFakeVelocity += this.playershipacceleration * param1 * 2;
               if(this.playersFakeVelocity >= this.playershipmaxvelocity)
               {
                  this.playersFakeVelocity = this.playershipafterburnerspeed;
               }
            }
            if(!this.isshiftkeypressed)
            {
               if(this.isupkeypressed)
               {
                  this.playersFakeVelocity += this.playershipacceleration * param1;
                  if(this.playersFakeVelocity > this.playershipmaxvelocity)
                  {
                     this.playersFakeVelocity = this.playershipmaxvelocity;
                  }
               }
               else if(this.isdownkeypressed)
               {
                  if(this.playersFakeVelocity > 0 && this.playersFakeVelocity < this.playershipacceleration * param1)
                  {
                     this.playersFakeVelocity = 0;
                  }
                  if(this.playersFakeVelocity > 0)
                  {
                     this.playersFakeVelocity -= this.playershipacceleration * param1;
                  }
               }
               if(this.playersFakeVelocity > this.playershipmaxvelocity)
               {
                  this.playersFakeVelocity = this.playershipmaxvelocity;
               }
            }
            _loc10_ = 0;
            if(this.isrightkeypressed == true)
            {
               _loc10_ += this.playershiprotation;
            }
            if(this.isleftkeypressed == true)
            {
               _loc10_ -= this.playershiprotation;
            }
            this.playerShipSpeedRatio = this.playersFakeVelocity / this.playershipmaxvelocity;
            if(_loc10_ != 0)
            {
               this.PlayersShipImage.rotation += _loc10_ * param1;
            }
            this.playershipstatus[6][0] = Math.ceil(this.shipcoordinatex / _loc5_);
            this.playershipstatus[6][1] = Math.ceil(this.shipcoordinatey / _loc6_);
            _loc11_ = this.playershipstatus[6][0] + "`" + this.playershipstatus[6][1];
            if((_loc12_ = this.playershipstatus[6][2] + "`" + this.playershipstatus[6][3]) != _loc11_ || this.keywaspressed == true || this.NextShipTimeResend < getTimer())
            {
               this.playershipstatus[6][2] = this.playershipstatus[6][0];
               this.playershipstatus[6][3] = this.playershipstatus[6][1];
               this.keywaspressed = false;
               _loc2_ = this.func_GetPlayerSEndOutData(_loc11_,_loc12_,getTimer());
               this.NextShipTimeResend = getTimer() + this.InfoResendDelay;
            }
         }
         if(this.isdkeypressed)
         {
            this.func_trytoDockTheShip();
         }
         return _loc2_;
      }
      
      internal function frame14() : *
      {
         this.chatDisplay.visible = true;
         this.regularchattextcolor = "#33FFFF";
         this.privatechattextcolor = "#33ff33";
         this.systemchattextcolor = "#33ff33";
         this.teamchattextcolor = "#ffff00";
         this.squadchattextcolor = "#dd3333";
         this.arenachattextcolor = "#ff00ff";
         this.staffhelpchattextcolor = "#cccc66";
         this.stafchattextcolor = "#cccc66";
         this.refreshchatdisplay = false;
         this.gamechatinfo = new Array();
         this.gamechatinfo[1] = new Array();
         this.gamechatinfo[2] = new Array();
         this.gamechatinfo[2][2] = false;
         this.gamechatinfo[2][1] = 30;
         this.gamechatinfo[3] = new Array();
         this.gamechatinfo[3][1] = false;
         this.gamechatinfo[6] = new Array();
         this.gamechatinfo[7] = new Array();
         this.gamechatinfo[7][0] = new Array();
         this.func_buildBlankChat();
         this.func_RefreshChat();
         this.func_enterintochat("Welcome To Alpha Testing of the new Client, please respect all bugs / exploits that exist.",this.arenachattextcolor);
         this.chatDisplay.visible = false;
         this.chatDisplay.textoutline.gotoAndStop(1);
         this.chatDisplay.chatInput.text = "";
         this.chatDisplay.chatInput.addEventListener(FocusEvent.FOCUS_IN,this.chatfocusInHandler);
         this.chatDisplay.chatInput.addEventListener(FocusEvent.FOCUS_OUT,this.chatfocusOutHandler);
         this.enterbadwords();
         this.replacewithchar = new Array();
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&apos;";
         this.replacewithchar[this.loc][1] = "\'";
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&quot;";
         this.replacewithchar[this.loc][1] = "\"";
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&gt;";
         this.replacewithchar[this.loc][1] = ">";
         this.loc = this.replacewithchar.length;
         this.replacewithchar[this.loc] = new Array();
         this.replacewithchar[this.loc][0] = "&amp;";
         this.replacewithchar[this.loc][1] = "&";
      }
      
      public function MissileTimerHandler(param1:TimerEvent) : void
      {
         var event:TimerEvent = param1;
         try
         {
            this.playershipstatus[10][0] = this.missileBankWindow.func_RefreshDisplay(this.playershipstatus[7],this.playershipstatus[10][0],this.playershipstatus[10][1]);
         }
         catch(error:Error)
         {
            MissileTimer.stop();
         }
      }
      
      public function func_TriggerGameHelp() : *
      {
         if(this.playershipstatus[5][4] != "")
         {
            if(this.inGameHelp.visible)
            {
               this.inGameHelp.visible = false;
            }
            else
            {
               this.inGameHelp.visible = true;
               if(this.playershipstatus[5][12] == "MOD" || this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "ADMIN")
               {
                  this.inGameHelp.butt9.visible = true;
               }
               else
               {
                  this.inGameHelp.butt9.visible = false;
               }
            }
         }
      }
      
      internal function frame18() : *
      {
         this.loginmovie.mysocket = this.mysocket;
         stop();
      }
      
      public function DisplayOnlinePlayersList() : *
      {
         var _loc1_:* = "";
         var _loc2_:* = 0;
         var _loc3_:* = 0;
         while(_loc3_ < this.currentonlineplayers.length)
         {
            if(this.currentonlineplayers[_loc3_][0] >= 0)
            {
               _loc2_++;
            }
            _loc3_++;
         }
         _loc1_ = "";
         _loc1_ += "( O )Online System Players: " + _loc2_ + " (" + (Number(this.currentonlineplayers.length) - _loc2_) + ")  ";
         this.OnlinePLayerList.onlinePlayersListing.text = _loc1_;
         if(this.OnlinePLayerList.currentFrame == 2)
         {
            this.DisplayOnlinePlayersListDetails();
         }
      }
      
      public function func_admincommands(param1:*) : *
      {
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         var _loc15_:* = undefined;
         var _loc16_:* = undefined;
         var _loc17_:* = undefined;
         var _loc18_:* = undefined;
         var _loc19_:* = undefined;
         var _loc20_:* = undefined;
         var _loc2_:* = param1.toUpperCase();
         var _loc3_:* = "";
         var _loc4_:* = 0;
         var _loc5_:* = 0;
         var _loc6_:* = 0;
         if(_loc2_.substr(0,6) == "/*KICK")
         {
            if((_loc4_ = this.func_namelocation(_loc2_.substr(7))) != null)
            {
               _loc2_ = "ADMIN`KICK`" + _loc4_ + "~";
               this.mysocket.send(_loc2_);
            }
         }
         else if(_loc2_.substr(0,14) == "/*IPUSEDBYNAME")
         {
            _loc3_ = _loc2_.substr(15);
            if(_loc3_ != null)
            {
               _loc2_ = "ADMIN`IPUSEDBYNAME`" + _loc3_ + "~";
               this.mysocket.send(_loc2_);
            }
         }
         else if(_loc2_.substr(0,15) == "/*NAMESUSEDBYIP")
         {
            _loc3_ = _loc2_.substr(16);
            if(_loc3_ != null)
            {
               _loc2_ = "ADMIN`NAMESUSEDBYIP`" + _loc3_ + "~";
               this.mysocket.send(_loc2_);
            }
         }
         else if(_loc2_.substr(0,6) == "/*PING")
         {
            _loc2_ = "PING`" + getTimer() + "~";
            this.mysocket.send(_loc2_);
         }
         else if(_loc2_.substr(0,13) == "/*DELETESQUAD")
         {
            _loc3_ = _loc2_.substr(14);
            if(_loc3_ != null)
            {
               _loc2_ = "ADMIN`DELETESQUAD`" + _loc3_ + "~";
               this.mysocket.send(_loc2_);
            }
         }
         else if(_loc2_.substr(0,11) == "/*SQUADINFO")
         {
            _loc3_ = _loc2_.substr(12);
            if(_loc3_ != null)
            {
               _loc2_ = "ADMIN`SQUADINFO`" + _loc3_ + "~";
               this.mysocket.send(_loc2_);
            }
         }
         else if(_loc2_.substr(0,6) == "/*MUTE")
         {
            if((_loc4_ = this.func_namelocation(_loc2_.substr(7))) != null)
            {
               _loc2_ = "ADMIN`MUTE`" + _loc4_ + "~";
               this.mysocket.send(_loc2_);
            }
         }
         else if(_loc2_.substr(0,7) == "/*ARENA")
         {
            _loc15_ = param1.substr(8);
            _loc2_ = "CHT~CH`" + this.playershipstatus[3][0] + "`AM`" + _loc15_ + "`" + "~";
            if(this.gamechatinfo[2][2] == false)
            {
               this.mysocket.send(_loc2_);
            }
            else
            {
               this.func_enterintochat(" You Are Muted ",this.systemchattextcolor);
            }
         }
         else if(_loc2_.substr(0,11) == "/*ALLARENAS")
         {
            _loc15_ = param1.substr(12);
            _loc2_ = "CHT~CH`" + this.playershipstatus[3][0] + "`SVR`" + _loc15_ + "`" + "~";
            if(this.gamechatinfo[2][2] == false)
            {
               this.mysocket.send(_loc2_);
            }
            else
            {
               this.func_enterintochat(" You Are Muted ",this.systemchattextcolor);
            }
         }
         else if(_loc2_.substr(0,7) == "/*FUNDS")
         {
            _loc7_ = 8;
            _loc5_ = 8;
            while(_loc5_ < _loc2_.length)
            {
               if(_loc2_.charAt(_loc5_) == " ")
               {
                  _loc6_ = _loc5_ + 1;
                  _loc8_ = _loc5_;
                  break;
               }
               _loc5_++;
            }
            _loc16_ = Number(_loc2_.substr(_loc7_,_loc8_ - _loc7_));
            if((_loc4_ = this.func_namelocation(_loc2_.substr(_loc6_))) != null && !isNaN(_loc16_))
            {
               _loc2_ = "ADMIN`FUNDS`" + _loc16_ + "`" + _loc4_ + "~";
               this.mysocket.send(_loc2_);
            }
         }
         else if(_loc2_.substr(0,7) == "/*SCORE")
         {
            if(this.playershipstatus[5][12] == "ADMIN")
            {
               _loc17_ = 8;
               _loc5_ = 8;
               while(_loc5_ < _loc2_.length)
               {
                  if(_loc2_.charAt(_loc5_) == " ")
                  {
                     _loc6_ = _loc5_ + 1;
                     _loc19_ = _loc5_;
                     break;
                  }
                  _loc5_++;
               }
               _loc18_ = Number(_loc2_.substr(_loc17_,_loc19_ - _loc17_)) * this.scoreratiomodifier;
               if((_loc4_ = this.func_namelocation(_loc2_.substr(_loc6_))) != null && !isNaN(_loc18_))
               {
                  _loc2_ = "ADMIN`" + _loc4_ + "`SCORE`" + _loc18_ + "~";
                  this.mysocket.send(_loc2_);
               }
            }
         }
         else if(_loc2_.substr(0,14) == "/*CREATEPIRATE")
         {
            if(this.playershipstatus[5][12] == "ADMIN")
            {
            }
         }
         else if(_loc2_.substr(0,7) != "/*PRICE")
         {
            if(_loc2_.substr(0,4) == "/*IP")
            {
               if((_loc4_ = this.func_namelocation(_loc2_.substr(5))) != null)
               {
                  _loc2_ = "ADMIN`IP`" + _loc4_ + "~";
                  this.mysocket.send(_loc2_);
               }
            }
            else if(_loc2_.substr(0,5) == "/*BAN")
            {
               if(this.playershipstatus[5][12] == "ADMIN" || this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "MOD")
               {
                  _loc17_ = 6;
                  _loc5_ = 6;
                  while(_loc5_ < _loc2_.length)
                  {
                     if(_loc2_.charAt(_loc5_) == " ")
                     {
                        _loc6_ = _loc5_ + 1;
                        _loc19_ = _loc5_;
                        break;
                     }
                     _loc5_++;
                  }
                  _loc20_ = Number(_loc2_.substr(_loc17_,_loc19_ - _loc17_));
                  if((_loc4_ = this.func_namelocation(_loc2_.substr(_loc6_))) != null && !isNaN(_loc20_))
                  {
                     _loc2_ = "ADMIN`" + _loc4_ + "`BAN`" + _loc20_ + "~";
                     this.mysocket.send(_loc2_);
                  }
               }
            }
            else if(_loc2_.substr(0,7) == "/*UNBAN")
            {
               if(this.playershipstatus[5][12] == "ADMIN")
               {
                  _loc6_ = 8;
                  _loc13_ = _loc2_.substr(_loc6_);
                  _loc2_ = "ADMIN`UNBAN`" + _loc13_ + "`~";
                  this.mysocket.send(_loc2_);
               }
            }
            else if(_loc2_.substr(0,6) == "/*INFO")
            {
               _loc14_ = _loc2_.substr(7);
               if(this.playershipstatus[5][12] == "ADMIN")
               {
                  this.datatosend = "ADMIN`INFO`" + _loc14_ + "`~";
                  this.mysocket.send(this.datatosend);
               }
            }
            else if(_loc2_.substr(0,9) == "/*PROMOTE")
            {
               _loc14_ = _loc2_.substr(10);
               if(this.playershipstatus[5][12] == "ADMIN")
               {
                  this.datatosend = "ADMIN`PROMOTE`" + _loc14_ + "`~";
                  this.mysocket.send(this.datatosend);
               }
            }
            else if(_loc2_.substr(0,8) == "/*DEMOTE")
            {
               _loc14_ = _loc2_.substr(9);
               if(this.playershipstatus[5][12] == "ADMIN")
               {
                  this.datatosend = "ADMIN`DEMOTE`" + _loc14_ + "`~";
                  this.mysocket.send(this.datatosend);
               }
            }
         }
      }
      
      public function NavigationTimerHandler(param1:TimerEvent) : void
      {
         var event:TimerEvent = param1;
         try
         {
            this.func_updateNavigationDisplay();
         }
         catch(error:Error)
         {
            NavigationTimer.stop();
         }
      }
      
      internal function frame39() : *
      {
         this.playershipstatus[2][5] = 0;
         this.playershipstatus[5][4] = "dead";
         this.playershipvelocity = 0;
         this.bountychanging = Number(this.playershipstatus[5][3] + this.playershipstatus[5][8]);
         if(this.bountychanging < 1 || this.bountychanging > 100000000 || isNaN(this.bountychanging))
         {
            this.playershipstatus[5][3] = 0;
            this.playershipstatus[5][8] = 0;
         }
         if(this.playershipstatus[3][0] == this.playershipstatus[5][5])
         {
            this.schange = (this.playershipstatus[5][3] + this.playershipstatus[5][8]) * -1;
         }
         else
         {
            this.schange = this.playershipstatus[5][3] + this.playershipstatus[5][8];
         }
         if(this.playershipstatus[3][0] == this.playershipstatus[5][5])
         {
            this.tb = 0;
         }
         else
         {
            this.tb = this.playershipstatus[5][3] + this.playershipstatus[5][8];
         }
         this.datatosend = "TC" + "`" + this.playershipstatus[3][0] + "`SD`" + this.playershipstatus[5][5] + "`" + this.tb + "`" + "0" + "~";
         trace(this.datatosend);
         this.mysocket.send(this.datatosend);
         this.playershipstatus[5][3] = 0;
         this.playershipstatus[5][8] = 0;
         this.deathmenu.deathbutton.addEventListener(MouseEvent.MOUSE_UP,this.func_AcceptDeathButton_Click);
         stop();
      }
      
      public function manualTurretsSelected(param1:MouseEvent) : void
      {
         this.func_setTurrets("manual");
      }
      
      public function enterbadwords() : *
      {
         this.badwords = new Array();
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "fuck";
         this.badwords[this.loc][1] = "f**k";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "shit";
         this.badwords[this.loc][1] = "s**t";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "asshole";
         this.badwords[this.loc][1] = "a**h*le";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "nigger";
         this.badwords[this.loc][1] = "ni**er";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "whore";
         this.badwords[this.loc][1] = "wh**e";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "cunt";
         this.badwords[this.loc][1] = "c*nt";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "cock";
         this.badwords[this.loc][1] = "c*ck";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "pussy";
         this.badwords[this.loc][1] = "pu**y";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "fag";
         this.badwords[this.loc][1] = "f*g";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "bitch";
         this.badwords[this.loc][1] = "b**ch";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "jackass";
         this.badwords[this.loc][1] = "j**k*ss";
         this.loc = this.badwords.length;
         this.badwords[this.loc] = new Array();
         this.badwords[this.loc][0] = "fuk";
         this.badwords[this.loc][1] = "f*k";
      }
      
      public function chatfocusOutHandler(param1:FocusEvent) : void
      {
         this.chatDisplay.chatInput.text = "";
         this.isPlayerChatting = false;
         this.chatDisplay.textoutline.gotoAndStop(1);
      }
      
      public function TargetTimerHandler(param1:TimerEvent) : void
      {
         var event:TimerEvent = param1;
         try
         {
            this.func_updateTargetDisplay();
         }
         catch(error:Error)
         {
            TargetTimer.stop();
            trace("Error Target");
         }
      }
      
      public function saveplayerextraships() : *
      {
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc1_:* = "";
         _loc1_ += "NO" + this.playerscurrentextrashipno + "~";
         var _loc2_:* = 0;
         while(_loc2_ < this.extraplayerships.length)
         {
            if(this.extraplayerships[_loc2_] != null)
            {
               if(this.extraplayerships[_loc2_][0] != null)
               {
                  _loc1_ += "SH" + _loc2_ + "`";
                  _loc1_ += "ST" + this.extraplayerships[_loc2_][0] + "`";
                  _loc1_ += "SG" + this.extraplayerships[_loc2_][1] + "`";
                  _loc1_ += "EG" + this.extraplayerships[_loc2_][2] + "`";
                  _loc1_ += "EC" + this.extraplayerships[_loc2_][3] + "`";
                  _loc3_ = 0;
                  while(_loc3_ < this.shiptype[this.extraplayerships[_loc2_][0]][2].length)
                  {
                     _loc1_ += "HP" + _loc3_ + "G" + this.extraplayerships[_loc2_][4][_loc3_] + "`";
                     _loc3_++;
                  }
                  _loc4_ = 0;
                  while(_loc4_ < this.shiptype[this.extraplayerships[_loc2_][0]][5].length)
                  {
                     _loc1_ += "TT" + _loc4_ + "G" + this.extraplayerships[_loc2_][5][_loc4_] + "`";
                     _loc4_++;
                  }
                  _loc5_ = 0;
                  while(_loc5_ < this.extraplayerships[_loc2_][11][1].length)
                  {
                     _loc1_ += "SP" + this.extraplayerships[_loc2_][11][1][_loc5_][0] + "Q" + this.extraplayerships[_loc2_][11][1][_loc5_][1] + "`";
                     _loc5_++;
                  }
                  _loc1_ += "~";
               }
            }
            _loc2_++;
         }
         return _loc1_;
      }
      
      public function disruptplayersengines() : *
      {
         this.isupkeypressed = false;
         this.isdownkeypressed = true;
         this.isshiftkeypressed = false;
         this.keywaspressed = true;
      }
      
      public function currentkeyedshipmovement() : *
      {
         var _loc2_:* = undefined;
         var _loc1_:* = "";
         if(this.isrightkeypressed == true)
         {
            _loc1_ += "R";
         }
         if(this.isleftkeypressed == true)
         {
            _loc1_ += "L";
         }
         if(_loc1_ == "RL")
         {
            _loc1_ = "";
         }
         if(this.isshiftkeypressed == true)
         {
            _loc1_ += "S";
         }
         else
         {
            _loc2_ = "";
            if(this.isupkeypressed == true)
            {
               _loc2_ += "U";
            }
            if(this.isdownkeypressed == true)
            {
               _loc2_ += "D";
            }
            if(_loc2_ != "UD")
            {
               _loc1_ += _loc2_;
            }
         }
         return _loc1_;
      }
      
      public function func_namelocation(param1:*) : *
      {
         var _loc2_:* = null;
         param1 = param1.toUpperCase();
         var _loc3_:* = 0;
         while(_loc3_ < this.currentonlineplayers.length)
         {
            if(param1 == this.currentonlineplayers[_loc3_][1].toUpperCase())
            {
               _loc2_ = this.currentonlineplayers[_loc3_][0];
               break;
            }
            _loc3_++;
         }
         return _loc2_;
      }
      
      public function func_PingRadar() : *
      {
         this.gameRadar.radarPing.play();
      }
      
      public function fun_showStats() : *
      {
         var zz:* = undefined;
         var xFromPlayer:* = undefined;
         var yFromPlayer:* = undefined;
         this.TargetDisplay.visible = true;
         if(this.targetinfo[4] < 4000)
         {
            try
            {
               zz = this.targetinfo[5];
               this.TargetDisplay.currenttargetname.text = this.func_getPlayersName(this.otherplayership[zz][0]);
               this.TargetDisplay.structdisp.text = Math.ceil(this.otherplayership[zz][40]) + " / " + Math.ceil(this.otherplayership[zz][47]);
               this.TargetDisplay.shielddisp.text = Math.ceil(this.otherplayership[zz][41]) + " / " + Math.ceil(this.otherplayership[zz][43]);
               xFromPlayer = this.otherplayership[zz][21].x;
               yFromPlayer = this.otherplayership[zz][21].y;
               this.TargetDisplay.currenttargetdist.text = Math.ceil(Math.sqrt(xFromPlayer * xFromPlayer + yFromPlayer * yFromPlayer));
               this.TargetDisplay.targetpointer.rotation = 180 - Math.atan2(xFromPlayer,yFromPlayer) / (Math.PI / 180);
            }
            catch(error:Error)
            {
               trace("Fail Target Display");
            }
         }
      }
      
      public function func_ignoreplayer(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         _loc2_ = false;
         _loc3_ = 0;
         while(_loc3_ < this.gamechatinfo[6].length)
         {
            if(param1 == this.gamechatinfo[6][_loc3_])
            {
               _loc2_ = true;
               break;
            }
            _loc3_++;
         }
         return _loc2_;
      }
      
      internal function frame60() : *
      {
         this.credDisplayTimer = new Timer(500);
         this.credDisplayTimer.addEventListener(TimerEvent.TIMER,this.credDisplayTimerHandler);
         this.credDisplayTimer.start();
         this.inGameHelpButton.visible = true;
         this.inGameMapButton.visible = true;
         this.saveplayersgame();
         try
         {
            removeEventListener(Event.ENTER_FRAME,this.func_OnGameFrame);
         }
         catch(error:Error)
         {
            trace("Game Event is not loaded");
         }
         try
         {
         }
         catch(error:Error)
         {
            trace("Game Timer is not loaded");
         }
         this.chatDisplay.visible = true;
         this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.visible = false;
         this.creditBar.creditsToDisp = this.playershipstatus[3][1];
         this.playersexitdocktimewait = 0;
         this.timePlayerCanExit = getTimer() + this.playersexitdocktimewait;
         this.playershipstatus[5][4] = "docking";
         this.main_docked_screen.dockedbackground.docked_image_holder.base_exit_button.alpha = 0;
         this.main_docked_screen.dockedbackground.docked_image_holder.base_exit_button.addEventListener(MouseEvent.MOUSE_UP,this.func_ExitBaseButton_Click);
         this.main_docked_screen.dockedbackground.docked_image_holder.base_hardware_button.alpha = 0;
         this.main_docked_screen.dockedbackground.docked_image_holder.base_hardware_button.addEventListener(MouseEvent.MOUSE_UP,this.func_HardwareBaseButton_Click);
         this.main_docked_screen.dockedbackground.docked_image_holder.hangarButton.alpha = 0;
         this.main_docked_screen.dockedbackground.docked_image_holder.hangarButton.addEventListener(MouseEvent.MOUSE_UP,this.func_HangarButton_Click);
         this.starbaseexitposition();
         this.mysocket.send("PI`CLEAR`~");
         this.counterForBaseLives = 0;
         this.DeathMatchTimer = new Timer(1000);
         this.DeathMatchTimer.addEventListener(TimerEvent.TIMER,this.DeathMatchTimerHandler);
         this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.addEventListener(MouseEvent.MOUSE_DOWN,this.func_DeahtMatchInfoClick);
         if(this.teamdeathmatch == true)
         {
            this.DeathMatchTimer.start();
            if(!(this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] >= 0 && this.playershipstatus[5][2] < this.teambases.length))
            {
               this.sendnewdmgame();
            }
         }
         stop();
      }
      
      public function func_cloakplayership(param1:*, param2:*, param3:*) : *
      {
         this.playerSpecialsSettings.isCloaked = true;
         this.playerSpecialsSettings.CloakEnergy = param1;
         this.playerSpecialsSettings.CloakLocation = param3;
         this.playershipstatus[5][15] = "C" + param2;
         this.PlayersShipImage.alpha = 0.5;
         this.keywaspressed = true;
         this.func_StealthTheRadar();
      }
      
      public function func_getPlayersName(param1:*) : *
      {
         var _loc2_:* = 0;
         while(_loc2_ < this.currentonlineplayers.length)
         {
            if(param1 == this.currentonlineplayers[_loc2_][0])
            {
               return this.currentonlineplayers[_loc2_][1];
            }
            _loc2_++;
         }
         return "";
      }
      
      internal function frame73() : *
      {
         this.ShipHardwareScreen.guntype = this.guntype;
         this.ShipHardwareScreen.shieldgenerators = this.shieldgenerators;
         this.ShipHardwareScreen.missile = this.missile;
         this.ShipHardwareScreen.shiptype = this.shiptype;
         this.ShipHardwareScreen.energycapacitors = this.energycapacitors;
         this.ShipHardwareScreen.energygenerators = this.energygenerators;
         this.ShipHardwareScreen.specialshipitems = this.specialshipitems;
         this.ShipHardwareScreen.playershipstatus = this.playershipstatus;
         this.ShipHardwareScreen.exit_but.addEventListener(MouseEvent.MOUSE_DOWN,this.func_exitHardWareScreen);
      }
      
      public function func_RequestStats(param1:*) : *
      {
         var _loc2_:* = undefined;
         if(this.playershipstatus[3][0] != param1)
         {
            _loc2_ = "STATS`TGT`GET`" + this.playershipstatus[3][0] + "`" + param1 + "`~";
            this.mysocket.send(_loc2_);
         }
      }
      
      public function GameKeyListenerKeyPress(param1:KeyboardEvent) : void
      {
         var e:KeyboardEvent = param1;
         var OutComeReturn:* = true;
         var keypressed:* = e.keyCode;
         if(!this.isPlayerChatting)
         {
            if(keypressed == this.gamesetting.accelkey)
            {
               if(this.isupkeypressed != OutComeReturn)
               {
                  this.isupkeypressed = OutComeReturn;
                  this.keywaspressed = true;
               }
            }
            else if(keypressed == this.gamesetting.turnleftkey)
            {
               if(this.isleftkeypressed != OutComeReturn)
               {
                  this.isleftkeypressed = OutComeReturn;
                  this.keywaspressed = true;
               }
            }
            else if(keypressed == this.gamesetting.turnrightkey)
            {
               if(this.isrightkeypressed != OutComeReturn)
               {
                  this.isrightkeypressed = OutComeReturn;
                  this.keywaspressed = true;
               }
            }
            else if(keypressed == this.gamesetting.deaccelkey)
            {
               if(this.isdownkeypressed != OutComeReturn)
               {
                  this.isdownkeypressed = OutComeReturn;
                  this.keywaspressed = true;
               }
            }
            else if(keypressed == this.gamesetting.afterburnerskey)
            {
               if(this.isshiftkeypressed != OutComeReturn)
               {
                  this.isshiftkeypressed = OutComeReturn;
                  this.keywaspressed = true;
               }
            }
            else if(keypressed == this.gamesetting.gunskey)
            {
               this.iscontrolkeypressed = OutComeReturn;
            }
            else if(keypressed == this.gamesetting.missilekey)
            {
               this.isspacekeypressed = OutComeReturn;
            }
            else if(keypressed == this.gamesetting.dockkey)
            {
               this.isdkeypressed = OutComeReturn;
            }
            else if(Number(keypressed) >= 49 && Number(keypressed) <= 57)
            {
               try
               {
                  this.func_Special_Selected(Number(keypressed) - 48);
               }
               catch(error:Error)
               {
               }
            }
         }
      }
      
      public function func_InitializeClockChecks() : *
      {
         this.loginmovie.mov_login.logindisplay.currentstatus.text += "\r\r Synchronizing \r";
         addEventListener(Event.ENTER_FRAME,this.func_ClockSynchronizeScript);
      }
      
      public function deathsgamerewards() : *
      {
         return 0;
      }
      
      public function func_globalTimeStamp(param1:*) : *
      {
         var _loc2_:* = undefined;
         if(param1 < 1)
         {
            _loc2_ = String(getTimer() + this.clocktimediff);
         }
         else
         {
            _loc2_ = String(param1 + this.clocktimediff);
         }
         _loc2_ = Number(_loc2_.substr(Number(_loc2_.length) - 4));
         if(_loc2_ > 10000)
         {
            _loc2_ -= 10000;
         }
         return String(_loc2_);
      }
      
      public function othergunfiremovement(param1:*) : *
      {
         var playerwhoshothitid:* = undefined;
         var gunshothitplayertype:* = undefined;
         var raceOfHitter:* = undefined;
         var shielddamagedone:* = undefined;
         var structdamagedone:* = undefined;
         var piercingdamagedone:* = undefined;
         var zzj:* = undefined;
         var dataupdated:* = undefined;
         var TimeChangeRatio:* = param1;
         var curenttime:* = getTimer();
         var cc:* = 0;
         var playersx:* = this.shipcoordinatex;
         var playersy:* = this.shipcoordinatey;
         var halfplayerswidth:* = Math.round(this.PlayersShipImage.width / 2);
         var halfplayersheight:* = Math.round(this.PlayersShipImage.height / 2);
         cc = 0;
         while(cc < this.othergunfire.length)
         {
            this.othergunfire[cc][1] += Number(this.othergunfire[cc][3]) * TimeChangeRatio;
            this.othergunfire[cc][2] += Number(this.othergunfire[cc][4]) * TimeChangeRatio;
            try
            {
               this.othergunfire[cc][16].x = Number(this.othergunfire[cc][1]) - this.shipcoordinatex;
               this.othergunfire[cc][16].y = Number(this.othergunfire[cc][2]) - this.shipcoordinatey;
            }
            catch(error:Error)
            {
               trace("Error moving other player gunfire: " + error);
            }
            if(this.othergunfire[cc][5] < curenttime)
            {
               try
               {
                  this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
               }
               catch(error:Error)
               {
                  trace("Error removing otherplayershot - timeout: " + error);
               }
               this.othergunfire.splice(cc,1);
               cc--;
            }
            else if(this.myLineHittest(this.shipcoordinatex,this.shipcoordinatey,Math.round(this.PlayersShipImage.width / 2) + this.othergunfire[cc][11],this.othergunfire[cc][18],this.othergunfire[cc][19],this.othergunfire[cc][1],this.othergunfire[cc][2]))
            {
               try
               {
                  this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
               }
               catch(error:Error)
               {
                  trace("Error removing otherplayershot - playerhit: " + error);
               }
               playerwhoshothitid = this.othergunfire[cc][0];
               gunshothitplayertype = this.othergunfire[cc][7];
               shielddamagedone = this.guntype[gunshothitplayertype][4];
               structdamagedone = this.guntype[gunshothitplayertype][7];
               piercingdamagedone = this.guntype[gunshothitplayertype][8];
               if(!this.playershipstatus[5][20])
               {
                  this.playershipstatus[2][1] -= shielddamagedone;
                  if(piercingdamagedone > 0)
                  {
                     this.playershipstatus[2][5] -= piercingdamagedone;
                  }
               }
               if(this.playershipstatus[2][1] < 0)
               {
                  this.playershipstatus[2][5] -= structdamagedone;
                  this.playershipstatus[2][1] = 0;
               }
               this.func_playerGotHit();
               this.gunShotBufferData += "GH" + "`" + this.playershipstatus[3][0] + "`";
               this.gunShotBufferData += this.othergunfire[cc][0] + "`";
               this.gunShotBufferData += this.othergunfire[cc][9] + "`SH`" + Math.round(this.playershipstatus[2][1]);
               this.gunShotBufferData += "`ST`" + Math.round(this.playershipstatus[2][5]) + "~";
               this.othergunfire.splice(cc,1);
               if(this.playershipstatus[2][5] <= 0 && this.playershipstatus[5][4] == "alive")
               {
                  this.playershipstatus[5][4] = "dead";
                  this.playershipstatus[5][5] = playerwhoshothitid;
                  zzj = 0;
                  dataupdated = false;
                  while(zzj < this.currentonlineplayers.length && dataupdated == false)
                  {
                     if(playerwhoshothitid == this.currentonlineplayers[zzj][0])
                     {
                        dataupdated = true;
                        this.playershipstatus[5][7] = this.currentonlineplayers[zzj][1];
                     }
                     zzj++;
                  }
                  this.playershipstatus[2][5] = 0;
                  gotoAndPlay("playerdeath");
               }
               cc--;
            }
            else
            {
               this.othergunfire[cc][18] = this.othergunfire[cc][1];
               this.othergunfire[cc][19] = this.othergunfire[cc][2];
            }
            cc++;
         }
      }
      
      public function func_selectShipTarget() : *
      {
         var _loc1_:* = undefined;
         var _loc2_:* = undefined;
         _loc1_ = 0;
         _loc2_ = -1;
         if(this.targetinfo[4] != this.playershipstatus[3][0])
         {
            if(this.targetinfo[4] < 4000)
            {
               _loc1_ = 0;
               while(_loc1_ < this.otherplayership.length)
               {
                  if(this.otherplayership[_loc1_][0] == this.targetinfo[4])
                  {
                     this.targetinfo[5] = _loc1_;
                     _loc2_ = _loc1_;
                     break;
                  }
                  _loc1_++;
               }
            }
         }
         if(_loc2_ == -1)
         {
            _loc1_ = 0;
            while(_loc1_ < this.otherplayership.length)
            {
               if(this.otherplayership[_loc1_][0] != this.playershipstatus[3][0])
               {
                  if(this.otherplayership[_loc1_][56] != "friend")
                  {
                     this.targetinfo[4] = this.otherplayership[_loc1_][0];
                     this.targetinfo[5] = _loc1_;
                     break;
                  }
               }
               _loc1_++;
            }
         }
         else if(this.targetinfo[4] < 4000)
         {
            _loc1_ = this.targetinfo[5] + 1;
            while(_loc1_ != this.targetinfo[5])
            {
               if(_loc1_ >= this.otherplayership.length)
               {
                  _loc1_ = 0;
               }
               if(this.otherplayership[_loc1_][0] != this.playershipstatus[3][0])
               {
                  if(this.otherplayership[_loc1_][56] != "friend")
                  {
                     this.targetinfo[4] = this.otherplayership[_loc1_][0];
                     this.targetinfo[5] = _loc1_;
                     break;
                  }
               }
               _loc1_++;
            }
         }
      }
      
      public function DisplayOnlinePlayersListDetails() : *
      {
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         var _loc1_:* = "";
         var _loc2_:* = 0;
         var _loc3_:* = new Array();
         var _loc4_:* = 0;
         while(_loc4_ < this.currentonlineplayers.length)
         {
            if(this.currentonlineplayers[_loc4_][0] >= -999999)
            {
               _loc8_ = -1;
               if(isNaN(Number(this.currentonlineplayers[_loc4_][4])) || Number(this.currentonlineplayers[_loc4_][4] == -1))
               {
                  _loc8_ = -1;
               }
               else
               {
                  _loc8_ = Number(this.currentonlineplayers[_loc4_][4]);
               }
               _loc9_ = false;
               _loc10_ = 0;
               while(_loc10_ < _loc3_.length)
               {
                  if(_loc3_[_loc10_][0] == _loc8_)
                  {
                     _loc11_ = _loc3_[_loc10_][1].length;
                     _loc3_[_loc10_][1][_loc11_] = new Array();
                     _loc3_[_loc10_][1][_loc11_][0] = this.currentonlineplayers[_loc4_][1];
                     _loc3_[_loc10_][1][_loc11_][1] = this.currentonlineplayers[_loc4_][5];
                     _loc3_[_loc10_][1][_loc11_][2] = this.currentonlineplayers[_loc4_][6];
                     _loc3_[_loc10_][1][_loc11_][3] = this.currentonlineplayers[_loc4_][7];
                     _loc9_ = true;
                     break;
                  }
                  _loc10_++;
               }
               if(_loc9_ == false)
               {
                  _loc12_ = _loc3_.length;
                  _loc3_[_loc12_] = new Array();
                  _loc3_[_loc12_][0] = _loc8_;
                  _loc3_[_loc12_][1] = new Array();
                  _loc3_[_loc12_][1][0] = new Array();
                  _loc3_[_loc12_][1][0][0] = this.currentonlineplayers[_loc4_][1];
                  _loc3_[_loc12_][1][0][1] = this.currentonlineplayers[_loc4_][5];
                  _loc3_[_loc12_][1][0][2] = this.currentonlineplayers[_loc4_][6];
                  _loc3_[_loc12_][1][0][3] = this.currentonlineplayers[_loc4_][7];
               }
            }
            _loc4_++;
         }
         var _loc5_:* = 1;
         while(_loc5_ < _loc3_.length)
         {
            if(_loc3_[_loc5_][0] < _loc3_[_loc5_ - 1][0])
            {
               _loc13_ = _loc3_[_loc5_];
               _loc3_[_loc5_] = _loc3_[_loc5_ - 1];
               _loc3_[_loc5_ - 1] = _loc13_;
               _loc5_ = 0;
            }
            _loc5_++;
         }
         var _loc6_:* = "";
         var _loc7_:* = 0;
         while(_loc7_ < _loc3_.length)
         {
            if(_loc3_[_loc7_][0] < 0)
            {
               _loc6_ += "Teamless players\r";
            }
            else
            {
               _loc6_ += "Team: " + _loc3_[_loc7_][0] + "\r";
            }
            _loc14_ = 0;
            while(_loc14_ < _loc3_[_loc7_][1].length)
            {
               _loc6_ += _loc3_[_loc7_][1][_loc14_][0] + " (" + Math.floor(Number(_loc3_[_loc7_][1][_loc14_][1]) / this.scoreratiomodifier) + ") K:" + _loc3_[_loc7_][1][_loc14_][2] + " D:" + _loc3_[_loc7_][1][_loc14_][3] + "\r";
               _loc14_++;
            }
            _loc6_ += "\r";
            _loc7_++;
         }
         this.OnlinePLayerList.onlineListDetailsone.text = _loc6_;
      }
      
      public function displaystats() : *
      {
         var _loc1_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.visible = true;
         if(this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] >= 0 && this.playershipstatus[5][2] < this.teambases.length)
         {
            this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.textinfo.text = "CHANGE TEAM";
         }
         else
         {
            this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.textinfo.text = "GO ON TEAM";
         }
         _loc1_ = "";
         var _loc2_:* = "";
         _loc1_ += "Teams: " + this.teambases.length + "\r";
         _loc3_ = new Array();
         _loc4_ = new Array();
         _loc5_ = 0;
         while(_loc5_ < this.teambases.length)
         {
            _loc3_[_loc5_] = 0;
            _loc4_[_loc5_] = 0;
            _loc5_++;
         }
         _loc5_ = 0;
         while(_loc5_ < this.currentonlineplayers.length)
         {
            if(this.currentonlineplayers[_loc5_][4] != "N/A" && this.currentonlineplayers[_loc5_][4] >= 0 && this.currentonlineplayers[_loc5_][4] < this.teambases.length)
            {
               if(this.currentonlineplayers[_loc5_][0] >= 0)
               {
                  _loc3_[this.currentonlineplayers[_loc5_][4]] += 1;
               }
               else
               {
                  _loc4_[this.currentonlineplayers[_loc5_][4]] += 1;
               }
            }
            _loc5_++;
         }
         if(this.squadwarinfo[0] == true)
         {
            this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.visible = false;
            _loc1_ += "Team / Squad Size / Base Structure\r";
            _loc5_ = 0;
            while(_loc5_ < this.teambases.length)
            {
               _loc1_ += " " + this.teambases[_loc5_][0].substr(2) + "  /  " + this.squadwarinfo[1][_loc5_] + ": " + _loc3_[_loc5_] + "  /  " + this.teambases[_loc5_][10] + "\r";
               _loc5_++;
            }
            if(this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] >= 0)
            {
               this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.yourteam.text = "Your Team: " + this.teambases[this.playershipstatus[5][2]][0].substr(2) + "  /  " + this.squadwarinfo[1][this.playershipstatus[5][2]] + "\r";
            }
            else
            {
               this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.yourteam.text = "Your Team: N/A";
            }
         }
         else
         {
            this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.visible = true;
            _loc1_ += "Team Size / Base Structure\r";
            _loc5_ = 0;
            while(_loc5_ < this.teambases.length)
            {
               _loc1_ += " " + this.teambases[_loc5_][0].substr(2) + ": " + _loc3_[_loc5_] + " (" + _loc4_[_loc5_] + " ai)  " + this.teambases[_loc5_][10] + "\r";
               _loc5_++;
            }
            if(this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] >= 0 && this.playershipstatus[5][2] < this.teambases.length)
            {
               this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.yourteam.text = "Your Team: " + this.teambases[this.playershipstatus[5][2]][0].substr(2) + "\r";
            }
            else
            {
               this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.yourteam.text = "Your Team: N/A";
            }
         }
         this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.information.text = _loc1_;
      }
      
      public function func_SeekTheOtherPlayersMissile(param1:*) : *
      {
         var _loc10_:* = undefined;
         var _loc2_:* = false;
         var _loc3_:* = this.shipcoordinatex;
         var _loc4_:* = this.shipcoordinatey;
         var _loc5_:* = this.othermissilefire[param1][1];
         var _loc6_:* = this.othermissilefire[param1][2];
         if(_loc2_)
         {
            _loc3_ = 0;
            _loc4_ = 0;
         }
         else
         {
            _loc3_ = this.shipcoordinatex;
            _loc4_ = this.shipcoordinatey;
         }
         var _loc7_:* = Math.round(Math.sqrt((_loc3_ - Number(this.xposition)) * (_loc3_ - Number(this.xposition)) + (_loc4_ - Number(this.yposition)) * (_loc4_ - Number(this.yposition))));
         var _loc8_:* = (_loc8_ = Math.atan2(_loc3_ - _loc5_,_loc6_ - _loc4_) * (180 / Math.PI)) - Number(this.othermissilefire[param1][6]);
         trace(_loc8_);
         var _loc9_:* = "";
         if(_loc8_ > 360)
         {
            _loc8_ -= 360;
         }
         if(_loc8_ < 0)
         {
            _loc8_ += 360;
         }
         if(_loc8_ < 0)
         {
            _loc8_ += 360;
         }
         if(_loc8_ < 10 || _loc8_ >= 350)
         {
            if(_loc7_ < 30)
            {
            }
         }
         else if(_loc8_ <= 180)
         {
            _loc9_ += "R";
         }
         else
         {
            _loc9_ += "L";
         }
         if(_loc9_ != this.othermissilefire[param1][15])
         {
            this.othermissilefire[param1][15] = _loc9_;
            _loc10_ = "MI" + "`" + this.othermissilefire[param1][0] + "`" + Math.round(_loc5_) + "`" + Math.round(_loc6_) + "`" + Math.round(this.othermissilefire[param1][8]) + "`" + Math.round(this.othermissilefire[param1][6]) + "`" + "`" + this.othermissilefire[param1][9] + "`" + this.othermissilefire[param1][15] + "~";
            this.gunShotBufferData += _loc10_;
         }
      }
      
      public function func_InGameHelp_Click(param1:MouseEvent) : void
      {
         this.func_TriggerGameHelp();
      }
      
      public function func_funcDeathMatchProcc(param1:*) : *
      {
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc2_:* = 0;
         if(param1[1] == "BL")
         {
            _loc2_ = 2;
            while(_loc2_ < param1.length)
            {
               this.teambases[_loc2_ - 2][10] = Number(param1[_loc2_]);
               if(this.teambases[_loc2_ - 2][10] < 1)
               {
                  this.teambases[_loc2_ - 2][10] = 1;
               }
               this.func_updateTameBaseHealthBars(_loc2_ - 2);
               _loc2_++;
            }
         }
         else if(param1[1] == "BH")
         {
            this.teambases[Number(param1[2])][10] = Number(param1[3]);
            this.func_updateTameBaseHealthBars(Number(param1[2]));
         }
         else if(param1[1] == "NG")
         {
            _loc2_ = 2;
            while(_loc2_ < param1.length)
            {
               this.teambases[_loc2_ - 2][10] = Number(param1[_loc2_]);
               this.teambases[_loc2_ - 2][11] = this.teambasetypes[this.teambases[_loc2_ - 2][3]][2];
               if(this.teambases[_loc2_ - 2][10] < 1)
               {
                  this.teambases[_loc2_ - 2][10] = 1;
               }
               _loc2_++;
            }
         }
         else if(param1[1] == "EG")
         {
            if(this.teamdeathmatch == true)
            {
               _loc3_ = Number(param1[3]);
               _loc4_ = Number(param1[2]);
               _loc5_ = this.teambases[_loc4_][0].substr(2);
               if(this.squadwarinfo[0] == true)
               {
                  _loc5_ = this.squadwarinfo[1][_loc4_];
               }
               this.teambases[_loc4_][10] = 0;
               _loc6_ = "None";
               _loc7_ = "No One";
               _loc8_ = "None";
               _loc2_ = 0;
               while(_loc2_ < this.currentonlineplayers.length)
               {
                  if(this.currentonlineplayers[_loc2_][0] == _loc3_)
                  {
                     _loc6_ = Number(this.currentonlineplayers[_loc2_][4]);
                     _loc7_ = this.currentonlineplayers[_loc2_][1];
                     _loc8_ = this.teambases[_loc6_][0].substr(2);
                     if(this.squadwarinfo[0] == true)
                     {
                        _loc8_ = this.squadwarinfo[1][_loc6_];
                     }
                  }
                  _loc2_++;
               }
               _loc9_ = int(param1[4]);
               _loc10_ = int(param1[5]);
               _loc11_ = "";
               _loc11_ = "HOST: Player " + _loc7_ + " of Team " + _loc8_ + " has destroyed " + _loc5_ + "\'s Base";
               this.func_enterintochat(_loc11_,this.systemchattextcolor);
               _loc11_ = "HOST: Reward for Team " + _loc8_ + " is " + _loc9_ + " Funds and " + _loc10_ / this.scoreratiomodifier + " Score";
               this.func_enterintochat(_loc11_,this.systemchattextcolor);
               if(this.playershipstatus[5][2] == _loc6_)
               {
                  this.playershipstatus[5][9] += Number(_loc10_);
                  this.playershipstatus[3][1] += Number(_loc9_);
               }
               trace("1");
               _loc2_ = 0;
               while(_loc2_ < this.currentonlineplayers.length)
               {
                  if(this.currentonlineplayers[_loc2_][0] >= 0)
                  {
                     if(this.currentonlineplayers[_loc2_][4] == _loc6_)
                     {
                        this.currentonlineplayers[_loc2_][5] += Number(_loc10_);
                     }
                  }
                  _loc2_++;
               }
               trace("2");
               this.playershipstatus[5][2] = "N/A";
               _loc2_ = 0;
               while(_loc2_ < this.currentonlineplayers.length)
               {
                  if(this.currentonlineplayers[_loc2_][0] >= 0)
                  {
                     this.currentonlineplayers[_loc2_][4] = "N/A";
                  }
                  _loc2_++;
               }
               trace("3");
               if(this.squadwarinfo[0] == true)
               {
                  this.squadwarinfo[0] = false;
                  this.squadwarinfo[2] = false;
               }
               trace("4");
               if(this.playershipstatus[5][4] != "docking")
               {
                  gotoAndStop("dockedscreen");
               }
            }
         }
      }
      
      public function func_updateSpecialsDisplay() : *
      {
         var _loc1_:* = undefined;
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         _loc1_ = getTimer();
         _loc2_ = 1;
         while(_loc2_ < this.playershipstatus[11][1].length + 1)
         {
            _loc3_ = _loc2_ - 1;
            if(this.playershipstatus[11][2][_loc3_][0] != -1)
            {
               if(this.playershipstatus[11][2][_loc3_][0] == 0)
               {
                  this.specialsingame["sp" + _loc2_].specialinfodata.text = "";
               }
               else if(this.playershipstatus[11][2][_loc3_][0] > _loc1_)
               {
                  this.specialsingame["sp" + _loc2_].specialinfodata.text = Math.ceil((Number(this.playershipstatus[11][2][_loc3_][0]) - _loc1_) / 1000);
               }
               else
               {
                  if((_loc4_ = this.specialshipitems[this.playershipstatus[11][1][_loc3_][0]][5]) == "FLARE" || _loc4_ == "RECHARGESHIELD" || _loc4_ == "RECHARGESTRUCT" || _loc4_ == "MINES" || _loc4_ == "CLOAK" || _loc4_ == "STEALTH")
                  {
                     this.specialsingame["sp" + _loc2_].specialinfodata.text = "";
                     this.specialsingame["sp" + _loc2_].specialbutton.gotoAndStop("OFF");
                     this.playershipstatus[11][2][_loc3_][0] = 0;
                  }
                  if(_loc4_ == "DETECTOR")
                  {
                     if(this.specialsingame["sp" + _loc2_].specialbutton.setting == "RELOAD")
                     {
                        this.playershipstatus[11][2][_loc3_][0] = 0;
                        this.specialsingame["sp" + _loc2_].specialinfodata.text = "";
                        this.specialsingame["sp" + _loc2_].specialbutton.gotoAndStop("OFF");
                     }
                     else
                     {
                        this.playershipstatus[11][2][_loc3_][0] = this.specialshipitems[this.playershipstatus[11][1][_loc3_][0]][11] + _loc1_;
                        this.specialsingame["sp" + _loc2_].specialinfodata.text = "SCAN";
                        this.func_detectorPing(this.specialshipitems[this.playershipstatus[11][1][_loc3_][0]][10]);
                     }
                  }
               }
            }
            _loc2_++;
         }
      }
      
      public function func_fireTurretsAt(param1:*, param2:*) : *
      {
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         _loc3_ = "";
         _loc4_ = 0;
         while(_loc4_ < this.playershipstatus[8].length)
         {
            if(this.playershipstatus[8][_loc4_][1] <= param2 && this.playershipstatus[1][1] > this.playershipstatus[8][_loc4_][5] && this.playershipstatus[8][_loc4_][0] != "none" && this.playershipstatus[8][_loc4_][4] == "ON")
            {
               if(this.playershipstatus[5][15] == "C")
               {
               }
               this.playershipstatus[1][1] -= this.playershipstatus[8][_loc4_][5];
               _loc5_ = this.playershipstatus[8][_loc4_][0];
               _loc6_ = this.playershipstatus[8][_loc4_][7];
               _loc7_ = this.PlayersShipImage.rotation;
               _loc8_ = _loc6_;
               _loc7_ = param1;
               _loc10_ = (_loc9_ = this.MovingObjectWithThrust(_loc7_,_loc8_))[0];
               _loc11_ = _loc9_[1];
               _loc12_ = this.playershipstatus[8][_loc4_][2];
               _loc13_ = this.playershipstatus[8][_loc4_][3];
               _loc12_ = (_loc14_ = this.firingbulletstartlocation(_loc12_,_loc13_,this.PlayersShipImage.rotation))[0] + this.shipcoordinatex;
               _loc13_ = _loc14_[1] + this.shipcoordinatey;
               this.playershipstatus[8][_loc4_][1] = getTimer() + Number(this.guntype[_loc5_][2]) * 1000;
               _loc3_ += this.func_fire_a_gunshot(_loc5_,_loc12_,_loc13_,_loc10_,_loc11_,_loc7_,_loc8_,param2);
            }
            _loc4_++;
         }
         this.gunShotBufferData += _loc3_;
      }
      
      public function ReceivedData(param1:*) : void
      {
         this.dataprocess(param1.data);
      }
      
      public function func_runFireScript(param1:*) : *
      {
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc2_:* = "";
         var _loc3_:* = 0;
         while(_loc3_ < this.playershipstatus[0].length)
         {
            if(this.playershipstatus[0][_loc3_][1] <= param1 && this.playershipstatus[1][1] > this.playershipstatus[0][_loc3_][5] && this.playershipstatus[0][_loc3_][0] != "none" && this.playershipstatus[0][_loc3_][4] == "ON")
            {
               if(this.playerSpecialsSettings.isCloaked)
               {
                  this.func_turnofshipcloak();
               }
               this.playershipstatus[1][1] -= this.playershipstatus[0][_loc3_][5];
               _loc4_ = this.playershipstatus[0][_loc3_][0];
               _loc5_ = this.playershipstatus[0][_loc3_][7];
               _loc6_ = this.PlayersShipImage.rotation;
               _loc7_ = this.playershipvelocity + _loc5_;
               _loc9_ = (_loc8_ = this.MovingObjectWithThrust(_loc6_,_loc7_))[0];
               _loc10_ = _loc8_[1];
               _loc11_ = this.playershipstatus[0][_loc3_][2];
               _loc12_ = this.playershipstatus[0][_loc3_][3];
               _loc11_ = (_loc13_ = this.firingbulletstartlocation(_loc11_,_loc12_,_loc6_))[0] + this.shipcoordinatex;
               _loc12_ = _loc13_[1] + this.shipcoordinatey;
               this.playershipstatus[0][_loc3_][1] = getTimer() + Number(this.guntype[_loc4_][2]) * 1000;
               _loc2_ += this.func_fire_a_gunshot(_loc4_,_loc11_,_loc12_,_loc9_,_loc10_,_loc6_,_loc7_,param1);
            }
            _loc3_++;
         }
         this.gunShotBufferData += _loc2_;
      }
      
      public function func_NormalRadar() : *
      {
         this.gameRadar.gotoAndStop(1);
      }
      
      public function func_trytoDockTheShip() : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc8_:* = undefined;
         var _loc1_:* = null;
         var _loc7_:* = 0;
         while(_loc7_ < this.starbaselocation.length)
         {
            if(this.starbaselocation[_loc7_][5] == "ACTIVE" || Number(this.starbaselocation[_loc7_][5]) < getTimer())
            {
               _loc2_ = this.starbaselocation[_loc7_][1];
               _loc3_ = this.starbaselocation[_loc7_][2];
               _loc4_ = this.starbaselocation[_loc7_][4];
               _loc5_ = _loc2_ - this.shipcoordinatex;
               _loc6_ = _loc3_ - this.shipcoordinatey;
               if(_loc5_ * _loc5_ + _loc6_ * _loc6_ <= _loc4_ * _loc4_)
               {
                  if("SB" != this.starbaselocation[_loc7_][0].substr(0,2))
                  {
                     if("PL" == this.starbaselocation[_loc7_][0].substr(0,2))
                     {
                     }
                  }
                  if(this.playershipvelocity <= this.maxdockingvelocity)
                  {
                     if(this.starbaselocation[_loc7_][9] <= getTimer())
                     {
                        this.otherplayerdockedon = null;
                        this.playershipstatus[4][0] = this.starbaselocation[_loc7_][0];
                        gotoAndStop("dockedscreen");
                     }
                  }
               }
            }
            _loc7_++;
         }
         if(this.teamdeathmatch == true)
         {
            _loc8_ = 0;
            while(_loc8_ < this.teambases.length)
            {
               _loc2_ = this.teambases[_loc8_][1];
               _loc3_ = this.teambases[_loc8_][2];
               _loc4_ = this.teambases[_loc8_][4];
               _loc5_ = Number(_loc2_) - Number(this.shipcoordinatex);
               _loc6_ = Number(_loc3_) - Number(this.shipcoordinatey);
               if(Math.sqrt(_loc5_ * _loc5_ + _loc6_ * _loc6_) <= _loc4_)
               {
                  if(_loc8_ == this.playershipstatus[5][2])
                  {
                     if(this.playershipvelocity <= this.maxdockingvelocity)
                     {
                        gotoAndStop("dockedscreen");
                     }
                  }
                  break;
               }
               _loc8_++;
            }
         }
      }
      
      public function setMouseIsUp(param1:MouseEvent) : void
      {
         this.TurretMouseDown = false;
      }
      
      internal function frame88() : *
      {
         this.hangarWindow.playerscurrentextrashipno = this.playerscurrentextrashipno;
         this.hangarWindow.extraplayerships = this.extraplayerships;
         this.hangarWindow.guntype = this.guntype;
         this.hangarWindow.shieldgenerators = this.shieldgenerators;
         this.hangarWindow.missile = this.missile;
         this.hangarWindow.shiptype = this.shiptype;
         this.hangarWindow.energycapacitors = this.energycapacitors;
         this.hangarWindow.energygenerators = this.energygenerators;
         this.hangarWindow.specialshipitems = this.specialshipitems;
         this.hangarWindow.playershipstatus = this.playershipstatus;
         this.OnlinePLayerList.visible = false;
         this.hangarWindow.exit_button.addEventListener(MouseEvent.MOUSE_DOWN,this.func_exitHangarScreen);
      }
      
      public function func_RunOtherSips(param1:*) : *
      {
         var _loc2_:* = getTimer();
         this.ProcessOtherShipKeys(_loc2_);
         this.func_moveAnothership(param1);
      }
      
      public function func_InitializeGameMap() : *
      {
         this.gameMap.sectorinformation = this.sectorinformation;
         this.gameMap.sectormapitems = this.sectormapitems;
         this.gameMap.starbaselocation = this.starbaselocation;
         this.gameMap.func_initMap();
      }
      
      public function func_MoveBackGroundStars() : *
      {
         var _loc1_:* = undefined;
         _loc1_ = 0;
         while(_loc1_ < this.gamesetting.totalstars)
         {
            this.backgroundstar[_loc1_][0] += -this.shipXmovement / Number(this.backgroundstar[_loc1_][3]);
            this.backgroundstar[_loc1_][1] += -this.shipYmovement / Number(this.backgroundstar[_loc1_][3]);
            this.backgroundstar[_loc1_][2].rotation = this.PlayersShipImage.rotation;
            if(this.playerShipSpeedRatio > 0.15)
            {
               this.backgroundstar[_loc1_][2].scaleY = 1 + this.playerShipSpeedRatio * 35 / Number(this.backgroundstar[_loc1_][3]);
            }
            else
            {
               this.backgroundstar[_loc1_][2].scaleY = 1;
            }
            if(this.backgroundstar[_loc1_][0] < -500)
            {
               this.backgroundstar[_loc1_][0] = 500;
               this.backgroundstar[_loc1_][1] = Math.random() * 600 - 300;
            }
            if(this.backgroundstar[_loc1_][0] > 500)
            {
               this.backgroundstar[_loc1_][0] = -500;
               this.backgroundstar[_loc1_][1] = Math.random() * 600 - 300;
            }
            if(this.backgroundstar[_loc1_][1] > 300)
            {
               this.backgroundstar[_loc1_][0] = Math.random() * 800 - 400;
               this.backgroundstar[_loc1_][1] = -300;
            }
            if(this.backgroundstar[_loc1_][1] < -300)
            {
               this.backgroundstar[_loc1_][0] = Math.random() * 800 - 400;
               this.backgroundstar[_loc1_][1] = 300;
            }
            this.backgroundstar[_loc1_][2].x = this.backgroundstar[_loc1_][0];
            this.backgroundstar[_loc1_][2].y = this.backgroundstar[_loc1_][1];
            _loc1_++;
         }
      }
      
      public function LogIntoZone() : *
      {
         var _loc1_:* = undefined;
         this.currentonlineplayers = new Array();
         _loc1_ = "ZONELOGIN`" + this.playershipstatus[3][0] + "`" + this.playershipstatus[3][2] + "`" + this.playershipstatus[5][0] + "`" + this.playershipstatus[5][3] + "`" + this.playershipstatus[5][2] + "`" + this.playershipstatus[5][9] + "`" + this.playershipstatus[5][10] + "`" + this.playershipstatus[3][3] + "~";
         this.mysocket.send(_loc1_);
      }
      
      public function func_setToNewDefaultGameKeys() : *
      {
         this.gamesetting.accelkey = this.gamesetting.newdefault.accelkey;
         this.gamesetting.deaccelkey = this.gamesetting.newdefault.deaccelkey;
         this.gamesetting.turnleftkey = this.gamesetting.newdefault.turnleftkey;
         this.gamesetting.turnrightkey = this.gamesetting.newdefault.turnrightkey;
         this.gamesetting.missilekey = this.gamesetting.newdefault.missilekey;
         this.gamesetting.afterburnerskey = this.gamesetting.newdefault.afterburnerskey;
         this.gamesetting.dockkey = this.gamesetting.newdefault.dockkey;
         this.gamesetting.gunskey = this.gamesetting.newdefault.gunskey;
         this.gamesetting.targeterkey = this.gamesetting.newdefault.targeterkey;
      }
      
      public function fund_OnlineListToggled() : *
      {
         if(this.currentonlineplayers.length > 0)
         {
            this.OnlinePLayerList.playershipstatus = this.playershipstatus;
            this.OnlinePLayerList.mysocket = this.mysocket;
            trace(this.OnlinePLayerList.currentFrame);
            if(this.OnlinePLayerList.currentFrame == 1)
            {
               this.OnlinePLayerList.gotoAndStop(2);
               this.OnlinePLayerList.minimizeButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_OnlinePlayersMinimize_Click);
               if(this.playershipstatus[5][4] == "alive" || this.teamdeathmatch)
               {
                  this.OnlinePLayerList.changeWindow.gotoAndStop(2);
               }
               else
               {
                  this.OnlinePLayerList.changeWindow.gotoAndStop(1);
               }
            }
            else if(this.OnlinePLayerList.currentFrame == 2)
            {
               this.OnlinePLayerList.gotoAndStop(1);
            }
         }
         this.func_refreshCurrentOnlineList();
      }
      
      public function func_setToOldGameKeys() : *
      {
         this.gamesetting.accelkey = this.gamesetting.olddefault.accelkey;
         this.gamesetting.deaccelkey = this.gamesetting.olddefault.deaccelkey;
         this.gamesetting.turnleftkey = this.gamesetting.olddefault.turnleftkey;
         this.gamesetting.turnrightkey = this.gamesetting.olddefault.turnrightkey;
         this.gamesetting.missilekey = this.gamesetting.olddefault.missilekey;
         this.gamesetting.afterburnerskey = this.gamesetting.olddefault.afterburnerskey;
         this.gamesetting.dockkey = this.gamesetting.olddefault.dockkey;
         this.gamesetting.gunskey = this.gamesetting.olddefault.gunskey;
         this.gamesetting.targeterkey = this.gamesetting.olddefault.targeterkey;
      }
      
      public function func_updateTargetDisplay() : *
      {
         var _loc1_:* = undefined;
         var _loc4_:* = undefined;
         _loc1_ = 0;
         var _loc2_:* = 0;
         var _loc3_:* = 0;
         if(this.targetinfo[4] != this.playershipstatus[3][0])
         {
            if(this.targetinfo[4] < 4000)
            {
               _loc4_ = this.targetinfo[4];
               this.targetinfo[4] = this.playershipstatus[3][0];
               _loc1_ = 0;
               while(_loc1_ < this.otherplayership.length)
               {
                  if(this.otherplayership[_loc1_][0] == _loc4_)
                  {
                     this.targetinfo[5] = _loc1_;
                     this.targetinfo[4] = _loc4_;
                     break;
                  }
                  _loc1_++;
               }
               if(this.targetinfo[4] != this.playershipstatus[3][0])
               {
                  this.fun_showStats();
               }
               else
               {
                  this.func_resetTarget();
               }
            }
         }
      }
      
      public function func_IncomingSeekerInfo(param1:*) : *
      {
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         trace(param1);
         var _loc2_:* = Number(param1[1]);
         var _loc3_:* = Number(param1[7]);
         var _loc4_:* = Number(param1[2]);
         var _loc5_:* = Number(param1[3]);
         var _loc6_:* = param1[8];
         var _loc7_:* = Number(param1[4]);
         var _loc8_:* = Number(param1[5]);
         if(Number(this.playershipstatus[3][0]) == Number(_loc2_))
         {
            _loc9_ = 0;
            while(_loc9_ < this.playershotsfired)
            {
               if(this.playershotsfired[_loc9_][0] == _loc3_)
               {
                  break;
               }
               _loc9_++;
            }
         }
         else
         {
            _loc10_ = 0;
            while(_loc10_ < this.othermissilefire.length)
            {
               if(this.othermissilefire[_loc10_][9] == _loc3_)
               {
                  if(_loc2_ == this.othermissilefire[_loc10_][0])
                  {
                     this.othermissilefire[_loc10_][15] = _loc6_;
                     break;
                  }
               }
               _loc10_++;
            }
         }
      }
      
      public function func_resetFlightKeys() : *
      {
         this.isupkeypressed = false;
         this.isdownkeypressed = false;
         this.isleftkeypressed = false;
         this.isrightkeypressed = false;
         this.iscontrolkeypressed = false;
         this.isdkeypressed = false;
         this.isshiftkeypressed = false;
         this.isspacekeypressed = false;
         this.afterburnerinuse = false;
         this.spacekeyjustpressed = false;
         this.iscontrolkeypressed = false;
         this.keywaspressed = true;
      }
      
      public function badwordfiltering(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         _loc2_ = String(param1.toLowerCase());
         _loc3_ = 0;
         while(_loc3_ < param1.length)
         {
            _loc4_ = 0;
            while(_loc4_ < this.badwords.length)
            {
               if(_loc2_.substr(_loc3_,this.badwords[_loc4_][0].length) == this.badwords[_loc4_][0])
               {
                  _loc5_ = param1.substr(0,_loc3_);
                  _loc6_ = param1.substr(_loc3_ + this.badwords[_loc4_][0].length);
                  param1 = _loc5_ + this.badwords[_loc4_][1] + _loc6_;
                  break;
               }
               _loc4_++;
            }
            _loc3_++;
         }
         return param1;
      }
      
      public function func_sendsectitemstatuscheck(param1:*) : *
      {
         if(!isNaN(param1))
         {
            this.datatosend = "STATS`TGT`SCT`" + param1 + "~";
            this.mysocket.send(this.datatosend);
         }
      }
      
      public function chatfocusInHandler(param1:FocusEvent) : void
      {
         this.isPlayerChatting = true;
         this.chatDisplay.textoutline.gotoAndStop(2);
         this.func_resetFlightKeys();
      }
      
      public function func_ignorelist(param1:*) : *
      {
         var _loc4_:* = undefined;
         var _loc2_:* = true;
         var _loc3_:* = 0;
         while(_loc3_ < this.gamechatinfo[6].length)
         {
            if(param1 == this.gamechatinfo[6][_loc3_])
            {
               _loc2_ = false;
               this.gamechatinfo[6].splice(_loc3_,1);
               this.func_enterintochat(" Removing From Ignore List: " + param1,this.systemchattextcolor);
               break;
            }
            _loc3_++;
         }
         if(_loc2_ == true)
         {
            _loc4_ = this.gamechatinfo[6].length;
            this.gamechatinfo[6][_loc4_] = param1;
            this.func_enterintochat(" Adding To Ignore List: " + param1,this.systemchattextcolor);
         }
      }
      
      public function func_addgunintocliet(param1:*) : *
      {
         var _loc2_:* = int(param1[1]);
         this.guntype[_loc2_] = new Array();
         this.guntype[_loc2_][0] = Number(param1[2]);
         trace("Gun:" + _loc2_ + "`" + param1[2]);
         this.guntype[_loc2_][1] = Number(param1[3]) / 1000;
         this.guntype[_loc2_][2] = Number(param1[4]) / 1000;
         this.guntype[_loc2_][3] = Number(param1[5]);
         this.guntype[_loc2_][4] = Number(param1[6]);
         this.guntype[_loc2_][5] = Number(param1[7]);
         this.guntype[_loc2_][6] = param1[8];
         this.guntype[_loc2_][7] = Number(param1[9]);
         this.guntype[_loc2_][8] = Number(param1[10]);
         this.guntype[_loc2_][10] = getDefinitionByName("guntype" + _loc2_ + "fire") as Class;
      }
      
      public function func_updateNavigationDisplay() : *
      {
         var _loc1_:* = undefined;
         var _loc2_:* = undefined;
         if(this.playersdestination[0] >= 0)
         {
            _loc1_ = Math.round(Math.sqrt(Math.pow(this.shipcoordinatex - Number(this.playersdestination[1]),2) + Math.pow(this.shipcoordinatey - Number(this.playersdestination[2]),2)));
            if(_loc1_ > 300)
            {
               this.NavigationImage.visible = true;
               _loc2_ = 180 - Math.atan2(Number(this.playersdestination[1]) - this.shipcoordinatex,Number(this.playersdestination[2]) - this.shipcoordinatey) / (Math.PI / 180);
               this.NavigationImage.rotation = _loc2_;
               this.NavigationImage.navText.rotation = -this.NavigationImage.rotation;
               this.NavigationImage.navText.directionInfo.text = this.playersdestination[4] + "\r" + _loc1_;
            }
            else
            {
               this.NavigationImage.visible = false;
            }
         }
         else
         {
            this.NavigationImage.visible = false;
         }
      }
      
      public function func_bringinspecial(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         trace("special:" + param1);
         _loc2_ = param1[1];
         _loc3_ = param1[2];
         if((_loc4_ = this.specialshipitems[_loc3_][5]) == "RECHARGESHIELD")
         {
            _loc5_ = 0;
            while(_loc5_ < this.otherplayership.length)
            {
               if(_loc2_ == this.otherplayership[_loc5_][0])
               {
                  this.otherplayership[_loc5_][41] = Number(param1[3]);
                  break;
               }
               _loc5_++;
            }
         }
         else if(_loc4_ == "RECHARGESTRUCT")
         {
            _loc5_ = 0;
            while(_loc5_ < this.otherplayership.length)
            {
               if(_loc2_ == this.otherplayership[_loc5_][0])
               {
                  this.otherplayership[_loc5_][40] = Number(param1[3]);
                  break;
               }
               _loc5_++;
            }
         }
      }
      
      internal function frame147() : *
      {
         this.chatDisplay.visible = false;
         this.gameError.reloadButton.visible = false;
         this.gameError.reloadButton.addEventListener(MouseEvent.MOUSE_DOWN,this.func_GameErrorReload);
         this.func_DisplayError(this.gameerror);
         stop();
      }
      
      public function MovingObjectWithThrust(param1:*, param2:*) : *
      {
         var _loc3_:* = new Array();
         param1 *= Math.PI / 180;
         if(param1 == 0)
         {
            _loc3_[1] = -param2;
            _loc3_[0] = 0;
         }
         if(param2 != 0)
         {
            _loc3_[1] = -(param2 * Math.cos(param1));
            _loc3_[0] = param2 * Math.sin(param1);
         }
         else
         {
            _loc3_[0] = 0;
            _loc3_[1] = 0;
         }
         return _loc3_;
      }
      
      public function func_AcceptDeathButton_Click(param1:MouseEvent) : void
      {
         gotoAndStop("dockedscreen");
      }
      
      public function func_EnableChatPress() : *
      {
         if(this.chatDisplay.visible)
         {
            if(!this.isPlayerChatting)
            {
               this.isPlayerChatting = true;
               this.func_resetFlightKeys();
               this.chatDisplay.chatInput.stage.focus = this.chatDisplay.chatInput;
            }
            else
            {
               this.func_SubmitChat();
               this.isPlayerChatting = false;
               stage.focus = stage;
            }
         }
      }
      
      public function func_initializePlayersship() : *
      {
         var _loc4_:* = undefined;
         this.playershipstatus[5][27] = false;
         var _loc1_:* = this.playershipstatus[5][0];
         this.playershipstatus[5][4] = "alive";
         this.playershiprotation = this.shiptype[_loc1_][3][2];
         this.playershipmaxvelocity = this.shiptype[_loc1_][3][1];
         this.playershipafterburnerspeed = this.shiptype[_loc1_][3][6];
         this.playershipacceleration = this.shiptype[_loc1_][3][0];
         this.playershipvelocity = 0;
         var _loc2_:* = 0;
         while(_loc2_ < this.playershipstatus[0].length)
         {
            _loc4_ = this.playershipstatus[0][_loc2_][0];
            if(isNaN(_loc4_))
            {
               _loc4_ = 0;
            }
            this.playershipstatus[0][_loc2_][1] = 0;
            this.playershipstatus[0][_loc2_][2] = this.shiptype[this.playershipstatus[5][0]][2][_loc2_][0];
            this.playershipstatus[0][_loc2_][3] = this.shiptype[this.playershipstatus[5][0]][2][_loc2_][1];
            this.playershipstatus[0][_loc2_][4] = "ON";
            this.playershipstatus[0][_loc2_][5] = this.guntype[_loc4_][3];
            this.playershipstatus[0][_loc2_][6] = Number(this.guntype[_loc4_][2]) * 1000;
            this.playershipstatus[0][_loc2_][7] = Math.round(this.guntype[_loc4_][0]);
            this.playershipstatus[0][_loc2_][8] = Number(this.guntype[_loc4_][1]) * 1000;
            _loc2_++;
         }
         var _loc3_:* = 0;
         while(_loc3_ < this.playershipstatus[8].length)
         {
            _loc4_ = this.playershipstatus[8][_loc3_][0];
            if(isNaN(_loc4_))
            {
               _loc4_ = 0;
            }
            this.playershipstatus[8][_loc3_][1] = 0;
            this.playershipstatus[8][_loc3_][2] = this.shiptype[this.playershipstatus[5][0]][5][_loc3_][0];
            this.playershipstatus[8][_loc3_][3] = this.shiptype[this.playershipstatus[5][0]][5][_loc3_][1];
            this.playershipstatus[8][_loc3_][4] = "ON";
            this.playershipstatus[8][_loc3_][5] = this.guntype[_loc4_][3];
            this.playershipstatus[8][_loc3_][6] = Number(this.guntype[_loc4_][2]) * 1000;
            this.playershipstatus[8][_loc3_][7] = Math.round(this.guntype[_loc4_][0]);
            this.playershipstatus[8][_loc3_][8] = Number(this.guntype[_loc4_][1]) * 1000;
            _loc3_++;
         }
         this.playershipstatus[1][1] = this.energycapacitors[this.playershipstatus[1][5]][0];
         this.playershipstatus[2][1] = this.shieldgenerators[this.playershipstatus[2][0]][0];
         this.playersmaxstructure = this.shiptype[this.playershipstatus[5][0]][3][3];
         this.playershipstatus[2][5] = this.playersmaxstructure;
         this.playershipstatus[2][6] = 0;
         this.playershipstatus[2][2] = "FULL";
      }
      
      public function credDisplayTimerHandler(param1:TimerEvent) : void
      {
         this.creditBar.func_displayCredits(Math.floor(this.playershipstatus[3][1]));
         this.scoreDISP.func_displayScore(Math.floor(Number(this.playershipstatus[5][9]) / 50));
      }
      
      public function UpdatePing() : *
      {
         var _loc2_:* = undefined;
         var _loc1_:* = getTimer();
         if(_loc1_ > this.lastpingcheck - this.pingintervalcheck + 1600)
         {
            _loc2_ = "PING`" + _loc1_ + "~";
            this.mysocket.send(_loc2_);
            this.remoteupdate = true;
            this.lastpingcheck += this.pingintervalcheck;
         }
      }
      
      public function onConnect(param1:*) : void
      {
         trace("Trace : Connection Made");
         this.IsSocketConnected = true;
         nextFrame();
      }
      
      public function func_checkothercharacters(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         _loc2_ = 0;
         while(_loc2_ < param1.length)
         {
            _loc3_ = 0;
            while(_loc3_ < this.replacewithchar.length)
            {
               if(param1.substr(_loc2_,this.replacewithchar[_loc3_][0].length) == this.replacewithchar[_loc3_][0])
               {
                  _loc4_ = param1.substr(0,_loc2_);
                  _loc5_ = param1.substr(_loc2_ + this.replacewithchar[_loc3_][0].length);
                  param1 = _loc4_ + this.replacewithchar[_loc3_][1] + _loc5_;
                  if(_loc2_ >= param1.length)
                  {
                     _loc2_ = 10000;
                  }
                  break;
               }
               _loc3_++;
            }
            _loc2_++;
         }
         return param1;
      }
      
      public function func_UpdateRadarPosition(param1:*, param2:*) : *
      {
         this.gameRadar.radarScreen.x = -param1 * 0.05;
         this.gameRadar.radarScreen.y = -param2 * 0.05;
      }
      
      public function func_GameSettingScreenPressed(param1:MouseEvent) : void
      {
         if(this.gameSettingScreen.isFinished == true)
         {
            nextFrame();
         }
      }
      
      public function func_displayspecials(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         _loc2_ = param1 - 1;
         _loc3_ = this.playershipstatus[11][1][_loc2_][0];
         this.specialsingame["sp" + param1].specialdisplay.text = this.specialshipitems[_loc3_][0];
         if(this.playershipstatus[11][1][_loc2_][1] >= 0)
         {
            this.specialsingame["sp" + param1].specialdisplay.text += " (" + this.playershipstatus[11][1][_loc2_][1] + ")";
         }
      }
      
      public function func_DockOtherShip(param1:*) : *
      {
         var currentplayerid:* = param1;
         var jjjj:* = 0;
         var TotalOthers:* = this.otherplayership.length;
         while(jjjj < TotalOthers)
         {
            if(currentplayerid == this.otherplayership[jjjj][0])
            {
               this.otherplayership[jjjj][4] = 0;
               this.otherplayership[jjjj][5][0] = 0;
               this.func_RemoveRadarDot(this.otherplayership[jjjj][59]);
               try
               {
                  this.gamedisplayarea.removeChild(this.otherplayership[jjjj][21]);
               }
               catch(error:Error)
               {
                  trace("Error: " + error);
               }
               this.otherplayership[jjjj][21] = new this.shipDockingImage() as MovieClip;
               this.gamedisplayarea.addChild(this.otherplayership[jjjj][21]);
               this.otherplayership[jjjj][21].x = -999999;
               this.otherplayership[jjjj][6] = getTimer() + 1200;
               this.otherplayership[jjjj][15] = "docking";
               break;
            }
            jjjj++;
         }
      }
      
      public function func_LoadZonesSystemMap(param1:*) : *
      {
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         this.loginmovie.mov_login.logindisplay.currentstatus.text = "Loading System";
         var _loc2_:* = param1.split("~");
         var _loc3_:* = 0;
         var _loc4_:* = "";
         this.sectormapitems = new Array();
         this.starbaselocation = new Array();
         this.teambases = new Array();
         while(_loc3_ < _loc2_.length - 1)
         {
            if(_loc2_[_loc3_].substr(0,2) == "SB" || _loc2_[_loc3_].substr(0,2) == "PL")
            {
               this.sectormapitems[_loc3_] = new Array();
               _loc4_ = _loc2_[_loc3_].split("`");
               _loc6_ = this.starbaselocation.length;
               this.starbaselocation[_loc6_] = new Array();
               this.starbaselocation[_loc6_][0] = _loc4_[0];
               this.sectormapitems[_loc3_][0] = _loc4_[0];
               this.sectormapitems[_loc3_][1] = Number(_loc4_[1].substr("1"));
               this.starbaselocation[_loc6_][1] = Number(_loc4_[1].substr("1"));
               this.sectormapitems[_loc3_][2] = Number(_loc4_[2].substr("1"));
               this.starbaselocation[_loc6_][2] = Number(_loc4_[2].substr("1"));
               this.sectormapitems[_loc3_][3] = int(_loc4_[3].substr("1"));
               this.starbaselocation[_loc6_][3] = int(_loc4_[3].substr("1"));
               this.starbaselocation[_loc6_][4] = int(_loc4_[4].substr("1"));
               this.sectormapitems[_loc3_][6] = int(_loc4_[4].substr("1"));
               this.starbaselocation[_loc6_][5] = Number(_loc4_[5]) * 1000 + getTimer();
               if(Number(_loc4_[5]) == 0)
               {
                  this.starbaselocation[_loc6_][5] = "ACTIVE";
               }
               this.starbaselocation[_loc6_][9] = 0;
               this.starbaselocation[_loc6_][11] = Number(_loc4_[6]);
               this.starbaselocation[_loc6_][12] = Number(_loc4_[7]);
               this.starbaselocation[_loc6_][13] = 400;
               this.starbaselocation[_loc6_][14] = 25000;
               this.starbaselocation[_loc6_][15] = 250000;
               this.starbaselocation[_loc6_][16] = "N/A";
               if(_loc2_[_loc3_].substr(0,2) == "SB")
               {
                  _loc5_ = getDefinitionByName("starbasetype" + this.starbaselocation[_loc6_][3]) as Class;
                  this.starbaselocation[_loc6_][21] = new _loc5_() as MovieClip;
               }
               else
               {
                  _loc5_ = getDefinitionByName("planettype" + this.starbaselocation[_loc6_][3]) as Class;
                  this.starbaselocation[_loc6_][21] = new _loc5_() as MovieClip;
               }
               this.starbaselocation[_loc6_][22] = new this.RadarBlot() as MovieClip;
               this.sectormapitems[_loc3_][9] = 0;
               this.sectormapitems[_loc3_][4] = "OFF";
               this.sectormapitems[_loc3_][10] = Math.ceil(Number(this.sectormapitems[_loc3_][1]) / Number(this.sectorinformation[1][0]));
               this.sectormapitems[_loc3_][11] = Math.ceil(Number(this.sectormapitems[_loc3_][2]) / Number(this.sectorinformation[1][1]));
            }
            if(_loc2_[_loc3_].substr(0,2) == "TB")
            {
               this.teamdeathmatch = true;
               this.sectormapitems[_loc3_] = new Array();
               _loc4_ = _loc2_[_loc3_].split("`");
               _loc7_ = this.teambases.length;
               this.teambases[_loc7_] = new Array();
               this.teambases[_loc7_][0] = _loc4_[0];
               this.sectormapitems[_loc3_][0] = _loc4_[0];
               this.sectormapitems[_loc3_][1] = int(_loc4_[1].substr("1"));
               this.teambases[_loc7_][1] = Number(_loc4_[1].substr("1"));
               this.sectormapitems[_loc3_][2] = int(_loc4_[2].substr("1"));
               this.teambases[_loc7_][2] = Number(_loc4_[2].substr("1"));
               this.sectormapitems[_loc3_][3] = int(_loc4_[3].substr("1"));
               this.teambases[_loc7_][3] = int(_loc4_[3].substr("1"));
               this.teambases[_loc7_][4] = Number(_loc4_[4].substr("1"));
               this.sectormapitems[_loc3_][6] = int(_loc4_[4].substr("1"));
               this.teambases[_loc7_][5] = _loc4_[5];
               this.teambases[_loc7_][9] = 0;
               this.sectormapitems[_loc3_][9] = 0;
               this.teambases[_loc7_][10] = 0;
               this.teambases[_loc7_][11] = 0;
               this.sectormapitems[_loc3_][4] = "OFF";
               this.sectormapitems[_loc3_][10] = Math.ceil(Number(this.sectormapitems[_loc3_][1]) / Number(this.sectorinformation[1][0]));
               this.sectormapitems[_loc3_][11] = Math.ceil(Number(this.sectormapitems[_loc3_][2]) / Number(this.sectorinformation[1][1]));
               this.sectormapitems[_loc3_][15] = int(_loc4_[6]);
               this.teambases[_loc7_][15] = int(_loc4_[6]);
               this.sectormapitems[_loc3_][17] = int(_loc4_[7]);
               this.teambases[_loc7_][17] = int(_loc4_[7]);
               _loc5_ = getDefinitionByName("teambasetype" + this.teambases[_loc7_][3]) as Class;
               this.teambases[_loc7_][21] = new _loc5_() as MovieClip;
               this.teambases[_loc7_][22] = new this.RadarBlot() as MovieClip;
               this.teambases[_loc7_][23] = new shiphealthbar() as MovieClip;
               this.teambases[_loc7_][21].addChild(this.teambases[_loc7_][23]);
               this.teambases[_loc7_][23].y = -63;
            }
            if(_loc2_[_loc3_].substr(0,2) == "AS")
            {
               this.sectormapitems[_loc3_] = new Array();
               _loc4_ = _loc2_[_loc3_].split("`");
               this.sectormapitems[_loc3_][0] = _loc4_[0];
               this.sectormapitems[_loc3_][1] = int(_loc4_[1].substr("1"));
               this.sectormapitems[_loc3_][2] = int(_loc4_[2].substr("1"));
               this.sectormapitems[_loc3_][3] = int(_loc4_[3].substr("1"));
               this.sectormapitems[_loc3_][4] = "OFF";
               this.sectormapitems[_loc3_][10] = Math.ceil(Number(this.sectormapitems[_loc3_][1]) / Number(this.sectorinformation[1][0]));
               this.sectormapitems[_loc3_][11] = Math.ceil(Number(this.sectormapitems[_loc3_][2]) / Number(this.sectorinformation[1][1]));
               this.sectormapitems[_loc3_][22] = new this.RadarBlot() as MovieClip;
            }
            if(_loc2_[_loc3_].substr(0,2) == "NP")
            {
               this.sectormapitems[_loc3_] = new Array();
               _loc4_ = _loc2_[_loc3_].split("`");
               this.sectormapitems[_loc3_][0] = _loc4_[0];
               this.sectormapitems[_loc3_][1] = Number(_loc4_[1].substr("1"));
               this.sectormapitems[_loc3_][2] = Number(_loc4_[2].substr("1"));
               this.sectormapitems[_loc3_][3] = Number(_loc4_[3].substr("1"));
               this.sectormapitems[_loc3_][4] = "OFF";
               _loc8_ = Number(_loc4_[4]);
               if(isNaN(_loc8_))
               {
                  this.sectormapitems[_loc3_][5] = _loc4_[4];
               }
               else
               {
                  this.sectormapitems[_loc3_][5] = "Jump Point to System: " + _loc4_[4];
                  this.sectormapitems[_loc3_][9] = _loc8_;
               }
               this.sectormapitems[_loc3_][10] = Math.ceil(Number(this.sectormapitems[_loc3_][1]) / Number(this.sectorinformation[1][0]));
               this.sectormapitems[_loc3_][11] = Math.ceil(Number(this.sectormapitems[_loc3_][2]) / Number(this.sectorinformation[1][1]));
               _loc5_ = getDefinitionByName("navpointtype" + this.sectormapitems[_loc3_][3]) as Class;
               this.sectormapitems[_loc3_][21] = new _loc5_() as MovieClip;
               this.sectormapitems[_loc3_][22] = new this.RadarBlot() as MovieClip;
            }
            if(_loc2_[_loc3_].substr(0,2) == "SI")
            {
               if((_loc4_ = _loc2_[_loc3_].split("`"))[1].substr(0,2) == "XL")
               {
                  this.sectorinformation = new Array();
                  this.sectorinformation[0] = new Array();
                  this.sectorinformation[0][0] = Number(_loc4_[1].substr("2"));
                  this.sectorinformation[0][1] = Number(_loc4_[2].substr("2"));
                  this.sectorinformation[1] = new Array();
                  this.sectorinformation[1][0] = Number(_loc4_[3].substr("2"));
                  this.sectorinformation[1][1] = Number(_loc4_[4].substr("2"));
                  this.isaracealteredZone = false;
                  _loc9_ = _loc4_[6].split(",");
                  _loc10_ = 0;
                  while(_loc10_ < _loc9_.length)
                  {
                     if(_loc9_[_loc10_] == "PULSAR")
                     {
                        this.pulsarsinzone = true;
                     }
                     if(_loc9_[_loc10_] == "RACES")
                     {
                        this.isaracealteredZone = true;
                     }
                     _loc10_++;
                  }
               }
            }
            _loc3_++;
         }
         if(this.isaracealteredZone == false)
         {
         }
         this.func_InitializeGameMap();
         this.loginmovie.mov_login.logindisplay.currentstatus.text += "\rSystemLoaded\r";
      }
      
      public function func_TriggerGameMap() : *
      {
         if(this.sectorinformation[0] != null)
         {
            if(this.gameMap.visible)
            {
               this.MapTimer.stop();
               this.gameMap.visible = false;
            }
            else
            {
               this.playersdestination[0] = this.gameMap.func_PlayerMapLocation(this.shipcoordinatex,this.shipcoordinatey,this.AnglePlayerShipFacing);
               this.func_setHeadingLocation();
               this.MapTimer.start();
               this.gameMap.visible = true;
            }
         }
      }
      
      public function loadplayerextraships(param1:*) : *
      {
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         var _loc15_:* = undefined;
         var _loc2_:* = param1.split("~");
         var _loc3_:* = 0;
         trace("INFO" + param1);
         trace("1");
         this.extraplayerships = new Array();
         _loc3_ = 0;
         while(_loc3_ < this.maxextraships)
         {
            this.extraplayerships[_loc3_] = new Array();
            this.extraplayerships[_loc3_][0] = null;
            _loc3_++;
         }
         var _loc4_:* = _loc2_[0].substr(2);
         _loc3_ = 0;
         while(_loc3_ + 1 < _loc2_.length - 1)
         {
            if(_loc2_[_loc3_ + 1].substr(0,2) == "SH")
            {
               if((_loc5_ = _loc2_[_loc3_ + 1].split("`"))[1].substr(0,2) == "ST")
               {
                  _loc6_ = Number(_loc5_[0].substr("2"));
                  this.extraplayerships[_loc6_] = new Array();
                  this.extraplayerships[_loc6_][0] = Number(_loc5_[1].substr("2"));
                  this.extraplayerships[_loc6_][1] = Number(_loc5_[2].substr("2"));
                  this.extraplayerships[_loc6_][2] = Number(_loc5_[3].substr("2"));
                  this.extraplayerships[_loc6_][3] = Number(_loc5_[4].substr("2"));
                  _loc7_ = 5;
                  this.extraplayerships[_loc6_][4] = new Array();
                  _loc8_ = 0;
                  while(_loc8_ < this.shiptype[this.extraplayerships[_loc6_][0]][2].length)
                  {
                     this.extraplayerships[_loc6_][4][_loc8_] = "none";
                     _loc8_++;
                  }
                  this.extraplayerships[_loc6_][5] = new Array();
                  _loc9_ = 0;
                  this.extraplayerships[_loc6_][5] = new Array();
                  while(_loc9_ < this.shiptype[this.extraplayerships[_loc6_][0]][5].length)
                  {
                     this.extraplayerships[_loc6_][5][_loc9_] = "none";
                     _loc9_++;
                  }
                  _loc10_ = 0;
                  this.extraplayerships[_loc6_][11] = new Array();
                  this.extraplayerships[_loc6_][11][1] = new Array();
                  _loc11_ = "";
                  while(_loc7_ < _loc5_.length)
                  {
                     if(_loc5_[_loc7_] == null)
                     {
                        break;
                     }
                     if(_loc5_[_loc7_].substr(0,2) == "HP")
                     {
                        trace("4-" + _loc7_ + "\'" + _loc5_.length);
                        _loc12_ = (_loc11_ = _loc5_[_loc7_].split("G"))[0].substr("2");
                        if(isNaN(_loc11_[1]))
                        {
                           _loc11_[1] = "none";
                        }
                        this.extraplayerships[_loc6_][4][_loc12_] = _loc11_[1];
                     }
                     else if(_loc5_[_loc7_].substr(0,2) == "TT")
                     {
                        _loc9_ = (_loc11_ = _loc5_[_loc7_].split("G"))[0].substr("2");
                        if(isNaN(_loc11_[1]))
                        {
                           _loc11_[1] = "none";
                        }
                        this.extraplayerships[_loc6_][5][_loc9_] = _loc11_[1];
                     }
                     else if(_loc5_[_loc7_].substr(0,2) == "SP")
                     {
                        _loc13_ = _loc5_[_loc7_].split("Q");
                        _loc14_ = Number(_loc13_[0].substr("2"));
                        _loc15_ = Number(_loc13_[1]);
                        this.extraplayerships[_loc6_][11][1][_loc10_] = new Array();
                        this.extraplayerships[_loc6_][11][1][_loc10_][0] = _loc14_;
                        this.extraplayerships[_loc6_][11][1][_loc10_][1] = _loc15_;
                        _loc10_++;
                     }
                     _loc7_++;
                  }
               }
            }
            _loc3_++;
         }
         if(_loc4_ != "capital")
         {
            if(isNaN(Number(_loc4_)) || Number(_loc4_) > this.maxextraships - 1)
            {
               _loc3_ = 0;
               while(_loc3_ < this.maxextraships)
               {
                  if(this.extraplayerships[_loc3_][0] != null && !isNaN(Number(_loc4_)))
                  {
                     _loc4_ = _loc3_;
                     break;
                  }
                  _loc3_++;
               }
            }
            this.changetonewship(_loc4_);
         }
      }
      
      public function MissileFireStyleChanged(param1:MouseEvent) : void
      {
         if(this.playershipstatus[10][1] != "Single")
         {
            this.playershipstatus[10][1] = "Single";
         }
         else
         {
            this.playershipstatus[10][1] = "All";
         }
         var _loc2_:* = this.missileBankWindow.func_RefreshDisplay(this.playershipstatus[7],this.playershipstatus[10][0],this.playershipstatus[10][1]);
      }
      
      public function func_removePlayersShotFromGlobalID(param1:*) : *
      {
         var _loc2_:* = 0;
         while(_loc2_ < this.playershotsfired.length)
         {
            if(param1 == this.playershotsfired[_loc2_][0])
            {
               this.func_removePlayersShot(_loc2_);
               break;
            }
            _loc2_++;
         }
      }
      
      public function func_IncomingChat(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         if(param1[2] == "M" || param1[2] == "PM" || param1[2] == "TM" || param1[2] == "SM" || param1[2] == "AM" || param1[2] == "STF" || param1[2] == "HLP")
         {
            _loc2_ = false;
            _loc4_ = 0;
            while(_loc4_ < this.currentonlineplayers.length)
            {
               _loc5_ = param1[1];
               if(param1[1].toUpperCase() == "HOST")
               {
                  if(param1[2] == "STF" || param1[2] == "HLP")
                  {
                     _loc3_ = this.func_checkothercharacters(param1[3]);
                  }
                  else
                  {
                     _loc3_ = "HOST: " + this.func_checkothercharacters(param1[3]);
                  }
                  break;
               }
               if(param1[2] == "PM")
               {
                  _loc3_ = param1[1] + ": " + this.func_checkothercharacters(param1[3]);
                  this.gamechatinfo[5] = param1[1];
               }
               else
               {
                  if(param1[2] == "SM")
                  {
                     _loc3_ = (_loc5_ = param1[1]) + ": " + this.func_checkothercharacters(param1[3]);
                     break;
                  }
                  if(param1[1] == this.currentonlineplayers[_loc4_][0])
                  {
                     _loc5_ = this.currentonlineplayers[_loc4_][1];
                     if(param1[2] == "PM")
                     {
                        this.gamechatinfo[5] = _loc5_;
                     }
                     if(param1[2] != "AM")
                     {
                        _loc2_ = this.func_ignoreplayer(_loc5_);
                     }
                     else
                     {
                        _loc2_ = false;
                     }
                     if(_loc2_ != true)
                     {
                        _loc3_ = _loc5_ + ": " + this.func_checkothercharacters(param1[3]);
                     }
                     break;
                  }
               }
               _loc4_++;
            }
            if(_loc2_ != true)
            {
               _loc6_ = this.regularchattextcolor;
               if(param1[2] == "M")
               {
                  _loc6_ = this.regularchattextcolor;
               }
               if(param1[2] == "PM")
               {
                  _loc6_ = this.privatechattextcolor;
               }
               if(param1[2] == "TM")
               {
                  _loc6_ = this.teamchattextcolor;
               }
               if(param1[2] == "SM")
               {
                  _loc6_ = this.squadchattextcolor;
               }
               if(param1[2] == "AM")
               {
                  _loc6_ = this.arenachattextcolor;
               }
               if(param1[2] == "STF")
               {
                  _loc6_ = this.stafchattextcolor;
               }
               if(param1[2] == "HLP")
               {
                  _loc6_ = this.staffhelpchattextcolor;
               }
               this.func_enterintochat(_loc3_,_loc6_);
            }
         }
         else if(param1[2] == "WM")
         {
            _loc3_ = "HOST: Your Base Has Been Attacked";
            _loc6_ = this.squadchattextcolor;
            this.func_enterintochat(_loc3_,_loc6_);
         }
      }
      
      public function func_enterintochat(param1:*, param2:*) : *
      {
         var _loc3_:* = new Array();
         _loc3_[0] = param1;
         _loc3_[1] = param2;
         this.gamechatinfo[1].splice(0,0,_loc3_);
         this.gamechatinfo[1].splice(30,1);
         this.func_RefreshChat();
      }
      
      public function func_AddChatter(param1:*) : *
      {
         trace(param1);
         this.chatDisplay.func_addMiscChatter(param1);
      }
      
      public function func_fire_a_gunshot(param1:*, param2:*, param3:*, param4:*, param5:*, param6:*, param7:*, param8:*) : *
      {
         var _loc9_:*;
         if((_loc9_ = this.playershotsfired.length) < 1)
         {
            this.playershotsfired = new Array();
            _loc9_ = 0;
         }
         if(this.currentplayershotsfired > 998)
         {
            this.currentplayershotsfired = 0;
         }
         var _loc10_:* = this.currentplayershotsfired;
         ++this.currentplayershotsfired;
         this.playershotsfired[_loc9_] = new Array();
         this.playershotsfired[_loc9_][0] = _loc10_;
         this.playershotsfired[_loc9_][1] = Math.round(param2);
         this.playershotsfired[_loc9_][2] = Math.round(param3);
         this.playershotsfired[_loc9_][3] = Math.round(param4);
         this.playershotsfired[_loc9_][4] = Math.round(param5);
         this.playershotsfired[_loc9_][5] = param8 + Number(this.guntype[param1][1]) * 1000;
         this.playershotsfired[_loc9_][6] = param1;
         this.playershotsfired[_loc9_][7] = param6;
         this.playershotsfired[_loc9_][8] = param8 + Number(this.guntype[param1][2]) * 1000;
         this.playershotsfired[_loc9_][9] = new this.guntype[param1][10]() as MovieClip;
         this.gamedisplayarea.addChild(this.playershotsfired[_loc9_][9]);
         this.playershotsfired[_loc9_][9].x = Number(this.playershotsfired[_loc9_][1]) - this.shipcoordinatex;
         this.playershotsfired[_loc9_][9].y = Number(this.playershotsfired[_loc9_][2]) - this.shipcoordinatey;
         this.playershotsfired[_loc9_][9].rotation = param6;
         this.playershotsfired[_loc9_][10] = true;
         this.playershotsfired[_loc9_][13] = "GUNS";
         var _loc11_:* = this.func_globalTimeStamp(param8);
         return "GF" + "`" + this.playershipstatus[3][0] + "`" + Math.round(param2) + "`" + Math.round(param3) + "`" + Math.round(param2 + param4 * this.shipositiondelay * 0.001) + "`" + Math.round(param3 + param5 * this.shipositiondelay * 0.001) + "`" + Math.round(param7) + "`" + Math.round(param6) + "`" + this.playershotsfired[_loc9_][6] + "`" + _loc10_ + "`" + _loc11_ + "~";
      }
      
      public function func_initalizeStatDisplay() : *
      {
         var _loc1_:* = this.playershipstatus[1][0];
         this.energyrechargerate = this.energygenerators[_loc1_][0];
         var _loc2_:* = this.playershipstatus[1][5];
         this.maxenergy = this.energycapacitors[_loc2_][0];
         var _loc3_:* = this.playershipstatus[2][0];
         this.energydrainedbyshieldgenatfull = this.shieldgenerators[_loc3_][3];
         this.shieldrechargerate = this.shieldgenerators[_loc3_][1];
         this.maxshieldstrength = this.shieldgenerators[_loc3_][0];
         this.baseshieldgendrain = this.shieldgenerators[_loc3_][2];
         this.isplayeremp = false;
         this.playerempend = 0;
         this.PlayerStatDisp.energy = this.maxenergy;
         this.PlayerStatDisp.maxenergy = this.maxenergy;
         this.PlayerStatDisp.shield = this.shieldgenerators[this.playershipstatus[2][0]][0];
         this.PlayerStatDisp.maxshieldstrength = this.maxshieldstrength;
         this.PlayerStatDisp.structure = this.shiptype[this.playershipstatus[5][0]][3][3];
         this.PlayerStatDisp.playersmaxstructure = this.shiptype[this.playershipstatus[5][0]][3][3];
         this.PlayerStatDisp.maxvelocity = this.playershipmaxvelocity;
      }
      
      public function func_KillOpponentsShip(param1:*) : *
      {
         var currentplayerid:* = param1;
         var jjjj:* = 0;
         var TotalOthers:* = this.otherplayership.length;
         while(jjjj < TotalOthers)
         {
            if(currentplayerid == this.otherplayership[jjjj][0])
            {
               this.func_RemoveRadarDot(this.otherplayership[jjjj][59]);
               try
               {
                  this.gamedisplayarea.removeChild(this.otherplayership[jjjj][21]);
               }
               catch(error:Error)
               {
                  trace("Kill Ship Error: " + error);
               }
               this.otherplayership[jjjj][21] = new this.shipDeadImage() as MovieClip;
               this.gamedisplayarea.addChild(this.otherplayership[jjjj][21]);
               this.otherplayership[jjjj][21].x = -999999;
               this.otherplayership[jjjj][6] = getTimer() + 2200;
               this.otherplayership[jjjj][15] = "dead";
               break;
            }
            jjjj++;
         }
      }
      
      public function func_dm_getBaseLives() : *
      {
         this.mysocket.send("DM`BL`~");
      }
      
      public function func_StealthTheRadar() : *
      {
         this.gameRadar.gotoAndStop(2);
      }
      
      public function func_LogOutPlayer(param1:*) : *
      {
         var _loc2_:* = 0;
         var _loc3_:* = "";
         while(_loc2_ < this.currentonlineplayers.length)
         {
            if(param1 == this.currentonlineplayers[_loc2_][0])
            {
               _loc3_ = this.currentonlineplayers[_loc2_][1];
               this.currentonlineplayers.splice(_loc2_,1);
            }
            _loc2_++;
         }
         this.func_refreshCurrentOnlineList();
         this.func_AddChatter(_loc3_ + " has left");
      }
      
      public function func_exitHardWareScreen(param1:MouseEvent) : void
      {
         this.playershipstatus = this.ShipHardwareScreen.playershipstatus;
         this.OnlinePLayerList.visible = true;
         this.savecurrenttoextraship(this.playerscurrentextrashipno);
         gotoAndStop("dockedscreen");
      }
      
      public function func_AddOtherGunshot(param1:*) : *
      {
         var _loc6_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         if(this.othergunfire.length < 1)
         {
            this.othergunfire = new Array();
         }
         ++this.currentotherplayshot;
         if(this.currentotherplayshot >= 500)
         {
            this.currentotherplayshot = 1;
         }
         var _loc2_:* = this.othergunfire.length;
         this.othergunfire[_loc2_] = new Array();
         this.othergunfire[_loc2_][0] = param1[1];
         if(Number(param1[1]) < 4000)
         {
            param1.splice(4,2);
         }
         if(this.othergunfire[_loc2_][0].substr(0,2) == "AI")
         {
            this.othergunfire[_loc2_][0].splice(2,1);
         }
         this.othergunfire[_loc2_][1] = Number(param1[2]);
         this.othergunfire[_loc2_][2] = Number(param1[3]);
         var _loc3_:* = int(param1[4]);
         this.othergunfire[_loc2_][6] = int(param1[5]);
         var _loc4_:* = this.othergunfire[_loc2_][6];
         var _loc5_:* = this.MovingObjectWithThrust(_loc4_,_loc3_);
         this.othergunfire[_loc2_][3] = _loc5_[0];
         this.othergunfire[_loc2_][4] = _loc5_[1];
         this.othergunfire[_loc2_][7] = int(param1[6]);
         this.othergunfire[_loc2_][8] = this.currentotherplayshot;
         this.othergunfire[_loc2_][9] = int(param1[7]);
         this.othergunfire[_loc2_][10] = "other";
         this.othergunfire[_loc2_][14] = false;
         if(this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] != -1)
         {
            if((_loc6_ = this.othergunfire[_loc2_][0]).substr(0,2) == "AI")
            {
               if(_loc6_.charAt(2) == "F")
               {
                  _loc6_ = (_loc10_ = _loc6_.split("-"))[0].substr(3);
               }
            }
            _loc9_ = 0;
            while(_loc9_ < this.currentonlineplayers.length)
            {
               if(this.currentonlineplayers[_loc9_][0] == _loc6_)
               {
                  if(this.currentonlineplayers[_loc9_][4] == this.playershipstatus[5][2])
                  {
                     this.othergunfire[_loc2_][14] = true;
                     break;
                  }
               }
               _loc9_++;
            }
         }
         this.othergunfire[_loc2_][5] = getTimer() + Number(this.guntype[this.othergunfire[_loc2_][7]][1]) * 1000;
         var _loc7_:* = String(getTimer() + this.clocktimediff);
         var _loc8_:*;
         if((_loc8_ = (_loc7_ = Number(_loc7_.substr(Number(_loc7_.length) - 4))) - Number(param1[8])) < -1000)
         {
            _loc8_ += 10000;
         }
         else if(_loc8_ > 10000)
         {
            _loc8_ -= 10000;
         }
         _loc8_ /= 1000;
         this.othergunfire[_loc2_][1] += Number(this.othergunfire[_loc2_][3]) * _loc8_;
         this.othergunfire[_loc2_][2] += Number(this.othergunfire[_loc2_][4]) * _loc8_;
         this.othergunfire[_loc2_][16] = new this.guntype[this.othergunfire[_loc2_][7]][10]() as MovieClip;
         this.gamedisplayarea.addChild(this.othergunfire[_loc2_][16]);
         this.othergunfire[_loc2_][16].x = Number(this.othergunfire[_loc2_][1]) - this.shipcoordinatex;
         this.othergunfire[_loc2_][16].y = Number(this.othergunfire[_loc2_][2]) - this.shipcoordinatey;
         this.othergunfire[_loc2_][16].rotation = this.othergunfire[_loc2_][6];
         this.othergunfire[_loc2_][18] = Number(param1[2]);
         this.othergunfire[_loc2_][19] = Number(param1[3]);
         this.othergunfire[_loc2_][11] = Number(this.othergunfire[_loc2_][16].width) / 2;
         this.othergunfire[_loc2_][12] = Number(this.othergunfire[_loc2_][16].height) / 2;
      }
      
      public function DeathMatchTimerHandler(param1:TimerEvent) : void
      {
         var event:TimerEvent = param1;
         try
         {
            this.displaystats();
            this.func_dm_getBaseLives();
            ++this.counterForBaseLives;
            if(this.counterForBaseLives > 10)
            {
               this.func_dm_getBaseLives();
               this.counterForBaseLives = 0;
            }
         }
         catch(error:Error)
         {
            DeathMatchTimer.stop();
         }
      }
      
      public function myLineHittest(param1:*, param2:*, param3:*, param4:*, param5:*, param6:*, param7:*) : *
      {
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         var _loc15_:* = undefined;
         var _loc16_:* = undefined;
         param3 *= 1.2;
         var _loc8_:* = 180 - Math.atan2(param4 - param6,param5 - param7) / (Math.PI / 180);
         var _loc9_:* = param4 - param1;
         var _loc10_:* = param5 - param2;
         var _loc12_:*;
         var _loc11_:*;
         if((_loc12_ = (_loc11_ = 180 - Math.atan2(_loc9_,_loc10_) / (Math.PI / 180)) - _loc8_) < -180)
         {
            _loc12_ += 360;
         }
         else if(_loc12_ > 180)
         {
            _loc12_ -= 360;
         }
         if(Math.abs(_loc12_) < 90)
         {
            _loc13_ = Math.sqrt((param4 - param6) * (param4 - param6) + (param5 - param7) * (param5 - param7));
            _loc14_ = Math.sqrt(_loc9_ * _loc9_ + _loc10_ * _loc10_);
            if((_loc15_ = Math.abs(Math.cos(_loc12_ * Math.PI / 180) * _loc14_)) > _loc13_)
            {
               if(Math.sqrt((param6 - param1) * (param6 - param1) + (param7 - param2) * (param7 - param2)) < param3)
               {
                  return true;
               }
               return false;
            }
            if((_loc16_ = Math.abs(Math.sin(_loc12_ * Math.PI / 180) * _loc14_)) < param3)
            {
               return true;
            }
            return false;
         }
         if(Math.sqrt((param4 - param1) * (param4 - param1) + (param5 - param2) * (param5 - param2)) < param3)
         {
            return true;
         }
         return false;
      }
      
      public function func_exitHangarScreen(param1:MouseEvent) : void
      {
         var _loc3_:* = undefined;
         var _loc2_:* = this.playershipstatus[5][0];
         this.playershipstatus = this.hangarWindow.playershipstatus;
         if(this.hangarWindow.playerscurrentextrashipno != this.playerscurrentextrashipno || this.playershipstatus[5][0] != this.hangarWindow.extraplayerships[this.playerscurrentextrashipno][0])
         {
            if(this.playershipstatus[5][0] == this.hangarWindow.extraplayerships[this.playerscurrentextrashipno][0])
            {
               this.savecurrenttoextraship(this.playerscurrentextrashipno);
            }
            this.extraplayerships = this.hangarWindow.extraplayerships;
            this.playerscurrentextrashipno = this.hangarWindow.playerscurrentextrashipno;
            this.changetonewship(this.playerscurrentextrashipno);
            this.initializemissilebanks();
            _loc3_ = "TC" + "`";
            _loc3_ += this.playershipstatus[3][0];
            _loc3_ += "`SC`" + this.playershipstatus[5][0] + "`0~";
            this.mysocket.send(_loc3_);
         }
         this.OnlinePLayerList.visible = true;
         gotoAndStop("dockedscreen");
      }
      
      public function func_ProcessClockTimer(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         if(this.ClockCheckTimesReceived.length <= this.ClockCheckstodo)
         {
            _loc2_ = Number(param1[1]);
            this.ClockCheckTimesReceived[this.ClockCheckTimesReceived.length] = _loc2_;
            this.loginmovie.mov_login.logindisplay.currentstatus.text += this.ClockCheckstodo - Number(this.ClockCheckTimesReceived.length) + ".";
            if(this.ClockCheckTimesReceived.length == this.ClockCheckstodo)
            {
               removeEventListener(Event.ENTER_FRAME,this.func_ClockSynchronizeScript);
               this.loginmovie.mov_login.logindisplay.currentstatus.text += "\r";
               _loc3_ = 0;
               _loc4_ = 0;
               while(_loc4_ < this.ClockCheckstodo)
               {
                  _loc3_ += this.ClockCheckTimesReceived[_loc4_];
                  _loc4_++;
               }
               this.clocktimediff = Math.round(_loc3_ / this.ClockCheckstodo);
               this.loginmovie.mov_login.logindisplay.currentstatus.text += "\r\rEntering Zone";
               this.LogIntoZone();
            }
            else
            {
               this.timetillnexClocktcheck = getTimer() + 200;
            }
         }
      }
      
      public function func_displayallspecials() : *
      {
         var _loc1_:* = undefined;
         _loc1_ = 1;
         while(_loc1_ < 10)
         {
            if(_loc1_ > this.playershipstatus[11][1].length)
            {
               this.specialsingame["sp" + _loc1_].visible = false;
            }
            else
            {
               this.specialsingame["sp" + _loc1_].specialinfodata.text = "";
               this.func_displayspecials(_loc1_);
               this.specialsingame["sp" + _loc1_].specialbutton.button.text = _loc1_;
               this.specialsingame["sp" + _loc1_].visible = true;
            }
            _loc1_++;
         }
      }
      
      public function func_updateTameBaseHealthBars(param1:*) : *
      {
         var baseNumber:* = param1;
         try
         {
            this.teambases[baseNumber][23].func_setLifeSettings(Number(this.teambases[baseNumber][10]) / Number(this.teambases[baseNumber][17]),0);
         }
         catch(error:Error)
         {
         }
      }
      
      public function func_MoveBackground() : *
      {
         this.gamedisplayarea.backgroundImage.x = (this.shipcoordinatex - this.HalfBackgroundWidth) / -30;
         this.gamedisplayarea.backgroundImage.y = (this.shipcoordinatey - this.HalfBackgroundMaxHeight) / -30;
      }
      
      public function func_turnoffstealth() : *
      {
         var _loc1_:* = undefined;
         _loc1_ = this.playerSpecialsSettings.StealthLocation;
         if(this.playershipstatus[5][15].charAt(0) == "S")
         {
            this.playershipstatus[5][15] = "";
            this.func_NormalRadar();
            this.playershipstatus[11][2][_loc1_ - 1][0] = 0;
            this.specialsingame["sp" + _loc1_].specialbutton.gotoAndStop("OFF");
            this.playerSpecialsSettings.isStealthed = false;
         }
      }
      
      public function func_resetTarget() : *
      {
         this.targetinfo[4] = this.playershipstatus[3][0];
         this.TargetDisplay.visible = false;
         this.targetinfo[0] = "";
      }
      
      public function func_buildBlankChat() : *
      {
         var _loc1_:* = this.gamechatinfo[2][1];
         while(_loc1_ >= 0)
         {
            this.gamechatinfo[1][_loc1_] = new Array();
            this.gamechatinfo[1][_loc1_][0] = "";
            this.gamechatinfo[1][_loc1_][1] = this.teamchattextcolor;
            _loc1_--;
         }
      }
      
      public function Proccess_Energies(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         if(this.playershipstatus[5][4] == "dead")
         {
            this.playershipstatus[2][1] = 0;
            this.playershipstatus[1][1] = 0;
            this.playershipstatus[2][5] = 0;
         }
         else
         {
            _loc2_ = this.playershipstatus[1][1];
            _loc3_ = this.playershipstatus[2][1];
            if(this.isplayeremp)
            {
               _loc3_ = 0;
               _loc2_ = 0;
               if(this.playerempend < getTimer())
               {
                  this.isplayeremp = false;
               }
            }
            else
            {
               if(isNaN(_loc3_))
               {
                  _loc3_ = 0;
               }
               if(_loc3_ < 0)
               {
                  _loc3_ = 0;
               }
               _loc3_ += this.shieldrechargerate * param1;
               if(this.playershipstatus[2][2] == "OFF")
               {
                  _loc3_ = 0;
               }
               else if(this.playershipstatus[2][2] == "HALF")
               {
                  if(_loc3_ > Math.round(this.maxshieldstrength / 2))
                  {
                     _loc3_ = Math.round(this.maxshieldstrength / 2);
                  }
               }
               else if(_loc3_ > this.maxshieldstrength)
               {
                  _loc3_ = this.maxshieldstrength;
               }
               _loc2_ += this.energyrechargerate * param1;
               if(this.playershipstatus[2][2] != "OFF")
               {
                  _loc2_ -= this.baseshieldgendrain * param1;
                  _loc2_ -= _loc3_ / this.maxshieldstrength * (this.energydrainedbyshieldgenatfull * param1);
               }
               if(_loc2_ < 0)
               {
                  _loc2_ = 0;
               }
               if(_loc2_ > this.maxenergy)
               {
                  _loc2_ = this.maxenergy;
               }
               this.playershipstatus[1][1] = _loc2_;
            }
         }
         this.playershipstatus[1][1] = _loc2_;
         this.playershipstatus[2][1] = _loc3_;
         this.PlayerStatDisp.func_displaystats(_loc2_,_loc3_,this.playershipstatus[2][5],this.playershipvelocity);
      }
      
      public function onSocketError(param1:IOErrorEvent) : void
      {
         trace("Socket ioErrorHandler: " + param1);
         this.gameerror = "failedtologin";
         gotoAndStop("gameclose");
      }
      
      public function func_HardwareBaseButton_Click(param1:MouseEvent) : void
      {
         gotoAndStop("ShipHardware");
      }
      
      public function func_turnofshipcloak() : *
      {
         var _loc1_:* = undefined;
         if(this.playershipstatus[5][15].charAt(0) == "C")
         {
            _loc1_ = this.playerSpecialsSettings.CloakLocation;
            this.playerSpecialsSettings.isCloaked = false;
            this.playershipstatus[5][15] = "";
            this.keywaspressed = true;
            this.PlayersShipImage.alpha = 1;
            this.playershipstatus[11][2][_loc1_ - 1][0] = getTimer() + 5000;
            this.specialsingame["sp" + _loc1_].specialbutton.gotoAndStop("RELOAD");
            this.specialsingame["sp" + _loc1_].specialinfodata.text = "";
         }
      }
      
      public function saveplayersgame() : *
      {
         var _loc1_:* = undefined;
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         if(this.savestatus == "Save Game")
         {
            _loc1_ = "";
            _loc1_ += "PI" + this.playershipstatus[3][0];
            _loc1_ += "`ST" + this.playershipstatus[5][0];
            _loc1_ += "`SG" + this.playershipstatus[2][0];
            _loc1_ += "`EG" + this.playershipstatus[1][0];
            _loc1_ += "`EC" + this.playershipstatus[1][5];
            _loc1_ += _loc1_ = _loc1_ + ("`CR" + this.playershipstatus[3][1]);
            _loc1_ += "`SE" + this.playershipstatus[5][1];
            _loc1_ += "`" + this.playershipstatus[4][0];
            _loc2_ = 0;
            while(_loc2_ < this.playershipstatus[0].length)
            {
               _loc1_ += "`HP" + _loc2_ + "G";
               _loc1_ += this.playershipstatus[0][_loc2_][0];
               _loc2_++;
            }
            _loc3_ = 0;
            while(_loc3_ < this.playershipstatus[8].length)
            {
               _loc1_ += "`TT" + _loc3_ + "G";
               _loc1_ += this.playershipstatus[8][_loc3_][0];
               _loc3_++;
            }
            _loc4_ = 0;
            while(_loc4_ < this.playershipstatus[11][1].length)
            {
               _loc1_ += "`SP" + this.playershipstatus[11][1][_loc4_][0] + "Q";
               _loc1_ += this.playershipstatus[11][1][_loc4_][1];
               _loc4_++;
            }
            _loc5_ = 0;
            while(_loc5_ < this.playershipstatus[7].length)
            {
               _loc6_ = 0;
               while(_loc6_ < this.playershipstatus[7][_loc5_][4].length)
               {
                  if(this.playershipstatus[7][_loc5_][4][_loc6_] > 0)
                  {
                     _loc1_ += "`MB" + _loc5_ + "T" + _loc6_ + "Q" + this.playershipstatus[7][_loc5_][4][_loc6_];
                  }
                  _loc6_++;
               }
               _loc5_++;
            }
            _loc1_ += "~";
            if(this.isplayeraguest == false)
            {
               _loc7_ = this.saveplayerextraships();
               if(this.lastplayerssavedinfosent == "info=" + _loc1_ + "&score=" + this.playershipstatus[5][9] + "&shipsdata=" + _loc7_)
               {
                  this.savestatus = "Save Game";
               }
               else
               {
                  this.lastplayerssavedinfosent = "info=" + _loc1_ + "&score=" + this.playershipstatus[5][9] + "&shipsdata=" + _loc7_;
                  _loc8_ = "SAVEG`~:" + this.playershipstatus[3][2] + ":" + this.playershipstatus[3][3] + ":" + _loc1_ + ":" + this.playershipstatus[5][9] + ":" + _loc7_ + ":" + this.playershipstatus[3][1] + ":" + this.playershipstatus[5][19] + ":";
                  this.mysocket.send(_loc8_);
                  if(this.isgamerunningfromremote == false)
                  {
                  }
               }
            }
         }
      }
      
      public function func_AddRadarDot(param1:*, param2:*) : *
      {
         if(param2 == "BASE")
         {
            param1.gotoAndStop("basedot");
         }
         else if(param2 == "NP")
         {
            param1.gotoAndStop("navpoint");
         }
         else if(param2 == "HOSTILE")
         {
            param1.gotoAndStop("enemydot");
         }
         else if(param2 == "FREIND")
         {
            param1.gotoAndStop("friendlydot");
         }
         this.gameRadar.radarScreen.addChild(param1);
      }
      
      public function func_resetSpecials() : *
      {
         var _loc1_:* = undefined;
         this.playershipstatus[11][2] = new Array();
         _loc1_ = 0;
         while(_loc1_ < this.playershipstatus[11][1].length)
         {
            this.playershipstatus[11][2][_loc1_] = new Array();
            this.playershipstatus[11][2][_loc1_][0] = 0;
            _loc1_++;
         }
      }
      
      public function sendnewdmgame() : *
      {
         var _loc1_:* = "";
         var _loc2_:* = 0;
         while(_loc2_ < this.teambases.length)
         {
            _loc1_ += "`" + this.teambasetypes[this.teambases[_loc2_][3]][0];
            _loc2_++;
         }
         var _loc3_:* = "DM" + "`NG" + _loc1_ + "~";
         this.mysocket.send(_loc3_);
      }
      
      public function func_setTurrets(param1:*) : *
      {
         this.turretcontrol.auto.gotoAndStop(2);
         this.turretcontrol.manual.gotoAndStop(2);
         this.turretcontrol.off.gotoAndStop(2);
         this.turretCrosshairs.visible = false;
         if(param1 == "auto")
         {
            this.playershipstatus[9] = "AUTO";
            this.turretcontrol.auto.gotoAndStop(1);
         }
         else if(param1 == "manual")
         {
            this.playershipstatus[9] = "MANUAL";
            this.turretcontrol.manual.gotoAndStop(1);
            this.turretCrosshairs.visible = true;
            this.turretCrosshairs.gotoAndStop(1);
         }
         else
         {
            this.playershipstatus[9] = "OFF";
            this.turretcontrol.off.gotoAndStop(1);
         }
      }
      
      public function func_InitPlayerAfterDock() : *
      {
         this.gunShotBufferData = "";
         this.missileShotBuuferData = "";
         this.currentplayershotsfired = 0;
         this.currenthelpframedisplayed = 0;
         this.playershipstatus[5][20] = false;
         this.playershotsfired = new Array();
         this.playerBeingSeekedByMissile = false;
         this.playershipstatus[6][0] = Math.ceil(this.shipcoordinatex / this.xwidthofasector);
         this.playershipstatus[6][1] = Math.ceil(this.shipcoordinatey / this.ywidthofasector);
         this.isupkeypressed = false;
         this.isdownkeypressed = false;
         this.isleftkeypressed = false;
         this.isrightkeypressed = false;
         this.iscontrolkeypressed = false;
         this.isdkeypressed = false;
         this.isshiftkeypressed = false;
         this.isspacekeypressed = false;
         this.keywaspressed = false;
         this.currenttimechangeratio = 0;
         this.afterburnerinuse = false;
         this.spacekeyjustpressed = false;
         this.iscontrolkeypressed = false;
         this.AnglePlayerShipFacing = 0;
         this.playershipvelocity = 0;
         this.NextShipTimeResend = getTimer() + this.InfoResendDelay - 500;
         this.playershipstatus[5][4] = "alive";
         this.playershipstatus[5][15] = "";
         this.playerShipSpeedRatio = 0;
         this.shipXmovement = 0;
         this.shipYmovement = 0;
         this.playershotsfired = new Array();
         this.func_initializePlayersship();
         this.func_initalizeStatDisplay();
         this.playershipstatus[6][2] = this.playershipstatus[6][0] = Math.ceil(this.shipcoordinatex / this.xwidthofasector);
         this.playershipstatus[6][3] = this.playershipstatus[6][1] = Math.ceil(this.shipcoordinatey / this.ywidthofasector);
         var _loc1_:* = this.playershipstatus[6][0] + "`" + this.playershipstatus[6][1];
         var _loc2_:* = "NEW`";
         this.lastshipcoordinatex = this.shipcoordinatex;
         this.lastshipcoordinatey = this.shipcoordinatey;
         this.playersFakeVelocity = 0;
      }
      
      public function func_ProcessGunHit(param1:*) : *
      {
         var basethatgothit:* = undefined;
         var shotdamage:* = undefined;
         var cc:* = undefined;
         var gunshottype:* = undefined;
         var currentnumber:* = undefined;
         var meteor:* = undefined;
         var aiwhogothit:* = undefined;
         var qz:* = undefined;
         var zzzz:* = undefined;
         var currentthread:* = param1;
         var playerwhogothit:* = currentthread[1];
         var shooterofbullet:* = currentthread[2];
         var shooternumberfire:* = Number(currentthread[3]);
         if(playerwhogothit == "SQ")
         {
            basethatgothit = String(currentthread[4]);
            shotdamage = 0;
            if(this.playershipstatus[3][0] != shooterofbullet)
            {
               cc = 0;
               while(cc < this.othergunfire.length)
               {
                  if(this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                  {
                     try
                     {
                        this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                     }
                     catch(error:Error)
                     {
                        trace("Error removing otherplayershot - gunhit: " + error);
                     }
                     gunshottype = this.othergunfire[cc][7];
                     shotdamage = this.guntype[gunshottype][4];
                     this.othergunfire.splice(cc,1);
                     cc = 2000;
                  }
                  cc++;
               }
            }
         }
         else if(playerwhogothit >= 4000)
         {
            if(playerwhogothit < 5000)
            {
               basethatgothit = playerwhogothit - 4000;
               basethatgothit = this.teambases[basethatgothit][0];
               shotdamage = 0;
               if(this.playershipstatus[3][0] != shooterofbullet)
               {
                  cc = 0;
                  while(cc < this.othergunfire.length)
                  {
                     if(this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                     {
                        try
                        {
                           this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                        }
                        catch(error:Error)
                        {
                           trace("Error removing otherplayershot - basehit: " + error);
                        }
                        this.othergunfire.splice(cc,1);
                        break;
                     }
                     cc++;
                  }
               }
               else if(this.playershipstatus[3][0] == shooterofbullet)
               {
                  currentnumber = 0;
                  while(currentnumber < this.playershotsfired.length)
                  {
                     if(this.playershotsfired[currentnumber][0] == shooternumberfire)
                     {
                        try
                        {
                           this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                        }
                        catch(error:Error)
                        {
                           trace("Error removing playershot - gunhit: " + error);
                        }
                        this.playershotsfired.splice(currentnumber,1);
                        break;
                     }
                     currentnumber++;
                  }
               }
            }
         }
         else if(playerwhogothit == "MET")
         {
            meteor = String(currentthread[4]);
            shotdamage = 0;
            if(this.playershipstatus[3][0] != shooterofbullet)
            {
               cc = 0;
               while(cc < this.othergunfire.length)
               {
                  if(this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                  {
                     try
                     {
                        this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                     }
                     catch(error:Error)
                     {
                        trace("Error removing otherplayershot - meteor hit: " + error);
                     }
                     gunshottype = this.othergunfire[cc][7];
                     shotdamage = this.guntype[gunshottype][4];
                     this.othergunfire.splice(cc,1);
                     cc = 2000;
                  }
                  cc++;
               }
            }
         }
         else
         {
            if(playerwhogothit == "AI")
            {
               playerwhogothit = aiwhogothit = currentthread[2];
               shooterofbullet = currentthread[3];
               shooternumberfire = Number(currentthread[4]);
            }
            else
            {
               qz = 0;
               while(qz < this.otherplayership.length)
               {
                  if(playerwhogothit == this.otherplayership[qz][0])
                  {
                     this.otherplayership[qz][57].gotoAndPlay(1);
                     break;
                  }
                  qz++;
               }
            }
            if(this.playershipstatus[3][0] != shooterofbullet)
            {
               cc = 0;
               while(cc < this.othergunfire.length)
               {
                  if(this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                  {
                     try
                     {
                        this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                     }
                     catch(error:Error)
                     {
                        trace("Error removing otherplayershot - meteor AIhit: " + error);
                     }
                     this.othergunfire.splice(cc,1);
                     cc = 2000;
                  }
                  cc++;
               }
            }
            else if(this.playershipstatus[3][0] == shooterofbullet)
            {
               zzzz = 0;
               while(zzzz < this.playershotsfired.length)
               {
                  if(this.playershotsfired[zzzz][0] == shooternumberfire)
                  {
                     try
                     {
                        this.gamedisplayarea.removeChild(this.playershotsfired[zzzz][9]);
                     }
                     catch(error:Error)
                     {
                        trace("Error removing playershot - AI hit: " + error);
                     }
                     this.playershotsfired.splice(zzzz,1);
                     break;
                  }
                  zzzz++;
               }
            }
         }
      }
      
      public function func_OnlinePlayersMinimize_Click(param1:MouseEvent) : void
      {
         this.fund_OnlineListToggled();
      }
      
      public function func_addshieldgenintocliet(param1:*) : *
      {
         var _loc2_:* = this.shieldgenerators.length;
         this.shieldgenerators[_loc2_] = new Array();
         this.shieldgenerators[_loc2_][0] = Number(param1[1]);
         this.shieldgenerators[_loc2_][1] = Number(param1[2]);
         this.shieldgenerators[_loc2_][2] = Number(param1[3]);
         this.shieldgenerators[_loc2_][3] = Number(param1[4]);
         this.shieldgenerators[_loc2_][4] = "Level " + (_loc2_ + 1);
         this.shieldgenerators[_loc2_][5] = Number(param1[5]);
      }
      
      public function func_isplayeronmap() : *
      {
         var _loc1_:* = false;
         var _loc2_:* = this.playershipstatus[6][0];
         var _loc3_:* = this.playershipstatus[6][1];
         if(_loc2_ >= 0 && _loc2_ <= this.sectorinformation[0][0])
         {
            if(_loc3_ >= 0 && _loc3_ <= this.sectorinformation[0][1])
            {
               _loc1_ = true;
            }
         }
         return _loc1_;
      }
      
      public function func_sendOutStats(param1:*) : *
      {
         var _loc2_:* = "STATS`TGT`INFO`";
         _loc2_ += this.playershipstatus[3][0] + "`" + param1 + "`";
         _loc2_ += Math.round(this.shieldrechargerate) + "`";
         _loc2_ += Math.round(this.maxshieldstrength) + "`";
         _loc2_ += Math.round(this.PlayerStatDisp.playersmaxstructure) + "`";
         _loc2_ += Math.round(this.playershipstatus[2][1]) + "`";
         _loc2_ += Math.round(this.playershipstatus[2][5]) + "`";
         _loc2_ += "~";
         this.mysocket.send(_loc2_);
      }
      
      public function deathfgamerewards() : *
      {
         return 0;
      }
      
      public function func_playerGotHit() : *
      {
         this.PlayersShipShieldImage.gotoAndPlay(1);
      }
      
      public function func_DeahtMatchInfoClick(param1:MouseEvent) : void
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         _loc2_ = new Array();
         _loc3_ = new Array();
         _loc4_ = 0;
         while(_loc4_ < this.teambases.length)
         {
            _loc2_[_loc4_] = 0;
            _loc3_[_loc4_] = 0;
            _loc4_++;
         }
         _loc4_ = 0;
         while(_loc4_ < this.currentonlineplayers.length)
         {
            if(this.currentonlineplayers[_loc4_][4] != "N/A" && this.currentonlineplayers[_loc4_][4] >= 0 && this.currentonlineplayers[_loc4_][4] < this.teambases.length)
            {
               if(this.currentonlineplayers[_loc4_][0] >= 0)
               {
                  _loc2_[this.currentonlineplayers[_loc4_][4]] += 1;
               }
               else
               {
                  _loc3_[this.currentonlineplayers[_loc4_][4]] += 1;
               }
            }
            _loc4_++;
         }
         _loc5_ = -1;
         _loc6_ = -1;
         if(this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] >= 0)
         {
            _loc6_ = -1;
            if(_loc2_[0] < _loc2_[1])
            {
               _loc6_ = 0;
            }
            else if(_loc2_[0] > _loc2_[1])
            {
               _loc6_ = 1;
            }
            else if(Number(this.playershipstatus[5][2]) > 1)
            {
               _loc6_ = Math.round(Math.random());
            }
            if(_loc6_ != this.playershipstatus[5][2] && _loc6_ > -1)
            {
               _loc5_ = _loc6_;
            }
            else
            {
               this.func_enterintochat(" Can\'t Uneven Teams",this.systemchattextcolor);
            }
         }
         else
         {
            _loc6_ = 0;
            if(_loc2_[0] == _loc2_[1])
            {
               _loc6_ = Math.round(Math.random() * (this.teambases.length - 1));
            }
            else if(_loc2_[0] > _loc2_[1])
            {
               _loc6_ = 1;
            }
            else
            {
               _loc6_ = 0;
            }
            _loc5_ = _loc6_;
         }
         if(_loc5_ >= 0)
         {
            this.datatosend = "TC" + "`" + this.playershipstatus[3][0] + "`TM`" + _loc5_ + "~";
            this.mysocket.send(this.datatosend);
            this.func_enterintochat(" Team Changing to : " + this.teambases[_loc5_][0].substr(2),this.systemchattextcolor);
         }
      }
      
      public function changetonewship(param1:*) : *
      {
         this.playerscurrentextrashipno = param1;
         var _loc2_:* = this.extraplayerships[param1][0];
         if(this.playershipstatus[5][0] != _loc2_)
         {
            this.playershipstatus[5][0] = _loc2_;
            this.playershipstatus[4][1] = new Array();
         }
         this.playershipstatus[5][27] = false;
         this.playershiprotation = this.shiptype[_loc2_][3][2];
         this.playershipacceleration = this.shiptype[_loc2_][3][0];
         this.playershipstatus[2][4] = this.shiptype[_loc2_][3][3];
         this.playershipstatus[0] = new Array();
         var _loc3_:* = 0;
         while(_loc3_ < this.shiptype[_loc2_][2].length)
         {
            this.playershipstatus[0][_loc3_] = new Array();
            this.playershipstatus[0][_loc3_][0] = this.extraplayerships[param1][4][_loc3_];
            this.playershipstatus[0][_loc3_][1] = 0;
            this.playershipstatus[0][_loc3_][2] = this.shiptype[_loc2_][2][_loc3_][0];
            this.playershipstatus[0][_loc3_][3] = this.shiptype[_loc2_][2][_loc3_][1];
            _loc3_++;
         }
         var _loc4_:* = 0;
         this.playershipstatus[8] = new Array();
         while(_loc4_ < this.shiptype[_loc2_][5].length)
         {
            this.playershipstatus[8][_loc4_] = new Array();
            this.playershipstatus[8][_loc4_][0] = this.extraplayerships[param1][5][_loc4_];
            _loc4_++;
         }
         this.playershipstatus[11] = new Array();
         this.playershipstatus[11][0] = new Array();
         this.playershipstatus[11][0][0] = 0;
         this.playershipstatus[11][0][1] = 0;
         this.playershipstatus[11][1] = new Array();
         this.playershipstatus[11][2] = new Array();
         if(this.extraplayerships[param1][11] == null)
         {
            this.extraplayerships[param1][11] = new Array();
         }
         if(this.extraplayerships[param1][11][1] == null)
         {
            this.extraplayerships[param1][11][1] = new Array();
         }
         var _loc5_:* = 0;
         while(_loc5_ < this.extraplayerships[param1][11][1].length)
         {
            this.playershipstatus[11][1][_loc5_] = new Array();
            this.playershipstatus[11][1][_loc5_][0] = this.extraplayerships[param1][11][1][_loc5_][0];
            this.playershipstatus[11][1][_loc5_][1] = this.extraplayerships[param1][11][1][_loc5_][1];
            _loc5_++;
         }
         this.playershipstatus[2][0] = this.extraplayerships[param1][1];
         this.playershipstatus[1][0] = this.extraplayerships[param1][2];
         this.playershipstatus[1][5] = this.extraplayerships[param1][3];
      }
      
      public function func_Missilewasfired(param1:*) : *
      {
         param1.splice(4,2);
         var _loc2_:* = Number(param1[6]);
         if(this.othermissilefire.length < 1)
         {
            this.othermissilefire = new Array();
         }
         ++this.currentothermissileshot;
         if(this.currentothermissileshot >= 1000)
         {
            this.currentothermissileshot = 1;
         }
         var _loc3_:* = String(getTimer() + this.clocktimediff);
         _loc3_ = Number(_loc3_.substr(Number(_loc3_.length) - 4));
         var _loc4_:*;
         if((_loc4_ = _loc3_ - Number(param1[8])) < -1000)
         {
            _loc4_ += 10000;
         }
         else if(_loc4_ > 10000)
         {
            _loc4_ -= 10000;
         }
         var _loc5_:* = this.othermissilefire.length;
         this.othermissilefire[_loc5_] = new Array();
         this.othermissilefire[_loc5_][0] = param1[1];
         this.othermissilefire[_loc5_][1] = Number(param1[2]);
         this.othermissilefire[_loc5_][2] = Number(param1[3]);
         var _loc6_:* = int(param1[4]);
         this.othermissilefire[_loc5_][6] = int(param1[5]);
         var _loc7_:* = Number(param1[5]);
         var _loc8_:* = this.MovingObjectWithThrust(_loc7_,_loc6_);
         this.othermissilefire[_loc5_][3] = _loc8_[0];
         this.othermissilefire[_loc5_][4] = _loc8_[1];
         this.othermissilefire[_loc5_][1] += Number(this.othermissilefire[_loc5_][3]) * _loc4_ * 0.001;
         this.othermissilefire[_loc5_][2] += Number(this.othermissilefire[_loc5_][4]) * _loc4_ * 0.001;
         this.othermissilefire[_loc5_][5] = getTimer() + this.missile[int(param1[6])][2];
         this.othermissilefire[_loc5_][7] = int(param1[6]);
         this.othermissilefire[_loc5_][8] = _loc6_;
         this.othermissilefire[_loc5_][9] = int(param1[7]);
         this.othermissilefire[_loc5_][10] = "other";
         this.othermissilefire[_loc5_][16] = new this.missile[this.othermissilefire[_loc5_][7]][10]() as MovieClip;
         this.gamedisplayarea.addChild(this.othermissilefire[_loc5_][16]);
         this.othermissilefire[_loc5_][16].x = Number(this.othermissilefire[_loc5_][1]) - this.shipcoordinatex;
         this.othermissilefire[_loc5_][16].y = Number(this.othermissilefire[_loc5_][2]) - this.shipcoordinatey;
         this.othermissilefire[_loc5_][16].rotation = this.othermissilefire[_loc5_][6];
         this.othermissilefire[_loc5_][16].gotoAndStop(1);
         this.othermissilefire[_loc5_][11] = Number(this.othermissilefire[_loc5_][16].width) / 2;
         this.othermissilefire[_loc5_][12] = Number(this.othermissilefire[_loc5_][16].height) / 2;
         this.othermissilefire[_loc5_][13] = "NOTSEEKING";
         if(this.missile[_loc2_][8] == "SEEKER" || this.missile[_loc2_][8] == "EMP" || this.missile[_loc2_][8] == "DISRUPTER")
         {
            if(this.playershipstatus[3][0] == Number(param1[9]))
            {
               this.othermissilefire[_loc5_][13] = "SEEKING";
            }
         }
         this.othermissilefire[_loc5_][14] = this.missile[this.othermissilefire[_loc5_][7]][1];
         this.othermissilefire[_loc5_][15] = "";
         this.othermissilefire[_loc5_][18] = Number(param1[2]);
         this.othermissilefire[_loc5_][19] = Number(param1[3]);
      }
      
      public function savecurrenttoextraship(param1:*) : *
      {
         this.extraplayerships[param1][0] = this.playershipstatus[5][0];
         var _loc2_:* = this.playershipstatus[5][0];
         this.extraplayerships[param1][4] = new Array();
         var _loc3_:* = 0;
         while(_loc3_ < this.shiptype[_loc2_][2].length)
         {
            this.extraplayerships[param1][4][_loc3_] = this.playershipstatus[0][_loc3_][0];
            _loc3_++;
         }
         var _loc4_:* = 0;
         this.extraplayerships[param1][5] = new Array();
         while(_loc4_ < this.shiptype[_loc2_][5].length)
         {
            this.extraplayerships[param1][5][_loc4_] = this.playershipstatus[8][_loc4_][0];
            _loc4_++;
         }
         var _loc5_:* = 0;
         this.extraplayerships[param1][11] = new Array();
         this.extraplayerships[param1][11][1] = new Array();
         while(_loc5_ < this.playershipstatus[11][1].length)
         {
            this.extraplayerships[param1][11][1][_loc5_] = new Array();
            this.extraplayerships[param1][11][1][_loc5_][0] = this.playershipstatus[11][1][_loc5_][0];
            this.extraplayerships[param1][11][1][_loc5_][1] = this.playershipstatus[11][1][_loc5_][1];
            _loc5_++;
         }
         this.extraplayerships[param1][1] = this.playershipstatus[2][0];
         this.extraplayerships[param1][2] = this.playershipstatus[1][0];
         this.extraplayerships[param1][3] = this.playershipstatus[1][5];
      }
      
      public function func_GameErrorReload(param1:MouseEvent) : void
      {
         gotoAndPlay(1);
      }
      
      public function onClose(param1:*) : void
      {
         this.IsSocketConnected = false;
         trace("Trace : Connection Close");
         gotoAndStop("gameclose");
      }
      
      public function func_statuscheck(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         if(param1[1] == "TGT")
         {
            if(param1[2] == "GET")
            {
               this.func_sendOutStats(param1[3]);
            }
            else if(param1[2] == "INFO")
            {
               _loc2_ = Number(param1[3]);
               if(this.playershipstatus[3][0] != _loc2_)
               {
                  _loc3_ = 0;
                  while(_loc3_ < this.otherplayership.length)
                  {
                     if(this.otherplayership[_loc3_][0] == _loc2_)
                     {
                        this.otherplayership[_loc3_][40] = Number(param1[8]);
                        this.otherplayership[_loc3_][41] = Number(param1[7]);
                        this.otherplayership[_loc3_][42] = Number(param1[4]);
                        this.otherplayership[_loc3_][43] = Number(param1[5]);
                        this.otherplayership[_loc3_][47] = Number(param1[6]);
                     }
                     _loc3_++;
                  }
               }
            }
         }
      }
      
      public function func_TurretControlScript(param1:*) : *
      {
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc2_:* = "";
         _loc3_ = 0;
         _loc4_ = 0;
         _loc5_ = 0;
         _loc6_ = 0;
         if(this.playershipstatus[9] == "MANUAL")
         {
            this.turretCrosshairs.x = mouseX;
            this.turretCrosshairs.y = mouseY;
            _loc4_ = mouseX - this.gamedisplayarea.x;
            _loc5_ = mouseY - this.gamedisplayarea.y;
            if((_loc6_ = Math.sqrt(_loc4_ * _loc4_ + _loc5_ * _loc5_)) < 300)
            {
               this.turretCrosshairs.visible = true;
               if(this.TurretMouseDown)
               {
                  this.turretCrosshairs.gotoAndStop(2);
                  _loc3_ = 180 - Math.atan2(_loc4_,_loc5_) / (Math.PI / 180);
                  this.func_fireTurretsAt(_loc3_,param1);
               }
               else
               {
                  this.turretCrosshairs.gotoAndStop(1);
               }
            }
            else
            {
               this.turretCrosshairs.visible = false;
               this.TurretMouseDown = false;
            }
         }
         if(this.playershipstatus[9] == "AUTO")
         {
            if(this.targetinfo[4] < 4000)
            {
               if(this.targetinfo[4] != this.playershipstatus[3][0])
               {
                  _loc7_ = this.targetinfo[5];
                  if(this.otherplayership.length > _loc7_)
                  {
                     if(this.otherplayership[_loc7_][0] != this.playershipstatus[3][0])
                     {
                        _loc4_ = this.otherplayership[_loc7_][21].x;
                        _loc5_ = this.otherplayership[_loc7_][21].y;
                        if((_loc6_ = Math.sqrt(_loc4_ * _loc4_ + _loc5_ * _loc5_)) < 300)
                        {
                           _loc3_ = 180 - Math.atan2(_loc4_,_loc5_) / (Math.PI / 180);
                           this.func_fireTurretsAt(_loc3_,param1);
                        }
                     }
                  }
               }
            }
         }
      }
      
      public function func_HangarButton_Click(param1:MouseEvent) : void
      {
         gotoAndStop("Hangar");
      }
      
      public function func_fire_a_missile(param1:*) : *
      {
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         var _loc15_:* = undefined;
         var _loc16_:* = undefined;
         var _loc17_:* = undefined;
         var _loc18_:* = undefined;
         var _loc19_:* = undefined;
         var _loc20_:* = undefined;
         var _loc21_:* = undefined;
         var _loc22_:* = undefined;
         var _loc23_:* = undefined;
         var _loc2_:* = false;
         var _loc3_:* = 1;
         var _loc4_:* = param1;
         var _loc5_:* = this.playershipstatus[7][_loc4_][0];
         if(this.playershipstatus[7][_loc4_][4][_loc5_] > 0)
         {
            _loc6_ = getTimer();
            if(this.playerSpecialsSettings.isCloaked)
            {
               this.func_turnofshipcloak();
            }
            if(_loc2_)
            {
               this.playershipstatus[7][_loc4_][1] = _loc6_ + this.missile[_loc5_][3];
               _loc7_ = _loc6_ + Number(this.missile[_loc5_][3]) * _loc3_;
            }
            else
            {
               _loc7_ = _loc6_ + this.missile[_loc5_][3];
            }
            if((_loc8_ = this.playershotsfired.length) < 1)
            {
               this.playershotsfired = new Array();
               _loc8_ = 0;
            }
            _loc9_ = _loc5_;
            _loc10_ = this.missile[_loc5_][0];
            _loc11_ = this.PlayersShipImage.rotation;
            _loc12_ = _loc10_ + this.playershipvelocity;
            _loc14_ = (_loc13_ = this.MovingObjectWithThrust(_loc11_,_loc12_))[0];
            _loc15_ = _loc13_[1];
            _loc16_ = String(getTimer() + this.clocktimediff);
            if((_loc16_ = Number(_loc16_.substr(Number(_loc16_.length) - 4))) > 10000)
            {
               _loc16_ -= 10000;
            }
            if(this.currentplayershotsfired > 999)
            {
               this.currentplayershotsfired = 0;
            }
            _loc17_ = this.playershipstatus[7][_loc4_][2];
            _loc18_ = this.playershipstatus[7][_loc4_][3];
            _loc17_ = (_loc19_ = this.firingbulletstartlocation(_loc17_,_loc18_,_loc11_))[0] + this.shipcoordinatex;
            _loc18_ = _loc19_[1] + this.shipcoordinatey;
            _loc20_ = true;
            _loc21_ = false;
            if(this.missile[_loc5_][8] == "SEEKER" || this.missile[_loc5_][8] == "EMP" || this.missile[_loc5_][8] == "DISRUPTER")
            {
               _loc21_ = true;
               _loc20_ = false;
               if(this.targetinfo[4] < 4000)
               {
                  if(this.targetinfo[4] != this.playershipstatus[3][0])
                  {
                     _loc20_ = true;
                     _loc20_ = false;
                  }
                  else
                  {
                     _loc20_ = false;
                  }
               }
            }
            if(_loc20_)
            {
               --this.playershipstatus[7][_loc4_][4][_loc5_];
               if(this.currentplayershotsfired > 998)
               {
                  this.currentplayershotsfired = 0;
               }
               _loc22_ = this.currentplayershotsfired;
               ++this.currentplayershotsfired;
               this.playershotsfired[_loc8_] = new Array();
               this.playershotsfired[_loc8_][0] = _loc22_;
               this.playershotsfired[_loc8_][1] = Math.round(_loc17_);
               this.playershotsfired[_loc8_][2] = Math.round(_loc18_);
               this.playershotsfired[_loc8_][3] = _loc14_;
               this.playershotsfired[_loc8_][4] = _loc15_;
               this.playershotsfired[_loc8_][5] = _loc6_ + this.missile[_loc5_][2];
               this.playershotsfired[_loc8_][6] = _loc5_;
               this.playershotsfired[_loc8_][7] = _loc11_;
               this.playershotsfired[_loc8_][8] = _loc6_ + this.missile[_loc5_][3];
               this.playershotsfired[_loc8_][9] = new this.missile[_loc5_][10]() as MovieClip;
               this.gamedisplayarea.addChild(this.playershotsfired[_loc8_][9]);
               this.playershotsfired[_loc8_][9].x = Number(this.playershotsfired[_loc8_][1]) - this.shipcoordinatex;
               this.playershotsfired[_loc8_][9].y = Number(this.playershotsfired[_loc8_][2]) - this.shipcoordinatey;
               this.playershotsfired[_loc8_][9].rotation = this.playershotsfired[_loc8_][7];
               this.playershotsfired[_loc8_][9].gotoAndStop(1);
               _loc23_ = (_loc23_ = (_loc23_ = (_loc23_ = (_loc23_ = (_loc23_ = (_loc23_ = (_loc23_ = (_loc23_ = (_loc23_ = "MF" + "`" + this.playershipstatus[3][0] + "`") + (this.playershotsfired[_loc8_][1] + "`")) + (this.playershotsfired[_loc8_][2] + "`")) + (Math.round(this.playershotsfired[_loc8_][1] + _loc14_ * this.shipositiondelay * 0.001) + "`")) + (Math.round(this.playershotsfired[_loc8_][2] + _loc15_ * this.shipositiondelay * 0.001) + "`")) + (Math.round(_loc12_) + "`")) + (_loc11_ + "`")) + (_loc5_ + "`")) + (_loc22_ + "`")) + _loc16_;
               if(_loc21_)
               {
                  _loc23_ += "`" + this.otherplayership[this.targetinfo[8]][0] + "`";
               }
               _loc23_ += "~";
               this.gunShotBufferData += _loc23_;
               this.playershotsfired[_loc8_][13] = "MISSILE";
               this.playershotsfired[_loc8_][14] = this.missile[_loc5_][1];
               this.playershotsfired[_loc8_][15] = "";
               this.playershipstatus[7][_loc4_][1] = _loc7_;
            }
         }
      }
      
      public function func_PingTimer_lastpingCame(param1:*) : *
      {
         var _loc2_:* = getTimer();
         this.gameplaystatus[1][0] = _loc2_ - param1;
         var _loc3_:* = this.shiplag;
         var _loc4_:* = "Ping: " + this.gameplaystatus[1][0] + "ms Current";
         if(this.gameplaystatus != null)
         {
            if(this.gameplaystatus[1][0] != null)
            {
            }
         }
         if(this.shiplag > 250)
         {
         }
         if(this.remoteupdate)
         {
            if(this.shiplag < _loc3_)
            {
            }
         }
         else if(_loc3_ - this.shiplag > 750)
         {
         }
         this.lastpingcheck = _loc2_ + this.pingintervalcheck;
         this.GamePingTimer.text = _loc4_;
      }
      
      public function func_lifestats(param1:*) : *
      {
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc2_:* = int(param1[1]);
         var _loc3_:* = Number(param1[2]);
         var _loc4_:* = Number(param1[3]);
         if(_loc2_ >= 4000)
         {
            if(_loc2_ < 5000)
            {
               _loc5_ = _loc2_ - 4000;
               this.teambases[_loc5_][10] = _loc3_;
               this.teambases[_loc5_][11] = _loc4_;
               this.func_updateTameBaseHealthBars(_loc5_);
            }
         }
         else
         {
            _loc6_ = this.otherplayership.length;
            _loc7_ = 0;
            while(_loc7_ < _loc6_)
            {
               if(this.otherplayership[_loc7_][0] == _loc2_)
               {
                  this.otherplayership[_loc7_][41] = _loc4_;
                  this.otherplayership[_loc7_][40] = _loc3_;
                  break;
               }
               _loc7_++;
            }
         }
      }
      
      public function func_refreshCurrentOnlineList() : *
      {
         this.OnlinePLayerList.visible = true;
         this.DisplayOnlinePlayersList();
      }
      
      public function ProcessOtherShipKeys(param1:*) : *
      {
         var _loc3_:* = undefined;
         var _loc2_:* = 0;
         while(_loc2_ < this.otherplayership.length)
         {
            _loc3_ = 0;
            while(_loc3_ < this.otherplayership[_loc2_][30].length)
            {
               if(this.otherplayership[_loc2_][30][_loc3_] == null)
               {
                  break;
               }
               if(this.otherplayership[_loc2_][30][_loc3_ + 1] != null && this.otherplayership[_loc2_][30][_loc3_ + 1][9] < param1)
               {
                  this.otherplayership[_loc2_][30].splice(_loc3_,1);
                  _loc3_--;
               }
               else
               {
                  if(this.otherplayership[_loc2_][30][_loc3_][9] >= param1)
                  {
                     break;
                  }
                  if(this.otherplayership[_loc2_][30][_loc3_][8] > this.otherplayership[_loc2_][5][8] || this.otherplayership[_loc2_][5][8] > 94)
                  {
                     if(Math.abs(Number(this.otherplayership[_loc2_][37]) - Number(this.otherplayership[_loc2_][30][_loc3_][0])) < 75 && Math.abs(Number(this.otherplayership[_loc2_][38]) - Number(this.otherplayership[_loc2_][30][_loc3_][1])) < 75)
                     {
                        this.otherplayership[_loc2_][36] = 4;
                     }
                     this.otherplayership[_loc2_][1] = this.otherplayership[_loc2_][30][_loc3_][0];
                     this.otherplayership[_loc2_][2] = this.otherplayership[_loc2_][30][_loc3_][1];
                     this.otherplayership[_loc2_][3] = this.otherplayership[_loc2_][30][_loc3_][2];
                     this.otherplayership[_loc2_][4] = this.otherplayership[_loc2_][30][_loc3_][3];
                     this.otherplayership[_loc2_][5] = new Array();
                     this.otherplayership[_loc2_][5][0] = this.otherplayership[_loc2_][30][_loc3_][6][0];
                     this.otherplayership[_loc2_][5][1] = this.otherplayership[_loc2_][30][_loc3_][6][1];
                     this.otherplayership[_loc2_][5][8] = this.otherplayership[_loc2_][30][_loc3_][8];
                     this.otherplayership[_loc2_][5][9] = this.otherplayership[_loc2_][30][_loc3_][9];
                  }
                  this.otherplayership[_loc2_][30].splice(_loc3_,1);
                  _loc3_--;
               }
               _loc3_++;
            }
            _loc2_++;
         }
      }
      
      public function loadplayerdata(param1:*) : *
      {
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         var _loc15_:* = undefined;
         var _loc16_:* = undefined;
         var _loc17_:* = undefined;
         var _loc18_:* = undefined;
         var _loc2_:* = "";
         var _loc3_:* = param1.split("~");
         var _loc4_:* = 0;
         while(_loc4_ < _loc3_.length - 1)
         {
            if(_loc3_[_loc4_].substr(0,2) == "PI")
            {
               if((_loc5_ = _loc3_[_loc4_].split("`"))[1].substr(0,2) == "ST")
               {
                  this.playershipstatus[5][0] = int(_loc5_[1].substr("2"));
                  this.playershipstatus[2][0] = int(_loc5_[2].substr("2"));
                  this.playershipstatus[1][0] = int(_loc5_[3].substr("2"));
                  this.playershipstatus[1][5] = int(_loc5_[4].substr("2"));
                  this.playershipstatus[3][1] = Number(_loc5_[5].substr("2"));
                  this.playershipstatus[5][1] = _loc5_[6].substr("2");
                  this.playershipstatus[4][0] = _loc5_[7];
                  _loc6_ = 8;
                  this.playershipstatus[0] = new Array();
                  this.playershipstatus[8] = new Array();
                  this.playershipstatus[11][1] = new Array();
                  _loc7_ = 0;
                  this.initializemissilebanks();
                  while(_loc6_ < _loc5_.length - 1)
                  {
                     _loc8_ = _loc6_;
                     if(_loc5_[_loc6_].substr(0,2) == "HP")
                     {
                        _loc10_ = (_loc9_ = _loc5_[_loc6_].split("G"))[0].substr("2");
                        this.playershipstatus[0][_loc10_] = new Array();
                        if(isNaN(_loc9_[1]))
                        {
                           _loc9_[1] = "none";
                        }
                        this.playershipstatus[0][_loc10_][0] = _loc9_[1];
                        _loc6_++;
                     }
                     else if(_loc5_[_loc6_].substr(0,2) == "TT")
                     {
                        _loc11_ = (_loc9_ = _loc5_[_loc6_].split("G"))[0].substr("2");
                        this.playershipstatus[8][_loc11_] = new Array();
                        if(isNaN(_loc9_[1]))
                        {
                           _loc9_[1] = "none";
                        }
                        this.playershipstatus[8][_loc11_][0] = _loc9_[1];
                        _loc6_++;
                     }
                     else if(_loc5_[_loc6_].substr(0,2) == "CO")
                     {
                        _loc13_ = (_loc12_ = _loc5_[_loc6_].split("A"))[0].substr("2");
                        this.playershipstatus[4][1][_loc13_] = int(_loc12_[1]);
                        _loc6_++;
                     }
                     else if(_loc5_[_loc6_].substr(0,2) == "SP")
                     {
                        _loc12_ = _loc5_[_loc6_].split("Q");
                        trace(_loc12_);
                        _loc7_ = this.playershipstatus[11][1].length;
                        this.playershipstatus[11][1][_loc7_] = new Array();
                        this.playershipstatus[11][1][_loc7_][0] = Number(_loc12_[0].substr("2"));
                        this.playershipstatus[11][1][_loc7_][1] = Number(_loc12_[1]);
                        _loc6_++;
                     }
                     else if(_loc5_[_loc6_].substr(0,2) == "MB")
                     {
                        _loc14_ = _loc5_[_loc6_].split("T");
                        _loc15_ = Number(_loc14_[0].substr(2));
                        _loc16_ = _loc14_[1].split("Q");
                        _loc17_ = Number(_loc16_[0]);
                        _loc18_ = Number(_loc16_[1]);
                        this.playershipstatus[7][_loc15_][4][_loc17_] = _loc18_;
                        if(this.playershipstatus[7][_loc15_][4][_loc17_] > this.playershipstatus[7][_loc15_][5])
                        {
                        }
                        _loc6_++;
                     }
                     else
                     {
                        _loc6_++;
                     }
                  }
                  trace("B");
               }
            }
            if(_loc3_[_loc4_].substr(0,5) == "score")
            {
               _loc5_ = _loc3_[_loc4_].split("`");
               this.playershipstatus[5][9] = Number(_loc5_[1]);
               if(isNaN(this.playershipstatus[5][9]))
               {
                  this.playershipstatus[5][9] = Number(0);
               }
               this.playersSessionScoreStart = this.playershipstatus[5][9];
            }
            if(_loc3_[_loc4_].substr(0,2) == "NO")
            {
               _loc2_ += _loc3_[_loc4_] + "~";
            }
            if(_loc3_[_loc4_].substr(0,2) == "SH")
            {
               _loc2_ += _loc3_[_loc4_] + "~";
            }
            if(_loc3_[_loc4_].substr(0,3) == "bty")
            {
               _loc5_ = _loc3_[_loc4_].split("`");
               this.playershipstatus[5][8] = Number(_loc5_[1]);
               if(this.playershipstatus[5][8] < 0)
               {
                  this.playershipstatus[5][8] = 0;
               }
            }
            if(_loc3_[_loc4_].substr(0,5) == "squad")
            {
               _loc5_ = _loc3_[_loc4_].split("`");
               this.playershipstatus[5][10] = _loc5_[1];
               this.playershipstatus[5][13] = _loc5_[3];
               this.playershipstatus[5][11] = false;
               if(_loc5_[2] == this.playershipstatus[3][2])
               {
                  this.playershipstatus[5][11] = true;
               }
            }
            if(_loc3_[_loc4_].substr(0,4) == "fund")
            {
               _loc5_ = _loc3_[_loc4_].split("`");
               this.playerfunds = Number(_loc5_[1]);
               if(this.playerfunds != 0)
               {
                  this.playershipstatus[3][1] = this.playerfunds;
               }
            }
            if(_loc3_[_loc4_].substr(0,2) == "AD")
            {
               _loc5_ = _loc3_[_loc4_].split("`");
               this.playershipstatus[5][12] = _loc5_[1].toUpperCase();
            }
            if(_loc3_[_loc4_].substr(0,3) == "SAF")
            {
               _loc5_ = _loc3_[_loc4_].split("`");
               this.playershipstatus[5][25] = "YES";
            }
            if(_loc3_[_loc4_].substr(0,6) == "BANNED")
            {
               _loc5_ = _loc3_[_loc4_].split("`");
               this.gameerror = "banned";
               this.timebannedfor = _loc5_[1];
               gotoAndStop("gameclose");
            }
            if(_loc3_[_loc4_].substr(0,2) == "EM")
            {
               _loc5_ = _loc3_[_loc4_].split("`");
               this.playershipstatus[3][5] = String(_loc5_[1]);
            }
            if(_loc3_[_loc4_].substr(0,4) == "RACE")
            {
               _loc5_ = _loc3_[_loc4_].split("`");
               this.playershipstatus[5][26] = int(_loc5_[1]);
            }
            _loc4_++;
         }
         this.loadplayerextraships(_loc2_);
      }
      
      public function myhittest(param1:*, param2:*, param3:*, param4:*, param5:*, param6:*) : *
      {
         if(param3 - param5 > param1)
         {
            return false;
         }
         if(param3 + param5 < -param1)
         {
            return false;
         }
         if(param4 - param6 > param2)
         {
            return false;
         }
         if(param4 + param6 < -param2)
         {
            return false;
         }
         return true;
      }
      
      public function func_addshipintoclient(param1:*) : *
      {
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc2_:* = int(param1[1]);
         this.shiptype[_loc2_] = new Array();
         this.shiptype[_loc2_][0] = param1[2];
         this.shiptype[_loc2_][1] = Number(param1[3]);
         this.shiptype[_loc2_][2] = new Array();
         var _loc3_:* = param1[23].split(",");
         var _loc4_:* = 0;
         while(_loc4_ < _loc3_.length - 1)
         {
            _loc7_ = _loc3_[_loc4_].split("=");
            this.shiptype[_loc2_][2][_loc4_] = new Array();
            this.shiptype[_loc2_][2][_loc4_][0] = Number(_loc7_[0]);
            this.shiptype[_loc2_][2][_loc4_][1] = Number(_loc7_[1]);
            _loc4_++;
         }
         this.shiptype[_loc2_][3] = new Array();
         this.shiptype[_loc2_][3][0] = Number(param1[4]);
         this.shiptype[_loc2_][3][1] = Number(param1[5]);
         this.shiptype[_loc2_][3][2] = Number(param1[6]);
         this.shiptype[_loc2_][3][3] = Number(param1[7]);
         this.shiptype[_loc2_][3][4] = Number(param1[8]);
         this.shiptype[_loc2_][3][5] = Number(param1[9]) / 100;
         this.shiptype[_loc2_][3][6] = Number(param1[10]);
         this.shiptype[_loc2_][3][7] = Number(param1[11]);
         if(param1[12] == "true")
         {
            this.shiptype[_loc2_][3][8] = true;
         }
         else
         {
            this.shiptype[_loc2_][3][8] = false;
         }
         if(param1[13] == "true")
         {
            this.shiptype[_loc2_][3][10] = true;
         }
         else
         {
            this.shiptype[_loc2_][3][10] = false;
         }
         if(param1[14] == "true")
         {
            this.shiptype[_loc2_][3][11] = true;
         }
         else
         {
            this.shiptype[_loc2_][3][11] = false;
         }
         if(param1[15] == "true")
         {
            this.shiptype[_loc2_][3][12] = true;
         }
         else
         {
            this.shiptype[_loc2_][3][12] = false;
         }
         if(param1[16] == "true")
         {
            this.shiptype[_loc2_][3][13] = true;
         }
         else
         {
            this.shiptype[_loc2_][3][13] = false;
         }
         this.shiptype[_loc2_][4] = Number(param1[17]);
         this.shiptype[_loc2_][5] = new Array();
         var _loc5_:* = param1[25].split(",");
         _loc4_ = 0;
         while(_loc4_ < _loc5_.length - 1)
         {
            _loc8_ = _loc5_[_loc4_].split("=");
            this.shiptype[_loc2_][5][_loc4_] = new Array();
            this.shiptype[_loc2_][5][_loc4_][0] = Number(_loc8_[0]);
            this.shiptype[_loc2_][5][_loc4_][1] = Number(_loc8_[1]);
            _loc4_++;
         }
         this.shiptype[_loc2_][6] = new Array();
         this.shiptype[_loc2_][6][0] = Number(param1[18]);
         this.shiptype[_loc2_][6][1] = Number(param1[19]);
         this.shiptype[_loc2_][6][2] = Number(param1[20]);
         this.shiptype[_loc2_][6][3] = Number(param1[21]);
         this.shiptype[_loc2_][6][4] = Number(param1[22]);
         this.shiptype[_loc2_][7] = new Array();
         var _loc6_:* = param1[24].split(",");
         _loc4_ = 0;
         while(_loc4_ < _loc6_.length - 1)
         {
            _loc9_ = _loc6_[_loc4_].split("=");
            this.shiptype[_loc2_][7][_loc4_] = new Array();
            this.shiptype[_loc2_][7][_loc4_][2] = Number(_loc9_[0]);
            this.shiptype[_loc2_][7][_loc4_][3] = Number(_loc9_[1]);
            this.shiptype[_loc2_][7][_loc4_][5] = Number(_loc9_[2]);
            _loc4_++;
         }
         this.shiptype[_loc2_][10] = getDefinitionByName("shiptype" + _loc2_) as Class;
         this.shiptype[_loc2_][11] = getDefinitionByName("shiptype" + _loc2_ + "shield") as Class;
      }
      
      public function autoTurretsSelected(param1:MouseEvent) : void
      {
         this.func_setTurrets("auto");
      }
      
      public function func_processgameinfo(param1:*) : *
      {
         var _loc2_:* = param1.split("~");
         var _loc3_:* = 1;
         while(_loc3_ < _loc2_.length - 1)
         {
            if(_loc2_[_loc3_].substr(0,4) == "SHIP")
            {
               this.func_addshipintoclient(_loc2_[_loc3_].split("`"));
            }
            else if(_loc2_[_loc3_].substr(0,6) == "GNTYPE")
            {
               this.func_addgunintocliet(_loc2_[_loc3_].split("`"));
            }
            else if(_loc2_[_loc3_].substr(0,7) == "SHLDGEN")
            {
               this.func_addshieldgenintocliet(_loc2_[_loc3_].split("`"));
            }
            _loc3_++;
         }
         this.loginmovie.gotoAndPlay("start");
      }
      
      public function pingTimerScript(param1:Event) : *
      {
         var _loc3_:* = undefined;
         var _loc2_:* = getTimer();
         if(this.IsSocketConnected)
         {
            if(_loc2_ > this.lastpingcheck)
            {
               this.lastpingcheck += this.pingintervalcheck;
               _loc3_ = "PING`" + _loc2_ + "~";
               this.mysocket.send(_loc3_);
               this.remoteupdate = false;
            }
         }
      }
      
      public function MapTimerHandler(param1:TimerEvent) : void
      {
         this.playersdestination[0] = this.gameMap.func_PlayerMapLocation(this.shipcoordinatex,this.shipcoordinatey,this.AnglePlayerShipFacing);
         this.func_setHeadingLocation();
      }
      
      public function func_processMissileHit(param1:*) : *
      {
         var playerwhogothit:*;
         var shooterofbullet:*;
         var shooternumberfire:*;
         var basethatgothit:* = undefined;
         var shotdamage:* = undefined;
         var cc:* = undefined;
         var currentnumber:* = undefined;
         var currentthread:* = param1;
         trace(currentthread);
         playerwhogothit = currentthread[1];
         shooterofbullet = currentthread[2];
         shooternumberfire = currentthread[3];
         if(playerwhogothit != "SQ")
         {
            if(Number(playerwhogothit) < 5000 && Number(playerwhogothit) > 3000)
            {
               basethatgothit = playerwhogothit - 4000;
               basethatgothit = this.teambases[basethatgothit][0];
               shotdamage = 0;
               if(this.playershipstatus[3][0] != shooterofbullet)
               {
                  cc = 0;
                  while(cc < this.othermissilefire.length)
                  {
                     if(this.othermissilefire[cc][0] == shooterofbullet && this.othermissilefire[cc][9] == shooternumberfire)
                     {
                        try
                        {
                           this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                        }
                        catch(error:Error)
                        {
                           trace("Error removing otherplayershot - basehit: " + error);
                        }
                        this.othermissilefire.splice(cc,1);
                        break;
                     }
                     cc++;
                  }
               }
               else if(this.playershipstatus[3][0] == shooterofbullet)
               {
                  trace("Hit Base Detected");
                  currentnumber = 0;
                  while(currentnumber < this.playershotsfired.length)
                  {
                     if(this.playershotsfired[currentnumber][0] == shooternumberfire)
                     {
                        try
                        {
                           this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                        }
                        catch(error:Error)
                        {
                           trace("Error removing playershot - gunhit: " + error);
                        }
                        this.playershotsfired.splice(currentnumber,1);
                        trace("Removed Shot");
                        break;
                     }
                     currentnumber++;
                  }
               }
            }
            else if(playerwhogothit != "MET")
            {
               if(playerwhogothit == "AI")
               {
                  shooterofbullet = currentthread[3];
                  shooternumberfire = Number(currentthread[4]);
               }
               if(this.playershipstatus[3][0] != shooterofbullet)
               {
                  cc = 0;
                  while(cc < this.othermissilefire.length)
                  {
                     if(this.othermissilefire[cc][0] == shooterofbullet && this.othermissilefire[cc][9] == shooternumberfire)
                     {
                        try
                        {
                           this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                        }
                        catch(error:Error)
                        {
                           trace("Error removing otherplayershot - MIssileHitonBase: " + error);
                        }
                        this.othermissilefire.splice(cc,1);
                        break;
                     }
                     cc++;
                  }
               }
               if(this.playershipstatus[3][0] == shooterofbullet)
               {
                  this.func_removePlayersShotFromGlobalID(shooternumberfire);
               }
            }
         }
      }
      
      public function dataprocess(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         var _loc15_:* = undefined;
         var _loc16_:* = undefined;
         var _loc17_:* = undefined;
         var _loc18_:* = undefined;
         var _loc19_:* = undefined;
         var _loc20_:* = undefined;
         var _loc21_:* = undefined;
         var _loc22_:* = undefined;
         var _loc23_:* = undefined;
         var _loc24_:* = undefined;
         var _loc25_:* = undefined;
         if(param1 != "")
         {
            if(param1.substr(0,4) != "PING")
            {
            }
         }
         _loc2_ = param1.split("~");
         _loc3_ = _loc2_.length - 1;
         _loc4_ = 0;
         while(_loc4_ < _loc3_)
         {
            if((_loc5_ = _loc2_[_loc4_].split("`"))[0] == "ACCT")
            {
               if(_loc5_[1] == "PILOAD")
               {
                  this.isplayeraguest = this.loginmovie.mov_login.isAGuest;
                  this.loadplayerdata(param1);
                  this.loginmovie.mov_login.func_loginaccepted();
                  break;
               }
               if(_loc5_[1] == "PINOLOAD")
               {
                  this.loginmovie.mov_login.func_failedLogin();
               }
               else if(_loc5_[1] == "nameexists")
               {
                  this.loginmovie.mov_login.func_NameExists();
               }
               else if(_loc5_[1] == "namecreated")
               {
                  this.loginmovie.mov_login.func_NameCreated();
               }
            }
            else if(_loc5_[0] == "JZONE")
            {
               this.loginmovie.mov_login.func_zoneconnectionbeenmade();
               this.mysocket.send("LOADZONESYSTEM`~");
            }
            else if(_loc5_[0] == "PI" && !isNaN(_loc5_[1]))
            {
               this.otherPilotProcess(_loc5_);
            }
            else if(_loc5_[0] == "GAMEINFO")
            {
               this.func_processgameinfo(param1);
            }
            else if(_loc5_[0] == "MI")
            {
               this.func_IncomingSeekerInfo(_loc5_);
            }
            else if(_loc5_[0] == "MF")
            {
               trace("MF DATA:" + _loc5_);
               this.func_Missilewasfired(_loc5_);
            }
            else if(_loc5_[0] == "GF")
            {
               this.func_AddOtherGunshot(_loc5_);
            }
            else if(_loc5_[0] == "GH")
            {
               this.func_ProcessGunHit(_loc5_);
            }
            else if(_loc5_[0] == "STATS")
            {
               this.func_statuscheck(_loc5_);
            }
            else if(_loc5_[0] == "SH")
            {
               this.func_bringinspecial(_loc5_);
            }
            else if(_loc5_[0] == "MH")
            {
               this.func_processMissileHit(_loc5_);
            }
            else if(_loc5_[0] == "DM")
            {
               this.func_funcDeathMatchProcc(_loc5_);
            }
            else if(_loc5_[0] == "CH")
            {
               this.func_IncomingChat(_loc5_);
            }
            else if(_loc5_[0] == "TO")
            {
               this.func_LogOutPlayer(_loc5_[1]);
            }
            else if(_loc5_[0] == "TC")
            {
               _loc6_ = _loc5_[1];
               if(_loc5_[2] == "DK")
               {
                  if(_loc6_ != this.playershipstatus[3][0])
                  {
                     this.func_DockOtherShip(_loc6_);
                  }
               }
               if(_loc5_[2] == "SC")
               {
                  _loc5_[3] = Number(_loc5_[3]);
                  if(isNaN(_loc5_[3]))
                  {
                     _loc5_[3] = 0;
                  }
                  _loc7_ = 0;
                  while(_loc7_ < this.currentonlineplayers.length)
                  {
                     if(_loc6_ == this.currentonlineplayers[_loc7_][0])
                     {
                        this.currentonlineplayers[_loc7_][2] = Number(_loc5_[3]);
                        break;
                     }
                     _loc7_++;
                  }
               }
               if(_loc5_[2] == "TM")
               {
                  if(_loc5_[3] == "-1")
                  {
                     _loc5_[3] = "N/A";
                  }
                  _loc8_ = 0;
                  while(_loc8_ < this.currentonlineplayers.length)
                  {
                     if(_loc6_ == this.currentonlineplayers[_loc8_][0])
                     {
                        this.currentonlineplayers[_loc8_][4] = _loc5_[3];
                        break;
                     }
                     _loc8_++;
                  }
                  if(this.playershipstatus[3][0] == _loc5_[1])
                  {
                     this.playershipstatus[5][2] = _loc5_[3];
                  }
                  this.func_refreshCurrentOnlineList();
               }
               else if(_loc5_[2] == "SCORE")
               {
                  _loc5_[3] = Number(_loc5_[3]);
                  if(isNaN(_loc5_[3]) || Number(_loc5_[3] < 0))
                  {
                     _loc5_[3] = 0;
                  }
                  _loc9_ = 0;
                  while(_loc9_ < this.currentonlineplayers.length)
                  {
                     if(_loc6_ == this.currentonlineplayers[_loc9_][0])
                     {
                        this.currentonlineplayers[_loc9_][5] = Number(this.currentonlineplayers[_loc9_][5]) + Number(_loc5_[3]);
                        break;
                     }
                     _loc9_++;
                  }
                  if(this.playershipstatus[3][0] == _loc5_[1])
                  {
                     this.playershipstatus[5][9] = Number(this.playershipstatus[5][9]) + Number(_loc5_[3]);
                     this.playersSessionScoreStart += Number(_loc5_[3]);
                  }
                  this.func_refreshCurrentOnlineList();
               }
               if(_loc5_[2] == "SD")
               {
                  _loc10_ = true;
                  if(String(_loc5_[1]) == String(999999))
                  {
                     _loc10_ = true;
                  }
                  else
                  {
                     _loc10_ = false;
                  }
                  this.func_KillOpponentsShip(_loc6_);
                  if(0 > Number(_loc5_[4]) || 10000000 < Number(_loc5_[4]))
                  {
                     _loc5_[4] = 0;
                  }
                  _loc11_ = 10;
                  _loc12_ = Number(_loc5_[4]) * _loc11_;
                  if((_loc12_ = Math.floor(_loc12_ * 2.5)) < 35000)
                  {
                     _loc12_ = 35000;
                  }
                  _loc13_ = false;
                  if(this.playershipstatus[3][0] == _loc5_[3])
                  {
                     _loc13_ = true;
                     this.playershipstatus[3][1] += _loc12_;
                     this.playershipstatus[5][8] = Number(this.playershipstatus[5][8]) + Math.round(Number(_loc5_[4]) * Number(this.playershipstatus[5][6]));
                     this.playershipstatus[5][9] = Number(this.playershipstatus[5][9]) + Number(_loc5_[4]);
                     if(_loc10_ != true)
                     {
                        this.playershipstatus[5][8] = Number(this.playershipstatus[5][8]) + Math.round(Number(_loc5_[4]) * Number(this.playershipstatus[5][6]));
                     }
                  }
                  _loc14_ = 0;
                  _loc15_ = false;
                  _loc16_ = String(_loc5_[3]);
                  _loc17_ = "AI";
                  while(_loc14_ < this.currentonlineplayers.length && _loc15_ == false)
                  {
                     if(_loc16_ == String(this.currentonlineplayers[_loc14_][0]))
                     {
                        this.currentonlineplayers[_loc14_][3] = Number(this.currentonlineplayers[_loc14_][3]) + Math.round(Number(_loc5_[4]) * Number(this.playershipstatus[5][6]));
                        if(_loc10_ != true)
                        {
                           this.currentonlineplayers[_loc14_][5] += Number(_loc5_[4]);
                        }
                        _loc17_ = this.currentonlineplayers[_loc14_][1];
                        _loc15_ = true;
                        _loc18_ = _loc14_;
                     }
                     _loc14_++;
                  }
                  _loc14_ = 0;
                  _loc15_ = false;
                  _loc19_ = String(_loc5_[1]);
                  if(String(this.otherplayerdockedon) == _loc19_)
                  {
                     this.otherplayerdockedon = null;
                  }
                  _loc20_ = "AI";
                  while(_loc14_ < this.currentonlineplayers.length && _loc15_ == false)
                  {
                     if(_loc19_ == String(this.currentonlineplayers[_loc14_][0]))
                     {
                        this.currentonlineplayers[_loc14_][3] = 0;
                        _loc20_ = this.currentonlineplayers[_loc14_][1];
                        _loc15_ = true;
                        _loc21_ = _loc14_;
                     }
                     _loc14_++;
                  }
                  _loc22_ = _loc20_ + " (" + _loc5_[4] + " BTY, " + _loc12_ + " Funds) killed by " + _loc17_;
                  this.func_AddChatter(_loc22_);
                  if(_loc13_ == true)
                  {
                  }
                  if(_loc10_ != true && _loc20_ != "AI")
                  {
                     this.currentonlineplayers[_loc18_][6] = Number(this.currentonlineplayers[_loc18_][6]) + 1;
                     this.currentonlineplayers[_loc21_][7] = Number(this.currentonlineplayers[_loc21_][7]) + 1;
                  }
                  this.func_refreshCurrentOnlineList();
               }
               else if(_loc5_[2] == "BC")
               {
                  _loc23_ = 0;
                  while(_loc14_ < this.currentonlineplayers.length)
                  {
                     if(_loc6_ == this.currentonlineplayers[_loc23_][0])
                     {
                        this.currentonlineplayers[_loc23_][3] = Number(_loc5_[3]);
                        break;
                     }
                     _loc23_++;
                  }
                  if(this.playershipstatus[3][0] == _loc6_)
                  {
                     this.playershipstatus[5][8] = Number(_loc5_[3]) - Number(this.playershipstatus[5][3]);
                  }
                  this.func_refreshCurrentOnlineList();
               }
            }
            else if(_loc5_[0] == "LFE")
            {
               this.func_lifestats(_loc5_);
            }
            else if(_loc5_[0] == "PING")
            {
               this.func_PingTimer_lastpingCame(Number(_loc5_[1]));
            }
            else if(_loc5_[0] == "LISTING")
            {
               this.loginmovie.mov_login.logindisplay.func_incominglisting(_loc5_);
            }
            else if(_loc5_[0] == "TI" && _loc5_[1] != this.playershipstatus[3][0])
            {
               this.func_otherPLayerLogin(_loc5_);
            }
            else if(_loc5_[0] == "LI")
            {
               this.playershipstatus[3][0] = _loc5_[1];
               this.playershipstatus[5][19] = _loc5_[2];
               gotoAndStop("dockedscreen");
               if(Number(this.playershipstatus[5][1]) < 300)
               {
                  _loc24_ = "TC" + "`" + this.playershipstatus[3][0] + "`TM`" + Math.round(Math.random() * 2) + "~";
                  this.mysocket.send(_loc24_);
               }
               this.mysocket.send("INITTRADE`LOAD`~");
               trace("loggedin");
            }
            else if(_loc5_[0] == "SGAME")
            {
               this.func_postsaveresults(_loc5_);
            }
            else if(_loc5_[0] == "SECTORLOADING")
            {
               this.func_LoadZonesSystemMap(param1);
               this.func_InitializeClockChecks();
            }
            else if(_loc5_[0] == "CLOCK")
            {
               this.func_ProcessClockTimer(_loc5_);
            }
            else if(_loc5_[0] == "ADMIN")
            {
               if(_loc5_[1] == "MUTE")
               {
                  if(this.gamechatinfo[2][2] == true)
                  {
                     this.gamechatinfo[2][2] = false;
                     this.func_enterintochat("HOST: You Are No Longer Muted",this.systemchattextcolor);
                  }
                  else
                  {
                     this.gamechatinfo[2][2] = true;
                     this.gamechatinfo[2][3] = getTimer() + 1000 * 60 * 15;
                     this.func_enterintochat("HOST: Muted for " + Math.ceil(Number(this.gamechatinfo[2][4]) / 60000) + " minuites by " + _loc5_[2],this.systemchattextcolor);
                  }
               }
               if(_loc5_[1] == "TRANSFER")
               {
                  _loc25_ = Number(_loc5_[3]);
                  if(!isNaN(_loc25_))
                  {
                     this.playershipstatus[3][1] = Number(_loc25_) + Number(this.playershipstatus[3][1]);
                     this.func_enterintochat("You Just Received " + _loc25_ + " credits from a mod",this.systemchattextcolor);
                  }
               }
            }
            else if(_loc5_[0] == "ERROR")
            {
               trace("GOT KICK");
               if(_loc5_[1] == "TOOMANYUSERS")
               {
                  this.gameerror = "toomanyplayers";
                  this.controlledserverclose = true;
                  gotoAndStop("gameclose");
               }
               else if(_loc5_[1] == "TOOMANYIPUSERS")
               {
                  this.gameerror = "toomanyipplayers";
                  this.controlledserverclose = true;
                  gotoAndStop("gameclose");
               }
               if(_loc5_[1] == "KICKED")
               {
                  this.controlledserverclose = true;
                  this.gameerror = "kicked";
                  this.gameerrordoneby = _loc5_[2];
                  gotoAndStop("gameclose");
               }
               if(_loc5_[1] == "SHUTDOWN")
               {
                  this.controlledserverclose = true;
                  this.gameerror = "servershutdown";
                  gotoAndStop("gameclose");
               }
               if(_loc5_[1] == "NAMEINUSE")
               {
                  this.controlledserverclose = true;
                  this.gameerror = "nameinuse";
                  gotoAndStop("gameclose");
               }
               if(_loc5_[1] == "SPAM")
               {
                  this.controlledserverclose = true;
                  this.gameerror = "FLOODING";
                  gotoAndStop("gameclose");
               }
               if(_loc5_[1] == "BANNED")
               {
                  this.controlledserverclose = true;
                  this.gameerror = "banned";
                  this.timebannedfor = _loc5_[2];
                  this.gameerrordoneby = _loc5_[3];
                  gotoAndStop("gameclose");
               }
            }
            _loc4_++;
         }
      }
      
      public function func_moveAnothership(param1:*) : *
      {
         var _loc2_:* = 0;
         var _loc3_:* = getTimer();
         _loc2_ = 0;
         while(_loc2_ < this.otherplayership.length)
         {
            this.func_moveanothership(param1,_loc2_,_loc3_);
            _loc2_++;
         }
      }
      
      public function func_runMissileFireScript(param1:*) : *
      {
         var _loc2_:* = 0;
         while(_loc2_ < this.playershipstatus[7].length)
         {
            if(this.playershipstatus[7][_loc2_] != null)
            {
               if(this.playershipstatus[7][_loc2_][1] <= param1)
               {
                  if(this.playershipstatus[10][1] != "Single" || this.playershipstatus[10][0] == _loc2_)
                  {
                     this.func_fire_a_missile(_loc2_);
                  }
               }
            }
            _loc2_++;
         }
      }
      
      public function func_RefreshChat() : *
      {
         var _loc1_:* = this.chatDisplay.textdisplay.scrollV;
         var _loc2_:* = this.chatDisplay.textdisplay.numLines;
         var _loc3_:* = true;
         if(_loc1_ == this.chatDisplay.textdisplay.maxScrollV)
         {
            _loc3_ = true;
         }
         var _loc4_:* = "";
         var _loc5_:* = this.gamechatinfo[2][1];
         while(_loc5_ >= 0)
         {
            _loc4_ += "<font color=\"" + this.gamechatinfo[1][_loc5_][1] + "\">" + this.gamechatinfo[1][_loc5_][0] + "</FONT>\r";
            _loc5_--;
         }
         this.chatDisplay.textdisplay.htmlText = _loc4_;
         if(_loc3_)
         {
            this.chatDisplay.textdisplay.scrollV = this.chatDisplay.textdisplay.maxScrollV;
         }
         else
         {
            this.chatDisplay.textdisplay.scrollV = _loc1_;
         }
      }
      
      public function func_Special_Selected(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         _loc2_ = param1 - 1;
         _loc3_ = this.playershipstatus[11][1][_loc2_][0];
         _loc4_ = getTimer();
         if(_loc3_ != null)
         {
            if(this.specialshipitems[_loc3_][5] == "FLARE")
            {
               _loc5_ = this.specialshipitems[_loc3_][2];
               if(this.playershipstatus[1][1] >= _loc5_ && this.playershipstatus[11][2][_loc2_][0] < _loc4_)
               {
                  this.playershipstatus[1][1] -= _loc5_;
                  this.playershipstatus[11][2][_loc2_][0] = _loc4_ + this.specialshipitems[_loc3_][4];
                  this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
                  _loc6_ = Math.round(this.shipcoordinatex);
                  _loc7_ = Math.round(this.shipcoordinatey);
                  trace("flare at:" + _loc6_ + "`" + _loc7_);
               }
            }
            else if(this.specialshipitems[_loc3_][5] == "DETECTOR")
            {
               if(this.playershipstatus[11][2][_loc2_][0] == 0)
               {
                  this.specialsingame["sp" + param1].specialbutton.gotoAndStop("ON");
                  this.playershipstatus[11][2][_loc2_][0] = this.specialshipitems[_loc3_][11] + _loc4_;
                  this.specialsingame["sp" + param1].specialinfodata.text = "ON";
                  this.func_detectorPing(this.specialshipitems[_loc3_][10]);
               }
               else
               {
                  this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
               }
            }
            else if(this.specialshipitems[_loc3_][5] == "STEALTH")
            {
               if(this.playershipstatus[11][2][_loc2_][0] == 0)
               {
                  if(!this.playerSpecialsSettings.isStealthed && !this.playerSpecialsSettings.isCloaked)
                  {
                     this.specialsingame["sp" + param1].specialbutton.gotoAndStop("ON");
                     this.playershipstatus[11][2][_loc2_][0] = -1;
                     this.specialsingame["sp" + param1].specialinfodata.text = "ON";
                     this.func_stealthplayership(100,this.specialshipitems[_loc3_][10],param1);
                  }
               }
               else
               {
                  this.func_turnoffstealth();
               }
            }
            else if(this.specialshipitems[_loc3_][5] == "CLOAK")
            {
               if(this.playershipstatus[11][2][_loc2_][0] == 0)
               {
                  if(!this.playerSpecialsSettings.isStealthed && !this.playerSpecialsSettings.isCloaked)
                  {
                     this.specialsingame["sp" + param1].specialbutton.gotoAndStop("ON");
                     this.playershipstatus[11][2][_loc2_][0] = -1;
                     this.specialsingame["sp" + param1].specialinfodata.text = "ON";
                     this.func_cloakplayership(100,this.specialshipitems[_loc3_][10],param1);
                  }
               }
               else
               {
                  this.func_turnofshipcloak();
               }
            }
            else if(this.specialshipitems[_loc3_][5] == "RAPIDMISSILE")
            {
               if(this.playershipstatus[11][2][_loc2_][0] == 0)
               {
                  this.specialsingame["sp" + param1].specialbutton.gotoAndStop("ON");
                  this.playershipstatus[11][2][_loc2_][0] = -1;
                  this.specialsingame["sp" + param1].specialinfodata.text = "ON";
               }
               else
               {
                  this.specialsingame["sp" + param1].specialbutton.gotoAndStop("OFF");
                  this.specialsingame["sp" + param1].specialinfodata.text = "";
                  this.playershipstatus[11][2][_loc2_][0] = 0;
               }
            }
            else if(this.specialshipitems[_loc3_][5] == "RECHARGESHIELD")
            {
               _loc5_ = Math.floor(this.playershipstatus[1][1]);
               if(this.playershipstatus[1][1] >= _loc5_ && this.playershipstatus[11][2][_loc2_][0] == 0)
               {
                  this.playershipstatus[1][1] -= _loc5_;
                  this.playershipstatus[11][2][_loc2_][0] = _loc4_ + this.specialshipitems[_loc3_][4];
                  this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
                  _loc8_ = _loc5_ * Number(this.specialshipitems[_loc3_][2]);
                  _loc9_ = this.playershipstatus[2][1];
                  if(_loc8_ + _loc9_ > this.PlayerStatDisp.maxshieldstrength)
                  {
                     _loc8_ = Math.floor(Number(this.PlayerStatDisp.maxshieldstrength) - _loc9_);
                  }
                  this.playershipstatus[2][1] += _loc8_;
                  this.gunShotBufferData += "SH" + "`" + this.playershipstatus[3][0] + "`" + _loc3_ + "`" + Math.floor(this.playershipstatus[2][1]) + "~";
               }
            }
            else if(this.specialshipitems[_loc3_][5] == "RECHARGESTRUCT")
            {
               _loc5_ = Math.floor(this.playershipstatus[1][1]);
               if(this.playershipstatus[1][1] >= _loc5_ && this.playershipstatus[11][2][_loc2_][0] == 0)
               {
                  if(this.PlayerStatDisp.playersmaxstructure != null)
                  {
                     this.playershipstatus[1][1] -= _loc5_;
                     this.playershipstatus[11][2][_loc2_][0] = _loc4_ + this.specialshipitems[_loc3_][4];
                     this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
                     _loc10_ = _loc5_ * Number(this.specialshipitems[_loc3_][2]);
                     this.specialsingame["sp" + param1].specialinfodata.text = "USED";
                     if((_loc11_ = this.playershipstatus[2][5]) + _loc10_ > this.PlayerStatDisp.playersmaxstructure)
                     {
                        _loc10_ = Math.floor(Number(this.PlayerStatDisp.playersmaxstructure) - _loc11_);
                     }
                     this.playershipstatus[2][5] += _loc10_;
                     this.gunShotBufferData += "SH" + "`" + this.playershipstatus[3][0] + "`" + _loc3_ + "`" + Math.floor(this.playershipstatus[2][5]) + "~";
                  }
               }
            }
            else if(this.specialshipitems[_loc3_][5] == "MINES")
            {
               _loc5_ = this.specialshipitems[_loc3_][2];
               if(this.playershipstatus[5][15].charAt(0) != "C")
               {
                  if(this.playershipstatus[11][1][_loc2_][1] >= 1)
                  {
                     if(this.playershipstatus[11][1][_loc2_][1] > 0)
                     {
                        --this.playershipstatus[11][1][_loc2_][1];
                        this.playershipstatus[11][2][_loc2_][0] = _loc4_ + this.specialshipitems[_loc3_][4];
                        this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
                        this.func_displayspecials(param1);
                        _loc12_ = Math.round(this.shipcoordinatex);
                        _loc13_ = Math.round(this.shipcoordinatey);
                        ++this.currentplayershotsfired;
                        if(this.currentplayershotsfired > 999)
                        {
                           this.currentplayershotsfired = 0;
                        }
                        _loc14_ = this.playershipstatus[3][0] + "a" + this.currentplayershotsfired;
                     }
                  }
               }
            }
         }
      }
      
      public function ChangeZoom(param1:*) : *
      {
         if(param1 == "in")
         {
            --this.currZoomFactor;
            if(this.currZoomFactor < 0)
            {
               this.currZoomFactor = 0;
            }
         }
         else
         {
            ++this.currZoomFactor;
            if(this.currZoomFactor >= this.arrayZoomFactors.length)
            {
               this.currZoomFactor = this.arrayZoomFactors.length - 1;
            }
         }
      }
      
      public function func_postsaveresults(param1:*) : *
      {
         var _loc2_:* = param1;
         trace(param1);
         if(_loc2_[1] == "savesuccess")
         {
            this.playershipstatus[5][19] = _loc2_[2];
         }
         else
         {
            this.playershipstatus[5][19] = _loc2_[2];
         }
      }
      
      public function otherPilotProcess(param1:*) : *
      {
         var _loc7_:* = undefined;
         var _loc8_:* = undefined;
         var _loc9_:* = undefined;
         var _loc10_:* = undefined;
         var _loc11_:* = undefined;
         var _loc12_:* = undefined;
         var _loc13_:* = undefined;
         var _loc14_:* = undefined;
         var _loc15_:* = undefined;
         var _loc16_:* = undefined;
         var _loc17_:* = undefined;
         var _loc18_:* = undefined;
         var _loc19_:* = undefined;
         var _loc2_:* = getTimer();
         var _loc3_:* = this.otherplayership.length;
         var _loc4_:* = param1[1];
         var _loc5_:* = 0;
         var _loc6_:* = false;
         while(_loc5_ < this.otherplayership.length && _loc6_ == false)
         {
            if(_loc4_ == this.otherplayership[_loc5_][0])
            {
               _loc7_ = Number(param1[9]);
               _loc3_ = _loc5_;
               _loc6_ = true;
               if(_loc7_ > this.otherplayership[_loc3_][51] || _loc7_ < 15 && this.otherplayership[_loc3_][51] > 60 || isNaN(_loc7_))
               {
                  this.otherplayership[_loc3_][51] = _loc7_;
                  this.otherplayership[_loc3_][0] = param1[1];
                  _loc8_ = int(param1[2]);
                  _loc9_ = this.otherplayership[_loc3_][30].length;
                  this.otherplayership[_loc3_][30][_loc9_] = new Array();
                  this.otherplayership[_loc3_][30][_loc9_][0] = _loc8_;
                  _loc10_ = int(param1[3]);
                  this.otherplayership[_loc3_][30][_loc9_][1] = int(_loc10_);
                  this.otherplayership[_loc3_][30][_loc9_][2] = int(param1[4]);
                  this.otherplayership[_loc3_][30][_loc9_][3] = Number(param1[5]);
                  this.otherplayership[_loc3_][30][_loc9_][6] = new Array();
                  this.otherplayership[_loc3_][30][_loc9_][6][0] = int(0);
                  this.otherplayership[_loc3_][30][_loc9_][6][1] = int(0);
                  _loc18_ = 0;
                  while(_loc18_ < param1[6].length)
                  {
                     if(param1[6].charAt(_loc18_) == "S")
                     {
                        this.otherplayership[_loc3_][30][_loc9_][6][0] = "S";
                     }
                     else if(param1[6].charAt(_loc18_) == "U")
                     {
                        this.otherplayership[_loc3_][30][_loc9_][6][0] += this.shiptype[this.otherplayership[_loc3_][11]][3][0];
                     }
                     else if(param1[6].charAt(_loc18_) == "D")
                     {
                        this.otherplayership[_loc3_][30][_loc9_][6][0] -= this.shiptype[this.otherplayership[_loc3_][11]][3][0];
                     }
                     else if(param1[6].charAt(_loc18_) == "L")
                     {
                        this.otherplayership[_loc3_][30][_loc9_][6][1] -= this.shiptype[this.otherplayership[_loc3_][11]][3][2];
                     }
                     else if(param1[6].charAt(_loc18_) == "R")
                     {
                        this.otherplayership[_loc3_][30][_loc9_][6][1] += this.shiptype[this.otherplayership[_loc3_][11]][3][2];
                     }
                     _loc18_++;
                  }
                  if(this.otherplayership[_loc3_][16] != param1[7])
                  {
                     if(!((_loc15_ = String(param1[7])) == "C" && this.otherplayership[_loc3_][16] != "D"))
                     {
                        if(!(_loc15_ == "S" && this.otherplayership[_loc3_][16] != "D"))
                        {
                           if(_loc15_ != "S" && _loc15_ != "C")
                           {
                           }
                        }
                     }
                     this.otherplayership[_loc3_][16] = _loc15_;
                  }
                  _loc11_ = String(getTimer() + this.clocktimediff);
                  if((_loc12_ = (_loc11_ = int(_loc11_.substr(Number(_loc11_.length) - 4))) - int(param1[8])) < -1500)
                  {
                     _loc12_ += 10000;
                  }
                  if(_loc12_ < 0)
                  {
                     _loc12_ = this.otherplayership[_loc3_][35];
                  }
                  else if(_loc12_ < 1000)
                  {
                     this.otherplayership[_loc3_][35] = _loc12_;
                  }
                  _loc13_ = this.shiplag - _loc12_ + getTimer();
                  if(this.shiplag - _loc12_ < -60 && this.shiplag - _loc12_ > -150)
                  {
                  }
                  this.otherplayership[_loc3_][30][_loc9_][9] = _loc13_;
                  this.otherplayership[_loc3_][30][_loc9_][8] = Number(param1[9]);
                  _loc14_ = 0;
                  while(_loc14_ < this.otherplayership[_loc3_][30].length - 1)
                  {
                     if(this.otherplayership[_loc3_][30][_loc14_][9] > this.otherplayership[_loc3_][30][_loc9_][9])
                     {
                        this.otherplayership[_loc3_][30].splice(_loc9_,1);
                        break;
                     }
                     _loc14_++;
                  }
                  this.otherplayership[_loc3_][6] = _loc2_ + this.shiptimeoutime;
               }
            }
            _loc5_++;
         }
         if(_loc6_ == false)
         {
            _loc3_ = _loc5_;
            this.otherplayership[_loc3_] = new Array();
            this.otherplayership[_loc3_][0] = param1[1];
            this.otherplayership[_loc3_][1] = int(param1[2]);
            this.otherplayership[_loc3_][2] = int(param1[3]);
            this.otherplayership[_loc3_][3] = int(param1[4]);
            this.otherplayership[_loc3_][4] = int(param1[5]);
            this.otherplayership[_loc3_][5] = new Array();
            this.otherplayership[_loc3_][5][0] = int(0);
            this.otherplayership[_loc3_][5][1] = int(0);
            this.otherplayership[_loc3_][6] = _loc2_ + this.shiptimeoutime;
            this.otherplayership[_loc3_][7] = _loc2_ + this.shiptimeoutime * 2;
            this.otherplayership[_loc3_][30] = new Array();
            _loc16_ = 0;
            _loc17_ = false;
            while(_loc16_ < this.currentonlineplayers.length && _loc17_ == false)
            {
               if(this.otherplayership[_loc3_][0] == this.currentonlineplayers[_loc16_][0])
               {
                  _loc17_ = true;
                  this.otherplayership[_loc3_][10] = this.currentonlineplayers[_loc16_][1];
                  this.otherplayership[_loc3_][11] = this.currentonlineplayers[_loc16_][2];
                  this.otherplayership[_loc3_][12] = this.currentonlineplayers[_loc16_][3];
                  this.otherplayership[_loc3_][13] = this.currentonlineplayers[_loc16_][4];
                  this.otherplayership[_loc3_][14] = this.currentonlineplayers[_loc16_][8];
                  this.otherplayership[_loc3_][19] = false;
               }
               _loc16_++;
            }
            if(_loc17_ == false)
            {
               this.otherplayership.splice(_loc3_,1);
            }
            else
            {
               this.otherplayership[_loc3_][15] = "alive";
               _loc18_ = 0;
               while(_loc18_ < param1[6].length)
               {
                  if(param1[6].charAt(_loc18_) == "S")
                  {
                     this.otherplayership[_loc3_][5][0] = "S";
                  }
                  else if(param1[6].charAt(_loc18_) == "U")
                  {
                     this.otherplayership[_loc3_][5][0] += this.shiptype[this.otherplayership[_loc3_][11]][3][0];
                  }
                  else if(param1[6].charAt(_loc18_) == "D")
                  {
                     this.otherplayership[_loc3_][5][0] -= this.shiptype[this.otherplayership[_loc3_][11]][3][0];
                  }
                  else if(param1[6].charAt(_loc18_) == "L")
                  {
                     this.otherplayership[_loc3_][5][1] -= this.shiptype[this.otherplayership[_loc3_][11]][3][2];
                  }
                  else if(param1[6].charAt(_loc18_) == "R")
                  {
                     this.otherplayership[_loc3_][5][1] += this.shiptype[this.otherplayership[_loc3_][11]][3][2];
                  }
                  _loc18_++;
               }
               this.otherplayership[_loc3_][5][9] = getTimer();
               this.otherplayership[_loc3_][5][8] = -1;
               this.otherplayership[_loc3_][16] = String(param1[7]);
               this.otherplayership[_loc3_][21] = new this.shiptype[this.otherplayership[_loc3_][11]][10]() as MovieClip;
               if(this.playershipstatus[3][0] != Number(param1[1]))
               {
                  this.gamedisplayarea.addChild(this.otherplayership[_loc3_][21]);
                  this.otherplayership[_loc3_][21].x = Number(this.otherplayership[_loc3_][1]) - this.shipcoordinatex;
                  this.otherplayership[_loc3_][21].y = Number(this.otherplayership[_loc3_][2]) - this.shipcoordinatey;
                  this.otherplayership[_loc3_][21].rotation = this.otherplayership[_loc3_][2];
                  this.otherplayership[_loc3_][21].gotoAndStop(1);
                  _loc19_ = "";
                  if(this.otherplayership[_loc3_][13] == "N/A" || this.otherplayership[_loc3_][13] == -1 || this.otherplayership[_loc3_][13] != this.playershipstatus[5][2])
                  {
                     _loc19_ = "enemy";
                  }
                  else
                  {
                     _loc19_ = "friend";
                  }
                  this.otherplayership[_loc3_][52] = new MovieClip();
                  this.otherplayership[_loc3_][53] = new shiphealthbar() as MovieClip;
                  this.otherplayership[_loc3_][53].y = -(Number(this.otherplayership[_loc3_][21].width) * 0.8);
                  this.otherplayership[_loc3_][52].addChild(this.otherplayership[_loc3_][53]);
                  this.otherplayership[_loc3_][54] = new shipnametag() as MovieClip;
                  this.otherplayership[_loc3_][54].y = this.otherplayership[_loc3_][53].y;
                  this.otherplayership[_loc3_][52].addChild(this.otherplayership[_loc3_][54]);
                  this.otherplayership[_loc3_][58] = new this.shipTargetDisplay() as MovieClip;
                  this.otherplayership[_loc3_][58].height = this.otherplayership[_loc3_][21].width;
                  this.otherplayership[_loc3_][58].width = this.otherplayership[_loc3_][21].width;
                  this.otherplayership[_loc3_][58].visible = false;
                  this.otherplayership[_loc3_][52].addChild(this.otherplayership[_loc3_][58]);
                  this.otherplayership[_loc3_][55] = new othershipiderDisplay() as MovieClip;
                  this.otherplayership[_loc3_][59] = new this.RadarBlot() as MovieClip;
                  if(_loc19_ == "friend")
                  {
                     this.otherplayership[_loc3_][54].gotoAndStop(2);
                     this.otherplayership[_loc3_][55].gotoAndStop("teammate");
                     this.func_AddRadarDot(this.otherplayership[_loc3_][59],"FREIND");
                  }
                  else
                  {
                     this.otherplayership[_loc3_][54].gotoAndStop(1);
                     this.otherplayership[_loc3_][55].gotoAndStop("enemy");
                     this.func_AddRadarDot(this.otherplayership[_loc3_][59],"HOSTILE");
                  }
                  this.otherplayership[_loc3_][55].height = this.otherplayership[_loc3_][21].width;
                  this.otherplayership[_loc3_][55].width = this.otherplayership[_loc3_][21].width;
                  this.otherplayership[_loc3_][52].addChild(this.otherplayership[_loc3_][55]);
                  this.otherplayership[_loc3_][54].shipLabel.text = this.func_getPlayersName(this.otherplayership[_loc3_][0]);
                  this.otherplayership[_loc3_][56] = _loc19_;
                  this.otherplayership[_loc3_][57] = new this.shiptype[this.otherplayership[_loc3_][11]][11]() as MovieClip;
                  this.otherplayership[_loc3_][57].gotoAndStop(4);
                  this.otherplayership[_loc3_][21].addChild(this.otherplayership[_loc3_][57]);
                  this.otherplayership[_loc3_][21].addChild(this.otherplayership[_loc3_][52]);
                  this.otherplayership[_loc3_][52].rotation = -Number(this.otherplayership[_loc3_][3]);
               }
               this.otherplayership[_loc3_][35] = 0;
               this.otherplayership[_loc3_][36] = 0;
               this.otherplayership[_loc3_][37] = -9999;
               this.otherplayership[_loc3_][38] = -9999;
               this.otherplayership[_loc3_][39] = -9999;
               this.otherplayership[_loc3_][40] = 0;
               this.otherplayership[_loc3_][41] = null;
               this.otherplayership[_loc3_][42] = null;
               this.otherplayership[_loc3_][43] = null;
               this.otherplayership[_loc3_][44] = null;
               this.otherplayership[_loc3_][46] = null;
               this.otherplayership[_loc3_][47] = 0;
               this.otherplayership[_loc3_][51] = -1;
               this.func_RequestStats(this.otherplayership[_loc3_][0]);
            }
         }
      }
      
      public function func_ClockSynchronizeScript(param1:Event) : *
      {
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         var _loc2_:* = getTimer();
         if(_loc2_ > this.timetillnexClocktcheck)
         {
            this.loginmovie.mov_login.logindisplay.currentstatus.text += ".";
            this.timetillnexClocktcheck = _loc2_ + this.ClockIntervalCheckTime;
            ++this.CurenntClockCheckNumber;
            _loc3_ = String(_loc2_);
            _loc3_ = _loc3_.substr(Number(_loc3_.length) - 4);
            _loc4_ = "CLOCK`" + _loc3_ + "~";
            this.mysocket.send(_loc4_);
         }
      }
      
      public function offTurretsSelected(param1:MouseEvent) : void
      {
         this.func_setTurrets("off");
      }
      
      public function func_moveanothership(param1:*, param2:*, param3:*) : *
      {
         var otherplayershiprotating:*;
         var otherplayershtype:*;
         var velocity:*;
         var relativefacing:*;
         var ShipMoved:*;
         var otherplayersmaxvelocity:* = undefined;
         var otherplayersafterburner:* = undefined;
         var bufferedx:* = undefined;
         var bufferedy:* = undefined;
         var PlayerSpeedRatio:* = undefined;
         var thetimeelapsed:* = param1;
         var shipno:* = param2;
         var CurrentTime:* = param3;
         var timeelapsed:* = thetimeelapsed;
         var zz:* = shipno;
         timeelapsed = (getTimer() - Number(this.otherplayership[zz][5][9])) / 1000;
         this.otherplayership[zz][5][9] = getTimer();
         otherplayershtype = this.otherplayership[zz][11];
         otherplayersmaxvelocity = this.shiptype[otherplayershtype][3][1];
         otherplayersafterburner = this.shiptype[otherplayershtype][3][6];
         velocity = 0;
         if(this.otherplayership[zz][5][0] == "S")
         {
            velocity = this.otherplayership[zz][4] = this.otherplayership[zz][4] + Number(this.shiptype[this.otherplayership[zz][11]][3][0]) * timeelapsed * 3;
            if(velocity > otherplayersafterburner)
            {
               velocity = this.otherplayership[zz][4] = otherplayersafterburner;
            }
         }
         else if(isNaN(Number(this.otherplayership[zz][5][0])))
         {
            velocity = 0;
         }
         else
         {
            this.otherplayership[zz][5][0] = Number(this.otherplayership[zz][5][0]);
            this.otherplayership[zz][4] += Number(this.otherplayership[zz][5][0]) * timeelapsed;
            if(this.otherplayership[zz][4] > otherplayersmaxvelocity)
            {
               this.otherplayership[zz][4] -= Number(this.shiptype[this.otherplayership[zz][11]][3][0]) * timeelapsed * 2;
               if(this.otherplayership[zz][4] < otherplayersmaxvelocity)
               {
                  this.otherplayership[zz][4] = otherplayersmaxvelocity;
               }
            }
            velocity = this.otherplayership[zz][4];
            if(velocity < 0)
            {
               velocity = 0;
               this.otherplayership[zz][4] = 0;
            }
         }
         otherplayershiprotating = Number(this.otherplayership[zz][5][1]) * timeelapsed;
         this.otherplayership[zz][3] += otherplayershiprotating;
         if(isNaN(this.otherplayership[zz][0]))
         {
         }
         if(this.otherplayership[zz][3] > 360)
         {
            this.otherplayership[zz][3] -= 360;
         }
         if(this.otherplayership[zz][3] < 0)
         {
            this.otherplayership[zz][3] += 360;
         }
         relativefacing = this.otherplayership[zz][3];
         velocity *= timeelapsed;
         ShipMoved = this.MovingObjectWithThrust(relativefacing,velocity);
         this.otherplayership[zz][1] += ShipMoved[0];
         this.otherplayership[zz][2] += ShipMoved[1];
         if(this.otherplayership[zz][36] > 0)
         {
            bufferedx = this.otherplayership[zz][37] = Number(this.otherplayership[zz][1]) - (Number(this.otherplayership[zz][1]) - Number(this.otherplayership[zz][37])) / 2;
            bufferedy = this.otherplayership[zz][38] = Number(this.otherplayership[zz][2]) - (Number(this.otherplayership[zz][2]) - Number(this.otherplayership[zz][38])) / 2;
            if(this.playershipstatus[3][0] != this.otherplayership[zz][0])
            {
               this.otherplayership[zz][21].x = bufferedx - this.shipcoordinatex;
               this.otherplayership[zz][21].y = bufferedy - this.shipcoordinatey;
               this.otherplayership[zz][21].rotation = Math.round(this.otherplayership[zz][3]);
               this.otherplayership[zz][59].x = bufferedx;
               this.otherplayership[zz][59].y = bufferedy;
            }
            else
            {
               this.AnglePlayerShipFacing = this.otherplayership[zz][3];
               this.playershipvelocity = this.otherplayership[zz][4];
               this.shipcoordinatex = bufferedx;
               this.shipcoordinatey = bufferedy;
            }
            --this.otherplayership[zz][36];
         }
         else
         {
            this.otherplayership[zz][37] = this.otherplayership[zz][1];
            this.otherplayership[zz][38] = this.otherplayership[zz][2];
            if(this.playershipstatus[3][0] != this.otherplayership[zz][0])
            {
               this.otherplayership[zz][21].x = Number(this.otherplayership[zz][1]) - this.shipcoordinatex;
               this.otherplayership[zz][21].y = Number(this.otherplayership[zz][2]) - this.shipcoordinatey;
               this.otherplayership[zz][21].rotation = Math.round(this.otherplayership[zz][3]);
               try
               {
                  this.otherplayership[zz][21].setSpeedRatio(Number(this.otherplayership[zz][4]) / otherplayersmaxvelocity);
               }
               catch(error:Error)
               {
                  trace("Could Not set Other Players Engine Ratio:" + error);
               }
               this.otherplayership[zz][59].x = this.otherplayership[zz][1];
               this.otherplayership[zz][59].y = this.otherplayership[zz][2];
            }
            else
            {
               this.AnglePlayerShipFacing = this.otherplayership[zz][3];
               this.shipcoordinatex = this.otherplayership[zz][1];
               this.shipcoordinatey = this.otherplayership[zz][2];
               this.playershipvelocity = this.otherplayership[zz][4];
               try
               {
                  PlayerSpeedRatio = Number(this.otherplayership[zz][4]) / otherplayersmaxvelocity;
                  if(PlayerSpeedRatio > 1)
                  {
                     if(this.isshiftkeypressed)
                     {
                        this.PlayersShipImage.setSpeedRatio(Number(this.otherplayership[zz][4]) / otherplayersmaxvelocity);
                     }
                     else
                     {
                        this.PlayersShipImage.setSpeedRatio(1);
                     }
                  }
                  else
                  {
                     this.PlayersShipImage.setSpeedRatio(Number(this.otherplayership[zz][4]) / otherplayersmaxvelocity);
                  }
               }
               catch(error:Error)
               {
                  trace("Could Not setPlayers Engine Ratio:" + error);
               }
            }
         }
         if(this.playershipstatus[3][0] != this.otherplayership[zz][0])
         {
            if(this.otherplayership[zz][47] > 0)
            {
               this.otherplayership[zz][41] += timeelapsed * Number(this.otherplayership[zz][42]);
               if(this.otherplayership[zz][41] > this.otherplayership[zz][43])
               {
                  this.otherplayership[zz][41] = this.otherplayership[zz][43];
               }
               try
               {
                  this.otherplayership[zz][53].func_setLifeSettings(Number(this.otherplayership[zz][40]) / Number(this.otherplayership[zz][47]),Number(this.otherplayership[zz][41]) / Number(this.otherplayership[zz][43]));
               }
               catch(error:Error)
               {
               }
            }
            else
            {
               this.otherplayership[zz][53].visible = false;
            }
            if(this.otherplayership[zz][16].length > 1)
            {
               this.otherplayership[zz][59].alpha -= 0.007;
               if(this.otherplayership[zz][56] == "friend")
               {
                  if(this.otherplayership[zz][59].alpha < 0.5)
                  {
                     this.otherplayership[zz][59].alpha = 0.5;
                  }
               }
               else if(this.otherplayership[zz][59].alpha < 0)
               {
                  this.otherplayership[zz][59].alpha = 0;
               }
               if(this.otherplayership[zz][16].charAt(0) == "C")
               {
                  this.otherplayership[zz][21].alpha -= 0.007;
                  if(this.otherplayership[zz][56] == "friend")
                  {
                     if(this.otherplayership[zz][21].alpha < 0.5)
                     {
                        this.otherplayership[zz][21].alpha = 0.5;
                     }
                  }
                  else if(this.otherplayership[zz][21].alpha < 0)
                  {
                     this.otherplayership[zz][21].alpha = 0;
                  }
               }
               else
               {
                  this.otherplayership[zz][21].alpha = 1;
               }
            }
            else
            {
               this.otherplayership[zz][59].alpha += 0.08;
               this.otherplayership[zz][21].alpha += 0.08;
               if(this.otherplayership[zz][59].alpha > 1)
               {
                  this.otherplayership[zz][59].alpha = 1;
               }
               if(this.otherplayership[zz][21].alpha > 1)
               {
                  this.otherplayership[zz][21].alpha = 1;
               }
            }
         }
         if(this.otherplayership[zz][52] != null)
         {
            this.otherplayership[zz][52].rotation = -Number(this.otherplayership[zz][21].rotation);
         }
         if(this.otherplayership[zz][58] != null)
         {
            if(this.targetinfo[4] == this.otherplayership[zz][0])
            {
               this.otherplayership[zz][58].visible = true;
            }
            else
            {
               this.otherplayership[zz][58].visible = false;
            }
         }
         if(CurrentTime > this.otherplayership[zz][6])
         {
            this.func_RemoveRadarDot(this.otherplayership[zz][59]);
            try
            {
               this.gamedisplayarea.removeChild(this.otherplayership[zz][21]);
            }
            catch(error:Error)
            {
               trace("Error: " + error);
            }
            this.otherplayership.splice(zz,1);
         }
      }
      
      public function func_ExitBaseButton_Click(param1:MouseEvent) : void
      {
         if(1 == 1)
         {
            this.starbaseexitposition();
            this.playerjustloggedin = false;
            if(this.teamdeathmatch)
            {
               if(this.squadwarinfo[0] == true && this.squadwarinfo[2] != true)
               {
                  trace("Squad War is waiting to start");
               }
               else if(this.playershipstatus[5][2] != Number(this.playershipstatus[5][2]) && this.squadwarinfo[2] == true)
               {
                  trace("You are not on one of the squads");
               }
               else if(this.playershipstatus[5][2] != Number(this.playershipstatus[5][2]))
               {
                  trace("Need to Choose / Select a Team First");
               }
               else if(this.timePlayerCanExit < getTimer())
               {
                  if(!isNaN(this.playershipstatus[5][2]) && this.playershipstatus[5][2] >= 0 && this.playershipstatus[5][2] < this.teambases.length)
                  {
                     gotoAndStop("maingameplaying");
                  }
               }
               else
               {
                  trace("Need to wait ");
               }
            }
            else
            {
               gotoAndStop("maingameplaying");
            }
         }
      }
      
      public function initializemissilebanks() : *
      {
         var _loc3_:* = undefined;
         this.playershipstatus[10][0] = 0;
         this.playershipstatus[7] = new Array();
         var _loc1_:* = this.playershipstatus[5][0];
         var _loc2_:* = 0;
         while(_loc2_ < this.shiptype[_loc1_][7].length)
         {
            this.playershipstatus[7][_loc2_] = new Array();
            this.playershipstatus[7][_loc2_][0] = 0;
            this.playershipstatus[7][_loc2_][1] = 0;
            this.playershipstatus[7][_loc2_][2] = this.shiptype[_loc1_][7][_loc2_][2];
            this.playershipstatus[7][_loc2_][3] = this.shiptype[_loc1_][7][_loc2_][3];
            this.playershipstatus[7][_loc2_][4] = new Array();
            _loc3_ = 0;
            while(_loc3_ < this.missile.length)
            {
               this.playershipstatus[7][_loc2_][4][_loc3_] = 0;
               _loc3_++;
            }
            this.playershipstatus[7][_loc2_][5] = this.shiptype[_loc1_][7][_loc2_][5];
            _loc2_++;
         }
      }
      
      public function func_removePlayersShot(param1:*) : *
      {
         var thecurrentnumber:* = param1;
         try
         {
            this.gamedisplayarea.removeChild(this.playershotsfired[thecurrentnumber][9]);
         }
         catch(error:Error)
         {
            trace("Error removing playershot: " + error);
         }
         this.playershotsfired.splice(thecurrentnumber,1);
      }
      
      public function func_detectorPing(param1:*) : *
      {
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         this.func_PingRadar();
         _loc2_ = 0;
         while(_loc2_ < this.otherplayership.length)
         {
            if(this.otherplayership[_loc2_][16].length > 1)
            {
               _loc3_ = this.otherplayership[_loc2_][16].charAt(0);
               if((_loc4_ = Number(this.otherplayership[_loc2_][16].charAt(1))) <= param1)
               {
                  if(_loc3_ == "C")
                  {
                     this.otherplayership[_loc2_][21].alpha = 0.95;
                  }
                  this.otherplayership[_loc2_][59].alpha = 1;
               }
            }
            _loc2_++;
         }
      }
      
      public function func_GetPlayerSEndOutData(param1:*, param2:*, param3:*) : *
      {
         var _loc4_:* = (_loc4_ = (_loc4_ = (_loc4_ = (_loc4_ = (_loc4_ = (_loc4_ = (_loc4_ = (_loc4_ = (_loc4_ = "PI`" + param2 + "`" + param1 + "~") + ("PI" + "`" + this.playershipstatus[3][0] + "`")) + (Math.round(this.shipcoordinatex) + "`")) + (Math.round(this.shipcoordinatey) + "`")) + (Math.round(this.PlayersShipImage.rotation) + "`")) + (Math.round(this.playershipvelocity) + "`")) + (this.currentkeyedshipmovement() + "`")) + (this.playershipstatus[5][15] + "`")) + (this.func_globalTimeStamp(param3) + "`")) + (this.currentPIpacket + "~");
         ++this.currentPIpacket;
         if(this.currentPIpacket > 99)
         {
            this.currentPIpacket = 0;
         }
         return _loc4_;
      }
      
      public function setRadarScale() : *
      {
         this.gameRadar.radarScreen.scaleX = 0.05;
         this.gameRadar.radarScreen.scaleY = 0.05;
      }
      
      public function starbaseexitposition() : *
      {
         var _loc1_:* = undefined;
         var _loc2_:* = undefined;
         var _loc3_:* = undefined;
         var _loc4_:* = undefined;
         if(this.teamdeathmatch)
         {
            trace(this.playershipstatus[5][2]);
            this.shipcoordinatex = 0;
            this.shipcoordinatey = 0;
            if(!isNaN(this.playershipstatus[5][2]) && this.playershipstatus[5][2] >= 0)
            {
               if(this.playershipstatus[5][2] < this.teambases.length)
               {
                  _loc1_ = Math.round(Number(this.teambases[Number(this.playershipstatus[5][2])][4]) / 5 * 4);
                  this.shipcoordinatex = this.teambases[this.playershipstatus[5][2]][1] + _loc1_ * 2 * Math.random() - _loc1_;
                  this.shipcoordinatey = this.teambases[this.playershipstatus[5][2]][2] + _loc1_ * 2 * Math.random() - _loc1_;
               }
            }
         }
         else
         {
            _loc2_ = 0;
            _loc3_ = false;
            _loc4_ = 0;
            while(_loc2_ < this.starbaselocation.length && _loc3_ == false)
            {
               if(this.starbaselocation[_loc2_][0] == this.playershipstatus[4][0])
               {
                  _loc3_ = true;
                  _loc4_ = Math.round(Number(this.starbaselocation[_loc2_][4]) / 5 * 4);
                  this.shipcoordinatex = this.starbaselocation[_loc2_][1] + _loc4_ * 2 * Math.random() - _loc4_;
                  this.shipcoordinatey = this.starbaselocation[_loc2_][2] + _loc4_ * 2 * Math.random() - _loc4_;
               }
               _loc2_++;
            }
            if(!_loc3_)
            {
               _loc2_ = Math.round(Math.random() * Number(this.starbaselocation.length) - 1);
               if(this.starbaselocation[_loc2_] != null)
               {
                  _loc4_ = Math.round(Number(this.starbaselocation[_loc2_][4]) / 5 * 4);
                  this.shipcoordinatex = this.starbaselocation[_loc2_][1] + _loc4_ * 2 * Math.random() - _loc4_;
                  this.shipcoordinatey = this.starbaselocation[_loc2_][2] + _loc4_ * 2 * Math.random() - _loc4_;
               }
            }
            if(this.shipcoordinatex < 0 || this.shipcoordinatey < 0)
            {
               this.shipcoordinatex = 0;
               this.shipcoordinatey = 0;
            }
         }
      }
      
      public function othermissilefiremovement(param1:*) : *
      {
         var playersx:* = undefined;
         var playersy:* = undefined;
         var halfplayerswidth:* = undefined;
         var halfplayersheight:* = undefined;
         var MovementOfShot:* = undefined;
         var playerwhoshothitid:* = undefined;
         var gunshothitplayertype:* = undefined;
         var gunfireinformation:* = undefined;
         var missileshotdamage:* = undefined;
         var zzj:* = undefined;
         var dataupdated:* = undefined;
         var timeChangeRatio:* = param1;
         var curenttime:* = getTimer();
         var MakeMissileSeek:* = false;
         var cc:* = 0;
         if(this.othermissilefire.length > 0)
         {
            playersx = this.shipcoordinatex;
            playersy = this.shipcoordinatey;
            halfplayerswidth = this.PlayersShipImage.width / 2;
            halfplayersheight = this.PlayersShipImage.height / 2;
         }
         while(cc < this.othermissilefire.length)
         {
            if(this.lastMissileSeek < curenttime)
            {
               this.lastMissileSeek = curenttime + this.MissileSeekDelay;
               MakeMissileSeek = true;
            }
            if(this.othermissilefire[cc][15] != "")
            {
               if(this.othermissilefire[cc][15] == "R")
               {
                  this.othermissilefire[cc][6] += Number(this.othermissilefire[cc][14]) * timeChangeRatio;
               }
               else
               {
                  this.othermissilefire[cc][6] -= Number(this.othermissilefire[cc][14]) * timeChangeRatio;
               }
               MovementOfShot = this.MovingObjectWithThrust(this.othermissilefire[cc][6],this.othermissilefire[cc][8]);
               this.othermissilefire[cc][3] = MovementOfShot[0];
               this.othermissilefire[cc][4] = MovementOfShot[1];
            }
            this.othermissilefire[cc][1] += Number(this.othermissilefire[cc][3]) * timeChangeRatio;
            this.othermissilefire[cc][2] += Number(this.othermissilefire[cc][4]) * timeChangeRatio;
            try
            {
               this.othermissilefire[cc][16].x = Number(this.othermissilefire[cc][1]) - this.shipcoordinatex;
               this.othermissilefire[cc][16].y = Number(this.othermissilefire[cc][2]) - this.shipcoordinatey;
               this.othermissilefire[cc][16].rotation = this.othermissilefire[cc][6];
            }
            catch(error:Error)
            {
               trace("Error moving other player MIssilefire: " + error);
            }
            if(this.othermissilefire[cc][5] < curenttime)
            {
               try
               {
                  this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
               }
               catch(error:Error)
               {
                  trace("Error removing otherplayermissile timeout: " + error);
               }
               this.othermissilefire.splice(cc,1);
               cc--;
            }
            else if(this.othermissilefire[cc][14] != true)
            {
               if(this.myLineHittest(this.shipcoordinatex,this.shipcoordinatey,Math.round(this.PlayersShipImage.width / 2) + this.othermissilefire[cc][11],this.othermissilefire[cc][18],this.othermissilefire[cc][19],this.othermissilefire[cc][1],this.othermissilefire[cc][2]) && this.playershipstatus[5][4] == "alive")
               {
                  try
                  {
                     this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                  }
                  catch(error:Error)
                  {
                     trace("Error removing otherplayermissile hitship: " + error);
                  }
                  playerwhoshothitid = this.othermissilefire[cc][0];
                  gunshothitplayertype = this.othermissilefire[cc][7];
                  if(this.playershipstatus[5][20] != true)
                  {
                     missileshotdamage = 0;
                     missileshotdamage = this.missile[gunshothitplayertype][4];
                     this.playershipstatus[2][1] -= missileshotdamage;
                  }
                  gunfireinformation = "";
                  if(this.playershipstatus[2][1] < 0)
                  {
                     this.playershipstatus[2][5] += this.playershipstatus[2][1];
                     this.playershipstatus[2][1] = 0;
                     gunfireinformation += "MH" + "`" + this.playershipstatus[3][0] + "`";
                     gunfireinformation += this.othermissilefire[cc][0] + "`";
                     gunfireinformation += this.othermissilefire[cc][9] + "`ST`" + Math.round(this.playershipstatus[2][5]) + "~";
                     this.gunShotBufferData += gunfireinformation;
                  }
                  else
                  {
                     gunfireinformation += "MH" + "`" + this.playershipstatus[3][0] + "`";
                     gunfireinformation += this.othermissilefire[cc][0] + "`";
                     gunfireinformation += this.othermissilefire[cc][9] + "`SH`" + Math.round(this.playershipstatus[2][1]) + "~";
                     this.gunShotBufferData += gunfireinformation;
                  }
                  this.func_playerGotHit();
                  if(this.playershipstatus[2][5] <= 0 && this.playershipstatus[5][4] == "alive")
                  {
                     this.playershipstatus[5][4] = "dead";
                     this.playershipstatus[5][5] = playerwhoshothitid;
                     zzj = 0;
                     dataupdated = false;
                     while(zzj < this.currentonlineplayers.length && dataupdated == false)
                     {
                        if(playerwhoshothitid == this.currentonlineplayers[zzj][0])
                        {
                           dataupdated = true;
                           this.playershipstatus[5][7] = this.currentonlineplayers[zzj][1];
                        }
                        zzj++;
                     }
                     this.playershipstatus[2][5] = 0;
                     gotoAndPlay("playerdeath");
                  }
                  this.othermissilefire.splice(cc,1);
                  cc--;
               }
               else
               {
                  this.othermissilefire[cc][18] = this.othermissilefire[cc][1];
                  this.othermissilefire[cc][19] = this.othermissilefire[cc][2];
                  if(this.othermissilefire[cc][13] == "SEEKING")
                  {
                     if(MakeMissileSeek)
                     {
                        this.func_SeekTheOtherPlayersMissile(cc);
                     }
                  }
               }
            }
            cc++;
         }
      }
   }
}
