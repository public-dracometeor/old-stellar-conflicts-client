package sc313_fla
{
   import flash.display.MovieClip;
   import flash.display.SimpleButton;
   import flash.events.Event;
   import flash.text.TextField;
   
   public dynamic class hardwaremissileinformationbutton_111 extends MovieClip
   {
       
      
      public var MissilePrice;
      
      public var maxpurchase:TextField;
      
      public var banknumberdisp:TextField;
      
      public var quantity:TextField;
      
      public var CurrentMissiles;
      
      public var costdisp:TextField;
      
      public var nobutton:SimpleButton;
      
      public var missilecostdisp:TextField;
      
      public var maxMissiles;
      
      public var yesbutton:SimpleButton;
      
      public function hardwaremissileinformationbutton_111()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      public function func_OnGameFrame(param1:Event) : *
      {
         var _loc2_:* = undefined;
         if(this.visible)
         {
            _loc2_ = Number(this.quantity.text);
            if(!isNaN(_loc2_))
            {
               _loc2_ = Math.round(_loc2_);
            }
            if(_loc2_ < 0)
            {
               this.quantity.text = "0";
            }
            else if(isNaN(_loc2_))
            {
               this.quantity.text = "0";
            }
            else if(_loc2_ > this.maxMissiles)
            {
               this.quantity.text = String(this.maxMissiles);
            }
            this.costdisp.text = "Cost: " + Number(this.quantity.text) * this.MissilePrice;
         }
      }
      
      internal function frame1() : *
      {
         this.visible = false;
         this.maxMissiles = 0;
         this.CurrentMissiles = 0;
         this.MissilePrice = 0;
         this.quantity.text = "0";
         addEventListener(Event.ENTER_FRAME,this.func_OnGameFrame);
         stop();
      }
   }
}
