package
{
   import flash.display.MovieClip;
   import flash.text.TextField;
   
   public dynamic class Missilebank extends MovieClip
   {
       
      
      public var missilebank:TextField;
      
      public var missilereadiness:MovieClip;
      
      public var missileqty:TextField;
      
      public var missilename:TextField;
      
      public var autoswtichbutton:MovieClip;
      
      public function Missilebank()
      {
         super();
         addFrameScript(0,this.frame1);
      }
      
      internal function frame1() : *
      {
         stop();
      }
   }
}
