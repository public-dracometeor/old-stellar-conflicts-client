﻿// Action script...

// [Action in Frame 1]
function setupfightingbase()
{
    turrettype = _root.guntype.length - 1;
    this.attachMovie("turrettypeaibase", "turrettypeaibase0", 1);
    this.turrettypeaibase0.shottype = turrettype - 1;
    this.turrettypeaibase0._x = -15;
    this.turrettypeaibase0._y = -43;
    this.attachMovie("turrettypeaibase", "turrettypeaibase1", 2);
    this.turrettypeaibase1.shottype = turrettype;
    this.turrettypeaibase1._x = 35;
    this.turrettypeaibase1._y = -10;
    this.attachMovie("turrettypeaibase", "turrettypeaibase2", 3);
    this.turrettypeaibase2.shottype = turrettype;
    this.turrettypeaibase2._x = -15;
    this.turrettypeaibase2._y = 43;
    this.attachMovie("turrettypeaibase", "turrettypeaibase3", 4);
    this.turrettypeaibase3.shottype = turrettype - 1;
    this.turrettypeaibase3._x = 35;
    this.turrettypeaibase3._y = 10;
    starbaseunhostileat = getTimer() + _root.starbasestayhostiletime;
    for (j = 0; j < _root.starbaselocation.length; j++)
    {
        if (baseidname == _root.starbaselocation[j][0])
        {
            _root.starbaselocation[j][9] = starbaseunhostileat;
            break;
        } // end if
    } // end of for
    for (j = 0; j < _root.sectormapitems.length; j++)
    {
        if (baseidname == _root.sectormapitems[j][0])
        {
            _root.sectormapitems[j][9] = starbaseunhostileat;
            break;
        } // end if
    } // end of for
} // End of the function
function playershittestneutral()
{
    numberofplayershots = _root.playershotsfired.length;
    if (numberofplayershots > 0)
    {
        halfshipswidth = this._width / 2;
        halfshipsheight = this._height / 2;
        this.relativex = this._x - _root.shipcoordinatex;
        this.relativey = this._y - _root.shipcoordinatey;
        nometeortarget = false;
        for (jj = 0; jj < _root.incomingmeteor.length; jj++)
        {
            if (_root.incomingmeteor[jj][4] == baseidname)
            {
                nometeortarget = true;
                break;
            } // end if
        } // end of for
    } // end if
    i = 0;
    if (nometeortarget == false)
    {
        while (i < numberofplayershots)
        {
            bulletshotid = _root.playershotsfired[i][0];
            bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
            bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
            if (bulletsx != null)
            {
                if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
                {
                    if (_root.playershotsfired[i][13] == "GUNS")
                    {
                        currentshieldstr = currentshieldstr - _root.guntype[_root.playershotsfired[i][6]][4];
                    }
                    else if (_root.playershotsfired[i][13] == "MISSILE")
                    {
                        currentshieldstr = currentshieldstr - _root.missile[_root.playershotsfired[i][6]][4];
                    } // end else if
                    if (currentshieldstr < 0)
                    {
                        totalstructdamage = totalstructdamage - currentshieldstr;
                        structuredisp = structuredisp + currentshieldstr;
                        currentshieldstr = 0;
                    } // end if
                    removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                    _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                    _root.playershotsfired[i][5] = 0;
                    if (isbasehostile != true)
                    {
                        --hitstillhostile;
                    } // end if
                    if (hitstillhostile <= 0)
                    {
                        message = "You were warned!";
                        _root.func_messangercom(message, "STARBASE", "BEGIN");
                        isbasehostile = true;
                        setupfightingbase();
                    }
                    else
                    {
                        message = "You hit me " + hitstillhostile + " more times and I will go hostile!";
                        _root.func_messangercom(message, "STARBASE", "BEGIN");
                    } // end if
                } // end if
            } // end else if
            ++i;
        } // end while
    } // end if
} // End of the function
function playershittest()
{
    numberofplayershots = _root.playershotsfired.length;
    if (numberofplayershots > 0)
    {
        halfshipswidth = this.starbaseimmage._width / 2;
        halfshipsheight = this.starbaseimmage._height / 2;
        this.relativex = this._x - _root.shipcoordinatex;
        this.relativey = this._y - _root.shipcoordinatey;
    } // end if
    for (i = 0; i < numberofplayershots; i++)
    {
        bulletshotid = _root.playershotsfired[i][0];
        bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
        bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
        if (bulletsx != null)
        {
            if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
            {
                if (_root.playershotsfired[i][13] == "GUNS")
                {
                    currentshieldstr = currentshieldstr - _root.guntype[_root.playershotsfired[i][6]][4];
                }
                else if (_root.playershotsfired[i][13] == "MISSILE")
                {
                    currentshieldstr = currentshieldstr - _root.missile[_root.playershotsfired[i][6]][4];
                } // end else if
                if (currentshieldstr < 0)
                {
                    totalstructdamage = totalstructdamage - currentshieldstr;
                    structuredisp = structuredisp + currentshieldstr;
                    currentshieldstr = 0;
                } // end if
                removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                _root.playershotsfired[i][5] = 0;
            } // end if
        } // end if
    } // end of for
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function
function fireamissile()
{
    this.missilexfire = this._x;
    this.missileyfire = this._y;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    playeranglefromship = 360 - Math.atan2(this.missilexfire - playersxcoord, this.missileyfire - playersycoord) / 0.017453;
    if (playeranglefromship > 360)
    {
        playeranglefromship = playeranglefromship - 360;
    } // end if
    if (playeranglefromship < 0)
    {
        playeranglefromship = playeranglefromship + 360;
    } // end if
    playeranglefromship = Math.round(playeranglefromship);
    ++_root.currentothermissileshot;
    if (_root.currentothermissileshot >= 1000)
    {
        _root.currentothermissileshot = 1;
    } // end if
    if (_root.othermissilefire.length < 1)
    {
        _root.othermissilefire = new Array();
    } // end if
    lastvar = _root.othermissilefire.length;
    _root.othermissilefire[lastvar] = new Array();
    _root.othermissilefire[lastvar][0] = 999999;
    _root.othermissilefire[lastvar][1] = this.missilexfire;
    _root.othermissilefire[lastvar][2] = this.missileyfire;
    velocity = _root.missile[missileshottype][0] * 2;
    _root.othermissilefire[lastvar][6] = playeranglefromship;
    _root.othermissilefire[lastvar][7] = missileshottype;
    _root.othermissilefire[lastvar][8] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][9] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][10] = "other";
    _root.othermissilefire[lastvar][5] = _root.curenttime + _root.missile[missileshottype][2];
    _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "fire", "othermissilefire" + _root.othermissilefire[lastvar][8], 999000 + _root.othermissilefire[lastvar][8]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _rotation, _root.othermissilefire[lastvar][6]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _x, _root.othermissilefire[lastvar][1] - _root.shipcoordinatex);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _y, _root.othermissilefire[lastvar][2] - _root.shipcoordinatey);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xposition", _root.othermissilefire[lastvar][1]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".yposition", _root.othermissilefire[lastvar][2]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".missiletype", missileshottype);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".velocity", velocity);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".seeking", "HOST");
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".shooterid", _root.othermissilefire[lastvar][0]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".globalid", _root.othermissilefire[lastvar][9]);
    _root.othermissilefire[lastvar][11] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._width / 2;
    _root.othermissilefire[lastvar][12] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._height / 2;
    _root.othermissilefire[lastvar][13] = "SEEKING";
} // End of the function
stop ();

basedestroyed = false;
basetakendamageyet = false;
hittestdelay = 550;
lasthittest = getTimer() + hittestdelay;
hitstillhostile = 6;
misileupdateint = 10000;
lastmisileupdate = 0;
missileshotsleft = 90;
missileshottype = 9;
missilesfired = 0;
missilfireint = _root.missile[missileshottype][3] / 2;
timetillnextmissile = 0;
xsector = _root.playershipstatus[6][0];
ysector = _root.playershipstatus[6][1];
if (displaymessage == true)
{
    message = "Welcome to sector " + xsector + "," + ysector + ".  Home of Starbase " + basename + ". \rRemember not to fire at the green docking circle or you will be fired upon!\rPressing D while not chatting will allow you to dock.";
    _root.func_messangercom(message, "STARBASE", "BEGIN");
} // end if
this.onEnterFrame = function ()
{
    if (basedestroyed == true)
    {
        this.onEnterFrame = function ()
        {
        };
        this.play();
    } // end if
    currenttime = getTimer();
    if (lasthittest < currenttime)
    {
        timechange = currenttime - (lasthittest - hittestdelay);
        lasthittest = currenttime + hittestdelay;
        if (isbasehostile == false)
        {
            if (_root.kingofflag[0] == null)
            {
                playershittestneutral();
            } // end if
        }
        else if (isbasehostile == true)
        {
            playershittest();
            if (timetillnextmissile < currenttime)
            {
                if (_root.playershipstatus[2][5] > 0)
                {
                    fireamissile();
                    if (_root.soundvolume != "off")
                    {
                        _root.missilefiresound.start();
                    } // end if
                    timetillnextmissile = missilfireint + currenttime;
                } // end if
            } // end if
        } // end if
    } // end else if
};
stop ();

// [Action in Frame 2]
stop ();
