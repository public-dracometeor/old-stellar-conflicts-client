﻿// Action script...

// [onClipEvent of sprite 1418 in frame 1]
onClipEvent (load)
{
    function func_damagetheradar()
    {
        if (radardamaged == false)
        {
            _root.radarscreen.coordinates = "Destroyed...";
            this.radarblown._visible = true;
            this.radarblown.play();
            radardamaged = true;
        } // end if
    } // End of the function
    function func_repairtheradar()
    {
        if (radardamaged == true)
        {
            _root.radarscreen.coordinates = "Locating...";
            this.radarblown._visible = false;
            this.radarblown.gotoAndStop(1);
            radardamaged = false;
        } // end if
    } // End of the function
    function func_pingcircledetector()
    {
        this.radarscreepingcirlce.play();
    } // End of the function
    maxy = 45;
    maxx = 45;
    radardamaged = false;
    _root.hostileplayerships = false;
    radartogamearearatio = 18;
    this.attachMovie("radarblot", "playershipdot", 5000);
    this.playershipdot.gotoAndStop(2);
    myColor = new Color(playershipdot);
    myColor.setRGB(16777215);
    lastsectoris = _root.playershipstatus[6][2] + "`" + _root.playershipstatus[6][3];
    location = "Locating...";
    _root.radarscreen.coordinates = "Locating...";
    currentinterval = 0;
    intervalstowait = 10;
    currentinterval = Math.round(Math.random() * intervalstowait);
    halfradarwidth = Math.round(this._width / 2 - 2);
    halfradarheight = Math.round(this._height / 2 - 2);
    currentitem = 0;
}

// [onClipEvent of sprite 1418 in frame 1]
onClipEvent (enterFrame)
{
    ++currentinterval;
    if (radardamaged)
    {
        currentinterval = 0;
    } // end if
    if (currentinterval > intervalstowait)
    {
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        currentinterval = 0;
        if (currentsectoris != _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1])
        {
            xgrid = _root.playershipstatus[6][0];
            ygrid = _root.playershipstatus[6][1];
            location = "Location X:" + xgrid + " Y:" + ygrid;
            currentsectoris = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
        } // end if
        _root.hostileplayerships = false;
        for (i = 0; i < _root.otherplayership.length; i++)
        {
            enemyxonradar = (_root.otherplayership[i][1] - _root.shipcoordinatex) / radartogamearearatio;
            enemyyonradar = (_root.otherplayership[i][2] - _root.shipcoordinatey) / radartogamearearatio;
            if (Math.abs(enemyxonradar) < maxx && Math.abs(enemyyonradar) < maxy)
            {
                this.attachMovie("radarblot", "enemyradardot" + i, 1000 + i);
                enemyColor = new Color("enemyradardot" + i);
                if (_root.playershipstatus[5][2] != _root.otherplayership[i][13] || _root.playershipstatus[5][2] == "N/A")
                {
                    if (_root.otherplayership[i][16] == "C" || _root.otherplayership[i][16] == "S")
                    {
                        this["enemyradardot" + i]._visible = false;
                    } // end if
                    enemyColor.setRGB(16711680);
                    _root.hostileplayerships = true;
                }
                else
                {
                    enemyColor.setRGB(16776960);
                } // end else if
                if (_root.otherplayership[i][0] == _root.kingofflag[0])
                {
                    this["enemyradardot" + i].attachMovie("radarflagholder", "radarflagholder", 5);
                    this["enemyradardot" + i]._visible = true;
                } // end if
                setProperty("enemyradardot" + i, _x, enemyxonradar);
                setProperty("enemyradardot" + i, _y, enemyyonradar);
            } // end if
        } // end of for
        for (j = 0; j < _root.aishipshosted.length; j++)
        {
            ++i;
            this.attachMovie("radarblot", "enemyradardot" + i, 1000 + i);
            enemyColor = new Color("enemyradardot" + i);
            enemyColor.setRGB(16711680);
            setProperty("enemyradardot" + i, _x, (_root.aishipshosted[j][1] - _root.shipcoordinatex) / radartogamearearatio);
            setProperty("enemyradardot" + i, _y, (_root.aishipshosted[j][2] - _root.shipcoordinatey) / radartogamearearatio);
        } // end of for
        radaritems = _root.radarbackitems;
        totalsectoritems = radaritems.length;
        for (i = 0; i < totalsectoritems; i++)
        {
            xposonradar = Math.round((radaritems[i][1] - playersxcoord) / radartogamearearatio);
            yposonradar = Math.round((radaritems[i][2] - playersycoord) / radartogamearearatio);
            if (Math.abs(yposonradar) < maxy && Math.abs(xposonradar) < maxx)
            {
                this.attachMovie("radarblot", "starbasedot" + i, 500 + i);
                dotcolor = new Color("starbasedot" + i);
                dotcolor.setRGB(radaritems[i][4]);
                this["starbasedot" + i].setRGB(radaritems[i][4]);
                setProperty("starbasedot" + i, _x, xposonradar);
                setProperty("starbasedot" + i, _y, yposonradar);
                this["starbasedot" + i]._visible = true;
                continue;
            } // end if
            this["starbasedot" + i]._visible = false;
        } // end of for
    } // end if
}

// [Action in Frame 1]
stop ();
