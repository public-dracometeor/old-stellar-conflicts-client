﻿// Action script...

// [onClipEvent of sprite 1418 in frame 1]
onClipEvent (load)
{
    function func_damagetheradar()
    {
        if (radardamaged == false)
        {
            _root.radarscreen.coordinates = "Destroyed...";
            this.radarblown._visible = true;
            this.radarblown.play();
            radardamaged = true;
        } // end if
    } // End of the function
    function func_repairtheradar()
    {
        if (radardamaged == true)
        {
            _root.radarscreen.coordinates = "Locating...";
            this.radarblown._visible = false;
            this.radarblown.gotoAndStop(1);
            radardamaged = false;
        } // end if
    } // End of the function
    function func_pingcircledetector()
    {
        this.radarscreepingcirlce.play();
    } // End of the function
    maxy = 45;
    maxx = 45;
    radardamaged = false;
    _root.hostileplayerships = false;
    radartogamearearatio = 18;
    this.attachMovie("radarblot", "playershipdot", 5000);
    this.playershipdot.gotoAndStop(2);
    myColor = new Color(playershipdot);
    myColor.setRGB(16777215);
    lastsectoris = _root.playershipstatus[6][2] + "`" + _root.playershipstatus[6][3];
    location = "Locating...";
    _root.radarscreen.coordinates = "Locating...";
    currentinterval = 0;
    intervalstowait = 10;
    currentinterval = Math.round(Math.random() * intervalstowait);
    halfradarwidth = Math.round(this._width / 2 - 2);
    halfradarheight = Math.round(this._height / 2 - 2);
    currentitem = 0;
}
