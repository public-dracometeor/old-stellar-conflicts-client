﻿// Action script...

on (release)
{
    function checknameforlegitcharacters(name)
    {
        legitcharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        for (i = 0; i < name.length; i++)
        {
            illegalcharacter = true;
            for (j = 0; j < legitcharacters.length; j++)
            {
                if (name.charAt(i) == legitcharacters.charAt(j))
                {
                    illegalcharacter = false;
                } // end if
            } // end of for
            if (illegalcharacter == true)
            {
                return (true);
                i = 99999;
            } // end if
        } // end of for
    } // End of the function
    squadname = squadname.toUpperCase();
    passwordd = passwordd.toUpperCase();
    if (squadname.length > 13 || passwordd.length > 13)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "12 Characters!";
    }
    else if (squadname.length < 1 || passwordd.length < 1)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "12 Characters!";
    }
    else if (checknameforlegitcharacters(squadname))
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "Letters or numbers!";
    }
    else if (checknameforlegitcharacters(passwordd))
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "Letters or numbers!";
    }
    else
    {
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squads.php?mode=squadcreate&sname=" + squadname + "&spass=" + passwordd + "&name=" + _root.playershipstatus[3][2] + "&pass=" + _root.playershipstatus[3][3]);
        newsquadVars.onLoad = function (success)
        {
            loadedinfo = String(newsquadVars);
            if (loadedinfo == "created")
            {
                _root.playershipstatus[5][10] = squadname;
                _root.playershipstatus[5][11] = true;
                _root.playershipstatus[5][13] = passwordd;
                datatosend = "SQUAD`" + _root.playershipstatus[3][0] + "`CHANGE`" + _root.playershipstatus[5][10] + "~";
                _root.mysocket.send(datatosend);
                gotoAndStop(1);
            }
            else
            {
                _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
                _parent.hardwarewarningbox.information = "Could Not Create!";
            } // end else if
        };
    } // end else if
}
