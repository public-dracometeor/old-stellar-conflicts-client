﻿// Action script...

// [onClipEvent of sprite 1266 in frame 6]
onClipEvent (enterFrame)
{
    ++currenttimesplayed;
    if (currenttimesplayed <= timestoplaysound)
    {
        guntype0sound.start();
    } // end if
    this.stars0._x = this.stars0._x - 1;
    this.stars1._x = this.stars1._x - 1;
    this.stars3._x = this.stars3._x - 1;
    this.stars2._x = this.stars2._x - 2;
    currenttime = getTimer();
    movement = (currenttime - lasttime) / timeallowed * enemygunsneedstomove;
    if (movementcomplete != true)
    {
        ship._rotation = ship._rotation + 0.500000;
        this.shot0._x = this.shot0._x + movement;
        this.shot1._x = this.shot1._x + movement;
        this.shot2._x = this.shot2._x + movement;
        this.shot3._x = this.shot3._x + movement;
        lasttime = currenttime;
    } // end if
    if (gunmovementendtime < currenttime && movementcomplete != true)
    {
        movementcomplete = true;
        _parent.attachMovie("shiptypedead", "explosion", 10);
        _parent.explosion._width = _parent.explosion._height = 90;
        _parent.explosion._x = ship._x - 15;
        _parent.explosion._y = ship._y - ship._height / 2 - 8;
        this.ship._visible = false;
        this.shot0._visible = false;
        this.shot1._visible = false;
        this.shot2._visible = false;
        this.shot3._visible = false;
    } // end if
}
