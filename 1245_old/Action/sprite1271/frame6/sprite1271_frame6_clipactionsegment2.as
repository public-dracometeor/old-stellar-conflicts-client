﻿// Action script...

// [onClipEvent of sprite 1266 in frame 6]
onClipEvent (load)
{
    guntype0sound = new Sound();
    guntype0sound.attachSound("guntype1sound");
    this.enemyship._x = _parent.enemyshipsx;
    this.enemyship._visible = true;
    enemygunsneedstomove = this.ship._x - this.enemyship._x - this.ship._width / 2;
    timeallowed = 1000;
    currenttime = getTimer();
    gunmovementendtime = currenttime + timeallowed;
    lasttime = currenttime;
    this.shot0._x = this.enemyship._x + 0;
    this.shot0._y = 15;
    this.shot1._x = this.enemyship._x + 5;
    this.shot1._y = 25;
    this.shot2._x = this.enemyship._x + 5;
    this.shot2._y = 58;
    this.shot3._x = this.enemyship._x + 0;
    this.shot3._y = 68;
    currenttimesplayed = 0;
    timestoplaysound = 4;
}
