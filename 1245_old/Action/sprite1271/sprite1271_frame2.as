﻿// Action script...

// [onClipEvent of sprite 1266 in frame 2]
onClipEvent (load)
{
    this.enemyship._visible = false;
    this.shot0._visible = false;
    this.shot1._visible = false;
    this.shot2._visible = false;
    this.shot3._visible = false;
}

// [onClipEvent of sprite 1266 in frame 2]
onClipEvent (enterFrame)
{
    this.stars0._x = this.stars0._x - 1;
    this.stars1._x = this.stars1._x - 1;
    this.stars3._x = this.stars3._x - 1;
    this.stars2._x = this.stars2._x - 2;
}
