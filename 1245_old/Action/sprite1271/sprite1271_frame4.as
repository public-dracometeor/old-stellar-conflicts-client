﻿// Action script...

// [onClipEvent of sprite 1266 in frame 4]
onClipEvent (load)
{
    this.enemyship._visible = true;
    enemyshipneedstomove = 250;
    timeallowed = 3000;
    currenttime = getTimer();
    lasttime = currenttime;
    this.shot0._visible = false;
    this.shot1._visible = false;
    this.shot2._visible = false;
    this.shot3._visible = false;
}

// [onClipEvent of sprite 1266 in frame 4]
onClipEvent (enterFrame)
{
    this.stars0._x = this.stars0._x - 1;
    this.stars1._x = this.stars1._x - 1;
    this.stars3._x = this.stars3._x - 1;
    this.stars2._x = this.stars2._x - 2;
    currenttime = getTimer();
    this.enemyship._x = this.enemyship._x + (currenttime - lasttime) / timeallowed * enemyshipneedstomove;
    lasttime = currenttime;
    _parent.enemyshipsx = this.enemyship._x;
}
