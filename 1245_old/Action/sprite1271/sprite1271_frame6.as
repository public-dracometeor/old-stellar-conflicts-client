﻿// Action script...

// [onClipEvent of sprite 1249 in frame 6]
onClipEvent (load)
{
    enemyshipneedstomove = 150;
    timeallowedformovement = 3000;
    lasttime = getTimer();
}

// [onClipEvent of sprite 1249 in frame 6]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    this.stars0._x = this.stars0._x - 1;
    this.stars1._x = this.stars1._x - 1;
    this.stars2._x = this.stars2._x - 2;
    this.enemyship._x = this.enemyship._x + (currenttime - lasttime) / 1000 * (enemyshipneedstomove / timeallowedformovement / 1000);
}

// [onClipEvent of sprite 1266 in frame 6]
onClipEvent (load)
{
    guntype0sound = new Sound();
    guntype0sound.attachSound("guntype1sound");
    this.enemyship._x = _parent.enemyshipsx;
    this.enemyship._visible = true;
    enemygunsneedstomove = this.ship._x - this.enemyship._x - this.ship._width / 2;
    timeallowed = 1000;
    currenttime = getTimer();
    gunmovementendtime = currenttime + timeallowed;
    lasttime = currenttime;
    this.shot0._x = this.enemyship._x + 0;
    this.shot0._y = 15;
    this.shot1._x = this.enemyship._x + 5;
    this.shot1._y = 25;
    this.shot2._x = this.enemyship._x + 5;
    this.shot2._y = 58;
    this.shot3._x = this.enemyship._x + 0;
    this.shot3._y = 68;
    currenttimesplayed = 0;
    timestoplaysound = 4;
}

// [onClipEvent of sprite 1266 in frame 6]
onClipEvent (enterFrame)
{
    ++currenttimesplayed;
    if (currenttimesplayed <= timestoplaysound)
    {
        guntype0sound.start();
    } // end if
    this.stars0._x = this.stars0._x - 1;
    this.stars1._x = this.stars1._x - 1;
    this.stars3._x = this.stars3._x - 1;
    this.stars2._x = this.stars2._x - 2;
    currenttime = getTimer();
    movement = (currenttime - lasttime) / timeallowed * enemygunsneedstomove;
    if (movementcomplete != true)
    {
        ship._rotation = ship._rotation + 0.500000;
        this.shot0._x = this.shot0._x + movement;
        this.shot1._x = this.shot1._x + movement;
        this.shot2._x = this.shot2._x + movement;
        this.shot3._x = this.shot3._x + movement;
        lasttime = currenttime;
    } // end if
    if (gunmovementendtime < currenttime && movementcomplete != true)
    {
        movementcomplete = true;
        _parent.attachMovie("shiptypedead", "explosion", 10);
        _parent.explosion._width = _parent.explosion._height = 90;
        _parent.explosion._x = ship._x - 15;
        _parent.explosion._y = ship._y - ship._height / 2 - 8;
        this.ship._visible = false;
        this.shot0._visible = false;
        this.shot1._visible = false;
        this.shot2._visible = false;
        this.shot3._visible = false;
    } // end if
}
