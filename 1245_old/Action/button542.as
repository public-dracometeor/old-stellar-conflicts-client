﻿// Action script...

on (release)
{
    if (rootvariable == "totalstars")
    {
        _root[rootvariable] = newnumber;
    } // end if
    if (rootvariable == "teamnumber")
    {
        desiredteamtochangeto = newnumber.substr(0, 4);
        if (!isNaN(desiredteamtochangeto) || desiredteamtochangeto == "N/A")
        {
            datatosend = "TC`" + _root.playershipstatus[3][0] + "`TM`" + desiredteamtochangeto + "~";
            _root.mysocket.send(datatosend);
            _root.gamechatinfo[1].splice(0, 0, 0);
            _root.gamechatinfo[1][0] = new Array();
            _root.gamechatinfo[1][0][0] = " Team Changing to : " + desiredteamtochangeto;
            _root.gamechatinfo[1][0][1] = _root.systemchattextcolor;
            if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
            {
                _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
            } // end if
            _root.refreshchatdisplay = true;
        } // end if
    } // end if
}
