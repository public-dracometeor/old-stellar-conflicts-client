﻿// Action script...

// [Action in Frame 1]
function fireamissile()
{
    this.missilexfire = _parent._x + this._x;
    this.missileyfire = _parent._y + this._y;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    playeranglefromship = 360 - Math.atan2(this.missilexfire - playersxcoord, this.missileyfire - playersycoord) / 0.017453;
    if (playeranglefromship > 360)
    {
        playeranglefromship = playeranglefromship - 360;
    } // end if
    if (playeranglefromship < 0)
    {
        playeranglefromship = playeranglefromship + 360;
    } // end if
    playeranglefromship = Math.round(playeranglefromship);
    ++_root.currentothermissileshot;
    if (_root.currentothermissileshot >= 1000)
    {
        _root.currentothermissileshot = 1;
    } // end if
    if (_root.othermissilefire.length < 1)
    {
        _root.othermissilefire = new Array();
    } // end if
    lastvar = _root.othermissilefire.length;
    _root.othermissilefire[lastvar] = new Array();
    _root.othermissilefire[lastvar][0] = 999999;
    _root.othermissilefire[lastvar][1] = this.missilexfire;
    _root.othermissilefire[lastvar][2] = this.missileyfire;
    velocity = _root.missile[missileshottype][0] * 2;
    _root.othermissilefire[lastvar][6] = playeranglefromship;
    _root.othermissilefire[lastvar][7] = missileshottype;
    _root.othermissilefire[lastvar][8] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][9] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][10] = "other";
    _root.othermissilefire[lastvar][5] = _root.curenttime + _root.missile[missileshottype][2];
    _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "fire", "othermissilefire" + _root.othermissilefire[lastvar][8], 999000 + _root.othermissilefire[lastvar][8]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _rotation, _root.othermissilefire[lastvar][6]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _x, _root.othermissilefire[lastvar][1] - _root.shipcoordinatex);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _y, _root.othermissilefire[lastvar][2] - _root.shipcoordinatey);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xposition", _root.othermissilefire[lastvar][1]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".yposition", _root.othermissilefire[lastvar][2]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".missiletype", missileshottype);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".velocity", velocity);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".seeking", "HOST");
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".shooterid", _root.othermissilefire[lastvar][0]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".globalid", _root.othermissilefire[lastvar][9]);
    _root.othermissilefire[lastvar][11] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._width / 2;
    _root.othermissilefire[lastvar][12] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._height / 2;
    _root.othermissilefire[lastvar][13] = "SEEKING";
} // End of the function
missilfireint = 2000;
lastmisileupdate = 0;
missileshottype = 9;
timetillnextmissile = 0;
this.onEnterFrame = function ()
{
    currenttime = getTimer();
    if (timetillnextmissile < currenttime)
    {
        if (_root.playershipstatus[2][5] > 0)
        {
            fireamissile();
            if (_root.soundvolume != "off")
            {
                _root.missilefiresound.start();
            } // end if
            timetillnextmissile = missilfireint + currenttime;
        } // end if
    } // end if
};
