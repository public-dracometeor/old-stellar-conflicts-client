﻿// Action script...

// [Action in Frame 1]
function currentkeyedshipmovement(keyspressed)
{
    keysbeingpressed = "";
    if (_root.gamedisplayarea.keyboardscript.isrightkeypressed == true)
    {
        keysbeingpressed = keysbeingpressed + "R";
    } // end if
    if (_root.gamedisplayarea.keyboardscript.isleftkeypressed == true)
    {
        keysbeingpressed = keysbeingpressed + "L";
    } // end if
    if (keysbeingpressed == "RL")
    {
        keysbeingpressed = "";
    } // end if
    if (_root.afterburnerinuse == true)
    {
        keysbeingpressed = keysbeingpressed + "S";
    }
    else
    {
        accelkeys = "";
        if (_root.gamedisplayarea.keyboardscript.isupkeypressed == true)
        {
            accelkeys = accelkeys + "U";
        } // end if
        if (_root.gamedisplayarea.keyboardscript.isdownkeypressed == true)
        {
            accelkeys = accelkeys + "D";
        } // end if
        if (accelkeys != "UD")
        {
            keysbeingpressed = keysbeingpressed + accelkeys;
        } // end if
    } // end else if
} // End of the function

function movementofanobjectwiththrust()
{
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    } // end if
    if (velocity != 0)
    {
        this.relativefacing = Math.round(this.relativefacing);
        ymovement = -velocity * _root.cosines[this.relativefacing];
        xmovement = velocity * _root.sines[this.relativefacing];
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function sendplayerinformation()
{
    globaltimestamp = String(getTimer() + _root.clocktimediff);
    globaltimestamp = Number(globaltimestamp.substr(globaltimestamp.length - 4));
    globaltimestamp = globaltimestamp + _root.keypressdelay;
    if (globaltimestamp > 10000)
    {
        globaltimestamp = globaltimestamp - 10000;
    } // end if
    currenttime = getTimer();
    keysarraypressed = new Array();
    for (i = 0; i < _root.keypressinwaiting.length; i++)
    {
        if (_root.keypressinwaiting[i][3] == false)
        {
            _root.keypressinwaiting[i][3] = true;
            if (_root.keypressinwaiting[i][0] == "UP")
            {
                if (_root.keypressinwaiting[i][1] == "PRESS")
                {
                    isupkeypressed = true;
                }
                else if (_root.keypressinwaiting[i][1] == "RELEASE")
                {
                    isupkeypressed = false;
                } // end else if
            }
            else if (_root.keypressinwaiting[i][0] == "DOWN")
            {
                if (_root.keypressinwaiting[i][1] == "PRESS")
                {
                    isdownkeypressed = true;
                }
                else if (_root.keypressinwaiting[i][1] == "RELEASE")
                {
                    isdownkeypressed = false;
                } // end else if
            }
            else if (_root.keypressinwaiting[i][0] == "LEFT")
            {
                if (_root.keypressinwaiting[i][1] == "PRESS")
                {
                    isleftkeypressed = true;
                }
                else if (_root.keypressinwaiting[i][1] == "RELEASE")
                {
                    isleftkeypressed = false;
                } // end else if
            }
            else if (_root.keypressinwaiting[i][0] == "RIGHT")
            {
                if (_root.keypressinwaiting[i][1] == "PRESS")
                {
                    isrightkeypressed = true;
                }
                else if (_root.keypressinwaiting[i][1] == "RELEASE")
                {
                    isrightkeypressed = false;
                } // end else if
            }
            else if (_root.keypressinwaiting[i][0] == "SHIFT")
            {
                if (_root.keypressinwaiting[i][1] == "PRESS")
                {
                    isshiftkeypressed = true;
                }
                else if (_root.keypressinwaiting[i][1] == "RELEASE")
                {
                    isshiftkeypressed = false;
                } // end else if
            } // end else if
            keysarraypressed[keysarraypressed.length] = _root.keypressinwaiting[i][2];
        } // end if
    } // end of for
    keysbeingpressed = "";
    newacceleration = false;
    if (isshiftkeypressed)
    {
        keysbeingpressed = keysbeingpressed + "S";
        newacceleration = "S";
    }
    else if (isupkeypressed && isdownkeypressed)
    {
    }
    else if (isupkeypressed)
    {
        keysbeingpressed = keysbeingpressed + "U";
        newacceleration = "U";
    }
    else if (isdownkeypressed)
    {
        keysbeingpressed = keysbeingpressed + "D";
        newacceleration = "D";
    } // end else if
    newturning = "";
    if (isleftkeypressed && isrightkeypressed)
    {
    }
    else if (isleftkeypressed)
    {
        keysbeingpressed = keysbeingpressed + "L";
        newturning = "L";
    }
    else if (isrightkeypressed)
    {
        keysbeingpressed = keysbeingpressed + "R";
        newturning = "R";
    } // end else if
    this.currentfacing = _root.playershipfacing;
    this.currentvelocity = _root.playershipvelocity;
    this.currentx = _root.shipcoordinatex;
    this.currenty = _root.shipcoordinatey;
    currenttime = getTimer();
    if (keysarraypressed[keysarraypressed.length][2] != null)
    {
        timechange = keysarraypressed[keysarraypressed.length][2] - currenttime;
    }
    else
    {
        timechange = _root.keypressdelay;
    } // end else if
    changemodifier = timechange / 1000;
    if (keysbeingpressed == "")
    {
        velocity = this.currentvelocity;
        this.relativefacing = this.currentfacing;
        movementofanobjectwiththrust();
        projectedx = Math.round(this.currentx + xmovement * changemodifier);
        projectedy = Math.round(this.currenty + ymovement * changemodifier);
    }
    else
    {
        projectedx = currentx;
        projectedy = currenty;
        velocity = currentvelocity;
        fpschangetouse = 33;
        changemodifier = fpschangetouse / 1000;
        relativefacing = Math.round(currentfacing);
        newshiprotation = currentfacing;
        for (i = 0; i < timechange; i = i + 33)
        {
            if (newacceleration != "")
            {
                if (newacceleration == "S")
                {
                    velocity = velocity + playershipacceleration * changemodifier * 2;
                    if (velocity > this.afterburnerspeed)
                    {
                        velocity = this.afterburnerspeed;
                    } // end if
                }
                else if (newacceleration == "U")
                {
                    velocity = velocity + playershipacceleration * changemodifier;
                    if (velocity > playershipmaxvelocity)
                    {
                        velocity = playershipmaxvelocity;
                    } // end if
                }
                else if (newacceleration == "D")
                {
                    velocity = velocity - playershipacceleration * changemodifier;
                    if (velocity < 0)
                    {
                        velocity = 0;
                    } // end if
                } // end else if
            } // end else if
            if (newturning != "")
            {
                shiprotating = 0;
                if (newturning == "L")
                {
                    shiprotating = shiprotating - this.playershiprotation;
                }
                else if (newturning == "R")
                {
                    shiprotating = shiprotating + this.playershiprotation;
                } // end else if
                if (shiprotating != 0)
                {
                    shiprotating = shiprotating - velocity / playershipmaxvelocity * playerrotationdegredation * shiprotating;
                    newshiprotation = currentfacing + shiprotating * changemodifier;
                    if (newshiprotation < 0)
                    {
                        newshiprotation = newshiprotation + 360;
                    } // end if
                    if (newshiprotation > 360)
                    {
                        newshiprotation = newshiprotation - 360;
                    } // end if
                    relativefacing = Math.round(newshiprotation);
                } // end if
            } // end if
            movementofanobjectwiththrust();
            projectedx = Math.round(this.currentx + xmovement * changemodifier);
            projectedy = Math.round(this.currenty + ymovement * changemodifier);
        } // end of for
    } // end else if
    information = "PI`" + lastsectoris + "`" + currentsectoris + "~PI" + "`" + _root.playershipstatus[3][0] + "`" + Math.round(projectedx) + "`" + Math.round(projectedy) + "`" + Math.round(newshiprotation) + "`" + Math.round(velocity) + "`" + keysbeingpressed + "`" + _root.playershipstatus[5][15] + "`" + globaltimestamp + "~" + _root.gunfireinformation + _root.ainformationtosend;
    _root.gunfireinformation = "";
    _root.ainformationtosend = "";
    if (_root.playershipstatus[5][4] != "dead")
    {
        _root.mysocket.send(information);
    } // end if
} // End of the function
this.playershipmaxvelocity = _root.playershipmaxvelocity;
this.playershipacceleration = _root.playershipacceleration;
playersshiptype = _root.playershipstatus[5][0];
this.playershiprotation = _root.shiptype[playersshiptype][3][2];
this.afterburnerspeed = _root.shiptype[playersshiptype][3][6];
playerrotationdegredation = _root.shiptype[playersshiptype][3][5];
