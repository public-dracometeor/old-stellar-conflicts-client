﻿// Action script...

on (release)
{
    this.value = onbuttonswitch;
    _root.soundvolume = onbuttonswitch;
    _root.musicvolume = "on";
    _root.mainscreenstarbase.func_eriemusic();
    _root.func_minor_clicksound();
    if (value == onbuttonswitch)
    {
        this.offbutton._alpha = 60;
        this.onbutton._alpha = 100;
    } // end if
    if (value == offbuttonswitch)
    {
        this.offbutton._alpha = 100;
        this.onbutton._alpha = 60;
    } // end if
}
