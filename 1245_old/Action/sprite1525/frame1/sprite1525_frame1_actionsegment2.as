﻿// Action script...

function func_creationofaship()
{
    shiptypetomake = 17;
    startlocationfound = false;
    for (q = 0; q < 30; q++)
    {
        numm = Math.round(Math.random() * _root.starbaselocation.length) - 1;
        if (_root.starbaselocation[numm][5] == "ACTIVE")
        {
            startlocationfound = true;
            startlocationloc = 0;
            startx = _root.starbaselocation[numm][1];
            starty = _root.starbaselocation[numm][2];
            break;
        } // end if
    } // end of for
    if (startlocationfound)
    {
        endlocationfound = false;
        for (q = 0; q < 30; q++)
        {
            numm = Math.round(Math.random() * _root.starbaselocation.length) - 1;
            if (_root.starbaselocation[numm][5] == "ACTIVE" && numm != startlocationloc && _root.starbaselocation[numm][0].substr(0, 2) == "PL")
            {
                endlocationfound = true;
                endx = _root.starbaselocation[numm][1];
                endy = _root.starbaselocation[numm][2];
                break;
            } // end if
        } // end of for
    } // end if
    delay = 0;
    numm = Math.round(Math.random() * 999999);
    globalid = "SHIP" + numm;
    name = "SHIP" + numm;
    turrettypes = 4;
    if (startlocationfound && endlocationfound)
    {
        datatobesend = "SA~NEUT`create`" + shiptypetomake + "`" + startx + "`" + starty + "`" + endx + "`" + endy + "`" + delay + "`" + globalid + "`" + name + "`" + turrettypes + "~";
        _root.mysocket.send(datatobesend);
    } // end if
} // End of the function
function func_getacurrentneutshiplevel()
{
    ++currentneutralshiplvl;
    if (currentneutralshiplvl > 200)
    {
        currentneutralshiplvl = 1;
    } // end if
    return (currentneutralshiplvl);
} // End of the function
function func_timetoreachdestination(shiptype, startx, starty, endx, endy)
{
    shipspeedaleration = _root.shipspeedalterationforneuts;
    travelvelocity = Math.round(_root.shiptype[shiptype][3][1]) / shipspeedaleration;
    playerdistancefromship = Math.round(Math.sqrt((startx - endx) * (startx - endx) + (starty - endy) * (starty - endy)));
    timetogetthere = Math.round(playerdistancefromship / travelvelocity * 1000);
    return (timetogetthere);
} // End of the function
function func_creationofnpcaiships(shiptype, xstart, ystart, xend, yend, delaytime, globalidname, shipname, turrettypes)
{
    lvl = func_getacurrentneutshiplevel();
    loc = _root.neutralships.length;
    _root.neutralships[loc] = new Array();
    _root.neutralships[loc][0] = Number(xstart);
    _root.neutralships[loc][1] = Number(ystart);
    _root.neutralships[loc][2] = Number(xend);
    _root.neutralships[loc][3] = Number(yend);
    _root.neutralships[loc][4] = Number(shiptype);
    if (delaytime != "N/A")
    {
        _root.neutralships[loc][6] = Number(delaytime) + getTimer();
        traveltime = func_timetoreachdestination(shiptype, xstart, ystart, xend, yend);
        _root.neutralships[loc][7] = Number(delaytime) + getTimer() + traveltime;
    }
    else
    {
        _root.neutralships[loc][6] = "N/A";
        _root.neutralships[loc][7] = "N/A";
    } // end else if
    _root.neutralships[loc][10] = globalidname;
    _root.neutralships[loc][11] = shipname;
    _root.neutralships[loc][12] = 5;
    _root.neutralships[loc][13] = lvl;
    _root.neutralships[loc][14] = null;
    _root.gamedisplayarea.func_drawneutralship(loc);
} // End of the function
function func_removeaship(shipid)
{
    for (tw = 0; tw < _root.neutralships.length; tw++)
    {
        if (String(shipid) == String(_root.neutralships[tw][10]))
        {
            _root.gamedisplayarea.func_removeneutralship(tw);
            _root.neutralships.splice(tw, 1);
            break;
        } // end if
    } // end of for
} // End of the function
_root.shipspeedalterationforneuts = 1.400000;
_root.neutralships = new Array();
currentneutralshiplvl = 1;
