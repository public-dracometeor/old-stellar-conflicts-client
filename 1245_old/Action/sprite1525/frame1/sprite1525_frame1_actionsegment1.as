﻿// Action script...

function func_pricechangeeffect(buyorsell, itemnum, expire, basenum, perchange)
{
    changeditemloc = null;
    for (iii = 0; iii < _root.starbasepricechanges.length; iii++)
    {
        if (_root.starbasepricechanges[i][0] == basetochange)
        {
            if (_root.starbasepricechanges[item][1] == itemtouseis)
            {
                changeditemloc = iii;
            } // end if
        } // end if
    } // end of for
    if (changeditemloc == null)
    {
        changeditemloc = _root.starbasepricechanges.length;
        _root.starbasepricechanges[changeditemloc] = new Array();
    } // end if
    _root.starbasepricechanges[changeditemloc][0] = basenum;
    _root.starbasepricechanges[changeditemloc][1] = itemnum;
    _root.starbasepricechanges[changeditemloc][2] = perchange;
    _root.starbasepricechanges[changeditemloc][3] = getTimer() + expire * 1000 * 60;
    buyorsell = Number(buyorsell);
    if (buyorsell == 0)
    {
        trasactionmessage = "offering a discount of";
    }
    else
    {
        trasactionmessage = "paying an additional rate of";
    } // end else if
    basename = _root.starbaselocation[basenum][0];
    if (basename.substr(0, 2) == "SB")
    {
        basename = "Starbase " + basename;
    }
    else if (basename.substr(0, 2) == "PL")
    {
        basename = "Planet " + basename.substr(2);
    } // end else if
    itemname = _root.tradegoods[itemnum][0];
} // End of the function
function func_altertradegooodpricing(currentbase)
{
    for (jj = 0; jj < _root.starbaselocation.length; jj++)
    {
        if (_root.starbaselocation[jj][0] == currentbase)
        {
            basieis = jj;
            break;
        } // end if
    } // end of for
    for (qq = 0; qq < _root.starbasetradeitem.length; qq++)
    {
        itemtype = _root.starbasetradeitem[qq][0];
        for (tt = 0; tt < _root.starbasepricechanges.length; tt++)
        {
            if (_root.starbasepricechanges[tt][0] == basieis)
            {
                if (_root.starbasepricechanges[tt][1] == itemtype)
                {
                    if (_root.starbasetradeitem[qq][2] == "buying")
                    {
                        _root.starbasetradeitem[qq][1] = _root.starbasetradeitem[qq][1] + Math.round(_root.starbasetradeitem[qq][1] * (_root.starbasepricechanges[tt][2] / 100));
                        continue;
                    } // end if
                    _root.starbasetradeitem[qq][1] = _root.starbasetradeitem[qq][1] - Math.round(_root.starbasetradeitem[qq][1] * (_root.starbasepricechanges[tt][2] / 100));
                } // end if
            } // end if
        } // end of for
    } // end of for
} // End of the function
function func_createapricechange()
{
    basetochange = func_getabasesgoodstochange();
    if (basetochange != null)
    {
        itemtochange = func_itemtochange(basetochange);
    } // end if
} // End of the function
function func_getabasesgoodstochange()
{
    noofbases = _root.starbaselocation.length;
    basetouse = null;
    for (jj = 0; jj < 20; jj++)
    {
        baseloc = Math.floor(Math.random() * noofbases);
        if (_root.starbaselocation[baseloc][5] == "ACTIVE" || Number(_root.starbaselocation[baseloc][5]) < getTimer())
        {
            basestatus = "ACTIVE";
            basetouse = baseloc;
            break;
        } // end if
    } // end of for
    return (basetouse);
} // End of the function
function func_itemtochange(basetochange)
{
    newsystemVars = new XML();
    newsystemVars.load(_root.pathtoaccounts + "tradegoods.php?system=" + _root.playershipstatus[5][1] + "&mode=items&starbase=" + _root.starbaselocation[basetochange][0]);
    newsystemVars.onLoad = function (success)
    {
        loadedvars = String(newsystemVars);
        if (loadedvars.length > 1)
        {
            itemsavail = func_determinegoods(loadedvars);
            buyorsell = Math.round(Math.random());
            itemtouseis = Math.floor(itemsavail[buyorsell].length * Math.random());
            func_sendoutitemchange(itemsavail[buyorsell][itemtouseis], buyorsell, basetochange);
        }
        else
        {
            _parent.message = "Failed to load system";
        } // end else if
    };
} // End of the function
function func_sendoutitemchange(itemtouseis, buyorsell, basetochange)
{
    changeitem = true;
    for (iii = 0; iii < _root.starbasepricechanges.length; iii++)
    {
        if (_root.starbasepricechanges[i][0] == basetochange)
        {
            if (_root.starbasepricechanges[item][1] == itemtouseis)
            {
                changeitem = false;
            } // end if
        } // end if
    } // end of for
    if (changeitem)
    {
        timelength = Math.ceil(Math.random() * 12) + 3;
        changerate = Math.round(Math.round(Math.random() * 7) * 5) + 10;
        datatosend = "SA~NEWS`PC`" + buyorsell + "`" + itemtouseis + "`" + timelength + "`" + basetochange + "`" + changerate + "~";
        _root.mysocket.send(datatosend);
    } // end if
} // End of the function
function func_determinegoods(loadedvars)
{
    infosplit = loadedvars.split("~");
    newinfo = infosplit[0].split("`");
    i = 0;
    currentgoods = new Array();
    currentgoods[0] = new Array();
    currentgoods[1] = new Array();
    while (i < newinfo.length - 1)
    {
        currentthread = newinfo[i].split("`");
        if (newinfo[i] == "S")
        {
            currentgoods[0][currentgoods[0].length] = i;
        } // end if
        if (newinfo[i] == "B")
        {
            currentgoods[1][currentgoods[1].length] = i;
        } // end if
        ++i;
    } // end while
    return (currentgoods);
} // End of the function
function func_checkforpriceresets()
{
    nowtime = getTimer();
    resetmessage = "";
    for (ttt = 0; ttt < _root.starbasepricechanges.length; ttt++)
    {
        if (_root.starbasepricechanges[ttt][3] < nowtime)
        {
            addtomessage = func_getdealendinfofor(ttt);
            resetmessage = resetmessage + String(addtomessage);
            _root.starbasepricechanges.splice(ttt, 1);
            --ttt;
        } // end if
    } // end of for
    if (resetmessage != "")
    {
    } // end if
} // End of the function
function func_getdealendinfofor(ttt)
{
    basename = _root.starbaselocation[_root.starbasepricechanges[ttt][0]][0];
    if (basename.substr(0, 2) == "PL")
    {
        basename = "Planet " + basename.substr(2);
    }
    else
    {
        basename = "Starbase " + basename;
    } // end else if
    itemname = _root.tradegoods[_root.starbasepricechanges[ttt][1]][0];
    messagetosend = "Deal at " + basename + " for " + itemname + " has expired. ";
    return (messagetosend);
} // End of the function
function func_printoutdealsforlist()
{
    textout = "";
    for (jt = 0; jt < _root.starbasepricechanges.length; jt++)
    {
        timeleft = _root.starbasepricechanges[jt][3] - getTimer();
        if (timeleft > 0)
        {
            basename = _root.starbaselocation[_root.starbasepricechanges[jt][0]][0];
            if (basename.substr(0, 2) == "PL")
            {
                basename = basename.substr(2);
            } // end if
            if (timeleft > 60000)
            {
                textout = textout + ("-" + _root.tradegoods[_root.starbasepricechanges[jt][1]][0] + " at " + basename + "\r  " + _root.starbasepricechanges[jt][2] + " % deal," + Math.floor(timeleft / 60000) + " minutes\r");
                continue;
            } // end if
            textout = textout + ("-" + _root.tradegoods[_root.starbasepricechanges[jt][1]][0] + " at " + basename + "\r  " + _root.starbasepricechanges[jt][2] + " % deal, expiring!" + "\r");
        } // end if
    } // end of for
    if (textout == "")
    {
        textout = "No Deals";
    } // end if
    return (textout);
} // End of the function
if (_root.starbasepricechanges.length < 1)
{
    _root.starbasepricechanges = new Array();
} // end if
