﻿// Action script...

// [Action in Frame 1]
function turretsmouseup()
{
    stopDrag ();
    _root.attachMovie("radarblot", "testdot", 99999);
    setProperty("_root.testdot", _x, _root._xmouse);
    setProperty("_root.testdot", _y, _root._ymouse);
    i = 0;
    hardpointmoved = false;
    while (i < _root.shiptype[playershiptype][5].length && sellingaturret == false && buyingaturret != true)
    {
        if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracket" + i))
        {
            if (i != hardpointselected)
            {
                hardpointmoved = true;
                hardpointmovedinto = i;
                extraplayersgunarray = _root.playershipstatus[8].length;
                _root.playershipstatus[8][extraplayersgunarray] = _root.playershipstatus[8][hardpointmovedfrom];
                _root.playershipstatus[8][hardpointmovedfrom] = _root.playershipstatus[8][hardpointmovedinto];
                _root.playershipstatus[8][hardpointmovedinto] = _root.playershipstatus[8][extraplayersgunarray];
                _root.playershipstatus[8].splice(extraplayersgunarray, 1);
                for (i = 0; i < _root.playershipstatus[8].length; i++)
                {
                    this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[8][i][0], "gun" + i, 12 + i);
                    setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][5][i][0] * gunlocationmultiplier);
                    setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][5][i][1] * gunlocationmultiplier);
                    set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[8][i][0]][6]);
                } // end of for
            } // end if
        } // end if
        ++i;
    } // end while
    i = hardpointmovedfrom;
    if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracketsell") && _root.playershipstatus[8][hardpointmovedfrom][0] != "none")
    {
        hardpointmoved = true;
        sellingaturret = true;
        attachMovie("hardwareinformationbutton", "hardwareinformationbutton", 99998);
        this.hardwareinformationbutton.question = "Sell: " + _root.guntype[_root.playershipstatus[8][hardpointmovedfrom][0]][6] + " \r" + "For: " + Math.round(_root.guntype[_root.playershipstatus[8][hardpointmovedfrom][0]][5] * turretcostmodifier / 2);
    } // end if
    if (hardpointmoved != true && sellingaturret == false)
    {
        setProperty("shipblueprint.gun" + hardpointmovedfrom, _x, _root.shiptype[playershiptype][5][hardpointmovedfrom][0] * gunlocationmultiplier);
        setProperty("shipblueprint.gun" + hardpointmovedfrom, _y, _root.shiptype[playershiptype][5][hardpointmovedfrom][1] * gunlocationmultiplier);
    } // end if
    removeMovieClip (_root.testdot);
} // End of the function
function turretsproccessoneachframe()
{
    if (itemselection != null && buyingaturret != true && sellingaturret != true)
    {
        itemturretselectiontype = itemselection.substring(0, 10);
        turretselectiontype = itemselection.substring(10);
        removeMovieClip (hardwarewarningbox);
    } // end if
    if (itemselection != null && (buyingaturret == true || sellingaturret == true))
    {
        itemselection = null;
    } // end if
    if (itemselection != null && itemturretselectiontype == "turrettype" && buyingaturret != true)
    {
        i = 0;
        emptyturretlocationavailable = false;
        while (i < _root.playershipstatus[8].length && emptyturretlocationavailable != true)
        {
            if (_root.playershipstatus[8][i][0] == "none")
            {
                emptyturretlocationavailable = true;
                emptyturretlocation = i;
            } // end if
            ++i;
        } // end while
        if (emptyturretlocationavailable == true)
        {
            buyingaturret = true;
            attachMovie("hardwareinformationbutton", "buyaturretquestion", 99998);
            this.buyaturretquestion.question = "Buy: " + _root.guntype[turretselectiontype][6] + " \r" + "For: " + _root.guntype[turretselectiontype][5] * turretcostmodifier;
        }
        else
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
            this.hardwarewarningbox.information = "You Need An Open Turret Point";
        } // end else if
        if (emptyturretlocationavailable == false)
        {
            itemselection = null;
            buyingaturret = false;
        } // end if
        itemselection = null;
    } // end if
    if (this.buyaturretquestion.requestedanswer == true && buyingaturret == true)
    {
        if (_root.playershipstatus[3][1] >= _root.guntype[turretselectiontype][5] * turretcostmodifier)
        {
            i = emptyturretlocation;
            _root.playershipstatus[8][i][0] = turretselectiontype;
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - _root.guntype[turretselectiontype][5] * turretcostmodifier;
            buyingaturret = false;
            removeMovieClip (this.buyaturretquestion);
            itemselection = null;
            removeMovieClip ("shipblueprint.gun" + i);
            this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[8][i][0], "gun" + i, 12 + i);
            setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][5][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][5][i][1] * gunlocationmultiplier);
            set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[8][i][0]][6]);
            emptyturretlocationavailable = null;
        }
        else
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
            this.hardwarewarningbox.information = "You Do Not Have Enough Funds";
            buyingaturret = false;
            removeMovieClip (this.buyaturretquestion);
            itemselection = null;
        } // end if
    } // end else if
    if (this.buyaturretquestion.requestedanswer == false && buyingaturret == true)
    {
        buyingaturret = false;
        removeMovieClip (this.buyaturretquestion);
        itemselection = null;
    } // end if
    if (sellingaturret == true && this.hardwareinformationbutton.requestedanswer == true)
    {
        i = hardpointmovedfrom;
        _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Math.round(_root.guntype[_root.playershipstatus[8][hardpointmovedfrom][0]][5] * turretcostmodifier / 2);
        _root.playershipstatus[8][i][0] = "none";
        this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[8][i][0], "gun" + i, 12 + i);
        setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][5][i][0] * gunlocationmultiplier);
        setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][5][i][1] * gunlocationmultiplier);
        removeMovieClip ("hardwareinformationbutton");
        sellingaturret = false;
    } // end if
    if (sellingaturret == true && this.hardwareinformationbutton.requestedanswer == false)
    {
        i = hardpointmovedfrom;
        setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][5][i][0] * gunlocationmultiplier);
        setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][5][i][1] * gunlocationmultiplier);
        set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[8][i][0]][6]);
        removeMovieClip ("hardwareinformationbutton");
        sellingaturret = false;
    } // end if
} // End of the function
stop ();
