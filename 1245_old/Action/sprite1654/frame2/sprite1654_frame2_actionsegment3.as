﻿// Action script...

costfromnormalgunratio = _root.squadbaseinfo[squadbasetype][2][0];
upgcost = "Cost: ";
maxupgllvl = _root.squadbaseinfo[squadbasetype][2][1];
upgradetolvl = 0;
turretnumber = 1;
maxturrets = _root.squadbaseinfo[squadbasetype][2][2];
currentlevel = _parent.turrets[turretnumber - 1];
lastturretnumber = -1;
gunname = "";
this.onEnterFrame = function ()
{
    if (lastturretnumber != turretnumber)
    {
        turretnumber = Math.round(turretnumber);
        if (isNaN(turretnumber) || turretnumber < 1 || turretnumber > maxturrets)
        {
            turretnumber = "";
        } // end if
        if (turretnumber > 0 && turretnumber < maxupgllvl + 1)
        {
            currentlevel = "Current Level: " + _parent.turrets[turretnumber - 1];
            upgradetolvl = "";
        } // end if
        lastturretnumber = turretnumber;
    } // end if
    if (upgradetolvl != lastupgradetolvl)
    {
        upgradetolvl = Math.round(upgradetolvl);
        currentturretlvl = _parent.turrets[turretnumber - 1];
        if (isNaN(currentturretlvl))
        {
            currentturretlvl = -1;
        } // end if
        if (isNaN(upgradetolvl) || upgradetolvl <= currentturretlvl || upgradetolvl > maxupgllvl)
        {
            upgradetolvl = "";
        } // end if
        if (upgradetolvl > maxupgllvl)
        {
            upgradetolvl = "";
        } // end if
        upgradecost = _root.guntype[upgradetolvl][5] * costfromnormalgunratio - _root.guntype[currentturretlvl][5] * costfromnormalgunratio;
        gunname = _root.guntype[upgradetolvl][6];
        upgcost = "Cost: " + upgradecost;
        lastupgradetolvl = upgradetolvl;
    } // end if
};
stop ();
