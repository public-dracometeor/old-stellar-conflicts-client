﻿// Action script...

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "UPGRADE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    turretnumber = _parent.turretnumber;
    upgradetolvl = _parent.upgradetolvl;
    upgradecost = _parent.upgradecost;
    if (turretnumber > 0 && !isNaN(upgradetolvl) && upgradecost <= _root.playershipstatus[3][1])
    {
        turretnumbertoupg = turretnumber - 1;
        turrets = new Array();
        turrets[0] = _parent._parent.turrets[0];
        turrets[1] = _parent._parent.turrets[1];
        turrets[2] = _parent._parent.turrets[2];
        turrets[3] = _parent._parent.turrets[3];
        turrets[4] = _parent._parent.turrets[4];
        upgradeturretto = upgradetolvl;
        turrets[turretnumbertoupg] = upgradeturretto;
        this.newbaseVars = new XML();
        this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=changegun&baseid=" + _parent._parent.baseidname + "&gunone=" + turrets[0] + "&guntwo=" + turrets[1] + "&gunthree=" + turrets[2] + "&gunfour=" + turrets[3] + "&gunfive=" + turrets[4]);
        this.newbaseVars.onLoad = function (success)
        {
            loadedvars = String(newbaseVars);
            if (loadedvars == "destroyed")
            {
                _parent.gotoAndStop("initialize");
            }
            else if (loadedvars == "completed")
            {
                _parent.costinvolved = upgradecost;
                _parent.successful = true;
                _parent._parent.turrets[0] = turrets[0];
                _parent._parent.turret1disp = "1: " + _root.guntype[turrets[0]][6];
                _parent._parent.turrets[1] = turrets[1];
                _parent._parent.turret2disp = "2: " + _root.guntype[turrets[1]][6];
                _parent._parent.turrets[2] = turrets[2];
                _parent._parent.turret3disp = "3: " + _root.guntype[turrets[2]][6];
                _parent._parent.turrets[3] = turrets[3];
                _parent._parent.turret4disp = "4: " + _root.guntype[turrets[3]][6];
                _parent._parent.turrets[4] = turrets[4];
                _parent._parent.turret5disp = "5: " + _root.guntype[turrets[4]][6];
                _parent.gotoAndStop("completed");
            }
            else
            {
                _parent.costinvolved = upgradecost;
                _parent.successful = false;
                _parent.gotoAndStop("completed");
            } // end else if
        };
    } // end if
}

// [Action in Frame 2]
costfromnormalgunratio = _root.squadbaseinfo[squadbasetype][2][0];
upgcost = "Cost: ";
maxupgllvl = _root.squadbaseinfo[squadbasetype][2][1];
upgradetolvl = 0;
turretnumber = 1;
maxturrets = _root.squadbaseinfo[squadbasetype][2][2];
currentlevel = _parent.turrets[turretnumber - 1];
lastturretnumber = -1;
gunname = "";
this.onEnterFrame = function ()
{
    if (lastturretnumber != turretnumber)
    {
        turretnumber = Math.round(turretnumber);
        if (isNaN(turretnumber) || turretnumber < 1 || turretnumber > maxturrets)
        {
            turretnumber = "";
        } // end if
        if (turretnumber > 0 && turretnumber < maxupgllvl + 1)
        {
            currentlevel = "Current Level: " + _parent.turrets[turretnumber - 1];
            upgradetolvl = "";
        } // end if
        lastturretnumber = turretnumber;
    } // end if
    if (upgradetolvl != lastupgradetolvl)
    {
        upgradetolvl = Math.round(upgradetolvl);
        currentturretlvl = _parent.turrets[turretnumber - 1];
        if (isNaN(currentturretlvl))
        {
            currentturretlvl = -1;
        } // end if
        if (isNaN(upgradetolvl) || upgradetolvl <= currentturretlvl || upgradetolvl > maxupgllvl)
        {
            upgradetolvl = "";
        } // end if
        if (upgradetolvl > maxupgllvl)
        {
            upgradetolvl = "";
        } // end if
        upgradecost = _root.guntype[upgradetolvl][5] * costfromnormalgunratio - _root.guntype[currentturretlvl][5] * costfromnormalgunratio;
        gunname = _root.guntype[upgradetolvl][6];
        upgcost = "Cost: " + upgradecost;
        lastupgradetolvl = upgradetolvl;
    } // end if
};
stop ();
