﻿// Action script...

// [onClipEvent of sprite 1065 in frame 6]
on (release)
{
    missilecosts = _parent.missilecosts;
    missilestobuy = _parent.missileshotsbuying;
    if (_parent.missilecosts > 0)
    {
        if (_parent.missilecosts <= _root.playershipstatus[3][1])
        {
            this.newbaseVars = new XML();
            newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=missileshot&baseid=" + _parent._parent.baseidname + "&shots=-" + missilestobuy);
            newbaseVars.onLoad = function (success)
            {
                loadedvars = String(newbaseVars);
                if (loadedvars == "destroyed")
                {
                    _parent.gotoAndStop("initialize");
                }
                else if (!isNaN(Number(loadedvars)) && loadedvars != "")
                {
                    _parent.costinvolved = missilecosts;
                    _parent.successful = true;
                    _parent._parent.missilerounds = Number(loadedvars);
                    _parent._parent.missileammodisp = "Shots: " + _parent._parent.missilerounds;
                    _parent.gotoAndStop("completed");
                }
                else
                {
                    _parent.costinvolved = missilecosts;
                    _parent.successful = false;
                    _parent.gotoAndStop("completed");
                } // end else if
            };
        } // end if
    } // end if
}
