﻿// Action script...

// [Action in Frame 7]
if (successful == true)
{
    resultdisp = "The Operation was completed Successfully \r";
    if (costinvolved != "NONE")
    {
        _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - costinvolved;
        _parent.fundsdisplay.funds = _root.playershipstatus[3][1];
        _root.savegamescript.saveplayersgame(this);
        resultdisp = resultdisp + (costinvolved + " Funds have been used.");
    } // end if
}
else
{
    resultdisp = "The Operation was NOT completed\r";
    if (costinvolved != "NONE")
    {
        resultdisp = resultdisp + (costinvolved + " Funds were not used.\r");
    } // end if
    resultdisp = resultdisp + "Please Try Again.";
} // end else if
this.onEnterFrame = function ()
{
};
stop ();
