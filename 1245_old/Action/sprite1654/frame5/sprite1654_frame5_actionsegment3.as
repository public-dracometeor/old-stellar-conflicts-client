﻿// Action script...

repaircostperstruct = _root.squadbaseinfo[squadbasetype][3][0];
maxstructure = _parent.maxstructure;
currentstructure = _parent.totalstructure;
amouttorepair = maxstructure - currentstructure;
this.onEnterFrame = function ()
{
    if (amouttorepair != lastamouttorepair)
    {
        if (isNaN(amouttorepair))
        {
            amouttorepair = 0;
        } // end if
        if (amouttorepair < 0)
        {
            amouttorepair = 0;
        } // end if
        if (amouttorepair > maxstructure)
        {
            amouttorepair = maxstructure - currentstructure;
        } // end if
        repaircost = amouttorepair * repaircostperstruct;
        repaircostdisp = "Cost: " + repaircost;
        lastamouttorepair = amouttorepair;
    } // end if
};
stop ();
