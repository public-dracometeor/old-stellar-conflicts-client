﻿// Action script...

// [onClipEvent of sprite 1065 in frame 5]
on (release)
{
    repaircost = _parent.repaircost;
    if (_parent.repaircost > 0)
    {
        if (_parent.repaircost <= _root.playershipstatus[3][1])
        {
            this.newbaseVars = new XML();
            this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=repairbase&baseid=" + _parent._parent.baseidname + "&struct=" + _parent.amouttorepair + "&maxstruct=" + _parent.maxstructure);
            this.newbaseVars.onLoad = function (success)
            {
                loadedvars = String(newbaseVars);
                if (loadedvars == "destroyed")
                {
                    _parent.gotoAndStop("initialize");
                }
                else if (!isNaN(Number(loadedvars)) && loadedvars != "")
                {
                    _parent.costinvolved = repaircost;
                    _parent.successful = true;
                    _parent._parent.totalstructure = Number(loadedvars);
                    _parent._parent.structuredisp = "Structure: " + _parent._parent.totalstructure + " / " + _parent._parent.maxstructure;
                    _parent.gotoAndStop("completed");
                }
                else
                {
                    _parent.costinvolved = repaircost;
                    _parent.successful = false;
                    _parent.gotoAndStop("completed");
                } // end else if
            };
        } // end if
    } // end if
}
