﻿// Action script...

// [onClipEvent of sprite 1065 in frame 5]
onClipEvent (load)
{
    this.label = "REPAIR";
}

// [onClipEvent of sprite 1065 in frame 5]
on (release)
{
    repaircost = _parent.repaircost;
    if (_parent.repaircost > 0)
    {
        if (_parent.repaircost <= _root.playershipstatus[3][1])
        {
            this.newbaseVars = new XML();
            this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=repairbase&baseid=" + _parent._parent.baseidname + "&struct=" + _parent.amouttorepair + "&maxstruct=" + _parent.maxstructure);
            this.newbaseVars.onLoad = function (success)
            {
                loadedvars = String(newbaseVars);
                if (loadedvars == "destroyed")
                {
                    _parent.gotoAndStop("initialize");
                }
                else if (!isNaN(Number(loadedvars)) && loadedvars != "")
                {
                    _parent.costinvolved = repaircost;
                    _parent.successful = true;
                    _parent._parent.totalstructure = Number(loadedvars);
                    _parent._parent.structuredisp = "Structure: " + _parent._parent.totalstructure + " / " + _parent._parent.maxstructure;
                    _parent.gotoAndStop("completed");
                }
                else
                {
                    _parent.costinvolved = repaircost;
                    _parent.successful = false;
                    _parent.gotoAndStop("completed");
                } // end else if
            };
        } // end if
    } // end if
}

// [Action in Frame 5]
repaircostperstruct = _root.squadbaseinfo[squadbasetype][3][0];
maxstructure = _parent.maxstructure;
currentstructure = _parent.totalstructure;
amouttorepair = maxstructure - currentstructure;
this.onEnterFrame = function ()
{
    if (amouttorepair != lastamouttorepair)
    {
        if (isNaN(amouttorepair))
        {
            amouttorepair = 0;
        } // end if
        if (amouttorepair < 0)
        {
            amouttorepair = 0;
        } // end if
        if (amouttorepair > maxstructure)
        {
            amouttorepair = maxstructure - currentstructure;
        } // end if
        repaircost = amouttorepair * repaircostperstruct;
        repaircostdisp = "Cost: " + repaircost;
        lastamouttorepair = amouttorepair;
    } // end if
};
stop ();
