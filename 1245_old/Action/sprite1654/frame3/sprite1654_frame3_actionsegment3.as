﻿// Action script...

costfromnormalgunratio = 10;
upgcost = "Cost: ";
maxupgllvl = _root.guntype.length - 1;
upgradetolvl = 0;
turretnumber = 1;
changeturretonumber = 2;
maxturrets = 4;
currentlevel = _parent.turrets[turretnumber - 1];
this.onEnterFrame = function ()
{
    if (lastturretnumber != turretnumber)
    {
        turretnumber = Math.round(turretnumber);
        if (isNaN(turretnumber) || turretnumber < 1 || turretnumber > maxturrets)
        {
            turretnumber = "";
        } // end if
        if (turretnumber > 0 && turretnumber < maxupgllvl + 1)
        {
        } // end if
        lastturretnumber = turretnumber;
        lastchangeturretonumber = "";
    } // end if
    if (changeturretonumber != lastchangeturretonumber)
    {
        changeturretonumber = Math.round(changeturretonumber);
        if (isNaN(changeturretonumber) || changeturretonumber < 1 || changeturretonumber > maxturrets || changeturretonumber == turretnumber)
        {
            changeturretonumber = "";
        } // end if
        if (changeturretonumber > 0 && changeturretonumber < maxupgllvl + 1)
        {
        } // end if
        lastchangeturretonumber = changeturretonumber;
    } // end if
};
stop ();
