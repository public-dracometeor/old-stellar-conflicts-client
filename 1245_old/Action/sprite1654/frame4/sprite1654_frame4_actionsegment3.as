﻿// Action script...

maxgenlvl = _root.squadbaseinfo[squadbasetype][1][3];
maxlvldisp = "Level " + (maxgenlvl + 1);
currentgenlvl = _parent.shieldgenerator;
changeshieldto = currentgenlvl + 2;
genlvldisp = _root.shieldgenerators[currentgenlvl][4];
costmultiplier = _root.squadbaseinfo[squadbasetype][1][0];
this.onEnterFrame = function ()
{
    if (changeshieldto != lastchangeshieldto)
    {
        if (isNaN(changeshieldto))
        {
            changeshieldto = -1;
        } // end if
        if (changeshieldto - 1 <= currentgenlvl || changeshieldto - 1 > maxgenlvl)
        {
            changeshieldto = "";
        } // end if
        upgradecost = _root.shieldgenerators[changeshieldto - 1][5] * costmultiplier - _root.shieldgenerators[currentgenlvl][5] * costmultiplier;
        shieldcostdisp = "Cost: " + upgradecost;
        lastchangeshieldto = changeshieldto;
    } // end if
};
stop ();
