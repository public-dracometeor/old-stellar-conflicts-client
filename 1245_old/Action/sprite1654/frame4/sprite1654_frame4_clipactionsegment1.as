﻿// Action script...

// [onClipEvent of sprite 1065 in frame 4]
on (release)
{
    newshieldgen = _parent.changeshieldto - 1;
    upgradecost = _parent.upgradecost;
    if (_parent.upgradecost > 0)
    {
        if (upgradecost <= _root.playershipstatus[3][1])
        {
            this.newbaseVars = new XML();
            this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=newshield&baseid=" + _parent._parent.baseidname + "&newshield=" + newshieldgen);
            this.newbaseVars.onLoad = function (success)
            {
                loadedvars = String(newbaseVars);
                if (loadedvars == "destroyed")
                {
                    _parent.gotoAndStop("initialize");
                }
                else if (loadedvars == "completed")
                {
                    _parent.costinvolved = upgradecost;
                    _parent.successful = true;
                    _parent._parent.shieldgenerator = newshieldgen;
                    _parent._parent.generatordisp = _root.shieldgenerators[newshieldgen][4];
                    _parent._parent.maxchargedisp = "Max Charge: " + _root.shieldgenerators[newshieldgen][0] * 10;
                    _parent._parent.chargeratedisp = "Charge Rate: " + _root.shieldgenerators[newshieldgen][1] * 5 + "/sec";
                    _parent.gotoAndStop("completed");
                }
                else
                {
                    _parent.costinvolved = upgradecost;
                    _parent.successful = false;
                    _parent.gotoAndStop("completed");
                } // end else if
            };
        } // end if
    } // end if
}
