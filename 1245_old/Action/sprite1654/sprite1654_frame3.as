﻿// Action script...

// [onClipEvent of sprite 1065 in frame 3]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 3]
on (release)
{
    if (_parent.turretnumber > 0 && _parent.changeturretonumber > 0)
    {
        turrets = new Array();
        turrets[0] = _parent._parent.turrets[0];
        turrets[1] = _parent._parent.turrets[1];
        turrets[2] = _parent._parent.turrets[2];
        turrets[3] = _parent._parent.turrets[3];
        turrets[4] = _parent._parent.turrets[4];
        changingturretfrom = _parent.turretnumber - 1;
        changingturretto = _parent.changeturretonumber - 1;
        turretfiller = turrets[changingturretto];
        turrets[changingturretto] = turrets[changingturretfrom];
        turrets[changingturretfrom] = turretfiller;
        this.newbaseVars = new XML();
        this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=changegun&baseid=" + _parent._parent.baseidname + "&gunone=" + turrets[0] + "&guntwo=" + turrets[1] + "&gunthree=" + turrets[2] + "&gunfour=" + turrets[3] + "&gunfive=" + turrets[4]);
        this.newbaseVars.onLoad = function (success)
        {
            loadedvars = String(newbaseVars);
            if (loadedvars == "destroyed")
            {
                _parent.gotoAndStop("initialize");
            }
            else if (loadedvars == "completed")
            {
                _parent.costinvolved = "NONE";
                _parent.successful = true;
                _parent._parent.turrets[0] = turrets[0];
                _parent._parent.turret1disp = "1: " + _root.guntype[turrets[0]][6];
                _parent._parent.turrets[1] = turrets[1];
                _parent._parent.turret2disp = "2: " + _root.guntype[turrets[1]][6];
                _parent._parent.turrets[2] = turrets[2];
                _parent._parent.turret3disp = "3: " + _root.guntype[turrets[2]][6];
                _parent._parent.turrets[3] = turrets[3];
                _parent._parent.turret4disp = "4: " + _root.guntype[turrets[3]][6];
                _parent._parent.turrets[4] = turrets[4];
                _parent._parent.turret5disp = "5: " + _root.guntype[turrets[4]][6];
                _parent.gotoAndStop("completed");
            }
            else
            {
                _parent.costinvolved = "NONE";
                _parent.successful = false;
                _parent.gotoAndStop("completed");
            } // end else if
        };
    } // end if
}

// [Action in Frame 3]
costfromnormalgunratio = 10;
upgcost = "Cost: ";
maxupgllvl = _root.guntype.length - 1;
upgradetolvl = 0;
turretnumber = 1;
changeturretonumber = 2;
maxturrets = 4;
currentlevel = _parent.turrets[turretnumber - 1];
this.onEnterFrame = function ()
{
    if (lastturretnumber != turretnumber)
    {
        turretnumber = Math.round(turretnumber);
        if (isNaN(turretnumber) || turretnumber < 1 || turretnumber > maxturrets)
        {
            turretnumber = "";
        } // end if
        if (turretnumber > 0 && turretnumber < maxupgllvl + 1)
        {
        } // end if
        lastturretnumber = turretnumber;
        lastchangeturretonumber = "";
    } // end if
    if (changeturretonumber != lastchangeturretonumber)
    {
        changeturretonumber = Math.round(changeturretonumber);
        if (isNaN(changeturretonumber) || changeturretonumber < 1 || changeturretonumber > maxturrets || changeturretonumber == turretnumber)
        {
            changeturretonumber = "";
        } // end if
        if (changeturretonumber > 0 && changeturretonumber < maxupgllvl + 1)
        {
        } // end if
        lastchangeturretonumber = changeturretonumber;
    } // end if
};
stop ();
