﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    field = _parent.field;
    changinto = _parent.changinto.toUpperCase();
    if (changinto.length < 3 || changinto > 16)
    {
        _parent.attachMovie("shipwarningbox", "shipwarningbox", 1555);
        _parent.shipwarningbox.information = "Must be more than 2 characters and\rless that 17";
    }
    else if (field == "basestatus" && changinto != "HOSTILE" && changinto != "NEUTRAL" && changinto != "FRIENDLY")
    {
        _parent.attachMovie("shipwarningbox", "shipwarningbox", 1555);
        _parent.shipwarningbox.information = "Must be HOSTILE, NEUTRAL, \r or FRIENDLY ";
        _parent.changinto = "";
    }
    else
    {
        _parent._parent.changething(field, changinto);
    } // end else if
}
