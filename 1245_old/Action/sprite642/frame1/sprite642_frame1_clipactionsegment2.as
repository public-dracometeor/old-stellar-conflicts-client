﻿// Action script...

// [onClipEvent of sprite 641 in frame 1]
onClipEvent (enterFrame)
{
    if (moveitemsdisplayed != null)
    {
        currentleftmostitem = currentleftmostitem + moveitemsdisplayed;
        moveitemsdisplayed = null;
        if (currentleftmostitem < 0)
        {
            currentleftmostitem = 0;
        } // end if
        if (currentleftmostitem > _root.missionsavailable)
        {
            currentleftmostitem = currentleftmostitem - 1;
        } // end if
        drawmissions(currentleftmostitem);
    } // end if
}
