﻿// Action script...

// [onClipEvent of sprite 641 in frame 1]
onClipEvent (enterFrame)
{
    function acceptmissioin(i)
    {
        _root.playersmission = new Array();
        _root.playersmission[0] = new Array();
        _root.playersmission[0][0] = new Array();
        if (i < _root.missionsavailable.length)
        {
            missioninfo = _root.missionsavailable[i][0].split("`");
        }
        else
        {
            num = i - _root.missionsavailable.length;
            missioninfo = assasarray[num][0].split("`");
        } // end else if
        if (missioninfo[0] == "Patrol" || missioninfo[0] == "Recon")
        {
            if (missioninfo[1].substr(0, 2) == "NP")
            {
                missioninfo[1] = "Nav " + missioninfo[1].substr(2);
            } // end if
            _root.playersmission[0][0][0] = missioninfo[0];
            _root.playersmission[0][0][1] = missioninfo[1];
            _root.playersmission[0][0][2] = i;
            _root.playersmission[0][1] = new Array();
            _root.playersmission[0][1][0] = new Array();
            _root.playersmission[0][1][0][0] = "Nav1";
            _root.playersmission[0][1][0][1] = _root.missionsavailable[i][1];
            _root.playersmission[0][1][0][2] = _root.missionsavailable[i][2];
            _root.playersmission[0][1][0][3] = "Pending";
            _root.playersmission[0][1][0][4] = Math.round(Math.random() * Number(_root.missionsavailable[i][5]));
            if (missioninfo[0] == "Patrol" && _root.playersmission[0][1][0][4] <= 0)
            {
                _root.playersmission[0][1][0][4] = 1;
            } // end if
            _root.playersmission[0][2] = _root.missionsavailable[i][6];
            _root.playersmission[0][3] = _root.missionsavailable[i][3];
            _root.playersmission[0][4] = _root.missionsavailable[i][4];
        }
        else if (missioninfo[0] == "Cargorun")
        {
            _root.playersmission[0][0][0] = missioninfo[0];
            _root.playersmission[0][0][1] = missioninfo[1] + "`" + missioninfo[2];
            _root.playersmission[0][0][2] = i;
            _root.playersmission[0][2] = _root.missionsavailable[i][6] + "`" + _root.missionsavailable[i][7];
            _root.playersmission[0][3] = _root.missionsavailable[i][3];
            _root.playersmission[0][4] = false;
            if (missioninfo[1] == _root.playershipstatus[4][0])
            {
                _root.playersmission[0][4] = true;
                enterintochat(" Misson Cargo Received, Mission begins when you exit the base.", _root.systemchattextcolor);
            } // end if
            num = i - _root.missionsavailable.length;
            missioninfosplit = assasarray[num][0].split("`");
        }
        else if (missioninfo[0] == "assasinate")
        {
            _root.playersmission[0][0][0] = missioninfo[0];
            _root.playersmission[0][0][1] = missioninfo[1];
            _root.playersmission[0][0][2] = i;
            _root.playersmission[0][2] = assasarray[num][6];
            _root.func_assmissionbeg(missioninfo[1]);
        } // end else if
    } // End of the function
    if (itemselection != null)
    {
    } // end if
    if (itemselection != null && waitingforanswer != true)
    {
        removeMovieClip (this.shipwarningbox);
        this.attachMovie("buyingashipquestion", "buyingashipquestion", 9980);
        if (itemselection < _root.missionsavailable.length)
        {
            missioninfosplit = _root.missionsavailable[itemselection][0].split("`");
            if (missioninfosplit[1].substr(0, 2) == "NP")
            {
                missioninfosplit[1] = "Nav " + missioninfosplit[1].substr(2);
                missiontitle = missioninfosplit[0] + " at " + missioninfosplit[1];
            } // end if
        }
        else
        {
            num = itemselection - _root.missionsavailable.length;
            missioninfosplit = assasarray[num][0].split("`");
            missiontitle = "Assassinate " + missioninfosplit[1];
        } // end else if
        this.buyingashipquestion.question = "Accept Mission:\r" + missiontitle;
        waitingforanswer = true;
    } // end if
    if (this.buyingashipquestion.requestedanswer == true && waitingforanswer == true)
    {
        acceptmissioin(itemselection);
        removeMovieClip (this.buyingashipquestion);
        drawmissions(currentleftmostitem);
        itemselection = null;
        waitingforanswer = false;
    } // end if
    if (this.buyingashipquestion.requestedanswer == false && waitingforanswer == true)
    {
        waitingforanswer = false;
        removeMovieClip (this.buyingashipquestion);
        itemselection = null;
    } // end if
}
