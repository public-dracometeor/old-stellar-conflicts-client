﻿// Action script...

on (release)
{
    currentsystem = _root.playershipstatus[5][1];
    xwidthofsector = _root.sectorinformation[1][0];
    ywidthofsector = _root.sectorinformation[1][1];
    squadname = _root.playershipstatus[5][10];
    desiredx = Math.round(xgrid);
    desiredy = Math.round(ygrid);
    isgridtaken = false;
    for (i = 0; i < _root.sectormapitems.length; i++)
    {
        if (desiredx == Math.ceil(_root.sectormapitems[i][1] / xwidthofsector) && desiredy == Math.ceil(_root.sectormapitems[i][2] / ywidthofsector))
        {
            isgridtaken = true;
            break;
        } // end if
    } // end of for
    if (isgridtaken == true)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "Grid Occupied!";
    }
    else
    {
        xcoord = xwidthofsector * desiredx - Math.round(xwidthofsector / 2);
        ycoord = ywidthofsector * desiredy - Math.round(ywidthofsector / 2);
    } // end else if
    xsector = desiredx;
    ysector = desiredy;
    gridsaroundtaken = false;
    for (i = 0; i < _root.sectormapitems.length; i++)
    {
        if (_root.sectormapitems[i][10] == xsector || _root.sectormapitems[i][10] == xsector - 1 || _root.sectormapitems[i][10] == xsector + 1)
        {
            if (_root.sectormapitems[i][11] == ysector || _root.sectormapitems[i][11] == ysector - 1 || _root.sectormapitems[i][11] == ysector + 1)
            {
                gridsaroundtaken = true;
            } // end if
        } // end if
    } // end of for
    for (i = 0; i < _root.playersquadbases.length; i++)
    {
        if (_root.playersquadbases[i][4] == xsector || _root.playersquadbases[i][4] == xsector - 1 || _root.playersquadbases[i][4] == xsector + 1)
        {
            if (_root.playersquadbases[i][5] == ysector || _root.playersquadbases[i][5] == ysector - 1 || _root.playersquadbases[i][5] == ysector + 1)
            {
                gridsaroundtaken = true;
            } // end if
        } // end if
    } // end of for
    if (gridsaroundtaken == true)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "To Close to Something";
    } // end if
    if (xgrid != "" && ygrid != "" && gridsaroundtaken == false)
    {
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadbases.php?mode=createbase&baseid=" + squadname + "&system=" + currentsystem + "&xcoord=" + xcoord + "&ycoord=" + ycoord);
        _root.testtt = currentsystem;
        newsquadVars.onLoad = function (success)
        {
            loadedinfo = String(newsquadVars);
            if (loadedinfo == "occupied")
            {
                _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
                _parent.hardwarewarningbox.information = "Base Already There";
            }
            else if (loadedinfo == "alreadyone")
            {
                _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
                _parent.hardwarewarningbox.information = "You Have A Base";
            }
            else
            {
                datatosend = "SA~NEWS`SB`CREATED`" + squadname + "`0`" + xcoord + "`" + ycoord + "~";
                _root.mysocket.send(datatosend);
                gotoAndPlay(3);
            } // end else if
        };
    } // end if
}
