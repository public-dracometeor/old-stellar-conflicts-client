﻿// Action script...

// [onClipEvent of sprite 527 in frame 1]
on (release)
{
    if (operation == "Sell Ship")
    {
        moneyreturn = 0;
        moneyreturn = moneyreturn + _root.shiptype[_root.extraplayerships[_parent.shipid][0]][1] / 2;
        moneyreturn = moneyreturn + _root.shieldgenerators[_root.extraplayerships[_parent.shipid][1]][5] / 4;
        moneyreturn = moneyreturn + _root.energygenerators[_root.extraplayerships[_parent.shipid][2]][2] / 4;
        moneyreturn = moneyreturn + _root.energycapacitors[_root.extraplayerships[_parent.shipid][3]][2] / 4;
        for (currentharpoint = 0; currentharpoint < _root.shiptype[_root.extraplayerships[_parent.shipid][0]][2].length; currentharpoint++)
        {
            if (!isNaN(Number(_root.extraplayerships[_parent.shipid][4][currentharpoint])))
            {
                moneyreturn = moneyreturn + _root.guntype[_root.extraplayerships[_parent.shipid][4][currentharpoint]][5] / 4;
            } // end if
        } // end of for
        for (currentturretpoint = 0; currentturretpoint < _root.shiptype[_root.extraplayerships[_parent.shipid][0]][5].length; currentturretpoint++)
        {
            if (!isNaN(Number(_root.extraplayerships[_parent.shipid][5][currentturretpoint])))
            {
                moneyreturn = moneyreturn + _root.guntype[_root.extraplayerships[_parent.shipid][5][currentturretpoint]][5] / 2;
            } // end if
        } // end of for
        moneyreturn = Math.round(moneyreturn);
        _parent.attachMovie("extrasellcurrentship", "extrasellcurrentship", 10);
        _parent.extrasellcurrentship._x = 25;
        _parent.extrasellcurrentship._y = 10;
        _parent.extrasellcurrentship.message = "Sell this Ship for \r " + moneyreturn + " Funds?";
        _parent.extrasellcurrentship.sellshipid = _parent.shipid;
        _parent.extrasellcurrentship.moneyreturn = moneyreturn;
    } // end if
    if (operation == "Purchase Ship")
    {
        _root.newshipid = _parent.shipid;
        _root.mainscreenstarbase.shopselection = "ship";
        _root.attachMovie("purchasenewship", "purchasenewship", 10);
    } // end if
}

// [onClipEvent of sprite 527 in frame 1]
on (release)
{
    if (operation == "N/A" || operation == "Current")
    {
    } // end if
    if (operation == "Change Ship")
    {
        if (_root.playerscurrentextrashipno == "capital")
        {
            moneyreturn = Math.round(_root.shiptype[_root.playershipstatus[5][0]][1] / 4 * 3);
            moneyreturn = Math.round(moneyreturn);
            _parent.attachMovie("extrasellcurrentship", "extrasellcurrentship", 10);
            _parent.extrasellcurrentship._x = 25;
            _parent.extrasellcurrentship._y = 10;
            _parent.extrasellcurrentship.message = "Sell Capital Ship for \r " + moneyreturn + " Funds?";
            _parent.extrasellcurrentship.sellshipid = "capital";
            _parent.extrasellcurrentship.moneyreturn = moneyreturn;
        }
        else
        {
            _root.changetonewship(_parent.shipid);
            _root.initializemissilebanks();
            _root.playerscurrentextrashipno = _parent.shipid;
            _root.changetonewship(_root.playerscurrentextrashipno);
            datatosend = "TC`" + _root.playershipstatus[3][0] + "`SC`" + _root.extraplayerships[_root.playerscurrentextrashipno][0] + "`" + _root.errorchecknumber + "~";
            _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
            _root.mysocket.send(datatosend);
            _root.mainscreenstarbase.shopselection = "extraships";
            _root.attachMovie("playerextraships", "playerextraships", 10);
        } // end if
    } // end else if
}

// [Action in Frame 1]
function processbty()
{
    totalshipworth = _root.shiptype[shiptype][1];
    for (currenthardpoint = 0; currenthardpoint < this.hps.length; currenthardpoint++)
    {
        currentguntype = this.hps[currenthardpoint];
        if (!isNaN(currentguntype))
        {
            totalshipworth = totalshipworth + _root.guntype[currentguntype][5];
        } // end if
    } // end of for
    totalshipworth = totalshipworth + _root.shieldgenerators[shiptypeshieldgen][5];
    totalshipworth = totalshipworth + _root.energycapacitors[shiptypeenergycap][2];
    totalshipworth = totalshipworth + _root.energygenerators[shiptypeenergygen][2];
    thisshipworth = Math.round(totalshipworth / _root.playersworthtobtymodifire);
    if (isNaN(thisshipworth))
    {
        thisshipworth = Number(0);
    } // end if
} // End of the function
function changeintonewship()
{
    _root.changetonewship(shipid);
    _root.initializemissilebanks();
    _root.playerscurrentextrashipno = shipid;
    _root.changetonewship(_root.playerscurrentextrashipno);
    datatosend = "TC`" + _root.playershipstatus[3][0] + "`SC`" + _root.extraplayerships[_root.playerscurrentextrashipno][0] + "`" + _root.errorchecknumber + "~";
    _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
    _root.mysocket.send(datatosend);
    _root.mainscreenstarbase.shopselection = "extraships";
    _root.attachMovie("playerextraships", "playerextraships", 10);
} // End of the function
shiptype = _root.extraplayerships[shipidno][0];
if (shiptype != null)
{
    shiptypeshieldgen = _root.extraplayerships[shipidno][1];
    shiptypeenergygen = _root.extraplayerships[shipidno][2];
    shiptypeenergycap = _root.extraplayerships[shipidno][3];
    i = 0;
    this.hps = new Array();
    while (i < _root.extraplayerships[shipidno][4].length)
    {
        this.hps[i] = _root.extraplayerships[shipidno][4][i];
        ++i;
    } // end while
    i = 0;
    this.turrets = new Array();
    while (i < _root.extraplayerships[shipidno][5].length)
    {
        this.turrets[i] = _root.extraplayerships[shipidno][5][i];
        ++i;
    } // end while
    this.attachMovie("shiptype" + shiptype, "ship", 1);
    this.ship._x = this._width / 2;
    this.ship._y = 65;
    processbty();
    printout = "Base Bty:" + thisshipworth + "\r" + "Ship Name:" + _root.shiptype[shiptype][0] + "\r" + "Shield Gen:" + _root.shieldgenerators[shiptypeshieldgen][4] + "\r" + "  Max Shield:" + _root.shieldgenerators[shiptypeshieldgen][0] + "\r" + "Energy Gen:" + _root.energygenerators[shiptypeenergygen][1] + "\r" + "  Charge Rate:" + _root.energygenerators[shiptypeenergygen][0] + "\r" + "Energy Cap:" + _root.energycapacitors[shiptypeenergycap][1] + "\r" + "  Max Energy:" + _root.energycapacitors[shiptypeenergycap][0] + "\r";
    for (i = 0; i < hps.length; i++)
    {
        printout = printout + ("Hardpoint " + i + ": " + _root.guntype[hps[i]][6] + "\r");
    } // end of for
    for (i = 0; i < turrets.length; i++)
    {
        printout = printout + ("Turret " + i + ": " + _root.guntype[turrets[i]][6] + "\r");
    } // end of for
}
else
{
    printout = "Spot Open For A Ship";
} // end else if
if (shipid == _root.playerscurrentextrashipno)
{
    psbutton.operation = "Current";
}
else if (_root.extraplayerships[shipid][0] == null)
{
    psbutton.operation = "Purchase Ship";
}
else if (_root.playerscurrentextrashipno == "capital")
{
    psbutton.operation = "Capital Owned";
}
else
{
    psbutton.operation = "Sell Ship";
} // end else if
if (shipid == _root.playerscurrentextrashipno)
{
    changeship.operation = "Current";
}
else if (_root.extraplayerships[shipid][0] == null)
{
    changeship.operation = "N/A";
}
else
{
    changeship.operation = "Change Ship";
} // end else if
stop ();
