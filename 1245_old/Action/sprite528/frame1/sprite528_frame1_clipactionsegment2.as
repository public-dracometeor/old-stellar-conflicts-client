﻿// Action script...

// [onClipEvent of sprite 527 in frame 1]
on (release)
{
    if (operation == "Sell Ship")
    {
        moneyreturn = 0;
        moneyreturn = moneyreturn + _root.shiptype[_root.extraplayerships[_parent.shipid][0]][1] / 2;
        moneyreturn = moneyreturn + _root.shieldgenerators[_root.extraplayerships[_parent.shipid][1]][5] / 4;
        moneyreturn = moneyreturn + _root.energygenerators[_root.extraplayerships[_parent.shipid][2]][2] / 4;
        moneyreturn = moneyreturn + _root.energycapacitors[_root.extraplayerships[_parent.shipid][3]][2] / 4;
        for (currentharpoint = 0; currentharpoint < _root.shiptype[_root.extraplayerships[_parent.shipid][0]][2].length; currentharpoint++)
        {
            if (!isNaN(Number(_root.extraplayerships[_parent.shipid][4][currentharpoint])))
            {
                moneyreturn = moneyreturn + _root.guntype[_root.extraplayerships[_parent.shipid][4][currentharpoint]][5] / 4;
            } // end if
        } // end of for
        for (currentturretpoint = 0; currentturretpoint < _root.shiptype[_root.extraplayerships[_parent.shipid][0]][5].length; currentturretpoint++)
        {
            if (!isNaN(Number(_root.extraplayerships[_parent.shipid][5][currentturretpoint])))
            {
                moneyreturn = moneyreturn + _root.guntype[_root.extraplayerships[_parent.shipid][5][currentturretpoint]][5] / 2;
            } // end if
        } // end of for
        moneyreturn = Math.round(moneyreturn);
        _parent.attachMovie("extrasellcurrentship", "extrasellcurrentship", 10);
        _parent.extrasellcurrentship._x = 25;
        _parent.extrasellcurrentship._y = 10;
        _parent.extrasellcurrentship.message = "Sell this Ship for \r " + moneyreturn + " Funds?";
        _parent.extrasellcurrentship.sellshipid = _parent.shipid;
        _parent.extrasellcurrentship.moneyreturn = moneyreturn;
    } // end if
    if (operation == "Purchase Ship")
    {
        _root.newshipid = _parent.shipid;
        _root.mainscreenstarbase.shopselection = "ship";
        _root.attachMovie("purchasenewship", "purchasenewship", 10);
    } // end if
}
