﻿// Action script...

// [onClipEvent of sprite 527 in frame 1]
on (release)
{
    if (operation == "N/A" || operation == "Current")
    {
    } // end if
    if (operation == "Change Ship")
    {
        if (_root.playerscurrentextrashipno == "capital")
        {
            moneyreturn = Math.round(_root.shiptype[_root.playershipstatus[5][0]][1] / 4 * 3);
            moneyreturn = Math.round(moneyreturn);
            _parent.attachMovie("extrasellcurrentship", "extrasellcurrentship", 10);
            _parent.extrasellcurrentship._x = 25;
            _parent.extrasellcurrentship._y = 10;
            _parent.extrasellcurrentship.message = "Sell Capital Ship for \r " + moneyreturn + " Funds?";
            _parent.extrasellcurrentship.sellshipid = "capital";
            _parent.extrasellcurrentship.moneyreturn = moneyreturn;
        }
        else
        {
            _root.changetonewship(_parent.shipid);
            _root.initializemissilebanks();
            _root.playerscurrentextrashipno = _parent.shipid;
            _root.changetonewship(_root.playerscurrentextrashipno);
            datatosend = "TC`" + _root.playershipstatus[3][0] + "`SC`" + _root.extraplayerships[_root.playerscurrentextrashipno][0] + "`" + _root.errorchecknumber + "~";
            _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
            _root.mysocket.send(datatosend);
            _root.mainscreenstarbase.shopselection = "extraships";
            _root.attachMovie("playerextraships", "playerextraships", 10);
        } // end if
    } // end else if
}
