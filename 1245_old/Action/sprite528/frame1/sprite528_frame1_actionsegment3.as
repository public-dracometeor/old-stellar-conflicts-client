﻿// Action script...

function processbty()
{
    totalshipworth = _root.shiptype[shiptype][1];
    for (currenthardpoint = 0; currenthardpoint < this.hps.length; currenthardpoint++)
    {
        currentguntype = this.hps[currenthardpoint];
        if (!isNaN(currentguntype))
        {
            totalshipworth = totalshipworth + _root.guntype[currentguntype][5];
        } // end if
    } // end of for
    totalshipworth = totalshipworth + _root.shieldgenerators[shiptypeshieldgen][5];
    totalshipworth = totalshipworth + _root.energycapacitors[shiptypeenergycap][2];
    totalshipworth = totalshipworth + _root.energygenerators[shiptypeenergygen][2];
    thisshipworth = Math.round(totalshipworth / _root.playersworthtobtymodifire);
    if (isNaN(thisshipworth))
    {
        thisshipworth = Number(0);
    } // end if
} // End of the function
function changeintonewship()
{
    _root.changetonewship(shipid);
    _root.initializemissilebanks();
    _root.playerscurrentextrashipno = shipid;
    _root.changetonewship(_root.playerscurrentextrashipno);
    datatosend = "TC`" + _root.playershipstatus[3][0] + "`SC`" + _root.extraplayerships[_root.playerscurrentextrashipno][0] + "`" + _root.errorchecknumber + "~";
    _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
    _root.mysocket.send(datatosend);
    _root.mainscreenstarbase.shopselection = "extraships";
    _root.attachMovie("playerextraships", "playerextraships", 10);
} // End of the function
shiptype = _root.extraplayerships[shipidno][0];
if (shiptype != null)
{
    shiptypeshieldgen = _root.extraplayerships[shipidno][1];
    shiptypeenergygen = _root.extraplayerships[shipidno][2];
    shiptypeenergycap = _root.extraplayerships[shipidno][3];
    i = 0;
    this.hps = new Array();
    while (i < _root.extraplayerships[shipidno][4].length)
    {
        this.hps[i] = _root.extraplayerships[shipidno][4][i];
        ++i;
    } // end while
    i = 0;
    this.turrets = new Array();
    while (i < _root.extraplayerships[shipidno][5].length)
    {
        this.turrets[i] = _root.extraplayerships[shipidno][5][i];
        ++i;
    } // end while
    this.attachMovie("shiptype" + shiptype, "ship", 1);
    this.ship._x = this._width / 2;
    this.ship._y = 65;
    processbty();
    printout = "Base Bty:" + thisshipworth + "\r" + "Ship Name:" + _root.shiptype[shiptype][0] + "\r" + "Shield Gen:" + _root.shieldgenerators[shiptypeshieldgen][4] + "\r" + "  Max Shield:" + _root.shieldgenerators[shiptypeshieldgen][0] + "\r" + "Energy Gen:" + _root.energygenerators[shiptypeenergygen][1] + "\r" + "  Charge Rate:" + _root.energygenerators[shiptypeenergygen][0] + "\r" + "Energy Cap:" + _root.energycapacitors[shiptypeenergycap][1] + "\r" + "  Max Energy:" + _root.energycapacitors[shiptypeenergycap][0] + "\r";
    for (i = 0; i < hps.length; i++)
    {
        printout = printout + ("Hardpoint " + i + ": " + _root.guntype[hps[i]][6] + "\r");
    } // end of for
    for (i = 0; i < turrets.length; i++)
    {
        printout = printout + ("Turret " + i + ": " + _root.guntype[turrets[i]][6] + "\r");
    } // end of for
}
else
{
    printout = "Spot Open For A Ship";
} // end else if
if (shipid == _root.playerscurrentextrashipno)
{
    psbutton.operation = "Current";
}
else if (_root.extraplayerships[shipid][0] == null)
{
    psbutton.operation = "Purchase Ship";
}
else if (_root.playerscurrentextrashipno == "capital")
{
    psbutton.operation = "Capital Owned";
}
else
{
    psbutton.operation = "Sell Ship";
} // end else if
if (shipid == _root.playerscurrentextrashipno)
{
    changeship.operation = "Current";
}
else if (_root.extraplayerships[shipid][0] == null)
{
    changeship.operation = "N/A";
}
else
{
    changeship.operation = "Change Ship";
} // end else if
stop ();
