﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CANCEL";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent._parent.changemessagebut._visible = true;
    _parent._visible = false;
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    newsquadmessage = _parent.newsquadmessage;
    if (newsquadmessage.length > 200)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "200 Chars Max";
    }
    else
    {
        _parent._visible = false;
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadscommand.php?mode=newmess&newmess=" + newsquadmessage + "&sname=" + _root.playershipstatus[5][10] + "&spass=" + _root.playershipstatus[5][13]);
        newsquadVars.onLoad = function (success)
        {
            loadedinfo = String(newsquadVars);
            if (loadedinfo == "success")
            {
                _parent._parent.changemessagebut._visible = true;
                _parent._visible = false;
                _parent._parent.squadmessage = newsquadmessage;
            }
            else
            {
                _parent._parent.changemessagebut._visible = true;
                _parent._visible = false;
            } // end else if
        };
    } // end else if
}
