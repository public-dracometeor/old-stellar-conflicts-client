﻿// Action script...

// [onClipEvent of sprite 1493 in frame 1]
onClipEvent (load)
{
    function showshield(shield)
    {
        scale = Math.ceil(shield / maxshieldstrength * 28);
        if (scale < 1)
        {
            scale = 1;
        } // end if
        this.gotoAndStop(this.scale);
    } // End of the function
    playercurrentshieldgenerator = _root.playershipstatus[2][0];
    maxshieldstrength = _root.shieldgenerators[playercurrentshieldgenerator][0];
    showshield(maxshieldstrength);
    this.gotoAndStop(28);
}
