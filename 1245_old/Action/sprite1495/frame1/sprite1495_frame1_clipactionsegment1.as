﻿// Action script...

// [onClipEvent of sprite 1494 in frame 1]
onClipEvent (enterFrame)
{
    structure = _root.playershipstatus[2][5];
    if (laststructure != structure)
    {
        laststructure = structure;
        lastscale = scale;
        scale = Math.ceil(_root.playershipstatus[2][5] / maxstructure * 28);
        if (scale < 1)
        {
            scale = 1;
        } // end if
        if (lastscale != scale)
        {
            this.gotoAndStop(this.scale);
        } // end if
    } // end if
}
