﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "EXIT";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent._parent.gotoAndStop("squadbasemenu");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    function func_initializeinfo()
    {
        currentbasetype = _parent.currentbasetype;
        playerssquad = _root.playershipstatus[5][10];
        _parent.upgradetolevel = "N/A";
        canupgrade = false;
        topupgradelevel = _root.squadbaseinfo.length - 1;
        _parent.currentbasetypedisp = currentbasetype + "/" + topupgradelevel;
        _parent.upgradebasetolevel = "";
        _parent.basetypeupgcost = "";
    } // End of the function
    function setplayersnewfunds()
    {
        _root.savegamescript.saveplayersgame(this);
    } // End of the function
    this.label = "UPGRADE";
    func_initializeinfo();
    if (_parent.currentbasetype < topupgradelevel)
    {
        canupgrade = true;
        _parent.upgradebasetolevel = _parent.currentbasetype + 1;
    } // end if
    sentbaseupgrade = false;
    this.onEnterFrame = function ()
    {
        if (canupgrade == true)
        {
            if (_parent.upgradebasetolevel > topupgradelevel)
            {
                _parent.upgradebasetolevel = topupgradelevel;
            }
            else if (_parent.upgradebasetolevel <= _parent.currentbasetype)
            {
                _parent.upgradebasetolevel = "";
            }
            else if (isNaN(_parent.upgradebasetolevel))
            {
                _parent.upgradebasetolevel = "";
            }
            else if (_parent.upgradebasetolevel != "")
            {
                _parent.basetypeupgcost = _root.squadbaseinfo[_parent.upgradebasetolevel][5][0][0];
                _parent.displayupgradeinfo(_parent.upgradebasetolevel);
            } // end else if
        } // end else if
        if (sentbaseupgrade == true)
        {
            for (i = 0; i < _root.playersquadbases.length; i++)
            {
                if (playerssquad == _root.playersquadbases[i][0])
                {
                    currentbasetype = _root.playersquadbases[i][1];
                    if (currentbasetype != lastcurrentbaselevel)
                    {
                        sentbaseupgrade = false;
                        _parent.func_refreshscreen();
                        func_initializeinfo();
                        _parent.basetypeupgcost = "Re Enter Base to Upgrade";
                    } // end if
                } // end if
            } // end of for
        } // end if
    };
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    upgradecost = _parent.basetypeupgcost;
    upgradebasetolevel = _parent.upgradebasetolevel;
    if (upgradebasetolevel > currentbasetype && upgradebasetolevel <= topupgradelevel && upgradebasetolevel != "" && upgradebasetolevel != 0 && sentbaseupgrade != true && _root.playershipstatus[3][1] >= upgradecost)
    {
        canupgrade = false;
        sentbaseupgrade = true;
        _parent.basetypeupgcost = "Please Wait";
        lastcurrentbaselevel = currentbasetype;
        this.newbaseVars = new XML();
        this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=upglvl&baseid=" + _parent.baseidname + "&lvl=" + upgradebasetolevel);
        this.newbaseVars.onLoad = function (success)
        {
            loadedvars = String(newbaseVars);
            if (loadedvars == "destroyed")
            {
                _parent.basetypeupgcost = "Base has been destroyed";
            }
            else if (loadedvars == "lowerlevel")
            {
                _parent.basetypeupgcost = "Base was upgraded already";
            }
            else if (loadedvars == "completed")
            {
                datatosend = "NEWS`SQUADUPG`" + playerssquad + "`baselvl`" + upgradebasetolevel + "`" + _root.errorchecknumber + "~";
                _root.mysocket.send(datatosend);
                _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - upgradecost;
                _parent.fundsdisplay.funds = _root.playershipstatus[3][1];
                setplayersnewfunds();
            }
            else
            {
                _parent.basetypeupgcost = "error occured";
            } // end else if
        };
    } // end if
}

// [Action in Frame 1]
function func_refreshscreen()
{
    playerssquad = _root.playershipstatus[5][10];
    currentbasetype = 0;
    for (i = 0; i < _root.playersquadbases.length; i++)
    {
        if (playerssquad == _root.playersquadbases[i][0])
        {
            currentbasetype = _root.playersquadbases[i][1];
        } // end if
    } // end of for
} // End of the function
function displayupgradeinfo(squadbasetype)
{
    this.baseimage.gotoAndStop(squadbasetype + 1);
    this.baseimage._visible = true;
    upginfo = "New Base Level: " + squadbasetype;
    upginfo = upginfo + ("\rMax Turrets: " + _root.squadbaseinfo[squadbasetype][2][2]);
    upginfo = upginfo + ("\rTop Turret Type: " + _root.guntype[_root.squadbaseinfo[squadbasetype][2][1]][6]);
    upginfo = upginfo + ("\rMax Shield: " + _root.shieldgenerators[_root.squadbaseinfo[squadbasetype][1][3]][0] * _root.squadbaseinfo[squadbasetype][1][1]);
    upginfo = upginfo + ("\rMax Structure: " + _root.squadbaseinfo[squadbasetype][3][1]);
} // End of the function
currentbasetype = "";
this.baseidname = _root.playershipstatus[5][10];
this.baseimage._visible = false;
upginfo = "Enter An Upgrade Level";
func_refreshscreen();
stop ();
