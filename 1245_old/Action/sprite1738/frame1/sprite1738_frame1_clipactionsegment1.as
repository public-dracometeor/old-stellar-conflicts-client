﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    upgradecost = _parent.basetypeupgcost;
    upgradebasetolevel = _parent.upgradebasetolevel;
    if (upgradebasetolevel > currentbasetype && upgradebasetolevel <= topupgradelevel && upgradebasetolevel != "" && upgradebasetolevel != 0 && sentbaseupgrade != true && _root.playershipstatus[3][1] >= upgradecost)
    {
        canupgrade = false;
        sentbaseupgrade = true;
        _parent.basetypeupgcost = "Please Wait";
        lastcurrentbaselevel = currentbasetype;
        this.newbaseVars = new XML();
        this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=upglvl&baseid=" + _parent.baseidname + "&lvl=" + upgradebasetolevel);
        this.newbaseVars.onLoad = function (success)
        {
            loadedvars = String(newbaseVars);
            if (loadedvars == "destroyed")
            {
                _parent.basetypeupgcost = "Base has been destroyed";
            }
            else if (loadedvars == "lowerlevel")
            {
                _parent.basetypeupgcost = "Base was upgraded already";
            }
            else if (loadedvars == "completed")
            {
                datatosend = "NEWS`SQUADUPG`" + playerssquad + "`baselvl`" + upgradebasetolevel + "`" + _root.errorchecknumber + "~";
                _root.mysocket.send(datatosend);
                _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - upgradecost;
                _parent.fundsdisplay.funds = _root.playershipstatus[3][1];
                setplayersnewfunds();
            }
            else
            {
                _parent.basetypeupgcost = "error occured";
            } // end else if
        };
    } // end if
}
