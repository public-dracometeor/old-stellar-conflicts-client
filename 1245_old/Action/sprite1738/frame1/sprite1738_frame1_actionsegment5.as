﻿// Action script...

function func_refreshscreen()
{
    playerssquad = _root.playershipstatus[5][10];
    currentbasetype = 0;
    for (i = 0; i < _root.playersquadbases.length; i++)
    {
        if (playerssquad == _root.playersquadbases[i][0])
        {
            currentbasetype = _root.playersquadbases[i][1];
        } // end if
    } // end of for
} // End of the function
function displayupgradeinfo(squadbasetype)
{
    this.baseimage.gotoAndStop(squadbasetype + 1);
    this.baseimage._visible = true;
    upginfo = "New Base Level: " + squadbasetype;
    upginfo = upginfo + ("\rMax Turrets: " + _root.squadbaseinfo[squadbasetype][2][2]);
    upginfo = upginfo + ("\rTop Turret Type: " + _root.guntype[_root.squadbaseinfo[squadbasetype][2][1]][6]);
    upginfo = upginfo + ("\rMax Shield: " + _root.shieldgenerators[_root.squadbaseinfo[squadbasetype][1][3]][0] * _root.squadbaseinfo[squadbasetype][1][1]);
    upginfo = upginfo + ("\rMax Structure: " + _root.squadbaseinfo[squadbasetype][3][1]);
} // End of the function
currentbasetype = "";
this.baseidname = _root.playershipstatus[5][10];
this.baseimage._visible = false;
upginfo = "Enter An Upgrade Level";
func_refreshscreen();
stop ();
