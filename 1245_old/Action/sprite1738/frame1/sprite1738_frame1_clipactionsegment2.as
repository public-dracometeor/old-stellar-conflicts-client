﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    function func_initializeinfo()
    {
        currentbasetype = _parent.currentbasetype;
        playerssquad = _root.playershipstatus[5][10];
        _parent.upgradetolevel = "N/A";
        canupgrade = false;
        topupgradelevel = _root.squadbaseinfo.length - 1;
        _parent.currentbasetypedisp = currentbasetype + "/" + topupgradelevel;
        _parent.upgradebasetolevel = "";
        _parent.basetypeupgcost = "";
    } // End of the function
    function setplayersnewfunds()
    {
        _root.savegamescript.saveplayersgame(this);
    } // End of the function
    this.label = "UPGRADE";
    func_initializeinfo();
    if (_parent.currentbasetype < topupgradelevel)
    {
        canupgrade = true;
        _parent.upgradebasetolevel = _parent.currentbasetype + 1;
    } // end if
    sentbaseupgrade = false;
    this.onEnterFrame = function ()
    {
        if (canupgrade == true)
        {
            if (_parent.upgradebasetolevel > topupgradelevel)
            {
                _parent.upgradebasetolevel = topupgradelevel;
            }
            else if (_parent.upgradebasetolevel <= _parent.currentbasetype)
            {
                _parent.upgradebasetolevel = "";
            }
            else if (isNaN(_parent.upgradebasetolevel))
            {
                _parent.upgradebasetolevel = "";
            }
            else if (_parent.upgradebasetolevel != "")
            {
                _parent.basetypeupgcost = _root.squadbaseinfo[_parent.upgradebasetolevel][5][0][0];
                _parent.displayupgradeinfo(_parent.upgradebasetolevel);
            } // end else if
        } // end else if
        if (sentbaseupgrade == true)
        {
            for (i = 0; i < _root.playersquadbases.length; i++)
            {
                if (playerssquad == _root.playersquadbases[i][0])
                {
                    currentbasetype = _root.playersquadbases[i][1];
                    if (currentbasetype != lastcurrentbaselevel)
                    {
                        sentbaseupgrade = false;
                        _parent.func_refreshscreen();
                        func_initializeinfo();
                        _parent.basetypeupgcost = "Re Enter Base to Upgrade";
                    } // end if
                } // end if
            } // end of for
        } // end if
    };
}
