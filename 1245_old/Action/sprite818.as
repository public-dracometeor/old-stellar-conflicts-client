﻿// Action script...

// [Action in Frame 1]
function func_attachturrets()
{
    turrettype = guntype;
    noofturrets = _root.shiptype[shiptype][5].length;
    for (qq = 0; qq < noofturrets; qq++)
    {
        this.attachMovie("turrettypeaiship", "turrettypeaiship" + qq, 200 + qq);
        this["turrettypeaiship" + qq].shottype = turrettype;
        this["turrettypeaiship" + qq]._x = _root.shiptype[shiptype][5][qq][0];
        this["turrettypeaiship" + qq]._y = _root.shiptype[shiptype][5][qq][1];
    } // end of for
    coords = "X:" + _root.playershipstatus[6][0] + " Y:" + _root.playershipstatus[6][1];
    idno = shipname.substr(4);
    endxcoord = _root.neutralships[loc][2];
    endycoord = _root.neutralships[loc][3];
    destination = "Unknown";
    for (qt = 0; qt < _root.starbaselocation.length; qt++)
    {
        if (_root.starbaselocation[qt][1] == endxcoord && _root.starbaselocation[qt][2] == endycoord)
        {
            destination = _root.starbaselocation[qt][0];
            if (destination.substr(0, 2) == "PL")
            {
                destination = "Planet " + destination.substr(2);
            } // end if
            break;
        } // end if
    } // end of for
    globalmessage = "The following is a Mayday Message.. This is Cargo Ship " + idno + " requesting help... under... attack from " + _root.playershipstatus[3][2] + ".  I am at " + coords + " heading... to " + destination;
    datatosend = "NEWS`MES`" + globalmessage + "~";
    _root.mysocket.send(datatosend);
} // End of the function
function adddamage(damagetoadd, damagefrom)
{
    damagetoadd = Number(damagetoadd);
    currentshipshield = currentshipshield - damagetoadd;
    if (currentshipshield < 0)
    {
        shiphealth = shiphealth + currentshipshield;
        currentshipshield = 0;
    } // end if
    if (shiphealth <= 0)
    {
        currentshipstatus = "dieing";
        i = 999999;
        shipkillerid = damagefrom;
    } // end if
    this.ship.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
    this.ship.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
} // End of the function
function movementofanobjectwiththrust()
{
    relativefacing = this.rotation;
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    }
    else if (relativefacing == 90)
    {
        ymovement = 0;
        xmovement = velocity;
    }
    else if (relativefacing == 270)
    {
        ymovement = 0;
        xmovement = -velocity;
    }
    else if (relativefacing == 180)
    {
        ymovement = velocity;
        xmovement = 0;
    }
    else if (velocity != 0)
    {
        this.relativefacing = this.relativefacing;
        ymovement = -velocity * Math.cos(this.relativefacing * radianmultiplier);
        xmovement = velocity * Math.sin(this.relativefacing * radianmultiplier);
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function deathinfosend()
{
    datatosend = "SA~NEUT`death`" + shipid + "`" + _root.playershipstatus[3][0] + "`" + String(shipbounty) + "`" + Math.round(shipbounty * _root.playershipstatus[5][6]) + "`" + shipbounty + "~";
    _root.mysocket.send(datatosend);
} // End of the function
function dropai(reason)
{
    datatosend = "AI`drop`" + shipid;
    "~";
    _root.mysocket.send(datatosend);
    if (reason == "otherships")
    {
        message = "Cya Mate, Looks like another player wants to battle you!";
        _root.func_messangercom(message, shipname, "BEGIN");
    } // end if
    if (reason == "friendlyfire")
    {
        message = "Hey, I\'m not going to play with your friend!";
        _root.func_messangercom(message, shipname, "BEGIN");
    } // end if
    for (i = 0; i < _root.aishipshosted.length; i++)
    {
        if (shipid == _root.aishipshosted[i][0])
        {
            _root.aishipshosted.splice(i, 1);
            this.gotoAndPlay("dropaiship");
            i = 999999;
        } // end if
    } // end of for
} // End of the function
function otherplayershipshots()
{
    for (cc = 0; cc < _root.othergunfire.length; cc++)
    {
        if (this.hitTest("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]) == true && currentshipstatus == "alive" && _root.othergunfire[cc][10] != "AI")
        {
            playerwhoshothitid = _root.othergunfire[cc][0];
            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
            _root.gunfireinformation = _root.gunfireinformation + ("GH`AI" + globalid + "`" + _root.othergunfire[cc][0] + "`" + _root.othergunfire[cc][9] + "~");
            gunshothitplayertype = _root.othergunfire[cc][7];
            _root.othergunfire.splice(cc, 1);
            --cc;
            currentshipshield = currentshipshield - _root.guntype[gunshothitplayertype][4];
            if (currentshipshield < 0)
            {
                shiphealth = shiphealth + currentshipshield;
                currentshipshield = 0;
            } // end if
            if (shiphealth <= 0 && currentshipstatus == "alive")
            {
                currentshipstatus = "dieing";
                shipkillerid = playerwhoshothitid;
                continue;
            } // end if
            this.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
            this.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
        } // end if
    } // end of for
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function
function func_begininteraction()
{
    message = "Leave me Alone, And I will leave you alone.";
    _root.func_messangercom(message, shipname, "BEGIN");
} // End of the function
shipspeedaleration = _root.shipspeedalterationforneuts;
maxplayerrangebeforeinteraction = 1000;
interactedwithplay = false;
radianmultiplier = 0.017453;
currenttimechangeratio = _root.currenttimechangeratio;
currenthostplayer = _root.playershipstatus[3][0];
lastshooter = new Array();
currentgunfireshot = 0;
shipkillerid = null;
shipid = globalshipname;
for (i = 0; i < _root.neutralships.length; i++)
{
    if (shipid == _root.neutralships[i][10])
    {
        loc = i;
        currentshipidno = i;
        shiptype = _root.neutralships[loc][4];
        shipname = _root.neutralships[loc][11];
        shieldgenerator = _root.shiptype[shiptype][6][0] - 1;
        guntype = _root.shiptype[shiptype][6][3] - 1;
        travelspeed = _root.shiptype[shiptype][3][1];
        currentvelocity = travelspeed / shipspeedaleration;
        startxcoord = _root.neutralships[loc][0];
        startycoord = _root.neutralships[loc][1];
        endxcoord = _root.neutralships[loc][2];
        endycoord = _root.neutralships[loc][3];
        totalrangeleft = Math.round(Math.sqrt((startxcoord - endxcoord) * (startxcoord - endxcoord) + (startycoord - endycoord) * (startycoord - endycoord)));
        angletotravel = 360 - Math.atan2(startxcoord - endxcoord, startycoord - endycoord) / 0.017453;
        if (angletotravel > 360)
        {
            angletotravel = angletotravel - 360;
        }
        else if (angletotravel < 0)
        {
            angletotravel = angletotravel + 360;
        } // end else if
        this.rotation = angletotravel;
        this._rotation = angletotravel;
        if (_root.neutralships[loc][6] != "N/A" && _root.neutralships[loc][6] < getTimer())
        {
            relativefacing = angletotravel;
            velocity = travelspeed * ((getTimer() - _root.neutralships[loc][6]) / 1000) / shipspeedaleration;
            movementofanobjectwiththrust();
        } // end if
        startedtime = _root.neutralships[loc][6];
        this.xcoord = startxcoord + xmovement;
        this._x = this.xcoord - _root.shipcoordinatex;
        this.ycoord = startycoord + ymovement;
        this._y = this.ycoord - _root.shipcoordinatey;
        i = 99999;
    } // end if
} // end of for
this.ainame = shipname;
distancetodropai = 12000;
maxshipshield = _root.shieldgenerators[shieldgenerator][0];
shipshieldreplenish = Math.round(_root.shieldgenerators[shieldgenerator][1]);
currentshipshield = maxshipshield;
currentshipstatus = "alive";
rotationspeed = _root.shiptype[shiptype][3][2];
acceleration = Math.round(_root.shiptype[shiptype][3][0]);
maxvelocity = Math.round(_root.shiptype[shiptype][3][1]) / shipspeedaleration;
rotationspeed = _root.shiptype[shiptype][3][2];
shiphealth = _root.shiptype[shiptype][3][3];
noofhardpoints = _root.shiptype[shiptype][2].length;
hardpoints = new Array();
for (i = 0; i < noofhardpoints; i++)
{
    hardpoints[i] = new Array();
    hardpoints[i][0] = guntype;
    hardpoints[i][1] = 0;
    hardpoints[i][2] = _root.shiptype[shiptype][2][i][0];
    hardpoints[i][3] = _root.shiptype[shiptype][2][i][1];
    hardpoints[i][4] = Math.round(_root.guntype[hardpoints[i][0]][2] * 1000);
    hardpoints[i][5] = Math.round(_root.guntype[hardpoints[i][0]][0]);
    hardpoints[i][6] = _root.guntype[hardpoints[i][0]][3];
} // end of for
this.attachMovie("shiptype" + shiptype, "ship", 1);
totalshipworth = _root.shiptype[shiptype][1];
for (currenthardpoint = 0; currenthardpoint < hardpoints.length; currenthardpoint++)
{
    currentguntype = hardpoints[0][currenthardpoint][0];
    if (!isNaN(currentguntype))
    {
        totalshipworth = totalshipworth + _root.guntype[currentguntype][5];
    } // end if
} // end of for
totalshipworth = totalshipworth + _root.shieldgenerators[shieldgenerator][5];
shipbounty = Math.round(totalshipworth / _root.playersworthtobtymodifire);

// [Action in Frame 2]
timeshit = 0;
hitstillhostile = 2;
shipnothostile = true;
curenttime = _root.curenttime;
shipdisrupted = false;
shipemped = false;
hitteestintervals = 4;
currentframeforhittest = Math.round(Math.random() * hitteestintervals);
nextupdateframe = 0;
framedelay = 7;
nextupdateframe = Math.round(Math.random() * framedelay);
movementupdateinterval = 40;
nextmovementtime = curenttime + movementupdateinterval;
lastshipdisplaytime = curenttime;
globalid = shipid;
this.onEnterFrame = function ()
{
    curenttime = _root.curenttime;
    ++currentframeforhittest;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    playerdistancefromship = Math.round(Math.sqrt((playersxcoord - this.xcoord) * (playersxcoord - this.xcoord) + (playersycoord - this.ycoord) * (playersycoord - this.ycoord)));
    if (playerdistancefromship < 2000 && currentshipstatus == "alive" && currentframeforhittest > hitteestintervals)
    {
        currentframeforhittest = 0;
        shieldtimechangeratio = (curenttime - lastshieldtime) / 1000;
        lastshieldtime = curenttime;
        if (shipemped == true)
        {
            currentshipshield = 0;
        }
        else if (currentshipshield < maxshipshield)
        {
            currentshipshield = currentshipshield + shipshieldreplenish * shieldtimechangeratio;
            if (currentshipshield > maxshipshield)
            {
                currentshipshield = maxshipshield;
            } // end if
        } // end else if
        numberofplayershots = _root.playershotsfired.length;
        if (numberofplayershots > 0)
        {
            halfshipswidth = this.ship._width / 2;
            halfshipsheight = this.ship._height / 2;
            this.relativex = this._x;
            this.relativey = this._y;
        } // end if
        for (i = 0; i < numberofplayershots; i++)
        {
            bulletshotid = _root.playershotsfired[i][0];
            bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
            bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
            if (bulletsx != null)
            {
                if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
                {
                    ++timeshit;
                    if (timeshit >= hitstillhostile && shipnothostile)
                    {
                        shipnothostile = false;
                        func_attachturrets();
                    } // end if
                    if (_root.playershotsfired[i][13] == "GUNS")
                    {
                        currentshipshield = currentshipshield - _root.guntype[_root.playershotsfired[i][6]][4];
                        _root.gunfireinformation = _root.gunfireinformation + ("GH`AI" + globalid + "`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[cc][0] + "~");
                    }
                    else if (_root.playershotsfired[i][13] == "MISSILE")
                    {
                        missiletype = _root.playershotsfired[i][6];
                        currentshipshield = currentshipshield - _root.missile[missiletype][4];
                        if (_root.missile[missiletype][8] == "EMP")
                        {
                        } // end if
                        if (_root.missile[missiletype][8] == "DISRUPTER")
                        {
                        } // end if
                    } // end else if
                    if (currentshipshield < 0)
                    {
                        shiphealth = shiphealth + currentshipshield;
                        currentshipshield = 0;
                    } // end if
                    if (shiphealth <= 0)
                    {
                        currentshipstatus = "dieing";
                        i = 999999;
                        shipkillerid = currenthostplayer;
                    } // end if
                    removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                    _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                    lastshooter[2] = lastshooter[1];
                    lastshooter[1] = lastshooter[0];
                    lastshooter[0] = currenthostplayer;
                    _root.playershotsfired[i][5] = 0;
                    this.ship.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
                    this.ship.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
                } // end if
            } // end if
        } // end of for
        otherplayershipshots();
    } // end if
    if (currentshipstatus == "dieing")
    {
        deathinfosend();
        this.xcoord = xcoord;
        this.ycoord = ycoord;
        currentshipstatus = "dead";
        numberoftaunts = _root.aimessages[1].length - 1;
        tauntnumber = Math.round(numberoftaunts * Math.random());
        message = _root.aimessages[1][tauntnumber];
        _root.func_messangercom(message, shipname, "END");
        tw = 0;
        this.ship.removeMovieClip();
        this.attachMovie("shiptype" + shiptype + "shield", "shipshield", 200);
        this.shipshield._alpha = 0;
        this.gotoAndPlay("dieing");
    } // end if
    if (currentshipstatus == "alive")
    {
        ++nextupdateframe;
        if (nextupdateframe > framedelay)
        {
            nextupdateframe = 0;
            if (shipemped == true)
            {
                if (timetillempends < getTimer())
                {
                    shipemped = false;
                } // end if
            } // end if
        } // end if
    } // end if
    if (nextmovementtime < curenttime)
    {
        nextmovementtime = curenttime + movementupdateinterval;
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        playerdistancefromship = Math.round(Math.sqrt((playersxcoord - this.xcoord) * (playersxcoord - this.xcoord) + (playersycoord - this.ycoord) * (playersycoord - this.ycoord)));
        if (interactedwithplay == false)
        {
            if (playerdistancefromship < maxplayerrangebeforeinteraction)
            {
                func_begininteraction();
                interactedwithplay = true;
            } // end if
        } // end if
        this.rotation = relativefacing = angletotravel;
        velocity = travelspeed * ((getTimer() - startedtime) / 1000) / shipspeedaleration;
        movementofanobjectwiththrust();
        this.xcoord = startxcoord + xmovement;
        this._x = this.xcoord - _root.shipcoordinatex;
        this.ycoord = startycoord + ymovement;
        this._y = this.ycoord - _root.shipcoordinatey;
    } // end if
};
stop ();

// [Action in Frame 3]
cycles = 0;
cyclestodo = 40;
this.onEnterFrame = function ()
{
    ++cycles;
    setProperty(this, _x, this.xcoord - _root.shipcoordinatex);
    setProperty(this, _y, this.ycoord - _root.shipcoordinatey);
    if (cycles > cyclestodo)
    {
        this.removeMovieClip();
    } // end if
};
stop ();

// [Action in Frame 4]
this.removeMovieClip();
