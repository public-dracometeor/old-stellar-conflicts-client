﻿// Action script...

// [onClipEvent of sprite 1746 in frame 1]
onClipEvent (load)
{
    textinfo = "CREATE A RACE";
}

// [onClipEvent of sprite 1746 in frame 1]
on (release)
{
    if (textinfo == "CREATE A RACE")
    {
        _root.func_main_clicksound();
        _parent.gotoAndStop("createarace");
    } // end if
}

// [onClipEvent of sprite 1746 in frame 1]
onClipEvent (load)
{
    textinfo = "SHOW MAP";
}

// [onClipEvent of sprite 1746 in frame 1]
on (release)
{
    if (_root.mapdsiplayed == false)
    {
        _root.mapdsiplayed = true;
        _root.attachMovie("ingamemap", "ingamemap", 9999999);
    }
    else if (_root.mapdsiplayed == true)
    {
        _root.mapdsiplayed = false;
        _root.createEmptyMovieClip("blanker", 9999999);
    } // end else if
}

// [Action in Frame 1]
function displaystats()
{
    if (_root.racingcheckpoints.length > 0)
    {
        this.butt._visible = false;
        if (_root.racinginformation[0] > getTimer())
        {
            bottominfo = "Race Start in " + Math.floor((_root.racinginformation[0] - getTimer()) / 1000) + ", Reward:" + _root.func_formatnumber(_root.racinginformation[1]);
        }
        else
        {
            bottominfo = "Race in Progress, Reward:" + _root.func_formatnumber(_root.racinginformation[1]);
        } // end else if
    }
    else if (_root.racingcheckpoints.length < 1)
    {
        bottominfo = "No Races Created";
        this.butt._visible = true;
    } // end else if
    checkpoints = 0;
    if (_root.racingcheckpoints.length > 0)
    {
        checkpoints = _root.racingcheckpoints.length;
    } // end if
    information = "NAV POINTS: " + checkpoints + "\r\r";
    if (_root.racingcheckpoints.length > 0)
    {
        startloc = _root.starbaselocation[0][0];
        information = information + (" START: " + startloc + "\r");
    } // end if
    for (jj = 0; jj < _root.racingcheckpoints.length; jj++)
    {
        dest = _root.sectormapitems[_root.racingcheckpoints[jj][0]][0];
        if (dest.substr(0, 2) == "PL")
        {
            dest = dest.substr(2);
        }
        else if (dest.substr(0, 2) == "NP")
        {
            dest = "NAV " + dest.substr(2);
        } // end else if
        information = information + (" " + (jj + 1) + ": " + dest + "\r");
    } // end of for
} // End of the function
if (_root.racingcheckpoints.length < 1)
{
    datatosend = datatosend + "RG`CG~";
    _root.mysocket.send(datatosend);
} // end if
displaystats();
counter = 0;
this.onEnterFrame = function ()
{
    ++counter;
    if (counter > 10)
    {
        displaystats();
        counter = 0;
    } // end if
};
stop ();
