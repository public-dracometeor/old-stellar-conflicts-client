﻿// Action script...

function displaypaths()
{
    startloc = _root.starbaselocation[0][0];
    distance = 0;
    lasty = Number(_root.starbaselocation[0][2]);
    lastx = Number(_root.starbaselocation[0][1]);
    for (jj = 0; jj < maxspots; jj++)
    {
        this["point" + jj].loc = jj + 1;
        this["point" + jj].location = jj;
        if (raceroute[jj] == null)
        {
            this["point" + jj].dest = "NONE SELECTED";
            continue;
        } // end if
        dest = _root.sectormapitems[raceroute[jj]][0];
        if (dest.substr(0, 2) == "PL")
        {
            dest = dest.substr(2);
        }
        else if (dest.substr(0, 2) == "NP")
        {
            dest = "NAV " + dest.substr(2);
        } // end else if
        this["point" + jj].dest = dest;
        xloc = Number(_root.sectormapitems[raceroute[jj]][1]);
        yloc = Number(_root.sectormapitems[raceroute[jj]][2]);
        xdiff = lastx - xloc;
        ydiff = lasty - yloc;
        lasty = yloc;
        lastx = xloc;
        distance = distance + Math.sqrt(xdiff * xdiff + ydiff * ydiff);
    } // end of for
    rewardforrace = _root.func_getracereward(distance);
    bottominfo2 = "Distance: " + _root.func_formatnumber(Math.round(distance)) + " Reward: " + _root.func_formatnumber(rewardforrace);
    if (distance > 0)
    {
        this.butt2.textinfo = "CREATE RACE";
    }
    else
    {
        this.butt2.textinfo = "CANCEL";
    } // end else if
} // End of the function
function arrowpressed(direction, location)
{
    if (direction == "left")
    {
        if (raceroute[location] == null)
        {
            raceroute[location] = _root.sectormapitems.length - 1;
        }
        else
        {
            --raceroute[location];
        } // end else if
    }
    else if (direction == "right")
    {
        if (raceroute[location] == null)
        {
            raceroute[location] = 1;
        }
        else
        {
            ++raceroute[location];
        } // end else if
    } // end else if
    if (raceroute[location] < 1)
    {
        raceroute[location] = null;
    } // end if
    if (raceroute[location] >= _root.sectormapitems.length)
    {
        raceroute[location] = null;
    } // end if
    displaypaths();
} // End of the function
function sendnewgame()
{
    datatosend = "RG`NG`" + _root.racegamestartdelay + "`" + _root.func_getracereward(distance);
    for (jj = 0; jj < maxspots; jj++)
    {
        if (raceroute[jj] != null)
        {
            datatosend = datatosend + ("`" + raceroute[jj]);
        } // end if
    } // end of for
    datatosend = datatosend + "~";
    _root.testtt = datatosend;
    _root.mysocket.send(datatosend);
} // End of the function
raceroute = new Array();
maxspots = 5;
for (jj = 0; jj < maxspots; jj++)
{
    raceroute[jj] = null;
} // end of for
bottominfo = "";
displaypaths();
this.butt2._visible = true;
this.onEnterFrame = function ()
{
    displaypaths();
    if (_root.racingcheckpoints.length > 0)
    {
        this.butt._visible = false;
        this.gotoAndStop(1);
    } // end if
};
stop ();
