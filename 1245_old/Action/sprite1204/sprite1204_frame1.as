﻿// Action script...

// [onClipEvent of sprite 1188 in frame 1]
onClipEvent (load)
{
    function checksettings()
    {
        if (lastturretetting != _root.playershipstatus[9])
        {
            lastturretetting = _root.playershipstatus[9];
            if (lastturretetting == "MANUAL")
            {
                _parent.manual.gotoAndStop(1);
                _parent.auto.gotoAndStop(2);
                _parent.off.gotoAndStop(2);
            } // end if
            if (lastturretetting == "AUTO")
            {
                _parent.manual.gotoAndStop(2);
                _parent.auto.gotoAndStop(1);
                _parent.off.gotoAndStop(2);
            } // end if
            if (lastturretetting == "OFF")
            {
                _parent.manual.gotoAndStop(2);
                _parent.auto.gotoAndStop(2);
                _parent.off.gotoAndStop(1);
            } // end if
        } // end if
        drainrate = 0;
        if (_root.playershipstatus[2][2] != "OFF")
        {
            drainrate = drainrate + _root.shieldgenerators[_root.playershipstatus[2][0]][2];
            drainrate = drainrate + _root.playershipstatus[2][1] / _root.shieldgenerators[_root.playershipstatus[2][0]][0] * _root.shieldgenerators[_root.playershipstatus[2][0]][3];
        } // end if
        drainrate = Math.round(drainrate);
        if (lastdrainrate != drainrate)
        {
            lastdrainrate = drainrate;
            _parent.drainrate = "Drain:" + drainrate + "/s";
        } // end if
    } // End of the function
    lastdrainrate = 0;
    updateinterval = 1500;
    lastturretetting = null;
    _root.playershipstatus[9] = "OFF";
    checksettings();
}

// [onClipEvent of sprite 1194 in frame 1]
on (release)
{
    if (_root.playershipstatus[8].length > 0)
    {
        _root.playershipstatus[9] = "MANUAL";
        _parent.turretsetings.checksettings();
    } // end if
}

// [onClipEvent of sprite 1198 in frame 1]
on (release)
{
    _root.playershipstatus[9] = "OFF";
    _parent.turretsetings.checksettings();
}

// [onClipEvent of sprite 1203 in frame 1]
on (release)
{
    if (_root.playershipstatus[8].length > 0)
    {
        _root.playershipstatus[9] = "AUTO";
        _parent.turretsetings.checksettings();
    } // end if
}

// [Action in Frame 1]
stop ();
