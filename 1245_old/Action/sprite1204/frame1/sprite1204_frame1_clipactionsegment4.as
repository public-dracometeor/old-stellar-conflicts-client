﻿// Action script...

// [onClipEvent of sprite 1188 in frame 1]
onClipEvent (load)
{
    function checksettings()
    {
        if (lastturretetting != _root.playershipstatus[9])
        {
            lastturretetting = _root.playershipstatus[9];
            if (lastturretetting == "MANUAL")
            {
                _parent.manual.gotoAndStop(1);
                _parent.auto.gotoAndStop(2);
                _parent.off.gotoAndStop(2);
            } // end if
            if (lastturretetting == "AUTO")
            {
                _parent.manual.gotoAndStop(2);
                _parent.auto.gotoAndStop(1);
                _parent.off.gotoAndStop(2);
            } // end if
            if (lastturretetting == "OFF")
            {
                _parent.manual.gotoAndStop(2);
                _parent.auto.gotoAndStop(2);
                _parent.off.gotoAndStop(1);
            } // end if
        } // end if
        drainrate = 0;
        if (_root.playershipstatus[2][2] != "OFF")
        {
            drainrate = drainrate + _root.shieldgenerators[_root.playershipstatus[2][0]][2];
            drainrate = drainrate + _root.playershipstatus[2][1] / _root.shieldgenerators[_root.playershipstatus[2][0]][0] * _root.shieldgenerators[_root.playershipstatus[2][0]][3];
        } // end if
        drainrate = Math.round(drainrate);
        if (lastdrainrate != drainrate)
        {
            lastdrainrate = drainrate;
            _parent.drainrate = "Drain:" + drainrate + "/s";
        } // end if
    } // End of the function
    lastdrainrate = 0;
    updateinterval = 1500;
    lastturretetting = null;
    _root.playershipstatus[9] = "OFF";
    checksettings();
}
