﻿// Action script...

// [Action in Frame 3]
cycles = 0;
cyclestodo = 40;
this.onEnterFrame = function ()
{
    ++cycles;
    setProperty(this, _x, this.xcoord - _root.shipcoordinatex);
    setProperty(this, _y, this.ycoord - _root.shipcoordinatey);
    if (cycles > cyclestodo)
    {
        this.removeMovieClip();
    } // end if
};
stop ();
