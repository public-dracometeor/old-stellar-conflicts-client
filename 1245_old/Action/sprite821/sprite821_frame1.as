﻿// Action script...

// [Action in Frame 1]
function adddamage(damagetoadd, damagefrom)
{
    damagetoadd = Number(damagetoadd);
    currentshipshield = currentshipshield - damagetoadd;
    if (currentshipshield < 0)
    {
        shiphealth = shiphealth + currentshipshield;
        currentshipshield = 0;
    } // end if
    if (shiphealth <= 0)
    {
        currentshipstatus = "dieing";
        i = 999999;
        shipkillerid = damagefrom;
    } // end if
    this.ship.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
    this.ship.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
} // End of the function
function aishipgunfire()
{
    gunshot0fired = false;
    gunshot1fired = false;
    gunshot2fired = false;
    currenttime = _root.curenttime;
    hardpointfired = false;
    shotinformation = "";
    for (i = 0; i < noofhardpoints; i++)
    {
        if (hardpoints[i][1] < currenttime && !shipemped)
        {
            hardpointfired = true;
            hardpoints[i][1] = hardpoints[i][4] + currenttime;
            xfireposition = hardpoints[i][2];
            yfireposition = hardpoints[i][3];
            velocity = currentvelocity * currenttimechangeratio + hardpoints[i][5] * currenttimechangeratio;
            firingbulletstartlocation();
            gunshottypenumber = hardpoints[i][0];
            if (gunshottypenumber == 0)
            {
                gunshot0fired = true;
            } // end if
            if (gunshottypenumber == 1)
            {
                gunshot1fired = true;
            } // end if
            if (gunshottypenumber == 2)
            {
                gunshot2fired = true;
            } // end if
            if (_root.othergunfire.length < 1)
            {
                _root.othergunfire = new Array();
            } // end if
            ++_root.currentotherplayshot;
            if (_root.currentotherplayshot >= 500)
            {
                _root.currentotherplayshot = 1;
            } // end if
            lastvar = _root.othergunfire.length;
            ++_root.othergunfirecount;
            _root.othergunfire[lastvar] = new Array();
            _root.othergunfire[lastvar][8] = _root.currentotherplayshot;
            shotinformation = shotinformation + "GF`";
            shotinformation = shotinformation + (_root.othergunfire[lastvar][0] = "AI" + globalid);
            shotinformation = shotinformation + "`";
            shotinformation = shotinformation + (_root.othergunfire[lastvar][1] = Math.round(xfireposition + xcoord));
            shotinformation = shotinformation + "`";
            shotinformation = shotinformation + (_root.othergunfire[lastvar][2] = Math.round(yfireposition + ycoord));
            shotinformation = shotinformation + "`";
            shotinformation = shotinformation + (velocity = Math.round(currentvelocity + hardpoints[i][5]));
            shotinformation = shotinformation + "`";
            shotinformation = shotinformation + (relativefacing = _root.othergunfire[lastvar][6] = Math.round(this.rotation));
            shotinformation = shotinformation + "`";
            shotinformation = shotinformation + (_root.othergunfire[lastvar][7] = hardpoints[i][0]);
            shotinformation = shotinformation + "`";
            shotinformation = shotinformation + (_root.othergunfire[lastvar][9] = currentgunfireshot);
            shotinformation = shotinformation + "`";
            shotinformation = shotinformation + (_root.func_globaltimestamp() + "~");
            _root.othergunfire[lastvar][5] = _root.guntype[hardpoints[i][0]][1] * 1000 + _root.curenttime;
            _root.othergunfire[lastvar][10] = "AI";
            movementofanobjectwiththrust();
            _root.othergunfire[lastvar][3] = xmovement;
            _root.othergunfire[lastvar][4] = ymovement;
            ++currentgunfireshot;
            if (currentgunfireshot > 500)
            {
                currentgunfireshot = 0;
            } // end if
            _root.ainformationtosend = _root.ainformationtosend + shotinformation;
            _root.gamedisplayarea.attachMovie("guntype" + _root.othergunfire[lastvar][7] + "fire", "othergunfire" + _root.othergunfire[lastvar][8], 3500 + _root.othergunfire[lastvar][8]);
            setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _rotation, _root.othergunfire[lastvar][6]);
            setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _x, _root.othergunfire[lastvar][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _y, _root.othergunfire[lastvar][2] - _root.shipcoordinatey);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".xmovement", _root.othergunfire[lastvar][3]);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".ymovement", _root.othergunfire[lastvar][4]);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".xposition", _root.othergunfire[lastvar][1]);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".yposition", _root.othergunfire[lastvar][2]);
            _root.othergunfire[lastvar][11] = _root.gamedisplayarea["othergunfire" + _root.othergunfire[lastvar][8]]._width / 2;
            _root.othergunfire[lastvar][12] = _root.gamedisplayarea["othergunfire" + _root.othergunfire[lastvar][8]]._height / 2;
            if (_root.soundvolume == "on")
            {
                _root["guntype" + _root.othergunfire[lastvar][7] + "sound"].start();
            } // end if
        } // end if
    } // end of for
} // End of the function
function movementofanobjectwiththrust()
{
    relativefacing = this.rotation;
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    } // end if
    if (velocity != 0)
    {
        this.relativefacing = Math.round(this.relativefacing);
        ymovement = Math.round(-velocity * _root.cosines[this.relativefacing]);
        xmovement = Math.round(velocity * _root.sines[this.relativefacing]);
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function firingbulletstartlocation()
{
    hardpointfromcenter = Math.sqrt(xfireposition * xfireposition + yfireposition * yfireposition);
    initialangleforgun = Math.ASIN(xfireposition / hardpointfromcenter) / 0.017453;
    rotationangle = initialangleforgun + this.ship._rotation;
    xfireposition = hardpointfromcenter * Math.sin(0.017453 * rotationangle);
    yfireposition = -hardpointfromcenter * Math.cos(0.017453 * rotationangle);
} // End of the function
function deathinfosend()
{
    datatosend = "AI`death`" + shipid + "`" + String(shipbounty) + "`" + Math.round(shipbounty * _root.playershipstatus[5][6]) + "`" + shipbounty + "~";
    _root.mysocket.send(datatosend);
    for (i = 0; i < _root.aishipshosted.length; i++)
    {
        if (shipid == _root.aishipshosted[i][0])
        {
            _root.aishipshosted.splice(i, 1);
            i = 999999;
        } // end if
    } // end of for
} // End of the function
function dropai(reason)
{
    datatosend = "AI`drop`" + shipid;
    "~";
    _root.mysocket.send(datatosend);
    if (reason == "otherships")
    {
        message = "Cya Mate, Looks like another player wants to battle you!";
        _root.func_messangercom(message, shipname, "BEGIN");
    } // end if
    if (reason == "friendlyfire")
    {
        message = "Hey, I\'m not going to play with your friend!";
        _root.func_messangercom(message, shipname, "BEGIN");
    } // end if
    for (i = 0; i < _root.aishipshosted.length; i++)
    {
        if (shipid == _root.aishipshosted[i][0])
        {
            _root.aishipshosted.splice(i, 1);
            this.gotoAndPlay("dropaiship");
            i = 999999;
        } // end if
    } // end of for
} // End of the function
function otherplayershipshots()
{
    for (cc = 0; cc < _root.othergunfire.length; cc++)
    {
        if (this.hitTest("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]) == true && currentshipstatus == "alive" && _root.othergunfire[cc][10] != "AI")
        {
            playerwhoshothitid = _root.othergunfire[cc][0];
            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
            _root.gunfireinformation = _root.gunfireinformation + ("GH`AI" + globalid + "`" + _root.othergunfire[cc][0] + "`" + _root.othergunfire[cc][9] + "~");
            gunshothitplayertype = _root.othergunfire[cc][7];
            _root.othergunfire.splice(cc, 1);
            --cc;
            currentshipshield = currentshipshield - _root.guntype[gunshothitplayertype][4];
            if (currentshipshield < 0)
            {
                shiphealth = shiphealth + currentshipshield;
                currentshipshield = 0;
            } // end if
            if (shiphealth <= 0 && currentshipstatus == "alive")
            {
                currentshipstatus = "dieing";
                shipkillerid = playerwhoshothitid;
                continue;
            } // end if
            this.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
            this.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
        } // end if
    } // end of for
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function
i = 0;
if (turrettype > 0)
{
    this.attachMovie("turrettypeaiship", "turrettypeaiship0", 200);
    this.turrettypeaiship0.shottype = turrettype;
} // end if
currenttimechangeratio = _root.currenttimechangeratio;
currenthostplayer = _root.playershipstatus[3][0];
lastshooter = new Array();
currentgunfireshot = 0;
shipkillerid = null;
shipid = Number(this.shipid);
for (i = 0; i < _root.aishipshosted.length; i++)
{
    if (shipid == _root.aishipshosted[i][0])
    {
        this.xcoord = _root.aishipshosted[i][1];
        this.ycoord = _root.aishipshosted[i][2];
        this.rotation = _root.aishipshosted[i][3];
        currentvelocity = _root.aishipshosted[i][4];
        shiptype = _root.aishipshosted[i][6];
        shipname = _root.aishipshosted[i][7];
        shieldgenerator = _root.aishipshosted[i][8];
        guntype = _root.aishipshosted[i][9];
        currentshipidno = i;
    } // end if
} // end of for
this.ainame = shipname;
distancetodropai = 12000;
maxshipshield = _root.shieldgenerators[shieldgenerator][0];
shipshieldreplenish = Math.round(_root.shieldgenerators[shieldgenerator][1]);
currentshipshield = maxshipshield;
currentshipstatus = "alive";
rotationspeed = _root.shiptype[shiptype][3][2];
acceleration = Math.round(_root.shiptype[shiptype][3][0]);
maxvelocity = Math.round(_root.shiptype[shiptype][3][1]);
rotationspeed = _root.shiptype[shiptype][3][2];
shiphealth = _root.shiptype[shiptype][3][3];
noofhardpoints = _root.shiptype[shiptype][2].length;
hardpoints = new Array();
for (i = 0; i < noofhardpoints; i++)
{
    hardpoints[i] = new Array();
    hardpoints[i][0] = guntype;
    hardpoints[i][1] = 0;
    hardpoints[i][2] = _root.shiptype[shiptype][2][i][0];
    hardpoints[i][3] = _root.shiptype[shiptype][2][i][1];
    hardpoints[i][4] = Math.round(_root.guntype[hardpoints[i][0]][2] * 1000);
    hardpoints[i][5] = Math.round(_root.guntype[hardpoints[i][0]][0]);
    hardpoints[i][6] = _root.guntype[hardpoints[i][0]][3];
} // end of for
this.attachMovie("shiptype" + shiptype, "ship", 1);
numberoftaunts = _root.aimessages[0].length - 1;
tauntnumber = Math.round(numberoftaunts * Math.random());
message = _root.aimessages[0][tauntnumber];
_root.func_messangercom(message, shipname, "BEGIN");
maxupdateinterval = 2000;
lastdatasent = _root.curenttime + maxupdateinterval;
shiprotationdegredation = _root.shiptype[shiptype][3][5];
totalshipworth = _root.shiptype[shiptype][1];
for (currenthardpoint = 0; currenthardpoint < hardpoints.length; currenthardpoint++)
{
    currentguntype = hardpoints[0][currenthardpoint][0];
    if (!isNaN(currentguntype))
    {
        totalshipworth = totalshipworth + _root.guntype[currentguntype][5];
    } // end if
} // end of for
totalshipworth = totalshipworth + _root.shieldgenerators[shieldgenerator][5];
shipbounty = Math.round(totalshipworth / _root.playersworthtobtymodifire);
_root.aishipshosted[currentshipidno][10] = shipbounty;
