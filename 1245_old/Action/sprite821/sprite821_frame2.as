﻿// Action script...

// [Action in Frame 2]
_root.aishipshosted[currentshipidno][20] = shiphealth;
_root.aishipshosted[currentshipidno][21] = shiphealth;
_root.aishipshosted[currentshipidno][22] = maxshipshield;
_root.aishipshosted[currentshipidno][23] = maxshipshield;
curenttime = _root.curenttime;
shipdisrupted = false;
shipemped = false;
hitteestintervals = 4;
currentframeforhittest = Math.round(Math.random() * hitteestintervals);
nextupdateframe = 0;
framedelay = 7;
nextupdateframe = Math.round(Math.random() * framedelay);
movementupdateinterval = 40;
nextmovementtime = curenttime + movementupdateinterval;
lastshipdisplaytime = curenttime;
globalid = _root.playershipstatus[3][0] + "-" + shipid;
this.onEnterFrame = function ()
{
    curenttime = _root.curenttime;
    ++currentframeforhittest;
    if (currentshipstatus == "alive" && currentframeforhittest > hitteestintervals)
    {
        currentframeforhittest = 0;
        shieldtimechangeratio = (curenttime - lastshieldtime) / 1000;
        lastshieldtime = curenttime;
        if (shipemped == true)
        {
            currentshipshield = 0;
        }
        else if (currentshipshield < maxshipshield)
        {
            currentshipshield = currentshipshield + shipshieldreplenish * shieldtimechangeratio;
            if (currentshipshield > maxshipshield)
            {
                currentshipshield = maxshipshield;
            } // end if
        } // end else if
        numberofplayershots = _root.playershotsfired.length;
        if (numberofplayershots > 0)
        {
            halfshipswidth = this.ship._width / 2;
            halfshipsheight = this.ship._height / 2;
            this.relativex = this._x;
            this.relativey = this._y;
        } // end if
        for (i = 0; i < numberofplayershots; i++)
        {
            bulletshotid = _root.playershotsfired[i][0];
            bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
            bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
            if (bulletsx != null)
            {
                if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
                {
                    if (_root.playershotsfired[i][13] == "GUNS")
                    {
                        currentshipshield = currentshipshield - _root.guntype[_root.playershotsfired[i][6]][4];
                        _root.gunfireinformation = _root.gunfireinformation + ("GH`AI" + globalid + "`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[cc][0] + "~");
                    }
                    else if (_root.playershotsfired[i][13] == "MISSILE")
                    {
                        missiletype = _root.playershotsfired[i][6];
                        currentshipshield = currentshipshield - _root.missile[missiletype][4];
                        if (_root.missile[missiletype][8] == "EMP")
                        {
                            timetillempends = getTimer() + _root.missile[missiletype][9];
                            shipemped = true;
                        } // end if
                        if (_root.missile[missiletype][8] == "DISRUPTER")
                        {
                            timedisruptedends = getTimer() + _root.missile[missiletype][9];
                            shipdisrupted = true;
                        } // end if
                    } // end else if
                    if (currentshipshield < 0)
                    {
                        shiphealth = shiphealth + currentshipshield;
                        currentshipshield = 0;
                    } // end if
                    if (shiphealth <= 0)
                    {
                        currentshipstatus = "dieing";
                        i = 999999;
                        shipkillerid = currenthostplayer;
                    } // end if
                    removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                    _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                    lastshooter[2] = lastshooter[1];
                    lastshooter[1] = lastshooter[0];
                    lastshooter[0] = currenthostplayer;
                    _root.playershotsfired[i][5] = 0;
                    this.ship.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
                    this.ship.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
                } // end if
            } // end if
        } // end of for
        otherplayershipshots();
    } // end if
    if (currentshipstatus == "dieing")
    {
        deathinfosend();
        this.xcoord = xcoord;
        this.ycoord = ycoord;
        currentshipstatus = "dead";
        numberoftaunts = _root.aimessages[1].length - 1;
        tauntnumber = Math.round(numberoftaunts * Math.random());
        message = _root.aimessages[1][tauntnumber];
        _root.func_messangercom(message, shipname, "END");
        for (tw = 0; tw < _root.aishipshosted.length; tw++)
        {
            if (shipname == _root.aishipshosted[tw][0])
            {
                _root.aishipshosted.splice(tw, 1);
            } // end if
        } // end of for
        this.ship.removeMovieClip();
        this.attachMovie("shiptype" + shiptype + "shield", "shipshield", 200);
        this.shipshield._alpha = 0;
        this.gotoAndPlay("dieing");
    } // end if
    if (currentshipstatus == "alive")
    {
        ++nextupdateframe;
        if (nextupdateframe > framedelay)
        {
            nextupdateframe = 0;
            if (shipemped == true)
            {
                if (timetillempends < getTimer())
                {
                    shipemped = false;
                } // end if
            } // end if
            if (shipdisrupted == true)
            {
                if (timedisruptedends < getTimer())
                {
                    shipdisrupted = false;
                } // end if
            } // end if
            if (_root.hostileplayerships == true)
            {
                dropai("otherships");
            } // end if
            playerdistancefromship = Math.round(Math.sqrt((playersxcoord - this.xcoord) * (playersxcoord - this.xcoord) + (playersycoord - this.ycoord) * (playersycoord - this.ycoord)));
            playeranglefromship = 360 - Math.atan2(this.xcoord - playersxcoord, this.ycoord - playersycoord) / 0.017453;
            playeranglefromship = playeranglefromship - rotation;
            if (playeranglefromship > 360)
            {
                playeranglefromship = playeranglefromship - 360;
            } // end if
            if (playeranglefromship < 0)
            {
                playeranglefromship = playeranglefromship + 360;
            } // end if
            if (playerdistancefromship > distancetodropai)
            {
                distancedropai();
            } // end if
            playersshipspeed = _root.playershipvelocity;
            currentkeypresses = "";
            this.rotating = 0;
            accelerating = 0;
            if (playeranglefromship < 10 || playeranglefromship >= 350)
            {
                if (playerdistancefromship < 60)
                {
                    aishipgunfire();
                    if (currentvelocity < playersshipspeed - acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "U";
                    } // end if
                    if (currentvelocity > playersshipspeed + acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "D";
                    } // end if
                }
                else if (playerdistancefromship >= 60 && playerdistancefromship < 150)
                {
                    aishipgunfire();
                    if (currentvelocity < playersshipspeed - acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "U";
                    } // end if
                    if (currentvelocity > playersshipspeed + acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "D";
                    } // end if
                }
                else if (playerdistancefromship >= 150)
                {
                    if (currentvelocity < maxvelocity - acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "U";
                    } // end else if
                } // end else if
            }
            else if (playeranglefromship < 210)
            {
                if (playeranglefromship >= 10 && playeranglefromship < 45)
                {
                    if (playerdistancefromship < 60)
                    {
                        aishipgunfire();
                        if (currentvelocity < playersshipspeed - acceleration * currenttimechangeratio * 2)
                        {
                            currentkeypresses = currentkeypresses + "U";
                        } // end if
                        if (currentvelocity > playersshipspeed + acceleration * currenttimechangeratio * 2)
                        {
                            currentkeypresses = currentkeypresses + "D";
                        } // end if
                    }
                    else if (playerdistancefromship >= 60 && playerdistancefromship < 200)
                    {
                        aishipgunfire();
                        if (currentvelocity < playersshipspeed - acceleration * currenttimechangeratio * 2)
                        {
                            currentkeypresses = currentkeypresses + "U";
                        } // end if
                        if (currentvelocity > playersshipspeed + acceleration * currenttimechangeratio * 2)
                        {
                            currentkeypresses = currentkeypresses + "D";
                        } // end if
                    }
                    else if (playerdistancefromship >= 200)
                    {
                        if (currentvelocity < maxvelocity - acceleration * currenttimechangeratio * 2)
                        {
                            currentkeypresses = currentkeypresses + "U";
                        } // end else if
                    } // end else if
                    this.rotating = 1;
                    currentkeypresses = currentkeypresses + "R";
                }
                else if (playeranglefromship >= 45 && playeranglefromship < 90)
                {
                    this.rotating = 1;
                    if (currentvelocity < maxvelocity - acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "U";
                    } // end if
                    currentkeypresses = currentkeypresses + "R";
                }
                else if (playeranglefromship >= 90 && playeranglefromship < 120)
                {
                    this.rotating = 1;
                    if (currentvelocity > maxvelocity / 3 * 2 + acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "D";
                    } // end if
                    currentkeypresses = currentkeypresses + "R";
                }
                else if (playeranglefromship >= 120 && playeranglefromship < 210)
                {
                    this.rotating = 1;
                    if (currentvelocity > maxvelocity / 2 + acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "D";
                    } // end if
                    currentkeypresses = currentkeypresses + "R";
                } // end else if
            }
            else if (playeranglefromship >= 210 && playeranglefromship < 270)
            {
                this.rotating = -1;
                if (currentvelocity > maxvelocity / 3 * 2 + acceleration * currenttimechangeratio * 2)
                {
                    currentkeypresses = currentkeypresses + "D";
                } // end if
                currentkeypresses = currentkeypresses + "L";
            }
            else if (playeranglefromship >= 270 && playeranglefromship < 315)
            {
                this.rotating = -1;
                if (currentvelocity < maxvelocity - acceleration * currenttimechangeratio * 2)
                {
                    currentkeypresses = currentkeypresses + "U";
                } // end if
                currentkeypresses = currentkeypresses + "L";
            }
            else if (playeranglefromship >= 315 && playeranglefromship < 350)
            {
                if (playerdistancefromship < 60)
                {
                    aishipgunfire();
                    if (currentvelocity < playersshipspeed - acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "U";
                    } // end if
                    if (currentvelocity > playersshipspeed + acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "D";
                    } // end if
                }
                else if (playerdistancefromship >= 60 && playerdistancefromship < 200)
                {
                    aishipgunfire();
                    if (currentvelocity < playersshipspeed - acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "U";
                    } // end if
                    if (currentvelocity > playersshipspeed + acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "D";
                    } // end if
                }
                else if (playerdistancefromship >= 200)
                {
                    if (currentvelocity < maxvelocity - acceleration * currenttimechangeratio * 2)
                    {
                        currentkeypresses = currentkeypresses + "U";
                    } // end else if
                } // end else if
                this.rotating = -1;
                currentkeypresses = currentkeypresses + "L";
            } // end else if
            if (shipdisrupted)
            {
                currentvelocity = 0;
                if (this.rotating == -1)
                {
                    currentkeypresses = "DL";
                }
                else if (this.rotating == 1)
                {
                    currentkeypresses = "DR";
                }
                else
                {
                    currentkeypresses = "D";
                } // end else if
            } // end else if
            shipaccelerating = 0;
            for (j = 0; j < currentkeypresses.length; j++)
            {
                if (currentkeypresses.charAt(j) == "U")
                {
                    shipaccelerating = acceleration;
                    continue;
                } // end if
                if (currentkeypresses.charAt(j) == "D")
                {
                    shipaccelerating = -acceleration;
                } // end if
            } // end of for
        } // end if
    } // end if
    if (nextmovementtime < curenttime)
    {
        nextmovementtime = curenttime + movementupdateinterval;
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        currentshipdisplaychangeratio = (curenttime - lastshipdisplaytime) / 1000;
        lastshipdisplaytime = curenttime;
        currentvelocity = currentvelocity + shipaccelerating * currentshipdisplaychangeratio;
        if (currentvelocity > maxvelocity)
        {
            currentvelocity = maxvelocity;
        } // end if
        if (currentvelocity < 0)
        {
            currentvelocity = 0;
        } // end if
        velocity = Math.round(currentvelocity * currentshipdisplaychangeratio);
        aishiprotating = 0;
        if (this.rotating > 0)
        {
            aishiprotating = aishiprotating + rotationspeed;
        } // end if
        if (this.rotating < 0)
        {
            aishiprotating = aishiprotating - rotationspeed;
        } // end if
        if (this.aishiprotating != 0)
        {
            aishiprotating = aishiprotating - currentvelocity / maxvelocity * shiprotationdegredation * aishiprotating;
            this.rotation = this.rotation + aishiprotating * currentshipdisplaychangeratio;
            if (this.rotation > 360)
            {
                this.rotation = this.rotation - 360;
            } // end if
            if (this.rotation < 0)
            {
                this.rotation = this.rotation + 360;
            } // end if
        } // end if
        velocity = Math.round(currentvelocity * currentshipdisplaychangeratio);
        relativefacing = Math.round(this.rotation);
        movementofanobjectwiththrust();
        this.xcoord = this.xcoord + xmovement;
        this.ycoord = this.ycoord + ymovement;
        this._x = this.xcoord - _root.shipcoordinatex;
        this._y = this.ycoord - _root.shipcoordinatey;
        if (lastkeypresses != currentkeypresses || lastdatasent < _root.curenttime)
        {
            lastdatasent = _root.curenttime + maxupdateinterval;
            lastkeypresses = currentkeypresses;
            _root.ainformationtosend = "AI`AI" + globalid + "`" + Math.round(this.xcoord) + "`" + Math.round(this.ycoord) + "`" + Math.round(relativefacing) + "`" + Math.round(currentvelocity) + "`" + currentkeypresses + "`" + shiptype + "`" + _root.func_globaltimestamp() + "~";
        } // end if
        if (lastshiprot != rotation)
        {
            lastshiprot = rotation;
            this.ship._rotation = rotation;
        } // end if
        if (shipid == _root.aishipshosted[currentshipidno][0])
        {
        }
        else
        {
            for (i = 0; i < _root.aishipshosted.length; i++)
            {
                if (shipid == _root.aishipshosted[i][0])
                {
                    currentshipidno = i;
                } // end if
            } // end of for
        } // end else if
        _root.aishipshosted[currentshipidno][1] = this.xcoord;
        _root.aishipshosted[currentshipidno][2] = this.ycoord;
        _root.aishipshosted[currentshipidno][20] = shiphealth;
        _root.testtt = currentshipshield + "`" + shiphealth;
        _root.aishipshosted[currentshipidno][22] = currentshipshield;
    } // end if
};

stop ();
