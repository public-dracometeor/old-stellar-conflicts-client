﻿// Action script...

// [Action in Frame 1]
specialitemlocation = Number(specialitemlocation);
specialsnumber = _root.playershipstatus[11][1][specialitemlocation][0];
specialname = _root.specialshipitems[specialsnumber][0];
quantity = _root.playershipstatus[11][1][specialitemlocation][1];
label = specialitemlocation + 1 + ": " + specialname;
if (quantity >= 0)
{
    label = label + (" (" + quantity + ")");
} // end if
if (_root.playershipstatus[11][1][specialitemlocation][1] >= 0)
{
    this.action = "DROP";
}
else
{
    this.action = "SELL";
} // end else if
stop ();
