﻿// Action script...

// [onClipEvent of sprite 682 in frame 1]
onClipEvent (enterFrame)
{
    itemspacing = 50;
    sellingyindent = 0;
    buyingyindent = 0;
    itemsx = 100;
    topitemy = 100;
    sellingyindent = topitemy;
    this._x = 0;
    this._y = 0;
    if (currentitemdisplay != null)
    {
        if (currentitemdisplay == "Buy")
        {
            setProperty("sellbutton", _alpha, "50");
            setProperty("buybutton", _alpha, "100");
        } // end if
        if (currentitemdisplay == "Sell")
        {
            setProperty("sellbutton", _alpha, "100");
            setProperty("buybutton", _alpha, "50");
        } // end if
        for (i = 0; i < _root.starbasetradeitem.length; i++)
        {
            removeMovieClip ("tradegooditem" + i);
        } // end of for
    } // end if
    for (i = 0; i < _root.starbasetradeitem.length; i++)
    {
        if (currentitemdisplay == "Buy")
        {
            this.attachMovie("purchasegoodsbuyheader", "purchasegoodsbuyheader", 50);
            setProperty("purchasegoodsbuyheader", _x, itemsx);
            setProperty("purchasegoodsbuyheader", _y, topitemy - itemspacing);
            if (_root.starbasetradeitem[i][2] == "selling")
            {
                this.attachMovie("tradegooditemlisting", "tradegooditem" + i, 100 + i);
                itemtype = _root.starbasetradeitem[i][0];
                set("tradegooditem" + i + ".itemtype", itemtype);
                set("tradegooditem" + i + ".itemname", _root.tradegoods[itemtype][0]);
                set("tradegooditem" + i + ".itemprice", _root.starbasetradeitem[i][1]);
                set("tradegooditem" + i + ".quantity", "0");
                set("tradegooditem" + i + ".functiontodo", "Buy");
                setProperty("tradegooditem" + i, _y, sellingyindent);
                setProperty("tradegooditem" + i, _x, itemsx);
                sellingyindent = sellingyindent + itemspacing;
            } // end if
            currentproccees = currentitemdisplay;
        } // end if
        if (currentitemdisplay == "Sell")
        {
            this.attachMovie("purchasegoodssellheader", "purchasegoodssellheader", 50);
            setProperty("purchasegoodssellheader", _x, itemsx);
            setProperty("purchasegoodssellheader", _y, topitemy - itemspacing);
            if (_root.starbasetradeitem[i][2] == "buying")
            {
                this.attachMovie("tradegooditemlisting", "tradegooditem" + i, 100 + i);
                itemtype = _root.starbasetradeitem[i][0];
                set("tradegooditem" + i + ".itemtype", itemtype);
                set("tradegooditem" + i + ".itemname", _root.tradegoods[itemtype][0]);
                set("tradegooditem" + i + ".itemprice", _root.starbasetradeitem[i][1]);
                set("tradegooditem" + i + ".quantity", "0");
                set("tradegooditem" + i + ".functiontodo", "Sell");
                setProperty("tradegooditem" + i, _y, sellingyindent);
                setProperty("tradegooditem" + i, _x, itemsx);
                sellingyindent = sellingyindent + itemspacing;
            } // end if
            currentproccees = currentitemdisplay;
        } // end if
    } // end of for
    currentitemdisplay = null;
}
