﻿// Action script...

// [onClipEvent of sprite 682 in frame 1]
onClipEvent (load)
{
    removeMovieClip ("_root.starbasehardwarebutton");
    removeMovieClip ("_root.exitstarbasebutton");
    removeMovieClip ("_root.purchaseshipbutton");
    removeMovieClip ("_root.purchasetradegoodsbutton");
    this.attachMovie("purchasetradegoodsselectionbutton", "buybutton", 10);
    setProperty("buybutton", _y, 100);
    setProperty("buybutton", _x, 10);
    this.buybutton.actiontype = "Buy";
    this.attachMovie("purchasetradegoodsselectionbutton", "sellbutton", 11);
    setProperty("sellbutton", _y, 150);
    setProperty("sellbutton", _x, 10);
    this.sellbutton.actiontype = "Sell";
    this.attachMovie("shopexitbutton", "exitbutton", 12);
    setProperty("exitbutton", _y, 400);
    setProperty("exitbutton", _x, 10);
    this.attachMovie("starbasefundsdisplay", "funds", 121);
    this.funds._y = 15;
    this.funds._x = _root.gameareawidth - 100;
    currentitemdisplay = "Buy";
}
