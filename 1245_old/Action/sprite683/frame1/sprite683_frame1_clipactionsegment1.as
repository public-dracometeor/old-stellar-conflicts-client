﻿// Action script...

// [onClipEvent of sprite 682 in frame 1]
onClipEvent (enterFrame)
{
    _parent.funds = _root.playershipstatus[3][1];
    maxcargo = _root.shiptype[_root.playershipstatus[5][0]][4];
    for (i = 0; i < _root.tradegoods.length; i++)
    {
        maxcargo = maxcargo - _root.playershipstatus[4][1][i];
    } // end of for
    for (i = 0; i < _root.starbasetradeitem.length; i++)
    {
        itemtype = _root.starbasetradeitem[i][0];
        if (_root.starbasetradeitem[i][2] == "selling" && currentproccees != "Sell")
        {
            itemtype = _root.starbasetradeitem[i][0];
            maxpurchaseprice = maxcargo * _root.starbasetradeitem[i][1];
            if (maxpurchaseprice <= _root.playershipstatus[3][1])
            {
                maxpurchaseofitems = maxcargo;
            }
            else
            {
                maxpurchaseofitems = Math.floor(_root.playershipstatus[3][1] / _root.starbasetradeitem[i][1]);
            } // end else if
            set("tradegooditem" + i + ".maxquantity", maxpurchaseofitems);
            set("tradegooditem" + i + ".itemowned", _root.playershipstatus[4][1][itemtype]);
        } // end if
        if (_root.starbasetradeitem[i][2] == "buying" && currentproccees == "Sell")
        {
            playerhasunits = _root.playershipstatus[4][1][itemtype];
            set("tradegooditem" + i + ".maxquantity", "");
            set("tradegooditem" + i + ".itemowned", _root.playershipstatus[4][1][itemtype]);
        } // end if
    } // end of for
}
