﻿// Action script...

function displaystats()
{
    for (loc = 0; loc < _root.currentonlineplayers.length; loc++)
    {
        if (_root.currentonlineplayers[loc][0] == _root.playershipstatus[3][0])
        {
            if (_root.playershipstatus[5][2] != _root.currentonlineplayers[loc][4])
            {
                _root.playershipstatus[5][2] = _root.currentonlineplayers[loc][4];
                this.butt.textinfo = "CHANGE TEAM";
            } // end if
        } // end if
    } // end of for
    if (_root.playershipstatus[5][2] != "N/A")
    {
        this.butt.textinfo = "CHANGE TEAM";
    }
    else
    {
        this.butt.textinfo = "GO ON TEAM";
    } // end else if
    information = "Teams: " + _root.teambases.length + "\r";
    teamsno = new Array();
    for (jj = 0; jj < _root.teambases.length; jj++)
    {
        teamsno[jj] = 0;
    } // end of for
    for (jj = 0; jj < _root.currentonlineplayers.length; jj++)
    {
        if (_root.currentonlineplayers[jj][4] != "N/A")
        {
            teamsno[_root.currentonlineplayers[jj][4]] = teamsno[_root.currentonlineplayers[jj][4]] + 1;
        } // end if
    } // end of for
    if (_root.squadwarinfo[0] == true)
    {
        information = information + "Team / Squad Size / Base Structure\r";
        for (jj = 0; jj < _root.teambases.length; jj++)
        {
            information = information + (" " + _root.teambases[jj][0].substr(2) + "  /  " + _root.squadwarinfo[1][jj] + ": " + teamsno[jj] + "  /  " + _root.teambases[jj][10] + "\r");
        } // end of for
        if (_root.playershipstatus[5][2] != "N/A")
        {
            yourteam = "Your Team: " + _root.teambases[_root.playershipstatus[5][2]][0].substr(2) + "  /  " + _root.squadwarinfo[1][_root.playershipstatus[5][2]] + "\r";
        }
        else
        {
            yourteam = "Your Team: " + _root.playershipstatus[5][2];
        } // end else if
    }
    else
    {
        information = information + "Team Size / Base Structure\r";
        for (jj = 0; jj < _root.teambases.length; jj++)
        {
            information = information + (" " + _root.teambases[jj][0].substr(2) + ": " + teamsno[jj] + "  " + _root.teambases[jj][10] + "\r");
        } // end of for
        if (_root.playershipstatus[5][2] != "N/A")
        {
            yourteam = "Your Team: " + _root.teambases[_root.playershipstatus[5][2]][0].substr(2) + "\r";
        }
        else
        {
            yourteam = "Your Team: " + _root.playershipstatus[5][2];
        } // end else if
    } // end else if
} // End of the function
displaystats();
counter = 0;
this.onEnterFrame = function ()
{
    ++counter;
    if (counter > 10)
    {
        displaystats();
        counter = 0;
    } // end if
};
