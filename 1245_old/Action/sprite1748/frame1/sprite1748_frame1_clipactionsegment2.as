﻿// Action script...

// [onClipEvent of sprite 1746 in frame 1]
onClipEvent (load)
{
    function changeteam(newnumber)
    {
        if (_root.teamdeathmatch == true)
        {
            desiredteamtochangeto = newnumber;
        }
        else
        {
            desiredteamtochangeto = newnumber.substr(0, 4);
        } // end else if
        if (!isNaN(desiredteamtochangeto) || desiredteamtochangeto == "N/A")
        {
            datatosend = "TC`" + _root.playershipstatus[3][0] + "`TM`" + desiredteamtochangeto + "~";
            _root.mysocket.send(datatosend);
            _root.gamechatinfo[1].splice(0, 0, 0);
            _root.gamechatinfo[1][0] = new Array();
            _root.gamechatinfo[1][0][0] = " Team Changing to : " + _root.teambases[desiredteamtochangeto][0].substr(2);
            _root.gamechatinfo[1][0][1] = _root.systemchattextcolor;
            if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
            {
                _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
            } // end if
            _root.refreshchatdisplay = true;
            if (_root.teamdeathmatch == true)
            {
                _parent.displaystats();
            } // end if
        } // end if
    } // End of the function
    if (_root.playershipstatus[5][2] == "N/A")
    {
        _root.sendnewdmgame();
        textinfo = "GO ON TEAM";
    }
    else
    {
        textinfo = "CHANGE TEAM";
    } // end else if
}
