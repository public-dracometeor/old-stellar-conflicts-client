﻿// Action script...

// [onClipEvent of sprite 1746 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    if (textinfo == "GO ON TEAM")
    {
        _parent.sendnewgame();
        if (_parent.teamsno[0] == _parent.teamsno[1])
        {
            teamno = Math.round(Math.random() * (_root.teambases.length - 1));
        }
        else if (_parent.teamsno[0] < _parent.teamsno[1])
        {
            teamno = 0;
        }
        else
        {
            teamno = 1;
        } // end else if
        changeteam(teamno);
    }
    else if (textinfo == "CHANGE TEAM")
    {
        teamno = -1;
        if (_parent.teamsno[0] < _parent.teamsno[1])
        {
            teamno = 0;
        }
        else if (_parent.teamsno[0] > _parent.teamsno[1])
        {
            teamno = 1;
        } // end else if
        if (teamno != _root.playershipstatus[5][2] & teamno > -1)
        {
            changeteam(teamno);
        }
        else
        {
            _parent.attachMovie("shipwarningbox", "shipwarningbox", 1555);
            _parent.shipwarningbox.information = "Would Uneven Teams";
        } // end else if
    } // end else if
}
