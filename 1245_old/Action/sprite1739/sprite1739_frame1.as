﻿// Action script...

// [onClipEvent of sprite 1586 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    _parent.gotoAndStop("basdefences");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    function resetsectorbackground()
    {
        for (i = 0; i < _root.sectormapitems.length; i++)
        {
            _root.sectormapitems[i][4] = "OFF";
        } // end of for
        for (i = 0; i < _root.playersquadbases.length; i++)
        {
            _root.playersquadbases[i][10] = false;
        } // end of for
    } // End of the function
    this.label = "EXIT";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    resetsectorbackground();
    _root.gotoAndStop("maingameplaying");
}

// [onClipEvent of sprite 1589 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    _parent.gotoAndStop("squadmanagement");
}

// [onClipEvent of sprite 1593 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    _parent.gotoAndStop("baseupgrades");
}

// [onClipEvent of sprite 1596 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    _parent.gotoAndStop("buycapitalship");
}

// [Action in Frame 1]
_root.func_checkfortoomuchaddedandset();
welcomemsg = "Welcome to Squad Base - " + _root.playershipstatus[5][10];
information = "PI`CLEAR`~";
_root.mysocket.send(information);
_root.aishipshosted = new Array();
_root.attachMovie("onlineplayerslist", "onlineplayerslist", 99982);
setProperty("_root.onlineplayerslist", _x, 0);
setProperty("_root.onlineplayerslist", _y, 0);
_root.playershipstatus[5][15] = "";
clearInterval(_root.specialstimers[1]);
clearInterval(_root.specialstimers[2]);
stop ();
