﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CANCEL";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent._visible = false;
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    usertochange = _parent.changeleadnameto;
    usertochange = usertochange.toUpperCase();
    if (usertochange == "N/A")
    {
        usertochange = "";
    } // end if
    squadmembers = _parent._parent.squadmembers;
    squadleaders = new Array();
    squadleaders[0] = _parent._parent.creatorname;
    squadleaders[1] = _parent._parent.secondname;
    squadleaders[2] = _parent._parent.thirdname;
    abletochangeuser = true;
    for (i = 0; i < squadleaders.length; i++)
    {
        if (usertochange == squadleaders[i])
        {
            abletochangeuser = false;
            _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
            _parent.hardwarewarningbox.information = "Already Leader!";
        } // end if
    } // end of for
    if (usertochange == "")
    {
        abletochangeuser = true;
    }
    else
    {
        isuseronsquad = false;
        for (i = 0; i < squadmembers.length; i++)
        {
            if (usertochange == squadmembers[i])
            {
                isuseronsquad = true;
                break;
            } // end if
        } // end of for
        if (isuseronsquad == false)
        {
            abletochangeuser = false;
            _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
            _parent.hardwarewarningbox.information = "Not In Squad!";
        } // end if
    } // end else if
    if (abletochangeuser == true && leaderposition == "SECOND")
    {
        _parent.changeleadnameto = " CHANGING";
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadscommand.php?mode=secondcomm&name=" + usertochange + "&sname=" + _root.playershipstatus[5][10] + "&spass=" + _root.playershipstatus[5][13]);
        newsquadVars.onLoad = function (success)
        {
            loadedinfo = String(newsquadVars);
            if (loadedinfo == "success")
            {
                _parent.changeleadnameto = " CHANGED";
                _parent._parent.secondname = usertochange;
                if (usertochange == "")
                {
                    _parent._parent.secondname = "NONE";
                } // end if
                _parent._visible = false;
            }
            else
            {
                _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
                _parent.hardwarewarningbox.information = "Could Not kick!";
            } // end else if
        };
    } // end if
    if (abletochangeuser == true && leaderposition == "THIRD")
    {
        _parent.changeleadnameto = " CHANGING";
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadscommand.php?mode=thirdcomm&name=" + usertochange + "&sname=" + _root.playershipstatus[5][10] + "&spass=" + _root.playershipstatus[5][13]);
        newsquadVars.onLoad = function (success)
        {
            loadedinfo = String(newsquadVars);
            if (loadedinfo == "success")
            {
                _parent.changeleadnameto = " CHANGED";
                _parent._parent.thirdname = usertochange;
                if (usertochange == "")
                {
                    _parent._parent.thirdname = "NONE";
                } // end if
                _parent._visible = false;
            }
            else
            {
                _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
                _parent.hardwarewarningbox.information = "Could Not kick!";
            } // end else if
        };
    } // end if
}
