﻿// Action script...

// [Action in Frame 1]
function func_takeonothershots(otherplayerdamage)
{
    currentshieldstr = currentshieldstr - otherplayerdamage;
    if (currentshieldstr < 0)
    {
        totalstructdamage = totalstructdamage - currentshieldstr;
        structuredisp = structuredisp + currentshieldstr;
        currentshieldstr = 0;
    } // end if
    this.shieldimage.play();
    this.shieldimage._alpha = currentshieldstr / (maxshield * 0.750000) * 100;
} // End of the function
function playershittest()
{
    didplayerhitbase = false;
    numberofplayershots = _root.playershotsfired.length;
    if (numberofplayershots > 0)
    {
        halfshipswidth = this._width / 2;
        halfshipsheight = this._height / 2;
        this.relativex = this._x - _root.shipcoordinatex;
        this.relativey = this._y - _root.shipcoordinatey;
    } // end if
    for (i = 0; i < numberofplayershots; i++)
    {
        bulletshotid = _root.playershotsfired[i][0];
        bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
        bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
        if (bulletsx != null)
        {
            if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
            {
                didplayerhitbase = true;
                if (isbasehostile == false)
                {
                    --hitstillhostile;
                    if (hitstillhostile <= 0)
                    {
                        isbasehostile = true;
                        setupturrets();
                        message = "Friendly or not, You hit me enough times!!";
                        _root.func_messangercom(message, "SQUADBASE" + squadbasetype, "BEGIN");
                    } // end if
                } // end if
                if (_root.playershotsfired[i][13] == "GUNS")
                {
                    currentshieldstr = currentshieldstr - _root.guntype[_root.playershotsfired[i][6]][4];
                    _root.gunfireinformation = _root.gunfireinformation + ("GH`SQ`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[i][0] + "`" + baseidname + "~");
                }
                else if (_root.playershotsfired[i][13] == "MISSILE")
                {
                    currentshieldstr = currentshieldstr - _root.missile[_root.playershotsfired[i][6]][4];
                    _root.gunfireinformation = _root.gunfireinformation + ("MH`SQ`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[i][0] + "`" + baseidname + "~");
                } // end else if
                if (currentshieldstr < 0)
                {
                    totalstructdamage = totalstructdamage - currentshieldstr;
                    structuredisp = structuredisp + currentshieldstr;
                    currentshieldstr = 0;
                } // end if
                removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                _root.playershotsfired[i][5] = 0;
                this.shieldimage.play();
                this.shieldimage._alpha = currentshieldstr / (maxshield * 0.750000) * 100;
            } // end if
        } // end if
    } // end of for
    if (structuredisp < 0)
    {
        if (didplayerhitbase == true)
        {
            laststructupdate = strucupdateint + getTimer();
            sendstructuredamage();
        } // end if
    } // end if
    func_settargetdisplayinfo(currentshieldstr, structuredisp);
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function
function fireamissile()
{
    this.missilexfire = this._x;
    this.missileyfire = this._y;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    playeranglefromship = 360 - Math.atan2(this.missilexfire - playersxcoord, this.missileyfire - playersycoord) / 0.017453;
    if (playeranglefromship > 360)
    {
        playeranglefromship = playeranglefromship - 360;
    } // end if
    if (playeranglefromship < 0)
    {
        playeranglefromship = playeranglefromship + 360;
    } // end if
    playeranglefromship = Math.round(playeranglefromship);
    ++_root.currentothermissileshot;
    if (_root.currentothermissileshot >= 1000)
    {
        _root.currentothermissileshot = 1;
    } // end if
    if (_root.othermissilefire.length < 1)
    {
        _root.othermissilefire = new Array();
    } // end if
    lastvar = _root.othermissilefire.length;
    _root.othermissilefire[lastvar] = new Array();
    _root.othermissilefire[lastvar][0] = 999999;
    _root.othermissilefire[lastvar][1] = this.missilexfire;
    _root.othermissilefire[lastvar][2] = this.missileyfire;
    velocity = _root.missile[missileshottype][0] * 1.500000;
    _root.othermissilefire[lastvar][6] = playeranglefromship;
    _root.othermissilefire[lastvar][7] = missileshottype;
    _root.othermissilefire[lastvar][8] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][9] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][10] = "other";
    _root.othermissilefire[lastvar][5] = _root.curenttime + _root.missile[missileshottype][2];
    _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "fire", "othermissilefire" + _root.othermissilefire[lastvar][8], 999000 + _root.othermissilefire[lastvar][8]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _rotation, _root.othermissilefire[lastvar][6]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _x, _root.othermissilefire[lastvar][1] - _root.shipcoordinatex);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _y, _root.othermissilefire[lastvar][2] - _root.shipcoordinatey);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xposition", _root.othermissilefire[lastvar][1]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".yposition", _root.othermissilefire[lastvar][2]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".missiletype", missileshottype);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".velocity", velocity);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".seeking", "HOST");
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".shooterid", _root.othermissilefire[lastvar][0]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".globalid", _root.othermissilefire[lastvar][9]);
    _root.othermissilefire[lastvar][11] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._width / 2;
    _root.othermissilefire[lastvar][12] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._height / 2;
    _root.othermissilefire[lastvar][13] = "SEEKING";
} // End of the function
stop ();

function chargeshields(timechange)
{
    if (currentshieldstr < maxshield)
    {
        currentshieldstr = currentshieldstr + shieldrechargerate * (timechange / 1000);
        if (basetakendamageyet == false)
        {
            basetakendamageyet = true;
            chattosend = "SQUAD`CH`" + baseidname + "`WM`" + "FILLER" + "~";
            _root.mysocket.send(chattosend);
        } // end if
    } // end if
    if (currentshieldstr > maxshield)
    {
        currentshieldstr = maxshield;
    } // end if
} // End of the function
function loadbasevars(loadedvars)
{
    newinfo = loadedvars.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentthread = newinfo[i].split("`");
        turrets = new Array();
        turrets[0] = Number(currentthread[0]);
        turrets[1] = Number(currentthread[1]);
        turrets[2] = Number(currentthread[2]);
        turrets[3] = Number(currentthread[3]);
        turrets[4] = Number(currentthread[4]);
        if (baseiddisp.substr(0, 3) != "*AI")
        {
            for (ii = 0; ii < turrets.length; ii++)
            {
                if (isNaN(turrets[ii]))
                {
                    continue;
                } // end if
                basebountyworth = basebountyworth + _root.guntype[turrets[ii]][5] / 4;
            } // end of for
        } // end if
        basebountyworth = Math.floor(basebountyworth) + 1000;
        baseiddisp = squadid + " (" + basebountyworth + ")";
        shieldgenerator = Number(currentthread[5]);
        maxshield = _root.shieldgenerators[shieldgenerator][0] * _root.squadbaseinfo[squadbasetype][1][1];
        shieldrechargerate = _root.shieldgenerators[shieldgenerator][1] * _root.squadbaseinfo[squadbasetype][1][2];
        currentshieldstr = maxshield;
        totalstructure = Number(currentthread[6]);
        structuredisp = Number(totalstructure);
        missileshotsleft = Number(currentthread[7]);
        missileshottype = 9;
        hostilestatus = String(currentthread[8]);
        if (hostilestatus == "HOSTILE" || hostilestatus == "ENEMY")
        {
            message = "You Are Designated as Hostile Ship, Do not Approach This Base!";
            _root.func_messangercom(message, "SQUADBASE" + squadbasetype, "BEGIN");
            isbasehostile = true;
            hitstillhostile = 0;
            continue;
        } // end if
        isbasehostile = false;
        hitstillhostile = 4;
        message = "You Are Designated as Neutral or Friendly Ship, You May Approach This Base, But Do Not Attack It!";
        _root.func_messangercom(message, "SQUADBASE" + squadbasetype, "BEGIN");
    } // end of for
} // End of the function
function setupturrets()
{
    if (!isNaN(turrets[0]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase0", 1);
        this.turrettypeaibase0.shottype = turrets[0];
        this.turrettypeaibase0._x = -32;
        this.turrettypeaibase0._y = -32;
    } // end if
    if (!isNaN(turrets[1]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase1", 2);
        this.turrettypeaibase1.shottype = turrets[1];
        this.turrettypeaibase1._x = 32;
        this.turrettypeaibase1._y = -32;
    } // end if
    if (!isNaN(turrets[2]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase2", 3);
        this.turrettypeaibase2.shottype = turrets[2];
        this.turrettypeaibase2._x = -32;
        this.turrettypeaibase2._y = 32;
    } // end if
    if (!isNaN(turrets[3]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase3", 4);
        this.turrettypeaibase3.shottype = turrets[3];
        this.turrettypeaibase3._x = 32;
        this.turrettypeaibase3._y = 32;
    } // end if
} // End of the function
stop ();

function updatefiredmissiles()
{
    newmissileammoVars.load(_root.pathtoaccounts + "squadbases.php?mode=missileshot&baseid=" + baseidname + "&shots=" + missilesfired);
    newmissileammoVars.onLoad = function (success)
    {
        loadedvars = String(newmissileammoVars);
        missileshotsleft = Number(loadedvars);
        missilesfired = 0;
    };
} // End of the function
function sendstructuredamage()
{
    newbaselifeVars.load(_root.pathtoaccounts + "squadbases.php?mode=damagebase&struct=" + totalstructdamage + "&baseid=" + baseidname);
    newbaselifeVars.onLoad = function (success)
    {
        loadedvars = String(newbaselifeVars);
        if (loadedvars == "destroyed")
        {
            basedestroyed = true;
        }
        else if (loadedvars == "killedit")
        {
            playerkilledbase = true;
            if (_root.playershipstatus[5][10] == squadid)
            {
                basebountyworth = 0;
            } // end if
            datatosend = "NEWS`SQUADBD`" + baseidname + "`" + _root.playershipstatus[3][0] + "`" + String(basebountyworth) + "`" + Math.round(basebountyworth * _root.playershipstatus[5][6]) + "~";
            _root.mysocket.send(datatosend);
        }
        else
        {
            structuredisp = Number(loadedvars);
        } // end else if
    };
    totalstructdamage = 0;
} // End of the function
function func_settargetdisplayinfo(shield, struct)
{
    for (location = 0; location < _root.sectoritemsfortarget.length; location++)
    {
        if (squadid == _root.sectoritemsfortarget[location][0])
        {
            _root.sectoritemsfortarget[location][5] = shield;
            _root.sectoritemsfortarget[location][6] = struct;
        } // end if
    } // end of for
} // End of the function
newbaselifeVars = new XML();
structuredisp = "";
rangeformissile = 1500;
squadbasetype = 0;
otherplayerdamage = 0;
baseiddisp = squadid;
newmissileammoVars = new XML();
this.newbaseVars = new XML();
this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=loadbattle&baseid=" + baseidname + "&psquad=" + _root.playershipstatus[5][10]);
this.newbaseVars.onLoad = function (success)
{
    loadedvars = String(newbaseVars);
    loadbasevars(loadedvars);
    if (isbasehostile == true)
    {
        setupturrets();
    } // end if
};
basedestroyed = false;
basetakendamageyet = false;
basebountyworth = 0;
if (baseiddisp.substr(0, 3) == "*AI")
{
    basebountyworth = 65000;
} // end if
baseiddisp = squadid + " (" + basebountyworth + ")";
strucupdateint = 20000;
laststructupdate = getTimer() + strucupdateint;
hittestdelay = 550;
lasthittest = getTimer() + hittestdelay;
misileupdateint = 20000;
lastmisileupdate = getTimer() + misileupdateint;
missileshotsleft = 0;
missileshottype = 9;
missilesfired = 0;
missilfireint = _root.missile[missileshottype][3] / 1.700000;
timetillnextmissile = missilfireint + getTimer();
this.onEnterFrame = function ()
{
    if (basedestroyed == true)
    {
        this.onEnterFrame = function ()
        {
        };
        this.play();
    } // end if
    currenttime = getTimer();
    if (lasthittest < currenttime)
    {
        timechange = currenttime - (lasthittest - hittestdelay);
        chargeshields(timechange);
        lasthittest = currenttime + hittestdelay;
        playershittest();
    } // end if
    if (laststructupdate < currenttime)
    {
        sendstructuredamage();
        laststructupdate = strucupdateint + currenttime;
    } // end if
    if (lastbasehostile != isbasehostile)
    {
        if (isbasehostile)
        {
            setupturrets();
        } // end if
        lastbasehostile = isbasehostile;
    } // end if
    if (isbasehostile)
    {
        if (lastmisileupdate < currenttime)
        {
            lastmisileupdate = misileupdateint + currenttime;
            updatefiredmissiles();
        } // end if
        if (timetillnextmissile < currenttime)
        {
            xdist = this._x - _root.shipcoordinatex;
            ydist = this._y - _root.shipcoordinatey;
            rangetoplayer = Math.sqrt(xdist * xdist + ydist * ydist);
            if (missileshotsleft > 0 && _root.playershipstatus[2][5] > 0 && rangetoplayer < rangeformissile)
            {
                --missileshotsleft;
                ++missilesfired;
                fireamissile();
                if (_root.soundvolume != "off")
                {
                    _root.missilefiresound.start();
                } // end if
                timetillnextmissile = missilfireint + currenttime;
            } // end if
        } // end if
    } // end if
};
stop ();
