﻿// Action script...

function updatefiredmissiles()
{
    newmissileammoVars.load(_root.pathtoaccounts + "squadbases.php?mode=missileshot&baseid=" + baseidname + "&shots=" + missilesfired);
    newmissileammoVars.onLoad = function (success)
    {
        loadedvars = String(newmissileammoVars);
        missileshotsleft = Number(loadedvars);
        missilesfired = 0;
    };
} // End of the function
function sendstructuredamage()
{
    newbaselifeVars.load(_root.pathtoaccounts + "squadbases.php?mode=damagebase&struct=" + totalstructdamage + "&baseid=" + baseidname);
    newbaselifeVars.onLoad = function (success)
    {
        loadedvars = String(newbaselifeVars);
        if (loadedvars == "destroyed")
        {
            basedestroyed = true;
        }
        else if (loadedvars == "killedit")
        {
            playerkilledbase = true;
            if (_root.playershipstatus[5][10] == squadid)
            {
                basebountyworth = 0;
            } // end if
            datatosend = "NEWS`SQUADBD`" + baseidname + "`" + _root.playershipstatus[3][0] + "`" + String(basebountyworth) + "`" + Math.round(basebountyworth * _root.playershipstatus[5][6]) + "~";
            _root.mysocket.send(datatosend);
        }
        else
        {
            structuredisp = Number(loadedvars);
        } // end else if
    };
    totalstructdamage = 0;
} // End of the function
function func_settargetdisplayinfo(shield, struct)
{
    for (location = 0; location < _root.sectoritemsfortarget.length; location++)
    {
        if (squadid == _root.sectoritemsfortarget[location][0])
        {
            _root.sectoritemsfortarget[location][5] = shield;
            _root.sectoritemsfortarget[location][6] = struct;
        } // end if
    } // end of for
} // End of the function
newbaselifeVars = new XML();
structuredisp = "";
rangeformissile = 1500;
squadbasetype = 0;
otherplayerdamage = 0;
baseiddisp = squadid;
newmissileammoVars = new XML();
this.newbaseVars = new XML();
this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=loadbattle&baseid=" + baseidname + "&psquad=" + _root.playershipstatus[5][10]);
this.newbaseVars.onLoad = function (success)
{
    loadedvars = String(newbaseVars);
    loadbasevars(loadedvars);
    if (isbasehostile == true)
    {
        setupturrets();
    } // end if
};
basedestroyed = false;
basetakendamageyet = false;
basebountyworth = 0;
if (baseiddisp.substr(0, 3) == "*AI")
{
    basebountyworth = 65000;
} // end if
baseiddisp = squadid + " (" + basebountyworth + ")";
strucupdateint = 20000;
laststructupdate = getTimer() + strucupdateint;
hittestdelay = 550;
lasthittest = getTimer() + hittestdelay;
misileupdateint = 20000;
lastmisileupdate = getTimer() + misileupdateint;
missileshotsleft = 0;
missileshottype = 9;
missilesfired = 0;
missilfireint = _root.missile[missileshottype][3] / 1.700000;
timetillnextmissile = missilfireint + getTimer();
this.onEnterFrame = function ()
{
    if (basedestroyed == true)
    {
        this.onEnterFrame = function ()
        {
        };
        this.play();
    } // end if
    currenttime = getTimer();
    if (lasthittest < currenttime)
    {
        timechange = currenttime - (lasthittest - hittestdelay);
        chargeshields(timechange);
        lasthittest = currenttime + hittestdelay;
        playershittest();
    } // end if
    if (laststructupdate < currenttime)
    {
        sendstructuredamage();
        laststructupdate = strucupdateint + currenttime;
    } // end if
    if (lastbasehostile != isbasehostile)
    {
        if (isbasehostile)
        {
            setupturrets();
        } // end if
        lastbasehostile = isbasehostile;
    } // end if
    if (isbasehostile)
    {
        if (lastmisileupdate < currenttime)
        {
            lastmisileupdate = misileupdateint + currenttime;
            updatefiredmissiles();
        } // end if
        if (timetillnextmissile < currenttime)
        {
            xdist = this._x - _root.shipcoordinatex;
            ydist = this._y - _root.shipcoordinatey;
            rangetoplayer = Math.sqrt(xdist * xdist + ydist * ydist);
            if (missileshotsleft > 0 && _root.playershipstatus[2][5] > 0 && rangetoplayer < rangeformissile)
            {
                --missileshotsleft;
                ++missilesfired;
                fireamissile();
                if (_root.soundvolume != "off")
                {
                    _root.missilefiresound.start();
                } // end if
                timetillnextmissile = missilfireint + currenttime;
            } // end if
        } // end if
    } // end if
};
stop ();
