﻿// Action script...

// [onClipEvent of sprite 1566 in frame 1]
on (release)
{
    function resetsectorbackground()
    {
        for (i = 0; i < _root.sectormapitems.length; i++)
        {
            _root.sectormapitems[i][4] = "OFF";
        } // end of for
        for (i = 0; i < _root.playersquadbases.length; i++)
        {
            _root.playersquadbases[i][10] = false;
        } // end of for
    } // End of the function
    function initializetheship()
    {
        cshiptype = _root.playershipstatus[5][0];
        _root.playershipstatus[5][4] = "alive";
        _root.playershiprotation = _root.shiptype[cshiptype][3][2];
        _root.playershipmaxvelocity = _root.shiptype[cshiptype][3][1];
        _root.playershipacceleration = _root.shiptype[cshiptype][3][0];
        _root.playershipvelocity = 0;
        starbaseexitposition();
        for (currenthardpoint = 0; currenthardpoint < _root.playershipstatus[0].length; currenthardpoint++)
        {
            currentguntype = _root.playershipstatus[0][currenthardpoint][0];
            _root.playershipstatus[0][currenthardpoint][1] = 0;
            _root.playershipstatus[0][currenthardpoint][2] = _root.shiptype[_root.playershipstatus[5][0]][2][currenthardpoint][0];
            _root.playershipstatus[0][currenthardpoint][3] = _root.shiptype[_root.playershipstatus[5][0]][2][currenthardpoint][1];
            _root.playershipstatus[0][currenthardpoint][4] = "ON";
            _root.playershipstatus[0][currenthardpoint][5] = _root.guntype[currentguntype][3];
            _root.playershipstatus[0][currenthardpoint][6] = _root.guntype[currentguntype][2] * 1000;
            _root.playershipstatus[0][currenthardpoint][7] = Math.round(_root.guntype[currentguntype][0]);
            _root.playershipstatus[0][currenthardpoint][8] = _root.guntype[currentguntype][1] * 1000;
        } // end of for
        _root.playershipstatus[1][1] = _root.energycapacitors[_root.playershipstatus[1][5]][0];
        _root.playershipstatus[2][1] = _root.shieldgenerators[_root.playershipstatus[2][0]][0];
        _root.playershipstatus[2][5] = _root.shiptype[cshiptype][3][3];
        _root.playershipstatus[2][6] = 0;
        _root.playershipstatus[2][2] = "FULL";
    } // End of the function
    function starbaseexitposition()
    {
        if (_root.teamdeathmatch == true)
        {
            dockingrange = Math.round(_root.teambases[_root.playershipstatus[5][2]][4] / 5 * 4);
            _root.shipcoordinatex = _root.teambases[_root.playershipstatus[5][2]][1] + dockingrange * 2 * Math.random() - dockingrange;
            _root.shipcoordinatey = _root.teambases[_root.playershipstatus[5][2]][2] + dockingrange * 2 * Math.random() - dockingrange;
        }
        else
        {
            jjj = 0;
            databeenupdated = false;
            while (jjj < _root.starbaselocation.length && databeenupdated == false)
            {
                if (_root.starbaselocation[jjj][0] == _root.playershipstatus[4][0])
                {
                    databeenupdated = true;
                    dockingrange = Math.round(_root.starbaselocation[jjj][4] / 5 * 4);
                    _root.shipcoordinatex = _root.starbaselocation[jjj][1] + dockingrange * 2 * Math.random() - dockingrange;
                    _root.shipcoordinatey = _root.starbaselocation[jjj][2] + dockingrange * 2 * Math.random() - dockingrange;
                } // end if
                ++jjj;
            } // end while
            if (_root.shipcoordinatex < 0 || _root.shipcoordinatey < 0)
            {
                _root.shipcoordinatex = 0;
                _root.shipcoordinatey = 0;
            } // end if
        } // end else if
    } // End of the function
    function processbty()
    {
        totalshipworth = _root.shiptype[_root.playershipstatus[5][0]][1];
        for (currenthardpoint = 0; currenthardpoint < _root.playershipstatus[0].length; currenthardpoint++)
        {
            currentguntype = _root.playershipstatus[0][currenthardpoint][0];
            if (!isNaN(currentguntype))
            {
                totalshipworth = totalshipworth + _root.guntype[currentguntype][5];
            } // end if
        } // end of for
        totalshipworth = totalshipworth + _root.shieldgenerators[_root.playershipstatus[2][0]][5];
        totalshipworth = totalshipworth + _root.energycapacitors[_root.playershipstatus[1][5]][2];
        totalshipworth = totalshipworth + _root.energygenerators[_root.playershipstatus[1][0]][2];
        _root.playershipstatus[5][3] = Math.round(totalshipworth / _root.playersworthtobtymodifire);
        if (_root.playershipstatus[5][8] < 0)
        {
            _root.playershipstatus[5][8] = 0;
        } // end if
        if (isNaN(_root.playershipstatus[5][3]))
        {
            _root.playershipstatus[5][3] = Number(0);
        } // end if
        if (isNaN(_root.playershipstatus[5][8]))
        {
            _root.playershipstatus[5][8] = Number(0);
        } // end if
        datatosend = "TC`" + _root.playershipstatus[3][0] + "`BC`" + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8])) + "~";
        _root.mysocket.send(datatosend);
        _root.playertargetrange = Math.round(_root.playershipstatus[5][3] / 4);
        if (_root.playertargetrange > 200)
        {
            _root.playertargetrange = 200;
        } // end if
    } // End of the function
    _parent.func_eriestop();
    initializetheship();
    _root.playerjustloggedin = false;
    resetsectorbackground();
    _root.playersexitdocktimewait = 5500;
    timetillplayercanexit = _root.playersdockedtime + _root.playersexitdocktimewait;
    if (_root.teamdeathmatch == true)
    {
        if (_root.squadwarinfo[0] == true && _root.squadwarinfo[2] != true)
        {
            _parent.attachMovie("shipwarningbox", "shipwarningbox", 1555);
            _parent.shipwarningbox.information = "Squad War is waiting to start";
        }
        else if (_root.playershipstatus[5][2] != Number(_root.playershipstatus[5][2]) && _root.squadwarinfo[2] == true)
        {
            _parent.attachMovie("shipwarningbox", "shipwarningbox", 1555);
            _parent.shipwarningbox.information = "You are not on one of the squads";
        }
        else if (_root.playershipstatus[5][2] != Number(_root.playershipstatus[5][2]))
        {
            _parent.attachMovie("shipwarningbox", "shipwarningbox", 1555);
            _parent.shipwarningbox.information = "Need to Choose / Select a Team First";
        }
        else if (timetillplayercanexit < getTimer())
        {
            processbty();
            removeMovieClip ("_root.starbasehardwarebutton");
            removeMovieClip ("_root.purchaseshipbutton");
            removeMovieClip ("_root.purchasetradegoodsbutton");
            _root.gotoAndStop("maingameplaying");
            removeMovieClip ("_root.exitstarbasebutton");
        }
        else
        {
            _parent.attachMovie("shipwarningbox", "shipwarningbox", 1555);
            timeleft = Math.ceil((timetillplayercanexit - getTimer()) / 1000);
            if (timeleft > 5)
            {
                timeleft = 10;
            } // end if
            _parent.shipwarningbox.information = "Need to wait " + timeleft + " seconds";
        } // end else if
    } // end else if
}
