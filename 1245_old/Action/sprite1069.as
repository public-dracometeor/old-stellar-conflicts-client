﻿// Action script...

// [onClipEvent of sprite 938 in frame 1]
on (release)
{
    _parent.moveitemsdisplayed = -1;
}

// [onClipEvent of sprite 938 in frame 1]
on (release)
{
    _parent.moveitemsdisplayed = 1;
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "EXIT";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    if (_root.teamdeathmatch == true)
    {
        _parent._parent.gotoAndStop("baseintro");
    }
    else
    {
        _parent._parent.gotoAndStop("squadbasemenu");
    } // end else if
}

// [Action in Frame 1]
function drawtheshipsforsale(currentleftmostitem)
{
    if (currentleftmostitem > 0)
    {
        islefton = true;
    }
    else
    {
        islefton = false;
    } // end else if
    if (currentleftmostitem < shipsavail.length - maxitemstostoshow)
    {
        isrighton = true;
    }
    else
    {
        isrighton = false;
    } // end else if
    drawarrows(islefton, isrighton);
    for (i = 0; i < maxitemstostoshow && i < _root.shiptype.length; i++)
    {
        set(this + ".newshipitembox" + i + ".itemname", _root.shiptype[shipsavail[currentleftmostitem + i]][0]);
        set(this + ".newshipitembox" + i + ".itemtype", "shiptype" + shipsavail[currentleftmostitem + i]);
        set(this + ".newshipitembox" + i + ".itemcost", "Cost:" + _root.shiptype[shipsavail[currentleftmostitem + i]][1]);
        if (shipsavail[currentleftmostitem + i] == _root.playershipstatus[5][0])
        {
            set(this + ".newshipitembox" + i + ".itemcost", "OWNED");
        } // end if
        set(this + ".newshipitembox" + i + ".itemspecs", "Hard Points: " + _root.shiptype[shipsavail[currentleftmostitem + i]][2].length + " \r" + "Rotation Degrees / Second \r" + "  When Stationary: " + _root.shiptype[shipsavail[currentleftmostitem + i]][3][2] + " \r" + "  Loss at Full Speed:" + "\r" + "   %" + _root.shiptype[shipsavail[currentleftmostitem + i]][3][5] * 100 + " \r" + "Max Speed: " + _root.shiptype[shipsavail[currentleftmostitem + i]][3][1] + "/ sec \r" + "Aft. Speed: " + _root.shiptype[shipsavail[currentleftmostitem + i]][3][6] + "/ sec \r" + "Acceleration: " + _root.shiptype[shipsavail[currentleftmostitem + i]][3][0] + "/ sec \r" + "Max Structure: " + _root.shiptype[shipsavail[currentleftmostitem + i]][3][3] + " \r" + "Max Cargo: " + _root.shiptype[shipsavail[currentleftmostitem + i]][4] + " \r" + "Turrets: " + (0 + _root.shiptype[shipsavail[currentleftmostitem + i]][5].length) + " \r" + "Top Weapon: " + _root.guntype[_root.shiptype[shipsavail[currentleftmostitem + i]][6][3]][6] + " \r" + "Max Shield: " + _root.shieldgenerators[_root.shiptype[shipsavail[currentleftmostitem + i]][6][0]][0] + " \r" + "Max Energy: " + _root.energycapacitors[_root.shiptype[shipsavail[currentleftmostitem + i]][6][1]][0] + " \r" + "Max Charge Rate: " + _root.energygenerators[_root.shiptype[shipsavail[currentleftmostitem + i]][6][2]][0]);
    } // end of for
} // End of the function
function drawarrows(islefton, isrighton)
{
    this.hardwareselectleft._alpha = 60;
    if (islefton == true)
    {
        this.hardwareselectleft._alpha = 100;
    } // end if
    this.hardwareselectright._alpha = 60;
    if (isrighton == true)
    {
        this.hardwareselectright._alpha = 100;
    } // end if
} // End of the function
function initializetheship()
{
    cshiptype = _root.playershipstatus[5][0];
    _root.playershipstatus[5][4] = "alive";
    _root.playershiprotation = _root.shiptype[cshiptype][3][2];
    _root.playershipmaxvelocity = _root.shiptype[cshiptype][3][1];
    _root.playershipacceleration = _root.shiptype[cshiptype][3][0];
    _root.playershipvelocity = 0;
    for (currenthardpoint = 0; currenthardpoint < _root.playershipstatus[0].length; currenthardpoint++)
    {
        currentguntype = _root.playershipstatus[0][currenthardpoint][0];
        _root.playershipstatus[0][currenthardpoint][1] = 0;
        _root.playershipstatus[0][currenthardpoint][2] = _root.shiptype[_root.playershipstatus[5][0]][2][currenthardpoint][0];
        _root.playershipstatus[0][currenthardpoint][3] = _root.shiptype[_root.playershipstatus[5][0]][2][currenthardpoint][1];
        _root.playershipstatus[0][currenthardpoint][4] = "ON";
        _root.playershipstatus[0][currenthardpoint][5] = _root.guntype[currentguntype][3];
        _root.playershipstatus[0][currenthardpoint][6] = _root.guntype[currentguntype][2] * 1000;
        _root.playershipstatus[0][currenthardpoint][7] = Math.round(_root.guntype[currentguntype][0]);
        _root.playershipstatus[0][currenthardpoint][8] = _root.guntype[currentguntype][1] * 1000;
    } // end of for
    _root.playershipstatus[1][1] = _root.energycapacitors[_root.playershipstatus[1][5]][0];
    _root.playershipstatus[2][1] = _root.shieldgenerators[_root.playershipstatus[2][0]][0];
    _root.playershipstatus[2][5] = _root.shiptype[cshiptype][3][3];
    _root.playershipstatus[2][6] = 0;
    _root.playershipstatus[2][2] = "FULL";
} // End of the function
function processbty()
{
    totalshipworth = _root.shiptype[_root.playershipstatus[5][0]][1];
    for (currenthardpoint = 0; currenthardpoint < _root.playershipstatus[0].length; currenthardpoint++)
    {
        currentguntype = _root.playershipstatus[0][currenthardpoint][0];
        if (!isNaN(currentguntype))
        {
            totalshipworth = totalshipworth + _root.guntype[currentguntype][5];
        } // end if
    } // end of for
    totalshipworth = totalshipworth + _root.shieldgenerators[_root.playershipstatus[2][0]][5];
    totalshipworth = totalshipworth + _root.energycapacitors[_root.playershipstatus[1][5]][2];
    totalshipworth = totalshipworth + _root.energygenerators[_root.playershipstatus[1][0]][2];
    _root.playershipstatus[5][3] = Math.round(totalshipworth / _root.playersworthtobtymodifire);
    if (_root.playershipstatus[5][8] < 0)
    {
        _root.playershipstatus[5][8] = 0;
    } // end if
    if (isNaN(_root.playershipstatus[5][3]))
    {
        _root.playershipstatus[5][3] = Number(0);
    } // end if
    if (isNaN(_root.playershipstatus[5][8]))
    {
        _root.playershipstatus[5][8] = Number(0);
    } // end if
    datatosend = "TC`" + _root.playershipstatus[3][0] + "`BC`" + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8])) + "~";
    _root.mysocket.send(datatosend);
    _root.playertargetrange = Math.round(_root.playershipstatus[5][3] / 4);
    if (_root.playertargetrange > 200)
    {
        _root.playertargetrange = 200;
    } // end if
} // End of the function
this.newshipid = 3;
startingship = 0;
shipsavail = new Array();
for (i = 0; i < _root.shiptype.length; i++)
{
    if (_root.shiptype[i][3][10] == true)
    {
        loc = shipsavail.length;
        shipsavail[loc] = i;
    } // end if
} // end of for
if (shipsavail.length < 2)
{
    newshipitembox1._visible = false;
    hardwareselectleft._visible = false;
    hardwareselectright._visible = false;
} // end if
if (shipsavail.length < 3)
{
    newshipitembox2._visible = false;
    hardwareselectleft._visible = false;
    hardwareselectright._visible = false;
} // end if
itemselection = null;
buyingaship = false;
maxitemstostoshow = 3;
currentleftmostitem = 0;
drawtheshipsforsale(currentleftmostitem);
this.onEnterFrame = function ()
{
    function changetonewship(i)
    {
        capitalshiplvlstart = 3;
        _root.savecurrenttoextraship(_root.playerscurrentextrashipno);
        _root.playerscurrentextrashipno = "capital";
        _root.playershipstatus[5][0] = i;
        currentharpoint = 0;
        _root.playershipstatus[0] = new Array();
        while (currentharpoint < _root.shiptype[i][2].length)
        {
            _root.playershipstatus[0][currentharpoint] = new Array();
            _root.playershipstatus[0][currentharpoint][0] = capitalshiplvlstart;
            _root.playershipstatus[0][currentharpoint][1] = 0;
            _root.playershipstatus[0][currentharpoint][2] = _root.shiptype[i][2][currentharpoint][0];
            _root.playershipstatus[0][currentharpoint][3] = _root.shiptype[i][2][currentharpoint][1];
            _root.playershipstatus[0][currenthardpoint] = new Array();
            _root.playershipstatus[0][currenthardpoint][0] = capitalshiplvlstart;
            ++currentharpoint;
        } // end while
        currentturretpoint = 0;
        _root.playershipstatus[8] = new Array();
        while (currentturretpoint < _root.shiptype[i][5].length)
        {
            _root.playershipstatus[8][currentturretpoint] = new Array();
            _root.playershipstatus[8][currentturretpoint][0] = capitalshiplvlstart;
            ++currentturretpoint;
        } // end while
        _root.playershipstatus[4][1] = new Array();
        _root.playershipstatus[11][1] = new Array();
        _root.playershipstatus[2][0] = capitalshiplvlstart;
        _root.playershipstatus[1][0] = capitalshiplvlstart;
        _root.playershipstatus[1][5] = capitalshiplvlstart;
        datatosend = "TC`" + _root.playershipstatus[3][0] + "`SC`" + _root.playershipstatus[5][0] + "`" + _root.errorchecknumber + "~";
        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
        _root.mysocket.send(datatosend);
        initializetheship();
        processbty();
        _root.initializemissilebanks();
    } // End of the function
    if (moveitemsdisplayed != null)
    {
        currentleftmostitem = currentleftmostitem + moveitemsdisplayed;
        moveitemsdisplayed = null;
        if (currentleftmostitem < 0)
        {
            currentleftmostitem = 0;
        } // end if
        if (currentleftmostitem > shipsavail.length - maxitemstostoshow)
        {
            currentleftmostitem = currentleftmostitem - 1;
        } // end if
        drawtheshipsforsale(currentleftmostitem);
    } // end if
    if (itemselection != null)
    {
        shipselectiontype = itemselection.substring(8);
    } // end if
    if (itemselection != null && buyingaship != true && shipselectiontype != _root.playershipstatus[5][0])
    {
        removeMovieClip (this.shipwarningbox);
        this.attachMovie("buyingashipquestion", "buyingashipquestion", 9980);
        this.buyingashipquestion._x = 300;
        this.buyingashipquestion._y = 300;
        currentshipupgradecost = _root.shiptype[shipselectiontype][1];
        this.buyingashipquestion.question = "Buy: " + _root.shiptype[shipselectiontype][0] + " \r" + "For: " + currentshipupgradecost;
        buyingaship = true;
    } // end if
    if (this.buyingashipquestion.requestedanswer == true && buyingaship == true)
    {
        i = shipselectiontype;
        currentshipupgradecost = _root.shiptype[shipselectiontype][1];
        numberofguns = 0;
        if (_root.playershipstatus[3][1] >= currentshipupgradecost)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - currentshipupgradecost;
            buyingaship = false;
            itemselection = null;
            removeMovieClip (this.buyingashipquestion);
            changetonewship(shipselectiontype);
            drawtheshipsforsale(currentleftmostitem);
            _root.savegamescript.saveplayersgame(this);
        }
        else
        {
            this.attachMovie("shipwarningbox", "shipwarningbox", 9990);
            this.shipwarningbox.information = "You Do Not Have Enough Funds";
            this.shipwarningbox._x = 300;
            this.shipwarningbox._y = 300;
            buyingaship = false;
            itemselection = null;
            removeMovieClip (this.buyingashipquestion);
        } // end if
    } // end else if
    if (this.buyingashipquestion.requestedanswer == false && buyingaship == true)
    {
        buyingaship = false;
        removeMovieClip (this.buyingashipquestion);
        itemselection = null;
    } // end if
};
stop ();
