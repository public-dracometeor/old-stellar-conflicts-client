﻿// Action script...

// [Action in Frame 1]
missilename = _root.missile[missileselected][6];
missilecost = _root.missile[missileselected][5];
missilecostdisp = missilename + " at " + missilecost;
banknumberdisp = "Purchase Missiles in Bank: " + bankno;
noofmissiles = 0;
for (gg = 0; gg < _root.missile.length; gg++)
{
    if (_root.playershipstatus[7][bankno][4][gg] > 0)
    {
        noofmissiles = noofmissiles + _root.playershipstatus[7][bankno][4][gg];
    } // end if
} // end of for
maxmissilesforbank = _root.playershipstatus[7][bankno][5];
maxmissile = maxmissilesforbank - noofmissiles;
if (missilecost * maxmissile > _root.playershipstatus[3][1])
{
    for (i = maxmissile; i >= 0; i--)
    {
        if (missilecost * i <= _root.playershipstatus[3][1])
        {
            maxmissile = i;
            break;
        } // end if
    } // end of for
} // end if
maxpurchase = "Max: " + maxmissile;
quantity = maxmissile;
this.onEnterFrame = function ()
{
    if (quantity < 0)
    {
        quantity = 0;
    } // end if
    if (quantity != lastquantity)
    {
        quantity = Math.round(quantity);
        if (isNaN(quantity))
        {
            quantity = 0;
        } // end if
        if (quantity < 0)
        {
            quantity = 0;
        } // end if
        if (quantity > maxmissile)
        {
            quantity = maxmissile;
        } // end if
        lastquantity = quantity;
    } // end if
    costdisp = "Cost: " + missilecost * quantity;
};
stop ();
