﻿// Action script...

// [onClipEvent of sprite 1182 in frame 1]
onClipEvent (load)
{
    arrow._rotation = 0;
    _parent.range = 0;
    destination = _root.playersdestination[0];
    _parent.destination = _root.playersdestination[0];
    if (_root.playersdestination[0].substr(0, 2) == "PL")
    {
        _parent.destination = _root.playersdestination[0].substr(2);
    } // end if
    if (_parent.destination == null)
    {
        _parent.destination = "None";
    } // end if
    framestoupdate = 15;
    currentframe = 0;
    xcoord = _root.playersdestination[1];
    ycoord = _root.playersdestination[2];
    arrow._visible = false;
}

// [onClipEvent of sprite 1182 in frame 1]
onClipEvent (enterFrame)
{
    ++currentframe;
    if (currentframe > framestoupdate)
    {
        currentframe = 0;
        if (destination != _root.playersdestination[0])
        {
            destination = _root.playersdestination[0];
            _parent.destination = destination;
            if (_root.playersdestination[0].substr(0, 2) == "PL")
            {
                _parent.destination = _root.playersdestination[0].substr(2);
            } // end if
            xcoord = _root.playersdestination[1];
            ycoord = _root.playersdestination[2];
        } // end if
        if (destination != null)
        {
            playersxcoord = _root.shipcoordinatex;
            playersycoord = _root.shipcoordinatey;
            range = Math.round(Math.sqrt((playersxcoord - this.xcoord) * (playersxcoord - this.xcoord) + (playersycoord - this.ycoord) * (playersycoord - this.ycoord)));
            _parent.range = range;
            if (range > 200)
            {
                arrow._visible = true;
                arrow._rotation = 180 - Math.atan2(this.xcoord - playersxcoord, this.ycoord - playersycoord) / 0.017453;
            }
            else
            {
                arrow._visible = false;
            } // end if
        } // end if
    } // end else if
}

// [Action in Frame 1]
stop ();

stop ();
