﻿// Action script...

// [Action in Frame 4]
helptext = "\t\t\t\tPurchase Trade Goods HELP (F12 to change and close) \r This is  Where You Can Purchase And Sell Trade Goods Between Bases \r Clicking on the \'Sell\' or \'Buy\' buttons to change the mode \r The Top Right number is how much funds you have\r \rBUYING /SELLING \r Item Column - name of item buying \r Price Column - Cost per unit to buy / sell \r Max Qty Column (while buying)- Maximum Items you can hold, or buy \r You Own - How units you hold of that item  \r Quantity - Enter how many units you wish to purchase / sell for the item on that row \r Click the blue \'BUY\'/\'SELL\' button to purchase / sell the item on that row \r \r EXIT BUTTON \r Click to exit and return to the main starbase screen \r ";
stop ();
