﻿// Action script...

// [Action in Frame 9]
helptext = "Hangar HELP (F12 to change and close) \r This is  Where You Change, Buy, or Sell your Ship\r \rCHANGING SHIPS \r Click the Change ship button to set this as your current ship  \r  The Ship Labeled Current is your current one.   \r \r BUYING A NEW SHIP \r If there is a Blank spot, clicking Purchase new ship will  \r  take you into a screen to buy a new ship.   \r \r SELL A SHIP \r Click to sell your ship, You will be offered to sell your ship \r for 1/2 of the ships worth, and 1/4 of the worth of the components. \r Click Yes to sell it, No to cancel. \r \r EXIT BUTTON \r Click to exit and return to the main starbase screen \r ";
stop ();
