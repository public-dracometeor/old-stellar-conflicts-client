﻿// Action script...

function func_beginmeteor()
{
    meteortarget = null;
    checkattempts = 30;
    for (ww = 0; ww < checkattempts; ww++)
    {
        targetnumber = Math.round(Math.random() * (_root.starbaselocation.length - 1));
        if (_root.starbaselocation[targetnumber][5] == "ACTIVE" && _root.starbaselocation[targetnumber][0].substr(0, 2) == "SB")
        {
            meteortarget = targetnumber;
            break;
        } // end if
    } // end of for
    if (meteortarget != null && _root.incomingmeteor.length < 4)
    {
        targetname = _root.starbaselocation[targetnumber][0];
        meteortype = Math.round(Math.random() * (destroyingmeteor.length - 1));
        meteorincomingangle = Math.round(Math.random() * 360);
        idtosend = "*M" + Math.round(Math.random() * 99);
        meteorlife = destroyingmeteor[meteortype][3];
        meteormovingagle = meteorincomingangle - 180;
        meteorlifetimeinsec = Math.round(destroyingmeteor[meteortype][1] / 1000);
        if (meteormovingagle < 0)
        {
            meteormovingagle = meteormovingagle + 360;
        } // end if
        timetillhit = Math.round(destroyingmeteor[meteortype][1] / 1000);
        datatosend = "MET`CREATE`" + idtosend + "`" + meteormovingagle + "`" + targetname + "`" + meteortype + "`" + meteorlife + "`" + timetillhit + "`" + _root.errorchecknumber + "~";
        _root.mysocket.send(datatosend);
        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
    } // end if
} // End of the function
function func_meteoricoming(meteorid, meteorheading, targetname, meteortype, meterorlife, timeelapsed)
{
    meteorid = String(meteorid);
    idtaken = false;
    for (qqq = 0; qqq < _root.incomingmeteor.length; qqq++)
    {
        if (meteorid == _root.incomingmeteor[qqq][0])
        {
            idtaken = true;
            break;
        } // end if
    } // end of for
    if (idtaken == false)
    {
        if (_root.incomingmeteor.length < 1)
        {
            _root.incomingmeteor = new Array();
        } // end if
        meteortype = Number(meteortype);
        timeelapsed = Number(timeelapsed);
        meteorheading = Number(meteorheading);
        for (ww = 0; ww < _root.starbaselocation.length; ww++)
        {
            if (String(targetname) == _root.starbaselocation[ww][0])
            {
                targetnumber = ww;
                break;
            } // end if
        } // end of for
        timetillhit = destroyingmeteor[meteortype][1] - timeelapsed * 1000;
        meterortargetx = _root.starbaselocation[targetnumber][1];
        meterortargety = _root.starbaselocation[targetnumber][2];
        velocity = destroyingmeteor[meteortype][0];
        ymovementpersec = -velocity * _root.cosines[meteorheading];
        xmovementpersec = velocity * _root.sines[meteorheading];
        currenty = Number(meterortargety) - destroyingmeteor[meteortype][1] / 1000 * ymovementpersec;
        currentx = Number(meterortargetx) - destroyingmeteor[meteortype][1] / 1000 * xmovementpersec;
        location = _root.incomingmeteor.length;
        _root.incomingmeteor[location] = new Array();
        _root.incomingmeteor[location][0] = meteorid;
        _root.incomingmeteor[location][1] = currentx;
        _root.incomingmeteor[location][2] = currenty;
        _root.incomingmeteor[location][3] = meteorheading;
        _root.incomingmeteor[location][4] = targetname;
        _root.incomingmeteor[location][5] = timetillimpact;
        _root.incomingmeteor[location][6] = meterorlife;
        _root.incomingmeteor[location][7] = meteortype;
        _root.incomingmeteor[location][8] = getTimer();
        _root.incomingmeteor[location][9] = getTimer() + (destroyingmeteor[meteortype][1] - timeelapsed * 1000) - timetocheckmeteorbeforehitting;
        _root.incomingmeteor[location][10] = xmovementpersec;
        _root.incomingmeteor[location][11] = ymovementpersec;
        _root.incomingmeteor[location][12] = getTimer() + (destroyingmeteor[meteortype][1] - timeelapsed * 1000);
        _root.incomingmeteor[location][13] = destroyingmeteor[meteortype][2];
        timetillhitleft = Math.round((_root.incomingmeteor[location][12] - getTimer()) / 1000);
        message = "Meteor Located: Target " + targetname + " ETA: " + timetillhitleft + " s, Check Map for location.";
        _root.enterintochat(message, _root.systemchattextcolor);
        _root.gamedisplayarea.func_meteordraw("ADD", meteorid);
        newsmessage = "There are reports of a meteor heading towards " + targetname + ". Its arrival time is " + " in " + timetillhitleft + " seconds. It will destroy the base if not stopped!";
        _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
    } // end if
} // End of the function
function func_meteorhitbase(meteorid)
{
    for (wq = 0; wq < _root.incomingmeteor.length; wq++)
    {
        if (meteorid == _root.incomingmeteor[wq][0])
        {
            destroyedbase = _root.incomingmeteor[wq][4];
            destroyedtime = getTimer() + _root.incomingmeteor[wq][13];
            for (hfd = 0; hfd < _root.starbaselocation.length; hfd++)
            {
                if (destroyedbase == _root.starbaselocation[hfd][0])
                {
                    if (_root.starbaselocation[hfd][5] == "ACTIVE")
                    {
                        _root.starbaselocation[hfd][5] = destroyedtime;
                        message = "Starbase " + destroyedbase + " was destroyed by a meteor for " + Math.round(_root.incomingmeteor[wq][13] / 1000) + " seconds";
                        _root.enterintochat(message, _root.systemchattextcolor);
                        _root.gamedisplayarea.func_meteordraw("REMOVE", meteorid);
                        newsmessage = "Starbase " + targetname + " has been destroyed by a meteor. It will take " + Math.round(_root.incomingmeteor[wq][13] / 1000) + " seconds until it will be repaired.";
                        _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
                        _root.gamedisplayarea.gamebackground[destroyedbase].basedestroyed = true;
                    }
                    else
                    {
                        message = "Starbase " + destroyedbase + " was already destroyed by a meteor";
                        _root.enterintochat(message, _root.systemchattextcolor);
                        _root.gamedisplayarea.func_meteordraw("REMOVE", meteorid);
                    } // end else if
                    break;
                } // end if
            } // end of for
            break;
        } // end if
    } // end of for
} // End of the function
function func_meteordamaged(meteorid, destroyedbase)
{
    _root.gamedisplayarea.func_meteordraw("REMOVE", meteorid);
    message = "A meteor was destroyed";
    _root.enterintochat(message, _root.systemchattextcolor);
} // End of the function
function func_removemeteor(meteorid)
{
} // End of the function
function func_meteorlife(meteorid, meteorlife)
{
    meteorlife = Number(meteorlife);
    for (location = 0; location < _root.incomingmeteor.length; location++)
    {
        if (_root.incomingmeteor[location][0] == meteorid)
        {
            if (_root.incomingmeteor[location][6] > meteorlife)
            {
                _root.incomingmeteor[location][6] = meteorlife;
                _root.gamedisplayarea.gamebackground["meteor" + meteorid].func_currentlife(meteorlife);
                break;
            } // end if
        } // end if
    } // end of for
} // End of the function
function func_meteordestroyer(meteorid, killer)
{
    for (location = 0; location < _root.incomingmeteor.length; location++)
    {
        if (_root.incomingmeteor[location][0] == meteorid)
        {
            _root.gamedisplayarea.func_meteordraw("DESTROYED", meteorid);
            meteortype = _root.incomingmeteor[location][7];
            target = _root.incomingmeteor[location][4];
            newsmessage = "A meteor heading towards Starbase " + targetname + " has been destroyed by " + func_playersrealname(killer) + " for " + destroyingmeteor[meteortype][4] + " funds and " + destroyingmeteor[meteortype][5] + " score";
            _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
            message = "A meteor was destroyed by " + func_playersrealname(killer);
            _root.enterintochat(message, _root.systemchattextcolor);
            _root.incomingmeteor.splice(location, 1);
            --location;
            if (String(_root.playershipstatus[3][0]) == String(killer))
            {
                datatosend = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + destroyingmeteor[meteortype][5] * _root.scoreratiomodifier;
                "~";
                _root.mysocket.send(datatosend);
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(destroyingmeteor[meteortype][4]);
                _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
            } // end if
        } // end if
    } // end of for
} // End of the function
currentno = 0;
currentmeteordrawlevel = 0;
maxmeteordrawlevel = 20;
timetocheckmeteorbeforehitting = 12000;
destroyingmeteor = new Array();
destroyingmeteor[currentno] = new Array();
destroyingmeteor[currentno][0] = 30;
destroyingmeteor[currentno][1] = 120000;
destroyingmeteor[currentno][2] = 180000;
destroyingmeteor[currentno][3] = 50000;
destroyingmeteor[currentno][4] = 65000;
destroyingmeteor[currentno][5] = 150;
++currentno;
destroyingmeteor[currentno] = new Array();
destroyingmeteor[currentno][0] = 20;
destroyingmeteor[currentno][1] = 180000;
destroyingmeteor[currentno][2] = 240000;
destroyingmeteor[currentno][3] = 100000;
destroyingmeteor[currentno][4] = 130000;
destroyingmeteor[currentno][5] = 220;
