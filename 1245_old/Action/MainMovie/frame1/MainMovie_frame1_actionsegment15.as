﻿// Action script...

function func_racinggamefinished()
{
    _root.racinginformation[0] = 0;
    _root.racingcheckpoints = new Array();
    colourtouse = _root.systemchattextcolor;
    message = "HOST: Race is Completed";
    _root.enterintochat(message, colourtouse);
    _root.gotoAndStop("racingzonescreen");
} // End of the function
function func_racinggamefinisher(playerloc, playerplaced)
{
    playerloc = Number(playerloc);
    playerplaced = Number(playerplaced);
    for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
    {
        if (playerloc == _root.currentonlineplayers[jjjj][0])
        {
            name = _root.currentonlineplayers[jjjj][1];
            break;
        } // end if
    } // end of for
    colourtouse = _root.systemchattextcolor;
    message = "HOST: Player " + name + " has finished with Rank # " + (playerplaced + 1);
    if (playerplaced == 0)
    {
        message = message + (", for " + _root.func_formatnumber(_root.racinginformation[1]) + " funds");
        if (_root.playershipstatus[3][0] == playerloc)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(_root.racinginformation[1]);
        } // end if
    } // end if
    _root.enterintochat(message, colourtouse);
} // End of the function
function func_createracinggamepoints(navpoints, racestarttime, reward)
{
    racingcheckpoints = new Array();
    for (rac = 0; rac < navpoints.length; rac++)
    {
        racingcheckpoints[rac] = new Array();
        racingcheckpoints[rac][0] = navpoints[rac];
        racingcheckpoints[rac][1] = false;
    } // end of for
    racinggameinprogress = true;
    distance = 0;
    lastx = 0;
    lasty = 0;
    for (rac = 0; rac < navpoints.length; rac++)
    {
        for (sect = 0; sect < _root.sectormapitems.length; sect++)
        {
            if (_root.sectormapitems[sect][0] == racingcheckpoints[rac][0])
            {
                xloc = _root.sectormapitems[sect][1];
                yloc = _root.sectormapitems[sect][2];
                xdiff = lastx - xloc;
                ydiff = lasty - yloc;
                lasty = yloc;
                lastx = xloc;
                distance = distance + Math.sqrt(xdiff * xdiff + ydiff * ydiff);
                break;
            } // end if
        } // end of for
    } // end of for
    racetotaldistance = distance;
    racestarttime = Number(racestarttime);
    racinginformation[1] = finalracegamereward = Number(reward);
    if (racestarttime > 0)
    {
        colourtouse = _root.systemchattextcolor;
        message = "HOST: Race Starting in " + racestarttime + " seconds, reward is " + _root.func_formatnumber(finalracegamereward) + " funds";
        _root.enterintochat(message, colourtouse);
        racinginformation[0] = getTimer() + racestarttime * 1000;
    }
    else
    {
        colourtouse = _root.systemchattextcolor;
        message = "HOST: Race Started " + Math.abs(racestarttime) + " seconds ago, reward is " + _root.func_formatnumber(finalracegamereward) + " funds";
        _root.enterintochat(message, colourtouse);
        racinginformation[0] = 0;
    } // end else if
} // End of the function
function func_getracereward(distancebeingtraveled)
{
    fundsperunitoftravel = 5;
    timesfundsforextraplayer = 0.300000;
    basefunds = distancebeingtraveled * fundsperunitoftravel;
    fundswithplayers = basefunds * (timesfundsforextraplayer * _root.currentonlineplayers.length + 0.700000);
    return (Math.round(fundswithplayers));
} // End of the function
isgameracingzone = false;
racegamepacespeed = 50;
racegamestartdelay = 30;
racinginformation = new Array();
racinginformation[0] = 0;
racinginformation[1] = 95;
racinginformation[2] = true;
