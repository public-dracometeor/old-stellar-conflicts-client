﻿// Action script...

// [onClipEvent of sprite 1389 in frame 1]
onClipEvent (load)
{
    function func_maplisterkey(incomingvariable)
    {
        if (_root.playershipstatus[5][1] != null)
        {
            keypressed = Key.getCode();
            if (incomingvariable == "buttonpress")
            {
                keypressed = "119";
            } // end if
            if (keypressed == "119")
            {
                if (_root.mapdsiplayed == false)
                {
                    _root.mapdsiplayed = true;
                    _root.attachMovie("ingamemap", "ingamemap", 9999999);
                }
                else
                {
                    _root.mapdsiplayed = false;
                    _root.createEmptyMovieClip("blanker", 9999999);
                } // end if
            } // end if
        } // end else if
    } // End of the function
    themapkeyListener = new Object();
    themapkeyListener.onKeyDown = function ()
    {
        func_maplisterkey();
    };
    Key.addListener(themapkeyListener);
}
