﻿// Action script...

_root.keypressdelay = 0;
_root.shipositiondelay = 300;
_root.errorcheckdelay = 7000;
_root.errorcheckdata = new Array();
_root.errorchecknumber = 0;
_root._x = 0;
_root._y = 0;
_root.cosines = new Array();
for (j = 0; j <= 360; j++)
{
    _root.cosines[j] = parseFloat(Math.cos(0.017453 * j));
} // end of for
_root.sines = new Array();
for (j = 0; j <= 360; j++)
{
    _root.sines[j] = parseFloat(Math.sin(0.017453 * j));
} // end of for
stop ();
