﻿// Action script...

function func_playercreatekflag()
{
    minusers = 4;
    location = _root.fun_kingofflagrandomcoords();
    xcoord = location[0];
    ycoord = location[1];
    if (_root.kingofflag[0] == null && _root.kingofflag[2] == null)
    {
        if (_root.currentonlineplayers.length >= minusers)
        {
            datatosend = "KFLAG`CREATE`" + xcoord + "`" + ycoord + "~";
            _root.mysocket.send(datatosend);
        }
        else
        {
            message = "HOST: Need at least " + minusers + " players to start the game";
            _root.enterintochat(message, _root.systemchattextcolor);
        } // end else if
    }
    else
    {
        if (_root.kingofflag[0] == null)
        {
            xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
            ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
            message = "HOST: Flag Already in game at X:" + xgrid + " Y:" + ygrid;
        }
        else
        {
            xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
            ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
            message = "HOST: Flag Already in game, held by " + _root.func_playersrealname(_root.kingofflag[0]) + " last report at X:" + xgrid + " Y:" + ygrid;
        } // end else if
        _root.enterintochat(message, _root.systemchattextcolor);
    } // end else if
} // End of the function
function func_playersrealname(playerid)
{
    for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
    {
        if (playerid == _root.currentonlineplayers[jjjj][0])
        {
            return (_root.currentonlineplayers[jjjj][1]);
            break;
        } // end if
    } // end of for
} // End of the function
function fun_kingofflagrandomcoords()
{
    xwidthofsector = _root.sectorinformation[1][0];
    ywidthofsector = _root.sectorinformation[1][1];
    xgrid = Math.round(Math.random() * Number(_root.sectorinformation[0][0] - 2)) + 1;
    xcoord = xwidthofsector * xgrid - Math.round(xwidthofsector / 2);
    ygrid = Math.round(Math.random() * Number(_root.sectorinformation[0][1] - 2)) + 1;
    ycoord = ywidthofsector * ygrid - Math.round(ywidthofsector / 2);
    location = new Array();
    location[0] = xcoord;
    location[1] = ycoord;
    return (location);
} // End of the function
function func_kingofflagcommand(method, var1, var2, var3)
{
    if (method == "TRANS")
    {
        currenthostalreadysent = false;
        if (String(var1) == String(_root.kingofflag[0]))
        {
            currenthostalreadysent = true;
        } // end if
        _root.kingofflag[0] = var1;
        _root.kingofflag[1] = getTimer() + _root.kingofflag[10];
        _root.gamedisplayarea.func_flagcontrols("REMOVE");
        if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
        {
            _root.game_top_bar.flagwindow.func_runflagscript();
        } // end if
        if (currenthostalreadysent != true)
        {
            message = "HOST: " + func_playersrealname(_root.kingofflag[0]) + " has the flag.";
            _root.enterintochat(message, _root.systemchattextcolor);
        } // end if
    }
    else if (method == "END")
    {
        func_kingofflagreset();
        scorereward = Number(var2);
        if (isNaN(scorereward))
        {
            scorereward = 0;
        } // end if
        fundreward = Number(var3);
        if (isNaN(fundreward))
        {
            fundreward = 0;
        } // end if
        message = "HOST: " + func_playersrealname(var1) + " won the flagging game for " + Math.floor(scorereward / _root.scoreratiomodifier) + " score and " + _root.func_formatnumber(fundreward) + " funds.";
        _root.enterintochat(message, _root.systemchattextcolor);
        for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
        {
            if (String(var1) == String(_root.currentonlineplayers[jjjj][0]))
            {
                _root.currentonlineplayers[jjjj][5] = Number(_root.currentonlineplayers[jjjj][5]) + Number(scorereward);
                break;
            } // end if
        } // end of for
        if (String(_root.playershipstatus[3][0]) == String(var1))
        {
            _root.playershipstatus[5][9] = Number(_root.playershipstatus[5][9]) + Number(scorereward);
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(fundreward);
            _root.toprightinformation.playerscore = "Score: " + Math.floor(Number(_root.playershipstatus[5][9]) / _root.scoreratiomodifier);
            _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
        } // end if
        _root.refreshonlinelist = true;
    }
    else if (method == "DROP")
    {
        _root.kingofflag[0] = null;
        _root.kingofflag[1] = null;
        _root.kingofflag[2] = Number(var1);
        _root.kingofflag[3] = Number(var2);
        _root.gamedisplayarea.func_flagcontrols("ADD", _root.kingofflag[2], _root.kingofflag[3]);
        xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
        ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
        message = "HOST: Flag located at X:" + xgrid + " Y:" + ygrid;
        _root.enterintochat(message, _root.systemchattextcolor);
    }
    else if (method == "CREATE")
    {
        _root.kingofflag[0] = null;
        _root.kingofflag[1] = null;
        _root.kingofflag[2] = Number(var1);
        _root.kingofflag[3] = Number(var2);
        _root.gamedisplayarea.func_flagcontrols("ADD", _root.kingofflag[2], _root.kingofflag[3]);
        xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
        ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
        message = "HOST: Flag located at X:" + xgrid + " Y:" + ygrid;
        _root.enterintochat(message, _root.systemchattextcolor);
    }
    else if (method == "INFO")
    {
        flagtime = Math.round((_root.kingofflag[1] - getTimer()) / 1000);
        _root.kingofflag[2] = Number(var1);
        _root.kingofflag[3] = Number(var2);
        xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
        ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
        name = func_playersrealname(playerid);
        message = "HOST: " + _root.func_playersrealname(_root.kingofflag[0]) + " has the flag at X:" + xgrid + " Y:" + ygrid + " with " + flagtime + " seconds left";
        _root.enterintochat(message, _root.systemchattextcolor);
    } // end else if
} // End of the function
function func_kingofflagreset()
{
    _root.kingofflag = new Array();
    _root.kingofflag[0] = null;
    _root.kingofflag[1] = null;
    _root.kingofflag[2] = null;
    _root.kingofflag[3] = null;
    _root.kingofflag[10] = 120000;
    _root.kingofflag[11] = 50;
    _root.kingofflag[12] = 15;
    _root.kingofflag[13] = 30000;
    _root.kingofflag[14] = 15000;
    _root.kingofflag[15] = 20000;
    _root.kingofflag[16] = false;
} // End of the function
func_kingofflagreset();
