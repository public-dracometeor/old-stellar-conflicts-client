﻿// Action script...

// [onClipEvent of sprite 1271 in frame 1]
onClipEvent (load)
{
    function toptenprocess(toptennames)
    {
        newinfo = toptennames.split("~");
        i = 0;
        topten = "Top Ten Players:\r";
        while (i < newinfo.length - 1)
        {
            currentthread = newinfo[i].split("`");
            topten = topten + (i + 1 + ": " + currentthread[0] + " - " + Math.floor(Number(currentthread[1]) / _root.scoreratiomodifier) + "\r");
            ++i;
        } // end while
    } // End of the function
    topten = "Loading Top Ten";
    myLoadVars = new XML();
    myLoadVars.load(_root.pathtoaccounts + "accounts.php?mode=topten");
    myLoadVars.onLoad = function (success)
    {
        toptennames = String(myLoadVars);
        toptenprocess(toptennames);
    };
}
