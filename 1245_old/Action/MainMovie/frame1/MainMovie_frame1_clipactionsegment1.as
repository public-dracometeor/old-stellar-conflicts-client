﻿// Action script...

// [onClipEvent of sprite 1392 in frame 1]
onClipEvent (load)
{
    function saveplayersgame(sender)
    {
        _root.func_checkfortoomuchaddedandset();
        _root.savegamesenderfrom = sender;
        if (savestatus == "Save Game")
        {
            savestatus = sender.savestatus = "Saving";
            variablestosave = "PI" + _root.playershipstatus[3][0] + "`ST" + _root.playershipstatus[5][0] + "`SG" + _root.playershipstatus[2][0] + "`EG" + _root.playershipstatus[1][0] + "`EC" + _root.playershipstatus[1][5] + "`CR" + _root.playershipstatus[3][1] + "`SE" + _root.playershipstatus[5][1] + "`" + _root.playershipstatus[4][0];
            for (currenthardpoint = 0; currenthardpoint < _root.playershipstatus[0].length; currenthardpoint++)
            {
                variablestosave = variablestosave + ("`HP" + currenthardpoint + "G" + _root.playershipstatus[0][currenthardpoint][0]);
            } // end of for
            for (currentturretpoint = 0; currentturretpoint < _root.playershipstatus[8].length; currentturretpoint++)
            {
                variablestosave = variablestosave + ("`TT" + currentturretpoint + "G" + _root.playershipstatus[8][currentturretpoint][0]);
            } // end of for
            for (currentcargo = 0; currentcargo < _root.playershipstatus[4][1].length; currentcargo++)
            {
                if (_root.playershipstatus[4][1][currentcargo] > 0)
                {
                    variablestosave = variablestosave + ("`CO" + currentcargo + "A" + _root.playershipstatus[4][1][currentcargo]);
                } // end if
            } // end of for
            for (spno = 0; spno < _root.playershipstatus[11][1].length; spno++)
            {
                variablestosave = variablestosave + ("`SP" + _root.playershipstatus[11][1][spno][0] + "Q" + _root.playershipstatus[11][1][spno][1]);
            } // end of for
            variablestosave = variablestosave + "~";
            if (_root.isplayeraguest == false)
            {
                _root.saveplayerextraships();
                if (_root.lastplayerssavedinfosent == "info=" + variablestosave + "&score=" + _root.playershipstatus[5][9] + "&shipsdata=" + _root.playersextrashipsdata)
                {
                    sender.savestatus = "No Updates";
                    savestatus = "Save Game";
                }
                else
                {
                    _root.lastplayerssavedinfosent = "info=" + variablestosave + "&score=" + _root.playershipstatus[5][9] + "&shipsdata=" + _root.playersextrashipsdata;
                    datatosend = "savegame`~:" + _root.playershipstatus[3][2] + ":" + _root.playershipstatus[3][3] + ":" + variablestosave + ":" + _root.playershipstatus[5][9] + ":" + _root.playersextrashipsdata + ":" + _root.playershipstatus[3][1] + ":" + _root.playershipstatus[5][19] + ":";
                    accountserv = new XMLSocket();
                    if (_root.isgamerunningfromremote == false)
                    {
                        currenturl = String(_root._url);
                        if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
                        {
                            accountserv.connect("", _root.accountserverport);
                        } // end if
                    }
                    else
                    {
                        accountserv.connect(_root.accountserveraddy, _root.accountserverport);
                    } // end else if
                    accountserv.onConnect = function (success)
                    {
                        okbutton._visible = true;
                        accountserv.send(datatosend);
                    };
                    accountserv.onXML = xmlhandler;
                } // end if
            } // end if
        } // end else if
    } // End of the function
    function xmlhandler(doc)
    {
        loadedvars = String(doc);
        info = loadedvars.split("~");
        newinfo = info[0].split("`");
        if (newinfo[0] == "savesuccess")
        {
            _root.savegamescript.savestatus = "Save Game";
            _root.savegamesenderfrom.savestatus = "Game Saved";
            _root.playershipstatus[5][19] = newinfo[1];
        }
        else
        {
            _root.savegamescript.savestatus = _root.savegamesenderfrom.savestatus = "Save Failed";
            _root.func_disconnectuser("savefailure");
        } // end else if
    } // End of the function
    this._visible = false;
    status = "N/A";
    savestatus = "Save Game";
}
