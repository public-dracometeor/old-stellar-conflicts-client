﻿// Action script...

function changetonewship(extrano)
{
    i = _root.extraplayerships[extrano][0];
    if (_root.playershipstatus[5][0] == i)
    {
    }
    else
    {
        _root.playershipstatus[5][0] = i;
        _root.playershipstatus[4][1] = new Array();
    } // end else if
    _root.playershiprotation = _root.shiptype[i][3][2];
    _root.playershipmaxvelocity = _root.shiptype[i][3][1];
    _root.playershipacceleration = _root.shiptype[i][3][0];
    _root.playershipstatus[2][4] = _root.shiptype[i][3][3];
    _root.playershipstatus[0] = new Array();
    for (currentharpoint = 0; currentharpoint < _root.shiptype[i][2].length; currentharpoint++)
    {
        _root.playershipstatus[0][currentharpoint] = new Array();
        _root.playershipstatus[0][currentharpoint][0] = _root.extraplayerships[extrano][4][currentharpoint];
        _root.playershipstatus[0][currentharpoint][1] = 0;
        _root.playershipstatus[0][currentharpoint][2] = _root.shiptype[i][2][currentharpoint][0];
        _root.playershipstatus[0][currentharpoint][3] = _root.shiptype[i][2][currentharpoint][1];
    } // end of for
    currentturretpoint = 0;
    _root.playershipstatus[8] = new Array();
    while (currentturretpoint < _root.shiptype[i][5].length)
    {
        _root.playershipstatus[8][currentturretpoint] = new Array();
        _root.playershipstatus[8][currentturretpoint][0] = _root.extraplayerships[extrano][5][currentturretpoint];
        ++currentturretpoint;
    } // end while
    _root.playershipstatus[11] = new Array();
    _root.playershipstatus[11][0] = new Array();
    _root.playershipstatus[11][0][0] = 0;
    _root.playershipstatus[11][0][1] = 0;
    _root.playershipstatus[11][1] = new Array();
    _root.playershipstatus[11][2] = new Array();
    _root.playershipstatus[11][2][0] = new Array();
    _root.playershipstatus[11][2][0][0] = 0;
    _root.playershipstatus[11][2][1] = new Array();
    _root.playershipstatus[11][2][1][0] = 0;
    _root.playershipstatus[11][2][2] = new Array();
    _root.playershipstatus[11][2][2][0] = 0;
    for (spno = 0; spno < _root.extraplayerships[extrano][11][1].length; spno++)
    {
        _root.playershipstatus[11][1][spno] = new Array();
        _root.playershipstatus[11][1][spno][0] = _root.extraplayerships[extrano][11][1][spno][0];
        _root.playershipstatus[11][1][spno][1] = _root.extraplayerships[extrano][11][1][spno][1];
    } // end of for
    _root.playershipstatus[2][0] = _root.extraplayerships[extrano][1];
    _root.playershipstatus[1][0] = _root.extraplayerships[extrano][2];
    _root.playershipstatus[1][5] = _root.extraplayerships[extrano][3];
} // End of the function
function savecurrenttoextraship(extrano)
{
    _root.extraplayerships[extrano][0] = _root.playershipstatus[5][0];
    i = _root.playershipstatus[5][0];
    _root.extraplayerships[extrano][4] = new Array();
    for (currentharpoint = 0; currentharpoint < _root.shiptype[i][2].length; currentharpoint++)
    {
        _root.extraplayerships[extrano][4][currentharpoint] = _root.playershipstatus[0][currentharpoint][0];
    } // end of for
    currentturretpoint = 0;
    _root.extraplayerships[extrano][5] = new Array();
    while (currentturretpoint < _root.shiptype[i][5].length)
    {
        _root.extraplayerships[extrano][5][currentturretpoint] = _root.playershipstatus[8][currentturretpoint][0];
        ++currentturretpoint;
    } // end while
    spno = 0;
    _root.extraplayerships[extrano][11] = new Array();
    _root.extraplayerships[extrano][11][1] = new Array();
    while (spno < _root.playershipstatus[11][1].length)
    {
        _root.extraplayerships[extrano][11][1][spno] = new Array();
        _root.extraplayerships[extrano][11][1][spno][0] = _root.playershipstatus[11][1][spno][0];
        _root.extraplayerships[extrano][11][1][spno][1] = _root.playershipstatus[11][1][spno][1];
        ++spno;
    } // end while
    _root.extraplayerships[extrano][1] = _root.playershipstatus[2][0];
    _root.extraplayerships[extrano][2] = _root.playershipstatus[1][0];
    _root.extraplayerships[extrano][3] = _root.playershipstatus[1][5];
} // End of the function
function loadplayerextraships(loadedvars)
{
    newinfo = loadedvars.split("~");
    i = 0;
    _root.extraplayerships = new Array();
    for (i = 0; i < maxextraships; i++)
    {
        _root.extraplayerships[i] = new Array();
        _root.extraplayerships[i][0] = null;
    } // end of for
    _root.playerscurrentextrashipno = newinfo[0].substr(2);
    for (i = 0; i + 1 < newinfo.length - 1; i++)
    {
        if (newinfo[i + 1].substr(0, 2) == "SH")
        {
            currentthread = newinfo[i + 1].split("`");
            if (currentthread[1].substr(0, 2) == "ST")
            {
                currentshipid = Number(currentthread[0].substr("2"));
                _root.extraplayerships[currentshipid] = new Array();
                _root.extraplayerships[currentshipid][0] = int(currentthread[1].substr("2"));
                _root.extraplayerships[currentshipid][1] = int(currentthread[2].substr("2"));
                _root.extraplayerships[currentshipid][2] = int(currentthread[3].substr("2"));
                _root.extraplayerships[currentshipid][3] = int(currentthread[4].substr("2"));
                tr = 5;
                _root.extraplayerships[currentshipid][4] = new Array();
                for (currentharpoint = 0; currentharpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][2].length; currentharpoint++)
                {
                    _root.extraplayerships[currentshipid][4][currentharpoint] = "none";
                } // end of for
                while (currentthread[tr].substr(0, 2) == "HP")
                {
                    guninfo = currentthread[tr].split("G");
                    currenthardpoint = guninfo[0].substr("2");
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.extraplayerships[currentshipid][4][currenthardpoint] = guninfo[1];
                    ++tr;
                } // end while
                _root.extraplayerships[currentshipid][5] = new Array();
                currentturretpoint = 0;
                _root.extraplayerships[currentshipid][5] = new Array();
                while (currentturretpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][5].length)
                {
                    _root.extraplayerships[currentshipid][5][currentturretpoint] = "none";
                    ++currentturretpoint;
                } // end while
                while (currentthread[tr].substr(0, 2) == "TT")
                {
                    guninfo = currentthread[tr].split("G");
                    currentturretpoint = guninfo[0].substr("2");
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.extraplayerships[currentshipid][5][currentturretpoint] = guninfo[1];
                    ++tr;
                } // end while
                spno = 0;
                _root.extraplayerships[currentshipid][11] = new Array();
                _root.extraplayerships[currentshipid][11][1] = new Array();
                while (currentthread[tr].substr(0, 2) == "SP")
                {
                    cargoinfo = currentthread[tr].split("Q");
                    currentspecialtype = Number(cargoinfo[0].substr("2"));
                    specialquantity = Number(cargoinfo[1]);
                    _root.extraplayerships[currentshipid][11][1][spno] = new Array();
                    _root.extraplayerships[currentshipid][11][1][spno][0] = currentspecialtype;
                    _root.extraplayerships[currentshipid][11][1][spno][1] = specialquantity;
                    ++spno;
                    ++tr;
                } // end while
            } // end if
        } // end if
    } // end of for
    if (_root.playerscurrentextrashipno != "capital")
    {
        if (isNaN(Number(_root.playerscurrentextrashipno)) || Number(_root.playerscurrentextrashipno) > maxextraships - 1)
        {
            for (i = 0; i < maxextraships; i++)
            {
                if (_root.extraplayerships[i][0] != null && !isNaN(Number(_root.playerscurrentextrashipno)))
                {
                    _root.playerscurrentextrashipno = i;
                    break;
                } // end if
            } // end of for
        } // end if
        _root.changetonewship(_root.playerscurrentextrashipno);
    } // end if
} // End of the function
function saveplayerextraships()
{
    saveinfo = "";
    saveinfo = saveinfo + ("NO" + _root.playerscurrentextrashipno + "~");
    for (currentshipid = 0; currentshipid < maxextraships; currentshipid++)
    {
        if (_root.extraplayerships[currentshipid][0] != null)
        {
            saveinfo = saveinfo + ("SH" + currentshipid + "`");
            saveinfo = saveinfo + ("ST" + _root.extraplayerships[currentshipid][0] + "`");
            saveinfo = saveinfo + ("SG" + _root.extraplayerships[currentshipid][1] + "`");
            saveinfo = saveinfo + ("EG" + _root.extraplayerships[currentshipid][2] + "`");
            saveinfo = saveinfo + ("EC" + _root.extraplayerships[currentshipid][3] + "`");
            for (currentharpoint = 0; currentharpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][2].length; currentharpoint++)
            {
                saveinfo = saveinfo + ("HP" + currentharpoint + "G" + _root.extraplayerships[currentshipid][4][currentharpoint] + "`");
            } // end of for
            for (currentturretpoint = 0; currentturretpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][5].length; currentturretpoint++)
            {
                saveinfo = saveinfo + ("TT" + currentturretpoint + "G" + _root.extraplayerships[currentshipid][5][currentturretpoint] + "`");
            } // end of for
            for (spno = 0; spno < _root.extraplayerships[currentshipid][11][1].length; spno++)
            {
                saveinfo = saveinfo + ("SP" + _root.extraplayerships[currentshipid][11][1][spno][0] + "Q" + _root.extraplayerships[currentshipid][11][1][spno][1] + "`");
            } // end of for
            saveinfo = saveinfo + "~";
        } // end if
    } // end of for
    _root.playersextrashipsdata = saveinfo;
} // End of the function
Stage.showMenu = false;
maxextraships = 3;
