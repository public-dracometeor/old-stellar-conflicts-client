﻿// Action script...

// [onClipEvent of sprite 1752 in frame 14]
onClipEvent (load)
{
    function starbaseexitposition()
    {
        jjj = 0;
        databeenupdated = false;
        while (jjj < _root.starbaselocation.length && databeenupdated == false)
        {
            if (_root.starbaselocation[jjj][0] == _root.playershipstatus[4][0])
            {
                databeenupdated = true;
                dockingrange = 0;
                _root.shipcoordinatex = _root.starbaselocation[jjj][1] + dockingrange * 2 * Math.random() - dockingrange;
                _root.shipcoordinatey = _root.starbaselocation[jjj][2] + dockingrange * 2 * Math.random() - dockingrange;
            } // end if
            ++jjj;
        } // end while
    } // End of the function
    function determinegoods(loadedvars)
    {
        goodsinfo = loadedvars.split("~");
        newinfo = goodsinfo[0].split("`");
        i = 0;
        currenttradegood = 0;
        _root.starbasetradeitem = new Array();
        while (i < newinfo.length - 1)
        {
            if (newinfo[i].charAt(0) == "S")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "selling";
                ++currenttradegood;
            } // end if
            if (newinfo[i].charAt(0) == "B")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "buying";
                ++currenttradegood;
            } // end if
            ++i;
        } // end while
    } // End of the function
    starbaseexitposition();
    _root.mainscreenstarbase.shopselection = "none";
    currenthelpframedisplayed = 0;
    removeMovieClip ("_root.toprightinformation");
    newtradeVars = new XML();
    newtradeVars.load(_root.pathtoaccounts + "tradegoods.php?system=" + _root.playershipstatus[5][1] + "&mode=prices&starbase=" + _root.playershipstatus[4][0]);
    newtradeVars.onLoad = function (success)
    {
        loadedvars = String(newtradeVars);
        if (loadedvars.length > 1)
        {
            determinegoods(loadedvars);
            
        } // end if
    };
    _root.mainscreenstarbase.welcometostarbase = "Welcome to StarBase " + _root.playershipstatus[4][0];
}
