﻿// Action script...

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (load)
{
    _root.playershipstatus[5][15] = "";
    clearInterval(_root.specialstimers[1]);
    clearInterval(_root.specialstimers[2]);
    _root.attachMovie("onlineplayerslist", "onlineplayerslist", 99982);
    setProperty("_root.onlineplayerslist", _x, 0);
    setProperty("_root.onlineplayerslist", _y, 0);
    if (_root.playerjustloggedin == false)
    {
        _root.hasplayerbeenlistedasdocked = false;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
        _root.playersdockedtime = 999999999;
        dockinginfosent = getTimer();
        dockinginfoerrorcheck = 10000;
    }
    else
    {
        dockinginfosent = 999999999;
        _root.hasplayerbeenlistedasdocked = true;
    } // end else if
    _root.hassafeleavebeeddisp = false;
    if (_root.playershipstatus[5][25] == "CAPLEFT")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Illegal Cap Left Detected \r Capital Ship Lost";
        _root.playershipstatus[5][25] = "YES";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
    _root.aishipshosted = new Array();
    this.mapdsiplayed = false;
}

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (_root.hasplayerbeenlistedasdocked == false && currenttime > dockinginfoerrorcheck + dockinginfosent && _root.playerjustloggedin == false)
    {
        dockinginfosent = currenttime;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
    } // end if
    if (_root.hasplayerbeenlistedasdocked == true && _root.hassafeleavebeeddisp == false && _root.playerscurrentextrashipno == "capital" && _root.playershipstatus[5][25] == "YES")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Safe to Exit Game \r With Capital Ship";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
}

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (keyDown)
{
    lastkeypressed = Key.getCode();
    if (lastkeypressed == "SDFdsf" && _root.mainscreenstarbase.shopselection == "none")
    {
        if (this.mapdsiplayed == false)
        {
            this.mapdsiplayed = true;
            _root.attachMovie("ingamemap", "ingamemap", 9999999);
            _root.mainscreenstarbase.gotoAndStop(2);
        }
        else if (this.mapdsiplayed == true)
        {
            this.mapdsiplayed = false;
            _root.createEmptyMovieClip("blanker", 9999999);
            _root.mainscreenstarbase.gotoAndStop(1);
            removeMovieClip ("ingamemap");
        } // end if
    } // end else if
    if (lastkeypressed == "123" && _root.mapdsiplayed == false)
    {
        if (_root.mainscreenstarbase.shopselection == "none")
        {
            if (currenthelpframedisplayed == 0)
            {
                _root.mapdsiplayed = false;
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "starbase" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "tradegoods")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "tradegoods" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "mission")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "mission" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "extraships")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "extraships" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "prefences")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "preferences" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "ship")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "ship" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "hardware")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "hardware" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end if
    } // end else if
    lastkeypressed = "";
}

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (load)
{
    function starbaseexitposition()
    {
        jjj = 0;
        databeenupdated = false;
        while (jjj < _root.starbaselocation.length && databeenupdated == false)
        {
            if (_root.starbaselocation[jjj][0] == _root.playershipstatus[4][0])
            {
                databeenupdated = true;
                dockingrange = 0;
                _root.shipcoordinatex = _root.starbaselocation[jjj][1] + dockingrange * 2 * Math.random() - dockingrange;
                _root.shipcoordinatey = _root.starbaselocation[jjj][2] + dockingrange * 2 * Math.random() - dockingrange;
            } // end if
            ++jjj;
        } // end while
    } // End of the function
    function determinegoods(loadedvars)
    {
        goodsinfo = loadedvars.split("~");
        newinfo = goodsinfo[0].split("`");
        i = 0;
        currenttradegood = 0;
        _root.starbasetradeitem = new Array();
        while (i < newinfo.length - 1)
        {
            if (newinfo[i].charAt(0) == "S")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "selling";
                ++currenttradegood;
            } // end if
            if (newinfo[i].charAt(0) == "B")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "buying";
                ++currenttradegood;
            } // end if
            ++i;
        } // end while
        _root.randomegameevents.func_altertradegooodpricing(_root.playershipstatus[4][0]);
    } // End of the function
    starbaseexitposition();
    _root.mainscreenstarbase.shopselection = "none";
    currenthelpframedisplayed = 0;
    removeMovieClip ("_root.toprightinformation");
    newtradeVars = new XML();
    newtradeVars.load(_root.pathtoaccounts + "tradegoods.php?system=" + _root.playershipstatus[5][1] + "&mode=prices&starbase=" + _root.playershipstatus[4][0]);
    newtradeVars.onLoad = function (success)
    {
        loadedvars = String(newtradeVars);
        if (loadedvars.length > 1)
        {
            determinegoods(loadedvars);
            
        } // end if
    };
    _root.mainscreenstarbase.welcometostarbase = "Welcome to StarBase " + _root.playershipstatus[4][0];
}

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (load)
{
    function enterintochat(info, colourtouse)
    {
        _root.gamechatinfo[1].splice(0, 0, 0);
        _root.gamechatinfo[1][0] = new Array();
        _root.gamechatinfo[1][0][0] = info;
        _root.gamechatinfo[1][0][1] = colourtouse;
        if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
        {
            _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
        } // end if
        _root.refreshchatdisplay = true;
    } // End of the function
    if (_root.playersmission[0][0][0] == "Cargorun")
    {
        currentlydockedbase = _root.playershipstatus[4][0];
        basesformission = _root.playersmission[0][0][1].split("`");
        if (_root.playersmission[0][4] == false)
        {
            if (currentlydockedbase == basesformission[0])
            {
                _root.playersmission[0][4] = true;
                enterintochat(" Cargo Picked Up! Mission begins once you exit the base.", _root.systemchattextcolor);
            } // end if
        }
        else if (currentlydockedbase != basesformission[1])
        {
            _root.playersmission = new Array();
            enterintochat(" Cargo Mission Failed!", _root.systemchattextcolor);
        }
        else
        {
            rewardamounts = _root.playersmission[0][2].split("`");
            if (_root.playersmission[0][4] == true)
            {
                reward = Number(rewardamounts[0]) + Number(rewardamounts[1]);
            }
            else
            {
                reward = Number(rewardamounts[0]);
            } // end else if
            enterintochat(" Cargo Mission Passed! Total Reward:" + reward, _root.systemchattextcolor);
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + reward;
            scoreaward = Math.round(Number(rewardamounts[0]) / _root.missiontoactualscoremodifier);
            information = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + scoreaward + "~";
            _root.mysocket.send(information);
            _root.playersmission = new Array();
        } // end else if
    } // end else if
}

// [Action in Frame 16]
stop ();
