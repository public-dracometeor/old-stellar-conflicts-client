﻿// Action script...

// [onClipEvent of sprite 1773 in frame 15]
onClipEvent (keyDown)
{
    lastkeypressed = Key.getCode();
    if (lastkeypressed == "SDFdsf" && _root.mainscreenstarbase.shopselection == "none")
    {
        if (this.mapdsiplayed == false)
        {
            this.mapdsiplayed = true;
            _root.attachMovie("ingamemap", "ingamemap", 9999999);
            _root.mainscreenstarbase.gotoAndStop(2);
        }
        else if (this.mapdsiplayed == true)
        {
            this.mapdsiplayed = false;
            _root.createEmptyMovieClip("blanker", 9999999);
            _root.mainscreenstarbase.gotoAndStop(1);
            removeMovieClip ("ingamemap");
        } // end if
    } // end else if
    if (lastkeypressed == "123" && _root.mapdsiplayed == false)
    {
        if (_root.mainscreenstarbase.shopselection == "none")
        {
            if (currenthelpframedisplayed == 0)
            {
                _root.mapdsiplayed = false;
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "starbase" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "tradegoods")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "tradegoods" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "mission")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "mission" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "extraships")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "extraships" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "prefences")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "preferences" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "ship")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "ship" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "hardware")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "hardware" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end if
    } // end else if
    lastkeypressed = "";
}
