﻿// Action script...

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (load)
{
    function func_sendstatuscheck(pilotno)
    {
        datatosend = "SP`" + pilotno + "~STATS`GET`" + _root.playershipstatus[3][0] + "~";
        _root.mysocket.send(datatosend);
    } // End of the function
    function attachtargetship(currenttargetshiptype)
    {
        this.attachMovie("shiptype" + currenttargetshiptype, "targetship", 1);
        if (this.targetship._height > 50 || this.targetship._width > 50)
        {
            if (this.targetship._height > this.targetship._width)
            {
                change = this.targetship._height - 50;
                this.targetship._height = this.targetship._height - change;
                this.targetship._width = this.targetship._width - change;
            }
            else
            {
                change = this.targetship._width - 50;
                this.targetship._height = this.targetship._height - change;
                this.targetship._width = this.targetship._width - change;
            } // end if
        } // end else if
        setProperty("targetship", _x, "153");
        setProperty("targetship", _y, "22");
        targetpointer._visible = true;
    } // End of the function
    function func_setlocalnpcs()
    {
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        minrangeforrad = 1500;
        neutralaiarray = new Array();
        for (q = 0; q < _root.neutralships.length; q++)
        {
            labell = _root.neutralships[q][10];
            xlocc = _root.gamedisplayarea[labell].xcoord;
            ylocc = _root.gamedisplayarea[labell].ycoord;
            playerdistancefromship = Math.round(Math.sqrt((playersxcoord - xlocc) * (playersxcoord - xlocc) + (playersycoord - ylocc) * (playersycoord - ylocc)));
            if (playerdistancefromship < 2000)
            {
                neutralaiarray[neutralaiarray.length] = _root.neutralships[q];
            } // end if
        } // end of for
    } // End of the function
    lasttarget = -1;
    currenttargetname = "No Target";
    currenttargetbounty = "";
    istkeypressed = false;
    targettype = "none";
    _root.targetinfo[0] = "None";
    _root.targetinfo[1] = 0;
    _root.targetinfo[2] = 0;
    _root.targetinfo[3] = "";
    this.currenttargetname.html = true;
    shielddisp = "";
    structdisp = "";
    targetName = new Color("currenttargetname");
    targetName.setRGB(16777215);
    _root.targetinfo[10] = "enemy";
    lasttarget = -1;
    lasttargetshiptype = null;
    currenttargetdist = "";
    targetpointer._visible = false;
    currentinterval = 0;
    intervalwait = 30;
}
