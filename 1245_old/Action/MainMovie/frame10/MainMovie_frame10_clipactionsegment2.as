﻿// Action script...

// [onClipEvent of sprite 1525 in frame 10]
onClipEvent (load)
{
    function func_spawnameteor(spawnchance)
    {
        minspawnchance = 0.820000;
        maxmeteors = 2;
        if (spawnchance > minspawnchance && _root.incomingmeteor.length < maxmeteors)
        {
            _root.func_beginmeteor();
        } // end if
    } // End of the function
    function func_randompiratebase(spawnchance)
    {
        numberofpirates = 0;
        for (i = 0; i < _root.playersquadbases.length; i++)
        {
            if (_root.playersquadbases[i][0].substr(0, 3) == "*AI")
            {
                ++numberofpirates;
            } // end if
        } // end of for
        if (numberofpirates < maxpiratebases && spawnchance > minpiratebaseodds)
        {
            for (attempts = 0; attempts < maxtriesforpiratebase; attempts++)
            {
                xsector = Math.round(Math.random() * _root.sectorinformation[0][0]);
                ysector = Math.round(Math.random() * _root.sectorinformation[0][1]);
                gridsaroundtaken = false;
                for (i = 0; i < _root.sectormapitems.length; i++)
                {
                    if (_root.sectormapitems[i][10] == xsector || _root.sectormapitems[i][10] == xsector - 1 || _root.sectormapitems[i][10] == xsector + 1)
                    {
                        if (_root.sectormapitems[i][11] == ysector || _root.sectormapitems[i][11] == ysector - 1 || _root.sectormapitems[i][11] == ysector + 1)
                        {
                            gridsaroundtaken = true;
                        } // end if
                    } // end if
                } // end of for
                for (i = 0; i < _root.playersquadbases.length; i++)
                {
                    if (_root.playersquadbases[i][4] == xsector || _root.playersquadbases[i][4] == xsector - 1 || _root.playersquadbases[i][4] == xsector + 1)
                    {
                        if (_root.playersquadbases[i][5] == ysector || _root.playersquadbases[i][5] == ysector - 1 || _root.playersquadbases[i][5] == ysector + 1)
                        {
                            gridsaroundtaken = true;
                        } // end if
                    } // end if
                } // end of for
                if (gridsaroundtaken == false)
                {
                    break;
                } // end if
            } // end of for
            if (gridsaroundtaken == false)
            {
                squadbasetype = 0;
                func_createapiratebase(squadbasetype, xsector, ysector);
            } // end if
        } // end if
    } // End of the function
    function func_createapiratebase(squadbasetype, xsector, ysector)
    {
        function func_setuppiratebase(squadbasetype, baseidname)
        {
            guntypes = _root.squadbaseinfo[squadbasetype][2][1];
            this.newbaseGuns = new XML();
            this.newbaseGuns.load(_root.pathtoaccounts + "squadbases.php?mode=changegun&baseid=" + baseidname + "&gunone=" + func_piratehardpoint(squadbasetype) + "&guntwo=" + func_piratehardpoint(squadbasetype) + "&gunthree=" + func_piratehardpoint(squadbasetype) + "&gunfour=" + func_piratehardpoint(squadbasetype));
            this.newbaseShield = new XML();
            this.newbaseShield.load(_root.pathtoaccounts + "squadbases.php?mode=newshield&baseid=" + baseidname + "&newshield=" + _root.squadbaseinfo[squadbasetype][1][3]);
            this.newbaseStruct = new XML();
            this.newbaseStruct.load(_root.pathtoaccounts + "squadbases.php?mode=repairbase&baseid=" + baseidname + "&struct=" + _root.squadbaseinfo[squadbasetype][3][1] + "&maxstruct=" + _root.squadbaseinfo[squadbasetype][3][1]);
            this.newbaseMissile = new XML();
            this.newbaseMissile.load(_root.pathtoaccounts + "squadbases.php?mode=missileshot&baseid=" + baseidname + "&shots=-" + 10000);
        } // End of the function
        xcoord = xsector * _root.sectorinformation[1][0] - Math.round(_root.sectorinformation[1][0] / 2);
        ycoord = ysector * _root.sectorinformation[1][1] - Math.round(_root.sectorinformation[1][1] / 2);
        baseidname = "*AI" + (Math.round(Math.random() * 89) + 10);
        currentsystem = _root.playershipstatus[5][1];
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadbases.php?mode=createbase&baseid=" + baseidname + "&system=" + currentsystem + "&xcoord=" + xcoord + "&ycoord=" + ycoord);
        newsquadVars.onLoad = function (success)
        {
            datatosend = "SA~NEWS`SB`CREATED`" + baseidname + "`" + basetype + "`" + xcoord + "`" + ycoord + "~";
            _root.mysocket.send(datatosend);
            func_setuppiratebase(squadbasetype, baseidname);
        };
    } // End of the function
    function func_piratehardpoint(squadbasetype)
    {
        guntype = _root.squadbaseinfo[squadbasetype][2][1] - Math.round(Math.random() * 2);
        return (guntype);
    } // End of the function
    function checkforrepstarbases()
    {
        repairedabase = false;
        newsmessage = "";
        for (ww = 0; ww < _root.starbaselocation.length; ww++)
        {
            currenttime = getTimer();
            if (_root.starbaselocation[ww][5] != "ACTIVE")
            {
                rerpairtime = Number(_root.starbaselocation[ww][5]);
                if (rerpairtime < currenttime && !isNaN(rerpairtime))
                {
                    repairedabase = true;
                    _root.starbaselocation[ww][5] = "ACTIVE";
                    newsmessage = newsmessage + ("Starbase " + _root.starbaselocation[ww][0] + " has been repaired. ");
                } // end if
            } // end if
        } // end of for
        if (repairedabase == true)
        {
            _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
            xsector = _root.playershipstatus[6][0];
            ysector = _root.playershipstatus[6][1];
            _root.gamedisplayarea.keyboardscript.sectoritemsdisp(xsector, ysector);
        } // end if
        func_checkforpriceresets();
    } // End of the function
    randomeventcheckinterval = 200000;
    nextcheckat = getTimer() + 60000;
    minpiratebaseodds = 0.850000;
    maxpiratebases = 3;
    maxtriesforpiratebase = 50;
    setInterval(checkforrepstarbases, 20000);
    lastime = getTimer();
}
