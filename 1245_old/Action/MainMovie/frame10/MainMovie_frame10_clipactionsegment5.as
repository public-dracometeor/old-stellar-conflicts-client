﻿// Action script...

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (load)
{
    function settonotarget()
    {
        currenttargetname = "No Target";
        currenttargetbounty = "";
        removeMovieClip (targetship);
        lasttarget = -1;
        _root.targetinfo[0] = "None";
        _root.targetinfo[1] = 0;
        _root.targetinfo[2] = 0;
        _root.targetinfo[3] = "";
        lasttargetshiptype = "";
        currenttargetdist = "";
        targetpointer._visible = false;
        _root.targetinfo[10] = "enemy";
        shielddisp = "";
        structdisp = "";
    } // End of the function
    function checkorsettarget()
    {
        func_setlocalnpcs();
        if (_root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length > 0)
        {
            if (lasttarget == -1)
            {
                lasttarget = 0;
                if (lasttarget < _root.otherplayership.length)
                {
                    currenttargetname = _root.otherplayership[lasttarget][10];
                    currenttargetshiptype = _root.otherplayership[lasttarget][11];
                    currenttargetbounty = _root.otherplayership[lasttarget][12];
                    targettype = "other";
                    pilotno = _root.otherplayership[lasttarget][0];
                    func_sendstatuscheck(pilotno);
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length)
                {
                    aitargetnumber = lasttarget - int(_root.otherplayership.length);
                    currenttargetname = _root.aishipshosted[aitargetnumber][7];
                    currenttargetshiptype = _root.aishipshosted[aitargetnumber][6];
                    currenttargetbounty = _root.aishipshosted[aitargetnumber][10];
                    targettype = "hosted";
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length)
                {
                    neuttargetnumber = lasttarget - _root.otherplayership.length - _root.aishipshosted.length;
                    currenttargetname = neutralaiarray[neuttargetnumber][11];
                    currenttargetshiptype = neutralaiarray[neuttargetnumber][4];
                    currenttargetbounty = "N/A";
                    targettype = "neutral";
                    attachtargetship(currenttargetshiptype);
                }
                else
                {
                    currentsectioritemnumber = lasttarget - (_root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length);
                    currenttargetname = _root.sectoritemsfortarget[currentsectioritemnumber][0];
                    currenttargetbounty = "UNKNOWN";
                    targettype = "sectoritem";
                    removeMovieClip (targetship);
                } // end else if
                _root.targetinfo[0] = currenttargetname;
                attachtargetship(currenttargetshiptype);
            }
            else if (currenttargetname != _root.otherplayership[lasttarget][10] || currenttargetname != _root.aishipshosted[aitargetnumber][7] || currenttargetname != neutralaiarray[neuttargetnumber][11] || currenttargetname != _root.sectoritemsfortarget[currentsectioritemnumber][0])
            {
                namestillexists = false;
                for (jjcq = 0; jjcq < _root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length; jjcq++)
                {
                    if (jjcq < _root.otherplayership.length)
                    {
                        if (currenttargetname == _root.otherplayership[jjcq][10])
                        {
                            namestillexists = true;
                            lasttarget = jjcq;
                        } // end if
                        continue;
                    } // end if
                    if (jjcq < _root.otherplayership.length + _root.aishipshosted.length)
                    {
                        aitargetnumber = jjcq - _root.otherplayership.length;
                        if (currenttargetname == _root.aishipshosted[aitargetnumber][7])
                        {
                            namestillexists = true;
                            lasttarget = jjcq;
                            jjcq = 99999;
                        } // end if
                        continue;
                    } // end if
                    if (jjcq < _root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length)
                    {
                        neuttargetnumber = jjcq - _root.otherplayership.length - _root.aishipshosted.length;
                        if (String(currenttargetname) == String(neutralaiarray[neuttargetnumber][11]))
                        {
                            namestillexists = true;
                            lasttarget = jjcq;
                            jjcq = 99999;
                        } // end if
                        continue;
                    } // end if
                    currentsectioritemnumber = jjcq - _root.otherplayership.length - _root.aishipshosted.length - neutralaiarray.length;
                    if (currenttargetname == _root.sectoritemsfortarget[currentsectioritemnumber][0])
                    {
                        namestillexists = true;
                        lasttarget = jjcq;
                        jjcq = 99999;
                    } // end if
                } // end of for
                if (namestillexists != true)
                {
                    settonotarget();
                    currentinterval = 9999;
                } // end if
            } // end else if
            if (_root.targetinfo[0] != "None")
            {
                if (targettype == "other")
                {
                    _root.targetinfo[1] = _root.otherplayership[lasttarget][1];
                    _root.targetinfo[2] = _root.otherplayership[lasttarget][2];
                    _root.targetinfo[5] = _root.otherplayership[lasttarget][5];
                    _root.targetinfo[6] = _root.otherplayership[lasttarget][4];
                    _root.targetinfo[7] = "otherplayership";
                    _root.targetinfo[8] = lasttarget;
                    _root.gamedisplayarea["otherplayership" + _root.otherplayership[lasttarget][0]].attachMovie("shiptargeted", "shiptargeted", 52);
                    set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[lasttarget][0] + ".shiptargeted.shipid", _root.targetinfo[0]);
                    targetcursorColor = new Color("_root.gamedisplayarea.otherplayership" + _root.otherplayership[lasttarget][0] + ".shiptargeted");
                    if (_root.playershipstatus[5][2] != _root.otherplayership[lasttarget][13] || _root.playershipstatus[5][2] == "N/A")
                    {
                        _root.targetinfo[10] = "enemy";
                        targetcursorColor.setRGB(16711680);
                    }
                    else
                    {
                        _root.targetinfo[10] = "friend";
                        targetcursorColor.setRGB(16776960);
                    } // end else if
                    if (_root.otherplayership[lasttarget][16] == "C" || _root.otherplayership[lasttarget][16] == "S")
                    {
                        func_targetlocation(true);
                    }
                    else
                    {
                        func_targetlocation(false);
                    } // end if
                } // end else if
                if (targettype == "hosted")
                {
                    _root.targetinfo[1] = _root.aishipshosted[aitargetnumber][1];
                    _root.targetinfo[2] = _root.aishipshosted[aitargetnumber][2];
                    _root.targetinfo[5] = _root.aishipshosted[aitargetnumber][3];
                    _root.targetinfo[6] = _root.aishipshosted[aitargetnumber][4];
                    _root.targetinfo[7] = "aiship";
                    _root.targetinfo[8] = aitargetnumber;
                    _root.targetinfo[10] = "enemy";
                    _root.gamedisplayarea["aiship" + _root.aishipshosted[aitargetnumber][0]].attachMovie("shiptargeted", "shiptargeted", 52);
                    set("_root.gamedisplayarea.aiship" + _root.aishipshosted[aitargetnumber][0] + ".shiptargeted.shipid", _root.targetinfo[0]);
                    targetcursorColor = new Color("_root.gamedisplayarea.aiship" + _root.aishipshosted[aitargetnumber][0] + ".shiptargeted");
                    targetcursorColor.setRGB(16711680);
                    func_targetlocation(false);
                } // end if
                if (targettype == "neutral")
                {
                    labell = neutralaiarray[neuttargetnumber][10];
                    _root.targetinfo[1] = _root.gamedisplayarea[labell].xcoord;
                    _root.targetinfo[2] = _root.gamedisplayarea[labell].ycoord;
                    _root.targetinfo[5] = _root.gamedisplayarea[labell].currentvelocity;
                    _root.targetinfo[6] = _root.gamedisplayarea[labell].rotation;
                    _root.targetinfo[7] = "neutral";
                    _root.targetinfo[8] = lasttarget;
                    _root.gamedisplayarea[labell].attachMovie("shiptargeted", "shiptargeted", 52);
                    set("_root.gamedisplayarea." + labell + ".shiptargeted.shipid", _root.targetinfo[0]);
                    targetcursorColor = new Color("_root.gamedisplayarea." + labell + ".shiptargeted");
                    targetcursorColor.setRGB(16711680);
                    func_targetlocation(false);
                } // end if
                if (targettype == "sectoritem")
                {
                    _root.targetinfo[1] = _root.sectoritemsfortarget[currentsectioritemnumber][1];
                    _root.targetinfo[2] = _root.sectoritemsfortarget[currentsectioritemnumber][2];
                    _root.targetinfo[5] = 0;
                    _root.targetinfo[6] = 0;
                    _root.targetinfo[7] = "sectoritem";
                    _root.targetinfo[8] = currentsectioritemnumber;
                    _root.targetinfo[10] = "enemy";
                    if (_root.playershipstatus[5][10] == _root.targetinfo[0])
                    {
                        _root.targetinfo[10] = "friend";
                    }
                    else if (_root.teamdeathmatch == true)
                    {
                        if (_root.targetinfo[0] == _root.teambases[_root.playershipstatus[5][2]][0])
                        {
                            _root.targetinfo[10] = "friend";
                        } // end if
                    } // end else if
                    func_targetlocation(false);
                    removeMovieClip (targetship);
                } // end if
            } // end if
            if (currenttargetbounty != _root.otherplayership[lasttarget][12] && targettype == "other")
            {
                currenttargetbounty = _root.otherplayership[lasttarget][12];
            }
            else if (currenttargetbounty != _root.aishipshosted[aitargetnumber][10] && targettype == "hosted")
            {
                currenttargetbounty = _root.aishipshosted[aitargetnumber][10];
            } // end if
        } // end else if
        if (_root.targetinfo[10] == "enemy")
        {
            targetName.setRGB(16711680);
        }
        else
        {
            targetName.setRGB(16776960);
        } // end else if
        if (targettype == "other")
        {
            if (_root.otherplayership[lasttarget][40] == null)
            {
                pilotno = _root.otherplayership[lasttarget][0];
                func_sendstatuscheck(pilotno);
                _root.otherplayership[lasttarget][40] = "waiting";
                structdisp = "Waiting";
            }
            else if (_root.otherplayership[lasttarget][40] != "waiting")
            {
                currentshield = Math.round(_root.otherplayership[lasttarget][41] + (getTimer() - _root.otherplayership[lasttarget][44]) * _root.otherplayership[lasttarget][42] / 1000);
                _root.otherplayership[lasttarget][44] = getTimer();
                if (currentshield > _root.otherplayership[lasttarget][43])
                {
                    currentshield = _root.otherplayership[lasttarget][43];
                } // end if
                _root.otherplayership[lasttarget][41] = currentshield;
                shielddisp = currentshield;
                structdisp = _root.otherplayership[lasttarget][40];
            } // end else if
        }
        else if (targettype == "hosted")
        {
            if (_root.aishipshosted[aitargetnumber][20] > 0)
            {
                shielddisp = Math.round(_root.aishipshosted[aitargetnumber][22]);
                structdisp = Math.round(_root.aishipshosted[aitargetnumber][20]);
                if (structdisp < 0)
                {
                    structdisp = 0;
                } // end if
            } // end if
        }
        else if (targettype == "sectoritem")
        {
            shielddisp = Math.round(_root.sectoritemsfortarget[currentsectioritemnumber][5]);
            structdisp = Math.round(_root.sectoritemsfortarget[currentsectioritemnumber][6]);
            if (structdisp < 0)
            {
                structdisp = 0;
            } // end if
        }
        else if (targettype == "neutral")
        {
            labell = neutralaiarray[neuttargetnumber][10];
            shielddisp = Math.round(_root.gamedisplayarea[labell].currentshipshield);
            structdisp = Math.round(_root.gamedisplayarea[labell].shiphealth);
            if (structdisp < 0)
            {
                structdisp = 0;
            } // end else if
        } // end else if
    } // End of the function
    function func_targetlocation(iscloakorstealth)
    {
        if (iscloakorstealth == false)
        {
            playersxcoord = _root.shipcoordinatex;
            playersycoord = _root.shipcoordinatey;
            currenttargetdist = _root.targetinfo[3] = Math.round(Math.sqrt((playersxcoord - _root.targetinfo[1]) * (playersxcoord - _root.targetinfo[1]) + (playersycoord - _root.targetinfo[2]) * (playersycoord - _root.targetinfo[2])));
            playeranglefromship = 180 - Math.atan2(_root.targetinfo[1] - playersxcoord, _root.targetinfo[2] - playersycoord) / 0.017453;
            targetpointer._visible = true;
            targetpointer._rotation = playeranglefromship;
            playeranglefromship = playeranglefromship - _root.playershipfacing;
            if (playeranglefromship > 360)
            {
                playeranglefromship = playeranglefromship - 360;
            } // end if
            if (playeranglefromship < 0)
            {
                playeranglefromship = playeranglefromship + 360;
            } // end if
            _root.targetinfo[4] = Math.round(playeranglefromship);
        }
        else if (iscloakorstealth == true)
        {
            targetpointer._visible = false;
            currenttargetdist = "HIDDEN";
            _root.targetinfo[3] = 9999999999.000000;
        } // end else if
    } // End of the function
}
