﻿// Action script...

// [onClipEvent of sprite 1498 in frame 10]
onClipEvent (enterFrame)
{
    function checkforloadingnewai()
    {
        oddsthataiwillappear = 16;
        checktheodds = Math.random() * 100;
        if (checktheodds < oddsthataiwillappear)
        {
            maxaitoappear = 2;
            aitoappear = Math.round(Math.random() * (maxaitoappear - 1)) + 1;
            _root.bringinaiships(aitoappear, "RANDOM");
        } // end if
    } // End of the function
    function othermissilefiremovement()
    {
        cc = 0;
        _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "otherfire", "othermissilefire" + _root.othermissilefire[lastvar][8], 3000 + _root.othermissilefire[lastvar][8]);
        while (cc < _root.othermissilefire.length)
        {
            if (_root.othermissilefire[cc][5] < curenttime)
            {
                removeMovieClip ("othermissilefire" + _root.othermissilefire[cc][8]);
                _root.othergunfire.splice(cc, 1);
                --i;
            } // end if
            ++cc;
        } // end while
    } // End of the function
    function checkothershiptimouts()
    {
        for (qz = 0; qz < _root.otherplayership.length; qz++)
        {
            if (_root.otherplayership[qz][6] < _root.curenttime)
            {
                removeMovieClip ("_root.gamedisplayarea.otherplayership" + _root.otherplayership[qz][0]);
                removeMovieClip ("_root.gamedisplayarea.nametag" + _root.otherplayership[qz][0]);
                _root.otherplayership.splice(qz, 1);
            } // end if
        } // end of for
    } // End of the function
    function movementofanobjectwiththrust()
    {
        if (relativefacing == 0)
        {
            ymovement = -velocity;
            xmovement = 0;
        } // end if
        if (velocity != 0)
        {
            this.relativefacing = Math.round(this.relativefacing);
            ymovement = Math.round(-velocity * _root.cosines[this.relativefacing]);
            xmovement = Math.round(velocity * _root.sines[this.relativefacing]);
        }
        else
        {
            ymovement = 0;
            xmovement = 0;
        } // end else if
    } // End of the function
    function getaiinformation()
    {
        aiinformation = "";
        for (zza = 0; zza < _root.aishipshosted.length; zza++)
        {
            aiinformation = aiinformation + ("AI`" + _root.aishipshosted[zza][0] + "`" + Math.round(_root.aishipshosted[zza][1]) + "`" + Math.round(_root.aishipshosted[zza][2]) + "`" + _root.aishipshosted[zza][3] + "`" + Math.round(_root.aishipshosted[zza][4]) + "`" + _root.aishipshosted[zza][5] + "~");
        } // end of for
    } // End of the function
    currenttimechangeratio = _root.currenttimechangeratio;
    curenttime = getTimer();
    if (_root.afterburnerinuse != lastafterburnerinuse)
    {
        _root.keywaspressed = true;
        lastafterburnerinuse = _root.afterburnerinuse;
    } // end if
    if (curenttime >= timeintervalcheck || _root.keywaspressed == true)
    {
        currentsectoris = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
        lastsectoris = _root.playershipstatus[6][2] + "`" + _root.playershipstatus[6][3];
        if (lastsectoris == currentsectoris)
        {
            lastsectoris = "FALSE";
        }
        else if (lastsectoris != currentsectoris)
        {
            _root.playershipstatus[6][2] = _root.playershipstatus[6][0];
            _root.playershipstatus[6][3] = _root.playershipstatus[6][1];
        } // end else if
        _root.keywaspressed = false;
        if (lastsectoris != "FALSE" || _root.otherplayership.length > 0 || curenttime >= timeintervalcheck)
        {
            timeintervalcheck = curenttime + updateinterval;
            currentkeyedshipmovement();
            getaiinformation();
            information = "PI`" + lastsectoris + "`" + currentsectoris + "~PI" + "`" + _root.playershipstatus[3][0] + "`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + Math.round(_root.playershipfacing) + "`" + Math.round(_root.playershipvelocity) + "`" + keysbeingpressed + "`" + _root.playershipstatus[5][15] + "`" + _root.func_globaltimestamp() + "~" + _root.gunfireinformation + _root.ainformationtosend;
            _root.gunfireinformation = "";
            _root.ainformationtosend = "";
            if (_root.playershipstatus[5][4] != "dead")
            {
                _root.mysocket.send(information);
            } // end if
        } // end if
    } // end if
    if (_root.gunfireinformation != "" || _root.ainformationtosend != "")
    {
        if (_root.otherplayership.length > 0)
        {
            currentsectoris = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
            information = "PI`FALSE`~" + _root.gunfireinformation + _root.ainformationtosend;
            _root.gunfireinformation = "";
            _root.mysocket.send(information);
        } // end if
    } // end if
    _root.gunfireinformation = "";
    _root.ainformationtosend = "";
    if (_root.teamdeathmatch != true && _root.isgameracingzone != true)
    {
        if (curenttime > lastaichecktime && func_isplayeronmap())
        {
            lastaichecktime = curenttime + aicheckinterval;
            if (_root.aishipshosted.length < 1 && _root.isaiturnedon == true && _root.hostileplayerships == false)
            {
                checkforloadingnewai();
            } // end if
        } // end if
    } // end if
    checkothershiptimouts();
}
