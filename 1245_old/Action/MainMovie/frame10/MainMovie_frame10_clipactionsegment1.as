﻿// Action script...

// [onClipEvent of sprite 1525 in frame 10]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (_root.gamechatinfo[2][2] == true)
    {
        if (_root.gamechatinfo[2][3] < currenttime)
        {
            _root.gamechatinfo[2][2] = false;
            _root.gamechatinfo[2][3] = 0;
            colourtouse = _root.systemchattextcolor;
            message = "HOST: You Are No Longer Muted";
            _root.enterintochat(message, colourtouse);
        } // end if
    } // end if
    if (_root.teamdeathmatch != true && _root.isgameracingzone != true)
    {
        timelapsed = currenttime - lastime;
        lastime = currenttime;
        if (nextcheckat < getTimer())
        {
            _root.func_checkfortoomuchaddedandset();
            nextcheckat = getTimer() + randomeventcheckinterval;
            whattospawn = Math.round(Math.random() * 5);
            if (whattospawn == 0)
            {
                spawnchance = Math.random();
                func_randompiratebase(spawnchance);
            }
            else if (whattospawn == 1)
            {
                spawnchance = Math.random();
                func_spawnameteor(spawnchance);
            }
            else if (whattospawn == 2 || whattospawn == 3)
            {
                spawnchance = Math.random();
                if (spawnchance > 0.300000)
                {
                    func_creationofaship();
                } // end if
            }
            else
            {
                spawnchance = Math.random();
                if (spawnchance > 0.300000)
                {
                    func_createapricechange();
                } // end else if
            } // end else if
        } // end else if
        if (_root.incomingmeteor.length > 0)
        {
            timechange = timelapsed / 1000;
            for (ii = 0; ii < _root.incomingmeteor.length; ii++)
            {
                if (currenttime > _root.incomingmeteor[ii][12])
                {
                    _root.func_meteorhitbase(_root.incomingmeteor[ii][0]);
                    _root.incomingmeteor.splice(ii, 1);
                    --ii;
                    continue;
                } // end if
                _root.incomingmeteor[ii][1] = _root.incomingmeteor[ii][1] + _root.incomingmeteor[ii][10] * timechange;
                _root.incomingmeteor[ii][2] = _root.incomingmeteor[ii][2] + _root.incomingmeteor[ii][11] * timechange;
                if (_root.incomingmeteor[ii][9] != false)
                {
                    if (_root.incomingmeteor[ii][9] < currenttime)
                    {
                        datatosend = "MET`DM`" + _root.incomingmeteor[ii][0] + "`" + _root.incomingmeteor[ii][6] + "`" + _root.playershipstatus[3][0] + "`" + _root.errorchecknumber + "~";
                        _root.mysocket.send(datatosend);
                        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
                        _root.incomingmeteor[ii][9] = false;
                    } // end if
                } // end if
            } // end of for
        } // end if
        if (_root.neutralships.length > 0)
        {
            for (jjj = 0; jjj < _root.neutralships.length; jjj++)
            {
                if (_root.neutralships[jjj][7] < currenttime)
                {
                    func_removeaship(_root.neutralships[jjj][10]);
                } // end if
            } // end of for
        } // end if
    } // end if
}
