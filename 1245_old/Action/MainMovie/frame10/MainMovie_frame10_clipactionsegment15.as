﻿// Action script...

// [onClipEvent of sprite 1407 in frame 10]
onClipEvent (enterFrame)
{
    lasttime = curenttime;
    curenttime = getTimer();
    currenttimechangeratio = (curenttime - lasttime) * 0.001000;
    _root.currenttimechangeratio = currenttimechangeratio;
    _root.curenttime = curenttime;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    ++currentframe;
    if (currentframe > updateinterval)
    {
        currentframe = 0;
        adjuststars(playersxcoord, playersycoord);
    } // end if
    this.gamebackground._x = -playersxcoord;
    this.gamebackground._y = -playersycoord;
}
