﻿// Action script...

// [onClipEvent of sprite 1498 in frame 10]
onClipEvent (load)
{
    function func_isplayeronmap()
    {
        onmap = false;
        playsxsector = _root.playershipstatus[6][0];
        playsysector = _root.playershipstatus[6][1];
        if (playsxsector >= 0 && playsxsector <= _root.sectorinformation[0][0])
        {
            if (playsysector >= 0 && playsysector <= _root.sectorinformation[0][1])
            {
                onmap = true;
            } // end if
        } // end if
        return (onmap);
    } // End of the function
    this.shipcoordinatex = _root.shipcoordinatex;
    this.shipcoordinatey = _root.shipcoordinatey;
    xwidthofasector = _root.sectorinformation[1][0];
    ywidthofasector = _root.sectorinformation[1][1];
    xsector = Math.ceil(shipcoordinatex / xwidthofasector);
    ysector = Math.ceil(shipcoordinatey / ywidthofasector);
    this._visible = false;
    updateinterval = 3000;
    timeintervalcheck = getTimer() + updateinterval;
    pathtosectorfile = "./systems/system" + _root.playershipstatus[5][1] + "/";
    _root.otherplayership = new Array();
    currentotherplayshot = 0;
    justenteredgame = true;
    this.gameareawidth = _root.gameareawidth;
    this.gameareaheight = _root.gameareaheight;
    lowestvolume = 50;
    lowestvolumelength = Math.sqrt(gameareawidth / 2 * (gameareawidth / 2) + gameareaheight / 2 * (gameareaheight / 2));
    currentsectoris = xsector + "`" + ysector;
    lastsectoris = "NEW`";
    errorinformation = "PI`" + lastsectoris + "`" + currentsectoris + "`" + _root.errorchecknumber + "~";
    information = errorinformation + "PI" + "`" + _root.playershipstatus[3][0] + "`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + Math.round(_root.playershipfacing) + "`" + Math.round(_root.playershipvelocity) + "`" + keysbeingpressed + "`" + _root.playershipstatus[5][15] + Number.NaN;
    _root.mysocket.send(information);
    _root.errorcheckedmessage(errorinformation, _root.errorchecknumber);
    lastsectoris = currentsectoris;
    _root.ainformationtosend = "";
    aicheckinterval = 18000;
    lastaichecktime = getTimer() + aicheckinterval;
    timetoredrawship = 7000;
    curenttime = getTimer();
}
