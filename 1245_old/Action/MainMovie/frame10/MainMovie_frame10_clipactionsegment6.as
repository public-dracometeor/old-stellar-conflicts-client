﻿// Action script...

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (enterFrame)
{
    ++currentinterval;
    if (currentinterval > intervalwait)
    {
        currentinterval = 0;
        func_setlocalnpcs();
        if (_root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length < 1)
        {
            if (lasttarget > -1)
            {
                settonotarget();
                lasttarget = -1;
            } // end if
        }
        else
        {
            checkorsettarget();
        } // end if
    } // end else if
}
