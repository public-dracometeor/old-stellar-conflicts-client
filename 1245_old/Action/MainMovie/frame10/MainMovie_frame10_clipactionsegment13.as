﻿// Action script...

// [onClipEvent of sprite 1495 in frame 10]
onClipEvent (load)
{
    function displaystats()
    {
        if (lastshownshield != _root.playershipstatus[2][1])
        {
            lastshownshield = _root.playershipstatus[2][1];
            this.shield = Math.round(lastshownshield);
            this.shieldbar.showshield(shield);
        } // end if
        if (lastshownenergy != _root.playershipstatus[1][1])
        {
            lastshownenergy = _root.playershipstatus[1][1];
            this.energy = Math.round(lastshownenergy);
            this.energybar.showenergy(energy);
        } // end if
        currentstructure = _root.playershipstatus[2][5];
        if (lastshownstructure != currentstructure)
        {
            this.structure = Math.round(currentstructure);
            this.structurebar.structure = laststructure;
            laststructure = currentstructure;
        } // end if
        if (_root.isplayeremp)
        {
            downtime = Math.ceil((_root.playerempend - currenttime) / 1000);
            this.energy = "OFFLINE " + downtime;
            _root.playershipstatus[1][1] = 0;
            this.shield = "OFFLINE " + downtime;
            _root.playershipstatus[2][1] = 0;
        } // end if
        if (_root.playershipstatus[5][20] == true)
        {
            timeleft = (_root.playershipstatus[5][21] - getTimer()) / 1000;
            if (timeleft < 0)
            {
                _root.playershipstatus[5][20] = false;
                this.shield = Math.round(lastshownshield);
            }
            else
            {
                this.shield = "INVULNERABLE " + Math.round(timeleft);
            } // end if
        } // end else if
    } // End of the function
    playercurrentenergygenerator = _root.playershipstatus[1][0];
    energyrechargerate = _root.energygenerators[playercurrentenergygenerator][0];
    playercurrentenergyenergycapacitor = _root.playershipstatus[1][5];
    maxenergy = _root.energycapacitors[playercurrentenergyenergycapacitor][0];
    playercurrentshieldgenerator = _root.playershipstatus[2][0];
    energydrainedbyshieldgenatfull = _root.shieldgenerators[playercurrentshieldgenerator][3];
    shieldrechargerate = _root.shieldgenerators[playercurrentshieldgenerator][1];
    maxshieldstrength = _root.shieldgenerators[playercurrentshieldgenerator][0];
    baseshieldgendrain = _root.shieldgenerators[playercurrentshieldgenerator][2];
    lastshieldstrength = 0;
    lastenergyamount = 0;
    displayupdaterate = 3;
    currentdisplayframe = 0;
    showupdaterate = 5;
    currentshowupdate = 0;
    lasttimerrefreshed = getTimer();
    _root.isplayeremp = false;
    _root.playerempend = 0;
    displaystats();
}
