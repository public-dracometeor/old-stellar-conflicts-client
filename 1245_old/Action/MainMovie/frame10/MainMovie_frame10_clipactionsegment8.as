﻿// Action script...

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (keyDown)
{
    if (Key.isDown(84) && _root.gamechatinfo[4] == false)
    {
        if (istkeypressed == false)
        {
            func_setlocalnpcs();
            if (_root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length > 0)
            {
                ++lasttarget;
                if (lasttarget >= _root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length)
                {
                    lasttarget = 0;
                } // end if
                if (lasttarget < _root.otherplayership.length)
                {
                    currenttargetname = _root.otherplayership[lasttarget][10];
                    currenttargetshiptype = _root.otherplayership[lasttarget][11];
                    currenttargetbounty = _root.otherplayership[lasttarget][12];
                    targettype = "other";
                    attachtargetship(currenttargetshiptype);
                    pilotno = _root.otherplayership[lasttarget][0];
                    func_sendstatuscheck(pilotno);
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length)
                {
                    aitargetnumber = lasttarget - int(_root.otherplayership.length);
                    currenttargetname = _root.aishipshosted[aitargetnumber][7];
                    currenttargetshiptype = _root.aishipshosted[aitargetnumber][6];
                    currenttargetbounty = "400";
                    targettype = "hosted";
                    attachtargetship(currenttargetshiptype);
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length)
                {
                    neuttargetnumber = lasttarget - _root.otherplayership.length - _root.aishipshosted.length;
                    currenttargetname = neutralaiarray[neuttargetnumber][11];
                    currenttargetshiptype = neutralaiarray[neuttargetnumber][4];
                    currenttargetbounty = "N/A";
                    targettype = "neutral";
                    attachtargetship(currenttargetshiptype);
                }
                else
                {
                    currentsectioritemnumber = lasttarget - (_root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length);
                    currenttargetname = _root.sectoritemsfortarget[currentsectioritemnumber][0];
                    currenttargetbounty = "UNKNOWN";
                    targettype = "sectoritem";
                    removeMovieClip (targetship);
                } // end else if
                _root.targetinfo[0] = currenttargetname;
                checkorsettarget();
            }
            else
            {
                settonotarget();
            } // end if
        } // end else if
        istkeypressed = true;
    }
    else
    {
        istkeypressed = false;
    } // end else if
}
