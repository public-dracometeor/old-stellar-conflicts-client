﻿// Action script...

// [onClipEvent of sprite 1495 in frame 10]
onClipEvent (enterFrame)
{
    ++currentshowupdate;
    if (currentshowupdate > showupdaterate)
    {
        currentshowupdate = 0;
        displaystats();
    } // end if
    if (_root.playershipstatus[5][4] == "dead")
    {
        _root.playershipstatus[2][1] = 0;
        _root.playershipstatus[1][1] = 0;
        _root.playershipstatus[2][5] = 0;
        this.flagwindow._visible = false;
        displaystats();
    }
    else
    {
        ++currentdisplayframe;
        if (currentdisplayframe > displayupdaterate)
        {
            currentdisplayframe = 0;
            currenttime = getTimer();
            currenttimechangeratio = (currenttime - lasttimerrefreshed) / 1000;
            lasttimerrefreshed = currenttime;
            if (_root.isplayeremp)
            {
                currentshieldstrength = 0;
                currentenergy = 0;
                if (_root.playerempend < currenttime)
                {
                    _root.isplayeremp = false;
                } // end if
            }
            else
            {
                currentshieldstrength = _root.playershipstatus[2][1];
                if (currentshieldstrength < 0)
                {
                    currentshieldstrength = 0;
                } // end if
                currentshieldstrength = currentshieldstrength + shieldrechargerate * currenttimechangeratio;
                if (_root.playershipstatus[2][2] == "OFF")
                {
                    currentshieldstrength = 0;
                }
                else if (_root.playershipstatus[2][2] == "HALF")
                {
                    if (currentshieldstrength > Math.round(maxshieldstrength / 2))
                    {
                        currentshieldstrength = Math.round(maxshieldstrength / 2);
                    } // end if
                }
                else if (currentshieldstrength > maxshieldstrength)
                {
                    currentshieldstrength = maxshieldstrength;
                } // end else if
                _root.playershipstatus[2][1] = currentshieldstrength;
                currentenergy = _root.playershipstatus[1][1];
                currentenergy = currentenergy + _root.energygenerators[playercurrentenergygenerator][0] * currenttimechangeratio;
                if (_root.playershipstatus[2][2] != "OFF")
                {
                    eval(currentenergy = currentenergy - baseshieldgendrain * currenttimechangeratio)(currentenergy = currentenergy - currentshieldstrength / maxshieldstrength * (energydrainedbyshieldgenatfull * currenttimechangeratio));
                } // end if
                if (currentenergy < 0)
                {
                    currentenergy = 0;
                } // end if
                if (currentenergy > maxenergy)
                {
                    currentenergy = maxenergy;
                } // end if
                _root.playershipstatus[1][1] = currentenergy;
            } // end else if
            if (lastshieldstrength != currentshieldstrength)
            {
                lastshieldstrength = currentshieldstrength;
            } // end if
            if (lastenergyamount != currentenergy)
            {
                lastenergyamount = currentenergy;
            } // end if
        } // end if
    } // end else if
}
