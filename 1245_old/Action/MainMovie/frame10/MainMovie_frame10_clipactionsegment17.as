﻿// Action script...

// [onClipEvent of sprite 1407 in frame 10]
onClipEvent (load)
{
    function func_flagcontrols(method, xcoord, ycoord)
    {
        if (method == "ADD")
        {
            this.gamebackground.attachMovie("kingflaggameicon", "kflag", 8001);
            this.gamebackground.kflag._x = xcoord;
            this.gamebackground.kflag._y = ycoord;
        }
        else
        {
            removeMovieClip (this.gamebackground.kflag);
        } // end else if
    } // End of the function
    function func_drawneutralship(currentno)
    {
        globalshipname = _root.neutralships[currentno][10];
        this.attachMovie("shiptypeaineutral", globalshipname, 600 + _root.neutralships[currentno][13]);
        this[globalshipname].globalshipname = globalshipname;
        this[globalshipname]._x = -1000;
        this[globalshipname]._y = -1000;
    } // End of the function
    function func_removeneutralship(currentno)
    {
        globalshipname = _root.neutralships[currentno][10];
        removeMovieClip (this[globalshipname]);
    } // End of the function
    function func_meteordraw(method, meteorid)
    {
        if (method == "ADD")
        {
            ++_root.currentmeteordrawlevel;
            if (_root.currentmeteordrawlevel > _root.maxmeteordrawlevel)
            {
                _root.currentmeteordrawlevel = 0;
            } // end if
            meteorlvl = _root.currentmeteordrawlevel;
            location = null;
            for (qq = 0; qq < _root.incomingmeteor.length; qq++)
            {
                if (meteorid == _root.incomingmeteor[qq][0])
                {
                    location = qq;
                    break;
                } // end if
            } // end of for
            if (location != null)
            {
                meteortype = _root.incomingmeteor[location][7];
                velocity = _root.destroyingmeteor[meteortype][0];
                relativefacing = _root.incomingmeteor[location][3];
                movementofanobjectwiththrust();
                ymovement = -velocity * _root.cosines[this.relativefacing];
                xmovement = velocity * _root.sines[this.relativefacing];
                this.gamebackground.attachMovie("meteorscript", "meteor" + meteorid, 8050 + meteorlvl);
                this.gamebackground["meteor" + meteorid].meteorid = meteorid;
                this.gamebackground["meteor" + meteorid]._x = _root.incomingmeteor[location][1];
                this.gamebackground["meteor" + meteorid]._y = _root.incomingmeteor[location][2];
                this.gamebackground["meteor" + meteorid].xmovement = xmovement;
                this.gamebackground["meteor" + meteorid].ymovement = ymovement;
                this.gamebackground["meteor" + meteorid].meteortype = meteortype;
                this.gamebackground["meteor" + meteorid].meteorlife = Number(_root.incomingmeteor[location][6]);
            } // end if
        }
        else
        {
            removeMovieClip (this.gamebackground["meteor" + meteorid]);
        } // end else if
    } // End of the function
    function makebackgroundstars()
    {
        backgroundstar = new Array();
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        playerslastxcoord = playersxcoord;
        playerslastycoord = playersycoord;
        for (i = 0; i < totalstars; i++)
        {
            backgroundstar[i] = new Array(2);
            backgroundstar[i][0] = random(doublegameareawidth) - Math.round(doublegameareawidth / 2) + playersxcoord;
            backgroundstar[i][1] = random(doublegameareaheight) - Math.round(doublegameareaheight / 2) + playersycoord;
            this.gamebackground.attachMovie("backgroundstar" + Math.round(Math.random() * _root.totalstartypes), "backgroundstar" + String(i), i + 500);
            setProperty("gamebackground.backgroundstar" + i, _x, backgroundstar[i][0]);
            setProperty("gamebackground.backgroundstar" + i, _y, backgroundstar[i][1]);
        } // end of for
    } // End of the function
    function adjuststars(playersxcoord, playersycoord)
    {
        xcoordadjustment = playersxcoord - playerslastxcoord;
        ycoordadjustment = playersycoord - playerslastycoord;
        playerslastxcoord = playersxcoord;
        playerslastycoord = playersycoord;
        leftofviewcheck = playersxcoord - gameareawidth;
        rightofviewcheck = playersxcoord + gameareawidth;
        topofviewcheck = playersycoord - gameareaheight;
        bottomofviewcheck = playersycoord + gameareaheight;
        for (i = 0; i < totalstars; i++)
        {
            redrawingstar = false;
            if (backgroundstar[i][0] < leftofviewcheck)
            {
                backgroundstar[i][0] = backgroundstar[i][0] + doublegameareawidth;
                this.gamebackground["backgroundstar" + i]._x = backgroundstar[i][0];
            }
            else if (backgroundstar[i][0] > rightofviewcheck)
            {
                backgroundstar[i][0] = backgroundstar[i][0] + -doublegameareawidth;
                this.gamebackground["backgroundstar" + i]._x = backgroundstar[i][0];
            } // end else if
            if (backgroundstar[i][1] < topofviewcheck)
            {
                backgroundstar[i][1] = backgroundstar[i][1] + doublegameareaheight;
                this.gamebackground["backgroundstar" + i]._y = backgroundstar[i][1];
                continue;
            } // end if
            if (backgroundstar[i][1] > bottomofviewcheck)
            {
                backgroundstar[i][1] = backgroundstar[i][1] + -doublegameareaheight;
                this.gamebackground["backgroundstar" + i]._y = backgroundstar[i][1];
            } // end if
        } // end of for
    } // End of the function
    for (iii = 0; iii < _root.neutralships.length; iii++)
    {
        func_drawneutralship(iii);
    } // end of for
    _root.otherplayership = new Array();
    _root.currentplayershotsfired = 0;
    this._x = _root.leftindentofgamearea + _root.gameareawidth / 2;
    this._y = _root.topindentofgamearea + _root.gameareaheight / 2;
    this.gameareawidth = _root.gameareawidth;
    this.gameareaheight = _root.gameareaheight;
    doublegameareawidth = gameareawidth * 2;
    doublegameareaheight = gameareaheight * 2;
    playersshiptype = _root.playershipstatus[5][0];
    this.attachMovie("shiptype" + playersshiptype, "playership", 1000);
    for (i = 0; i < _root.playershipstatus[8].length; i++)
    {
        if (_root.playershipstatus[8][i][0] != "none" || !isNaN(_root.playershipstatus[8][i][0]))
        {
            this.playership.attachMovie("turrettype0", "turret" + i, 250 + i);
            this.playership["turret" + i].shottype = _root.playershipstatus[8][i][0];
            this.playership["turret" + i].xonship = _root.shiptype[playersshiptype][5][i][0];
            this.playership["turret" + i].yonship = _root.shiptype[playersshiptype][5][i][1];
            this.playership["turret" + i]._x = this.playership["turret" + i].xonship;
            this.playership["turret" + i]._y = this.playership["turret" + i].yonship;
        } // end if
    } // end of for
    for (ii = 0; ii < _root.guntype.length; ii++)
    {
        _root["guntype" + ii + "sound"] = new Sound();
        _root["guntype" + ii + "sound"].attachSound("guntype" + ii + "sound");
        _root["guntype" + ii + "sound"].setVolume("75");
    } // end of for
    _root.missilefiresound = new Sound();
    _root.missilefiresound.attachSound("missilefiresound");
    _root.missilefiresound.setVolume("75");
    _root.aimessageradiostatic = new Sound();
    _root.aimessageradiostatic.attachSound("radiostaticsound");
    _root.aimessageradiostatic.setVolume("75");
    this.playershipfacing = _root.playershipfacing;
    this.playershiprotation = _root.playershiprotation;
    this.playershipvelocity = _root.playershipvelocity;
    this.playershipmaxvelocity = _root.playershipmaxvelocity;
    this.playershipacceleration = _root.playershipacceleration;
    this.totalstars = _root.totalstars;
    this.shipcoordinatex = _root.shipcoordinatex;
    this.shipcoordinatey = _root.shipcoordinatey;
    this.playershotsfired = new Array();
    _root.attachMovie("onlineplayerslist", "onlineplayerslist", 99982);
    setProperty("_root.onlineplayerslist", _x, 0);
    setProperty("_root.onlineplayerslist", _y, 105);
    this.halfgameareawidth = Math.round(gameareawidth / 2) + 1;
    this.halfgameareaheight = Math.round(gameareaheight / 2) + 1;
}
