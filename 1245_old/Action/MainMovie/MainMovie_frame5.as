﻿// Action script...

// [Action in Frame 5]
function errorcheckedmessage(datatosend, errorno)
{
    currentloc = _root.errorcheckdata.length;
    _root.errorcheckdata[currentloc] = new Array();
    _root.errorcheckdata[currentloc][0] = datatosend;
    _root.errorcheckdata[currentloc][1] = getTimer() + _root.errorcheckdelay;
    _root.errorcheckdata[currentloc][2] = errorno;
    ++_root.errorchecknumber;
    if (_root.errorchecknumber > 100)
    {
        _root.errorchecknumber = 0;
    } // end if
} // End of the function
function initializemissilebanks()
{
    _root.playershipstatus[10] = 0;
    _root.playershipstatus[7] = new Array();
    playershiptype = _root.playershipstatus[5][0];
    for (bankno = 0; bankno < _root.shiptype[playershiptype][7].length; bankno++)
    {
        _root.playershipstatus[7][bankno] = new Array();
        _root.playershipstatus[7][bankno][0] = 0;
        _root.playershipstatus[7][bankno][1] = 0;
        _root.playershipstatus[7][bankno][2] = _root.shiptype[playershiptype][7][bankno][2];
        _root.playershipstatus[7][bankno][3] = _root.shiptype[playershiptype][7][bankno][3];
        _root.playershipstatus[7][bankno][4] = new Array();
        for (abc = 0; abc < _root.missile.length; abc++)
        {
            _root.playershipstatus[7][bankno][4][abc] = 0;
        } // end of for
        _root.playershipstatus[7][bankno][5] = _root.shiptype[playershiptype][7][bankno][5];
    } // end of for
} // End of the function
function loadnewaiintogame(ailoadedvars)
{
    newinfo = ailoadedvars.split("~");
    _root.aishipsinfo = new Array();
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentai = i;
        currentthread = newinfo[i].split("`");
        _root.aishipsinfo[currentai] = new Array();
        _root.aishipsinfo[currentai][0] = currentthread[0];
        _root.currentgamestatus = _root.currentgamestatus + (" \r " + _root.aishipsinfo[currentai][0]);
        _root.aishipsinfo[currentai][1] = int(currentthread[1]);
        _root.currentgamestatus = _root.currentgamestatus + (" \r " + _root.aishipsinfo[currentai][1]);
    } // end of for
} // End of the function
function checkplayersdockedbase()
{
    baseexists = false;
    for (jjj = 0; jjj < _root.starbaselocation.length; jjj++)
    {
        if (_root.starbaselocation[jjj][0] == _root.playershipstatus[4][0])
        {
            baseexists = true;
            break;
        } // end if
    } // end of for
    if (baseexists == false)
    {
        noofbases = _root.starbaselocation.length - 1;
        basetouse = Math.round(Math.random() * noofbases);
        _root.playershipstatus[4][0] = _root.starbaselocation[basetouse][0];
    } // end if
} // End of the function
function loadsectormap(loadedvars)
{
    newinfo = loadedvars.split("~");
    i = 0;
    _root.sectormapitems = new Array();
    _root.starbaselocation = new Array();
    _root.teambases = new Array();
    while (i < newinfo.length - 1)
    {
        if (newinfo[i].substr(0, 2) == "SB" || newinfo[i].substr(0, 2) == "PL")
        {
            _root.sectormapitems[i] = new Array();
            currentthread = newinfo[i].split("`");
            currentstarbase = _root.starbaselocation.length;
            _root.starbaselocation[currentstarbase] = new Array();
            _root.starbaselocation[currentstarbase][0] = currentthread[0];
            _root.sectormapitems[i][0] = currentthread[0];
            _root.sectormapitems[i][1] = Number(currentthread[1].substr("1"));
            _root.starbaselocation[currentstarbase][1] = Number(currentthread[1].substr("1"));
            _root.sectormapitems[i][2] = Number(currentthread[2].substr("1"));
            _root.starbaselocation[currentstarbase][2] = Number(currentthread[2].substr("1"));
            _root.sectormapitems[i][3] = int(currentthread[3].substr("1"));
            _root.starbaselocation[currentstarbase][3] = int(currentthread[3].substr("1"));
            _root.starbaselocation[currentstarbase][4] = int(currentthread[4].substr("1"));
            _root.sectormapitems[i][6] = int(currentthread[4].substr("1"));
            _root.starbaselocation[currentstarbase][5] = currentthread[5];
            _root.starbaselocation[currentstarbase][9] = 0;
            _root.sectormapitems[i][9] = 0;
            _root.sectormapitems[i][4] = "OFF";
            _root.sectormapitems[i][10] = Math.ceil(Number(_root.sectormapitems[i][1]) / _root.sectorinformation[1][0]);
            _root.sectormapitems[i][11] = Math.ceil(Number(_root.sectormapitems[i][2]) / _root.sectorinformation[1][1]);
        } // end if
        if (newinfo[i].substr(0, 2) == "TB")
        {
            _root.teamdeathmatch = true;
            _root.sectormapitems[i] = new Array();
            currentthread = newinfo[i].split("`");
            currentteambases = _root.teambases.length;
            _root.teambases[currentteambases] = new Array();
            _root.teambases[currentteambases][0] = currentthread[0];
            _root.sectormapitems[i][0] = currentthread[0];
            _root.sectormapitems[i][1] = int(currentthread[1].substr("1"));
            _root.teambases[currentteambases][1] = Number(currentthread[1].substr("1"));
            _root.sectormapitems[i][2] = int(currentthread[2].substr("1"));
            _root.teambases[currentteambases][2] = Number(currentthread[2].substr("1"));
            _root.sectormapitems[i][3] = int(currentthread[3].substr("1"));
            _root.teambases[currentteambases][3] = int(currentthread[3].substr("1"));
            _root.teambases[currentteambases][4] = Number(currentthread[4].substr("1"));
            _root.sectormapitems[i][6] = int(currentthread[4].substr("1"));
            _root.teambases[currentteambases][5] = currentthread[5];
            _root.teambases[currentteambases][9] = 0;
            _root.sectormapitems[i][9] = 0;
            _root.teambases[currentteambases][10] = "N/A";
            _root.teambases[currentteambases][11] = _root.teambasetypes[_root.teambases[currentteambases][3]][2];
            _root.sectormapitems[i][4] = "OFF";
            _root.sectormapitems[i][10] = Math.ceil(Number(_root.sectormapitems[i][1]) / _root.sectorinformation[1][0]);
            _root.sectormapitems[i][11] = Math.ceil(Number(_root.sectormapitems[i][2]) / _root.sectorinformation[1][1]);
        } // end if
        if (newinfo[i].substr(0, 2) == "AS")
        {
            _root.sectormapitems[i] = new Array();
            currentthread = newinfo[i].split("`");
            _root.sectormapitems[i][0] = currentthread[0];
            _root.sectormapitems[i][1] = int(currentthread[1].substr("1"));
            _root.sectormapitems[i][2] = int(currentthread[2].substr("1"));
            _root.sectormapitems[i][3] = int(currentthread[3].substr("1"));
            _root.sectormapitems[i][4] = "OFF";
            _root.sectormapitems[i][10] = Math.ceil(Number(_root.sectormapitems[i][1]) / _root.sectorinformation[1][0]);
            _root.sectormapitems[i][11] = Math.ceil(Number(_root.sectormapitems[i][2]) / _root.sectorinformation[1][1]);
        } // end if
        if (newinfo[i].substr(0, 2) == "NP")
        {
            _root.sectormapitems[i] = new Array();
            currentthread = newinfo[i].split("`");
            _root.sectormapitems[i][0] = currentthread[0];
            _root.sectormapitems[i][1] = Number(currentthread[1].substr("1"));
            _root.sectormapitems[i][2] = Number(currentthread[2].substr("1"));
            _root.sectormapitems[i][3] = Number(currentthread[3].substr("1"));
            _root.sectormapitems[i][4] = "OFF";
            tester = Number(currentthread[4]);
            if (isNaN(tester))
            {
                _root.sectormapitems[i][5] = currentthread[4];
            }
            else
            {
                _root.sectormapitems[i][5] = "Jump Point to System: " + currentthread[4];
                _root.sectormapitems[i][9] = tester;
            } // end else if
            _root.sectormapitems[i][10] = Math.ceil(Number(_root.sectormapitems[i][1]) / _root.sectorinformation[1][0]);
            _root.sectormapitems[i][11] = Math.ceil(Number(_root.sectormapitems[i][2]) / _root.sectorinformation[1][1]);
        } // end if
        if (newinfo[i].substr(0, 2) == "SI")
        {
            currentthread = newinfo[i].split("`");
            if (currentthread[1].substr(0, 2) == "XL")
            {
                _root.sectorinformation = new Array();
                _root.sectorinformation[0] = new Array();
                _root.sectorinformation[0][0] = Number(currentthread[1].substr("2"));
                _root.sectorinformation[0][1] = Number(currentthread[2].substr("2"));
                _root.sectorinformation[1] = new Array();
                _root.sectorinformation[1][0] = Number(currentthread[3].substr("2"));
                _root.sectorinformation[1][1] = Number(currentthread[4].substr("2"));
                zoneinfo = currentthread[6].split(",");
                for (jjt = 0; jjt < zoneinfo.length; jjt++)
                {
                    if (zoneinfo[jjt] == "PULSAR")
                    {
                        _root.pulsarsinzone = true;
                    } // end if
                    if (zoneinfo[jjt] == "RACING")
                    {
                        isgameracingzone = true;
                    } // end if
                } // end of for
            } // end if
        } // end if
        ++i;
    } // end while
} // End of the function
_root.starbasestayhostiletime = 15000;
pathtoaifile = "./ai/";
_root.attachMovie("chatdialogue", "chatdialogue", 99983);
setProperty("_root.chatdialogue", _x, 130);
setProperty("_root.chatdialogue", _y, 500);
initializemissilebanks();
_root.playershipstatus[5][1] = 500;
if (_root.isgamerunningfromremote == false)
{
    _root.playershipstatus[5][1] = _root.portandsystem;
} // end if
if (_root.jumpinginfo.length > 1)
{
    _root.playershipstatus[5][1] = _root.portandsystem = _root.jumpinginfo[0];
} // end if
_root.currentgamestatus = _root.currentgamestatus + ("Loading System " + _root.playershipstatus[5][1] + "\r");
if (_root.isgamerunningfromremote == false)
{
    currenturl = String(_root._url);
    filler = currenturl.split("/");
    if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
    {
        loadVariables("http://www.gecko-games.com/stellar/gameserv" + portandsystem + ".php", _root.scoreClip, "POST";
    }
    else if (currenturl.substr(0, 32) == "http://www.stellar-conflicts.com")
    {
        loadVariables("http://www.stellar-conflicts.com/gametesting/gameserv" + portandsystem + ".php", _root.scoreClip, "POST";
    } // end else if
}
else
{
    loadVariables("http://localhost/september/gameserv6.php", _root.scoreClip, "POST";
} // end else if
newsystemVars = new XML();
newsystemVars.load(_root.pathtoaccounts + "systems.php?system=" + _root.playershipstatus[5][1]);
newsystemVars.onLoad = function (success)
{
    loadedvars = String(newsystemVars);
    if (loadedvars.length > 5)
    {
        loadsectormap(loadedvars);
        _root.currentgamestatus = _root.currentgamestatus + ("System " + _root.playershipstatus[5][1] + " loaded\r\r");
        checkplayersdockedbase();
        _root.play();
    }
    else
    {
        _root.currentgamestatus = _root.currentgamestatus + "Failed to load system\r";
    } // end else if
};
stop ();
