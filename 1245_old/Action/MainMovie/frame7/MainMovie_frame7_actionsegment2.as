﻿// Action script...

function enterintochat(info, colourtouse)
{
    _root.gamechatinfo[1].splice(0, 0, 0);
    _root.gamechatinfo[1][0] = new Array();
    _root.gamechatinfo[1][0][0] = info;
    _root.gamechatinfo[1][0][1] = colourtouse;
    if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
    {
        _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
    } // end if
    _root.refreshchatdisplay = true;
} // End of the function
function func_ignoreplayer(playerthatsaidit)
{
    ignoringplayer = false;
    for (ignoreing = 0; ignoreing < _root.gamechatinfo[6].length; ignoreing++)
    {
        if (playerthatsaidit == _root.gamechatinfo[6][ignoreing])
        {
            ignoringplayer = true;
            break;
        } // end if
    } // end of for
    return (ignoringplayer);
} // End of the function
function bringinaiships(noofenemyaiships, typeofscript)
{
    weakesthardpoint = 999999;
    for (i = 0; i < _root.playershipstatus[0].length; i++)
    {
        if (_root.playershipstatus[0][i][1] != "none" && weakesthardpoint > _root.playershipstatus[0][i][0])
        {
            weakesthardpoint = _root.playershipstatus[0][i][0];
        } // end if
    } // end of for
    guntypetouse = weakesthardpoint - 2;
    if (guntypetouse < 0 || guntypetouse > 5000)
    {
        guntypetouse = 0;
    } // end if
    if (typeofscript != "RANDOM")
    {
        ++guntypetouse;
    } // end if
    for (i = 0; i < noofenemyaiships; i++)
    {
        shiptypetouse = _root.playershipstatus[5][0];
        shieldgentouse = _root.playershipstatus[1][0];
        if (_root.playerscurrentextrashipno == "capital")
        {
            shiptypetouse = Math.round(Math.random() * 2) * 4 + 3;
            guntypetouse = _root.shiptype[shiptypetouse][6][3];
            shieldgentouse = _root.shiptype[shiptypetouse][6][0];
        } // end if
        currentai = int(_root.aishipshosted.length);
        if (currentai < 1)
        {
            _root.aishipshosted = new Array();
            currentai = 0;
        } // end if
        _root.aishipshosted[currentai] = new Array();
        _root.aishipshosted[currentai][0] = Math.round(Math.random() * 100) + 10;
        _root.aishipshosted[currentai][1] = _root.shipcoordinatex + Math.random() * 750 - 340;
        _root.aishipshosted[currentai][2] = _root.shipcoordinatey + Math.random() * 750 - 340;
        _root.aishipshosted[currentai][3] = 0;
        _root.aishipshosted[currentai][4] = 20;
        _root.aishipshosted[currentai][5] = "";
        _root.aishipshosted[currentai][6] = shiptypetouse;
        _root.aishipshosted[currentai][7] = "ai" + _root.ainames[Math.round(Math.random() * (_root.ainames.length - 1))];
        _root.aishipshosted[currentai][8] = shieldgentouse;
        _root.aishipshosted[currentai][9] = guntypetouse;
        _root.aishipshosted[currentai][10] = 0;
        _root.gamedisplayarea.attachMovie("shiptypeai", "aiship" + _root.aishipshosted[currentai][0], 11000 + _root.aishipshosted[currentai][0]);
        setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _x, _root.aishipshosted[currentai][1] - _root.shipcoordinatex);
        setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _y, _root.aishipshosted[currentai][2] - _root.shipcoordinatey);
        setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _rotation, _root.aishipshosted[currentai][3]);
        set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".shipid", _root.aishipshosted[currentai][0]);
        if (typeofscript != "RANDOM")
        {
            if (_root.playershipstatus[5][9] / _root.scoreratiomodifier > 1999)
            {
                set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".turrettype", 5);
                continue;
            } // end if
            if (_root.playershipstatus[5][9] / _root.scoreratiomodifier > 999)
            {
                set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".turrettype", 4);
                continue;
            } // end if
            if (_root.playershipstatus[5][9] / _root.scoreratiomodifier > 499)
            {
                set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".turrettype", 3);
            } // end if
        } // end if
    } // end of for
} // End of the function
_root.currentonlineplayers = new Array();
_root.attachMovie("onlineplayerslist", "onlineplayerslist", 99982);
setProperty("_root.onlineplayerslist", _x, 0);
setProperty("_root.onlineplayerslist", _y, 0);
