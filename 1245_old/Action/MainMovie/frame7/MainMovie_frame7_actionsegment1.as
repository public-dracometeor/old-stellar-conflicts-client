﻿// Action script...

function func_globaltimestamp()
{
    globaltimestamp = String(getTimer() + _root.clocktimediff);
    globaltimestamp = Number(globaltimestamp.substr(globaltimestamp.length - 4));
    globaltimestamp = globaltimestamp + _root.keypressdelay;
    if (globaltimestamp > 10000)
    {
        globaltimestamp = globaltimestamp - 10000;
    } // end if
    return (globaltimestamp);
} // End of the function
