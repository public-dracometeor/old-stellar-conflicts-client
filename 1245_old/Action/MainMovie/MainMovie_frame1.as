﻿// Action script...

// [onClipEvent of sprite 1249 in frame 1]
onClipEvent (load)
{
    _root.versionno = ".184b";
    _root.isgamerunningfromremote = false;
    _root.accountserveraddy = "217.160.243.182";
    _root.accountserverport = "2125";
    _root._quality = "HIGH";
    _root.scoreratiomodifier = 50;
    _root.missiontoactualscoremodifier = 10;
    _root.publicteams = 20;
    _root.lastplayerssavedinfo = "";
    _root.regularchattextcolor = 3407871;
    _root.privatechattextcolor = 3407667;
    _root.systemchattextcolor = 3407667;
    _root.teamchattextcolor = 16776960;
    _root.squadchattextcolor = 14496563;
    _root.arenachattextcolor = 16711935;
    _root.playershipstatus[3][2] = "Enter Name";
    _root.isplayeraguest = false;
    if (_root.isgamerunningfromremote != true)
    {
        _root.pathtoaccounts = "";
    }
    else
    {
        _root.pathtoaccounts = "http://www.gecko-games.com/stellar/";
    } // end else if
    loadVariables("http://www.gecko-games.com/stellar/accountsserv2.php", _root.scoreClip, "POST";
    _root.isplayeremp = false;
    _root.playerempend = 0;
    _root.playerdiruptend = 0;
    _root.isplayerdisrupt = false;
    _root.accountcreationsavedgame = "PI`ST0`SG0`EG0`EC0`CR350000`SE100`SB113`HP0G0`HP1G0~";
    _root.pinginformation = "";
    _root.mapdsiplayed = false;
    _root.isaiturnedon = true;
    _root.playersdockedtime = null;
    _root.playersexitdocktimewait = 4500;
    _root.playersdestination = new Array();
    _root.playersdestination[0] = null;
    _root.playersdestination[1] = null;
    _root.playersdestination[2] = null;
    _root.playerjustloggedin = true;
    _root.hasplayerbeenlistedasdocked = false;
    _root.soundvolume = "on";
    _root.loginerror = "Use a name of only letters, numbers. Under 11 Digits";
    _root.currentmaxplayers = 15;
    _root.playersworthtobtymodifire = 1000;
    _root.leftindentofgamearea = 0;
    _root.topindentofgamearea = 0;
    _root.gameareawidth = 700;
    _root.gameareaheight = 550;
    _root.playershiptype = "shiptype1";
    _root.playershipfacing = 0;
    _root.playershiprotation = 10;
    _root.playershipvelocity = 0;
    _root.playershipmaxvelocity = 100;
    _root.playershipacceleration = 40;
    _root.newdraw = true;
    _root.totalstars = 3;
    _root.totalstartypes = 5;
    _root.shipcoordinatex = 0;
    _root.shipcoordinatey = 0;
    _root.currentplayershotsfired = 0;
    _root.playershipstatus = new Array();
    _root.playershipstatus[0] = new Array();
    _root.playershipstatus[0][0] = new Array();
    _root.playershipstatus[0][0][0] = 0;
    _root.playershipstatus[0][0][1] = 0;
    _root.playershipstatus[0][0][2] = 5;
    _root.playershipstatus[0][0][3] = 0;
    _root.playershipstatus[0][0][4] = "ON";
    _root.playershipstatus[0][1] = new Array();
    _root.playershipstatus[0][1][0] = 1;
    _root.playershipstatus[0][1][1] = 0;
    _root.playershipstatus[0][1][2] = -7;
    _root.playershipstatus[0][1][3] = 0;
    _root.playershipstatus[0][1][4] = "ON";
    _root.playershipstatus[1] = new Array();
    _root.playershipstatus[1][0] = 0;
    _root.playershipstatus[1][1] = 0;
    _root.playershipstatus[1][5] = 0;
    _root.playershipstatus[2] = new Array();
    _root.playershipstatus[2][0] = 0;
    _root.playershipstatus[2][1] = 0;
    _root.playershipstatus[2][2] = "FULL";
    _root.playershipstatus[2][5] = 1000;
    _root.playershipstatus[2][6] = 0;
    _root.playershipstatus[3] = new Array();
    _root.playershipstatus[3][0] = "";
    _root.playershipstatus[3][1] = 20000;
    _root.playershipstatus[3][3] = null;
    _root.playershipstatus[3][5] = null;
    _root.playershipstatus[4] = new Array();
    _root.playershipstatus[4][0] = "";
    _root.playershipstatus[4][1] = new Array();
    _root.playershipstatus[5] = new Array();
    _root.playershipstatus[5][0] = 0;
    _root.playershipstatus[5][1] = null;
    _root.playershipstatus[5][2] = "N/A";
    _root.playershipstatus[5][3] = 0;
    _root.playershipstatus[5][4] = "alive";
    _root.playershipstatus[5][5] = null;
    _root.playershipstatus[5][6] = 0.125000;
    _root.playershipstatus[5][7] = null;
    _root.playershipstatus[5][8] = 0;
    _root.playershipstatus[5][9] = Number(0);
    _root.playershipstatus[5][10] = "NONE";
    _root.playershipstatus[5][11] = false;
    _root.playershipstatus[5][12] = "";
    _root.playershipstatus[5][13] = "";
    _root.playershipstatus[5][15] = "";
    _root.playershipstatus[5][19] = "";
    _root.playershipstatus[5][20] = false;
    _root.playershipstatus[5][21] = 0;
    _root.playershipstatus[5][22] = false;
    _root.playershipstatus[5][25] = "YES";
    _root.playershipstatus[6] = new Array();
    _root.playershipstatus[6][0] = 0;
    _root.playershipstatus[6][1] = 0;
    _root.playershipstatus[6][2] = 0;
    _root.playershipstatus[6][3] = 0;
    _root.playershipstatus[7] = new Array();
    _root.playershipstatus[7][0] = new Array();
    _root.playershipstatus[7][0][0] = 0;
    _root.playershipstatus[7][0][1] = 0;
    _root.playershipstatus[7][0][2] = 5;
    _root.playershipstatus[7][0][3] = 0;
    _root.playershipstatus[7][0][4] = new Array();
    _root.playershipstatus[7][0][4][0] = 2;
    _root.playershipstatus[7][0][4][1] = 2;
    _root.playershipstatus[7][0][5] = 10;
    _root.playershipstatus[7][1] = new Array();
    _root.playershipstatus[7][1][0] = 0;
    _root.playershipstatus[7][1][1] = 0;
    _root.playershipstatus[7][1][2] = 5;
    _root.playershipstatus[7][1][3] = 0;
    _root.playershipstatus[7][1][4] = new Array();
    _root.playershipstatus[7][1][4][0] = 10;
    _root.playershipstatus[7][1][4][1] = 5;
    _root.playershipstatus[7][1][5] = 10;
    _root.playershipstatus[8] = new Array();
    _root.playershipstatus[9] = "AUTO";
    _root.playershipstatus[10] = 0;
    _root.playershipstatus[11] = new Array();
    _root.playershipstatus[11][0] = new Array();
    _root.playershipstatus[11][0][0] = 0;
    _root.playershipstatus[11][0][1] = 0;
    _root.playershipstatus[11][0][2] = 0;
    _root.playershipstatus[11][1] = new Array();
    _root.playershipstatus[11][2] = new Array();
    _root.playershipstatus[11][2][0] = new Array();
    _root.playershipstatus[11][2][0][0] = 0;
    _root.playershipstatus[11][2][1] = new Array();
    _root.playershipstatus[11][2][1][0] = 0;
    _root.playershipstatus[11][2][2] = new Array();
    _root.playershipstatus[11][2][2][0] = 0;
    currentmissile = 0;
    _root.missile = new Array();
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 120;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 8000;
    _root.missile[currentmissile][3] = 3000;
    _root.missile[currentmissile][4] = 1250;
    _root.missile[currentmissile][5] = 500;
    _root.missile[currentmissile][6] = "Torp 1";
    _root.missile[currentmissile][7] = "Torp 1";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 110;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 11000;
    _root.missile[currentmissile][3] = 3500;
    _root.missile[currentmissile][4] = 2200;
    _root.missile[currentmissile][5] = 1500;
    _root.missile[currentmissile][6] = "Torp 2";
    _root.missile[currentmissile][7] = "Torp 2";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 100;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 12000;
    _root.missile[currentmissile][3] = 4000;
    _root.missile[currentmissile][4] = 3400;
    _root.missile[currentmissile][5] = 2750;
    _root.missile[currentmissile][6] = "Torp 3";
    _root.missile[currentmissile][7] = "Torp 3";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 90;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 15000;
    _root.missile[currentmissile][3] = 5000;
    _root.missile[currentmissile][4] = 5500;
    _root.missile[currentmissile][5] = 4500;
    _root.missile[currentmissile][6] = "Torp 4";
    _root.missile[currentmissile][7] = "Torp 4";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 120;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 12000;
    _root.missile[currentmissile][3] = 4000;
    _root.missile[currentmissile][4] = 4500;
    _root.missile[currentmissile][5] = 4250;
    _root.missile[currentmissile][6] = "Stealth";
    _root.missile[currentmissile][7] = "Stealth";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 100;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 7000;
    _root.missile[currentmissile][3] = 4000;
    _root.missile[currentmissile][4] = 2000;
    _root.missile[currentmissile][5] = 1400;
    _root.missile[currentmissile][6] = "Mirv 1";
    _root.missile[currentmissile][7] = "Mirv 1";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 85;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 8000;
    _root.missile[currentmissile][3] = 6000;
    _root.missile[currentmissile][4] = 4000;
    _root.missile[currentmissile][5] = 3000;
    _root.missile[currentmissile][6] = "Mirv 2";
    _root.missile[currentmissile][7] = "Mirv 2";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 70;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 9000;
    _root.missile[currentmissile][3] = 7500;
    _root.missile[currentmissile][4] = 6500;
    _root.missile[currentmissile][5] = 5500;
    _root.missile[currentmissile][6] = "Mirv 3";
    _root.missile[currentmissile][7] = "Mirv 3";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 70;
    _root.missile[currentmissile][1] = 65;
    _root.missile[currentmissile][2] = 8000;
    _root.missile[currentmissile][3] = 6500;
    _root.missile[currentmissile][4] = 1500;
    _root.missile[currentmissile][5] = 3500;
    _root.missile[currentmissile][6] = "Seek 1";
    _root.missile[currentmissile][7] = "Seek 1";
    _root.missile[currentmissile][8] = "SEEKER";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 60;
    _root.missile[currentmissile][1] = 55;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 7500;
    _root.missile[currentmissile][4] = 2550;
    _root.missile[currentmissile][5] = 5500;
    _root.missile[currentmissile][6] = "Seek 2";
    _root.missile[currentmissile][7] = "Seek 2";
    _root.missile[currentmissile][8] = "SEEKER";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 50;
    _root.missile[currentmissile][1] = 35;
    _root.missile[currentmissile][2] = 13000;
    _root.missile[currentmissile][3] = 10000;
    _root.missile[currentmissile][4] = 3500;
    _root.missile[currentmissile][5] = 8500;
    _root.missile[currentmissile][6] = "Seek 3";
    _root.missile[currentmissile][7] = "Seek 3";
    _root.missile[currentmissile][8] = "SEEKER";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 55;
    _root.missile[currentmissile][1] = 45;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 9000;
    _root.missile[currentmissile][4] = 3500;
    _root.missile[currentmissile][5] = 7500;
    _root.missile[currentmissile][6] = "Seek ST";
    _root.missile[currentmissile][7] = "Seek ST";
    _root.missile[currentmissile][8] = "SEEKER";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 65;
    _root.missile[currentmissile][1] = 65;
    _root.missile[currentmissile][2] = 15000;
    _root.missile[currentmissile][3] = 6000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 4000;
    _root.missile[currentmissile][6] = "EMP 1";
    _root.missile[currentmissile][7] = "EMP 1";
    _root.missile[currentmissile][8] = "EMP";
    _root.missile[currentmissile][9] = 3000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 60;
    _root.missile[currentmissile][1] = 60;
    _root.missile[currentmissile][2] = 12000;
    _root.missile[currentmissile][3] = 7000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 6000;
    _root.missile[currentmissile][6] = "EMP 2";
    _root.missile[currentmissile][7] = "EMP 2";
    _root.missile[currentmissile][8] = "EMP";
    _root.missile[currentmissile][9] = 6000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 50;
    _root.missile[currentmissile][1] = 53;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 9000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 8000;
    _root.missile[currentmissile][6] = "EMP 3";
    _root.missile[currentmissile][7] = "EMP 3";
    _root.missile[currentmissile][8] = "EMP";
    _root.missile[currentmissile][9] = 10000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 60;
    _root.missile[currentmissile][1] = 55;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 9000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 8000;
    _root.missile[currentmissile][6] = "Disrupter 1";
    _root.missile[currentmissile][7] = "Disrupter 1";
    _root.missile[currentmissile][8] = "DISRUPTER";
    _root.missile[currentmissile][9] = 2000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 50;
    _root.missile[currentmissile][1] = 50;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 11000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 14000;
    _root.missile[currentmissile][6] = "Disrupter 2";
    _root.missile[currentmissile][7] = "Disrupter 2";
    _root.missile[currentmissile][8] = "DISRUPTER";
    _root.missile[currentmissile][9] = 4000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 45;
    _root.missile[currentmissile][1] = 45;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 15000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 20000;
    _root.missile[currentmissile][6] = "Disrupter 3";
    _root.missile[currentmissile][7] = "Disrupter 3";
    _root.missile[currentmissile][8] = "DISRUPTER";
    _root.missile[currentmissile][9] = 5500;
    ++currentmissile;
    _root.energygenerators = new Array();
    _root.energygenerators[0] = new Array();
    _root.energygenerators[0][0] = 100;
    _root.energygenerators[0][1] = "Level 1";
    _root.energygenerators[0][2] = 1000;
    _root.energygenerators[1] = new Array();
    _root.energygenerators[1][0] = 150;
    _root.energygenerators[1][1] = "Level 2";
    _root.energygenerators[1][2] = 1000;
    _root.energygenerators[2] = new Array();
    _root.energygenerators[2][0] = 200;
    _root.energygenerators[2][1] = "Level 3";
    _root.energygenerators[2][2] = 22000;
    _root.energygenerators[3] = new Array();
    _root.energygenerators[3][0] = 250;
    _root.energygenerators[3][1] = "Level 4";
    _root.energygenerators[3][2] = 35000;
    _root.energygenerators[4] = new Array();
    _root.energygenerators[4][0] = 300;
    _root.energygenerators[4][1] = "Level 5";
    _root.energygenerators[4][2] = 51000;
    _root.energygenerators[5] = new Array();
    _root.energygenerators[5][0] = 425;
    _root.energygenerators[5][1] = "Level 6";
    _root.energygenerators[5][2] = 159000;
    _root.energygenerators[6] = new Array();
    _root.energygenerators[6][0] = 540;
    _root.energygenerators[6][1] = "Level 7";
    _root.energygenerators[6][2] = 335500;
    _root.energygenerators[7] = new Array();
    _root.energygenerators[7][0] = 690;
    _root.energygenerators[7][1] = "Level 8";
    _root.energygenerators[7][2] = 535500;
    _root.energygenerators[8] = new Array();
    _root.energygenerators[8][0] = 810;
    _root.energygenerators[8][1] = "Level 9";
    _root.energygenerators[8][2] = 735500;
    _root.energygenerators[9] = new Array();
    _root.energygenerators[9][0] = 970;
    _root.energygenerators[9][1] = "Level 10";
    _root.energygenerators[9][2] = 935500;
    _root.energygenerators[10] = new Array();
    _root.energygenerators[10][0] = 1155;
    _root.energygenerators[10][1] = "Level 11";
    _root.energygenerators[10][2] = 935500;
    _root.energycapacitors = new Array();
    _root.energycapacitors[0] = new Array();
    _root.energycapacitors[0][0] = 200;
    _root.energycapacitors[0][1] = "Level 1";
    _root.energycapacitors[0][2] = 1500;
    _root.energycapacitors[1] = new Array();
    _root.energycapacitors[1][0] = 300;
    _root.energycapacitors[1][1] = "Level 2";
    _root.energycapacitors[1][2] = 6000;
    _root.energycapacitors[2] = new Array();
    _root.energycapacitors[2][0] = 400;
    _root.energycapacitors[2][1] = "Level 3";
    _root.energycapacitors[2][2] = 13000;
    _root.energycapacitors[3] = new Array();
    _root.energycapacitors[3][0] = 500;
    _root.energycapacitors[3][1] = "Level 4";
    _root.energycapacitors[3][2] = 21000;
    _root.energycapacitors[4] = new Array();
    _root.energycapacitors[4][0] = 700;
    _root.energycapacitors[4][1] = "Level 5";
    _root.energycapacitors[4][2] = 83000;
    _root.energycapacitors[5] = new Array();
    _root.energycapacitors[5][0] = 900;
    _root.energycapacitors[5][1] = "Level 6";
    _root.energycapacitors[5][2] = 235000;
    _root.energycapacitors[6] = new Array();
    _root.energycapacitors[6][0] = 1150;
    _root.energycapacitors[6][1] = "Level 7";
    _root.energycapacitors[6][2] = 385000;
    _root.energycapacitors[7] = new Array();
    _root.energycapacitors[7][0] = 1425;
    _root.energycapacitors[7][1] = "Level 8";
    _root.energycapacitors[7][2] = 535000;
    _root.energycapacitors[8] = new Array();
    _root.energycapacitors[8][0] = 1750;
    _root.energycapacitors[8][1] = "Level 9";
    _root.energycapacitors[8][2] = 755000;
    _root.energycapacitors[9] = new Array();
    _root.energycapacitors[9][0] = 2150;
    _root.energycapacitors[9][1] = "Level 10";
    _root.energycapacitors[9][2] = 1435000;
    _root.shieldgenerators = new Array();
    _root.shieldgenerators[0] = new Array();
    _root.shieldgenerators[0][0] = 1000;
    _root.shieldgenerators[0][1] = 50;
    _root.shieldgenerators[0][2] = 10;
    _root.shieldgenerators[0][3] = 10;
    _root.shieldgenerators[0][4] = "Level 1";
    _root.shieldgenerators[0][5] = 750;
    _root.shieldgenerators[1] = new Array();
    _root.shieldgenerators[1][0] = 1500;
    _root.shieldgenerators[1][1] = 75;
    _root.shieldgenerators[1][2] = 20;
    _root.shieldgenerators[1][3] = 50;
    _root.shieldgenerators[1][4] = "Level 2";
    _root.shieldgenerators[1][5] = 8000;
    _root.shieldgenerators[2] = new Array();
    _root.shieldgenerators[2][0] = 2000;
    _root.shieldgenerators[2][1] = 100;
    _root.shieldgenerators[2][2] = 30;
    _root.shieldgenerators[2][3] = 100;
    _root.shieldgenerators[2][4] = "Level 3";
    _root.shieldgenerators[2][5] = 25500;
    _root.shieldgenerators[3] = new Array();
    _root.shieldgenerators[3][0] = 2600;
    _root.shieldgenerators[3][1] = 125;
    _root.shieldgenerators[3][2] = 35;
    _root.shieldgenerators[3][3] = 125;
    _root.shieldgenerators[3][4] = "Level 4";
    _root.shieldgenerators[3][5] = 68500;
    _root.shieldgenerators[4] = new Array();
    _root.shieldgenerators[4][0] = 6700;
    _root.shieldgenerators[4][1] = 195;
    _root.shieldgenerators[4][2] = 50;
    _root.shieldgenerators[4][3] = 200;
    _root.shieldgenerators[4][4] = "Level 5";
    _root.shieldgenerators[4][5] = 140995;
    _root.shieldgenerators[5] = new Array();
    _root.shieldgenerators[5][0] = 11000;
    _root.shieldgenerators[5][1] = 275;
    _root.shieldgenerators[5][2] = 75;
    _root.shieldgenerators[5][3] = 300;
    _root.shieldgenerators[5][4] = "Level 6";
    _root.shieldgenerators[5][5] = 310000;
    _root.shieldgenerators[6] = new Array();
    _root.shieldgenerators[6][0] = 17000;
    _root.shieldgenerators[6][1] = 345;
    _root.shieldgenerators[6][2] = 150;
    _root.shieldgenerators[6][3] = 400;
    _root.shieldgenerators[6][4] = "Level 7";
    _root.shieldgenerators[6][5] = 620456;
    _root.shieldgenerators[7] = new Array();
    _root.shieldgenerators[7][0] = 28000;
    _root.shieldgenerators[7][1] = 425;
    _root.shieldgenerators[7][2] = 230;
    _root.shieldgenerators[7][3] = 500;
    _root.shieldgenerators[7][4] = "Level 8";
    _root.shieldgenerators[7][5] = 1511548;
    _root.shieldgenerators[8] = new Array();
    _root.shieldgenerators[8][0] = 41000;
    _root.shieldgenerators[8][1] = 520;
    _root.shieldgenerators[8][2] = 305;
    _root.shieldgenerators[8][3] = 600;
    _root.shieldgenerators[8][4] = "Level 9";
    _root.shieldgenerators[8][5] = 2811548;
    _root.guntype = new Array();
    _root.guntype[0] = new Array();
    _root.guntype[0][0] = 120;
    _root.guntype[0][1] = 2;
    _root.guntype[0][2] = 0.500000;
    _root.guntype[0][3] = 15;
    _root.guntype[0][4] = 150;
    _root.guntype[0][5] = 1000;
    _root.guntype[0][6] = "Laser";
    _root.guntype[1] = new Array();
    _root.guntype[1][0] = 120;
    _root.guntype[1][1] = 2;
    _root.guntype[1][2] = 0.600000;
    _root.guntype[1][3] = 40;
    _root.guntype[1][4] = 250;
    _root.guntype[1][5] = 8000;
    _root.guntype[1][6] = "Double Laser";
    _root.guntype[2] = new Array();
    _root.guntype[2][0] = 100;
    _root.guntype[2][1] = 3;
    _root.guntype[2][2] = 0.800000;
    _root.guntype[2][3] = 35;
    _root.guntype[2][4] = 325;
    _root.guntype[2][5] = 23500;
    _root.guntype[2][6] = "Mass Driver";
    _root.guntype[3] = new Array();
    _root.guntype[3][0] = 95;
    _root.guntype[3][1] = 3;
    _root.guntype[3][2] = 0.800000;
    _root.guntype[3][3] = 50;
    _root.guntype[3][4] = 425;
    _root.guntype[3][5] = 48000;
    _root.guntype[3][6] = "Neutron Blaster";
    _root.guntype[4] = new Array();
    _root.guntype[4][0] = 75;
    _root.guntype[4][1] = 4;
    _root.guntype[4][2] = 1;
    _root.guntype[4][3] = 70;
    _root.guntype[4][4] = 650;
    _root.guntype[4][5] = 95000;
    _root.guntype[4][6] = "Plasma Cannon";
    _root.guntype[5] = new Array();
    _root.guntype[5][0] = 95;
    _root.guntype[5][1] = 3.500000;
    _root.guntype[5][2] = 1.500000;
    _root.guntype[5][3] = 120;
    _root.guntype[5][4] = 895;
    _root.guntype[5][5] = 195000;
    _root.guntype[5][6] = "Phasor";
    gunno = 6;
    _root.guntype[gunno] = new Array();
    _root.guntype[gunno][0] = 85;
    _root.guntype[gunno][1] = 4.500000;
    _root.guntype[gunno][2] = 1.100000;
    _root.guntype[gunno][3] = 170;
    _root.guntype[gunno][4] = 1150;
    _root.guntype[gunno][5] = 510000;
    _root.guntype[gunno][6] = "Neutron Pulse";
    _root.totalsmallships = 16;
    _root.shiptype = new Array();
    currentship = 0;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Human Scout";
    _root.shiptype[currentship][1] = 75000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 9;
    _root.shiptype[currentship][2][0][1] = -5;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -10;
    _root.shiptype[currentship][2][1][1] = -5;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 40;
    _root.shiptype[currentship][3][1] = 95;
    _root.shiptype[currentship][3][2] = 90;
    _root.shiptype[currentship][3][3] = 1000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 115;
    _root.shiptype[currentship][3][7] = 3;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 40;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 2;
    _root.shiptype[currentship][6][1] = 2;
    _root.shiptype[currentship][6][2] = 3;
    _root.shiptype[currentship][6][3] = 3;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -5;
    _root.shiptype[currentship][7][0][5] = 6;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Human Frigate";
    _root.shiptype[currentship][1] = 250000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 7;
    _root.shiptype[currentship][2][0][1] = -3;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -1;
    _root.shiptype[currentship][2][1][1] = -7;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -8;
    _root.shiptype[currentship][2][2][1] = -3;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 40;
    _root.shiptype[currentship][3][1] = 90;
    _root.shiptype[currentship][3][2] = 90;
    _root.shiptype[currentship][3][3] = 1500;
    _root.shiptype[currentship][3][4] = 12;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 110;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 70;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 3;
    _root.shiptype[currentship][6][1] = 3;
    _root.shiptype[currentship][6][2] = 4;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 5;
    _root.shiptype[currentship][7][0][3] = 0;
    _root.shiptype[currentship][7][0][5] = 6;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -5;
    _root.shiptype[currentship][7][1][3] = 0;
    _root.shiptype[currentship][7][1][5] = 6;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Light Freighter";
    _root.shiptype[currentship][1] = 450000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 10;
    _root.shiptype[currentship][2][0][1] = -2;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -10;
    _root.shiptype[currentship][2][1][1] = -2;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 35;
    _root.shiptype[currentship][3][1] = 80;
    _root.shiptype[currentship][3][2] = 75;
    _root.shiptype[currentship][3][3] = 5000;
    _root.shiptype[currentship][3][4] = 11;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 95;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 250;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = 8;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -5;
    _root.shiptype[currentship][7][0][5] = 12;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Human Hornet";
    _root.shiptype[currentship][1] = 1250000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 14;
    _root.shiptype[currentship][2][0][1] = 0;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 7;
    _root.shiptype[currentship][2][1][1] = -2;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -7;
    _root.shiptype[currentship][2][2][1] = -2;
    _root.shiptype[currentship][2][3] = new Array();
    _root.shiptype[currentship][2][3][0] = -14;
    _root.shiptype[currentship][2][3][1] = 0;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 38;
    _root.shiptype[currentship][3][1] = 83;
    _root.shiptype[currentship][3][2] = 105;
    _root.shiptype[currentship][3][3] = 7500;
    _root.shiptype[currentship][3][4] = 9;
    _root.shiptype[currentship][3][5] = 0.250000;
    _root.shiptype[currentship][3][6] = 98;
    _root.shiptype[currentship][3][7] = 7;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 50;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 7;
    _root.shiptype[currentship][7][0][3] = -2;
    _root.shiptype[currentship][7][0][5] = 10;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -7;
    _root.shiptype[currentship][7][1][3] = -2;
    _root.shiptype[currentship][7][1][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Tuskan Scout";
    _root.shiptype[currentship][1] = 70000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 5;
    _root.shiptype[currentship][2][0][1] = -10;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -5;
    _root.shiptype[currentship][2][1][1] = -10;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 50;
    _root.shiptype[currentship][3][1] = 95;
    _root.shiptype[currentship][3][2] = 85;
    _root.shiptype[currentship][3][3] = 1500;
    _root.shiptype[currentship][3][4] = 9;
    _root.shiptype[currentship][3][5] = 0.250000;
    _root.shiptype[currentship][3][6] = 105;
    _root.shiptype[currentship][3][7] = 2;
    _root.shiptype[currentship][3][8] = true;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 30;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 2;
    _root.shiptype[currentship][6][1] = 4;
    _root.shiptype[currentship][6][2] = 4;
    _root.shiptype[currentship][6][3] = 3;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -3;
    _root.shiptype[currentship][7][0][5] = 5;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Tuskan Freighter";
    _root.shiptype[currentship][1] = 950000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 0;
    _root.shiptype[currentship][2][0][1] = -10;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 25;
    _root.shiptype[currentship][3][1] = 78;
    _root.shiptype[currentship][3][2] = 70;
    _root.shiptype[currentship][3][3] = 5000;
    _root.shiptype[currentship][3][4] = 11;
    _root.shiptype[currentship][3][5] = 0.500000;
    _root.shiptype[currentship][3][6] = 93;
    _root.shiptype[currentship][3][7] = 4;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 400;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -10;
    _root.shiptype[currentship][5][0][1] = 3;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 10;
    _root.shiptype[currentship][5][1][1] = 3;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -3;
    _root.shiptype[currentship][7][0][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Tuskan Interceptor";
    _root.shiptype[currentship][1] = 600000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 11;
    _root.shiptype[currentship][2][0][1] = -9;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 6;
    _root.shiptype[currentship][2][1][1] = -11;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -6;
    _root.shiptype[currentship][2][2][1] = -11;
    _root.shiptype[currentship][2][3] = new Array();
    _root.shiptype[currentship][2][3][0] = -11;
    _root.shiptype[currentship][2][3][1] = -9;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 38;
    _root.shiptype[currentship][3][1] = 85;
    _root.shiptype[currentship][3][2] = 85;
    _root.shiptype[currentship][3][3] = 3500;
    _root.shiptype[currentship][3][4] = 9;
    _root.shiptype[currentship][3][5] = 0.250000;
    _root.shiptype[currentship][3][6] = 103;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 50;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 5;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 11;
    _root.shiptype[currentship][7][0][3] = -9;
    _root.shiptype[currentship][7][0][5] = 8;
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = -11;
    _root.shiptype[currentship][7][0][3] = -9;
    _root.shiptype[currentship][7][0][5] = 8;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Tuskan Avenger";
    _root.shiptype[currentship][1] = 1955000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 7;
    _root.shiptype[currentship][2][0][1] = -18;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -7;
    _root.shiptype[currentship][2][1][1] = -18;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 33;
    _root.shiptype[currentship][3][1] = 75;
    _root.shiptype[currentship][3][2] = 70;
    _root.shiptype[currentship][3][3] = 21000;
    _root.shiptype[currentship][3][4] = 6;
    _root.shiptype[currentship][3][5] = 0.450000;
    _root.shiptype[currentship][3][6] = 88;
    _root.shiptype[currentship][3][7] = 10;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 150;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -13;
    _root.shiptype[currentship][5][0][1] = 0;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 13;
    _root.shiptype[currentship][5][1][1] = 0;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 7;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -3;
    _root.shiptype[currentship][7][0][5] = 10;
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = -12;
    _root.shiptype[currentship][7][0][3] = 0;
    _root.shiptype[currentship][7][0][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Feril Raider";
    _root.shiptype[currentship][1] = 140000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 11;
    _root.shiptype[currentship][2][0][1] = 0;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -11;
    _root.shiptype[currentship][2][1][1] = 0;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 60;
    _root.shiptype[currentship][3][1] = 90;
    _root.shiptype[currentship][3][2] = 90;
    _root.shiptype[currentship][3][3] = 2500;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.150000;
    _root.shiptype[currentship][3][6] = 102;
    _root.shiptype[currentship][3][7] = 3;
    _root.shiptype[currentship][3][8] = true;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 30;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 2;
    _root.shiptype[currentship][6][1] = 4;
    _root.shiptype[currentship][6][2] = 4;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 8;
    _root.shiptype[currentship][7][0][3] = 5;
    _root.shiptype[currentship][7][0][5] = 3;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -8;
    _root.shiptype[currentship][7][1][3] = 5;
    _root.shiptype[currentship][7][1][5] = 3;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Feril Freighter";
    _root.shiptype[currentship][1] = 2250000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 7;
    _root.shiptype[currentship][2][0][1] = -11;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -7;
    _root.shiptype[currentship][2][1][1] = -11;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 30;
    _root.shiptype[currentship][3][1] = 73;
    _root.shiptype[currentship][3][2] = 65;
    _root.shiptype[currentship][3][3] = 6000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.500000;
    _root.shiptype[currentship][3][6] = 87;
    _root.shiptype[currentship][3][7] = 6;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 1000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -12;
    _root.shiptype[currentship][5][0][1] = 10;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 12;
    _root.shiptype[currentship][5][1][1] = 10;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 5;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -11;
    _root.shiptype[currentship][7][0][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Feril Mirauder";
    _root.shiptype[currentship][1] = 1650000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 14;
    _root.shiptype[currentship][2][0][1] = -2;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -14;
    _root.shiptype[currentship][2][1][1] = -2;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 50;
    _root.shiptype[currentship][3][1] = 85;
    _root.shiptype[currentship][3][2] = 70;
    _root.shiptype[currentship][3][3] = 5500;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.200000;
    _root.shiptype[currentship][3][6] = 97;
    _root.shiptype[currentship][3][7] = 6;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 175;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -8;
    _root.shiptype[currentship][5][0][1] = 4;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 8;
    _root.shiptype[currentship][5][1][1] = 4;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 5;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 8;
    _root.shiptype[currentship][7][0][3] = 4;
    _root.shiptype[currentship][7][0][5] = 6;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -8;
    _root.shiptype[currentship][7][1][3] = 4;
    _root.shiptype[currentship][7][1][5] = 6;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Feril Assaulter";
    _root.shiptype[currentship][1] = 2750000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 14;
    _root.shiptype[currentship][2][0][1] = 4;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 9;
    _root.shiptype[currentship][2][1][1] = -7;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -9;
    _root.shiptype[currentship][2][2][1] = -7;
    _root.shiptype[currentship][2][3] = new Array();
    _root.shiptype[currentship][2][3][0] = -14;
    _root.shiptype[currentship][2][3][1] = 4;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 40;
    _root.shiptype[currentship][3][1] = 86;
    _root.shiptype[currentship][3][2] = 60;
    _root.shiptype[currentship][3][3] = 5500;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.320000;
    _root.shiptype[currentship][3][6] = 103;
    _root.shiptype[currentship][3][7] = 8;
    _root.shiptype[currentship][3][8] = true;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 200;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 10;
    _root.shiptype[currentship][7][0][3] = 5;
    _root.shiptype[currentship][7][0][5] = 10;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -10;
    _root.shiptype[currentship][7][1][3] = 5;
    _root.shiptype[currentship][7][1][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Alkari Runner";
    _root.shiptype[currentship][1] = 795000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 0;
    _root.shiptype[currentship][2][0][1] = -10;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 60;
    _root.shiptype[currentship][3][1] = 115;
    _root.shiptype[currentship][3][2] = 80;
    _root.shiptype[currentship][3][3] = 2000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 130;
    _root.shiptype[currentship][3][7] = 2;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 30;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = 7;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 2;
    _root.shiptype[currentship][6][1] = 4;
    _root.shiptype[currentship][6][2] = 3;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 12;
    _root.shiptype[currentship][7][0][3] = 0;
    _root.shiptype[currentship][7][0][5] = 4;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -12;
    _root.shiptype[currentship][7][1][3] = 0;
    _root.shiptype[currentship][7][1][5] = 4;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Alkari Hauler";
    _root.shiptype[currentship][1] = 3595000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 30;
    _root.shiptype[currentship][3][1] = 65;
    _root.shiptype[currentship][3][2] = 45;
    _root.shiptype[currentship][3][3] = 12000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.550000;
    _root.shiptype[currentship][3][6] = 87;
    _root.shiptype[currentship][3][7] = 4;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 1500;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 15;
    _root.shiptype[currentship][5][0][1] = 4;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 0;
    _root.shiptype[currentship][5][1][1] = -8;
    _root.shiptype[currentship][5][2] = new Array();
    _root.shiptype[currentship][5][2][0] = -15;
    _root.shiptype[currentship][5][2][1] = 4;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 6;
    _root.shiptype[currentship][6][2] = 7;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 12;
    _root.shiptype[currentship][7][0][3] = 0;
    _root.shiptype[currentship][7][0][5] = 4;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -12;
    _root.shiptype[currentship][7][1][3] = 0;
    _root.shiptype[currentship][7][1][5] = 4;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Alkari Chaser";
    _root.shiptype[currentship][1] = 2795000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 8;
    _root.shiptype[currentship][2][0][1] = 0;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 0;
    _root.shiptype[currentship][2][1][1] = -12;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -8;
    _root.shiptype[currentship][2][2][1] = 0;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 80;
    _root.shiptype[currentship][3][1] = 100;
    _root.shiptype[currentship][3][2] = 75;
    _root.shiptype[currentship][3][3] = 7000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.300000;
    _root.shiptype[currentship][3][6] = 110;
    _root.shiptype[currentship][3][7] = 4;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 150;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = 10;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 6;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 12;
    _root.shiptype[currentship][7][0][3] = 3;
    _root.shiptype[currentship][7][0][5] = 8;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -12;
    _root.shiptype[currentship][7][1][3] = 3;
    _root.shiptype[currentship][7][1][5] = 8;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Alkari DeathWing";
    _root.shiptype[currentship][1] = 4595000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 10;
    _root.shiptype[currentship][2][0][1] = -11;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 0;
    _root.shiptype[currentship][2][1][1] = -18;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -10;
    _root.shiptype[currentship][2][2][1] = -11;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 30;
    _root.shiptype[currentship][3][1] = 75;
    _root.shiptype[currentship][3][2] = 60;
    _root.shiptype[currentship][3][3] = 10500;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 87;
    _root.shiptype[currentship][3][7] = 8;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 200;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 6;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 14;
    _root.shiptype[currentship][7][0][3] = -3;
    _root.shiptype[currentship][7][0][5] = 6;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = 7;
    _root.shiptype[currentship][7][1][3] = 0;
    _root.shiptype[currentship][7][1][5] = 6;
    _root.shiptype[currentship][7][2] = new Array();
    _root.shiptype[currentship][7][2][2] = -7;
    _root.shiptype[currentship][7][2][3] = 0;
    _root.shiptype[currentship][7][2][5] = 6;
    _root.shiptype[currentship][7][3] = new Array();
    _root.shiptype[currentship][7][3][2] = -14;
    _root.shiptype[currentship][7][3][3] = -3;
    _root.shiptype[currentship][7][3][5] = 6;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Large Freighter";
    _root.shiptype[currentship][1] = 8000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 20;
    _root.shiptype[currentship][3][1] = 85;
    _root.shiptype[currentship][3][2] = 50;
    _root.shiptype[currentship][3][3] = 30000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.500000;
    _root.shiptype[currentship][3][6] = 88;
    _root.shiptype[currentship][3][7] = 2;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 5000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = -12.700000;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = -3.500000;
    _root.shiptype[currentship][5][1][1] = 20;
    _root.shiptype[currentship][5][2] = new Array();
    _root.shiptype[currentship][5][2][0] = 3.500000;
    _root.shiptype[currentship][5][2][1] = 20;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 7;
    _root.shiptype[currentship][6][2] = 8;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -20;
    _root.shiptype[currentship][7][0][5] = 30;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Super Freighter";
    _root.shiptype[currentship][1] = 52000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 15;
    _root.shiptype[currentship][3][1] = 75;
    _root.shiptype[currentship][3][2] = 40;
    _root.shiptype[currentship][3][3] = 68000;
    _root.shiptype[currentship][3][4] = 3;
    _root.shiptype[currentship][3][5] = 0.650000;
    _root.shiptype[currentship][3][6] = 82;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 14000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -9.500000;
    _root.shiptype[currentship][5][0][1] = -32;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 9.500000;
    _root.shiptype[currentship][5][1][1] = -32;
    _root.shiptype[currentship][5][2] = new Array();
    _root.shiptype[currentship][5][2][0] = -10;
    _root.shiptype[currentship][5][2][1] = 27;
    _root.shiptype[currentship][5][3] = new Array();
    _root.shiptype[currentship][5][3][0] = 10;
    _root.shiptype[currentship][5][3][1] = 27;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 8;
    _root.shiptype[currentship][6][1] = 8;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = -9.500000;
    _root.shiptype[currentship][7][0][3] = -32;
    _root.shiptype[currentship][7][0][5] = 30;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = 9.500000;
    _root.shiptype[currentship][7][1][3] = -32;
    _root.shiptype[currentship][7][1][5] = 30;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Armed Freighter";
    _root.shiptype[currentship][1] = 26000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 16.500000;
    _root.shiptype[currentship][2][0][1] = -20;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -16.500000;
    _root.shiptype[currentship][2][1][1] = -20;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 25;
    _root.shiptype[currentship][3][1] = 80;
    _root.shiptype[currentship][3][2] = 50;
    _root.shiptype[currentship][3][3] = 43000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.450000;
    _root.shiptype[currentship][3][6] = 88;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 7000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = -2.500000;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 0;
    _root.shiptype[currentship][5][1][1] = 18;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 7;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 20;
    _root.shiptype[currentship][7][0][3] = -10;
    _root.shiptype[currentship][7][0][5] = 30;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -20;
    _root.shiptype[currentship][7][1][3] = -10;
    _root.shiptype[currentship][7][1][5] = 30;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Light Destroyer";
    _root.shiptype[currentship][1] = 19000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 16;
    _root.shiptype[currentship][2][0][1] = -15;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -16;
    _root.shiptype[currentship][2][1][1] = -15;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 25;
    _root.shiptype[currentship][3][1] = 75;
    _root.shiptype[currentship][3][2] = 60;
    _root.shiptype[currentship][3][3] = 41000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 88;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 2000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = -2;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 0;
    _root.shiptype[currentship][5][1][1] = 14;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 8;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 10;
    _root.shiptype[currentship][7][0][3] = -10;
    _root.shiptype[currentship][7][0][5] = 15;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -10;
    _root.shiptype[currentship][7][1][3] = -10;
    _root.shiptype[currentship][7][1][5] = 15;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Missile Destroyer";
    _root.shiptype[currentship][1] = 26000000;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 25;
    _root.shiptype[currentship][3][1] = 73;
    _root.shiptype[currentship][3][2] = 70;
    _root.shiptype[currentship][3][3] = 38000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.300000;
    _root.shiptype[currentship][3][6] = 85;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 1500;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = -2;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 0;
    _root.shiptype[currentship][5][1][1] = 14;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 7;
    _root.shiptype[currentship][6][1] = 8;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 15;
    _root.shiptype[currentship][7][0][3] = -14;
    _root.shiptype[currentship][7][0][5] = 50;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -15;
    _root.shiptype[currentship][7][1][3] = -14;
    _root.shiptype[currentship][7][1][5] = 50;
    _root.shiptype[currentship][7][2] = new Array();
    _root.shiptype[currentship][7][2][2] = 21;
    _root.shiptype[currentship][7][2][3] = -14;
    _root.shiptype[currentship][7][2][5] = 50;
    _root.shiptype[currentship][7][3] = new Array();
    _root.shiptype[currentship][7][3][2] = -21;
    _root.shiptype[currentship][7][3][3] = -10;
    _root.shiptype[currentship][7][3][5] = 50;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Bird of Prey";
    _root.shiptype[currentship][1] = 63000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 18;
    _root.shiptype[currentship][2][0][1] = -6;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 11;
    _root.shiptype[currentship][2][1][1] = -17;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -11;
    _root.shiptype[currentship][2][2][1] = -17;
    _root.shiptype[currentship][2][3] = new Array();
    _root.shiptype[currentship][2][3][0] = -18;
    _root.shiptype[currentship][2][3][1] = -6;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 15;
    _root.shiptype[currentship][3][1] = 70;
    _root.shiptype[currentship][3][2] = 65;
    _root.shiptype[currentship][3][3] = 45000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.500000;
    _root.shiptype[currentship][3][6] = 110;
    _root.shiptype[currentship][3][7] = 4;
    _root.shiptype[currentship][3][8] = true;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 100;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 7;
    _root.shiptype[currentship][6][1] = 8;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = -25;
    _root.shiptype[currentship][7][0][3] = 10;
    _root.shiptype[currentship][7][0][5] = 15;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = 25;
    _root.shiptype[currentship][7][1][3] = 10;
    _root.shiptype[currentship][7][1][5] = 15;
    _root.shiptype[currentship][7][2] = new Array();
    _root.shiptype[currentship][7][2][2] = -14;
    _root.shiptype[currentship][7][2][3] = 7;
    _root.shiptype[currentship][7][2][5] = 15;
    _root.shiptype[currentship][7][3] = new Array();
    _root.shiptype[currentship][7][3][2] = 14;
    _root.shiptype[currentship][7][3][3] = 7;
    _root.shiptype[currentship][7][3][5] = 15;
    ++currentship;
    ++currentship;
    ++currentship;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Desolator";
    _root.shiptype[currentship][1] = 145000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 14;
    _root.shiptype[currentship][2][0][1] = -30;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -14;
    _root.shiptype[currentship][2][1][1] = -30;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 10;
    _root.shiptype[currentship][3][1] = 62;
    _root.shiptype[currentship][3][2] = 45;
    _root.shiptype[currentship][3][3] = 165000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.600000;
    _root.shiptype[currentship][3][6] = 73;
    _root.shiptype[currentship][3][7] = 10;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 5000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 23;
    _root.shiptype[currentship][5][0][1] = -8;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = -23;
    _root.shiptype[currentship][5][1][1] = -8;
    _root.shiptype[currentship][5][2] = new Array();
    _root.shiptype[currentship][5][2][0] = 25;
    _root.shiptype[currentship][5][2][1] = 20;
    _root.shiptype[currentship][5][3] = new Array();
    _root.shiptype[currentship][5][3][0] = -25;
    _root.shiptype[currentship][5][3][1] = 20;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 8;
    _root.shiptype[currentship][6][1] = 9;
    _root.shiptype[currentship][6][2] = 10;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 7;
    _root.shiptype[currentship][7][0][3] = -10;
    _root.shiptype[currentship][7][0][5] = 40;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -7;
    _root.shiptype[currentship][7][1][3] = -10;
    _root.shiptype[currentship][7][1][5] = 40;
    _root.shiptype[currentship][7][2] = new Array();
    _root.shiptype[currentship][7][2][2] = 7;
    _root.shiptype[currentship][7][2][3] = 6;
    _root.shiptype[currentship][7][2][5] = 40;
    _root.shiptype[currentship][7][3] = new Array();
    _root.shiptype[currentship][7][3][2] = -7;
    _root.shiptype[currentship][7][3][3] = 6;
    _root.shiptype[currentship][7][3][5] = 40;
    _root.tradegoods = new Array();
    _root.tradegoods[0] = new Array();
    _root.tradegoods[0][0] = "Food";
    _root.tradegoods[1] = new Array();
    _root.tradegoods[1][0] = "Wood";
    _root.tradegoods[2] = new Array();
    _root.tradegoods[2][0] = "Iron";
    _root.tradegoods[3] = new Array();
    _root.tradegoods[3][0] = "Machinery";
    _root.tradegoods[4] = new Array();
    _root.tradegoods[4][0] = "Tools";
    _root.tradegoods[5] = new Array();
    _root.tradegoods[5][0] = "Spare Parts";
    _root.tradegoods[6] = new Array();
    _root.tradegoods[6][0] = "Liquor";
    for (i = 0; i < _root.tradegoods.length; i++)
    {
        _root.playershipstatus[4][1][i] = 0;
    } // end of for
    _root.playernameinput = "Enter Name Here";
    _root.gamechatinfo = new Array();
    _root.gamechatinfo[0] = new Array();
    _root.gamechatinfo[0][0] = "";
    _root.gamechatinfo[1] = new Array();
    _root.gamechatinfo[1][0] = new Array();
    _root.gamechatinfo[1][0][0] = "Press enter to type and send a message, F12 for help";
    _root.gamechatinfo[1][0][1] = _root.systemchattextcolor;
    _root.gamechatinfo[1][1] = new Array();
    _root.gamechatinfo[1][1][0] = "Hello,Press D to dock in game, \'HOME\' to extend text";
    _root.gamechatinfo[1][1][1] = _root.systemchattextcolor;
    _root.gamechatinfo[2] = new Array();
    _root.gamechatinfo[2][0] = 5;
    _root.gamechatinfo[2][1] = 17;
    _root.gamechatinfo[2][2] = false;
    _root.gamechatinfo[2][3] = 0;
    _root.gamechatinfo[2][4] = 300000;
    _root.gamechatinfo[3] = new Array();
    _root.gamechatinfo[3][0] = "";
    _root.gamechatinfo[3][1] = false;
    _root.gamechatinfo[4] = false;
    _root.gamechatinfo[5] = "";
    _root.gamechatinfo[6] = new Array();
    _root.gamechatinfo[7] = new Array();
    _root.gamechatinfo[7][0] = new Array();
    _root.gamechatinfo[7][0][0] = -50000;
    _root.gamechatinfo[7][0][1] = -40000;
    _root.gamechatinfo[7][0][2] = -30000;
    _root.gamechatinfo[7][0][3] = -20000;
    _root.gamechatinfo[7][1] = 3000;
    _root.gamechatinfo[7][2] = 10000;
    _root.gameplaystatus = new Array();
    _root.gameplaystatus[0] = new Array();
    _root.gameplaystatus[0][0] = 4;
    _root.gameplaystatus[1] = new Array();
    _root.gameplaystatus[1][0] = 0;
    _root.gameplaystatus[1][1] = 0;
    _root.gameplaystatus[1][2] = 0;
    _root.gameplaystatus[1][3] = 0;
    _root.framestojumpgunfire = _root.gameplaystatus[0][0] + 1;
    _root.currentonlineplayers = new Array();
    _root.currentgamestatus = "";
    _root.aimessages = new Array();
    _root.aimessages[0] = new Array();
    _root.aimessages[0][0] = "Hey Baby, Say Hi to Elvis for me!!!!";
    _root.aimessages[0][1] = "This won\'t hurt a bit, I promise!!!";
    _root.aimessages[0][2] = "I hope you can put up a challenge, The others were boring";
    _root.aimessages[0][3] = "I hope you don\'t whine as much as my last victim";
    _root.aimessages[0][4] = "You look kind of cute, for a dead man";
    _root.aimessages[0][5] = "I almost feel sorry for you, oh well, EAT SOME OF THIS!!";
    _root.aimessages[0][6] = "I wonder if space is cold as they say it is. When you explode, could you tell me?";
    _root.aimessages[0][7] = "Your death will mean so much to me, It will get me into the clan.";
    _root.aimessages[0][8] = "I already have enough friends, So I can just finish you off";
    _root.aimessages[1] = new Array();
    _root.aimessages[1][0] = "WHAT THE?? How did you pull this one off?!?!!?";
    _root.aimessages[1][1] = "AWWWW CRAP! I only had 10 more payments left on this baby";
    _root.aimessages[1][2] = "Darn, looks like you know what your doing... AHHHHHHHH!!!!!";
    _root.aimessages[1][3] = "I COMING ELVIS, IM COMING!";
    _root.aimessages[1][4] = "Well, I guess another one bites the dust";
    _root.aimessages[1][5] = "Couldn\'t you have taken it easy on me??";
    _root.aimessages[1][6] = "Good Game Son,  Good Game!";
    _root.aimessages[1][7] = "Nice fight, Looks like you are the better pilot";
    _root.aimessages[1][8] = "Couldn\'t we have just been friends?";
    _root.ainames = new Array();
    _root.ainames[0] = "Vader";
    _root.ainames[1] = "Scorpio";
    _root.ainames[2] = "Kirk";
    _root.ainames[3] = "Maniac";
    _root.ainames[4] = "Valerie";
    _root.ainames[5] = "Kitty";
    _root.ainames[6] = "Charlie";
    _root.ainames[7] = "Borris";
    _root.ainames[8] = "Melissa";
    _root.ainames[9] = "Borris";
    _root.ainames[10] = "The Evil One";
    _root.currentailvl = 0;
    _root.targetinfo = new Array();
    _root.targetinfo[0] = "None";
    _root.targetinfo[1] = 0;
    _root.targetinfo[2] = 0;
    _root.targetinfo[3] = 0;
    _root.targetinfo[4] = 0;
    _root.targetinfo[5] = 0;
    _root.targetinfo[6] = 0;
    _root.keywaspressed = false;
    _root.sectorinformation[0] = new Array();
    _root.sectorinformation[0][0] = 50;
    _root.sectorinformation[0][1] = 50;
    _root.sectorinformation[1] = new Array();
    _root.sectorinformation[1][0] = 1000;
    _root.sectorinformation[1][1] = 1000;
    _root.portandsystem = null;
    if (_root.isgamerunningfromremote == false)
    {
        newzoneVars = new XML();
        newzoneVars.load("getzone.php");
        newzoneVars.onLoad = function (success)
        {
            loadedvars = String(newzoneVars);
            if (loadedvars < 1)
            {
                loadedvars = 200;
            } // end if
            _root.portandsystem = Number(loadedvars);
        };
    } // end if
}

// [onClipEvent of sprite 1271 in frame 1]
onClipEvent (load)
{
    scenes = new Array();
    scenes[0] = 11000;
    scenes[1] = 5000;
    scenes[2] = 6000;
    scenes[3] = 3000;
    scenes[4] = 8000;
    scenes[5] = 3500;
    scenes[6] = 4000;
    scenes[7] = 6000;
    currentscene = 0;
    timetillnextscene = Number(getTimer()) + scenes[0];
}

// [onClipEvent of sprite 1271 in frame 1]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (timetillnextscene < currenttime)
    {
        ++currentscene;
        if (currentscene > scenes.length - 1)
        {
            currentscene = 0;
        } // end if
        timetillnextscene = currenttime + scenes[currentscene];
        scenes[0] = 15000;
    } // end if
}

// [onClipEvent of sprite 1271 in frame 1]
onClipEvent (load)
{
    function toptenprocess(toptennames)
    {
        newinfo = toptennames.split("~");
        i = 0;
        topten = "Top Ten Players:\r";
        while (i < newinfo.length - 1)
        {
            currentthread = newinfo[i].split("`");
            topten = topten + (i + 1 + ": " + currentthread[0] + " - " + Math.floor(Number(currentthread[1]) / _root.scoreratiomodifier) + "\r");
            ++i;
        } // end while
    } // End of the function
    topten = "Loading Top Ten";
    myLoadVars = new XML();
    myLoadVars.load(_root.pathtoaccounts + "accounts.php?mode=topten");
    myLoadVars.onLoad = function (success)
    {
        toptennames = String(myLoadVars);
        toptenprocess(toptennames);
    };
}

// [onClipEvent of sprite 1387 in frame 1]
onClipEvent (load)
{
    this._xscale = 70;
    this._yscale = 70;
}

// [onClipEvent of sprite 1389 in frame 1]
onClipEvent (load)
{
    function func_maplisterkey(incomingvariable)
    {
        if (_root.playershipstatus[5][1] != null)
        {
            keypressed = Key.getCode();
            if (incomingvariable == "buttonpress")
            {
                keypressed = "119";
            } // end if
            if (keypressed == "119")
            {
                if (_root.mapdsiplayed == false)
                {
                    _root.mapdsiplayed = true;
                    _root.attachMovie("ingamemap", "ingamemap", 9999999);
                }
                else
                {
                    _root.mapdsiplayed = false;
                    _root.createEmptyMovieClip("blanker", 9999999);
                } // end if
            } // end if
        } // end else if
    } // End of the function
    themapkeyListener = new Object();
    themapkeyListener.onKeyDown = function ()
    {
        func_maplisterkey();
    };
    Key.addListener(themapkeyListener);
}

// [onClipEvent of sprite 1392 in frame 1]
onClipEvent (load)
{
    function saveplayersgame(sender)
    {
        _root.func_checkfortoomuchaddedandset();
        _root.savegamesenderfrom = sender;
        if (savestatus == "Save Game")
        {
            savestatus = sender.savestatus = "Saving";
            variablestosave = "PI" + _root.playershipstatus[3][0] + "`ST" + _root.playershipstatus[5][0] + "`SG" + _root.playershipstatus[2][0] + "`EG" + _root.playershipstatus[1][0] + "`EC" + _root.playershipstatus[1][5] + "`CR" + _root.playershipstatus[3][1] + "`SE" + _root.playershipstatus[5][1] + "`" + _root.playershipstatus[4][0];
            for (currenthardpoint = 0; currenthardpoint < _root.playershipstatus[0].length; currenthardpoint++)
            {
                variablestosave = variablestosave + ("`HP" + currenthardpoint + "G" + _root.playershipstatus[0][currenthardpoint][0]);
            } // end of for
            for (currentturretpoint = 0; currentturretpoint < _root.playershipstatus[8].length; currentturretpoint++)
            {
                variablestosave = variablestosave + ("`TT" + currentturretpoint + "G" + _root.playershipstatus[8][currentturretpoint][0]);
            } // end of for
            for (currentcargo = 0; currentcargo < _root.playershipstatus[4][1].length; currentcargo++)
            {
                if (_root.playershipstatus[4][1][currentcargo] > 0)
                {
                    variablestosave = variablestosave + ("`CO" + currentcargo + "A" + _root.playershipstatus[4][1][currentcargo]);
                } // end if
            } // end of for
            for (spno = 0; spno < _root.playershipstatus[11][1].length; spno++)
            {
                variablestosave = variablestosave + ("`SP" + _root.playershipstatus[11][1][spno][0] + "Q" + _root.playershipstatus[11][1][spno][1]);
            } // end of for
            variablestosave = variablestosave + "~";
            if (_root.isplayeraguest == false)
            {
                _root.saveplayerextraships();
                if (_root.lastplayerssavedinfosent == "info=" + variablestosave + "&score=" + _root.playershipstatus[5][9] + "&shipsdata=" + _root.playersextrashipsdata)
                {
                    sender.savestatus = "No Updates";
                    savestatus = "Save Game";
                }
                else
                {
                    _root.lastplayerssavedinfosent = "info=" + variablestosave + "&score=" + _root.playershipstatus[5][9] + "&shipsdata=" + _root.playersextrashipsdata;
                    datatosend = "savegame`~:" + _root.playershipstatus[3][2] + ":" + _root.playershipstatus[3][3] + ":" + variablestosave + ":" + _root.playershipstatus[5][9] + ":" + _root.playersextrashipsdata + ":" + _root.playershipstatus[3][1] + ":" + _root.playershipstatus[5][19] + ":";
                    accountserv = new XMLSocket();
                    if (_root.isgamerunningfromremote == false)
                    {
                        currenturl = String(_root._url);
                        if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
                        {
                            accountserv.connect("", _root.accountserverport);
                        } // end if
                    }
                    else
                    {
                        accountserv.connect(_root.accountserveraddy, _root.accountserverport);
                    } // end else if
                    accountserv.onConnect = function (success)
                    {
                        okbutton._visible = true;
                        accountserv.send(datatosend);
                    };
                    accountserv.onXML = xmlhandler;
                } // end if
            } // end if
        } // end else if
    } // End of the function
    function xmlhandler(doc)
    {
        loadedvars = String(doc);
        info = loadedvars.split("~");
        newinfo = info[0].split("`");
        if (newinfo[0] == "savesuccess")
        {
            _root.savegamescript.savestatus = "Save Game";
            _root.savegamesenderfrom.savestatus = "Game Saved";
            _root.playershipstatus[5][19] = newinfo[1];
        }
        else
        {
            _root.savegamescript.savestatus = _root.savegamesenderfrom.savestatus = "Save Failed";
            _root.func_disconnectuser("savefailure");
        } // end else if
    } // End of the function
    this._visible = false;
    status = "N/A";
    savestatus = "Save Game";
}

// [Action in Frame 1]
function func_checkfortoomuchaddedandset()
{
    if (_root.playershipstatus[3][1] < 1)
    {
        _root.playershipstatus[3][1] = 1;
    } // end if
    oldnumbs = func_converttonumberfund(_root.oldcodedfunds);
    if (_root.playershipstatus[3][1] - oldnumbs > maxjfundsjump)
    {
        _root.playershipstatus[3][1] = Number(oldnumbs);
    }
    else
    {
        func_setoldfundsuptocurrentammount();
    } // end else if
} // End of the function
function func_setoldfundsuptocurrentammount()
{
    _root.oldcodedfunds = func_converttocodedfund(_root.playershipstatus[3][1]);
} // End of the function
function func_converttocodedfund(anumber)
{
    anumber = String(anumber);
    letteredfund = "";
    for (qtt = 0; qtt < anumber.length; qtt++)
    {
        numcharactertouse = anumber.charAt(qtt);
        letteredfund = letteredfund + fundcodedconversionchars[Number(numcharactertouse)];
    } // end of for
    return (letteredfund);
} // End of the function
function func_converttonumberfund(astring)
{
    astring = String(astring);
    stringnumberfund = "";
    for (qtt = 0; qtt < astring.length; qtt++)
    {
        lettertocheck = astring.charAt(qtt);
        for (jjp = 0; jjp <= fundcodedconversionchars.length; jjp++)
        {
            if (lettertocheck == fundcodedconversionchars[jjp])
            {
                stringnumberfund = stringnumberfund + String(jjp);
                jjp = 999;
            } // end if
        } // end of for
    } // end of for
    return (Number(stringnumberfund));
} // End of the function
maxjfundsjump = 25000000;
_root.oldcodedfunds = "";
fundcodedconversionchars = new Array("A", "B", "T", "Q", "R", "S", "P", "M", "N", "E");

function func_disconnectuser(reason)
{
    _root.controlledserverclose = true;
    _root.gameerror = reason;
    _root.func_closegameserver();
} // End of the function

function func_keyboardkeysound()
{
    keyboardkeysound.start();
} // End of the function
function func_main_clicksound()
{
    if (_root.soundvolume != "off")
    {
        main_clicksound.start();
    } // end if
} // End of the function
function func_minor_clicksound()
{
    if (_root.soundvolume != "off")
    {
        minor_clicksound.start();
    } // end if
} // End of the function
function func_enterpresssound()
{
    if (_root.soundvolume != "off")
    {
        enterpresssound.start();
    } // end if
} // End of the function
function func_login_button_press_sound()
{
    login_button_press_sound.start();
} // End of the function
function func_connecting_sound()
{
    connecting_sound.start();
} // End of the function
function func_error_sound()
{
    if (_root.soundvolume != "off")
    {
        error_sound.start();
    } // end if
} // End of the function
function func_login_but_pops_sound()
{
    if (_root.soundvolume != "off")
    {
        login_but_pops_sound.start();
    } // end if
} // End of the function
function func_login_opening_sound()
{
    if (_root.soundvolume != "off")
    {
        login_opening_sound.start();
    } // end if
} // End of the function
function func_keyboardkeysound()
{
    keyboardkeysound.start();
} // End of the function
function func_main_clicksound()
{
    if (_root.soundvolume != "off")
    {
        main_clicksound.start();
    } // end if
} // End of the function
function func_minor_clicksound()
{
    if (_root.soundvolume != "off")
    {
        minor_clicksound.start();
    } // end if
} // End of the function
function func_enterpresssound()
{
    if (_root.soundvolume != "off")
    {
        enterpresssound.start();
    } // end if
} // End of the function
function func_connecting_sound()
{
    connecting_sound.start();
} // End of the function
function func_error_sound()
{
    if (_root.soundvolume != "off")
    {
        error_sound.start();
    } // end if
} // End of the function
_root.musicvolume = "on";
keyboardkeysound = new Sound();
keyboardkeysound.attachSound("typekeypress.wav");
main_clicksound = new Sound();
main_clicksound.attachSound("main click.wav");
minor_clicksound = new Sound();
minor_clicksound.attachSound("minor click.wav");
enterpresssound = new Sound();
enterpresssound.attachSound("enterpress.wav");
login_button_press_sound = new Sound();
login_button_press_sound.attachSound("login button press.mp3");
connecting_sound = new Sound();
connecting_sound.attachSound("Conneting Sound.mp3");
error_sound = new Sound();
error_sound.attachSound("error.wav");
login_but_pops_sound = new Sound();
login_but_pops_sound.attachSound("login but pops.wav");
login_opening_sound = new Sound();
login_opening_sound.attachSound("opening.wav");
keyboardkeysound = new Sound();
keyboardkeysound.attachSound("typekeypress.wav");
main_clicksound = new Sound();
main_clicksound.attachSound("main click.wav");
minor_clicksound = new Sound();
minor_clicksound.attachSound("minor click.wav");
enterpresssound = new Sound();
enterpresssound.attachSound("enterpress.wav");
connecting_sound = new Sound();
connecting_sound.attachSound("Conneting Sound.mp3");
error_sound = new Sound();
error_sound.attachSound("error.wav");

function enterbadwords()
{
    badwords = new Array();
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "fuck";
    badwords[loc][1] = "f**k";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "shit";
    badwords[loc][1] = "s**t";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "asshole";
    badwords[loc][1] = "a**h*le";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "nigger";
    badwords[loc][1] = "ni**er";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "whore";
    badwords[loc][1] = "wh**e";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "cunt";
    badwords[loc][1] = "c*nt";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "cock";
    badwords[loc][1] = "c*ck";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "pussy";
    badwords[loc][1] = "pu**y";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "fag";
    badwords[loc][1] = "f*g";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "bitch";
    badwords[loc][1] = "b**ch";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "rape";
    badwords[loc][1] = "r*pe";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "homo";
    badwords[loc][1] = "h*m*";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "jackass";
    badwords[loc][1] = "j**k*ss";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "fuk";
    badwords[loc][1] = "f*k";
} // End of the function
function badwordfiltering(phraseinput)
{
    phrasetocheckwith = String(phraseinput.toLowerCase());
    for (qq = 0; qq < phraseinput.length; qq++)
    {
        for (jj = 0; jj < badwords.length; jj++)
        {
            if (phrasetocheckwith.substr(qq, badwords[jj][0].length) == badwords[jj][0])
            {
                beginnign = phraseinput.substr(0, qq);
                ending = phraseinput.substr(qq + badwords[jj][0].length);
                phraseinput = beginnign + badwords[jj][1] + ending;
                break;
            } // end if
        } // end of for
    } // end of for
    return (phraseinput);
} // End of the function
function func_checkothercharacters(phraseoutput)
{
    for (qq = 0; qq < phraseoutput.length; qq++)
    {
        for (jj = 0; jj < replacewithchar.length; jj++)
        {
            if (phraseoutput.substr(qq, replacewithchar[jj][0].length) == replacewithchar[jj][0])
            {
                beginnign = phraseoutput.substr(0, qq);
                ending = phraseoutput.substr(qq + replacewithchar[jj][0].length);
                phraseoutput = beginnign + replacewithchar[jj][1] + ending;
                if (qq >= phraseoutput.length)
                {
                    qq = 10000;
                } // end if
                break;
            } // end if
        } // end of for
    } // end of for
    return (phraseoutput);
} // End of the function
enterbadwords();
replacewithchar = new Array();
loc = replacewithchar.length;
replacewithchar[loc] = new Array();
replacewithchar[loc][0] = "&apos;";
replacewithchar[loc][1] = "\'";
loc = replacewithchar.length;
replacewithchar[loc] = new Array();
replacewithchar[loc][0] = "&quot;";
replacewithchar[loc][1] = "\"";
loc = replacewithchar.length;
replacewithchar[loc] = new Array();
replacewithchar[loc][0] = "&gt;";
replacewithchar[loc][1] = ">";
loc = replacewithchar.length;
replacewithchar[loc] = new Array();
replacewithchar[loc][0] = "&amp;";
replacewithchar[loc][1] = "&";

function func_beginmeteor()
{
    meteortarget = null;
    checkattempts = 30;
    for (ww = 0; ww < checkattempts; ww++)
    {
        targetnumber = Math.round(Math.random() * (_root.starbaselocation.length - 1));
        if (_root.starbaselocation[targetnumber][5] == "ACTIVE" && _root.starbaselocation[targetnumber][0].substr(0, 2) == "SB")
        {
            meteortarget = targetnumber;
            break;
        } // end if
    } // end of for
    if (meteortarget != null && _root.incomingmeteor.length < 4)
    {
        targetname = _root.starbaselocation[targetnumber][0];
        meteortype = Math.round(Math.random() * (destroyingmeteor.length - 1));
        meteorincomingangle = Math.round(Math.random() * 360);
        idtosend = "*M" + Math.round(Math.random() * 99);
        meteorlife = destroyingmeteor[meteortype][3];
        meteormovingagle = meteorincomingangle - 180;
        meteorlifetimeinsec = Math.round(destroyingmeteor[meteortype][1] / 1000);
        if (meteormovingagle < 0)
        {
            meteormovingagle = meteormovingagle + 360;
        } // end if
        timetillhit = Math.round(destroyingmeteor[meteortype][1] / 1000);
        datatosend = "MET`CREATE`" + idtosend + "`" + meteormovingagle + "`" + targetname + "`" + meteortype + "`" + meteorlife + "`" + timetillhit + "`" + _root.errorchecknumber + "~";
        _root.mysocket.send(datatosend);
        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
    } // end if
} // End of the function
function func_meteoricoming(meteorid, meteorheading, targetname, meteortype, meterorlife, timeelapsed)
{
    meteorid = String(meteorid);
    idtaken = false;
    for (qqq = 0; qqq < _root.incomingmeteor.length; qqq++)
    {
        if (meteorid == _root.incomingmeteor[qqq][0])
        {
            idtaken = true;
            break;
        } // end if
    } // end of for
    if (idtaken == false)
    {
        if (_root.incomingmeteor.length < 1)
        {
            _root.incomingmeteor = new Array();
        } // end if
        meteortype = Number(meteortype);
        timeelapsed = Number(timeelapsed);
        meteorheading = Number(meteorheading);
        for (ww = 0; ww < _root.starbaselocation.length; ww++)
        {
            if (String(targetname) == _root.starbaselocation[ww][0])
            {
                targetnumber = ww;
                break;
            } // end if
        } // end of for
        timetillhit = destroyingmeteor[meteortype][1] - timeelapsed * 1000;
        meterortargetx = _root.starbaselocation[targetnumber][1];
        meterortargety = _root.starbaselocation[targetnumber][2];
        velocity = destroyingmeteor[meteortype][0];
        ymovementpersec = -velocity * _root.cosines[meteorheading];
        xmovementpersec = velocity * _root.sines[meteorheading];
        currenty = Number(meterortargety) - destroyingmeteor[meteortype][1] / 1000 * ymovementpersec;
        currentx = Number(meterortargetx) - destroyingmeteor[meteortype][1] / 1000 * xmovementpersec;
        location = _root.incomingmeteor.length;
        _root.incomingmeteor[location] = new Array();
        _root.incomingmeteor[location][0] = meteorid;
        _root.incomingmeteor[location][1] = currentx;
        _root.incomingmeteor[location][2] = currenty;
        _root.incomingmeteor[location][3] = meteorheading;
        _root.incomingmeteor[location][4] = targetname;
        _root.incomingmeteor[location][5] = timetillimpact;
        _root.incomingmeteor[location][6] = meterorlife;
        _root.incomingmeteor[location][7] = meteortype;
        _root.incomingmeteor[location][8] = getTimer();
        _root.incomingmeteor[location][9] = getTimer() + (destroyingmeteor[meteortype][1] - timeelapsed * 1000) - timetocheckmeteorbeforehitting;
        _root.incomingmeteor[location][10] = xmovementpersec;
        _root.incomingmeteor[location][11] = ymovementpersec;
        _root.incomingmeteor[location][12] = getTimer() + (destroyingmeteor[meteortype][1] - timeelapsed * 1000);
        _root.incomingmeteor[location][13] = destroyingmeteor[meteortype][2];
        timetillhitleft = Math.round((_root.incomingmeteor[location][12] - getTimer()) / 1000);
        message = "Meteor Located: Target " + targetname + " ETA: " + timetillhitleft + " s, Check Map for location.";
        _root.enterintochat(message, _root.systemchattextcolor);
        _root.gamedisplayarea.func_meteordraw("ADD", meteorid);
        newsmessage = "There are reports of a meteor heading towards " + targetname + ". Its arrival time is " + " in " + timetillhitleft + " seconds. It will destroy the base if not stopped!";
        _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
    } // end if
} // End of the function
function func_meteorhitbase(meteorid)
{
    for (wq = 0; wq < _root.incomingmeteor.length; wq++)
    {
        if (meteorid == _root.incomingmeteor[wq][0])
        {
            destroyedbase = _root.incomingmeteor[wq][4];
            destroyedtime = getTimer() + _root.incomingmeteor[wq][13];
            for (hfd = 0; hfd < _root.starbaselocation.length; hfd++)
            {
                if (destroyedbase == _root.starbaselocation[hfd][0])
                {
                    if (_root.starbaselocation[hfd][5] == "ACTIVE")
                    {
                        _root.starbaselocation[hfd][5] = destroyedtime;
                        message = "Starbase " + destroyedbase + " was destroyed by a meteor for " + Math.round(_root.incomingmeteor[wq][13] / 1000) + " seconds";
                        _root.enterintochat(message, _root.systemchattextcolor);
                        _root.gamedisplayarea.func_meteordraw("REMOVE", meteorid);
                        newsmessage = "Starbase " + targetname + " has been destroyed by a meteor. It will take " + Math.round(_root.incomingmeteor[wq][13] / 1000) + " seconds until it will be repaired.";
                        _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
                        _root.gamedisplayarea.gamebackground[destroyedbase].basedestroyed = true;
                    }
                    else
                    {
                        message = "Starbase " + destroyedbase + " was already destroyed by a meteor";
                        _root.enterintochat(message, _root.systemchattextcolor);
                        _root.gamedisplayarea.func_meteordraw("REMOVE", meteorid);
                    } // end else if
                    break;
                } // end if
            } // end of for
            break;
        } // end if
    } // end of for
} // End of the function
function func_meteordamaged(meteorid, destroyedbase)
{
    _root.gamedisplayarea.func_meteordraw("REMOVE", meteorid);
    message = "A meteor was destroyed";
    _root.enterintochat(message, _root.systemchattextcolor);
} // End of the function
function func_removemeteor(meteorid)
{
} // End of the function
function func_meteorlife(meteorid, meteorlife)
{
    meteorlife = Number(meteorlife);
    for (location = 0; location < _root.incomingmeteor.length; location++)
    {
        if (_root.incomingmeteor[location][0] == meteorid)
        {
            if (_root.incomingmeteor[location][6] > meteorlife)
            {
                _root.incomingmeteor[location][6] = meteorlife;
                _root.gamedisplayarea.gamebackground["meteor" + meteorid].func_currentlife(meteorlife);
                break;
            } // end if
        } // end if
    } // end of for
} // End of the function
function func_meteordestroyer(meteorid, killer)
{
    for (location = 0; location < _root.incomingmeteor.length; location++)
    {
        if (_root.incomingmeteor[location][0] == meteorid)
        {
            _root.gamedisplayarea.func_meteordraw("DESTROYED", meteorid);
            meteortype = _root.incomingmeteor[location][7];
            target = _root.incomingmeteor[location][4];
            newsmessage = "A meteor heading towards Starbase " + targetname + " has been destroyed by " + func_playersrealname(killer) + " for " + destroyingmeteor[meteortype][4] + " funds and " + destroyingmeteor[meteortype][5] + " score";
            _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
            message = "A meteor was destroyed by " + func_playersrealname(killer);
            _root.enterintochat(message, _root.systemchattextcolor);
            _root.incomingmeteor.splice(location, 1);
            --location;
            if (String(_root.playershipstatus[3][0]) == String(killer))
            {
                datatosend = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + destroyingmeteor[meteortype][5] * _root.scoreratiomodifier;
                "~";
                _root.mysocket.send(datatosend);
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(destroyingmeteor[meteortype][4]);
                _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
            } // end if
        } // end if
    } // end of for
} // End of the function
currentno = 0;
currentmeteordrawlevel = 0;
maxmeteordrawlevel = 20;
timetocheckmeteorbeforehitting = 12000;
destroyingmeteor = new Array();
destroyingmeteor[currentno] = new Array();
destroyingmeteor[currentno][0] = 30;
destroyingmeteor[currentno][1] = 120000;
destroyingmeteor[currentno][2] = 180000;
destroyingmeteor[currentno][3] = 50000;
destroyingmeteor[currentno][4] = 65000;
destroyingmeteor[currentno][5] = 150;
++currentno;
destroyingmeteor[currentno] = new Array();
destroyingmeteor[currentno][0] = 20;
destroyingmeteor[currentno][1] = 180000;
destroyingmeteor[currentno][2] = 240000;
destroyingmeteor[currentno][3] = 100000;
destroyingmeteor[currentno][4] = 130000;
destroyingmeteor[currentno][5] = 220;

function changetonewship(extrano)
{
    i = _root.extraplayerships[extrano][0];
    if (_root.playershipstatus[5][0] == i)
    {
    }
    else
    {
        _root.playershipstatus[5][0] = i;
        _root.playershipstatus[4][1] = new Array();
    } // end else if
    _root.playershiprotation = _root.shiptype[i][3][2];
    _root.playershipmaxvelocity = _root.shiptype[i][3][1];
    _root.playershipacceleration = _root.shiptype[i][3][0];
    _root.playershipstatus[2][4] = _root.shiptype[i][3][3];
    _root.playershipstatus[0] = new Array();
    for (currentharpoint = 0; currentharpoint < _root.shiptype[i][2].length; currentharpoint++)
    {
        _root.playershipstatus[0][currentharpoint] = new Array();
        _root.playershipstatus[0][currentharpoint][0] = _root.extraplayerships[extrano][4][currentharpoint];
        _root.playershipstatus[0][currentharpoint][1] = 0;
        _root.playershipstatus[0][currentharpoint][2] = _root.shiptype[i][2][currentharpoint][0];
        _root.playershipstatus[0][currentharpoint][3] = _root.shiptype[i][2][currentharpoint][1];
    } // end of for
    currentturretpoint = 0;
    _root.playershipstatus[8] = new Array();
    while (currentturretpoint < _root.shiptype[i][5].length)
    {
        _root.playershipstatus[8][currentturretpoint] = new Array();
        _root.playershipstatus[8][currentturretpoint][0] = _root.extraplayerships[extrano][5][currentturretpoint];
        ++currentturretpoint;
    } // end while
    _root.playershipstatus[11] = new Array();
    _root.playershipstatus[11][0] = new Array();
    _root.playershipstatus[11][0][0] = 0;
    _root.playershipstatus[11][0][1] = 0;
    _root.playershipstatus[11][1] = new Array();
    _root.playershipstatus[11][2] = new Array();
    _root.playershipstatus[11][2][0] = new Array();
    _root.playershipstatus[11][2][0][0] = 0;
    _root.playershipstatus[11][2][1] = new Array();
    _root.playershipstatus[11][2][1][0] = 0;
    _root.playershipstatus[11][2][2] = new Array();
    _root.playershipstatus[11][2][2][0] = 0;
    for (spno = 0; spno < _root.extraplayerships[extrano][11][1].length; spno++)
    {
        _root.playershipstatus[11][1][spno] = new Array();
        _root.playershipstatus[11][1][spno][0] = _root.extraplayerships[extrano][11][1][spno][0];
        _root.playershipstatus[11][1][spno][1] = _root.extraplayerships[extrano][11][1][spno][1];
    } // end of for
    _root.playershipstatus[2][0] = _root.extraplayerships[extrano][1];
    _root.playershipstatus[1][0] = _root.extraplayerships[extrano][2];
    _root.playershipstatus[1][5] = _root.extraplayerships[extrano][3];
} // End of the function
function savecurrenttoextraship(extrano)
{
    _root.extraplayerships[extrano][0] = _root.playershipstatus[5][0];
    i = _root.playershipstatus[5][0];
    _root.extraplayerships[extrano][4] = new Array();
    for (currentharpoint = 0; currentharpoint < _root.shiptype[i][2].length; currentharpoint++)
    {
        _root.extraplayerships[extrano][4][currentharpoint] = _root.playershipstatus[0][currentharpoint][0];
    } // end of for
    currentturretpoint = 0;
    _root.extraplayerships[extrano][5] = new Array();
    while (currentturretpoint < _root.shiptype[i][5].length)
    {
        _root.extraplayerships[extrano][5][currentturretpoint] = _root.playershipstatus[8][currentturretpoint][0];
        ++currentturretpoint;
    } // end while
    spno = 0;
    _root.extraplayerships[extrano][11] = new Array();
    _root.extraplayerships[extrano][11][1] = new Array();
    while (spno < _root.playershipstatus[11][1].length)
    {
        _root.extraplayerships[extrano][11][1][spno] = new Array();
        _root.extraplayerships[extrano][11][1][spno][0] = _root.playershipstatus[11][1][spno][0];
        _root.extraplayerships[extrano][11][1][spno][1] = _root.playershipstatus[11][1][spno][1];
        ++spno;
    } // end while
    _root.extraplayerships[extrano][1] = _root.playershipstatus[2][0];
    _root.extraplayerships[extrano][2] = _root.playershipstatus[1][0];
    _root.extraplayerships[extrano][3] = _root.playershipstatus[1][5];
} // End of the function
function loadplayerextraships(loadedvars)
{
    newinfo = loadedvars.split("~");
    i = 0;
    _root.extraplayerships = new Array();
    for (i = 0; i < maxextraships; i++)
    {
        _root.extraplayerships[i] = new Array();
        _root.extraplayerships[i][0] = null;
    } // end of for
    _root.playerscurrentextrashipno = newinfo[0].substr(2);
    for (i = 0; i + 1 < newinfo.length - 1; i++)
    {
        if (newinfo[i + 1].substr(0, 2) == "SH")
        {
            currentthread = newinfo[i + 1].split("`");
            if (currentthread[1].substr(0, 2) == "ST")
            {
                currentshipid = Number(currentthread[0].substr("2"));
                _root.extraplayerships[currentshipid] = new Array();
                _root.extraplayerships[currentshipid][0] = int(currentthread[1].substr("2"));
                _root.extraplayerships[currentshipid][1] = int(currentthread[2].substr("2"));
                _root.extraplayerships[currentshipid][2] = int(currentthread[3].substr("2"));
                _root.extraplayerships[currentshipid][3] = int(currentthread[4].substr("2"));
                tr = 5;
                _root.extraplayerships[currentshipid][4] = new Array();
                for (currentharpoint = 0; currentharpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][2].length; currentharpoint++)
                {
                    _root.extraplayerships[currentshipid][4][currentharpoint] = "none";
                } // end of for
                while (currentthread[tr].substr(0, 2) == "HP")
                {
                    guninfo = currentthread[tr].split("G");
                    currenthardpoint = guninfo[0].substr("2");
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.extraplayerships[currentshipid][4][currenthardpoint] = guninfo[1];
                    ++tr;
                } // end while
                _root.extraplayerships[currentshipid][5] = new Array();
                currentturretpoint = 0;
                _root.extraplayerships[currentshipid][5] = new Array();
                while (currentturretpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][5].length)
                {
                    _root.extraplayerships[currentshipid][5][currentturretpoint] = "none";
                    ++currentturretpoint;
                } // end while
                while (currentthread[tr].substr(0, 2) == "TT")
                {
                    guninfo = currentthread[tr].split("G");
                    currentturretpoint = guninfo[0].substr("2");
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.extraplayerships[currentshipid][5][currentturretpoint] = guninfo[1];
                    ++tr;
                } // end while
                spno = 0;
                _root.extraplayerships[currentshipid][11] = new Array();
                _root.extraplayerships[currentshipid][11][1] = new Array();
                while (currentthread[tr].substr(0, 2) == "SP")
                {
                    cargoinfo = currentthread[tr].split("Q");
                    currentspecialtype = Number(cargoinfo[0].substr("2"));
                    specialquantity = Number(cargoinfo[1]);
                    _root.extraplayerships[currentshipid][11][1][spno] = new Array();
                    _root.extraplayerships[currentshipid][11][1][spno][0] = currentspecialtype;
                    _root.extraplayerships[currentshipid][11][1][spno][1] = specialquantity;
                    ++spno;
                    ++tr;
                } // end while
            } // end if
        } // end if
    } // end of for
    if (_root.playerscurrentextrashipno != "capital")
    {
        if (isNaN(Number(_root.playerscurrentextrashipno)) || Number(_root.playerscurrentextrashipno) > maxextraships - 1)
        {
            for (i = 0; i < maxextraships; i++)
            {
                if (_root.extraplayerships[i][0] != null && !isNaN(Number(_root.playerscurrentextrashipno)))
                {
                    _root.playerscurrentextrashipno = i;
                    break;
                } // end if
            } // end of for
        } // end if
        _root.changetonewship(_root.playerscurrentextrashipno);
    } // end if
} // End of the function
function saveplayerextraships()
{
    saveinfo = "";
    saveinfo = saveinfo + ("NO" + _root.playerscurrentextrashipno + "~");
    for (currentshipid = 0; currentshipid < maxextraships; currentshipid++)
    {
        if (_root.extraplayerships[currentshipid][0] != null)
        {
            saveinfo = saveinfo + ("SH" + currentshipid + "`");
            saveinfo = saveinfo + ("ST" + _root.extraplayerships[currentshipid][0] + "`");
            saveinfo = saveinfo + ("SG" + _root.extraplayerships[currentshipid][1] + "`");
            saveinfo = saveinfo + ("EG" + _root.extraplayerships[currentshipid][2] + "`");
            saveinfo = saveinfo + ("EC" + _root.extraplayerships[currentshipid][3] + "`");
            for (currentharpoint = 0; currentharpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][2].length; currentharpoint++)
            {
                saveinfo = saveinfo + ("HP" + currentharpoint + "G" + _root.extraplayerships[currentshipid][4][currentharpoint] + "`");
            } // end of for
            for (currentturretpoint = 0; currentturretpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][5].length; currentturretpoint++)
            {
                saveinfo = saveinfo + ("TT" + currentturretpoint + "G" + _root.extraplayerships[currentshipid][5][currentturretpoint] + "`");
            } // end of for
            for (spno = 0; spno < _root.extraplayerships[currentshipid][11][1].length; spno++)
            {
                saveinfo = saveinfo + ("SP" + _root.extraplayerships[currentshipid][11][1][spno][0] + "Q" + _root.extraplayerships[currentshipid][11][1][spno][1] + "`");
            } // end of for
            saveinfo = saveinfo + "~";
        } // end if
    } // end of for
    _root.playersextrashipsdata = saveinfo;
} // End of the function
Stage.showMenu = false;
maxextraships = 3;

_root.specialshipitems = new Array();
_root.pulsarseldammodier = 1;
_root.pulsarsinzone = false;
itemno = 0;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Flare 1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 200;
_root.specialshipitems[itemno][3] = 10000;
_root.specialshipitems[itemno][4] = 3000;
_root.specialshipitems[itemno][5] = "FLARE";
_root.specialshipitems[itemno][6] = 100000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "specialflare1";
_root.specialshipitems[itemno][10] = 0.120000;
_root.specialshipitems[itemno][11] = 1000;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Flare 2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 400;
_root.specialshipitems[itemno][3] = 10000;
_root.specialshipitems[itemno][4] = 4000;
_root.specialshipitems[itemno][5] = "FLARE";
_root.specialshipitems[itemno][6] = 280000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "specialflare2";
_root.specialshipitems[itemno][10] = 0.230000;
_root.specialshipitems[itemno][11] = 1500;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Flare 3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 600;
_root.specialshipitems[itemno][3] = 10000;
_root.specialshipitems[itemno][4] = 5000;
_root.specialshipitems[itemno][5] = "FLARE";
_root.specialshipitems[itemno][6] = 750000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "specialflare3";
_root.specialshipitems[itemno][10] = 0.330000;
_root.specialshipitems[itemno][11] = 2000;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Detector 1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 100;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "DETECTOR";
_root.specialshipitems[itemno][6] = 350000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.200000;
_root.specialshipitems[itemno][11] = 10000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Detector 2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 220;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "DETECTOR";
_root.specialshipitems[itemno][6] = 750000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.300000;
_root.specialshipitems[itemno][11] = 8000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Detector 3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 325;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "DETECTOR";
_root.specialshipitems[itemno][6] = 2150000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.400000;
_root.specialshipitems[itemno][11] = 7000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Stealth 1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 75;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "STEALTH";
_root.specialshipitems[itemno][6] = 250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.600000;
_root.specialshipitems[itemno][11] = 20000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Stealth 2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 175;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "STEALTH";
_root.specialshipitems[itemno][6] = 1250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.750000;
_root.specialshipitems[itemno][11] = 15000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Stealth 3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 250;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "STEALTH";
_root.specialshipitems[itemno][6] = 2500000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.900000;
_root.specialshipitems[itemno][11] = 10000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Cloak+Stealth 1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 150;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "CLOAK";
_root.specialshipitems[itemno][6] = 500000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.660000;
_root.specialshipitems[itemno][11] = 9000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Cloak+Stealth 2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 350;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "CLOAK";
_root.specialshipitems[itemno][6] = 1750000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.850000;
_root.specialshipitems[itemno][11] = 7500;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Cloak+Stealth 3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 650;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "CLOAK";
_root.specialshipitems[itemno][6] = 4200000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.950000;
_root.specialshipitems[itemno][11] = 6000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Pulsar-1";
_root.specialshipitems[itemno][1] = 3;
_root.specialshipitems[itemno][2] = 400;
_root.specialshipitems[itemno][3] = 1000;
_root.specialshipitems[itemno][4] = 5000;
_root.specialshipitems[itemno][5] = "PULSAR";
_root.specialshipitems[itemno][6] = 50000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "pulsar1";
_root.specialshipitems[itemno][10] = 0.950000;
_root.specialshipitems[itemno][11] = 4000;
_root.specialshipitems[itemno][12] = 150;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Pulsar-2";
_root.specialshipitems[itemno][1] = 2;
_root.specialshipitems[itemno][2] = 600;
_root.specialshipitems[itemno][3] = 1500;
_root.specialshipitems[itemno][4] = 7000;
_root.specialshipitems[itemno][5] = "PULSAR";
_root.specialshipitems[itemno][6] = 125000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "pulsar2";
_root.specialshipitems[itemno][10] = 0.950000;
_root.specialshipitems[itemno][11] = 7000;
_root.specialshipitems[itemno][12] = 225;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Pulsar-3";
_root.specialshipitems[itemno][1] = 1;
_root.specialshipitems[itemno][2] = 750;
_root.specialshipitems[itemno][3] = 2000;
_root.specialshipitems[itemno][4] = 15000;
_root.specialshipitems[itemno][5] = "PULSAR";
_root.specialshipitems[itemno][6] = 250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "pulsar3";
_root.specialshipitems[itemno][10] = 0.950000;
_root.specialshipitems[itemno][11] = 9500;
_root.specialshipitems[itemno][12] = 300;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Shield-1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 5;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 15000;
_root.specialshipitems[itemno][5] = "RECHARGESHIELD";
_root.specialshipitems[itemno][6] = 650000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "reshield1";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Shield-2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 12;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 25000;
_root.specialshipitems[itemno][5] = "RECHARGESHIELD";
_root.specialshipitems[itemno][6] = 1250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "reshield2";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Shield-3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 20;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 50000;
_root.specialshipitems[itemno][5] = "RECHARGESHIELD";
_root.specialshipitems[itemno][6] = 2500000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "reshield3";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Struct-1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 5;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 25000;
_root.specialshipitems[itemno][5] = "RECHARGESTRUCT";
_root.specialshipitems[itemno][6] = 1250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "restruct1";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Struct-2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 12;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 35000;
_root.specialshipitems[itemno][5] = "RECHARGESTRUCT";
_root.specialshipitems[itemno][6] = 2750000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "restruct2";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Struct-3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 20;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 50000;
_root.specialshipitems[itemno][5] = "RECHARGESTRUCT";
_root.specialshipitems[itemno][6] = 5890000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "restruct3";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Mine-1";
_root.specialshipitems[itemno][1] = 15;
_root.specialshipitems[itemno][2] = 0;
_root.specialshipitems[itemno][3] = 5000;
_root.specialshipitems[itemno][4] = 3000;
_root.specialshipitems[itemno][5] = "MINES";
_root.specialshipitems[itemno][6] = 50000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "mine1";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 2000;
_root.specialshipitems[itemno][12] = 5000;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Mine-2";
_root.specialshipitems[itemno][1] = 10;
_root.specialshipitems[itemno][2] = 0;
_root.specialshipitems[itemno][3] = 5000;
_root.specialshipitems[itemno][4] = 5500;
_root.specialshipitems[itemno][5] = "MINES";
_root.specialshipitems[itemno][6] = 95000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "mine2";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 2000;
_root.specialshipitems[itemno][12] = 10000;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Mine-3";
_root.specialshipitems[itemno][1] = 5;
_root.specialshipitems[itemno][2] = 0;
_root.specialshipitems[itemno][3] = 5000;
_root.specialshipitems[itemno][4] = 8000;
_root.specialshipitems[itemno][5] = "MINES";
_root.specialshipitems[itemno][6] = 135000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "mine3";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 2000;
_root.specialshipitems[itemno][12] = 25000;

function func_racinggamefinished()
{
    _root.racinginformation[0] = 0;
    _root.racingcheckpoints = new Array();
    colourtouse = _root.systemchattextcolor;
    message = "HOST: Race is Completed";
    _root.enterintochat(message, colourtouse);
    _root.gotoAndStop("racingzonescreen");
} // End of the function
function func_racinggamefinisher(playerloc, playerplaced)
{
    playerloc = Number(playerloc);
    playerplaced = Number(playerplaced);
    for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
    {
        if (playerloc == _root.currentonlineplayers[jjjj][0])
        {
            name = _root.currentonlineplayers[jjjj][1];
            break;
        } // end if
    } // end of for
    colourtouse = _root.systemchattextcolor;
    message = "HOST: Player " + name + " has finished with Rank # " + (playerplaced + 1);
    if (playerplaced == 0)
    {
        message = message + (", for " + _root.func_formatnumber(_root.racinginformation[1]) + " funds");
        if (_root.playershipstatus[3][0] == playerloc)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(_root.racinginformation[1]);
        } // end if
    } // end if
    _root.enterintochat(message, colourtouse);
} // End of the function
function func_createracinggamepoints(navpoints, racestarttime, reward)
{
    racingcheckpoints = new Array();
    for (rac = 0; rac < navpoints.length; rac++)
    {
        racingcheckpoints[rac] = new Array();
        racingcheckpoints[rac][0] = navpoints[rac];
        racingcheckpoints[rac][1] = false;
    } // end of for
    racinggameinprogress = true;
    distance = 0;
    lastx = 0;
    lasty = 0;
    for (rac = 0; rac < navpoints.length; rac++)
    {
        for (sect = 0; sect < _root.sectormapitems.length; sect++)
        {
            if (_root.sectormapitems[sect][0] == racingcheckpoints[rac][0])
            {
                xloc = _root.sectormapitems[sect][1];
                yloc = _root.sectormapitems[sect][2];
                xdiff = lastx - xloc;
                ydiff = lasty - yloc;
                lasty = yloc;
                lastx = xloc;
                distance = distance + Math.sqrt(xdiff * xdiff + ydiff * ydiff);
                break;
            } // end if
        } // end of for
    } // end of for
    racetotaldistance = distance;
    racestarttime = Number(racestarttime);
    racinginformation[1] = finalracegamereward = Number(reward);
    if (racestarttime > 0)
    {
        colourtouse = _root.systemchattextcolor;
        message = "HOST: Race Starting in " + racestarttime + " seconds, reward is " + _root.func_formatnumber(finalracegamereward) + " funds";
        _root.enterintochat(message, colourtouse);
        racinginformation[0] = getTimer() + racestarttime * 1000;
    }
    else
    {
        colourtouse = _root.systemchattextcolor;
        message = "HOST: Race Started " + Math.abs(racestarttime) + " seconds ago, reward is " + _root.func_formatnumber(finalracegamereward) + " funds";
        _root.enterintochat(message, colourtouse);
        racinginformation[0] = 0;
    } // end else if
} // End of the function
function func_getracereward(distancebeingtraveled)
{
    fundsperunitoftravel = 5;
    timesfundsforextraplayer = 0.300000;
    basefunds = distancebeingtraveled * fundsperunitoftravel;
    fundswithplayers = basefunds * (timesfundsforextraplayer * _root.currentonlineplayers.length + 0.700000);
    return (Math.round(fundswithplayers));
} // End of the function
isgameracingzone = false;
racegamepacespeed = 50;
racegamestartdelay = 30;
racinginformation = new Array();
racinginformation[0] = 0;
racinginformation[1] = 95;
racinginformation[2] = true;

function sendnewdmgame()
{
    info = "";
    for (xx = 0; xx < _root.teambases.length; xx++)
    {
        info = info + ("`" + _root.teambasetypes[_root.teambases[xx][3]][0]);
    } // end of for
    datatosend = "DM`NG" + info + "~";
    _root.mysocket.send(datatosend);
} // End of the function
function deathfgamerewards()
{
    return (_root.currentonlineplayers.length * _root.teamdeathmatchinfo[1] + _root.teamdeathmatchinfo[0]);
} // End of the function
function deathsgamerewards()
{
    return (Math.floor((_root.currentonlineplayers.length * _root.teamdeathmatchinfo[1] + _root.teamdeathmatchinfo[0]) / 1000));
} // End of the function
_root.teamdeathmatch = false;
_root.teamdeathmatchinfo = new Array();
_root.teamdeathmatchinfo[0] = 300000;
_root.teamdeathmatchinfo[1] = 100000;
_root.teambasetypes = new Array();
_root.teambasetypes[0] = new Array();
_root.teambasetypes[0][0] = 300000;
_root.teambasetypes[0][1] = 800;
_root.teambasetypes[0][2] = 10000;
_root.teambasetypes[0][3] = 300000;
_root.teambasetypes[0][4] = 20000;
squadwarinfo = new Array();
squadwarinfo[0] = false;
squadwarinfo[1] = new Array();

function func_playercreatekflag()
{
    minusers = 4;
    location = _root.fun_kingofflagrandomcoords();
    xcoord = location[0];
    ycoord = location[1];
    if (_root.kingofflag[0] == null && _root.kingofflag[2] == null)
    {
        if (_root.currentonlineplayers.length >= minusers)
        {
            datatosend = "KFLAG`CREATE`" + xcoord + "`" + ycoord + "~";
            _root.mysocket.send(datatosend);
        }
        else
        {
            message = "HOST: Need at least " + minusers + " players to start the game";
            _root.enterintochat(message, _root.systemchattextcolor);
        } // end else if
    }
    else
    {
        if (_root.kingofflag[0] == null)
        {
            xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
            ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
            message = "HOST: Flag Already in game at X:" + xgrid + " Y:" + ygrid;
        }
        else
        {
            xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
            ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
            message = "HOST: Flag Already in game, held by " + _root.func_playersrealname(_root.kingofflag[0]) + " last report at X:" + xgrid + " Y:" + ygrid;
        } // end else if
        _root.enterintochat(message, _root.systemchattextcolor);
    } // end else if
} // End of the function
function func_playersrealname(playerid)
{
    for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
    {
        if (playerid == _root.currentonlineplayers[jjjj][0])
        {
            return (_root.currentonlineplayers[jjjj][1]);
            break;
        } // end if
    } // end of for
} // End of the function
function fun_kingofflagrandomcoords()
{
    xwidthofsector = _root.sectorinformation[1][0];
    ywidthofsector = _root.sectorinformation[1][1];
    xgrid = Math.round(Math.random() * Number(_root.sectorinformation[0][0] - 2)) + 1;
    xcoord = xwidthofsector * xgrid - Math.round(xwidthofsector / 2);
    ygrid = Math.round(Math.random() * Number(_root.sectorinformation[0][1] - 2)) + 1;
    ycoord = ywidthofsector * ygrid - Math.round(ywidthofsector / 2);
    location = new Array();
    location[0] = xcoord;
    location[1] = ycoord;
    return (location);
} // End of the function
function func_kingofflagcommand(method, var1, var2, var3)
{
    if (method == "TRANS")
    {
        currenthostalreadysent = false;
        if (String(var1) == String(_root.kingofflag[0]))
        {
            currenthostalreadysent = true;
        } // end if
        _root.kingofflag[0] = var1;
        _root.kingofflag[1] = getTimer() + _root.kingofflag[10];
        _root.gamedisplayarea.func_flagcontrols("REMOVE");
        if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
        {
            _root.game_top_bar.flagwindow.func_runflagscript();
        } // end if
        if (currenthostalreadysent != true)
        {
            message = "HOST: " + func_playersrealname(_root.kingofflag[0]) + " has the flag.";
            _root.enterintochat(message, _root.systemchattextcolor);
        } // end if
    }
    else if (method == "END")
    {
        func_kingofflagreset();
        scorereward = Number(var2);
        if (isNaN(scorereward))
        {
            scorereward = 0;
        } // end if
        fundreward = Number(var3);
        if (isNaN(fundreward))
        {
            fundreward = 0;
        } // end if
        message = "HOST: " + func_playersrealname(var1) + " won the flagging game for " + Math.floor(scorereward / _root.scoreratiomodifier) + " score and " + _root.func_formatnumber(fundreward) + " funds.";
        _root.enterintochat(message, _root.systemchattextcolor);
        for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
        {
            if (String(var1) == String(_root.currentonlineplayers[jjjj][0]))
            {
                _root.currentonlineplayers[jjjj][5] = Number(_root.currentonlineplayers[jjjj][5]) + Number(scorereward);
                break;
            } // end if
        } // end of for
        if (String(_root.playershipstatus[3][0]) == String(var1))
        {
            _root.playershipstatus[5][9] = Number(_root.playershipstatus[5][9]) + Number(scorereward);
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(fundreward);
            _root.toprightinformation.playerscore = "Score: " + Math.floor(Number(_root.playershipstatus[5][9]) / _root.scoreratiomodifier);
            _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
        } // end if
        _root.refreshonlinelist = true;
    }
    else if (method == "DROP")
    {
        _root.kingofflag[0] = null;
        _root.kingofflag[1] = null;
        _root.kingofflag[2] = Number(var1);
        _root.kingofflag[3] = Number(var2);
        _root.gamedisplayarea.func_flagcontrols("ADD", _root.kingofflag[2], _root.kingofflag[3]);
        xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
        ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
        message = "HOST: Flag located at X:" + xgrid + " Y:" + ygrid;
        _root.enterintochat(message, _root.systemchattextcolor);
    }
    else if (method == "CREATE")
    {
        _root.kingofflag[0] = null;
        _root.kingofflag[1] = null;
        _root.kingofflag[2] = Number(var1);
        _root.kingofflag[3] = Number(var2);
        _root.gamedisplayarea.func_flagcontrols("ADD", _root.kingofflag[2], _root.kingofflag[3]);
        xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
        ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
        message = "HOST: Flag located at X:" + xgrid + " Y:" + ygrid;
        _root.enterintochat(message, _root.systemchattextcolor);
    }
    else if (method == "INFO")
    {
        flagtime = Math.round((_root.kingofflag[1] - getTimer()) / 1000);
        _root.kingofflag[2] = Number(var1);
        _root.kingofflag[3] = Number(var2);
        xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
        ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
        name = func_playersrealname(playerid);
        message = "HOST: " + _root.func_playersrealname(_root.kingofflag[0]) + " has the flag at X:" + xgrid + " Y:" + ygrid + " with " + flagtime + " seconds left";
        _root.enterintochat(message, _root.systemchattextcolor);
    } // end else if
} // End of the function
function func_kingofflagreset()
{
    _root.kingofflag = new Array();
    _root.kingofflag[0] = null;
    _root.kingofflag[1] = null;
    _root.kingofflag[2] = null;
    _root.kingofflag[3] = null;
    _root.kingofflag[10] = 120000;
    _root.kingofflag[11] = 50;
    _root.kingofflag[12] = 15;
    _root.kingofflag[13] = 30000;
    _root.kingofflag[14] = 15000;
    _root.kingofflag[15] = 20000;
    _root.kingofflag[16] = false;
} // End of the function
func_kingofflagreset();

_root.keypressdelay = 0;
_root.shipositiondelay = 300;
_root.errorcheckdelay = 7000;
_root.errorcheckdata = new Array();
_root.errorchecknumber = 0;
_root._x = 0;
_root._y = 0;
_root.cosines = new Array();
for (j = 0; j <= 360; j++)
{
    _root.cosines[j] = parseFloat(Math.cos(0.017453 * j));
} // end of for
_root.sines = new Array();
for (j = 0; j <= 360; j++)
{
    _root.sines[j] = parseFloat(Math.sin(0.017453 * j));
} // end of for
stop ();
