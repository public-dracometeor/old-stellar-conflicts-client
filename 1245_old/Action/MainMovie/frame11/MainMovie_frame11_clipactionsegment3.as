﻿// Action script...

// [onClipEvent of sprite 1529 in frame 11]
onClipEvent (load)
{
    function dropaiships()
    {
        i = 0;
        updatedata = "";
        while (i < _root.aishipshosted.length)
        {
            updatedata = updatedata + (_root.aishipshosted[i][0] + "`" + _root.aishipshosted[i][1] + "`" + _root.aishipshosted[i][2] + "~");
            ++i;
        } // end while
        pathtosectorfile = _root.pathtosectorfile;
        if (i > 0)
        {
            aidropsending = new XML();
            aidropsending.load(pathtosectorfile + "airun.php?mode=drop&aidata=" + updatedata);
            aidropsending.onLoad = function (success)
            {
            };
        } // end if
        _root.aishipshosted = null;
    } // End of the function
    _root.playershipstatus[2][5] = 0;
    _root.playershipstatus[5][4] = "dead";
    removeMovieClip ("_root.gamedisplayarea.playership");
    _root.gamedisplayarea.attachMovie("shiptypedead", "playership", 1000);
    _root.playershipvelocity = 0;
    _root.playersmission = new Array();
    bountychanging = Number(_root.playershipstatus[5][3] + _root.playershipstatus[5][8]);
    if (bountychanging < 1 || bountychanging > 100000000 || isNaN(bountychanging))
    {
        _root.playershipstatus[5][3] = 0;
        _root.playershipstatus[5][8] = 0;
    } // end if
    if (_root.playershipstatus[3][0] == _root.playershipstatus[5][5])
    {
        schange = (_root.playershipstatus[5][3] + _root.playershipstatus[5][8]) * -1;
    }
    else
    {
        schange = _root.playershipstatus[5][3] + _root.playershipstatus[5][8];
    } // end else if
    if (_root.playershipstatus[3][0] == _root.playershipstatus[5][5])
    {
        tb = 0;
    }
    else
    {
        tb = _root.playershipstatus[5][3] + _root.playershipstatus[5][8];
    } // end else if
    datatosend = "TC`" + _root.playershipstatus[3][0] + "`SD`" + _root.playershipstatus[5][5] + "`" + tb + "`" + Math.round(_root.playershipstatus[5][3] * _root.playershipstatus[5][6]) + "~";
    _root.mysocket.send(datatosend);
    _root.playershipstatus[5][3] = 0;
    _root.playershipstatus[5][8] = 0;
    _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
}
