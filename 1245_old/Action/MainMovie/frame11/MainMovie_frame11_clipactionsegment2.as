﻿// Action script...

// [onClipEvent of sprite 1529 in frame 11]
on (release)
{
    _root.func_main_clicksound();
    if (_root.teamdeathmatch == true)
    {
        _root.gotoAndStop("teambase");
    }
    else if (_root.isgameracingzone == true)
    {
        _root.gotoAndStop("racingzonescreen");
    }
    else if (_root.playershipstatus[4][0].substr(0, 2) == "SB")
    {
        _root.gotoAndStop("stardock");
    }
    else if (_root.playershipstatus[4][0].substr(0, 2) == "PL")
    {
        _root.gotoAndStop("planet");
    } // end else if
}
