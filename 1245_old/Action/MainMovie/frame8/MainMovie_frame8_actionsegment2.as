﻿// Action script...

function initializemissions()
{
    _root.missionsavailable = new Array();
    currentmission = 0;
    for (i = 0; i < _root.sectormapitems.length; i++)
    {
        if (_root.sectormapitems[i][0].substr(0, 2) == "NP")
        {
            _root.missionsavailable[currentmission] = new Array();
            _root.missionsavailable[currentmission][0] = "Recon`" + _root.sectormapitems[i][0];
            _root.missionsavailable[currentmission][1] = _root.sectormapitems[i][1];
            _root.missionsavailable[currentmission][2] = _root.sectormapitems[i][2];
            _root.missionsavailable[currentmission][3] = 20000;
            _root.missionsavailable[currentmission][4] = 600;
            _root.missionsavailable[currentmission][5] = 2;
            _root.missionsavailable[currentmission][6] = 35000;
            ++currentmission;
            _root.missionsavailable[currentmission] = new Array();
            _root.missionsavailable[currentmission][0] = "Patrol`" + _root.sectormapitems[i][0];
            _root.missionsavailable[currentmission][1] = _root.sectormapitems[i][1];
            _root.missionsavailable[currentmission][2] = _root.sectormapitems[i][2];
            _root.missionsavailable[currentmission][3] = 40000;
            _root.missionsavailable[currentmission][4] = 550;
            _root.missionsavailable[currentmission][5] = 2;
            _root.missionsavailable[currentmission][6] = 65500;
            ++currentmission;
        } // end if
    } // end of for
    for (i = 0; i < _root.starbaselocation.length; i++)
    {
        for (j = 0; j < _root.starbaselocation.length; j++)
        {
            if (_root.starbaselocation[i][0] != _root.starbaselocation[j][0])
            {
                _root.missionsavailable[currentmission] = new Array();
                _root.missionsavailable[currentmission][0] = "Cargorun`" + _root.starbaselocation[i][0] + "`" + _root.starbaselocation[j][0];
                _root.missionsavailable[currentmission][1] = "";
                _root.missionsavailable[currentmission][2] = "";
                distancebetweenbases = Math.sqrt(Math.pow(_root.starbaselocation[i][1] - _root.starbaselocation[j][1], 2) + Math.pow(_root.starbaselocation[i][2] - _root.starbaselocation[j][2], 2));
                _root.missionsavailable[currentmission][3] = Math.round(distancebetweenbases / 0.055000);
                _root.missionsavailable[currentmission][4] = 600;
                _root.missionsavailable[currentmission][5] = 2;
                _root.missionsavailable[currentmission][6] = Math.round(50000 * (distancebetweenbases / 100 / 60));
                _root.missionsavailable[currentmission][7] = 40000;
                ++currentmission;
            } // end if
        } // end of for
    } // end of for
} // End of the function
function func_assmissionbeg(name)
{
    namefound = false;
    for (jj = 0; jj < _root.currentonlineplayers.length; jj++)
    {
        if (_root.currentonlineplayers[jj][1] == name)
        {
            namefound = true;
            break;
        } // end if
    } // end of for
    if (namefound)
    {
        _root.enterintochat(" Assassinate Mission has began, Assassinate " + name + ".", _root.systemchattextcolor);
    }
    else
    {
        _root.enterintochat(" Assassinate Mission can not start, " + name + " is no longer here.", _root.systemchattextcolor);
        _root.playersmission = new Array();
    } // end else if
} // End of the function
function func_playerleftass(leavinname)
{
    if (_root.playersmission[0][0][0] == "assasinate" && leavinname == _root.playersmission[0][0][1])
    {
        _root.playersmission = new Array();
        _root.enterintochat(" Assassinate Mission has ended, " + leavinname + " has left.", _root.systemchattextcolor);
    } // end if
} // End of the function
function func_playerkilled(playerkilled)
{
    if (playerkilled == _root.playersmission[0][0][1] && _root.playersmission[0][0][0] == "assasinate")
    {
        _root.rightside.missionbox.missionscripts.func_assasskilled();
    } // end if
} // End of the function
_root.playersmission = new Array();
initializemissions();
