﻿// Action script...

// [onClipEvent of sprite 1529 in frame 11]
onClipEvent (load)
{
    function dropaiships()
    {
        i = 0;
        updatedata = "";
        while (i < _root.aishipshosted.length)
        {
            updatedata = updatedata + (_root.aishipshosted[i][0] + "`" + _root.aishipshosted[i][1] + "`" + _root.aishipshosted[i][2] + "~");
            ++i;
        } // end while
        pathtosectorfile = _root.pathtosectorfile;
        if (i > 0)
        {
            aidropsending = new XML();
            aidropsending.load(pathtosectorfile + "airun.php?mode=drop&aidata=" + updatedata);
            aidropsending.onLoad = function (success)
            {
            };
        } // end if
        _root.aishipshosted = null;
    } // End of the function
    _root.playershipstatus[2][5] = 0;
    _root.playershipstatus[5][4] = "dead";
    removeMovieClip ("_root.gamedisplayarea.playership");
    _root.gamedisplayarea.attachMovie("shiptypedead", "playership", 1000);
    _root.playershipvelocity = 0;
    _root.playersmission = new Array();
    bountychanging = Number(_root.playershipstatus[5][3] + _root.playershipstatus[5][8]);
    if (bountychanging < 1 || bountychanging > 100000000 || isNaN(bountychanging))
    {
        _root.playershipstatus[5][3] = 0;
        _root.playershipstatus[5][8] = 0;
    } // end if
    if (_root.playershipstatus[3][0] == _root.playershipstatus[5][5])
    {
        schange = (_root.playershipstatus[5][3] + _root.playershipstatus[5][8]) * -1;
    }
    else
    {
        schange = _root.playershipstatus[5][3] + _root.playershipstatus[5][8];
    } // end else if
    if (_root.playershipstatus[3][0] == _root.playershipstatus[5][5])
    {
        tb = 0;
    }
    else
    {
        tb = _root.playershipstatus[5][3] + _root.playershipstatus[5][8];
    } // end else if
    datatosend = "TC`" + _root.playershipstatus[3][0] + "`SD`" + _root.playershipstatus[5][5] + "`" + tb + "`" + Math.round(_root.playershipstatus[5][3] * _root.playershipstatus[5][6]) + "~";
    _root.mysocket.send(datatosend);
    _root.playershipstatus[5][3] = 0;
    _root.playershipstatus[5][8] = 0;
    _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
}

// [onClipEvent of sprite 1529 in frame 11]
on (release)
{
    _root.func_main_clicksound();
    if (_root.teamdeathmatch == true)
    {
        _root.gotoAndStop("teambase");
    }
    else if (_root.isgameracingzone == true)
    {
        _root.gotoAndStop("racingzonescreen");
    }
    else if (_root.playershipstatus[4][0].substr(0, 2) == "SB")
    {
        _root.gotoAndStop("stardock");
    }
    else if (_root.playershipstatus[4][0].substr(0, 2) == "PL")
    {
        _root.gotoAndStop("planet");
    } // end else if
}

// [onClipEvent of sprite 1529 in frame 11]
onClipEvent (load)
{
    function loadplayerdata(loadedvars)
    {
        newinfo = loadedvars.split("~");
        i = 0;
        otherplayership = new Array();
        while (i < newinfo.length - 1)
        {
            if (newinfo[i].substr(0, 2) == "PI")
            {
                currentthread = newinfo[i].split("`");
                if (currentthread[1].substr(0, 2) == "ST")
                {
                    _root.playershipstatus[5][0] = int(currentthread[1].substr("2"));
                    _root.playershipstatus[2][0] = int(currentthread[2].substr("2"));
                    _root.playershipstatus[1][0] = int(currentthread[3].substr("2"));
                    _root.playershipstatus[1][5] = int(currentthread[4].substr("2"));
                    _root.playershipstatus[3][1] = int(currentthread[5].substr("2"));
                    _root.playershipstatus[5][1] = int(currentthread[6].substr("2"));
                    _root.playershipstatus[4][0] = currentthread[7];
                    tr = 8;
                    _root.playershipstatus[0] = new Array();
                    while (currentthread[tr].substr(0, 2) == "HP")
                    {
                        guninfo = currentthread[tr].split("G");
                        currenthardpoint = guninfo[0].substr("2");
                        _root.playershipstatus[0][currenthardpoint] = new Array();
                        if (isNaN(guninfo[1]))
                        {
                            guninfo[1] = "none";
                        } // end if
                        _root.playershipstatus[0][currenthardpoint][0] = guninfo[1];
                        ++tr;
                    } // end while
                    _root.playershipstatus[8] = new Array();
                    while (currentthread[tr].substr(0, 2) == "TT")
                    {
                        guninfo = currentthread[tr].split("G");
                        currentturretpoint = guninfo[0].substr("2");
                        _root.playershipstatus[8][currentturretpoint] = new Array();
                        if (isNaN(guninfo[1]))
                        {
                            guninfo[1] = "none";
                        } // end if
                        _root.playershipstatus[8][currentturretpoint][0] = guninfo[1];
                        ++tr;
                    } // end while
                    while (currentthread[tr].substr(0, 2) == "CO")
                    {
                        cargoinfo = currentthread[tr].split("A");
                        currentcargotype = cargoinfo[0].substr("2");
                        _root.playershipstatus[4][1][currentcargotype] = int(cargoinfo[1]);
                        ++tr;
                    } // end while
                } // end if
            } // end if
            ++i;
        } // end while
    } // End of the function
    if (_root.playerscurrentextrashipno == "capital")
    {
        _parent.capitalinfo = "Capital Ship Has Been Lost";
        for (i = 0; i < _root.maxextraships; i++)
        {
            if (_root.extraplayerships[i][0] != null)
            {
                _root.playerscurrentextrashipno = i;
                break;
            } // end if
        } // end of for
        _root.changetonewship(_root.playerscurrentextrashipno);
        _root.initializemissilebanks();
        _root.savegamescript.saveplayersgame(this);
        datatosend = "TC`" + _root.playershipstatus[3][0] + "`SC`" + _root.extraplayerships[_root.playerscurrentextrashipno][0] + "`" + _root.errorchecknumber + "~";
        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
        _root.mysocket.send(datatosend);
    }
    else
    {
        _parent.capitalinfo = "";
    } // end else if
}

// [Action in Frame 11]
stop ();
