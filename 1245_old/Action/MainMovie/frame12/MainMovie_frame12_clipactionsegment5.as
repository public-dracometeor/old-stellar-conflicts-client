﻿// Action script...

// [onClipEvent of sprite 1579 in frame 12]
onClipEvent (load)
{
    setProperty("_root.onlineplayerslist", _x, 0);
    setProperty("_root.onlineplayerslist", _y, 0);
    _root.playershipstatus[5][15] = "";
    clearInterval(_root.specialstimers[1]);
    clearInterval(_root.specialstimers[2]);
    _root._quality = "HIGH";
    if (_root.playerjustloggedin == false)
    {
        _root.hasplayerbeenlistedasdocked = false;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
        _root.playersdockedtime = 999999999;
        dockinginfosent = getTimer();
        dockinginfoerrorcheck = 10000;
    }
    else
    {
        dockinginfosent = 999999999;
        _root.hasplayerbeenlistedasdocked = true;
        _root.hassafeleavebeeddisp = true;
    } // end else if
    _root.hassafeleavebeeddisp = false;
    if (_root.playershipstatus[5][25] == "CAPLEFT")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Illegal Cap Left Detected \r Capital Ship Lost";
        _root.playershipstatus[5][25] = "YES";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
    _root.aishipshosted = new Array();
    this.mapdsiplayed = false;
}
