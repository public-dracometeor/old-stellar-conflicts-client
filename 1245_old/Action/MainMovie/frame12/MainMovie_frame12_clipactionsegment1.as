﻿// Action script...

// [onClipEvent of sprite 1579 in frame 12]
onClipEvent (load)
{
    function enterintochat(info, colourtouse)
    {
        _root.gamechatinfo[1].splice(0, 0, 0);
        _root.gamechatinfo[1][0] = new Array();
        _root.gamechatinfo[1][0][0] = info;
        _root.gamechatinfo[1][0][1] = colourtouse;
        if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
        {
            _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
        } // end if
        _root.refreshchatdisplay = true;
    } // End of the function
    if (_root.playersmission[0][0][0] == "Cargorun")
    {
        currentlydockedbase = _root.playershipstatus[4][0];
        basesformission = _root.playersmission[0][0][1].split("`");
        if (_root.playersmission[0][4] == false)
        {
            if (currentlydockedbase == basesformission[0])
            {
                _root.playersmission[0][4] = true;
                enterintochat(" Cargo Picked Up! Mission begins once you exit the base.", _root.systemchattextcolor);
            } // end if
        }
        else if (currentlydockedbase != basesformission[1])
        {
            _root.playersmission = new Array();
            enterintochat(" Cargo Mission Failed!", _root.systemchattextcolor);
        }
        else
        {
            rewardamounts = _root.playersmission[0][2].split("`");
            if (_root.playersmission[0][4] == true)
            {
                reward = Number(rewardamounts[0]) + Number(rewardamounts[1]);
            }
            else
            {
                reward = Number(rewardamounts[0]);
            } // end else if
            enterintochat(" Cargo Mission Passed! Total Reward:" + reward, _root.systemchattextcolor);
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + reward;
            scoreaward = Math.round(Number(rewardamounts[0]) / _root.missiontoactualscoremodifier);
            information = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + scoreaward + "~";
            _root.mysocket.send(information);
            _root.playersmission = new Array();
        } // end else if
    } // end else if
}
