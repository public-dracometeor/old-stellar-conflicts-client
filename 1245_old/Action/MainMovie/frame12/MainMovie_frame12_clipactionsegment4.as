﻿// Action script...

// [onClipEvent of sprite 1579 in frame 12]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (_root.hasplayerbeenlistedasdocked == false && currenttime > dockinginfoerrorcheck + dockinginfosent && _root.playerjustloggedin == false)
    {
        dockinginfosent = currenttime;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
    } // end if
    if (_root.hasplayerbeenlistedasdocked == true && _root.hassafeleavebeeddisp == false && _root.playerscurrentextrashipno == "capital" && _root.playershipstatus[5][25] == "YES")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Safe to Exit Game \r With Capital Ship";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
}
