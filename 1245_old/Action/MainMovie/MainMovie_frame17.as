﻿// Action script...

// [Action in Frame 17]
savestatus = "";
removeMovieClip ("_root.toprightinformation");
removeMovieClip ("_root.onlineplayerslist");
removeMovieClip ("_root.chatdialogue");
if (_root.gameerror == "failedtologin")
{
    _root.message = " Failed to Login \rProbable Reasons For This - \r1. Server is down, or could not connect to server \r2. You are not connected to the internet or information cannot  \r\tPass through a firewall";
    if (_root.reconnectingfromgame == true)
    {
        _root.savegamescript.saveplayersgame(this);
    } // end if
}
else if (_root.gameerror == "hostclosedconnection")
{
    _root.savegamescript.saveplayersgame(this);
    _root.message = " Connection Has Closed \rYou Have lost the connection to the server \rProbable Reasons For This - \r1. Server shut down \r2. Your connection to the internet was broken \r";
}
else if (_root.gameerror == "toomanyplayers")
{
    _root.message = " Too Many Players \rProbable Reasons For This - \r1. There are too many players, Try again later \r";
}
else if (_root.gameerror == "servershutdown")
{
    _root.savegamescript.saveplayersgame(this);
    _root.message = " The server was shut down \rProbable Reasons For This - \r1. An Admin needed to restart the server. \rIt will take a few minuites for the server to reload \rYou need to close this window and try reloading form the Homepage.";
}
else if (_root.gameerror == "nameinuse")
{
    _root.message = " Your Name is Already in Use \rProbable Reasons For This - \r1. Your name is already in use in this zone. \rIt will take a few minuites for you to time out if disconnected  \r";
}
else if (_root.gameerror == "kicked")
{
    _root.savegamescript.saveplayersgame(this);
    _root.message = " You were booted by " + _root.gameerrordoneby + "! \r" + "Probable Reasons For This - \r" + "1. You Have been a very bad person \r" + "2. Game Error \r";
}
else if (_root.gameerror == "banned")
{
    bannedtime = Number(_root.timebannedfor);
    diff = bannedtime * 60;
    daysDiff = Math.floor(diff / 60 / 60 / 24);
    diff = diff - daysDiff * 60 * 60 * 24;
    hrsDiff = Math.floor(diff / 60 / 60);
    diff = diff - hrsDiff * 60 * 60;
    minsDiff = Math.floor(diff / 60);
    diff = diff - minsDiff * 60;
    secsDiff = diff;
    bannedtime = daysDiff + " Days, " + hrsDiff + " Hours, " + minsDiff + " Minutes";
    _root.message = " Your Have Been Banned for " + bannedtime + " by " + _root.gameerrordoneby + "\r" + "Probable Reasons For This - \r" + "1. You are probably not welcomed here. \r" + "2. This Point of Access is banned. \r" + "3. In case of an error you can post in the forum. \r";
}
else if (_root.gameerror == "fpsstopped")
{
    _root.savegamescript.saveplayersgame(this);
    _root.message = " Your Frame Rate is Too Slow! \rProbable Reasons For This - \r1. You computer was running to slow \r2. You tried to cheat \r";
}
else if (_root.gameerror == "savefailure")
{
    _root.message = " Your Game Failed to Save! \rProbable Reasons For This - \r1. Your Save Transmission Failed \r2. You are Playing With This Account in Another Zone at the Same Time \r\rIf this continues you may end up being banned, /r and you will have to contact the webmaster to become unbanned.";
}
else if (_root.gameerror == "FLOODING")
{
    _root.message = " Your Must Stop Flooding \rProbable Reasons For This - \r1. Your Are Flooding the Chat \r\rIf this continues you may end up being reported and being banned.";
}
else
{
    _root.message = " Error Occured, If you connected to the server and immediately went to this screen after the countdown report BUG#111 to Kyled on the forums.\r If you just finished loggin into your account and did not conect to the server report BUG#222 to Kyled on the forums.";
} // end else if
_root.createEmptyMovieClip("blanker", 9999999);
_root.createEmptyMovieClip("blanker", 10);
stop ();
