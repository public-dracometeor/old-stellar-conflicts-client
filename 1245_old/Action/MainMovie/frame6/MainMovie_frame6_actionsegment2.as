﻿// Action script...

function loadsquadbases()
{
    newsystemVars = new XML();
    newsystemVars.load(_root.pathtoaccounts + "squadbases.php?mode=load&system=" + _root.playershipstatus[5][1]);
    newsystemVars.onLoad = function (success)
    {
        loadedvars = String(newsystemVars);
        loadsectormap(loadedvars);
        _root.currentgamestatus = _root.currentgamestatus + "Squad Bases Loaded\r\r";
        if (squadbasesalreadyloaded == false)
        {
            squadbasesalreadyloaded = true;
            _root.play();
        } // end if
    };
} // End of the function
function loadsectormap(loadedvars)
{
    newinfo = loadedvars.split("~");
    i = 0;
    _root.playersquadbases = new Array();
    while (i < newinfo.length - 1)
    {
        currentthread = newinfo[i].split("`");
        currentsquadbase = _root.playersquadbases.length;
        _root.playersquadbases[currentsquadbase] = new Array();
        _root.playersquadbases[currentsquadbase][0] = currentthread[0];
        _root.playersquadbases[currentsquadbase][1] = Number(currentthread[1]);
        _root.playersquadbases[currentsquadbase][2] = Number(currentthread[2]);
        _root.playersquadbases[currentsquadbase][3] = Number(currentthread[3]);
        _root.playersquadbases[currentsquadbase][4] = Math.ceil(Number(currentthread[2]) / _root.sectorinformation[1][0]);
        _root.playersquadbases[currentsquadbase][5] = Math.ceil(Number(currentthread[3]) / _root.sectorinformation[1][1]);
        _root.playersquadbases[currentsquadbase][10] = false;
        ++i;
    } // end while
} // End of the function
_root.playersquadbaseslvl = 0;
_root.currentgamestatus = _root.currentgamestatus + "Loading Squad Bases: \r";
loadsquadbases();
squadbasesalreadyloaded = false;
_root.squadbaseinfo = new Array();
squadbasetype = 0;
_root.squadbaseinfo[squadbasetype] = new Array();
_root.squadbaseinfo[squadbasetype][1] = new Array();
_root.squadbaseinfo[squadbasetype][1][0] = 10;
_root.squadbaseinfo[squadbasetype][1][1] = 8;
_root.squadbaseinfo[squadbasetype][1][2] = 5;
_root.squadbaseinfo[squadbasetype][1][3] = _root.shieldgenerators.length - 4;
_root.squadbaseinfo[squadbasetype][2] = new Array();
_root.squadbaseinfo[squadbasetype][2][0] = 2;
_root.squadbaseinfo[squadbasetype][2][1] = _root.guntype.length - 3;
_root.squadbaseinfo[squadbasetype][2][2] = 4;
_root.squadbaseinfo[squadbasetype][3] = new Array();
_root.squadbaseinfo[squadbasetype][3][0] = 2;
_root.squadbaseinfo[squadbasetype][3][1] = 125000;
_root.squadbaseinfo[squadbasetype][4] = new Array();
_root.squadbaseinfo[squadbasetype][4][0] = 1250;
++squadbasetype;
_root.squadbaseinfo[squadbasetype] = new Array();
_root.squadbaseinfo[squadbasetype][1] = new Array();
_root.squadbaseinfo[squadbasetype][1][0] = 10;
_root.squadbaseinfo[squadbasetype][1][1] = 7;
_root.squadbaseinfo[squadbasetype][1][2] = 4;
_root.squadbaseinfo[squadbasetype][1][3] = _root.shieldgenerators.length - 3;
_root.squadbaseinfo[squadbasetype][2] = new Array();
_root.squadbaseinfo[squadbasetype][2][0] = 2;
_root.squadbaseinfo[squadbasetype][2][1] = _root.guntype.length - 2;
_root.squadbaseinfo[squadbasetype][2][2] = 4;
_root.squadbaseinfo[squadbasetype][3] = new Array();
_root.squadbaseinfo[squadbasetype][3][0] = 2;
_root.squadbaseinfo[squadbasetype][3][1] = 500000;
_root.squadbaseinfo[squadbasetype][4] = new Array();
_root.squadbaseinfo[squadbasetype][4][0] = 1000;
_root.squadbaseinfo[squadbasetype][5] = new Array();
_root.squadbaseinfo[squadbasetype][5][0] = new Array();
_root.squadbaseinfo[squadbasetype][5][0][0] = 23000000;
_root.squadbaseinfo[squadbasetype][5][1] = new Array();
++squadbasetype;
_root.squadbaseinfo[squadbasetype] = new Array();
_root.squadbaseinfo[squadbasetype][1] = new Array();
_root.squadbaseinfo[squadbasetype][1][0] = 15;
_root.squadbaseinfo[squadbasetype][1][1] = 5;
_root.squadbaseinfo[squadbasetype][1][2] = 3;
_root.squadbaseinfo[squadbasetype][1][3] = _root.shieldgenerators.length - 2;
_root.squadbaseinfo[squadbasetype][2] = new Array();
_root.squadbaseinfo[squadbasetype][2][0] = 3;
_root.squadbaseinfo[squadbasetype][2][1] = _root.guntype.length - 1;
_root.squadbaseinfo[squadbasetype][2][2] = 5;
_root.squadbaseinfo[squadbasetype][3] = new Array();
_root.squadbaseinfo[squadbasetype][3][0] = 3;
_root.squadbaseinfo[squadbasetype][3][1] = 900000;
_root.squadbaseinfo[squadbasetype][4] = new Array();
_root.squadbaseinfo[squadbasetype][4][0] = 750;
_root.squadbaseinfo[squadbasetype][5] = new Array();
_root.squadbaseinfo[squadbasetype][5][0] = new Array();
_root.squadbaseinfo[squadbasetype][5][0][0] = 59000000;
_root.squadbaseinfo[squadbasetype][5][1] = new Array();
stop ();
