﻿// Action script...

// [onClipEvent of sprite 1407 in frame 10]
onClipEvent (load)
{
    function func_flagcontrols(method, xcoord, ycoord)
    {
        if (method == "ADD")
        {
            this.gamebackground.attachMovie("kingflaggameicon", "kflag", 8001);
            this.gamebackground.kflag._x = xcoord;
            this.gamebackground.kflag._y = ycoord;
        }
        else
        {
            removeMovieClip (this.gamebackground.kflag);
        } // end else if
    } // End of the function
    function func_drawneutralship(currentno)
    {
        globalshipname = _root.neutralships[currentno][10];
        this.attachMovie("shiptypeaineutral", globalshipname, 600 + _root.neutralships[currentno][13]);
        this[globalshipname].globalshipname = globalshipname;
        this[globalshipname]._x = -1000;
        this[globalshipname]._y = -1000;
    } // End of the function
    function func_removeneutralship(currentno)
    {
        globalshipname = _root.neutralships[currentno][10];
        removeMovieClip (this[globalshipname]);
    } // End of the function
    function func_meteordraw(method, meteorid)
    {
        if (method == "ADD")
        {
            ++_root.currentmeteordrawlevel;
            if (_root.currentmeteordrawlevel > _root.maxmeteordrawlevel)
            {
                _root.currentmeteordrawlevel = 0;
            } // end if
            meteorlvl = _root.currentmeteordrawlevel;
            location = null;
            for (qq = 0; qq < _root.incomingmeteor.length; qq++)
            {
                if (meteorid == _root.incomingmeteor[qq][0])
                {
                    location = qq;
                    break;
                } // end if
            } // end of for
            if (location != null)
            {
                meteortype = _root.incomingmeteor[location][7];
                velocity = _root.destroyingmeteor[meteortype][0];
                relativefacing = _root.incomingmeteor[location][3];
                movementofanobjectwiththrust();
                ymovement = -velocity * _root.cosines[this.relativefacing];
                xmovement = velocity * _root.sines[this.relativefacing];
                this.gamebackground.attachMovie("meteorscript", "meteor" + meteorid, 8050 + meteorlvl);
                this.gamebackground["meteor" + meteorid].meteorid = meteorid;
                this.gamebackground["meteor" + meteorid]._x = _root.incomingmeteor[location][1];
                this.gamebackground["meteor" + meteorid]._y = _root.incomingmeteor[location][2];
                this.gamebackground["meteor" + meteorid].xmovement = xmovement;
                this.gamebackground["meteor" + meteorid].ymovement = ymovement;
                this.gamebackground["meteor" + meteorid].meteortype = meteortype;
                this.gamebackground["meteor" + meteorid].meteorlife = Number(_root.incomingmeteor[location][6]);
            } // end if
        }
        else
        {
            removeMovieClip (this.gamebackground["meteor" + meteorid]);
        } // end else if
    } // End of the function
    function makebackgroundstars()
    {
        backgroundstar = new Array();
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        playerslastxcoord = playersxcoord;
        playerslastycoord = playersycoord;
        for (i = 0; i < totalstars; i++)
        {
            backgroundstar[i] = new Array(2);
            backgroundstar[i][0] = random(doublegameareawidth) - Math.round(doublegameareawidth / 2) + playersxcoord;
            backgroundstar[i][1] = random(doublegameareaheight) - Math.round(doublegameareaheight / 2) + playersycoord;
            this.gamebackground.attachMovie("backgroundstar" + Math.round(Math.random() * _root.totalstartypes), "backgroundstar" + String(i), i + 500);
            setProperty("gamebackground.backgroundstar" + i, _x, backgroundstar[i][0]);
            setProperty("gamebackground.backgroundstar" + i, _y, backgroundstar[i][1]);
        } // end of for
    } // End of the function
    function adjuststars(playersxcoord, playersycoord)
    {
        xcoordadjustment = playersxcoord - playerslastxcoord;
        ycoordadjustment = playersycoord - playerslastycoord;
        playerslastxcoord = playersxcoord;
        playerslastycoord = playersycoord;
        leftofviewcheck = playersxcoord - gameareawidth;
        rightofviewcheck = playersxcoord + gameareawidth;
        topofviewcheck = playersycoord - gameareaheight;
        bottomofviewcheck = playersycoord + gameareaheight;
        for (i = 0; i < totalstars; i++)
        {
            redrawingstar = false;
            if (backgroundstar[i][0] < leftofviewcheck)
            {
                backgroundstar[i][0] = backgroundstar[i][0] + doublegameareawidth;
                this.gamebackground["backgroundstar" + i]._x = backgroundstar[i][0];
            }
            else if (backgroundstar[i][0] > rightofviewcheck)
            {
                backgroundstar[i][0] = backgroundstar[i][0] + -doublegameareawidth;
                this.gamebackground["backgroundstar" + i]._x = backgroundstar[i][0];
            } // end else if
            if (backgroundstar[i][1] < topofviewcheck)
            {
                backgroundstar[i][1] = backgroundstar[i][1] + doublegameareaheight;
                this.gamebackground["backgroundstar" + i]._y = backgroundstar[i][1];
                continue;
            } // end if
            if (backgroundstar[i][1] > bottomofviewcheck)
            {
                backgroundstar[i][1] = backgroundstar[i][1] + -doublegameareaheight;
                this.gamebackground["backgroundstar" + i]._y = backgroundstar[i][1];
            } // end if
        } // end of for
    } // End of the function
    for (iii = 0; iii < _root.neutralships.length; iii++)
    {
        func_drawneutralship(iii);
    } // end of for
    _root.otherplayership = new Array();
    _root.currentplayershotsfired = 0;
    this._x = _root.leftindentofgamearea + _root.gameareawidth / 2;
    this._y = _root.topindentofgamearea + _root.gameareaheight / 2;
    this.gameareawidth = _root.gameareawidth;
    this.gameareaheight = _root.gameareaheight;
    doublegameareawidth = gameareawidth * 2;
    doublegameareaheight = gameareaheight * 2;
    playersshiptype = _root.playershipstatus[5][0];
    this.attachMovie("shiptype" + playersshiptype, "playership", 1000);
    for (i = 0; i < _root.playershipstatus[8].length; i++)
    {
        if (_root.playershipstatus[8][i][0] != "none" || !isNaN(_root.playershipstatus[8][i][0]))
        {
            this.playership.attachMovie("turrettype0", "turret" + i, 250 + i);
            this.playership["turret" + i].shottype = _root.playershipstatus[8][i][0];
            this.playership["turret" + i].xonship = _root.shiptype[playersshiptype][5][i][0];
            this.playership["turret" + i].yonship = _root.shiptype[playersshiptype][5][i][1];
            this.playership["turret" + i]._x = this.playership["turret" + i].xonship;
            this.playership["turret" + i]._y = this.playership["turret" + i].yonship;
        } // end if
    } // end of for
    for (ii = 0; ii < _root.guntype.length; ii++)
    {
        _root["guntype" + ii + "sound"] = new Sound();
        _root["guntype" + ii + "sound"].attachSound("guntype" + ii + "sound");
        _root["guntype" + ii + "sound"].setVolume("75");
    } // end of for
    _root.missilefiresound = new Sound();
    _root.missilefiresound.attachSound("missilefiresound");
    _root.missilefiresound.setVolume("75");
    _root.aimessageradiostatic = new Sound();
    _root.aimessageradiostatic.attachSound("radiostaticsound");
    _root.aimessageradiostatic.setVolume("75");
    this.playershipfacing = _root.playershipfacing;
    this.playershiprotation = _root.playershiprotation;
    this.playershipvelocity = _root.playershipvelocity;
    this.playershipmaxvelocity = _root.playershipmaxvelocity;
    this.playershipacceleration = _root.playershipacceleration;
    this.totalstars = _root.totalstars;
    this.shipcoordinatex = _root.shipcoordinatex;
    this.shipcoordinatey = _root.shipcoordinatey;
    this.playershotsfired = new Array();
    _root.attachMovie("onlineplayerslist", "onlineplayerslist", 99982);
    setProperty("_root.onlineplayerslist", _x, 0);
    setProperty("_root.onlineplayerslist", _y, 105);
    this.halfgameareawidth = Math.round(gameareawidth / 2) + 1;
    this.halfgameareaheight = Math.round(gameareaheight / 2) + 1;
}

// [onClipEvent of sprite 1407 in frame 10]
onClipEvent (load)
{
    this.createEmptyMovieClip("gamebackground", 10);
    makebackgroundstars();
    if (_root.kingofflag[0] == null)
    {
        if (_root.kingofflag[2] != null && _root.kingofflag[3] != null)
        {
            _root.func_kingofflagcommand("CREATE", _root.kingofflag[2], _root.kingofflag[3]);
        } // end if
    } // end if
    for (jjj = 0; jjj < _root.incomingmeteor.length; jjj++)
    {
        meteorid = _root.incomingmeteor[jjj][0];
        func_meteordraw("ADD", meteorid);
    } // end of for
    currentframe = 0;
    updateinterval = 80;
}

// [onClipEvent of sprite 1407 in frame 10]
onClipEvent (enterFrame)
{
    lasttime = curenttime;
    curenttime = getTimer();
    currenttimechangeratio = (curenttime - lasttime) * 0.001000;
    _root.currenttimechangeratio = currenttimechangeratio;
    _root.curenttime = curenttime;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    ++currentframe;
    if (currentframe > updateinterval)
    {
        currentframe = 0;
        adjuststars(playersxcoord, playersycoord);
    } // end if
    this.gamebackground._x = -playersxcoord;
    this.gamebackground._y = -playersycoord;
}

// [onClipEvent of sprite 1420 in frame 10]
onClipEvent (load)
{
    currentylocationofitem = 0;
    weaponactivationspacing = 45;
    shieldactivationspacing = 45;
    this._x = _root.leftindentofgamearea + _root.gameareawidth - 25;
    this._y = _root.topindentofgamearea;
    this._height = gameareaheight;
    currentylocationofitem = currentylocationofitem + shieldactivationspacing;
}

// [onClipEvent of sprite 1495 in frame 10]
onClipEvent (load)
{
    function displaystats()
    {
        if (lastshownshield != _root.playershipstatus[2][1])
        {
            lastshownshield = _root.playershipstatus[2][1];
            this.shield = Math.round(lastshownshield);
            this.shieldbar.showshield(shield);
        } // end if
        if (lastshownenergy != _root.playershipstatus[1][1])
        {
            lastshownenergy = _root.playershipstatus[1][1];
            this.energy = Math.round(lastshownenergy);
            this.energybar.showenergy(energy);
        } // end if
        currentstructure = _root.playershipstatus[2][5];
        if (lastshownstructure != currentstructure)
        {
            this.structure = Math.round(currentstructure);
            this.structurebar.structure = laststructure;
            laststructure = currentstructure;
        } // end if
        if (_root.isplayeremp)
        {
            downtime = Math.ceil((_root.playerempend - currenttime) / 1000);
            this.energy = "OFFLINE " + downtime;
            _root.playershipstatus[1][1] = 0;
            this.shield = "OFFLINE " + downtime;
            _root.playershipstatus[2][1] = 0;
        } // end if
        if (_root.playershipstatus[5][20] == true)
        {
            timeleft = (_root.playershipstatus[5][21] - getTimer()) / 1000;
            if (timeleft < 0)
            {
                _root.playershipstatus[5][20] = false;
                this.shield = Math.round(lastshownshield);
            }
            else
            {
                this.shield = "INVULNERABLE " + Math.round(timeleft);
            } // end if
        } // end else if
    } // End of the function
    playercurrentenergygenerator = _root.playershipstatus[1][0];
    energyrechargerate = _root.energygenerators[playercurrentenergygenerator][0];
    playercurrentenergyenergycapacitor = _root.playershipstatus[1][5];
    maxenergy = _root.energycapacitors[playercurrentenergyenergycapacitor][0];
    playercurrentshieldgenerator = _root.playershipstatus[2][0];
    energydrainedbyshieldgenatfull = _root.shieldgenerators[playercurrentshieldgenerator][3];
    shieldrechargerate = _root.shieldgenerators[playercurrentshieldgenerator][1];
    maxshieldstrength = _root.shieldgenerators[playercurrentshieldgenerator][0];
    baseshieldgendrain = _root.shieldgenerators[playercurrentshieldgenerator][2];
    lastshieldstrength = 0;
    lastenergyamount = 0;
    displayupdaterate = 3;
    currentdisplayframe = 0;
    showupdaterate = 5;
    currentshowupdate = 0;
    lasttimerrefreshed = getTimer();
    _root.isplayeremp = false;
    _root.playerempend = 0;
    displaystats();
}

// [onClipEvent of sprite 1495 in frame 10]
onClipEvent (enterFrame)
{
    ++currentshowupdate;
    if (currentshowupdate > showupdaterate)
    {
        currentshowupdate = 0;
        displaystats();
    } // end if
    if (_root.playershipstatus[5][4] == "dead")
    {
        _root.playershipstatus[2][1] = 0;
        _root.playershipstatus[1][1] = 0;
        _root.playershipstatus[2][5] = 0;
        this.flagwindow._visible = false;
        displaystats();
    }
    else
    {
        ++currentdisplayframe;
        if (currentdisplayframe > displayupdaterate)
        {
            currentdisplayframe = 0;
            currenttime = getTimer();
            currenttimechangeratio = (currenttime - lasttimerrefreshed) / 1000;
            lasttimerrefreshed = currenttime;
            if (_root.isplayeremp)
            {
                currentshieldstrength = 0;
                currentenergy = 0;
                if (_root.playerempend < currenttime)
                {
                    _root.isplayeremp = false;
                } // end if
            }
            else
            {
                currentshieldstrength = _root.playershipstatus[2][1];
                if (currentshieldstrength < 0)
                {
                    currentshieldstrength = 0;
                } // end if
                currentshieldstrength = currentshieldstrength + shieldrechargerate * currenttimechangeratio;
                if (_root.playershipstatus[2][2] == "OFF")
                {
                    currentshieldstrength = 0;
                }
                else if (_root.playershipstatus[2][2] == "HALF")
                {
                    if (currentshieldstrength > Math.round(maxshieldstrength / 2))
                    {
                        currentshieldstrength = Math.round(maxshieldstrength / 2);
                    } // end if
                }
                else if (currentshieldstrength > maxshieldstrength)
                {
                    currentshieldstrength = maxshieldstrength;
                } // end else if
                _root.playershipstatus[2][1] = currentshieldstrength;
                currentenergy = _root.playershipstatus[1][1];
                currentenergy = currentenergy + _root.energygenerators[playercurrentenergygenerator][0] * currenttimechangeratio;
                if (_root.playershipstatus[2][2] != "OFF")
                {
                    eval(currentenergy = currentenergy - baseshieldgendrain * currenttimechangeratio)(currentenergy = currentenergy - currentshieldstrength / maxshieldstrength * (energydrainedbyshieldgenatfull * currenttimechangeratio));
                } // end if
                if (currentenergy < 0)
                {
                    currentenergy = 0;
                } // end if
                if (currentenergy > maxenergy)
                {
                    currentenergy = maxenergy;
                } // end if
                _root.playershipstatus[1][1] = currentenergy;
            } // end else if
            if (lastshieldstrength != currentshieldstrength)
            {
                lastshieldstrength = currentshieldstrength;
            } // end if
            if (lastenergyamount != currentenergy)
            {
                lastenergyamount = currentenergy;
            } // end if
        } // end if
    } // end else if
}

// [onClipEvent of sprite 1498 in frame 10]
onClipEvent (load)
{
    function func_isplayeronmap()
    {
        onmap = false;
        playsxsector = _root.playershipstatus[6][0];
        playsysector = _root.playershipstatus[6][1];
        if (playsxsector >= 0 && playsxsector <= _root.sectorinformation[0][0])
        {
            if (playsysector >= 0 && playsysector <= _root.sectorinformation[0][1])
            {
                onmap = true;
            } // end if
        } // end if
        return (onmap);
    } // End of the function
    this.shipcoordinatex = _root.shipcoordinatex;
    this.shipcoordinatey = _root.shipcoordinatey;
    xwidthofasector = _root.sectorinformation[1][0];
    ywidthofasector = _root.sectorinformation[1][1];
    xsector = Math.ceil(shipcoordinatex / xwidthofasector);
    ysector = Math.ceil(shipcoordinatey / ywidthofasector);
    this._visible = false;
    updateinterval = 3000;
    timeintervalcheck = getTimer() + updateinterval;
    pathtosectorfile = "./systems/system" + _root.playershipstatus[5][1] + "/";
    _root.otherplayership = new Array();
    currentotherplayshot = 0;
    justenteredgame = true;
    this.gameareawidth = _root.gameareawidth;
    this.gameareaheight = _root.gameareaheight;
    lowestvolume = 50;
    lowestvolumelength = Math.sqrt(gameareawidth / 2 * (gameareawidth / 2) + gameareaheight / 2 * (gameareaheight / 2));
    currentsectoris = xsector + "`" + ysector;
    lastsectoris = "NEW`";
    errorinformation = "PI`" + lastsectoris + "`" + currentsectoris + "`" + _root.errorchecknumber + "~";
    information = errorinformation + "PI" + "`" + _root.playershipstatus[3][0] + "`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + Math.round(_root.playershipfacing) + "`" + Math.round(_root.playershipvelocity) + "`" + keysbeingpressed + "`" + _root.playershipstatus[5][15] + Number.NaN;
    _root.mysocket.send(information);
    _root.errorcheckedmessage(errorinformation, _root.errorchecknumber);
    lastsectoris = currentsectoris;
    _root.ainformationtosend = "";
    aicheckinterval = 18000;
    lastaichecktime = getTimer() + aicheckinterval;
    timetoredrawship = 7000;
    curenttime = getTimer();
}

// [onClipEvent of sprite 1498 in frame 10]
onClipEvent (enterFrame)
{
    function checkforloadingnewai()
    {
        oddsthataiwillappear = 16;
        checktheodds = Math.random() * 100;
        if (checktheodds < oddsthataiwillappear)
        {
            maxaitoappear = 2;
            aitoappear = Math.round(Math.random() * (maxaitoappear - 1)) + 1;
            _root.bringinaiships(aitoappear, "RANDOM");
        } // end if
    } // End of the function
    function othermissilefiremovement()
    {
        cc = 0;
        _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "otherfire", "othermissilefire" + _root.othermissilefire[lastvar][8], 3000 + _root.othermissilefire[lastvar][8]);
        while (cc < _root.othermissilefire.length)
        {
            if (_root.othermissilefire[cc][5] < curenttime)
            {
                removeMovieClip ("othermissilefire" + _root.othermissilefire[cc][8]);
                _root.othergunfire.splice(cc, 1);
                --i;
            } // end if
            ++cc;
        } // end while
    } // End of the function
    function checkothershiptimouts()
    {
        for (qz = 0; qz < _root.otherplayership.length; qz++)
        {
            if (_root.otherplayership[qz][6] < _root.curenttime)
            {
                removeMovieClip ("_root.gamedisplayarea.otherplayership" + _root.otherplayership[qz][0]);
                removeMovieClip ("_root.gamedisplayarea.nametag" + _root.otherplayership[qz][0]);
                _root.otherplayership.splice(qz, 1);
            } // end if
        } // end of for
    } // End of the function
    function movementofanobjectwiththrust()
    {
        if (relativefacing == 0)
        {
            ymovement = -velocity;
            xmovement = 0;
        } // end if
        if (velocity != 0)
        {
            this.relativefacing = Math.round(this.relativefacing);
            ymovement = Math.round(-velocity * _root.cosines[this.relativefacing]);
            xmovement = Math.round(velocity * _root.sines[this.relativefacing]);
        }
        else
        {
            ymovement = 0;
            xmovement = 0;
        } // end else if
    } // End of the function
    function getaiinformation()
    {
        aiinformation = "";
        for (zza = 0; zza < _root.aishipshosted.length; zza++)
        {
            aiinformation = aiinformation + ("AI`" + _root.aishipshosted[zza][0] + "`" + Math.round(_root.aishipshosted[zza][1]) + "`" + Math.round(_root.aishipshosted[zza][2]) + "`" + _root.aishipshosted[zza][3] + "`" + Math.round(_root.aishipshosted[zza][4]) + "`" + _root.aishipshosted[zza][5] + "~");
        } // end of for
    } // End of the function
    currenttimechangeratio = _root.currenttimechangeratio;
    curenttime = getTimer();
    if (_root.afterburnerinuse != lastafterburnerinuse)
    {
        _root.keywaspressed = true;
        lastafterburnerinuse = _root.afterburnerinuse;
    } // end if
    if (curenttime >= timeintervalcheck || _root.keywaspressed == true)
    {
        currentsectoris = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
        lastsectoris = _root.playershipstatus[6][2] + "`" + _root.playershipstatus[6][3];
        if (lastsectoris == currentsectoris)
        {
            lastsectoris = "FALSE";
        }
        else if (lastsectoris != currentsectoris)
        {
            _root.playershipstatus[6][2] = _root.playershipstatus[6][0];
            _root.playershipstatus[6][3] = _root.playershipstatus[6][1];
        } // end else if
        _root.keywaspressed = false;
        if (lastsectoris != "FALSE" || _root.otherplayership.length > 0 || curenttime >= timeintervalcheck)
        {
            timeintervalcheck = curenttime + updateinterval;
            currentkeyedshipmovement();
            getaiinformation();
            information = "PI`" + lastsectoris + "`" + currentsectoris + "~PI" + "`" + _root.playershipstatus[3][0] + "`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + Math.round(_root.playershipfacing) + "`" + Math.round(_root.playershipvelocity) + "`" + keysbeingpressed + "`" + _root.playershipstatus[5][15] + "`" + _root.func_globaltimestamp() + "~" + _root.gunfireinformation + _root.ainformationtosend;
            _root.gunfireinformation = "";
            _root.ainformationtosend = "";
            if (_root.playershipstatus[5][4] != "dead")
            {
                _root.mysocket.send(information);
            } // end if
        } // end if
    } // end if
    if (_root.gunfireinformation != "" || _root.ainformationtosend != "")
    {
        if (_root.otherplayership.length > 0)
        {
            currentsectoris = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
            information = "PI`FALSE`~" + _root.gunfireinformation + _root.ainformationtosend;
            _root.gunfireinformation = "";
            _root.mysocket.send(information);
        } // end if
    } // end if
    _root.gunfireinformation = "";
    _root.ainformationtosend = "";
    if (_root.teamdeathmatch != true && _root.isgameracingzone != true)
    {
        if (curenttime > lastaichecktime && func_isplayeronmap())
        {
            lastaichecktime = curenttime + aicheckinterval;
            if (_root.aishipshosted.length < 1 && _root.isaiturnedon == true && _root.hostileplayerships == false)
            {
                checkforloadingnewai();
            } // end if
        } // end if
    } // end if
    checkothershiptimouts();
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (load)
{
    function func_sendstatuscheck(pilotno)
    {
        datatosend = "SP`" + pilotno + "~STATS`GET`" + _root.playershipstatus[3][0] + "~";
        _root.mysocket.send(datatosend);
    } // End of the function
    function attachtargetship(currenttargetshiptype)
    {
        this.attachMovie("shiptype" + currenttargetshiptype, "targetship", 1);
        if (this.targetship._height > 50 || this.targetship._width > 50)
        {
            if (this.targetship._height > this.targetship._width)
            {
                change = this.targetship._height - 50;
                this.targetship._height = this.targetship._height - change;
                this.targetship._width = this.targetship._width - change;
            }
            else
            {
                change = this.targetship._width - 50;
                this.targetship._height = this.targetship._height - change;
                this.targetship._width = this.targetship._width - change;
            } // end if
        } // end else if
        setProperty("targetship", _x, "153");
        setProperty("targetship", _y, "22");
        targetpointer._visible = true;
    } // End of the function
    function func_setlocalnpcs()
    {
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        minrangeforrad = 1500;
        neutralaiarray = new Array();
        for (q = 0; q < _root.neutralships.length; q++)
        {
            labell = _root.neutralships[q][10];
            xlocc = _root.gamedisplayarea[labell].xcoord;
            ylocc = _root.gamedisplayarea[labell].ycoord;
            playerdistancefromship = Math.round(Math.sqrt((playersxcoord - xlocc) * (playersxcoord - xlocc) + (playersycoord - ylocc) * (playersycoord - ylocc)));
            if (playerdistancefromship < 2000)
            {
                neutralaiarray[neutralaiarray.length] = _root.neutralships[q];
            } // end if
        } // end of for
    } // End of the function
    lasttarget = -1;
    currenttargetname = "No Target";
    currenttargetbounty = "";
    istkeypressed = false;
    targettype = "none";
    _root.targetinfo[0] = "None";
    _root.targetinfo[1] = 0;
    _root.targetinfo[2] = 0;
    _root.targetinfo[3] = "";
    this.currenttargetname.html = true;
    shielddisp = "";
    structdisp = "";
    targetName = new Color("currenttargetname");
    targetName.setRGB(16777215);
    _root.targetinfo[10] = "enemy";
    lasttarget = -1;
    lasttargetshiptype = null;
    currenttargetdist = "";
    targetpointer._visible = false;
    currentinterval = 0;
    intervalwait = 30;
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (keyDown)
{
    if (Key.isDown(84) && _root.gamechatinfo[4] == false)
    {
        if (istkeypressed == false)
        {
            func_setlocalnpcs();
            if (_root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length > 0)
            {
                ++lasttarget;
                if (lasttarget >= _root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length)
                {
                    lasttarget = 0;
                } // end if
                if (lasttarget < _root.otherplayership.length)
                {
                    currenttargetname = _root.otherplayership[lasttarget][10];
                    currenttargetshiptype = _root.otherplayership[lasttarget][11];
                    currenttargetbounty = _root.otherplayership[lasttarget][12];
                    targettype = "other";
                    attachtargetship(currenttargetshiptype);
                    pilotno = _root.otherplayership[lasttarget][0];
                    func_sendstatuscheck(pilotno);
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length)
                {
                    aitargetnumber = lasttarget - int(_root.otherplayership.length);
                    currenttargetname = _root.aishipshosted[aitargetnumber][7];
                    currenttargetshiptype = _root.aishipshosted[aitargetnumber][6];
                    currenttargetbounty = "400";
                    targettype = "hosted";
                    attachtargetship(currenttargetshiptype);
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length)
                {
                    neuttargetnumber = lasttarget - _root.otherplayership.length - _root.aishipshosted.length;
                    currenttargetname = neutralaiarray[neuttargetnumber][11];
                    currenttargetshiptype = neutralaiarray[neuttargetnumber][4];
                    currenttargetbounty = "N/A";
                    targettype = "neutral";
                    attachtargetship(currenttargetshiptype);
                }
                else
                {
                    currentsectioritemnumber = lasttarget - (_root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length);
                    currenttargetname = _root.sectoritemsfortarget[currentsectioritemnumber][0];
                    currenttargetbounty = "UNKNOWN";
                    targettype = "sectoritem";
                    removeMovieClip (targetship);
                } // end else if
                _root.targetinfo[0] = currenttargetname;
                checkorsettarget();
            }
            else
            {
                settonotarget();
            } // end if
        } // end else if
        istkeypressed = true;
    }
    else
    {
        istkeypressed = false;
    } // end else if
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (keyUp)
{
    if (Key.isDown(84))
    {
        istkeypressed = true;
    }
    else
    {
        istkeypressed = false;
    } // end else if
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (enterFrame)
{
    ++currentinterval;
    if (currentinterval > intervalwait)
    {
        currentinterval = 0;
        func_setlocalnpcs();
        if (_root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length < 1)
        {
            if (lasttarget > -1)
            {
                settonotarget();
                lasttarget = -1;
            } // end if
        }
        else
        {
            checkorsettarget();
        } // end if
    } // end else if
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (load)
{
    function settonotarget()
    {
        currenttargetname = "No Target";
        currenttargetbounty = "";
        removeMovieClip (targetship);
        lasttarget = -1;
        _root.targetinfo[0] = "None";
        _root.targetinfo[1] = 0;
        _root.targetinfo[2] = 0;
        _root.targetinfo[3] = "";
        lasttargetshiptype = "";
        currenttargetdist = "";
        targetpointer._visible = false;
        _root.targetinfo[10] = "enemy";
        shielddisp = "";
        structdisp = "";
    } // End of the function
    function checkorsettarget()
    {
        func_setlocalnpcs();
        if (_root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length > 0)
        {
            if (lasttarget == -1)
            {
                lasttarget = 0;
                if (lasttarget < _root.otherplayership.length)
                {
                    currenttargetname = _root.otherplayership[lasttarget][10];
                    currenttargetshiptype = _root.otherplayership[lasttarget][11];
                    currenttargetbounty = _root.otherplayership[lasttarget][12];
                    targettype = "other";
                    pilotno = _root.otherplayership[lasttarget][0];
                    func_sendstatuscheck(pilotno);
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length)
                {
                    aitargetnumber = lasttarget - int(_root.otherplayership.length);
                    currenttargetname = _root.aishipshosted[aitargetnumber][7];
                    currenttargetshiptype = _root.aishipshosted[aitargetnumber][6];
                    currenttargetbounty = _root.aishipshosted[aitargetnumber][10];
                    targettype = "hosted";
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length)
                {
                    neuttargetnumber = lasttarget - _root.otherplayership.length - _root.aishipshosted.length;
                    currenttargetname = neutralaiarray[neuttargetnumber][11];
                    currenttargetshiptype = neutralaiarray[neuttargetnumber][4];
                    currenttargetbounty = "N/A";
                    targettype = "neutral";
                    attachtargetship(currenttargetshiptype);
                }
                else
                {
                    currentsectioritemnumber = lasttarget - (_root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length);
                    currenttargetname = _root.sectoritemsfortarget[currentsectioritemnumber][0];
                    currenttargetbounty = "UNKNOWN";
                    targettype = "sectoritem";
                    removeMovieClip (targetship);
                } // end else if
                _root.targetinfo[0] = currenttargetname;
                attachtargetship(currenttargetshiptype);
            }
            else if (currenttargetname != _root.otherplayership[lasttarget][10] || currenttargetname != _root.aishipshosted[aitargetnumber][7] || currenttargetname != neutralaiarray[neuttargetnumber][11] || currenttargetname != _root.sectoritemsfortarget[currentsectioritemnumber][0])
            {
                namestillexists = false;
                for (jjcq = 0; jjcq < _root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length; jjcq++)
                {
                    if (jjcq < _root.otherplayership.length)
                    {
                        if (currenttargetname == _root.otherplayership[jjcq][10])
                        {
                            namestillexists = true;
                            lasttarget = jjcq;
                        } // end if
                        continue;
                    } // end if
                    if (jjcq < _root.otherplayership.length + _root.aishipshosted.length)
                    {
                        aitargetnumber = jjcq - _root.otherplayership.length;
                        if (currenttargetname == _root.aishipshosted[aitargetnumber][7])
                        {
                            namestillexists = true;
                            lasttarget = jjcq;
                            jjcq = 99999;
                        } // end if
                        continue;
                    } // end if
                    if (jjcq < _root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length)
                    {
                        neuttargetnumber = jjcq - _root.otherplayership.length - _root.aishipshosted.length;
                        if (String(currenttargetname) == String(neutralaiarray[neuttargetnumber][11]))
                        {
                            namestillexists = true;
                            lasttarget = jjcq;
                            jjcq = 99999;
                        } // end if
                        continue;
                    } // end if
                    currentsectioritemnumber = jjcq - _root.otherplayership.length - _root.aishipshosted.length - neutralaiarray.length;
                    if (currenttargetname == _root.sectoritemsfortarget[currentsectioritemnumber][0])
                    {
                        namestillexists = true;
                        lasttarget = jjcq;
                        jjcq = 99999;
                    } // end if
                } // end of for
                if (namestillexists != true)
                {
                    settonotarget();
                    currentinterval = 9999;
                } // end if
            } // end else if
            if (_root.targetinfo[0] != "None")
            {
                if (targettype == "other")
                {
                    _root.targetinfo[1] = _root.otherplayership[lasttarget][1];
                    _root.targetinfo[2] = _root.otherplayership[lasttarget][2];
                    _root.targetinfo[5] = _root.otherplayership[lasttarget][5];
                    _root.targetinfo[6] = _root.otherplayership[lasttarget][4];
                    _root.targetinfo[7] = "otherplayership";
                    _root.targetinfo[8] = lasttarget;
                    _root.gamedisplayarea["otherplayership" + _root.otherplayership[lasttarget][0]].attachMovie("shiptargeted", "shiptargeted", 52);
                    set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[lasttarget][0] + ".shiptargeted.shipid", _root.targetinfo[0]);
                    targetcursorColor = new Color("_root.gamedisplayarea.otherplayership" + _root.otherplayership[lasttarget][0] + ".shiptargeted");
                    if (_root.playershipstatus[5][2] != _root.otherplayership[lasttarget][13] || _root.playershipstatus[5][2] == "N/A")
                    {
                        _root.targetinfo[10] = "enemy";
                        targetcursorColor.setRGB(16711680);
                    }
                    else
                    {
                        _root.targetinfo[10] = "friend";
                        targetcursorColor.setRGB(16776960);
                    } // end else if
                    if (_root.otherplayership[lasttarget][16] == "C" || _root.otherplayership[lasttarget][16] == "S")
                    {
                        func_targetlocation(true);
                    }
                    else
                    {
                        func_targetlocation(false);
                    } // end if
                } // end else if
                if (targettype == "hosted")
                {
                    _root.targetinfo[1] = _root.aishipshosted[aitargetnumber][1];
                    _root.targetinfo[2] = _root.aishipshosted[aitargetnumber][2];
                    _root.targetinfo[5] = _root.aishipshosted[aitargetnumber][3];
                    _root.targetinfo[6] = _root.aishipshosted[aitargetnumber][4];
                    _root.targetinfo[7] = "aiship";
                    _root.targetinfo[8] = aitargetnumber;
                    _root.targetinfo[10] = "enemy";
                    _root.gamedisplayarea["aiship" + _root.aishipshosted[aitargetnumber][0]].attachMovie("shiptargeted", "shiptargeted", 52);
                    set("_root.gamedisplayarea.aiship" + _root.aishipshosted[aitargetnumber][0] + ".shiptargeted.shipid", _root.targetinfo[0]);
                    targetcursorColor = new Color("_root.gamedisplayarea.aiship" + _root.aishipshosted[aitargetnumber][0] + ".shiptargeted");
                    targetcursorColor.setRGB(16711680);
                    func_targetlocation(false);
                } // end if
                if (targettype == "neutral")
                {
                    labell = neutralaiarray[neuttargetnumber][10];
                    _root.targetinfo[1] = _root.gamedisplayarea[labell].xcoord;
                    _root.targetinfo[2] = _root.gamedisplayarea[labell].ycoord;
                    _root.targetinfo[5] = _root.gamedisplayarea[labell].currentvelocity;
                    _root.targetinfo[6] = _root.gamedisplayarea[labell].rotation;
                    _root.targetinfo[7] = "neutral";
                    _root.targetinfo[8] = lasttarget;
                    _root.gamedisplayarea[labell].attachMovie("shiptargeted", "shiptargeted", 52);
                    set("_root.gamedisplayarea." + labell + ".shiptargeted.shipid", _root.targetinfo[0]);
                    targetcursorColor = new Color("_root.gamedisplayarea." + labell + ".shiptargeted");
                    targetcursorColor.setRGB(16711680);
                    func_targetlocation(false);
                } // end if
                if (targettype == "sectoritem")
                {
                    _root.targetinfo[1] = _root.sectoritemsfortarget[currentsectioritemnumber][1];
                    _root.targetinfo[2] = _root.sectoritemsfortarget[currentsectioritemnumber][2];
                    _root.targetinfo[5] = 0;
                    _root.targetinfo[6] = 0;
                    _root.targetinfo[7] = "sectoritem";
                    _root.targetinfo[8] = currentsectioritemnumber;
                    _root.targetinfo[10] = "enemy";
                    if (_root.playershipstatus[5][10] == _root.targetinfo[0])
                    {
                        _root.targetinfo[10] = "friend";
                    }
                    else if (_root.teamdeathmatch == true)
                    {
                        if (_root.targetinfo[0] == _root.teambases[_root.playershipstatus[5][2]][0])
                        {
                            _root.targetinfo[10] = "friend";
                        } // end if
                    } // end else if
                    func_targetlocation(false);
                    removeMovieClip (targetship);
                } // end if
            } // end if
            if (currenttargetbounty != _root.otherplayership[lasttarget][12] && targettype == "other")
            {
                currenttargetbounty = _root.otherplayership[lasttarget][12];
            }
            else if (currenttargetbounty != _root.aishipshosted[aitargetnumber][10] && targettype == "hosted")
            {
                currenttargetbounty = _root.aishipshosted[aitargetnumber][10];
            } // end if
        } // end else if
        if (_root.targetinfo[10] == "enemy")
        {
            targetName.setRGB(16711680);
        }
        else
        {
            targetName.setRGB(16776960);
        } // end else if
        if (targettype == "other")
        {
            if (_root.otherplayership[lasttarget][40] == null)
            {
                pilotno = _root.otherplayership[lasttarget][0];
                func_sendstatuscheck(pilotno);
                _root.otherplayership[lasttarget][40] = "waiting";
                structdisp = "Waiting";
            }
            else if (_root.otherplayership[lasttarget][40] != "waiting")
            {
                currentshield = Math.round(_root.otherplayership[lasttarget][41] + (getTimer() - _root.otherplayership[lasttarget][44]) * _root.otherplayership[lasttarget][42] / 1000);
                _root.otherplayership[lasttarget][44] = getTimer();
                if (currentshield > _root.otherplayership[lasttarget][43])
                {
                    currentshield = _root.otherplayership[lasttarget][43];
                } // end if
                _root.otherplayership[lasttarget][41] = currentshield;
                shielddisp = currentshield;
                structdisp = _root.otherplayership[lasttarget][40];
            } // end else if
        }
        else if (targettype == "hosted")
        {
            if (_root.aishipshosted[aitargetnumber][20] > 0)
            {
                shielddisp = Math.round(_root.aishipshosted[aitargetnumber][22]);
                structdisp = Math.round(_root.aishipshosted[aitargetnumber][20]);
                if (structdisp < 0)
                {
                    structdisp = 0;
                } // end if
            } // end if
        }
        else if (targettype == "sectoritem")
        {
            shielddisp = Math.round(_root.sectoritemsfortarget[currentsectioritemnumber][5]);
            structdisp = Math.round(_root.sectoritemsfortarget[currentsectioritemnumber][6]);
            if (structdisp < 0)
            {
                structdisp = 0;
            } // end if
        }
        else if (targettype == "neutral")
        {
            labell = neutralaiarray[neuttargetnumber][10];
            shielddisp = Math.round(_root.gamedisplayarea[labell].currentshipshield);
            structdisp = Math.round(_root.gamedisplayarea[labell].shiphealth);
            if (structdisp < 0)
            {
                structdisp = 0;
            } // end else if
        } // end else if
    } // End of the function
    function func_targetlocation(iscloakorstealth)
    {
        if (iscloakorstealth == false)
        {
            playersxcoord = _root.shipcoordinatex;
            playersycoord = _root.shipcoordinatey;
            currenttargetdist = _root.targetinfo[3] = Math.round(Math.sqrt((playersxcoord - _root.targetinfo[1]) * (playersxcoord - _root.targetinfo[1]) + (playersycoord - _root.targetinfo[2]) * (playersycoord - _root.targetinfo[2])));
            playeranglefromship = 180 - Math.atan2(_root.targetinfo[1] - playersxcoord, _root.targetinfo[2] - playersycoord) / 0.017453;
            targetpointer._visible = true;
            targetpointer._rotation = playeranglefromship;
            playeranglefromship = playeranglefromship - _root.playershipfacing;
            if (playeranglefromship > 360)
            {
                playeranglefromship = playeranglefromship - 360;
            } // end if
            if (playeranglefromship < 0)
            {
                playeranglefromship = playeranglefromship + 360;
            } // end if
            _root.targetinfo[4] = Math.round(playeranglefromship);
        }
        else if (iscloakorstealth == true)
        {
            targetpointer._visible = false;
            currenttargetdist = "HIDDEN";
            _root.targetinfo[3] = 9999999999.000000;
        } // end else if
    } // End of the function
}

// [onClipEvent of sprite 1522 in frame 10]
onClipEvent (load)
{
    this._visible = false;
    pingintervalcheck = 8000;
    lastpingcheck = getTimer() + 2000;
}

// [onClipEvent of sprite 1522 in frame 10]
onClipEvent (enterFrame)
{
    curenttime = getTimer();
    if (curenttime > lastpingcheck)
    {
        lastpingcheck = curenttime + pingintervalcheck;
        information = "PING`" + curenttime + "~";
        _root.mysocket.send(information);
    } // end if
    for (i = 0; i < _root.errorcheckdata.length; i++)
    {
        if (curenttime > _root.errorcheckdata[i][1])
        {
            _root.mysocket.send(_root.errorcheckdata[i][0]);
            _root.errorcheckdata[i][1] = curenttime + _root.errorcheckdelay;
        } // end if
    } // end of for
}

// [onClipEvent of sprite 1525 in frame 10]
onClipEvent (load)
{
    function func_spawnameteor(spawnchance)
    {
        minspawnchance = 0.820000;
        maxmeteors = 2;
        if (spawnchance > minspawnchance && _root.incomingmeteor.length < maxmeteors)
        {
            _root.func_beginmeteor();
        } // end if
    } // End of the function
    function func_randompiratebase(spawnchance)
    {
        numberofpirates = 0;
        for (i = 0; i < _root.playersquadbases.length; i++)
        {
            if (_root.playersquadbases[i][0].substr(0, 3) == "*AI")
            {
                ++numberofpirates;
            } // end if
        } // end of for
        if (numberofpirates < maxpiratebases && spawnchance > minpiratebaseodds)
        {
            for (attempts = 0; attempts < maxtriesforpiratebase; attempts++)
            {
                xsector = Math.round(Math.random() * _root.sectorinformation[0][0]);
                ysector = Math.round(Math.random() * _root.sectorinformation[0][1]);
                gridsaroundtaken = false;
                for (i = 0; i < _root.sectormapitems.length; i++)
                {
                    if (_root.sectormapitems[i][10] == xsector || _root.sectormapitems[i][10] == xsector - 1 || _root.sectormapitems[i][10] == xsector + 1)
                    {
                        if (_root.sectormapitems[i][11] == ysector || _root.sectormapitems[i][11] == ysector - 1 || _root.sectormapitems[i][11] == ysector + 1)
                        {
                            gridsaroundtaken = true;
                        } // end if
                    } // end if
                } // end of for
                for (i = 0; i < _root.playersquadbases.length; i++)
                {
                    if (_root.playersquadbases[i][4] == xsector || _root.playersquadbases[i][4] == xsector - 1 || _root.playersquadbases[i][4] == xsector + 1)
                    {
                        if (_root.playersquadbases[i][5] == ysector || _root.playersquadbases[i][5] == ysector - 1 || _root.playersquadbases[i][5] == ysector + 1)
                        {
                            gridsaroundtaken = true;
                        } // end if
                    } // end if
                } // end of for
                if (gridsaroundtaken == false)
                {
                    break;
                } // end if
            } // end of for
            if (gridsaroundtaken == false)
            {
                squadbasetype = 0;
                func_createapiratebase(squadbasetype, xsector, ysector);
            } // end if
        } // end if
    } // End of the function
    function func_createapiratebase(squadbasetype, xsector, ysector)
    {
        function func_setuppiratebase(squadbasetype, baseidname)
        {
            guntypes = _root.squadbaseinfo[squadbasetype][2][1];
            this.newbaseGuns = new XML();
            this.newbaseGuns.load(_root.pathtoaccounts + "squadbases.php?mode=changegun&baseid=" + baseidname + "&gunone=" + func_piratehardpoint(squadbasetype) + "&guntwo=" + func_piratehardpoint(squadbasetype) + "&gunthree=" + func_piratehardpoint(squadbasetype) + "&gunfour=" + func_piratehardpoint(squadbasetype));
            this.newbaseShield = new XML();
            this.newbaseShield.load(_root.pathtoaccounts + "squadbases.php?mode=newshield&baseid=" + baseidname + "&newshield=" + _root.squadbaseinfo[squadbasetype][1][3]);
            this.newbaseStruct = new XML();
            this.newbaseStruct.load(_root.pathtoaccounts + "squadbases.php?mode=repairbase&baseid=" + baseidname + "&struct=" + _root.squadbaseinfo[squadbasetype][3][1] + "&maxstruct=" + _root.squadbaseinfo[squadbasetype][3][1]);
            this.newbaseMissile = new XML();
            this.newbaseMissile.load(_root.pathtoaccounts + "squadbases.php?mode=missileshot&baseid=" + baseidname + "&shots=-" + 10000);
        } // End of the function
        xcoord = xsector * _root.sectorinformation[1][0] - Math.round(_root.sectorinformation[1][0] / 2);
        ycoord = ysector * _root.sectorinformation[1][1] - Math.round(_root.sectorinformation[1][1] / 2);
        baseidname = "*AI" + (Math.round(Math.random() * 89) + 10);
        currentsystem = _root.playershipstatus[5][1];
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadbases.php?mode=createbase&baseid=" + baseidname + "&system=" + currentsystem + "&xcoord=" + xcoord + "&ycoord=" + ycoord);
        newsquadVars.onLoad = function (success)
        {
            datatosend = "SA~NEWS`SB`CREATED`" + baseidname + "`" + basetype + "`" + xcoord + "`" + ycoord + "~";
            _root.mysocket.send(datatosend);
            func_setuppiratebase(squadbasetype, baseidname);
        };
    } // End of the function
    function func_piratehardpoint(squadbasetype)
    {
        guntype = _root.squadbaseinfo[squadbasetype][2][1] - Math.round(Math.random() * 2);
        return (guntype);
    } // End of the function
    function checkforrepstarbases()
    {
        repairedabase = false;
        newsmessage = "";
        for (ww = 0; ww < _root.starbaselocation.length; ww++)
        {
            currenttime = getTimer();
            if (_root.starbaselocation[ww][5] != "ACTIVE")
            {
                rerpairtime = Number(_root.starbaselocation[ww][5]);
                if (rerpairtime < currenttime && !isNaN(rerpairtime))
                {
                    repairedabase = true;
                    _root.starbaselocation[ww][5] = "ACTIVE";
                    newsmessage = newsmessage + ("Starbase " + _root.starbaselocation[ww][0] + " has been repaired. ");
                } // end if
            } // end if
        } // end of for
        if (repairedabase == true)
        {
            _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
            xsector = _root.playershipstatus[6][0];
            ysector = _root.playershipstatus[6][1];
            _root.gamedisplayarea.keyboardscript.sectoritemsdisp(xsector, ysector);
        } // end if
        func_checkforpriceresets();
    } // End of the function
    randomeventcheckinterval = 200000;
    nextcheckat = getTimer() + 60000;
    minpiratebaseodds = 0.850000;
    maxpiratebases = 3;
    maxtriesforpiratebase = 50;
    setInterval(checkforrepstarbases, 20000);
    lastime = getTimer();
}

// [onClipEvent of sprite 1525 in frame 10]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (_root.gamechatinfo[2][2] == true)
    {
        if (_root.gamechatinfo[2][3] < currenttime)
        {
            _root.gamechatinfo[2][2] = false;
            _root.gamechatinfo[2][3] = 0;
            colourtouse = _root.systemchattextcolor;
            message = "HOST: You Are No Longer Muted";
            _root.enterintochat(message, colourtouse);
        } // end if
    } // end if
    if (_root.teamdeathmatch != true && _root.isgameracingzone != true)
    {
        timelapsed = currenttime - lastime;
        lastime = currenttime;
        if (nextcheckat < getTimer())
        {
            _root.func_checkfortoomuchaddedandset();
            nextcheckat = getTimer() + randomeventcheckinterval;
            whattospawn = Math.round(Math.random() * 5);
            if (whattospawn == 0)
            {
                spawnchance = Math.random();
                func_randompiratebase(spawnchance);
            }
            else if (whattospawn == 1)
            {
                spawnchance = Math.random();
                func_spawnameteor(spawnchance);
            }
            else if (whattospawn == 2 || whattospawn == 3)
            {
                spawnchance = Math.random();
                if (spawnchance > 0.300000)
                {
                    func_creationofaship();
                } // end if
            }
            else
            {
                spawnchance = Math.random();
                if (spawnchance > 0.300000)
                {
                    func_createapricechange();
                } // end else if
            } // end else if
        } // end else if
        if (_root.incomingmeteor.length > 0)
        {
            timechange = timelapsed / 1000;
            for (ii = 0; ii < _root.incomingmeteor.length; ii++)
            {
                if (currenttime > _root.incomingmeteor[ii][12])
                {
                    _root.func_meteorhitbase(_root.incomingmeteor[ii][0]);
                    _root.incomingmeteor.splice(ii, 1);
                    --ii;
                    continue;
                } // end if
                _root.incomingmeteor[ii][1] = _root.incomingmeteor[ii][1] + _root.incomingmeteor[ii][10] * timechange;
                _root.incomingmeteor[ii][2] = _root.incomingmeteor[ii][2] + _root.incomingmeteor[ii][11] * timechange;
                if (_root.incomingmeteor[ii][9] != false)
                {
                    if (_root.incomingmeteor[ii][9] < currenttime)
                    {
                        datatosend = "MET`DM`" + _root.incomingmeteor[ii][0] + "`" + _root.incomingmeteor[ii][6] + "`" + _root.playershipstatus[3][0] + "`" + _root.errorchecknumber + "~";
                        _root.mysocket.send(datatosend);
                        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
                        _root.incomingmeteor[ii][9] = false;
                    } // end if
                } // end if
            } // end of for
        } // end if
        if (_root.neutralships.length > 0)
        {
            for (jjj = 0; jjj < _root.neutralships.length; jjj++)
            {
                if (_root.neutralships[jjj][7] < currenttime)
                {
                    func_removeaship(_root.neutralships[jjj][10]);
                } // end if
            } // end of for
        } // end if
    } // end if
}

// [Action in Frame 10]
_root.maingameplayingframe = _root._currentframe;

stop ();
