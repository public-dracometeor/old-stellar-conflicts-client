﻿// Action script...

// [Action in Frame 9]
function func_neutralships(currentthread)
{
    if (currentthread[1] == "death")
    {
        shipid = String(currentthread[2]);
        for (tw = 0; tw < _root.neutralships.length; tw++)
        {
            if (shipid == String(_root.neutralships[tw][10]))
            {
                shipname = _root.neutralships[tw][11];
                _root.neutralships.splice(tw, 1);
                killerid = Number(currentthread[3]);
                for (jj = 0; jj < _root.currentonlineplayers.length && dataupdated == false; jj++)
                {
                    if (killerid == String(_root.currentonlineplayers[jj][0]))
                    {
                        killerrealname = _root.currentonlineplayers[jj][1];
                    } // end if
                } // end of for
                message = "HOST: " + shipname + " (" + currentthread[4] + ") killed by " + killerrealname;
                _root.enterintochat(message, _root.regularchattextcolor);
                break;
            } // end if
        } // end of for
    }
    else if (currentthread[1] == "create")
    {
        _root.randomegameevents.func_creationofnpcaiships(currentthread[2], currentthread[3], currentthread[4], currentthread[5], currentthread[6], currentthread[7], currentthread[8], currentthread[9], currentthread[10]);
    } // end else if
} // End of the function
function errorcontrolchecking(errorno)
{
    for (q = 0; q < _root.errorcheckdata.length; q++)
    {
        if (_root.errorcheckdata[q][2] == errorno)
        {
            _root.errorcheckdata.splice(q, 1);
            break;
        } // end if
    } // end of for
} // End of the function
function func_statsshiphit(currentthread)
{
    if (currentthread[0] == "GH" || currentthread[0] == "MH")
    {
        playergothit = currentthread[1];
        for (jj = 0; jj < _root.otherplayership.length; jj++)
        {
            if (_root.otherplayership[jj][0] == playergothit)
            {
                if (currentthread[4] == "SH")
                {
                    _root.otherplayership[jj][41] = Number(currentthread[5]);
                }
                else if (currentthread[4] == "ST")
                {
                    _root.otherplayership[jj][40] = Number(currentthread[5]);
                    if (_root.otherplayership[jj][40] < 0)
                    {
                        _root.otherplayership[jj][40] = 0;
                    } // end if
                    _root.otherplayership[jj][41] = 0;
                } // end else if
                _root.otherplayership[jj][44] = getTimer();
            } // end if
        } // end of for
    } // end if
} // End of the function
function func_otherplayersai(currentthread)
{
    currentothership = _root.otherplayership.length;
    currentname = currentthread[1];
    qz = 0;
    doesplayerexist = false;
    while (qz < _root.otherplayership.length && doesplayerexist == false)
    {
        if (currentname == _root.otherplayership[qz][0])
        {
            doesplayerexist = true;
            currentothership = qz;
            _root.otherplayership[currentothership][0] = currentthread[1];
            recievedxpos = Number(currentthread[2]);
            currentxposition = _root.otherplayership[currentothership][1];
            currentloc = _root.otherplayership[currentothership][30].length;
            _root.otherplayership[currentothership][30][currentloc] = new Array();
            _root.otherplayership[currentothership][30][currentloc][0] = recievedxpos;
            recievedypos = Number(currentthread[3]);
            _root.otherplayership[currentothership][30][currentloc][1] = recievedypos;
            _root.otherplayership[currentothership][30][currentloc][2] = Number(currentthread[4]);
            _root.otherplayership[currentothership][30][currentloc][3] = Number(currentthread[5]);
            _root.otherplayership[currentothership][30][currentloc][6] = new Array();
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = _root.otherplayership[currentothership][30][currentloc][6][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = _root.otherplayership[currentothership][30][currentloc][6][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][1] = _root.otherplayership[currentothership][30][currentloc][6][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][1] = _root.otherplayership[currentothership][30][currentloc][6][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            systemtime = String(getTimer() + _root.clocktimediff);
            systemtime = Number(systemtime.substr(systemtime.length - 4));
            jumpahead = systemtime - Number(currentthread[8]);
            if (jumpahead < -1000)
            {
                jumpahead = jumpahead + 10000;
            } // end if
            jumptime = _root.shipositiondelay - jumpahead + getTimer();
            _root.otherplayership[currentothership][30][currentloc][9] = jumptime;
            _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        } // end if
        ++qz;
    } // end while
    if (doesplayerexist == false)
    {
        currentothership = _root.otherplayership.length;
        _root.otherplayership[currentothership] = new Array();
        _root.otherplayership[currentothership][0] = currentthread[1];
        _root.otherplayership[currentothership][1] = int(currentthread[2]);
        _root.otherplayership[currentothership][2] = int(currentthread[3]);
        _root.otherplayership[currentothership][3] = int(currentthread[4]);
        _root.otherplayership[currentothership][4] = int(currentthread[5]);
        _root.otherplayership[currentothership][5] = new Array();
        _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        _root.otherplayership[currentothership][10] = _root.otherplayership[currentothership][0];
        _root.otherplayership[currentothership][11] = Number(currentthread[7]);
        _root.otherplayership[currentothership][12] = 400;
        _root.otherplayership[currentothership][7] = _root.curenttime + shiptimeoutime * 2;
        _root.otherplayership[currentothership][13] = "N/A";
        _root.otherplayership[currentothership][15] = "alive";
        for (jjj = 0; jjj < currentthread[6].length; jjj++)
        {
            if (currentthread[6].charAt(jjj) == "U")
            {
                _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
            } // end if
            if (currentthread[6].charAt(jjj) == "D")
            {
                _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
            } // end if
            if (currentthread[6].charAt(jjj) == "L")
            {
                _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
            } // end if
            if (currentthread[6].charAt(jjj) == "R")
            {
                _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
            } // end if
        } // end of for
        ++_root.currentailvl;
        if (_root.currentailvl > 100)
        {
            _root.currentailvl = 1;
        } // end if
        _root.gamedisplayarea.attachMovie("shiptype" + _root.otherplayership[currentothership][11], "otherplayership" + _root.otherplayership[currentothership][0], 11100 + _root.currentailvl);
        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
        set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".shiptype", Number(_root.otherplayership[currentothership][11]));
        set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".gothit", false);
    } // end if
} // End of the function
function otherpilot(currentthread)
{
    currentothership = _root.otherplayership.length;
    currentname = currentthread[1];
    qz = 0;
    doesplayerexist = false;
    while (qz < _root.otherplayership.length && doesplayerexist == false)
    {
        if (currentname == _root.otherplayership[qz][0])
        {
            doesplayerexist = true;
            currentothership = qz;
            _root.otherplayership[currentothership][0] = currentthread[1];
            allowance = int(currentthread[5]) / 7;
            recievedxpos = int(currentthread[2]);
            currentloc = _root.otherplayership[currentothership][30].length;
            _root.otherplayership[currentothership][30][currentloc] = new Array();
            _root.otherplayership[currentothership][30][currentloc][0] = recievedxpos;
            recievedypos = int(currentthread[3]);
            _root.otherplayership[currentothership][30][currentloc][1] = recievedypos;
            _root.otherplayership[currentothership][30][currentloc][2] = int(currentthread[4]);
            _root.otherplayership[currentothership][30][currentloc][3] = Number(currentthread[5]);
            _root.otherplayership[currentothership][30][currentloc][6] = new Array();
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = _root.otherplayership[currentothership][30][currentloc][6][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = _root.otherplayership[currentothership][30][currentloc][6][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][1] = _root.otherplayership[currentothership][30][currentloc][6][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][1] = _root.otherplayership[currentothership][30][currentloc][6][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            if (_root.otherplayership[currentothership][16] != currentthread[7])
            {
                newaction = String(currentthread[7]);
                if (newaction == "C" && _root.otherplayership[currentothership][16] != "D")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, false);
                    _root.gamedisplayarea.attachMovie("shipactivateothercloak", "shipactivateothercloak", 69);
                    _root.gamedisplayarea.shipactivateothercloak._x = _root.otherplayership[currentothership][1];
                    _root.gamedisplayarea.shipactivateothercloak._y = _root.otherplayership[currentothership][2];
                    _root.otherplayership[currentothership][16] = newaction;
                }
                else if (newaction == "S" && _root.otherplayership[currentothership][16] != "D")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, true);
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _alpha, 100);
                    _root.otherplayership[currentothership][16] = "S";
                }
                else if (newaction != "S" && newaction != "C")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, true);
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _alpha, 100);
                    if (_root.otherplayership[currentothership][16] == "C")
                    {
                        _root.gamedisplayarea["otherplayership" + _root.otherplayership[currentothership][0]].attachMovie("shipactivateotherdecloak", "shipactivateotherdecloak", 69);
                        
                    } // end if
                    _root.otherplayership[currentothership][16] = newaction;
                } // end else if
            } // end else if
            systemtime = String(getTimer() + _root.clocktimediff);
            systemtime = Number(systemtime.substr(systemtime.length - 4));
            jumpahead = systemtime - Number(currentthread[8]);
            if (jumpahead < -1000)
            {
                jumpahead = jumpahead + 10000;
            } // end if
            jumptime = _root.shipositiondelay - jumpahead + getTimer();
            _root.otherplayership[currentothership][30][currentloc][9] = jumptime;
            _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        } // end if
        ++qz;
    } // end while
    if (doesplayerexist == false)
    {
        currentothership = qz;
        _root.otherplayership[currentothership] = new Array();
        _root.otherplayership[currentothership][0] = currentthread[1];
        _root.otherplayership[currentothership][1] = int(currentthread[2]);
        _root.otherplayership[currentothership][2] = int(currentthread[3]);
        _root.otherplayership[currentothership][3] = int(currentthread[4]);
        _root.otherplayership[currentothership][4] = int(currentthread[5]);
        _root.otherplayership[currentothership][5] = new Array();
        _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        _root.otherplayership[currentothership][7] = _root.curenttime + shiptimeoutime * 2;
        _root.otherplayership[currentothership][30] = new Array();
        jjjj = 0;
        dataupdated = false;
        while (jjjj < _root.currentonlineplayers.length && dataupdated == false)
        {
            if (_root.otherplayership[currentothership][0] == _root.currentonlineplayers[jjjj][0])
            {
                dataupdated = true;
                _root.otherplayership[currentothership][10] = _root.currentonlineplayers[jjjj][1];
                _root.otherplayership[currentothership][11] = _root.currentonlineplayers[jjjj][2];
                _root.otherplayership[currentothership][12] = _root.currentonlineplayers[jjjj][3];
                _root.otherplayership[currentothership][13] = _root.currentonlineplayers[jjjj][4];
            } // end if
            ++jjjj;
        } // end while
        if (dataupdated == false)
        {
            _root.otherplayership.splice(currentothership, 1);
        }
        else
        {
            _root.otherplayership[currentothership][15] = "alive";
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][5][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            _root.otherplayership[currentothership][16] = String(currentthread[7]);
            _root.gamedisplayarea.attachMovie("shiptype" + _root.otherplayership[currentothership][11], "otherplayership" + _root.otherplayership[currentothership][0], 2300 + Number(_root.otherplayership[currentothership][0]));
            if (_root.otherplayership[currentothership][16] == "C")
            {
                setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, false);
            } // end if
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".shiptype", Number(_root.otherplayership[currentothership][11]));
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".gothit", false);
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".isplayervisible", true);
            _root.otherplayership[currentothership][40] = null;
            _root.otherplayership[currentothership][41] = null;
            _root.otherplayership[currentothership][42] = null;
            _root.otherplayership[currentothership][43] = null;
            _root.otherplayership[currentothership][44] = null;
            _root.otherplayership[currentothership][46] = null;
        } // end if
    } // end else if
} // End of the function
function func_closegameserver()
{
    information = "QUIT`" + _root.errorchecknumber + "`~";
    _root.mysocket.send(information);
    _root.errorcheckedmessage(information, _root.errorchecknumber);
} // End of the function
function login()
{
    _root.currentonlineplayers = new Array();
    information = "LOGIN`" + _root.playershipstatus[3][0] + "`" + _root.playershipstatus[3][2] + "`" + _root.playershipstatus[5][0] + "`" + _root.playershipstatus[5][3] + "`" + _root.playershipstatus[5][2] + "`" + _root.playershipstatus[5][9] + "`" + _root.playershipstatus[5][10] + "`" + _root.playershipstatus[3][3] + "~";
    _root.mysocket.send(information);
} // End of the function
function xmlhandler(doc)
{
    loadedvars = String(doc);
    dataprocess(loadedvars);
} // End of the function
function dataprocess(myLoadVars)
{
    curenttime = getTimer();
    gunshot0fired = false;
    gunshot1fired = false;
    gunshot2fired = false;
    gunshot3fired = false;
    newinfo = loadedvars.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentthread = newinfo[i].split("`");
        if (currentthread[0] == "PI" && !isNaN(currentthread[1]) && _root.playershipstatus[3][0] != Number(currentthread[1]))
        {
            otherpilot(currentthread);
        }
        else if (currentthread[0] == "AI")
        {
            func_otherplayersai(currentthread);
        }
        else if (currentthread[0] == "MI")
        {
            xcoord = Number(currentthread[2]);
            ycoord = Number(currentthread[3]);
            missilename = currentthread[1] + "n" + Number(currentthread[7]);
            setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _rotation, Number(currentthread[5]));
            setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _x, xcoord - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _y, ycoord - _root.shipcoordinatey);
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".xposition", xcoord);
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".yposition", ycoord);
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".missiletype", Number(currentthread[6]));
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".velocity", Number(currentthread[4]));
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".rotation", Number(currentthread[5]));
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".currentkeypresses", currentthread[8]);
        } // end else if
        if (currentthread[0] == "MF")
        {
            if (_root.soundvolume != "off")
            {
                _root.missilefiresound.start();
            } // end if
            missileno = Number(currentthread[6]);
            if (_root.othermissilefire.length < 1)
            {
                _root.othermissilefire = new Array();
            } // end if
            if (_root.missile[missileno][8] == "SEEKER" || _root.missile[missileno][8] == "EMP" || _root.missile[missileno][8] == "DISRUPTER")
            {
                if (_root.playershipstatus[3][0] == Number(currentthread[9]))
                {
                    ++currentothermissileshot;
                    if (currentothermissileshot >= 1000)
                    {
                        currentothermissileshot = 1;
                    } // end if
                    lastvar = _root.othermissilefire.length;
                    _root.othermissilefire[lastvar] = new Array();
                    _root.othermissilefire[lastvar][0] = currentthread[1];
                    _root.othermissilefire[lastvar][1] = int(currentthread[2]);
                    _root.othermissilefire[lastvar][2] = int(currentthread[3]);
                    velocity = int(currentthread[4]);
                    _root.othermissilefire[lastvar][6] = int(currentthread[5]);
                    relativefacing = _root.othermissilefire[lastvar][6];
                    movementofanobjectwiththrust();
                    _root.othermissilefire[lastvar][3] = xmovement;
                    _root.othermissilefire[lastvar][4] = ymovement;
                    _root.othermissilefire[lastvar][7] = int(currentthread[6]);
                    _root.othermissilefire[lastvar][8] = currentothermissileshot;
                    _root.othermissilefire[lastvar][9] = int(currentthread[7]);
                    _root.othermissilefire[lastvar][10] = "other";
                    _root.othermissilefire[lastvar][5] = _root.curenttime + _root.missile[_root.othermissilefire[lastvar][7]][2];
                    systemtime = String(getTimer() + _root.clocktimediff);
                    systemtime = Number(systemtime.substr(systemtime.length - 4));
                    jumpahead = systemtime - Number(currentthread[8]);
                    if (jumpahead < -1000)
                    {
                        jumpahead = jumpahead + 10000;
                    } // end if
                    jumpahead = (jumpahead + _root.shipositiondelay) / 1000;
                    _root.othermissilefire[lastvar][1] = _root.othermissilefire[lastvar][1] + _root.othermissilefire[lastvar][3] * jumpahead;
                    _root.othermissilefire[lastvar][2] = _root.othermissilefire[lastvar][2] + _root.othermissilefire[lastvar][4] * jumpahead;
                    _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "fire", "othermissilefire" + _root.othermissilefire[lastvar][8], 999000 + _root.othermissilefire[lastvar][8]);
                    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _rotation, _root.othermissilefire[lastvar][6]);
                    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _x, _root.othermissilefire[lastvar][1] - _root.shipcoordinatex);
                    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _y, _root.othermissilefire[lastvar][2] - _root.shipcoordinatey);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xposition", _root.othermissilefire[lastvar][1]);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".yposition", _root.othermissilefire[lastvar][2]);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".missiletype", _root.othermissilefire[lastvar][7]);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".velocity", int(currentthread[4]));
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".seeking", "HOST");
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".shooterid", _root.othermissilefire[lastvar][0]);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".globalid", _root.othermissilefire[lastvar][9]);
                    _root.othermissilefire[lastvar][11] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._width / 2;
                    _root.othermissilefire[lastvar][12] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._height / 2;
                    _root.othermissilefire[lastvar][13] = "SEEKING";
                }
                else
                {
                    ++currentothermissileshot;
                    if (currentothermissileshot >= 1000)
                    {
                        currentothermissileshot = 1;
                    } // end if
                    systemtime = String(getTimer() + _root.clocktimediff);
                    systemtime = Number(systemtime.substr(systemtime.length - 4));
                    jumpahead = systemtime - Number(currentthread[8]);
                    if (jumpahead < -1000)
                    {
                        jumpahead = jumpahead + 10000;
                    } // end if
                    jumpahead = (jumpahead + _root.shipositiondelay) / 1000;
                    relativefacing = Number(currentthread[5]);
                    movementofanobjectwiththrust();
                    xcoord = Number(currentthread[2]) + xmovement * jumpahead;
                    ycoord = Number(currentthread[3]) + ymovement * jumpahead;
                    missilename = currentthread[1] + "n" + Number(currentthread[7]);
                    _root.gamedisplayarea.attachMovie("missile" + Number(currentthread[6]) + "fire", "othermissilefire" + missilename, 999000 + currentothermissileshot);
                    setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _rotation, Number(currentthread[5]));
                    setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _x, xcoord - _root.shipcoordinatex);
                    setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _y, ycoord - _root.shipcoordinatey);
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".xposition", xcoord);
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".yposition", ycoord);
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".missiletype", Number(currentthread[6]));
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".velocity", Number(currentthread[4]));
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".seeking", "OTHER");
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".rotation", Number(currentthread[5]));
                    _root.othermissilefire[lastvar][13] = "NOTSEEKING";
                } // end else if
            }
            else
            {
                ++currentothermissileshot;
                if (currentothermissileshot >= 1000)
                {
                    currentothermissileshot = 1;
                } // end if
                lastvar = _root.othermissilefire.length;
                _root.othermissilefire[lastvar] = new Array();
                _root.othermissilefire[lastvar][0] = currentthread[1];
                _root.othermissilefire[lastvar][1] = int(currentthread[2]);
                _root.othermissilefire[lastvar][2] = int(currentthread[3]);
                velocity = int(currentthread[4]);
                _root.othermissilefire[lastvar][6] = int(currentthread[5]);
                relativefacing = _root.othermissilefire[lastvar][6];
                movementofanobjectwiththrust();
                _root.othermissilefire[lastvar][3] = xmovement;
                _root.othermissilefire[lastvar][4] = ymovement;
                _root.othermissilefire[lastvar][7] = int(currentthread[6]);
                _root.othermissilefire[lastvar][8] = currentothermissileshot;
                _root.othermissilefire[lastvar][9] = int(currentthread[7]);
                _root.othermissilefire[lastvar][10] = "other";
                _root.othermissilefire[lastvar][5] = _root.curenttime + _root.missile[_root.othermissilefire[lastvar][7]][2];
                systemtime = String(getTimer() + _root.clocktimediff);
                systemtime = Number(systemtime.substr(systemtime.length - 4));
                jumpahead = systemtime - Number(currentthread[8]);
                if (jumpahead < -1000)
                {
                    jumpahead = jumpahead + 10000;
                } // end if
                jumpahead = (jumpahead + _root.shipositiondelay) / 1000;
                _root.othermissilefire[lastvar][1] = _root.othermissilefire[lastvar][1] + _root.othermissilefire[lastvar][3] * jumpahead;
                _root.othermissilefire[lastvar][2] = _root.othermissilefire[lastvar][2] + _root.othermissilefire[lastvar][4] * jumpahead;
                _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "fire", "othermissilefire" + _root.othermissilefire[lastvar][8], 999000 + _root.othermissilefire[lastvar][8]);
                setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _rotation, _root.othermissilefire[lastvar][6]);
                setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _x, _root.othermissilefire[lastvar][1] - _root.shipcoordinatex);
                setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _y, _root.othermissilefire[lastvar][2] - _root.shipcoordinatey);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xmovement", _root.othermissilefire[lastvar][3]);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".ymovement", _root.othermissilefire[lastvar][4]);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xposition", _root.othermissilefire[lastvar][1]);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".yposition", _root.othermissilefire[lastvar][2]);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".turning", "0");
                _root.othermissilefire[lastvar][11] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._width / 2;
                _root.othermissilefire[lastvar][12] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._height / 2;
                _root.othermissilefire[lastvar][13] = "NOTSEEKING";
            } // end else if
        }
        else if (currentthread[0] == "GF")
        {
            if (_root.othergunfire.length < 1)
            {
                _root.othergunfire = new Array();
            } // end if
            ++_root.currentotherplayshot;
            if (_root.currentotherplayshot >= 500)
            {
                _root.currentotherplayshot = 1;
            } // end if
            lastvar = _root.othergunfire.length;
            ++_root.othergunfirecount;
            _root.othergunfire[lastvar] = new Array();
            _root.othergunfire[lastvar][0] = currentthread[1];
            _root.othergunfire[lastvar][1] = int(currentthread[2]);
            _root.othergunfire[lastvar][2] = int(currentthread[3]);
            velocity = int(currentthread[4]);
            _root.othergunfire[lastvar][6] = int(currentthread[5]);
            relativefacing = _root.othergunfire[lastvar][6];
            movementofanobjectwiththrust();
            _root.othergunfire[lastvar][3] = xmovement;
            _root.othergunfire[lastvar][4] = ymovement;
            _root.othergunfire[lastvar][7] = int(currentthread[6]);
            _root.othergunfire[lastvar][8] = _root.currentotherplayshot;
            _root.othergunfire[lastvar][9] = int(currentthread[7]);
            _root.othergunfire[lastvar][10] = "other";
            _root.othergunfire[lastvar][5] = _root.curenttime + _root.guntype[_root.othergunfire[lastvar][7]][1] * 1000;
            systemtime = String(getTimer() + _root.clocktimediff);
            systemtime = Number(systemtime.substr(systemtime.length - 4));
            jumpahead = systemtime - Number(currentthread[8]);
            if (jumpahead < -1000)
            {
                jumpahead = jumpahead + 10000;
            } // end if
            jumpahead = (jumpahead + _root.shipositiondelay) / 1000;
            _root.othergunfire[lastvar][1] = _root.othergunfire[lastvar][1] + _root.othergunfire[lastvar][3] * jumpahead;
            _root.othergunfire[lastvar][2] = _root.othergunfire[lastvar][2] + _root.othergunfire[lastvar][4] * jumpahead;
            _root.gamedisplayarea.attachMovie("guntype" + _root.othergunfire[lastvar][7] + "fire", "othergunfire" + _root.othergunfire[lastvar][8], 3500 + _root.othergunfire[lastvar][8]);
            setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _rotation, _root.othergunfire[lastvar][6]);
            setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _x, _root.othergunfire[lastvar][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _y, _root.othergunfire[lastvar][2] - _root.shipcoordinatey);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".xmovement", _root.othergunfire[lastvar][3]);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".ymovement", _root.othergunfire[lastvar][4]);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".xposition", _root.othergunfire[lastvar][1]);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".yposition", _root.othergunfire[lastvar][2]);
            _root.othergunfire[lastvar][11] = _root.gamedisplayarea["othergunfire" + _root.othergunfire[lastvar][8]]._width / 2;
            _root.othergunfire[lastvar][12] = _root.gamedisplayarea["othergunfire" + _root.othergunfire[lastvar][8]]._height / 2;
            if (_root.soundvolume != "off")
            {
                _root["guntype" + _root.othergunfire[lastvar][7] + "sound"].start();
            } // end if
        }
        else if (currentthread[0] == "GH")
        {
            func_statsshiphit(currentthread);
            playerwhogothit = currentthread[1];
            shooterofbullet = Number(currentthread[2]);
            shooternumberfire = Number(currentthread[3]);
            if (playerwhogothit == "SQ")
            {
                basethatgothit = String(currentthread[4]);
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othergunfire.length; cc++)
                    {
                        if (_root.othergunfire[cc][0] == shooterofbullet && _root.othergunfire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
                            gunshottype = _root.othergunfire[cc][7];
                            shotdamage = _root.guntype[gunshottype][4];
                            _root.othergunfire.splice(cc, 1);
                            cc = 2000;
                            _root.gamedisplayarea.gamebackground["playerbase" + basethatgothit].func_takeonothershots(shotdamage);
                        } // end if
                    } // end of for
                } // end if
            }
            else if (playerwhogothit == "TB")
            {
                basethatgothit = _root.teambases[Number(currentthread[4])][0];
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othergunfire.length; cc++)
                    {
                        if (_root.othergunfire[cc][0] == shooterofbullet && _root.othergunfire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
                            gunshottype = _root.othergunfire[cc][7];
                            shotdamage = _root.guntype[gunshottype][4];
                            _root.othergunfire.splice(cc, 1);
                            cc = 2000;
                            _root.gamedisplayarea.gamebackground["teambase" + basethatgothit].func_takeonothershots(shotdamage);
                        } // end if
                    } // end of for
                } // end if
            }
            else if (playerwhogothit == "MET")
            {
                meteor = String(currentthread[4]);
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othergunfire.length; cc++)
                    {
                        if (_root.othergunfire[cc][0] == shooterofbullet && _root.othergunfire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
                            gunshottype = _root.othergunfire[cc][7];
                            shotdamage = _root.guntype[gunshottype][4];
                            _root.othergunfire.splice(cc, 1);
                            cc = 2000;
                        } // end if
                    } // end of for
                } // end if
            }
            else
            {
                set("_root.gamedisplayarea.otherplayership" + playerwhogothit + ".gothit", true);
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othergunfire.length; cc++)
                    {
                        if (_root.othergunfire[cc][0] == shooterofbullet && _root.othergunfire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
                            _root.othergunfire.splice(cc, 1);
                            cc = 2000;
                        } // end if
                    } // end of for
                }
                else if (_root.playershipstatus[3][0] == shooterofbullet)
                {
                    removeMovieClip ("_root.gamedisplayarea.playergunfire" + shooternumberfire);
                } // end else if
            } // end else if
        }
        else if (currentthread[0] == "STATS")
        {
            func_statuscheck(currentthread);
        }
        else if (currentthread[0] == "NEUT")
        {
            func_neutralships(currentthread);
        }
        else if (currentthread[0] == "SP")
        {
            if (currentthread[1] == "MINEHIT")
            {
                mineid = currentthread[2];
                set("_root.gamedisplayarea.playermine" + mineid + ".justgothit", true);
            }
            else
            {
                playerid = currentthread[1];
                specialtype = Number(currentthread[2]);
                specialx = Number(currentthread[3]);
                specialy = Number(currentthread[4]);
                velocity = Number(currentthread[5]);
                rotation = Number(currentthread[6]);
                bringinspecial(playerid, specialtype, specialx, specialy, velocity, rotation);
            } // end else if
        }
        else if (currentthread[0] == "MH")
        {
            func_statsshiphit(currentthread);
            playerwhogothit = currentthread[1];
            shooterofbullet = currentthread[2];
            shooternumberfire = int(currentthread[3]);
            if (playerwhogothit == "SQ")
            {
                basethatgothit = String(currentthread[4]);
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othermissilefire.length; cc++)
                    {
                        if (_root.othermissilefire[cc][0] == shooterofbullet && _root.othermissilefire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[cc][8]);
                            missileshottype = _root.othermissilefire[cc][7];
                            shotdamage = _root.missile[missileshottype][4];
                            _root.othermissilefire.splice(cc, 1);
                            cc = 2000;
                            _root.gamedisplayarea.gamebackground["playerbase" + basethatgothit].func_takeonothershots(shotdamage);
                        } // end if
                        otherplayerdamage;
                    } // end of for
                } // end if
            }
            else if (playerwhogothit == "TB")
            {
                basethatgothit = _root.teambases[Number(currentthread[4])][0];
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othermissilefire.length; cc++)
                    {
                        if (_root.othermissilefire[cc][0] == shooterofbullet && _root.othermissilefire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[cc][8]);
                            missileshottype = _root.othermissilefire[cc][7];
                            shotdamage = _root.missile[missileshottype][4];
                            _root.othermissilefire.splice(cc, 1);
                            cc = 2000;
                            _root.gamedisplayarea.gamebackground["teambase" + basethatgothit].func_takeonothershots(shotdamage);
                        } // end if
                        otherplayerdamage;
                    } // end of for
                } // end if
            }
            else if (playerwhogothit == "MET")
            {
                basethatgothit = String(currentthread[4]);
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othermissilefire.length; cc++)
                    {
                        if (_root.othermissilefire[cc][0] == shooterofbullet && _root.othermissilefire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[cc][8]);
                            missileshottype = _root.othermissilefire[cc][7];
                            shotdamage = _root.missile[missileshottype][4];
                            _root.othermissilefire.splice(cc, 1);
                            cc = 2000;
                        } // end if
                        otherplayerdamage;
                    } // end of for
                } // end if
            }
            else
            {
                set("_root.gamedisplayarea.otherplayership" + playerwhogothit + ".gothit", true);
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othermissilefire.length; cc++)
                    {
                        if (_root.othermissilefire[cc][0] == shooterofbullet && _root.othermissilefire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[cc][8]);
                            _root.othermissilefire.splice(cc, 1);
                            cc = 2000;
                        } // end if
                    } // end of for
                } // end if
                if (_root.playershipstatus[3][0] == shooterofbullet)
                {
                    removeMovieClip ("_root.gamedisplayarea.playergunfire" + shooternumberfire);
                } // end if
                missilename = currentthread[2] + "n" + Number(currentthread[3]);
                removeMovieClip ("_root.gamedisplayarea.othermissilefire" + missilename);
            } // end else if
        }
        else if (currentthread[0] == "RG")
        {
            if (currentthread[1] == "NG")
            {
                temp = new Array();
                for (jt = 4; jt < currentthread.length; jt++)
                {
                    temp[jt - 4] = Number(currentthread[jt]);
                } // end of for
                _root.func_createracinggamepoints(temp, currentthread[2], currentthread[3]);
            }
            else if (currentthread[1] == "FG")
            {
                playerloc = Number(currentthread[2]);
                playerplaced = Number(currentthread[3]);
                _root.func_racinggamefinisher(playerloc, playerplaced);
            }
            else if (currentthread[1] == "EG")
            {
                _root.func_racinggamefinished();
            } // end else if
        }
        else if (currentthread[0] == "SQW")
        {
            if (currentthread[1] == "STARTING")
            {
                message = "HOST: A Squad War Between ";
                _root.squadwarinfo[0] = true;
                _root.squadwarinfo[2] = false;
                _root.squadwarinfo[1] = new Array();
                for (jt = 2; jt < currentthread.length; jt++)
                {
                    _root.squadwarinfo[1][_root.squadwarinfo[1].length] = currentthread[jt];
                    message = message + currentthread[jt];
                    if (jt != currentthread.length - 1)
                    {
                        message = message + " and ";
                    } // end if
                } // end of for
                message = message + " is about to begin!";
                _root.enterintochat(message, _root.systemchattextcolor);
                _root.gotoAndStop("teambase");
            }
            else if (currentthread[1] == "BEGIN")
            {
                _root.squadwarinfo[2] = true;
                message = "HOST: Squad War Has Now Started!";
                _root.enterintochat(message, _root.systemchattextcolor);
            }
            else if (currentthread[1] == "MODENDED")
            {
                _root.squadwarinfo[0] = false;
                _root.squadwarinfo[2] = false;
                message = "HOST: Squad War Was Ended By A Mod";
                _root.enterintochat(message, _root.systemchattextcolor);
                _root.playershipstatus[5][2] = "N/A";
                for (jt = 0; jt < _root.currentonlineplayers.length; jt++)
                {
                    _root.currentonlineplayers[jt][4] = "N/A";
                } // end of for
                _root.gotoAndStop("teambase");
            }
            else if (currentthread[1] == "INPROGRESS")
            {
                message = "HOST: A Squad War Between ";
                _root.squadwarinfo[0] = true;
                _root.squadwarinfo[2] = true;
                _root.squadwarinfo[1] = new Array();
                for (jt = 2; jt < currentthread.length; jt++)
                {
                    _root.squadwarinfo[1][_root.squadwarinfo[1].length] = currentthread[jt];
                    message = message + currentthread[jt];
                    if (jt != currentthread.length - 1)
                    {
                        message = message + " and ";
                    } // end if
                } // end of for
                message = message + " is already progress.";
                _root.enterintochat(message, _root.systemchattextcolor);
                _root.gotoAndStop("teambase");
            } // end else if
        }
        else if (currentthread[0] == "DM")
        {
            if (currentthread[1] == "BH")
            {
                _root.teambases[Number(currentthread[2])][10] = _root.teambases[Number(currentthread[2])][10] - Number(currentthread[3]);
                if (_root.teambases[Number(currentthread[2])][10] < 1)
                {
                    _root.teambases[Number(currentthread[2])][10] = 1;
                } // end if
            }
            else if (currentthread[1] == "NG")
            {
                for (jt = 2; jt < currentthread.length; jt++)
                {
                    _root.teambases[jt - 2][10] = Number(currentthread[jt]);
                    _root.teambases[jt - 2][11] = _root.teambasetypes[_root.teambases[jt - 2][3]][2];
                    if (_root.teambases[jt - 2][10] < 1)
                    {
                        _root.teambases[jt - 2][10] = 1;
                    } // end if
                } // end of for
            }
            else if (currentthread[1] == "EG")
            {
                if (_root.teamdeathmatch == true)
                {
                    basekillingplayer = Number(currentthread[3]);
                    basekilled = Number(currentthread[2]);
                    looseteamname = _root.teambases[basekilled][0].substr(2);
                    if (_root.squadwarinfo[0] == true)
                    {
                        looseteamname = _root.squadwarinfo[1][basekilled];
                    } // end if
                    _root.teambases[basekilled][10] = 0;
                    for (jt = 0; jt < _root.currentonlineplayers.length; jt++)
                    {
                        if (_root.currentonlineplayers[jt][0] == basekillingplayer)
                        {
                            winteamno = Number(_root.currentonlineplayers[jt][4]);
                            winplayname = _root.currentonlineplayers[jt][1];
                            winteamname = _root.teambases[winteamno][0].substr(2);
                            if (_root.squadwarinfo[0] == true)
                            {
                                winteamname = _root.squadwarinfo[1][winteamno];
                            } // end if
                        } // end if
                    } // end of for
                    rewardf = _root.deathfgamerewards();
                    colourtouse = _root.systemchattextcolor;
                    message = "HOST: Player " + winplayname + " of Team " + winteamname + " has destroyed " + looseteamname + "\'s Base";
                    _root.enterintochat(message, colourtouse);
                    message = "HOST: Reward for Team " + winteamname + " is " + _root.func_formatnumber(rewardf) + " Funds";
                    _root.enterintochat(message, colourtouse);
                    if (_root.playershipstatus[5][2] == winteamno)
                    {
                        _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(rewardf);
                    } // end if
                    _root.playershipstatus[5][2] = "N/A";
                    for (jt = 0; jt < _root.currentonlineplayers.length; jt++)
                    {
                        _root.currentonlineplayers[jt][4] = "N/A";
                    } // end of for
                    if (_root.squadwarinfo[0] == true)
                    {
                        _root.squadwarinfo[0] = false;
                        _root.squadwarinfo[2] = false;
                    } // end if
                    _root.gotoAndStop("teambase");
                } // end else if
            } // end else if
        }
        else if (currentthread[0] == "CH")
        {
            if (currentthread[2] == "M" || currentthread[2] == "PM" || currentthread[2] == "TM" || currentthread[2] == "SM" || currentthread[2] == "AM")
            {
                ignoringplayer = false;
                for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
                {
                    playerthatsaidit = currentthread[1];
                    if (currentthread[1] == "HOST")
                    {
                        message = "HOST: " + _root.func_checkothercharacters(currentthread[3]);
                        break;
                        continue;
                    } // end if
                    if (currentthread[1] == _root.currentonlineplayers[jjjj][0])
                    {
                        playerthatsaidit = _root.currentonlineplayers[jjjj][1];
                        if (currentthread[2] == "PM")
                        {
                            _root.gamechatinfo[5] = playerthatsaidit;
                        } // end if
                        if (currentthread[2] != "AM")
                        {
                            ignoringplayer = _root.func_ignoreplayer(playerthatsaidit);
                        }
                        else
                        {
                            ignoringplayer = false;
                        } // end else if
                        if (ignoringplayer != true)
                        {
                            message = playerthatsaidit + ": " + _root.func_checkothercharacters(currentthread[3]);
                        } // end if
                        break;
                    } // end if
                } // end of for
                if (ignoringplayer != true)
                {
                    if (currentthread[2] == "M")
                    {
                        colourtouse = _root.regularchattextcolor;
                    } // end if
                    if (currentthread[2] == "PM")
                    {
                        colourtouse = _root.privatechattextcolor;
                    } // end if
                    if (currentthread[2] == "TM")
                    {
                        colourtouse = _root.teamchattextcolor;
                    } // end if
                    if (currentthread[2] == "SM")
                    {
                        colourtouse = _root.squadchattextcolor;
                    } // end if
                    if (currentthread[2] == "AM")
                    {
                        colourtouse = _root.arenachattextcolor;
                    } // end if
                    _root.enterintochat(message, colourtouse);
                } // end if
            }
            else if (currentthread[2] == "WM")
            {
                message = "HOST: Your Base Has Been Attacked";
                colourtouse = _root.squadchattextcolor;
                _root.enterintochat(message, colourtouse);
            } // end else if
            _root.refreshchatdisplay = true;
        }
        else if (currentthread[0] == "ECC")
        {
            errorno = Number(currentthread[1]);
            errorcontrolchecking(errorno);
        }
        else if (currentthread[0] == "HC")
        {
            currentai = int(_root.aishipshosted.length);
            if (currentai < 1)
            {
                _root.aishipshosted = new Array();
                currentai = 0;
            } // end if
            _root.aishipshosted[currentai] = new Array();
            _root.aishipshosted[currentai][0] = Number(currentthread[1]);
            _root.aishipshosted[currentai][1] = Number(currentthread[2]);
            _root.aishipshosted[currentai][2] = Number(currentthread[3]);
            _root.aishipshosted[currentai][3] = Number(currentthread[4]);
            _root.aishipshosted[currentai][4] = Number(currentthread[5]);
            _root.aishipshosted[currentai][5] = "";
            _root.aishipshosted[currentai][6] = _root.aishipsinfo[_root.aishipshosted[currentai][0]][1];
            _root.aishipshosted[currentai][7] = _root.aishipsinfo[_root.aishipshosted[currentai][0]][0];
            _root.gamedisplayarea.attachMovie("shiptype" + _root.aishipshosted[currentai][6] + "ai", "aiship" + _root.aishipshosted[currentai][0], 11000 + _root.aishipshosted[currentai][0]);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _x, _root.aishipshosted[currentai][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _y, _root.aishipshosted[currentai][2] - _root.shipcoordinatey);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _rotation, _root.aishipshosted[currentai][3]);
            set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".shipid", _root.aishipshosted[currentai][0]);
        }
        else if (currentthread[0] == "AIIN")
        {
            currentai = int(_root.aishipshosted.length);
            if (currentai < 1)
            {
                _root.aishipshosted = new Array();
                currentai = 0;
            } // end if
            _root.aishipshosted[currentai] = new Array();
            _root.aishipshosted[currentai][0] = Number(currentthread[1]);
            _root.aishipshosted[currentai][1] = _root.shipcoordinatex + Math.random() * 750 - 340;
            _root.aishipshosted[currentai][2] = _root.shipcoordinatey + Math.random() * 750 - 340;
            _root.aishipshosted[currentai][3] = Math.random() * 360;
            _root.aishipshosted[currentai][4] = 0;
            _root.aishipshosted[currentai][5] = "";
            _root.aishipshosted[currentai][6] = _root.aishipsinfo[_root.aishipshosted[currentai][0]][1];
            _root.aishipshosted[currentai][7] = _root.aishipsinfo[_root.aishipshosted[currentai][0]][0];
            _root.gamedisplayarea.attachMovie("shiptype" + _root.aishipshosted[currentai][6] + "ai", "aiship" + _root.aishipshosted[currentai][0], 11000 + _root.aishipshosted[currentai][0]);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _x, _root.aishipshosted[currentai][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _y, _root.aishipshosted[currentai][2] - _root.shipcoordinatey);
            set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".shipid", _root.aishipshosted[currentai][0]);
        }
        else if (currentthread[0] == "TO")
        {
            currentplayerid = currentthread[1];
            for (jj = 0; jj < _root.currentonlineplayers.length; jj++)
            {
                if (currentplayerid == _root.currentonlineplayers[jj][0])
                {
                    leavingplayername = _root.currentonlineplayers[jj][1];
                    _root.func_playerleftass(leavingplayername);
                    _root.currentonlineplayers.splice(jj, 1);
                } // end if
            } // end of for
            _root.refreshonlinelist = true;
            _root.gamechatinfo[1].splice(0, 0, 0);
            _root.gamechatinfo[1][0] = new Array();
            _root.gamechatinfo[1][0][0] = "Host: " + leavingplayername + " has left";
            _root.gamechatinfo[1][0][1] = _root.regularchattextcolor;
            if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
            {
                _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
            } // end if
            _root.refreshchatdisplay = true;
        }
        else if (currentthread[0] == "TC")
        {
            currentplayerid = currentthread[1];
            if (currentthread[2] == "DK")
            {
                if (currentplayerid == _root.playershipstatus[3][0])
                {
                    _root.hasplayerbeenlistedasdocked = true;
                    _root.playersdockedtime = getTimer();
                }
                else
                {
                    jjjj = 0;
                    dataupdated = false;
                    while (jjjj < _root.otherplayership.length && dataupdated == false)
                    {
                        if (currentplayerid == _root.otherplayership[jjjj][0])
                        {
                            currentothership = jjjj;
                            _root.otherplayership[jjjj][15] = "dead";
                            dataupdated = true;
                            _root.gamedisplayarea.attachMovie("shiptypedock", "otherplayership" + _root.otherplayership[currentothership][0], 2300 + Number(_root.otherplayership[currentothership][0]));
                            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
                            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
                            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
                            _root.otherplayership[currentothership][6] = _root.curenttime + 1200;
                            _root.otherplayership[zz][15] = "docking";
                        } // end if
                        ++jjjj;
                    } // end while
                } // end if
            } // end else if
            if (currentthread[2] == "SC")
            {
                currentthread[3] = Number(currentthread[3]);
                if (isNaN(currentthread[3]))
                {
                    currentthread[3] = 0;
                } // end if
                jj = 0;
                dataupdated = false;
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (currentplayerid == _root.currentonlineplayers[jj][0])
                    {
                        _root.currentonlineplayers[jj][2] = Number(currentthread[3]);
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
            } // end if
            if (currentthread[2] == "TM")
            {
                jj = 0;
                dataupdated = false;
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (currentplayerid == _root.currentonlineplayers[jj][0])
                    {
                        _root.currentonlineplayers[jj][4] = currentthread[3];
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                if (_root.playershipstatus[3][0] == currentthread[1])
                {
                    _root.playershipstatus[5][2] = currentthread[3];
                    _root.toprightinformation.playerteam = "Team: " + _root.playershipstatus[5][2];
                } // end if
            } // end if
            if (currentthread[2] == "SCORE")
            {
                currentthread[3] = Number(currentthread[3]);
                if (isNaN(currentthread[3]) || Number(currentthread[3] < 0))
                {
                    currentthread[3] = 0;
                } // end if
                jj = 0;
                dataupdated = false;
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (currentplayerid == _root.currentonlineplayers[jj][0])
                    {
                        _root.currentonlineplayers[jj][5] = Number(_root.currentonlineplayers[jj][5]) + Number(currentthread[3]);
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                if (_root.playershipstatus[3][0] == currentthread[1])
                {
                    _root.playershipstatus[5][9] = Number(_root.playershipstatus[5][9]) + Number(currentthread[3]);
                    _root.toprightinformation.playerscore = "Score: " + Math.floor(Number(_root.playershipstatus[5][9]) / _root.scoreratiomodifier);
                } // end if
            } // end if
            if (currentthread[2] == "SD")
            {
                if (String(currentthread[1]) == String(999999))
                {
                    aiwaskilled = true;
                }
                else
                {
                    aiwaskilled = false;
                } // end else if
                jjjj = 0;
                dataupdated = false;
                while (jjjj < _root.otherplayership.length && dataupdated == false)
                {
                    if (currentplayerid == _root.otherplayership[jjjj][0])
                    {
                        currentothership = jjjj;
                        _root.otherplayership[jjjj][15] = "dead";
                        dataupdated = true;
                        _root.gamedisplayarea.attachMovie("shiptypedead", "otherplayership" + _root.otherplayership[currentothership][0], 2300 + Number(_root.otherplayership[currentothership][0]));
                        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
                        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
                        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
                        _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
                        _root.otherplayership[zz][15] = "dieing";
                    } // end if
                    ++jjjj;
                } // end while
                if (Number(currentthread[4]) < 0 || Number(currentthread[4]) > 10000000)
                {
                    currentthread[4] = 0;
                } // end if
                extrafundsmultiplier = 10;
                fundsworth = Number(currentthread[4]) * extrafundsmultiplier;
                if (_root.playershipstatus[3][0] == currentthread[3])
                {
                    playerkilled = true;
                    _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + fundsworth;
                    _root.playershipstatus[5][8] = Number(_root.playershipstatus[5][8]) + Math.round(Number(currentthread[4]) * _root.playershipstatus[5][6]);
                    if (aiwaskilled != true)
                    {
                        _root.playershipstatus[5][9] = Number(_root.playershipstatus[5][9]) + Number(currentthread[4]);
                    } // end if
                    _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
                    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
                    if (_root.playertargetrange > Number(currentthread[4]) && currentplayerid != "999999" && _root.playershipstatus[3][0] != currentthread[1])
                    {
                        scoreloss = Math.round((Number(_root.playershipstatus[5][3]) - Number(currentthread[4])) * _root.scoreratiomodifier / 3);
                        bountytoadd = (Number(_root.playershipstatus[5][3]) - Number(currentthread[4])) * 3;
                        if (bountytoadd > 10000)
                        {
                            bountytoadd = 10000;
                        } // end if
                        if (bountytoadd > 0)
                        {
                            message = "NEWS`NBASH``" + _root.playershipstatus[3][0] + "`" + currentthread[1] + "`" + "0" + "`" + bountytoadd + "`" + "~";
                            _root.mysocket.send(message);
                        } // end if
                    } // end if
                }
                else
                {
                    playerkilled = false;
                } // end else if
                jj = 0;
                dataupdated = false;
                killerid = String(currentthread[3]);
                killerrealname = "AI";
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (killerid == String(_root.currentonlineplayers[jj][0]))
                    {
                        _root.currentonlineplayers[jj][3] = Number(_root.currentonlineplayers[jj][3]) + Math.round(Number(currentthread[4]) * _root.playershipstatus[5][6]);
                        if (aiwaskilled != true)
                        {
                            _root.currentonlineplayers[jj][5] = _root.currentonlineplayers[jj][5] + Number(currentthread[4]);
                        } // end if
                        killerrealname = _root.currentonlineplayers[jj][1];
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                jjjj = 0;
                dataupdated = false;
                while (jjjj < _root.otherplayership.length && dataupdated == false)
                {
                    if (killerid == String(_root.otherplayership[jjjj][0]))
                    {
                        _root.otherplayership[jjjj][12] = _root.currentonlineplayers[jj - 1][3];
                        dataupdated = true;
                        set("_root.gamedisplayarea.nametag" + _root.otherplayership[jjjj][0] + ".nametag", "" + _root.otherplayership[jjjj][10] + " (" + _root.otherplayership[jjjj][12] + ")");
                    } // end if
                    ++jjjj;
                } // end while
                jj = 0;
                dataupdated = false;
                killedid = String(currentthread[1]);
                deadguysname = "AI";
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (killedid == String(_root.currentonlineplayers[jj][0]))
                    {
                        _root.currentonlineplayers[jj][3] = 0;
                        deadguysname = _root.currentonlineplayers[jj][1];
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                message = "HOST: " + deadguysname + " (" + currentthread[4] + " BTY, " + fundsworth + " Funds) killed by " + killerrealname;
                _root.enterintochat(message, _root.regularchattextcolor);
                _root.refreshonlinelist = true;
                if (playerkilled == true)
                {
                    _root.func_playerkilled(deadguysname);
                } // end if
            } // end if
            if (currentthread[2] == "BC")
            {
                jj = 0;
                dataupdated = false;
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (currentplayerid == _root.currentonlineplayers[jj][0])
                    {
                        _root.currentonlineplayers[jj][3] = Number(currentthread[3]);
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                jjjj = 0;
                dataupdated = false;
                while (jjjj < _root.otherplayership.length && dataupdated == false)
                {
                    if (currentplayerid == _root.otherplayership[jjjj][0])
                    {
                        _root.otherplayership[jjjj][12] = Number(currentthread[3]);
                        set("_root.gamedisplayarea.nametag" + _root.otherplayership[jjjj][0] + ".nametag", "" + _root.otherplayership[jjjj][10] + " (" + _root.otherplayership[jjjj][12] + ")");
                        dataupdated = true;
                    } // end if
                    ++jjjj;
                } // end while
                if (_root.playershipstatus[3][0] == currentplayerid)
                {
                    _root.playershipstatus[5][8] = Number(currentthread[3]) - Number(_root.playershipstatus[5][3]);
                } // end if
            } // end if
            _root.refreshonlinelist = true;
            _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
            _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
            _root.toprightinformation.playerscore = "Score: " + Math.floor(Number(_root.playershipstatus[5][9]) / _root.scoreratiomodifier);
        }
        else if (currentthread[0] == "PING")
        {
            currentthread = newinfo[i].split("`");
            _root.gameplaystatus[1][0] = getTimer() - Number(currentthread[1]);
            _root.pinginformation = "Ping: " + _root.gameplaystatus[1][0] + "ms Current";
        }
        else if (currentthread[0] == "TI" && currentthread[1] != _root.playershipstatus[3][0])
        {
            currentthread = newinfo[i].split("`");
            idlocation = currentthread[1];
            realname = String(currentthread[2]);
            replacedaspot = false;
            for (qq = 0; qq < _root.currentonlineplayers.length; qq++)
            {
                if (String(_root.currentonlineplayers[qq][0]) == String(idlocation) || String(_root.currentonlineplayers[qq][1]) == String(realname))
                {
                    _root.currentonlineplayers.splice(qq, 1);
                    --qq;
                } // end if
            } // end of for
            lastspot = _root.currentonlineplayers.length;
            _root.currentonlineplayers[lastspot] = new Array();
            _root.currentonlineplayers[lastspot][0] = currentthread[1];
            _root.currentonlineplayers[lastspot][1] = currentthread[2];
            _root.currentonlineplayers[lastspot][2] = currentthread[3];
            _root.currentonlineplayers[lastspot][3] = currentthread[4];
            _root.currentonlineplayers[lastspot][4] = currentthread[5];
            _root.currentonlineplayers[lastspot][5] = Number(currentthread[6]);
            if (currentthread[2] == _root.playershipstatus[3][2] && _root.playershipstatus[3][2] == _root.playershipstatus[3][0])
            {
                _root.playershipstatus[3][0] = currentthread[1];
            } // end if
            _root.refreshonlinelist = true;
            if (_root.playershipstatus[3][0] != _root.playershipstatus[3][2])
            {
                _root.gamechatinfo[1].splice(0, 0, 0);
                _root.gamechatinfo[1][0] = new Array();
                _root.gamechatinfo[1][0][0] = "Host: " + currentthread[2] + " has entered";
                _root.gamechatinfo[1][0][1] = _root.regularchattextcolor;
                if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
                {
                    _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
                } // end if
                _root.refreshchatdisplay = true;
            } // end else if
        } // end else if
        if (currentthread[0] == "LI")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[3][0] = currentthread[1];
            _root.playershipstatus[5][19] = currentthread[2];
            if (_root.playershipstatus[5][25] == "NO" && _root.playerscurrentextrashipno == "capital")
            {
                for (qqt = 0; qqt < _root.maxextraships; qqt++)
                {
                    if (_root.extraplayerships[qqt][0] != null)
                    {
                        _root.playerscurrentextrashipno = qqt;
                        break;
                    } // end if
                } // end of for
                _root.changetonewship(_root.playerscurrentextrashipno);
                _root.initializemissilebanks();
                _root.savegamescript.saveplayersgame(this);
                _root.playershipstatus[5][25] = "CAPLEFT";
            } // end if
            _root.currentgamestatus = _root.currentgamestatus + "Synchronizing..";
            _root.attachMovie("gametimersync", "gametimersync", 540);
            continue;
        } // end if
        if (currentthread[0] == "CLOCK")
        {
            if (clocktimes.length <= clockchecks)
            {
                timedifference = Number(currentthread[1]);
                clocktimes[clocktimes.length] = timedifference;
                _root.currentgamestatus = _root.currentgamestatus + (_root.clockchecks - _root.clocktimes.length + ".");
                if (clocktimes.length == clockchecks)
                {
                    this.gametimersync.removeMovieClip();
                    _root.currentgamestatus = _root.currentgamestatus + "\r";
                    totaltime = 0;
                    for (q = 0; q < clocktimes.length; q++)
                    {
                        totaltime = totaltime + clocktimes[q];
                    } // end of for
                    _root.clocktimediff = Math.round(totaltime / clocktimes.length);
                    if (_root.jumpinginfo.length > 1)
                    {
                        message = "Jumping System Complete, Now in system " + _root.jumpinginfo[0];
                        _root.enterintochat(message, _root.systemchattextcolor);
                        _root.jumpinginfo.length = null;
                    } // end if
                    if (_root.teamdeathmatch == true)
                    {
                        _root.gotoAndStop("teambase");
                    }
                    else if (_root.isgameracingzone == true)
                    {
                        _root.gotoAndStop("racingzonescreen");
                    }
                    else if (_root.playershipstatus[4][0].substr(0, 2) == "SB")
                    {
                        _root.gotoAndStop("stardock");
                    }
                    else if (_root.playershipstatus[4][0].substr(0, 2) == "PL")
                    {
                        _root.gotoAndStop("planet");
                    } // end else if
                } // end else if
            } // end else if
            continue;
        } // end if
        if (currentthread[0] == "NEWS")
        {
            if (currentthread[1] == "BOUNTY")
            {
                playerwhosgettingbty = Number(currentthread[2]);
                amountofbty = Number(currentthread[3]);
                playerwhoboughtbty = currentthread[4];
                gettingtruename = currentthread[5];
                if (isNaN(amountofbty))
                {
                    amountofbty = 0;
                } // end if
                if (_root.playershipstatus[3][0] == playerwhosgettingbty)
                {
                    _root.playershipstatus[5][8] = _root.playershipstatus[5][8] + amountofbty;
                    if (_root.isplayeraguest == false)
                    {
                        newaccountVars = new XML();
                        newaccountVars.load(_root.pathtoaccounts + "accounts.php?name=" + _root.playershipstatus[3][2] + "&pass=" + _root.playershipstatus[3][3] + "&bty=" + _root.playershipstatus[5][8]);
                        newaccountVars.onLoad = function (success)
                        {
                        };
                    } // end if
                    datatosend = "TC`" + _root.playershipstatus[3][0] + "`BC`" + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8])) + "~";
                    _root.mysocket.send(datatosend);
                } // end if
                message = "Player " + playerwhoboughtbty + " has placed an additional bounty of " + amountofbty + " on player " + gettingtruename;
                _root.func_messangercom(message, "NEWS", "BEGIN");
            } // end if
            if (currentthread[1] == "MES")
            {
                message = currentthread[2];
                message = currentthread[2];
                _root.func_messangercom(message, "NEWS", "BEGIN");
            } // end if
            if (currentthread[1] == "SB")
            {
                if (currentthread[2] == "DESTROYED")
                {
                    baseidname = currentthread[3];
                    for (jj = 0; jj < _root.playersquadbases.length; jj++)
                    {
                        if (_root.playersquadbases[jj][0] == baseidname)
                        {
                            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]].basedestroyed = true;
                            _root.playersquadbases.splice(jj, 1);
                        } // end if
                    } // end of for
                } // end if
                if (currentthread[2] == "CREATED")
                {
                    if (_root.playersquadbases.length < 1)
                    {
                        _root.playersquadbases = new Array();
                    } // end if
                    currentsquadbase = _root.playersquadbases.length;
                    _root.playersquadbases[currentsquadbase] = new Array();
                    _root.playersquadbases[currentsquadbase][0] = currentthread[3];
                    _root.playersquadbases[currentsquadbase][1] = Number(currentthread[4]);
                    _root.playersquadbases[currentsquadbase][2] = Number(currentthread[5]);
                    _root.playersquadbases[currentsquadbase][3] = Number(currentthread[6]);
                    _root.playersquadbases[currentsquadbase][4] = Math.ceil(Number(currentthread[5]) / _root.sectorinformation[1][0]);
                    _root.playersquadbases[currentsquadbase][5] = Math.ceil(Number(currentthread[6]) / _root.sectorinformation[1][1]);
                    if (_root.playersquadbases[currentsquadbase][0].substr(0, 3) == "*AI")
                    {
                        message = "An AI Base has been detected at X:" + _root.playersquadbases[currentsquadbase][4] + " Y:" + _root.playersquadbases[currentsquadbase][5];
                        message = message + "\rA Bounty of 65,000 has been placed on its elimination";
                        ainame = "NEWS";
                        _root.func_messangercom(message, "NEWS", "BEGIN");
                    } // end if
                    _root.testtt = _root.playersquadbases.length;
                } // end if
            } // end if
            if (currentthread[1] == "SQUADBD")
            {
                killersid = Number(currentthread[3]);
                bountytoadd = Number(currentthread[4]);
                baseidname = String(currentthread[2]);
                basedestroyed(killersid, bountytoadd, baseidname);
            } // end if
            if (currentthread[1] == "SQUADUPG")
            {
                baseid = String(currentthread[2]);
                mode = String(currentthread[3]);
                if (mode == "baselvl")
                {
                    newbaselevel = Number(currentthread[4]);
                    for (jj = 0; jj < _root.playersquadbases.length; jj++)
                    {
                        if (baseid == _root.playersquadbases[jj][0])
                        {
                            _root.playersquadbases[jj][1] = newbaselevel;
                            break;
                        } // end if
                    } // end of for
                    _root.gamedisplayarea.keyboardscript.func_redrawsquadbase(baseid, "LEVEL");
                } // end if
            } // end if
            if (currentthread[1] == "TRANSFER")
            {
                fundstoadd = Number(currentthread[3]);
                donater = Number(currentthread[4]);
                donatersname = null;
                for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
                {
                    if (donater == _root.currentonlineplayers[jjjj][0])
                    {
                        donatersname = _root.currentonlineplayers[jjjj][1];
                    } // end if
                } // end of for
                if (donatersname != null)
                {
                    if (fundstoadd > 5000000)
                    {
                        fundstoadd = 5000000;
                    } // end if
                    if (fundstoadd < 0)
                    {
                        fundstoadd = 0;
                    } // end if
                    if (!isNaN(fundstoadd))
                    {
                        _root.playershipstatus[3][1] = Number(fundstoadd) + Number(_root.playershipstatus[3][1]);
                        _root.func_setoldfundsuptocurrentammount();
                        _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
                        message = "You Just Received " + fundstoadd + " funds from Player " + donatersname;
                        ainame = "NEWS";
                        _root.func_messangercom(message, "NEWS", "BEGIN");
                    } // end if
                } // end if
            } // end if
            if (currentthread[1] == "NBASH")
            {
                attacker = currentthread[3];
                bashed = currentthread[4];
                scoreloss = currentthread[5];
                btyincrease = currentthread[6];
                location = currentthread[7];
                for (jj = 0; jj < _root.currentonlineplayers.length; jj++)
                {
                    if (attacker == _root.currentonlineplayers[jj][0])
                    {
                        attackersname = _root.currentonlineplayers[jj][1];
                        _root.currentonlineplayers[jj][3] = _root.currentonlineplayers[jj][3] + Number(btyincrease);
                        _root.currentonlineplayers[jj][5] = _root.currentonlineplayers[jj][5] - Number(scoreloss);
                        dataupdated = true;
                    } // end if
                    if (bashed == _root.currentonlineplayers[jj][0])
                    {
                        bashedname = _root.currentonlineplayers[jj][1];
                    } // end if
                } // end of for
                message = attackersname + " has just bashed " + bashedname + " at Coordinates " + location + "\r" + attackersname + " has lost " + Math.round(scoreloss / _root.scoreratiomodifier) + " points and has gained a bounty increase of " + btyincrease;
                _root.func_messangercom(message, "NEWS", "BEGIN");
                if (_root.playershipstatus[3][0] == currentthread[3])
                {
                    _root.playershipstatus[5][8] = Number(_root.playershipstatus[5][8]) + Number(btyincrease);
                    _root.playershipstatus[5][9] = Number(_root.playershipstatus[5][9]) - Number(scoreloss);
                    _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
                    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
                } // end if
                _root.refreshonlinelist = true;
            } // end if
            if (currentthread[1] == "PC")
            {
                _root.randomegameevents.func_pricechangeeffect(currentthread[2], currentthread[3], currentthread[4], currentthread[5], currentthread[6]);
            } // end if
            continue;
        } // end if
        if (currentthread[0] == "KFLAG")
        {
            _root.func_kingofflagcommand(currentthread[1], currentthread[2], currentthread[3], currentthread[4]);
            continue;
        } // end if
        if (currentthread[0] == "ADMIN")
        {
            if (currentthread[1] == "MUTE")
            {
                if (_root.gamechatinfo[2][2] == true)
                {
                    _root.gamechatinfo[2][2] = false;
                    colourtouse = _root.systemchattextcolor;
                    message = "HOST: You Are No Longer Muted";
                    _root.enterintochat(message, colourtouse);
                }
                else
                {
                    _root.gamechatinfo[2][2] = true;
                    _root.gamechatinfo[2][3] = getTimer() + _root.gamechatinfo[2][4];
                    colourtouse = _root.systemchattextcolor;
                    message = "HOST: Muted for " + Math.ceil(_root.gamechatinfo[2][4] / 60000) + " minuites by " + currentthread[2];
                    _root.enterintochat(message, colourtouse);
                } // end if
            } // end else if
            if (currentthread[1] == "TRANSFER")
            {
                fundstoadd = Number(currentthread[3]);
                donater = Number(currentthread[4]);
                donatersname = "*AN OP*";
                if (!isNaN(fundstoadd))
                {
                    _root.playershipstatus[3][1] = Number(fundstoadd) + Number(_root.playershipstatus[3][1]);
                    _root.func_setoldfundsuptocurrentammount();
                    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
                    message = "You Just Received " + fundstoadd + " funds from Player " + donatersname;
                    ainame = "NEWS";
                    _root.func_messangercom(message, "NEWS", "BEGIN");
                    _root.savegamescript.saveplayersgame(this);
                } // end if
            } // end if
            continue;
        } // end if
        if (currentthread[0] == "ERROR")
        {
            if (currentthread[1] == "TOOMANYUSERS")
            {
                _root.gameerror = "toomanyplayers";
                _root.controlledserverclose = true;
                _root.gotoAndStop("gameclose");
            } // end if
            if (currentthread[1] == "KICKED")
            {
                _root.controlledserverclose = true;
                _root.gameerror = "kicked";
                _root.gameerrordoneby = currentthread[2];
                _root.func_closegameserver();
            } // end if
            if (currentthread[1] == "SHUTDOWN")
            {
                _root.controlledserverclose = true;
                _root.gameerror = "servershutdown";
                _root.func_closegameserver();
            } // end if
            if (currentthread[1] == "NAMEINUSE")
            {
                _root.controlledserverclose = true;
                _root.gameerror = "nameinuse";
                _root.func_closegameserver();
            } // end if
            if (currentthread[1] == "BANNED")
            {
                _root.controlledserverclose = true;
                _root.gameerror = "banned";
                _root.timebannedfor = currentthread[2];
                _root.gameerrordoneby = currentthread[3];
                _root.func_closegameserver();
            } // end if
            continue;
        } // end if
        if (currentthread[0] == "MET")
        {
            if (currentthread[1] == "CR")
            {
                meteorid = currentthread[2];
                meteorheading = currentthread[3];
                targetname = currentthread[4];
                meteortype = currentthread[5];
                meterorlife = currentthread[6];
                timeelapsed = currentthread[7];
                _root.func_meteoricoming(meteorid, meteorheading, targetname, meteortype, meterorlife, timeelapsed);
            }
            else if (currentthread[1] == "LF")
            {
                meteorid = currentthread[2];
                meteorlife = currentthread[3];
                _root.func_meteorlife(meteorid, meteorlife);
            } // end else if
            if (currentthread[1] == "DEST")
            {
                meteorid = currentthread[2];
                killer = currentthread[3];
                _root.func_meteordestroyer(meteorid, killer);
            } // end if
        } // end if
    } // end of for
} // End of the function
function movementofanobjectwiththrust()
{
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    } // end if
    if (velocity != 0)
    {
        ymovement = -velocity * Math.cos(0.017453 * this.relativefacing);
        xmovement = velocity * Math.sin(0.017453 * this.relativefacing);
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function func_messangercom(message, nameofsender, transmissiontype)
{
    message = _root.func_checkothercharacters(message);
    _root.aimessagesbox.gotoAndPlay(2);
    _root.aimessagesbox.aimessage = message;
    _root.aimessagesbox.ainame = nameofsender;
} // End of the function
function basedestroyed(killersid, bountytoadd, baseidname)
{
    _root.gamedisplayarea.keyboardscript.func_setsectortargetitem("REMOVE", baseidname);
    for (jj = 0; jj < _root.playersquadbases.length; jj++)
    {
        if (_root.playersquadbases[jj][0] == baseidname)
        {
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]].basedestroyed = true;
            _root.playersquadbases.splice(jj, 1);
        } // end if
    } // end of for
    if (Number(currentthread[4]) < 0)
    {
        currentthread[4] = 0;
    } // end if
    if (_root.playershipstatus[3][0] == killersid)
    {
        _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(bountytoadd);
        _root.playershipstatus[5][8] = Number(_root.playershipstatus[5][8]) + Math.round(Number(bountytoadd) * _root.playershipstatus[5][6]);
        _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
        _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
    } // end if
    jj = 0;
    dataupdated = false;
    killerid = String(killersid);
    while (jj < _root.currentonlineplayers.length && dataupdated == false)
    {
        if (killerid == String(_root.currentonlineplayers[jj][0]))
        {
            _root.currentonlineplayers[jj][3] = Number(_root.currentonlineplayers[jj][3]) + Math.round(Number(bountytoadd) * _root.playershipstatus[5][6]);
            killerrealname = _root.currentonlineplayers[jj][1];
            dataupdated = true;
        } // end if
        ++jj;
    } // end while
    jjjj = 0;
    dataupdated = false;
    while (jjjj < _root.otherplayership.length && dataupdated == false)
    {
        if (killerid == String(_root.otherplayership[jjjj][0]))
        {
            _root.otherplayership[jjjj][12] = _root.currentonlineplayers[jj - 1][3];
            dataupdated = true;
            set("_root.gamedisplayarea.nametag" + _root.otherplayership[jjjj][0] + ".nametag", "" + _root.otherplayership[jjjj][10] + " (" + _root.otherplayership[jjjj][12] + ")");
        } // end if
        ++jjjj;
    } // end while
    _root.refreshonlinelist = true;
    message = "Player " + killerrealname + " Just Killed Squad Base " + baseidname + ".";
    _root.gamechatinfo[1].splice(0, 0, 0);
    _root.gamechatinfo[1][0] = new Array();
    _root.gamechatinfo[1][0][0] = "HOST: " + message;
    _root.gamechatinfo[1][0][1] = _root.systemchattextcolor;
    if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
    {
        _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
    } // end if
    _root.refreshchatdisplay = true;
} // End of the function
function bringinspecial(playerid, specialno, specialx, specialy, velocity, rotation)
{
    typeofspecial = _root.specialshipitems[specialno][5];
    if (typeofspecial == "FLARE")
    {
        i = _root.currentplayershotsfired;
        ++_root.currentplayershotsfired;
        if (_root.currentplayershotsfired > 999)
        {
            _root.currentplayershotsfired = 0;
        } // end if
        _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialno][8], "playerflare" + i, 5000 + i);
        setProperty("_root.gamedisplayarea.playerflare" + i, _x, specialx);
        setProperty("_root.gamedisplayarea.playerflare" + i, _y, specialy);
        set("_root.gamedisplayarea.playerflare" + i + ".xposition", specialx);
        set("_root.gamedisplayarea.playerflare" + i + ".yposition", specialy);
        set("_root.gamedisplayarea.playerflare" + i + ".removetime", getTimer() + _root.specialshipitems[specialno][3]);
    }
    else if (typeofspecial == "PULSAR")
    {
        i = _root.currentplayershotsfired;
        ++_root.currentplayershotsfired;
        if (_root.currentplayershotsfired > 999)
        {
            _root.currentplayershotsfired = 0;
        } // end if
        _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialno][8], "selfdest" + i, 5000 + i);
        setProperty("_root.gamedisplayarea.selfdest" + i, _x, specialx);
        setProperty("_root.gamedisplayarea.selfdest" + i, _y, specialy);
        set("_root.gamedisplayarea.selfdest" + i + ".xposition", specialx);
        set("_root.gamedisplayarea.selfdest" + i + ".yposition", specialy);
        set("_root.gamedisplayarea.selfdest" + i + ".totaltime", _root.specialshipitems[specialno][3]);
        set("_root.gamedisplayarea.selfdest" + i + ".finishsize", _root.specialshipitems[specialno][12]);
        set("_root.gamedisplayarea.selfdest" + i + ".damage", _root.specialshipitems[specialno][11]);
        set("_root.gamedisplayarea.selfdest" + i + ".playerid", playerid);
    }
    else if (typeofspecial == "RECHARGESHIELD")
    {
        pilotno = playerid;
        shieldcharge = specialx;
        currentthread = new Array(null, "RECHARGESHIELD", pilotno, shieldcharge);
        func_statuscheck(currentthread);
    }
    else if (typeofspecial == "RECHARGESTRUCT")
    {
        pilotno = playerid;
        structcharge = specialx;
        currentthread = new Array(null, "RECHARGESTRUCT", pilotno, structcharge);
        func_statuscheck(currentthread);
    }
    else if (typeofspecial == "MINES")
    {
        mineid = playerid;
        minesplitinfo = playerid.split("a");
        playerid = Number(minesplitinfo[0]);
        i = _root.currentplayershotsfired;
        ++_root.currentplayershotsfired;
        if (_root.currentplayershotsfired > 999)
        {
            _root.currentplayershotsfired = 0;
        } // end if
        _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialno][8], "playermine" + mineid, 5000 + i);
        setProperty("_root.gamedisplayarea.playermine" + mineid, _x, specialx);
        setProperty("_root.gamedisplayarea.playermine" + mineid, _y, specialy);
        set("_root.gamedisplayarea.playermine" + mineid + ".mineid", mineid);
        set("_root.gamedisplayarea.playermine" + mineid + ".xposition", specialx);
        set("_root.gamedisplayarea.playermine" + mineid + ".yposition", specialy);
        set("_root.gamedisplayarea.playermine" + mineid + ".removetime", getTimer() + _root.specialshipitems[specialno][3]);
        set("_root.gamedisplayarea.playermine" + mineid + ".damage", _root.specialshipitems[specialno][12]);
        set("_root.gamedisplayarea.playermine" + mineid + ".playerfrom", playerid);
        ishostilemine = func_isotherplayeroneyourteam(playerid);
        set("_root.gamedisplayarea.playermine" + mineid + ".ishostilemine", ishostilemine);
    } // end else if
} // End of the function
function func_isotherplayeroneyourteam(otherplayerid)
{
    yteam = _root.playershipstatus[5][2];
    if (yteam == "N/A")
    {
        return (true);
    }
    else
    {
        totalplayers = _root.currentonlineplayers.length;
        for (q = 0; q < totalplayers; q++)
        {
            if (_root.currentonlineplayers[q][0] == otherplayerid)
            {
                opponenteam = _root.currentonlineplayers[q][4];
                break;
            } // end if
        } // end of for
        if (opponenteam != yteam)
        {
            return (true);
        }
        else
        {
            return (false);
        } // end else if
    } // end else if
} // End of the function
function func_statuscheck(currentthread)
{
    if (currentthread[1] == "GET")
    {
        pilotno = currentthread[2];
        datatosend = "SP`" + pilotno + "~STATS`INFO`" + _root.playershipstatus[3][0] + "`" + _root.playershipstatus[2][0] + "`" + Math.round(_root.playershipstatus[2][1]) + "`" + Math.round(_root.playershipstatus[2][5]) + "~";
        _root.mysocket.send(datatosend);
    }
    else if (currentthread[1] == "INFO")
    {
        currentpilot = Number(currentthread[2]);
        for (jj = 0; jj < _root.otherplayership.length; jj++)
        {
            if (_root.otherplayership[jj][0] == currentpilot)
            {
                _root.otherplayership[jj][40] = Number(currentthread[5]);
                _root.otherplayership[jj][41] = Number(currentthread[4]);
                _root.otherplayership[jj][46] = Number(currentthread[3]);
                _root.otherplayership[jj][42] = _root.shieldgenerators[_root.otherplayership[jj][46]][1];
                _root.otherplayership[jj][43] = _root.shieldgenerators[_root.otherplayership[jj][46]][0];
                _root.otherplayership[jj][44] = getTimer();
            } // end if
        } // end of for
    }
    else if (currentthread[1] == "RECHARGESHIELD")
    {
        currentpilot = Number(currentthread[2]);
        for (jj = 0; jj < _root.otherplayership.length; jj++)
        {
            if (_root.otherplayership[jj][0] == currentpilot)
            {
                _root.otherplayership[jj][41] = _root.otherplayership[jj][41] + Number(currentthread[3]);
            } // end if
        } // end of for
    }
    else if (currentthread[1] == "RECHARGESTRUCT")
    {
        _root.testtt = currentpilot + "`" + currentthread[4];
        currentpilot = Number(currentthread[2]);
        for (jj = 0; jj < _root.otherplayership.length; jj++)
        {
            if (_root.otherplayership[jj][0] == currentpilot)
            {
                _root.otherplayership[jj][40] = _root.otherplayership[jj][40] + Number(currentthread[3]);
            } // end if
        } // end of for
    } // end else if
} // End of the function
framestobuffer = 4;
_root.othershipbuffer = framestobuffer;
shiptimeoutime = 3500;
timeintervalcheck = getTimer();
pathtosectorfile = "./systems/system" + _root.playershipstatus[5][1] + "/";
_root.otherplayership = new Array();
_root.currentotherplayshot = 0;
currentothermissileshot = 0;
justenteredgame = true;
clocktimes = new Array();
_root.clocktimediff = 0;
clockchecks = 10;
if (_root.isgamerunningfromremote == true)
{
    clockchecks = 2;
} // end if
this.gameareawidth = _root.gameareawidth;
this.gameareaheight = _root.gameareaheight;
lowestvolume = 50;
lowestvolumelength = Math.sqrt(gameareawidth / 2 * (gameareawidth / 2) + gameareaheight / 2 * (gameareaheight / 2));
_root.mysocket = new XMLSocket();
_root.currentgamestatus = _root.currentgamestatus + "Connecting to Game Server \r";
_root.currentgamestatus = _root.currentgamestatus + "Logging In \r";
if (_root.playershipstatus[3][2] == "Enter Name")
{
    _root.gotoAndStop("loginscreen");
}
else if (_root.isgamerunningfromremote == false)
{
    currenturl = String(_root._url);
    if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
    {
        portouse = 2 + String(_root.portandsystem);
        _root.mysocket.connect("", portouse);
    }
    else if (currenturl.substr(0, 32) == "http://www.stellar-conflicts.com")
    {
        portouse = 2 + String(_root.portandsystem);
        _root.mysocket.connect("", portouse);
    } // end else if
}
else
{
    _root.mysocket.connect("217.160.243.182", 2500);
} // end else if
_root.mysocket.onConnect = function (success)
{
    if (success)
    {
        _root.playerjustloggedin = false;
        login();
    }
    else
    {
        _root.status = "Failed";
        _root.gameerror = "failedtologin";
        _root.gotoAndPlay("gameclose");
    } // end else if
};
_root.mysocket.onClose = function (success)
{
    _root.errorcheckdata = new Array();
    if (_root.jumpinginfo.length > 1)
    {
    }
    else if (_root.controlledserverclose == true)
    {
        _root.gotoAndStop("gameclose");
    }
    else
    {
        _root.currentgamestatus = "Connection Broken\rAttempting to Reconnect\r\r";
        _root.playershipstatus[3][0] = null;
        _root.reconnectingfromgame = true;
        _root.gotoAndStop("beforeserverconnect");
    } // end else if
};
_root.mysocket.onXML = xmlhandler;

function otherpilotdfgdf(currentthread)
{
    currentothership = _root.otherplayership.length;
    currentname = currentthread[1];
    qz = 0;
    doesplayerexist = false;
    while (qz < _root.otherplayership.length && doesplayerexist == false)
    {
        if (currentname == _root.otherplayership[qz][0])
        {
            doesplayerexist = true;
            currentothership = qz;
            _root.otherplayership[currentothership][0] = currentthread[1];
            allowance = int(currentthread[5]) / 7;
            recievedxpos = int(currentthread[2]);
            _root.otherplayership[currentothership][30] = recievedxpos;
            recievedypos = int(currentthread[3]);
            _root.otherplayership[currentothership][31] = recievedypos;
            _root.otherplayership[currentothership][32] = int(currentthread[4]);
            _root.otherplayership[currentothership][25] = Math.floor((_root.otherplayership[currentothership][21] - _root.otherplayership[currentothership][1]) / framestobuffer);
            _root.otherplayership[currentothership][26] = Math.floor((_root.otherplayership[currentothership][22] - _root.otherplayership[currentothership][2]) / framestobuffer);
            _root.otherplayership[currentothership][27] = _root.otherplayership[currentothership][23] - _root.otherplayership[currentothership][3];
            if (_root.otherplayership[currentothership][27] > 180)
            {
                _root.otherplayership[currentothership][27] = (_root.otherplayership[currentothership][27] - 360) * -1;
            }
            else if (_root.otherplayership[currentothership][27] < -180)
            {
                _root.otherplayership[currentothership][27] = (_root.otherplayership[currentothership][27] + 360) * -1;
            } // end else if
            _root.otherplayership[currentothership][27] = Math.floor(_root.otherplayership[currentothership][27] / framestobuffer);
            _root.otherplayership[currentothership][33] = Number(currentthread[5]);
            _root.otherplayership[currentothership][36] = new Array();
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][36][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][36][0] = _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][36][0] = _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][36][1] = _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][36][1] = _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            if (_root.otherplayership[currentothership][16] != currentthread[7])
            {
                newaction = String(currentthread[7]);
                if (newaction == "C" && _root.otherplayership[currentothership][16] != "D")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, false);
                    _root.gamedisplayarea["otherplayership" + _root.otherplayership[currentothership][0]].attachMovie("shipactivateothercloak", "shipactivateothercloak", 69);
                    _root.otherplayership[currentothership][16] = newaction;
                }
                else if (newaction == "S" && _root.otherplayership[currentothership][16] != "D")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, true);
                    _root.otherplayership[currentothership][16] = newaction;
                }
                else if (newaction != "S" && newaction != "C")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, true);
                    if (root.otherplayership[currentothership][16] == "C")
                    {
                        _root.gamedisplayarea["otherplayership" + _root.otherplayership[currentothership][0]].attachMovie("shipactivateotherdecloak", "shipactivateotherdecloak", 69);
                        
                    } // end if
                    _root.otherplayership[currentothership][16] = newaction;
                } // end else if
            } // end else if
            systemtime = String(getTimer() + _root.clocktimediff);
            systemtime = Number(systemtime.substr(systemtime.length - 4));
            jumpahead = systemtime - Number(currentthread[8]);
            if (jumpahead < -1000)
            {
                jumpahead = jumpahead + 10000;
            } // end if
            jumptime = jumpahead + getTimer();
            _root.otherplayership[currentothership][39] = Number(currentthread[8]);
            _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        } // end if
        ++qz;
    } // end while
    if (doesplayerexist == false)
    {
        currentothership = qz;
        _root.otherplayership[currentothership] = new Array();
        _root.otherplayership[currentothership][0] = currentthread[1];
        _root.otherplayership[currentothership][1] = int(currentthread[2]);
        _root.otherplayership[currentothership][2] = int(currentthread[3]);
        _root.otherplayership[currentothership][3] = int(currentthread[4]);
        _root.otherplayership[currentothership][4] = int(currentthread[5]);
        _root.otherplayership[currentothership][5] = new Array();
        _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        _root.otherplayership[currentothership][7] = _root.curenttime + shiptimeoutime * 2;
        jjjj = 0;
        dataupdated = false;
        while (jjjj < _root.currentonlineplayers.length && dataupdated == false)
        {
            if (_root.otherplayership[currentothership][0] == _root.currentonlineplayers[jjjj][0])
            {
                dataupdated = true;
                _root.otherplayership[currentothership][10] = _root.currentonlineplayers[jjjj][1];
                _root.otherplayership[currentothership][11] = _root.currentonlineplayers[jjjj][2];
                _root.otherplayership[currentothership][12] = _root.currentonlineplayers[jjjj][3];
                _root.otherplayership[currentothership][13] = _root.currentonlineplayers[jjjj][4];
            } // end if
            ++jjjj;
        } // end while
        if (dataupdated == false)
        {
            _root.otherplayership.splice(currentothership, 1);
        }
        else
        {
            _root.otherplayership[currentothership][15] = "alive";
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][5][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            _root.otherplayership[currentothership][16] = currentthread[7];
            _root.gamedisplayarea.attachMovie("shiptype" + _root.otherplayership[currentothership][11], "otherplayership" + _root.otherplayership[currentothership][0], 2300 + Number(_root.otherplayership[currentothership][0]));
            if (_root.otherplayership[currentothership][16] == "C")
            {
                setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, false);
            } // end if
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".shiptype", Number(_root.otherplayership[currentothership][11]));
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".gothit", false);
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".isplayervisible", true);
        } // end if
    } // end else if
} // End of the function

stop ();

stop ();
