﻿// Action script...

// [onClipEvent of sprite 706 in frame 1]
onClipEvent (enterFrame)
{
    if (_root.gamechatinfo[4] == true)
    {
        if (chatinput == "/w " + _root.gamechatinfo[5] + " ")
        {
            Selection.setSelection(chatinput.length, chatinput.length);
        } // end if
        if (chatinput.length > 120)
        {
            chatinput = chatinput.substr(0, 120);
        } // end if
    } // end if
    if (_root.refreshchatdisplay != false)
    {
        _root.refreshchatdisplay = false;
        for (i = 0; i < currentline; i++)
        {
            removeMovieClip (_parent["chatdisplay" + i]);
        } // end of for
        currentyposition = 0;
        if (fullchatview == false)
        {
            totallines = _root.gamechatinfo[2][0];
        }
        else if (fullchatview == true)
        {
            totallines = _root.gamechatinfo[2][1];
        } // end else if
        maxperline = 60;
        currentline = 0;
        for (i = 0; i < totallines; i++)
        {
            if (_root.gamechatinfo[1][i][0].length > maxperline)
            {
                chattext = new Array();
                index = 0;
                lengthofmessage = _root.gamechatinfo[1][i][0].length;
                for (q = 0; q != -5; q++)
                {
                    cutlineat = maxperline;
                    for (tty = maxperline; tty > maxperline - 12; tty--)
                    {
                        if (_root.gamechatinfo[1][i][0].charAt(index + tty) == " ")
                        {
                            cutlineat = tty;
                            break;
                        } // end if
                    } // end of for
                    if (lengthofmessage <= index + cutlineat)
                    {
                        chattext[q] = _root.gamechatinfo[1][i][0].substr(index);
                        break;
                        continue;
                    } // end if
                    chattext[q] = _root.gamechatinfo[1][i][0].substr(index, cutlineat);
                    index = index + cutlineat;
                } // end of for
            }
            else
            {
                chattext = new Array();
                chattext[0] = _root.gamechatinfo[1][i][0];
            } // end else if
            for (j = chattext.length - 1; j > -1; j--)
            {
                _parent.attachMovie("gamechatoutput", "chatdisplay" + currentline, 500 + currentline);
                _parent["chatdisplay" + currentline].chatdisplay.html = false;
                _parent["chatdisplay" + currentline].html = false;
                _parent["chatdisplay" + currentline].chatdisplay = chattext[j];
                _parent["chatdisplay" + currentline]._visible = true;
                mycolor = new Array();
                mycolor[currentline] = new Color(_parent["chatdisplay" + currentline]);
                colortosetto = _root.gamechatinfo[1][i][1];
                mycolor[currentline].setRGB(colortosetto);
                _parent["chatdisplay" + currentline]._x = chatdisplayxcoord;
                currentyposition = currentyposition - (20 + spacingbetweenchatlines);
                _parent["chatdisplay" + currentline]._y = currentyposition;
                ++currentline;
            } // end of for
        } // end of for
    } // end if
}
