﻿// Action script...

// [onClipEvent of sprite 706 in frame 1]
onClipEvent (keyDown)
{
    if (Key.isDown(36))
    {
        if (fullchatview == false)
        {
            fullchatview = true;
            _parent.chatbackground.gotoAndStop(2);
            setProperty("_parent.chatdisplay", _height, chatheightperline * _root.gamechatinfo[2][1]);
            setProperty("_parent.chatdisplay", _y, -chatheightperline * (_root.gamechatinfo[2][1] - 1) + 10);
        }
        else
        {
            fullchatview = false;
            _parent.chatbackground.gotoAndStop(1);
            setProperty("_parent.chatdisplay", _height, chatheightperline * _root.gamechatinfo[2][0]);
            setProperty("_parent.chatdisplay", _y, -chatheightperline * _root.gamechatinfo[2][0] + 10);
        } // end else if
        _root.refreshchatdisplay = true;
    } // end if
    if (Key.isDown(13) && typingtext == true && typingtextchange == false)
    {
        if (chatinput != "")
        {
            chatinput = func_removekeys(chatinput);
            if (chatinput == "?sound=on")
            {
                _root.soundvolume = "on";
                _root.enterintochat(" Sound Is  Now Turned On ", _root.systemchattextcolor);
            }
            else if (chatinput == "?sound=off")
            {
                _root.soundvolume = "off";
                _root.enterintochat(" Sound Is Now Turned Off  ", _root.systemchattextcolor);
            }
            else if (chatinput.toUpperCase() == "?KFLAG")
            {
                if (_root.teamdeathmatch == true || _root.isgameracingzone == true)
                {
                    _root.enterintochat(" This is Not a flagging Zone", _root.systemchattextcolor);
                }
                else
                {
                    _root.func_playercreatekflag();
                } // end else if
            }
            else if (chatinput.substr(0, 6) == "?news=")
            {
                if (_root.playershipstatus[5][12] == "SMOD" || _root.playershipstatus[5][12] == "ADMIN")
                {
                    newstosend = chatinput.substr(6);
                    datatosend = "NEWS`MES`" + newstosend + "~";
                    _root.mysocket.send(datatosend);
                } // end if
            }
            else if (chatinput.substr(0, 3) == "/w " || chatinput.substr(0, 3) == "/W ")
            {
                for (j = 3; j < chatinput.length; j++)
                {
                    if (chatinput.charAt(j) != " ")
                    {
                        namestartsat = j;
                        break;
                    } // end if
                } // end of for
                idnametosendto = null;
                messagetocapitals = chatinput.toUpperCase();
                for (j = 0; j < _root.currentonlineplayers.length; j++)
                {
                    nametocheckwithlength = _root.currentonlineplayers[j][1].toUpperCase();
                    lengthofnametocheck = nametocheckwithlength.length;
                    if (nametocheckwithlength == messagetocapitals.substr(namestartsat, lengthofnametocheck) && messagetocapitals.charAt(namestartsat + lengthofnametocheck) == " ")
                    {
                        idnametosendto = _root.currentonlineplayers[j][0];
                        messagetosend = chatinput.substr(namestartsat + lengthofnametocheck + 1);
                        break;
                    } // end if
                } // end of for
                if (messagetosend.length > 0 && idnametosendto != null)
                {
                    chattosend = "SP`" + idnametosendto + "~CH`" + _root.playershipstatus[3][0] + "`PM`" + messagetosend + "~";
                    if (_root.gamechatinfo[2][2] == false)
                    {
                        _root.mysocket.send(chattosend);
                        if (idnametosendto != _root.playershipstatus[3][0])
                        {
                            _root.enterintochat(_root.playershipstatus[3][2] + ": " + messagetosend, _root.privatechattextcolor);
                        } // end if
                    }
                    else
                    {
                        _root.enterintochat(" You Are Muted ", _root.systemchattextcolor);
                    } // end if
                } // end else if
            }
            else if (chatinput.substr(0, 7).toUpperCase() == "?IGNORE")
            {
                playername = chatinput.substr(8).toUpperCase();
                func_ignorelist(playername);
            }
            else if (chatinput.substr(0, 2) == "//" && _root.playershipstatus[5][2] != "N/A")
            {
                chattosend = "TM~CH`" + _root.playershipstatus[3][0] + "`TM`" + chatinput.substr(2) + "~";
                if (_root.gamechatinfo[2][2] == false)
                {
                    _root.mysocket.send(chattosend);
                }
                else
                {
                    _root.enterintochat(" You Are Muted ", _root.systemchattextcolor);
                } // end else if
            }
            else if (chatinput.charAt(0) == ";" && _root.playershipstatus[5][10] != "NONE")
            {
                chattosend = "SQUAD`CH`" + _root.playershipstatus[3][0] + "`SM`" + chatinput.substr(1) + "~";
                _root.mysocket.send(chattosend);
            }
            else if (chatinput.substr(0, 2) == "/*" && (_root.playershipstatus[5][12] == "MOD" || _root.playershipstatus[5][12] == "SMOD" || _root.playershipstatus[5][12] == "ADMIN"))
            {
                func_admincommands(chatinput);
            }
            else
            {
                chattosend = "SA~CH`" + _root.playershipstatus[3][0] + "`M`" + _root.badwordfiltering(chatinput) + "`" + _root.errorchecknumber + "~";
                if (_root.gamechatinfo[2][2] == false)
                {
                    _root.mysocket.send(chattosend);
                    _root.errorcheckedmessage(chattosend, _root.errorchecknumber);
                }
                else
                {
                    _root.enterintochat(" You Are Muted ", _root.systemchattextcolor);
                } // end else if
                func_checkforspamming();
            } // end else if
        } // end else if
        typingtext = false;
        typingtextchange = true;
        chatinput = "";
        _root.gamechatinfo[4] = false;
        Selection.setFocus("_parent.hiddentextfield");
        _parent.textoutline.gotoAndStop(1);
    } // end if
    if (Key.isDown(13) && typingtext == false && typingtextchange == false)
    {
        _parent.textoutline.gotoAndStop(2);
        typingtext = true;
        typingtextchange = true;
        _root.gamechatinfo[4] = true;
        Selection.setFocus("chatinput");
    } // end if
    typingtextchange = false;
}
