﻿// Action script...

// [onClipEvent of sprite 706 in frame 1]
onClipEvent (load)
{
    function func_ignorelist(playername)
    {
        ignoringplayer = true;
        for (ignoreing = 0; ignoreing < _root.gamechatinfo[6].length; ignoreing++)
        {
            if (playername == _root.gamechatinfo[6][ignoreing])
            {
                ignoringplayer = false;
                _root.gamechatinfo[6].splice(ignoreing, 1);
                _root.enterintochat(" Removing From Ignore List: " + playername, _root.systemchattextcolor);
                break;
            } // end if
        } // end of for
        if (ignoringplayer == true)
        {
            location = _root.gamechatinfo[6].length;
            _root.gamechatinfo[6][location] = playername;
            _root.enterintochat(" Adding To Ignore List: " + playername, _root.systemchattextcolor);
        } // end if
    } // End of the function
    function func_admincommands(chatinput)
    {
        chattosend = chatinput.toUpperCase();
        if (chattosend.substr(0, 6) == "/*SHIP")
        {
            if (_root.playershipstatus[5][12] == "ADMIN" || _root.playershipstatus[5][12] == "SMOD")
            {
                _root.randomegameevents.func_creationofaship();
            } // end if
        } // end if
        if (chattosend.substr(0, 6) == "/*KICK")
        {
            playerlocation = func_namelocation(chattosend.substr(7));
            if (playerlocation != null)
            {
                chattosend = "ADMIN`KICK`" + playerlocation + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 14) == "/*IPUSEDBYNAME")
        {
            squadname = chattosend.substr(15);
            if (squadname != null)
            {
                chattosend = "ADMIN`IPUSEDBYNAME`" + squadname + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 15) == "/*NAMESUSEDBYIP")
        {
            squadname = chattosend.substr(16);
            if (squadname != null)
            {
                chattosend = "ADMIN`NAMESUSEDBYIP`" + squadname + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 13) == "/*DELETESQUAD")
        {
            squadname = chattosend.substr(14);
            if (squadname != null)
            {
                chattosend = "ADMIN`DELETESQUAD`" + squadname + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 11) == "/*SQUADINFO")
        {
            squadname = chattosend.substr(12);
            if (squadname != null)
            {
                chattosend = "ADMIN`SQUADINFO`" + squadname + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 6) == "/*MUTE")
        {
            playerlocation = func_namelocation(chattosend.substr(7));
            if (playerlocation != null)
            {
                chattosend = "ADMIN`MUTE`" + playerlocation + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*ARENA")
        {
            messagetosend = chatinput.substr(8);
            chattosend = "SA~CH`" + _root.playershipstatus[3][0] + "`AM`" + messagetosend + "`" + _root.errorchecknumber + "~";
            if (_root.gamechatinfo[2][2] == false)
            {
                _root.mysocket.send(chattosend);
                _root.errorcheckedmessage(chattosend, _root.errorchecknumber);
            }
            else
            {
                _root.enterintochat(" You Are Muted ", _root.systemchattextcolor);
            } // end else if
        }
        else if (chattosend.substr(0, 10) == "/*SHUTDOWN")
        {
            if (_root.playershipstatus[5][12] != "MOD")
            {
                chattosend = "ADMIN`SHUTDOWN`~";
                _root.mysocket.send(chattosend);
            }
            else
            {
                _root.enterintochat(" Not For Mods", _root.systemchattextcolor);
            } // end else if
        }
        else if (chattosend.substr(0, 13) == "/*SQUADWAREND")
        {
            chattosend = "ADMIN`SQUAD WAR`MODEND`~";
            _root.mysocket.send(chattosend);
        }
        else if (chattosend.substr(0, 15) == "/*SQUADWARSTART")
        {
            chattosend = "ADMIN`SQUAD WAR`MODSTART`~";
            _root.mysocket.send(chattosend);
        }
        else if (chattosend.substr(0, 10) == "/*SQUADWAR")
        {
            chattosend = chattosend + " ";
            squadnames = new Array();
            startofname = 11;
            for (i = 11; i < chattosend.length; i++)
            {
                if (chattosend.charAt(i) == " ")
                {
                    endofnamelocation = i;
                    if (endofnamelocation - startofname > 2)
                    {
                        difference = endofnamelocation - startofname;
                        name = chattosend.substr(startofname, difference);
                        squadnames[squadnames.length] = name.toUpperCase();
                    } // end if
                    i = i + 1;
                    startofname = i;
                } // end if
            } // end of for
            chattosend = "ADMIN`SQUAD WAR`CREATE";
            for (jt = 0; jt < 2; jt++)
            {
                chattosend = chattosend + ("`" + squadnames[jt]);
            } // end of for
            chattosend = chattosend + "~";
            if (squadnames.length >= 1)
            {
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*FUNDS")
        {
            if (_root.playershipstatus[5][12] != "MOD")
            {
                startoffunds = 8;
                for (i = 8; i < chattosend.length; i++)
                {
                    if (chattosend.charAt(i) == " ")
                    {
                        startofname = i + 1;
                        endoffundslocation = i;
                        break;
                    } // end if
                } // end of for
                totalfunds = Number(chattosend.substr(startoffunds, endoffundslocation - startoffunds));
                playerlocation = func_namelocation(chattosend.substr(startofname));
                if (playerlocation != null && !isNaN(totalfunds))
                {
                    chattosend = "ADMIN`FUNDS`" + totalfunds + "`" + playerlocation + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            }
            else
            {
                _root.enterintochat(" Not For Mods", _root.systemchattextcolor);
            } // end else if
        }
        else if (chattosend.substr(0, 8) == "/*BOUNTY")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                startofbty = 9;
                for (i = 9; i < chattosend.length; i++)
                {
                    if (chattosend.charAt(i) == " ")
                    {
                        startofname = i + 1;
                        endofbtylocation = i;
                        break;
                    } // end if
                } // end of for
                totalbounty = Number(chattosend.substr(startofbty, endofbtylocation - startofbty));
                playerlocation = func_namelocation(chattosend.substr(startofname));
                if (playerlocation != null && !isNaN(totalbounty))
                {
                    chattosend = "ADMIN`BOUNTY`" + totalbounty + "`" + playerlocation + "`" + chattosend.substr(startofname) + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*SCORE")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                startofscore = 8;
                for (i = 8; i < chattosend.length; i++)
                {
                    if (chattosend.charAt(i) == " ")
                    {
                        startofname = i + 1;
                        endofscorelocation = i;
                        break;
                    } // end if
                } // end of for
                totalscore = Number(chattosend.substr(startofscore, endofscorelocation - startofscore)) * _root.scoreratiomodifier;
                playerlocation = func_namelocation(chattosend.substr(startofname));
                if (playerlocation != null && !isNaN(totalscore))
                {
                    chattosend = "ADMIN`" + playerlocation + "`SCORE`" + totalscore + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            } // end if
        }
        else if (chattosend.substr(0, 14) == "/*CREATEPIRATE")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                _root.randomegameevents.func_randompiratebase(1000);
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*PRICE")
        {
            _root.randomegameevents.func_createapricechange();
        }
        else if (chattosend.substr(0, 6) == "/*FLAG" && _root.kingofflag[0] == null)
        {
            startofxloc = 7;
            for (i = startofxloc; i < chattosend.length; i++)
            {
                if (chattosend.charAt(i) == " ")
                {
                    startofy = i + 1;
                    endofloc = i;
                    break;
                } // end if
            } // end of for
            desiredx = Number(chattosend.substr(startofxloc, endofloc - startofxloc));
            desiredy = Number(chattosend.substr(startofy));
            if (isNaN(desiredx))
            {
                desiredx = 0;
            } // end if
            if (isNaN(desiredy))
            {
                desiredy = 0;
            } // end if
            xwidthofsector = _root.sectorinformation[1][0];
            ywidthofsector = _root.sectorinformation[1][1];
            xcoord = xwidthofsector * desiredx - Math.round(xwidthofsector / 2);
            ycoord = ywidthofsector * desiredy - Math.round(ywidthofsector / 2);
            if (_root.kingofflag[0] == null && _root.playershipstatus[5][12] != "MOD")
            {
                datatosend = "KFLAG`CREATE`" + xcoord + "`" + ycoord + "~";
                _root.mysocket.send(datatosend);
            } // end if
        }
        else if (chattosend.substr(0, 8) == "/*METEOR")
        {
            if (_root.playershipstatus[5][12] == "ADMIN" || _root.playershipstatus[5][12] == "SMOD")
            {
                _root.func_beginmeteor();
            } // end if
        }
        else if (chattosend.substr(0, 4) == "/*IP")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                playerlocation = func_namelocation(chattosend.substr(5));
                if (playerlocation != null)
                {
                    chattosend = "ADMIN`IP`" + playerlocation + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            } // end if
        }
        else if (chattosend.substr(0, 5) == "/*BAN")
        {
            if (_root.playershipstatus[5][12] == "ADMIN" || _root.playershipstatus[5][12] == "SMOD" || _root.playershipstatus[5][12] == "MOD")
            {
                startofscore = 6;
                for (i = 6; i < chattosend.length; i++)
                {
                    if (chattosend.charAt(i) == " ")
                    {
                        startofname = i + 1;
                        endofscorelocation = i;
                        break;
                    } // end if
                } // end of for
                totaltime = Number(chattosend.substr(startofscore, endofscorelocation - startofscore));
                playerlocation = func_namelocation(chattosend.substr(startofname));
                if (playerlocation != null && !isNaN(totalscore))
                {
                    chattosend = "ADMIN`" + playerlocation + "`BAN`" + totaltime + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*UNBAN")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                startofname = 8;
                playername = chattosend.substr(startofname);
                chattosend = "ADMIN`UNBAN`" + playername + "`~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 6) == "/*INFO")
        {
            nametocheck = chattosend.substr(7);
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                datatosend = "ADMIN`INFO`" + nametocheck + "`~";
                _root.mysocket.send(datatosend);
            } // end if
        }
        else if (chattosend.substr(0, 9) == "/*PROMOTE")
        {
            nametocheck = chattosend.substr(10);
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                datatosend = "ADMIN`PROMOTE`" + nametocheck + "`~";
                _root.mysocket.send(datatosend);
            } // end if
        }
        else if (chattosend.substr(0, 8) == "/*DEMOTE")
        {
            nametocheck = chattosend.substr(9);
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                datatosend = "ADMIN`DEMOTE`" + nametocheck + "`~";
                _root.mysocket.send(datatosend);
            } // end else if
        } // end else if
    } // End of the function
    function func_namelocation(nametoget)
    {
        idtosendto = null;
        nametoget = nametoget.toUpperCase();
        for (j = 0; j < _root.currentonlineplayers.length; j++)
        {
            nametocheckwithlength = nametoget;
            if (nametoget == _root.currentonlineplayers[j][1].toUpperCase())
            {
                idtosendto = _root.currentonlineplayers[j][0];
                break;
            } // end if
        } // end of for
        return (idtosendto);
    } // End of the function
    function func_removekeys(textinfo)
    {
        for (i = 0; i < textinfo.length; i++)
        {
            if (textinfo.charAt(i) == "`" || textinfo.charAt(i) == "~")
            {
                blankchar = " ";
                textinfo = textinfo.substr(0, i) + blankchar + textinfo.substr(i + 1);
            } // end if
        } // end of for
        return (textinfo);
    } // End of the function
    function func_checkforspamming()
    {
        _root.gamechatinfo[7][0].splice(0, 0, 0);
        _root.gamechatinfo[7][0][0] = getTimer();
        if (_root.gamechatinfo[7][0].length > 5)
        {
            _root.gamechatinfo[7][0].splice(5);
        } // end if
        if (_root.gamechatinfo[7][0][0] - _root.gamechatinfo[7][0][2] < _root.gamechatinfo[7][1])
        {
            _root.func_disconnectuser("FLOODING");
        }
        else if (_root.gamechatinfo[7][0][0] - _root.gamechatinfo[7][0][3] < _root.gamechatinfo[7][2])
        {
            _root.func_disconnectuser("FLOODING");
        } // end else if
    } // End of the function
    _parent.chatbackground.gotoAndStop(1);
    _parent.chatdisplay._visible = false;
    typingtext = false;
    typingtextchange = false;
    chatinput = "";
    _root.gamechatinfo[3][1] = false;
    fullchatview = false;
    chatheightperline = _parent.chatdisplay._height;
    spacingbetweenchatlines = -5;
    chatdisplayxcoord = 70;
    setProperty("_parent.chatdisplay", _y, -chatheightperline * _root.gamechatinfo[2][0] + 10);
    lastmessageshown = null;
    textoutlineoffalpha = 50;
    _parent.textoutline.gotoAndStop(1);
    currentline = 0;
}
