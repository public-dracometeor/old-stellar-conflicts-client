﻿// Action script...

// [onClipEvent of sprite 706 in frame 1]
onClipEvent (load)
{
    function func_ignorelist(playername)
    {
        ignoringplayer = true;
        for (ignoreing = 0; ignoreing < _root.gamechatinfo[6].length; ignoreing++)
        {
            if (playername == _root.gamechatinfo[6][ignoreing])
            {
                ignoringplayer = false;
                _root.gamechatinfo[6].splice(ignoreing, 1);
                _root.enterintochat(" Removing From Ignore List: " + playername, _root.systemchattextcolor);
                break;
            } // end if
        } // end of for
        if (ignoringplayer == true)
        {
            location = _root.gamechatinfo[6].length;
            _root.gamechatinfo[6][location] = playername;
            _root.enterintochat(" Adding To Ignore List: " + playername, _root.systemchattextcolor);
        } // end if
    } // End of the function
    function func_admincommands(chatinput)
    {
        chattosend = chatinput.toUpperCase();
        if (chattosend.substr(0, 6) == "/*SHIP")
        {
            if (_root.playershipstatus[5][12] == "ADMIN" || _root.playershipstatus[5][12] == "SMOD")
            {
                _root.randomegameevents.func_creationofaship();
            } // end if
        } // end if
        if (chattosend.substr(0, 6) == "/*KICK")
        {
            playerlocation = func_namelocation(chattosend.substr(7));
            if (playerlocation != null)
            {
                chattosend = "ADMIN`KICK`" + playerlocation + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 14) == "/*IPUSEDBYNAME")
        {
            squadname = chattosend.substr(15);
            if (squadname != null)
            {
                chattosend = "ADMIN`IPUSEDBYNAME`" + squadname + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 15) == "/*NAMESUSEDBYIP")
        {
            squadname = chattosend.substr(16);
            if (squadname != null)
            {
                chattosend = "ADMIN`NAMESUSEDBYIP`" + squadname + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 13) == "/*DELETESQUAD")
        {
            squadname = chattosend.substr(14);
            if (squadname != null)
            {
                chattosend = "ADMIN`DELETESQUAD`" + squadname + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 11) == "/*SQUADINFO")
        {
            squadname = chattosend.substr(12);
            if (squadname != null)
            {
                chattosend = "ADMIN`SQUADINFO`" + squadname + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 6) == "/*MUTE")
        {
            playerlocation = func_namelocation(chattosend.substr(7));
            if (playerlocation != null)
            {
                chattosend = "ADMIN`MUTE`" + playerlocation + "~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*ARENA")
        {
            messagetosend = chatinput.substr(8);
            chattosend = "SA~CH`" + _root.playershipstatus[3][0] + "`AM`" + messagetosend + "`" + _root.errorchecknumber + "~";
            if (_root.gamechatinfo[2][2] == false)
            {
                _root.mysocket.send(chattosend);
                _root.errorcheckedmessage(chattosend, _root.errorchecknumber);
            }
            else
            {
                _root.enterintochat(" You Are Muted ", _root.systemchattextcolor);
            } // end else if
        }
        else if (chattosend.substr(0, 10) == "/*SHUTDOWN")
        {
            if (_root.playershipstatus[5][12] != "MOD")
            {
                chattosend = "ADMIN`SHUTDOWN`~";
                _root.mysocket.send(chattosend);
            }
            else
            {
                _root.enterintochat(" Not For Mods", _root.systemchattextcolor);
            } // end else if
        }
        else if (chattosend.substr(0, 13) == "/*SQUADWAREND")
        {
            chattosend = "ADMIN`SQUAD WAR`MODEND`~";
            _root.mysocket.send(chattosend);
        }
        else if (chattosend.substr(0, 15) == "/*SQUADWARSTART")
        {
            chattosend = "ADMIN`SQUAD WAR`MODSTART`~";
            _root.mysocket.send(chattosend);
        }
        else if (chattosend.substr(0, 10) == "/*SQUADWAR")
        {
            chattosend = chattosend + " ";
            squadnames = new Array();
            startofname = 11;
            for (i = 11; i < chattosend.length; i++)
            {
                if (chattosend.charAt(i) == " ")
                {
                    endofnamelocation = i;
                    if (endofnamelocation - startofname > 2)
                    {
                        difference = endofnamelocation - startofname;
                        name = chattosend.substr(startofname, difference);
                        squadnames[squadnames.length] = name.toUpperCase();
                    } // end if
                    i = i + 1;
                    startofname = i;
                } // end if
            } // end of for
            chattosend = "ADMIN`SQUAD WAR`CREATE";
            for (jt = 0; jt < 2; jt++)
            {
                chattosend = chattosend + ("`" + squadnames[jt]);
            } // end of for
            chattosend = chattosend + "~";
            if (squadnames.length >= 1)
            {
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*FUNDS")
        {
            if (_root.playershipstatus[5][12] != "MOD")
            {
                startoffunds = 8;
                for (i = 8; i < chattosend.length; i++)
                {
                    if (chattosend.charAt(i) == " ")
                    {
                        startofname = i + 1;
                        endoffundslocation = i;
                        break;
                    } // end if
                } // end of for
                totalfunds = Number(chattosend.substr(startoffunds, endoffundslocation - startoffunds));
                playerlocation = func_namelocation(chattosend.substr(startofname));
                if (playerlocation != null && !isNaN(totalfunds))
                {
                    chattosend = "ADMIN`FUNDS`" + totalfunds + "`" + playerlocation + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            }
            else
            {
                _root.enterintochat(" Not For Mods", _root.systemchattextcolor);
            } // end else if
        }
        else if (chattosend.substr(0, 8) == "/*BOUNTY")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                startofbty = 9;
                for (i = 9; i < chattosend.length; i++)
                {
                    if (chattosend.charAt(i) == " ")
                    {
                        startofname = i + 1;
                        endofbtylocation = i;
                        break;
                    } // end if
                } // end of for
                totalbounty = Number(chattosend.substr(startofbty, endofbtylocation - startofbty));
                playerlocation = func_namelocation(chattosend.substr(startofname));
                if (playerlocation != null && !isNaN(totalbounty))
                {
                    chattosend = "ADMIN`BOUNTY`" + totalbounty + "`" + playerlocation + "`" + chattosend.substr(startofname) + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*SCORE")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                startofscore = 8;
                for (i = 8; i < chattosend.length; i++)
                {
                    if (chattosend.charAt(i) == " ")
                    {
                        startofname = i + 1;
                        endofscorelocation = i;
                        break;
                    } // end if
                } // end of for
                totalscore = Number(chattosend.substr(startofscore, endofscorelocation - startofscore)) * _root.scoreratiomodifier;
                playerlocation = func_namelocation(chattosend.substr(startofname));
                if (playerlocation != null && !isNaN(totalscore))
                {
                    chattosend = "ADMIN`" + playerlocation + "`SCORE`" + totalscore + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            } // end if
        }
        else if (chattosend.substr(0, 14) == "/*CREATEPIRATE")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                _root.randomegameevents.func_randompiratebase(1000);
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*PRICE")
        {
            _root.randomegameevents.func_createapricechange();
        }
        else if (chattosend.substr(0, 6) == "/*FLAG" && _root.kingofflag[0] == null)
        {
            startofxloc = 7;
            for (i = startofxloc; i < chattosend.length; i++)
            {
                if (chattosend.charAt(i) == " ")
                {
                    startofy = i + 1;
                    endofloc = i;
                    break;
                } // end if
            } // end of for
            desiredx = Number(chattosend.substr(startofxloc, endofloc - startofxloc));
            desiredy = Number(chattosend.substr(startofy));
            if (isNaN(desiredx))
            {
                desiredx = 0;
            } // end if
            if (isNaN(desiredy))
            {
                desiredy = 0;
            } // end if
            xwidthofsector = _root.sectorinformation[1][0];
            ywidthofsector = _root.sectorinformation[1][1];
            xcoord = xwidthofsector * desiredx - Math.round(xwidthofsector / 2);
            ycoord = ywidthofsector * desiredy - Math.round(ywidthofsector / 2);
            if (_root.kingofflag[0] == null && _root.playershipstatus[5][12] != "MOD")
            {
                datatosend = "KFLAG`CREATE`" + xcoord + "`" + ycoord + "~";
                _root.mysocket.send(datatosend);
            } // end if
        }
        else if (chattosend.substr(0, 8) == "/*METEOR")
        {
            if (_root.playershipstatus[5][12] == "ADMIN" || _root.playershipstatus[5][12] == "SMOD")
            {
                _root.func_beginmeteor();
            } // end if
        }
        else if (chattosend.substr(0, 4) == "/*IP")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                playerlocation = func_namelocation(chattosend.substr(5));
                if (playerlocation != null)
                {
                    chattosend = "ADMIN`IP`" + playerlocation + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            } // end if
        }
        else if (chattosend.substr(0, 5) == "/*BAN")
        {
            if (_root.playershipstatus[5][12] == "ADMIN" || _root.playershipstatus[5][12] == "SMOD" || _root.playershipstatus[5][12] == "MOD")
            {
                startofscore = 6;
                for (i = 6; i < chattosend.length; i++)
                {
                    if (chattosend.charAt(i) == " ")
                    {
                        startofname = i + 1;
                        endofscorelocation = i;
                        break;
                    } // end if
                } // end of for
                totaltime = Number(chattosend.substr(startofscore, endofscorelocation - startofscore));
                playerlocation = func_namelocation(chattosend.substr(startofname));
                if (playerlocation != null && !isNaN(totalscore))
                {
                    chattosend = "ADMIN`" + playerlocation + "`BAN`" + totaltime + "~";
                    _root.mysocket.send(chattosend);
                } // end if
            } // end if
        }
        else if (chattosend.substr(0, 7) == "/*UNBAN")
        {
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                startofname = 8;
                playername = chattosend.substr(startofname);
                chattosend = "ADMIN`UNBAN`" + playername + "`~";
                _root.mysocket.send(chattosend);
            } // end if
        }
        else if (chattosend.substr(0, 6) == "/*INFO")
        {
            nametocheck = chattosend.substr(7);
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                datatosend = "ADMIN`INFO`" + nametocheck + "`~";
                _root.mysocket.send(datatosend);
            } // end if
        }
        else if (chattosend.substr(0, 9) == "/*PROMOTE")
        {
            nametocheck = chattosend.substr(10);
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                datatosend = "ADMIN`PROMOTE`" + nametocheck + "`~";
                _root.mysocket.send(datatosend);
            } // end if
        }
        else if (chattosend.substr(0, 8) == "/*DEMOTE")
        {
            nametocheck = chattosend.substr(9);
            if (_root.playershipstatus[5][12] == "ADMIN")
            {
                datatosend = "ADMIN`DEMOTE`" + nametocheck + "`~";
                _root.mysocket.send(datatosend);
            } // end else if
        } // end else if
    } // End of the function
    function func_namelocation(nametoget)
    {
        idtosendto = null;
        nametoget = nametoget.toUpperCase();
        for (j = 0; j < _root.currentonlineplayers.length; j++)
        {
            nametocheckwithlength = nametoget;
            if (nametoget == _root.currentonlineplayers[j][1].toUpperCase())
            {
                idtosendto = _root.currentonlineplayers[j][0];
                break;
            } // end if
        } // end of for
        return (idtosendto);
    } // End of the function
    function func_removekeys(textinfo)
    {
        for (i = 0; i < textinfo.length; i++)
        {
            if (textinfo.charAt(i) == "`" || textinfo.charAt(i) == "~")
            {
                blankchar = " ";
                textinfo = textinfo.substr(0, i) + blankchar + textinfo.substr(i + 1);
            } // end if
        } // end of for
        return (textinfo);
    } // End of the function
    function func_checkforspamming()
    {
        _root.gamechatinfo[7][0].splice(0, 0, 0);
        _root.gamechatinfo[7][0][0] = getTimer();
        if (_root.gamechatinfo[7][0].length > 5)
        {
            _root.gamechatinfo[7][0].splice(5);
        } // end if
        if (_root.gamechatinfo[7][0][0] - _root.gamechatinfo[7][0][2] < _root.gamechatinfo[7][1])
        {
            _root.func_disconnectuser("FLOODING");
        }
        else if (_root.gamechatinfo[7][0][0] - _root.gamechatinfo[7][0][3] < _root.gamechatinfo[7][2])
        {
            _root.func_disconnectuser("FLOODING");
        } // end else if
    } // End of the function
    _parent.chatbackground.gotoAndStop(1);
    _parent.chatdisplay._visible = false;
    typingtext = false;
    typingtextchange = false;
    chatinput = "";
    _root.gamechatinfo[3][1] = false;
    fullchatview = false;
    chatheightperline = _parent.chatdisplay._height;
    spacingbetweenchatlines = -5;
    chatdisplayxcoord = 70;
    setProperty("_parent.chatdisplay", _y, -chatheightperline * _root.gamechatinfo[2][0] + 10);
    lastmessageshown = null;
    textoutlineoffalpha = 50;
    _parent.textoutline.gotoAndStop(1);
    currentline = 0;
}

// [onClipEvent of sprite 706 in frame 1]
onClipEvent (keyDown)
{
    if (Key.isDown(36))
    {
        if (fullchatview == false)
        {
            fullchatview = true;
            _parent.chatbackground.gotoAndStop(2);
            setProperty("_parent.chatdisplay", _height, chatheightperline * _root.gamechatinfo[2][1]);
            setProperty("_parent.chatdisplay", _y, -chatheightperline * (_root.gamechatinfo[2][1] - 1) + 10);
        }
        else
        {
            fullchatview = false;
            _parent.chatbackground.gotoAndStop(1);
            setProperty("_parent.chatdisplay", _height, chatheightperline * _root.gamechatinfo[2][0]);
            setProperty("_parent.chatdisplay", _y, -chatheightperline * _root.gamechatinfo[2][0] + 10);
        } // end else if
        _root.refreshchatdisplay = true;
    } // end if
    if (Key.isDown(13) && typingtext == true && typingtextchange == false)
    {
        if (chatinput != "")
        {
            chatinput = func_removekeys(chatinput);
            if (chatinput == "?sound=on")
            {
                _root.soundvolume = "on";
                _root.enterintochat(" Sound Is  Now Turned On ", _root.systemchattextcolor);
            }
            else if (chatinput == "?sound=off")
            {
                _root.soundvolume = "off";
                _root.enterintochat(" Sound Is Now Turned Off  ", _root.systemchattextcolor);
            }
            else if (chatinput.toUpperCase() == "?KFLAG")
            {
                if (_root.teamdeathmatch == true || _root.isgameracingzone == true)
                {
                    _root.enterintochat(" This is Not a flagging Zone", _root.systemchattextcolor);
                }
                else
                {
                    _root.func_playercreatekflag();
                } // end else if
            }
            else if (chatinput.substr(0, 6) == "?news=")
            {
                if (_root.playershipstatus[5][12] == "SMOD" || _root.playershipstatus[5][12] == "ADMIN")
                {
                    newstosend = chatinput.substr(6);
                    datatosend = "NEWS`MES`" + newstosend + "~";
                    _root.mysocket.send(datatosend);
                } // end if
            }
            else if (chatinput.substr(0, 3) == "/w " || chatinput.substr(0, 3) == "/W ")
            {
                for (j = 3; j < chatinput.length; j++)
                {
                    if (chatinput.charAt(j) != " ")
                    {
                        namestartsat = j;
                        break;
                    } // end if
                } // end of for
                idnametosendto = null;
                messagetocapitals = chatinput.toUpperCase();
                for (j = 0; j < _root.currentonlineplayers.length; j++)
                {
                    nametocheckwithlength = _root.currentonlineplayers[j][1].toUpperCase();
                    lengthofnametocheck = nametocheckwithlength.length;
                    if (nametocheckwithlength == messagetocapitals.substr(namestartsat, lengthofnametocheck) && messagetocapitals.charAt(namestartsat + lengthofnametocheck) == " ")
                    {
                        idnametosendto = _root.currentonlineplayers[j][0];
                        messagetosend = chatinput.substr(namestartsat + lengthofnametocheck + 1);
                        break;
                    } // end if
                } // end of for
                if (messagetosend.length > 0 && idnametosendto != null)
                {
                    chattosend = "SP`" + idnametosendto + "~CH`" + _root.playershipstatus[3][0] + "`PM`" + messagetosend + "~";
                    if (_root.gamechatinfo[2][2] == false)
                    {
                        _root.mysocket.send(chattosend);
                        if (idnametosendto != _root.playershipstatus[3][0])
                        {
                            _root.enterintochat(_root.playershipstatus[3][2] + ": " + messagetosend, _root.privatechattextcolor);
                        } // end if
                    }
                    else
                    {
                        _root.enterintochat(" You Are Muted ", _root.systemchattextcolor);
                    } // end if
                } // end else if
            }
            else if (chatinput.substr(0, 7).toUpperCase() == "?IGNORE")
            {
                playername = chatinput.substr(8).toUpperCase();
                func_ignorelist(playername);
            }
            else if (chatinput.substr(0, 2) == "//" && _root.playershipstatus[5][2] != "N/A")
            {
                chattosend = "TM~CH`" + _root.playershipstatus[3][0] + "`TM`" + chatinput.substr(2) + "~";
                if (_root.gamechatinfo[2][2] == false)
                {
                    _root.mysocket.send(chattosend);
                }
                else
                {
                    _root.enterintochat(" You Are Muted ", _root.systemchattextcolor);
                } // end else if
            }
            else if (chatinput.charAt(0) == ";" && _root.playershipstatus[5][10] != "NONE")
            {
                chattosend = "SQUAD`CH`" + _root.playershipstatus[3][0] + "`SM`" + chatinput.substr(1) + "~";
                _root.mysocket.send(chattosend);
            }
            else if (chatinput.substr(0, 2) == "/*" && (_root.playershipstatus[5][12] == "MOD" || _root.playershipstatus[5][12] == "SMOD" || _root.playershipstatus[5][12] == "ADMIN"))
            {
                func_admincommands(chatinput);
            }
            else
            {
                chattosend = "SA~CH`" + _root.playershipstatus[3][0] + "`M`" + _root.badwordfiltering(chatinput) + "`" + _root.errorchecknumber + "~";
                if (_root.gamechatinfo[2][2] == false)
                {
                    _root.mysocket.send(chattosend);
                    _root.errorcheckedmessage(chattosend, _root.errorchecknumber);
                }
                else
                {
                    _root.enterintochat(" You Are Muted ", _root.systemchattextcolor);
                } // end else if
                func_checkforspamming();
            } // end else if
        } // end else if
        typingtext = false;
        typingtextchange = true;
        chatinput = "";
        _root.gamechatinfo[4] = false;
        Selection.setFocus("_parent.hiddentextfield");
        _parent.textoutline.gotoAndStop(1);
    } // end if
    if (Key.isDown(13) && typingtext == false && typingtextchange == false)
    {
        _parent.textoutline.gotoAndStop(2);
        typingtext = true;
        typingtextchange = true;
        _root.gamechatinfo[4] = true;
        Selection.setFocus("chatinput");
    } // end if
    typingtextchange = false;
}

// [onClipEvent of sprite 706 in frame 1]
onClipEvent (keyUp)
{
    if (chatinput == "::")
    {
        if (_root.gamechatinfo[5] != "")
        {
            chatinput = "/w " + _root.gamechatinfo[5] + " ";
        } // end if
    } // end if
}

// [onClipEvent of sprite 706 in frame 1]
on (release)
{
    if (_root.gamechatinfo[4] != true)
    {
        Selection.setFocus("_parent.hiddentextfield");
    }
    else
    {
        Selection.setFocus("chatinput");
    } // end else if
}

// [onClipEvent of sprite 706 in frame 1]
onClipEvent (enterFrame)
{
    if (_root.gamechatinfo[4] == true)
    {
        if (chatinput == "/w " + _root.gamechatinfo[5] + " ")
        {
            Selection.setSelection(chatinput.length, chatinput.length);
        } // end if
        if (chatinput.length > 120)
        {
            chatinput = chatinput.substr(0, 120);
        } // end if
    } // end if
    if (_root.refreshchatdisplay != false)
    {
        _root.refreshchatdisplay = false;
        for (i = 0; i < currentline; i++)
        {
            removeMovieClip (_parent["chatdisplay" + i]);
        } // end of for
        currentyposition = 0;
        if (fullchatview == false)
        {
            totallines = _root.gamechatinfo[2][0];
        }
        else if (fullchatview == true)
        {
            totallines = _root.gamechatinfo[2][1];
        } // end else if
        maxperline = 60;
        currentline = 0;
        for (i = 0; i < totallines; i++)
        {
            if (_root.gamechatinfo[1][i][0].length > maxperline)
            {
                chattext = new Array();
                index = 0;
                lengthofmessage = _root.gamechatinfo[1][i][0].length;
                for (q = 0; q != -5; q++)
                {
                    cutlineat = maxperline;
                    for (tty = maxperline; tty > maxperline - 12; tty--)
                    {
                        if (_root.gamechatinfo[1][i][0].charAt(index + tty) == " ")
                        {
                            cutlineat = tty;
                            break;
                        } // end if
                    } // end of for
                    if (lengthofmessage <= index + cutlineat)
                    {
                        chattext[q] = _root.gamechatinfo[1][i][0].substr(index);
                        break;
                        continue;
                    } // end if
                    chattext[q] = _root.gamechatinfo[1][i][0].substr(index, cutlineat);
                    index = index + cutlineat;
                } // end of for
            }
            else
            {
                chattext = new Array();
                chattext[0] = _root.gamechatinfo[1][i][0];
            } // end else if
            for (j = chattext.length - 1; j > -1; j--)
            {
                _parent.attachMovie("gamechatoutput", "chatdisplay" + currentline, 500 + currentline);
                _parent["chatdisplay" + currentline].chatdisplay.html = false;
                _parent["chatdisplay" + currentline].html = false;
                _parent["chatdisplay" + currentline].chatdisplay = chattext[j];
                _parent["chatdisplay" + currentline]._visible = true;
                mycolor = new Array();
                mycolor[currentline] = new Color(_parent["chatdisplay" + currentline]);
                colortosetto = _root.gamechatinfo[1][i][1];
                mycolor[currentline].setRGB(colortosetto);
                _parent["chatdisplay" + currentline]._x = chatdisplayxcoord;
                currentyposition = currentyposition - (20 + spacingbetweenchatlines);
                _parent["chatdisplay" + currentline]._y = currentyposition;
                ++currentline;
            } // end of for
        } // end of for
    } // end if
}

// [Action in Frame 1]
stop ();
