﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
    _parent.changepassto = "";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    function checknameforlegitcharacters(name)
    {
        legitcharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        for (i = 0; i < name.length; i++)
        {
            illegalcharacter = true;
            for (j = 0; j < legitcharacters.length; j++)
            {
                if (name.charAt(i) == legitcharacters.charAt(j))
                {
                    illegalcharacter = false;
                } // end if
            } // end of for
            if (illegalcharacter == true)
            {
                return (true);
                i = 99999;
            } // end if
        } // end of for
    } // End of the function
    changepassword = _parent.changepassto;
    changepassword = changepassword.toUpperCase();
    _parent.changepassto = " CHANGING";
    if (changepassword.length > 13)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "12 Characters!";
    }
    else if (changepassword.length < 1)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "More Characters!";
    }
    else if (checknameforlegitcharacters(changepassword))
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "Letters or numbers!";
    }
    else
    {
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadscommand.php?mode=changepass&newpass=" + changepassword + "&sname=" + _root.playershipstatus[5][10] + "&spass=" + _root.playershipstatus[5][13]);
        newsquadVars.onLoad = function (success)
        {
            loadedinfo = String(newsquadVars);
            if (loadedinfo == "success")
            {
                _parent.currentpass = changepassword;
                _parent.changepassto = "";
            }
            else
            {
                _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
                _parent.hardwarewarningbox.information = "Could Not Change!\r" + changepassword;
            } // end else if
        };
    } // end else if
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "KICK";
    _parent.kickauser = "";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    usertokick = _parent.kickauser;
    usertokick = usertokick.toUpperCase();
    squadmembers = _parent._parent.squadmembers;
    squadleaders = new Array();
    squadleaders[0] = _parent._parent.creatorname;
    squadleaders[1] = _parent._parent.secondname;
    squadleaders[2] = _parent._parent.thirdname;
    abletokickuser = true;
    for (i = 0; i < squadleaders.length; i++)
    {
        if (usertokick == squadleaders[i])
        {
            abletokickuser = false;
            _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
            _parent.hardwarewarningbox.information = "Not Leaders!";
        } // end if
    } // end of for
    isuseronsquad = false;
    for (i = 0; i < squadmembers.length; i++)
    {
        if (usertokick == squadmembers[i])
        {
            isuseronsquad = true;
            playerlocation = i;
            break;
        } // end if
    } // end of for
    if (isuseronsquad == false)
    {
        abletokickuser = false;
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "Not In Squad!";
    } // end if
    if (abletokickuser == true)
    {
        _parent.kickauser = " KICKING";
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadscommand.php?mode=kickuser&name=" + usertokick + "&sname=" + _root.playershipstatus[5][10] + "&spass=" + _root.playershipstatus[5][13]);
        newsquadVars.onLoad = function (success)
        {
            loadedinfo = String(newsquadVars);
            if (loadedinfo == "success")
            {
                _parent.kickauser = " KICKED";
                _parent._parent.squadmembers.splice(playerlocation, 1);
                _parent._parent.displaysquadlist();
            }
            else
            {
                _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
                _parent.hardwarewarningbox.information = "Could Not kick!";
            } // end else if
        };
    } // end if
}
