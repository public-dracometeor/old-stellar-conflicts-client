﻿// Action script...

// [onClipEvent of sprite 1586 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    _parent.gotoAndStop("basdefences");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    function resetsectorbackground()
    {
        for (i = 0; i < _root.sectormapitems.length; i++)
        {
            _root.sectormapitems[i][4] = "OFF";
        } // end of for
        for (i = 0; i < _root.playersquadbases.length; i++)
        {
            _root.playersquadbases[i][10] = false;
        } // end of for
    } // End of the function
    this.label = "EXIT";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    resetsectorbackground();
    _root.gotoAndStop("maingameplaying");
}

// [onClipEvent of sprite 1589 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    _parent.gotoAndStop("squadmanagement");
}

// [onClipEvent of sprite 1593 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    _parent.gotoAndStop("baseupgrades");
}

// [onClipEvent of sprite 1596 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    _parent.gotoAndStop("buycapitalship");
}

// [onClipEvent of sprite 1669 in frame 2]
onClipEvent (load)
{
    function func_setupinfo()
    {
        this.basefunction.gotoAndStop(1);
        squadbasetype = 0;
        playerssquad = _root.playershipstatus[5][10];
        for (jk = 0; jk < _root.playersquadbases.length; jk++)
        {
            if (playerssquad == _root.playersquadbases[jk][0])
            {
                squadbasetype = _root.playersquadbases[jk][1];
            } // end if
        } // end of for
        this.baseimage.gotoAndStop(squadbasetype + 1);
        this.basefunction.squadbasetype = squadbasetype;
        numberofbaseturrets = Number(_root.squadbaseinfo[squadbasetype][2][2]);
        turrets[0] = "Loading";
        turret1disp = "1: Loading";
        turrets[1] = "Loading";
        turret2disp = "2: Loading";
        turrets[2] = "Loading";
        turret3disp = "3: Loading";
        if (numberofbaseturrets >= 4)
        {
            turrets[3] = "Loading";
            turret4disp = "4: Loading";
            turret4disp._visible = true;
        }
        else
        {
            turret4disp._visible = false;
        } // end else if
        if (numberofbaseturrets >= 5)
        {
            turrets[4] = "Loading";
            turret5disp = "5: Loading";
            turret5disp._visible = true;
        }
        else
        {
            turret5disp._visible = false;
        } // end else if
        this.generatordisp = "Loading";
        this.maxchargedisp = "Loading";
        this.chargeratedisp = "Loading";
        this.structuredisp = "Loading";
        this.missileammodisp = "Loading";
        baseidname = _root.squadbasedockedat;
        maxstructure = _root.squadbaseinfo[squadbasetype][3][1];
        this.structuredisp = "Structure: Loading";
    } // End of the function
    function getbasevariables()
    {
        func_setupinfo();
        this.newbaseVars = new XML();
        this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=loadbattle&baseid=" + baseidname);
        this.newbaseVars.onLoad = function (success)
        {
            loadedvars = String(newbaseVars);
            loadbasevars(loadedvars);
            setupturrets();
        };
    } // End of the function
    function loadbasevars(loadedvars)
    {
        newinfo = loadedvars.split("~");
        for (i = 0; i < newinfo.length - 1; i++)
        {
            currentthread = newinfo[i].split("`");
            turrets = new Array();
            turrets[0] = Number(currentthread[0]);
            if (isNaN(turrets[0]))
            {
                turrets[0] = "NONE";
            } // end if
            turret1disp = "1: " + _root.guntype[turrets[0]][6];
            turrets[1] = Number(currentthread[1]);
            if (isNaN(turrets[1]))
            {
                turrets[1] = "NONE";
            } // end if
            turret2disp = "2: " + _root.guntype[turrets[1]][6];
            turrets[2] = Number(currentthread[2]);
            if (isNaN(turrets[2]))
            {
                turrets[2] = "NONE";
            } // end if
            turret3disp = "3: " + _root.guntype[turrets[2]][6];
            turrets[3] = Number(currentthread[3]);
            if (isNaN(turrets[3]))
            {
                turrets[3] = "NONE";
            } // end if
            turret4disp = "4: " + _root.guntype[turrets[3]][6];
            turrets[4] = Number(currentthread[4]);
            if (isNaN(turrets[4]))
            {
                turrets[4] = "NONE";
            } // end if
            turret5disp = "5: " + _root.guntype[turrets[4]][6];
            shieldgenerator = Number(currentthread[5]);
            this.generatordisp = _root.shieldgenerators[shieldgenerator][4];
            this.maxchargedisp = "Max Charge: " + _root.shieldgenerators[shieldgenerator][0] * _root.squadbaseinfo[squadbasetype][1][1];
            this.chargeratedisp = "Charge Rate: " + _root.shieldgenerators[shieldgenerator][1] * _root.squadbaseinfo[squadbasetype][1][2] + "/sec";
            totalstructure = Number(currentthread[6]);
            this.structuredisp = "Structure: " + totalstructure + " / " + maxstructure;
            this.missilerounds = Number(currentthread[7]);
            this.missileammodisp = "Shots: " + this.missilerounds;
        } // end of for
    } // End of the function
    getbasevariables();
}

// [onClipEvent of sprite 1069 in frame 4]
onClipEvent (load)
{
}

// [Action in Frame 1]
_root.func_checkfortoomuchaddedandset();
welcomemsg = "Welcome to Squad Base - " + _root.playershipstatus[5][10];
information = "PI`CLEAR`~";
_root.mysocket.send(information);
_root.aishipshosted = new Array();
_root.attachMovie("onlineplayerslist", "onlineplayerslist", 99982);
setProperty("_root.onlineplayerslist", _x, 0);
setProperty("_root.onlineplayerslist", _y, 0);
_root.playershipstatus[5][15] = "";
clearInterval(_root.specialstimers[1]);
clearInterval(_root.specialstimers[2]);
stop ();

// [Action in Frame 2]
stop ();

// [Action in Frame 3]
stop ();

// [Action in Frame 4]
stop ();

// [Action in Frame 5]
stop ();
