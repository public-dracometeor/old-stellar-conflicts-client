﻿// Action script...

// [onClipEvent of sprite 1249 in frame 1]
onClipEvent (load)
{
    _root.versionno = ".184b";
    _root.isgamerunningfromremote = false;
    _root.accountserveraddy = "217.160.243.182";
    _root.accountserverport = "2125";
    _root._quality = "HIGH";
    _root.scoreratiomodifier = 50;
    _root.missiontoactualscoremodifier = 10;
    _root.publicteams = 20;
    _root.lastplayerssavedinfo = "";
    _root.regularchattextcolor = 3407871;
    _root.privatechattextcolor = 3407667;
    _root.systemchattextcolor = 3407667;
    _root.teamchattextcolor = 16776960;
    _root.squadchattextcolor = 14496563;
    _root.arenachattextcolor = 16711935;
    _root.playershipstatus[3][2] = "Enter Name";
    _root.isplayeraguest = false;
    if (_root.isgamerunningfromremote != true)
    {
        _root.pathtoaccounts = "";
    }
    else
    {
        _root.pathtoaccounts = "http://www.gecko-games.com/stellar/";
    } // end else if
    loadVariables("http://www.gecko-games.com/stellar/accountsserv2.php", _root.scoreClip, "POST";
    _root.isplayeremp = false;
    _root.playerempend = 0;
    _root.playerdiruptend = 0;
    _root.isplayerdisrupt = false;
    _root.accountcreationsavedgame = "PI`ST0`SG0`EG0`EC0`CR350000`SE100`SB113`HP0G0`HP1G0~";
    _root.pinginformation = "";
    _root.mapdsiplayed = false;
    _root.isaiturnedon = true;
    _root.playersdockedtime = null;
    _root.playersexitdocktimewait = 4500;
    _root.playersdestination = new Array();
    _root.playersdestination[0] = null;
    _root.playersdestination[1] = null;
    _root.playersdestination[2] = null;
    _root.playerjustloggedin = true;
    _root.hasplayerbeenlistedasdocked = false;
    _root.soundvolume = "on";
    _root.loginerror = "Use a name of only letters, numbers. Under 11 Digits";
    _root.currentmaxplayers = 15;
    _root.playersworthtobtymodifire = 1000;
    _root.leftindentofgamearea = 0;
    _root.topindentofgamearea = 0;
    _root.gameareawidth = 700;
    _root.gameareaheight = 550;
    _root.playershiptype = "shiptype1";
    _root.playershipfacing = 0;
    _root.playershiprotation = 10;
    _root.playershipvelocity = 0;
    _root.playershipmaxvelocity = 100;
    _root.playershipacceleration = 40;
    _root.newdraw = true;
    _root.totalstars = 3;
    _root.totalstartypes = 5;
    _root.shipcoordinatex = 0;
    _root.shipcoordinatey = 0;
    _root.currentplayershotsfired = 0;
    _root.playershipstatus = new Array();
    _root.playershipstatus[0] = new Array();
    _root.playershipstatus[0][0] = new Array();
    _root.playershipstatus[0][0][0] = 0;
    _root.playershipstatus[0][0][1] = 0;
    _root.playershipstatus[0][0][2] = 5;
    _root.playershipstatus[0][0][3] = 0;
    _root.playershipstatus[0][0][4] = "ON";
    _root.playershipstatus[0][1] = new Array();
    _root.playershipstatus[0][1][0] = 1;
    _root.playershipstatus[0][1][1] = 0;
    _root.playershipstatus[0][1][2] = -7;
    _root.playershipstatus[0][1][3] = 0;
    _root.playershipstatus[0][1][4] = "ON";
    _root.playershipstatus[1] = new Array();
    _root.playershipstatus[1][0] = 0;
    _root.playershipstatus[1][1] = 0;
    _root.playershipstatus[1][5] = 0;
    _root.playershipstatus[2] = new Array();
    _root.playershipstatus[2][0] = 0;
    _root.playershipstatus[2][1] = 0;
    _root.playershipstatus[2][2] = "FULL";
    _root.playershipstatus[2][5] = 1000;
    _root.playershipstatus[2][6] = 0;
    _root.playershipstatus[3] = new Array();
    _root.playershipstatus[3][0] = "";
    _root.playershipstatus[3][1] = 20000;
    _root.playershipstatus[3][3] = null;
    _root.playershipstatus[3][5] = null;
    _root.playershipstatus[4] = new Array();
    _root.playershipstatus[4][0] = "";
    _root.playershipstatus[4][1] = new Array();
    _root.playershipstatus[5] = new Array();
    _root.playershipstatus[5][0] = 0;
    _root.playershipstatus[5][1] = null;
    _root.playershipstatus[5][2] = "N/A";
    _root.playershipstatus[5][3] = 0;
    _root.playershipstatus[5][4] = "alive";
    _root.playershipstatus[5][5] = null;
    _root.playershipstatus[5][6] = 0.125000;
    _root.playershipstatus[5][7] = null;
    _root.playershipstatus[5][8] = 0;
    _root.playershipstatus[5][9] = Number(0);
    _root.playershipstatus[5][10] = "NONE";
    _root.playershipstatus[5][11] = false;
    _root.playershipstatus[5][12] = "";
    _root.playershipstatus[5][13] = "";
    _root.playershipstatus[5][15] = "";
    _root.playershipstatus[5][19] = "";
    _root.playershipstatus[5][20] = false;
    _root.playershipstatus[5][21] = 0;
    _root.playershipstatus[5][22] = false;
    _root.playershipstatus[5][25] = "YES";
    _root.playershipstatus[6] = new Array();
    _root.playershipstatus[6][0] = 0;
    _root.playershipstatus[6][1] = 0;
    _root.playershipstatus[6][2] = 0;
    _root.playershipstatus[6][3] = 0;
    _root.playershipstatus[7] = new Array();
    _root.playershipstatus[7][0] = new Array();
    _root.playershipstatus[7][0][0] = 0;
    _root.playershipstatus[7][0][1] = 0;
    _root.playershipstatus[7][0][2] = 5;
    _root.playershipstatus[7][0][3] = 0;
    _root.playershipstatus[7][0][4] = new Array();
    _root.playershipstatus[7][0][4][0] = 2;
    _root.playershipstatus[7][0][4][1] = 2;
    _root.playershipstatus[7][0][5] = 10;
    _root.playershipstatus[7][1] = new Array();
    _root.playershipstatus[7][1][0] = 0;
    _root.playershipstatus[7][1][1] = 0;
    _root.playershipstatus[7][1][2] = 5;
    _root.playershipstatus[7][1][3] = 0;
    _root.playershipstatus[7][1][4] = new Array();
    _root.playershipstatus[7][1][4][0] = 10;
    _root.playershipstatus[7][1][4][1] = 5;
    _root.playershipstatus[7][1][5] = 10;
    _root.playershipstatus[8] = new Array();
    _root.playershipstatus[9] = "AUTO";
    _root.playershipstatus[10] = 0;
    _root.playershipstatus[11] = new Array();
    _root.playershipstatus[11][0] = new Array();
    _root.playershipstatus[11][0][0] = 0;
    _root.playershipstatus[11][0][1] = 0;
    _root.playershipstatus[11][0][2] = 0;
    _root.playershipstatus[11][1] = new Array();
    _root.playershipstatus[11][2] = new Array();
    _root.playershipstatus[11][2][0] = new Array();
    _root.playershipstatus[11][2][0][0] = 0;
    _root.playershipstatus[11][2][1] = new Array();
    _root.playershipstatus[11][2][1][0] = 0;
    _root.playershipstatus[11][2][2] = new Array();
    _root.playershipstatus[11][2][2][0] = 0;
    currentmissile = 0;
    _root.missile = new Array();
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 120;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 8000;
    _root.missile[currentmissile][3] = 3000;
    _root.missile[currentmissile][4] = 1250;
    _root.missile[currentmissile][5] = 500;
    _root.missile[currentmissile][6] = "Torp 1";
    _root.missile[currentmissile][7] = "Torp 1";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 110;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 11000;
    _root.missile[currentmissile][3] = 3500;
    _root.missile[currentmissile][4] = 2200;
    _root.missile[currentmissile][5] = 1500;
    _root.missile[currentmissile][6] = "Torp 2";
    _root.missile[currentmissile][7] = "Torp 2";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 100;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 12000;
    _root.missile[currentmissile][3] = 4000;
    _root.missile[currentmissile][4] = 3400;
    _root.missile[currentmissile][5] = 2750;
    _root.missile[currentmissile][6] = "Torp 3";
    _root.missile[currentmissile][7] = "Torp 3";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 90;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 15000;
    _root.missile[currentmissile][3] = 5000;
    _root.missile[currentmissile][4] = 5500;
    _root.missile[currentmissile][5] = 4500;
    _root.missile[currentmissile][6] = "Torp 4";
    _root.missile[currentmissile][7] = "Torp 4";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 120;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 12000;
    _root.missile[currentmissile][3] = 4000;
    _root.missile[currentmissile][4] = 4500;
    _root.missile[currentmissile][5] = 4250;
    _root.missile[currentmissile][6] = "Stealth";
    _root.missile[currentmissile][7] = "Stealth";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 100;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 7000;
    _root.missile[currentmissile][3] = 4000;
    _root.missile[currentmissile][4] = 2000;
    _root.missile[currentmissile][5] = 1400;
    _root.missile[currentmissile][6] = "Mirv 1";
    _root.missile[currentmissile][7] = "Mirv 1";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 85;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 8000;
    _root.missile[currentmissile][3] = 6000;
    _root.missile[currentmissile][4] = 4000;
    _root.missile[currentmissile][5] = 3000;
    _root.missile[currentmissile][6] = "Mirv 2";
    _root.missile[currentmissile][7] = "Mirv 2";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 70;
    _root.missile[currentmissile][1] = 0;
    _root.missile[currentmissile][2] = 9000;
    _root.missile[currentmissile][3] = 7500;
    _root.missile[currentmissile][4] = 6500;
    _root.missile[currentmissile][5] = 5500;
    _root.missile[currentmissile][6] = "Mirv 3";
    _root.missile[currentmissile][7] = "Mirv 3";
    _root.missile[currentmissile][8] = "DUMBFIRE";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 70;
    _root.missile[currentmissile][1] = 65;
    _root.missile[currentmissile][2] = 8000;
    _root.missile[currentmissile][3] = 6500;
    _root.missile[currentmissile][4] = 1500;
    _root.missile[currentmissile][5] = 3500;
    _root.missile[currentmissile][6] = "Seek 1";
    _root.missile[currentmissile][7] = "Seek 1";
    _root.missile[currentmissile][8] = "SEEKER";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 60;
    _root.missile[currentmissile][1] = 55;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 7500;
    _root.missile[currentmissile][4] = 2550;
    _root.missile[currentmissile][5] = 5500;
    _root.missile[currentmissile][6] = "Seek 2";
    _root.missile[currentmissile][7] = "Seek 2";
    _root.missile[currentmissile][8] = "SEEKER";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 50;
    _root.missile[currentmissile][1] = 35;
    _root.missile[currentmissile][2] = 13000;
    _root.missile[currentmissile][3] = 10000;
    _root.missile[currentmissile][4] = 3500;
    _root.missile[currentmissile][5] = 8500;
    _root.missile[currentmissile][6] = "Seek 3";
    _root.missile[currentmissile][7] = "Seek 3";
    _root.missile[currentmissile][8] = "SEEKER";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 55;
    _root.missile[currentmissile][1] = 45;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 9000;
    _root.missile[currentmissile][4] = 3500;
    _root.missile[currentmissile][5] = 7500;
    _root.missile[currentmissile][6] = "Seek ST";
    _root.missile[currentmissile][7] = "Seek ST";
    _root.missile[currentmissile][8] = "SEEKER";
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 65;
    _root.missile[currentmissile][1] = 65;
    _root.missile[currentmissile][2] = 15000;
    _root.missile[currentmissile][3] = 6000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 4000;
    _root.missile[currentmissile][6] = "EMP 1";
    _root.missile[currentmissile][7] = "EMP 1";
    _root.missile[currentmissile][8] = "EMP";
    _root.missile[currentmissile][9] = 3000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 60;
    _root.missile[currentmissile][1] = 60;
    _root.missile[currentmissile][2] = 12000;
    _root.missile[currentmissile][3] = 7000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 6000;
    _root.missile[currentmissile][6] = "EMP 2";
    _root.missile[currentmissile][7] = "EMP 2";
    _root.missile[currentmissile][8] = "EMP";
    _root.missile[currentmissile][9] = 6000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 50;
    _root.missile[currentmissile][1] = 53;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 9000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 8000;
    _root.missile[currentmissile][6] = "EMP 3";
    _root.missile[currentmissile][7] = "EMP 3";
    _root.missile[currentmissile][8] = "EMP";
    _root.missile[currentmissile][9] = 10000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 60;
    _root.missile[currentmissile][1] = 55;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 9000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 8000;
    _root.missile[currentmissile][6] = "Disrupter 1";
    _root.missile[currentmissile][7] = "Disrupter 1";
    _root.missile[currentmissile][8] = "DISRUPTER";
    _root.missile[currentmissile][9] = 2000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 50;
    _root.missile[currentmissile][1] = 50;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 11000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 14000;
    _root.missile[currentmissile][6] = "Disrupter 2";
    _root.missile[currentmissile][7] = "Disrupter 2";
    _root.missile[currentmissile][8] = "DISRUPTER";
    _root.missile[currentmissile][9] = 4000;
    ++currentmissile;
    _root.missile[currentmissile] = new Array();
    _root.missile[currentmissile][0] = 45;
    _root.missile[currentmissile][1] = 45;
    _root.missile[currentmissile][2] = 10000;
    _root.missile[currentmissile][3] = 15000;
    _root.missile[currentmissile][4] = 0;
    _root.missile[currentmissile][5] = 20000;
    _root.missile[currentmissile][6] = "Disrupter 3";
    _root.missile[currentmissile][7] = "Disrupter 3";
    _root.missile[currentmissile][8] = "DISRUPTER";
    _root.missile[currentmissile][9] = 5500;
    ++currentmissile;
    _root.energygenerators = new Array();
    _root.energygenerators[0] = new Array();
    _root.energygenerators[0][0] = 100;
    _root.energygenerators[0][1] = "Level 1";
    _root.energygenerators[0][2] = 1000;
    _root.energygenerators[1] = new Array();
    _root.energygenerators[1][0] = 150;
    _root.energygenerators[1][1] = "Level 2";
    _root.energygenerators[1][2] = 1000;
    _root.energygenerators[2] = new Array();
    _root.energygenerators[2][0] = 200;
    _root.energygenerators[2][1] = "Level 3";
    _root.energygenerators[2][2] = 22000;
    _root.energygenerators[3] = new Array();
    _root.energygenerators[3][0] = 250;
    _root.energygenerators[3][1] = "Level 4";
    _root.energygenerators[3][2] = 35000;
    _root.energygenerators[4] = new Array();
    _root.energygenerators[4][0] = 300;
    _root.energygenerators[4][1] = "Level 5";
    _root.energygenerators[4][2] = 51000;
    _root.energygenerators[5] = new Array();
    _root.energygenerators[5][0] = 425;
    _root.energygenerators[5][1] = "Level 6";
    _root.energygenerators[5][2] = 159000;
    _root.energygenerators[6] = new Array();
    _root.energygenerators[6][0] = 540;
    _root.energygenerators[6][1] = "Level 7";
    _root.energygenerators[6][2] = 335500;
    _root.energygenerators[7] = new Array();
    _root.energygenerators[7][0] = 690;
    _root.energygenerators[7][1] = "Level 8";
    _root.energygenerators[7][2] = 535500;
    _root.energygenerators[8] = new Array();
    _root.energygenerators[8][0] = 810;
    _root.energygenerators[8][1] = "Level 9";
    _root.energygenerators[8][2] = 735500;
    _root.energygenerators[9] = new Array();
    _root.energygenerators[9][0] = 970;
    _root.energygenerators[9][1] = "Level 10";
    _root.energygenerators[9][2] = 935500;
    _root.energygenerators[10] = new Array();
    _root.energygenerators[10][0] = 1155;
    _root.energygenerators[10][1] = "Level 11";
    _root.energygenerators[10][2] = 935500;
    _root.energycapacitors = new Array();
    _root.energycapacitors[0] = new Array();
    _root.energycapacitors[0][0] = 200;
    _root.energycapacitors[0][1] = "Level 1";
    _root.energycapacitors[0][2] = 1500;
    _root.energycapacitors[1] = new Array();
    _root.energycapacitors[1][0] = 300;
    _root.energycapacitors[1][1] = "Level 2";
    _root.energycapacitors[1][2] = 6000;
    _root.energycapacitors[2] = new Array();
    _root.energycapacitors[2][0] = 400;
    _root.energycapacitors[2][1] = "Level 3";
    _root.energycapacitors[2][2] = 13000;
    _root.energycapacitors[3] = new Array();
    _root.energycapacitors[3][0] = 500;
    _root.energycapacitors[3][1] = "Level 4";
    _root.energycapacitors[3][2] = 21000;
    _root.energycapacitors[4] = new Array();
    _root.energycapacitors[4][0] = 700;
    _root.energycapacitors[4][1] = "Level 5";
    _root.energycapacitors[4][2] = 83000;
    _root.energycapacitors[5] = new Array();
    _root.energycapacitors[5][0] = 900;
    _root.energycapacitors[5][1] = "Level 6";
    _root.energycapacitors[5][2] = 235000;
    _root.energycapacitors[6] = new Array();
    _root.energycapacitors[6][0] = 1150;
    _root.energycapacitors[6][1] = "Level 7";
    _root.energycapacitors[6][2] = 385000;
    _root.energycapacitors[7] = new Array();
    _root.energycapacitors[7][0] = 1425;
    _root.energycapacitors[7][1] = "Level 8";
    _root.energycapacitors[7][2] = 535000;
    _root.energycapacitors[8] = new Array();
    _root.energycapacitors[8][0] = 1750;
    _root.energycapacitors[8][1] = "Level 9";
    _root.energycapacitors[8][2] = 755000;
    _root.energycapacitors[9] = new Array();
    _root.energycapacitors[9][0] = 2150;
    _root.energycapacitors[9][1] = "Level 10";
    _root.energycapacitors[9][2] = 1435000;
    _root.shieldgenerators = new Array();
    _root.shieldgenerators[0] = new Array();
    _root.shieldgenerators[0][0] = 1000;
    _root.shieldgenerators[0][1] = 50;
    _root.shieldgenerators[0][2] = 10;
    _root.shieldgenerators[0][3] = 10;
    _root.shieldgenerators[0][4] = "Level 1";
    _root.shieldgenerators[0][5] = 750;
    _root.shieldgenerators[1] = new Array();
    _root.shieldgenerators[1][0] = 1500;
    _root.shieldgenerators[1][1] = 75;
    _root.shieldgenerators[1][2] = 20;
    _root.shieldgenerators[1][3] = 50;
    _root.shieldgenerators[1][4] = "Level 2";
    _root.shieldgenerators[1][5] = 8000;
    _root.shieldgenerators[2] = new Array();
    _root.shieldgenerators[2][0] = 2000;
    _root.shieldgenerators[2][1] = 100;
    _root.shieldgenerators[2][2] = 30;
    _root.shieldgenerators[2][3] = 100;
    _root.shieldgenerators[2][4] = "Level 3";
    _root.shieldgenerators[2][5] = 25500;
    _root.shieldgenerators[3] = new Array();
    _root.shieldgenerators[3][0] = 2600;
    _root.shieldgenerators[3][1] = 125;
    _root.shieldgenerators[3][2] = 35;
    _root.shieldgenerators[3][3] = 125;
    _root.shieldgenerators[3][4] = "Level 4";
    _root.shieldgenerators[3][5] = 68500;
    _root.shieldgenerators[4] = new Array();
    _root.shieldgenerators[4][0] = 6700;
    _root.shieldgenerators[4][1] = 195;
    _root.shieldgenerators[4][2] = 50;
    _root.shieldgenerators[4][3] = 200;
    _root.shieldgenerators[4][4] = "Level 5";
    _root.shieldgenerators[4][5] = 140995;
    _root.shieldgenerators[5] = new Array();
    _root.shieldgenerators[5][0] = 11000;
    _root.shieldgenerators[5][1] = 275;
    _root.shieldgenerators[5][2] = 75;
    _root.shieldgenerators[5][3] = 300;
    _root.shieldgenerators[5][4] = "Level 6";
    _root.shieldgenerators[5][5] = 310000;
    _root.shieldgenerators[6] = new Array();
    _root.shieldgenerators[6][0] = 17000;
    _root.shieldgenerators[6][1] = 345;
    _root.shieldgenerators[6][2] = 150;
    _root.shieldgenerators[6][3] = 400;
    _root.shieldgenerators[6][4] = "Level 7";
    _root.shieldgenerators[6][5] = 620456;
    _root.shieldgenerators[7] = new Array();
    _root.shieldgenerators[7][0] = 28000;
    _root.shieldgenerators[7][1] = 425;
    _root.shieldgenerators[7][2] = 230;
    _root.shieldgenerators[7][3] = 500;
    _root.shieldgenerators[7][4] = "Level 8";
    _root.shieldgenerators[7][5] = 1511548;
    _root.shieldgenerators[8] = new Array();
    _root.shieldgenerators[8][0] = 41000;
    _root.shieldgenerators[8][1] = 520;
    _root.shieldgenerators[8][2] = 305;
    _root.shieldgenerators[8][3] = 600;
    _root.shieldgenerators[8][4] = "Level 9";
    _root.shieldgenerators[8][5] = 2811548;
    _root.guntype = new Array();
    _root.guntype[0] = new Array();
    _root.guntype[0][0] = 120;
    _root.guntype[0][1] = 2;
    _root.guntype[0][2] = 0.500000;
    _root.guntype[0][3] = 15;
    _root.guntype[0][4] = 150;
    _root.guntype[0][5] = 1000;
    _root.guntype[0][6] = "Laser";
    _root.guntype[1] = new Array();
    _root.guntype[1][0] = 120;
    _root.guntype[1][1] = 2;
    _root.guntype[1][2] = 0.600000;
    _root.guntype[1][3] = 40;
    _root.guntype[1][4] = 250;
    _root.guntype[1][5] = 8000;
    _root.guntype[1][6] = "Double Laser";
    _root.guntype[2] = new Array();
    _root.guntype[2][0] = 100;
    _root.guntype[2][1] = 3;
    _root.guntype[2][2] = 0.800000;
    _root.guntype[2][3] = 35;
    _root.guntype[2][4] = 325;
    _root.guntype[2][5] = 23500;
    _root.guntype[2][6] = "Mass Driver";
    _root.guntype[3] = new Array();
    _root.guntype[3][0] = 95;
    _root.guntype[3][1] = 3;
    _root.guntype[3][2] = 0.800000;
    _root.guntype[3][3] = 50;
    _root.guntype[3][4] = 425;
    _root.guntype[3][5] = 48000;
    _root.guntype[3][6] = "Neutron Blaster";
    _root.guntype[4] = new Array();
    _root.guntype[4][0] = 75;
    _root.guntype[4][1] = 4;
    _root.guntype[4][2] = 1;
    _root.guntype[4][3] = 70;
    _root.guntype[4][4] = 650;
    _root.guntype[4][5] = 95000;
    _root.guntype[4][6] = "Plasma Cannon";
    _root.guntype[5] = new Array();
    _root.guntype[5][0] = 95;
    _root.guntype[5][1] = 3.500000;
    _root.guntype[5][2] = 1.500000;
    _root.guntype[5][3] = 120;
    _root.guntype[5][4] = 895;
    _root.guntype[5][5] = 195000;
    _root.guntype[5][6] = "Phasor";
    gunno = 6;
    _root.guntype[gunno] = new Array();
    _root.guntype[gunno][0] = 85;
    _root.guntype[gunno][1] = 4.500000;
    _root.guntype[gunno][2] = 1.100000;
    _root.guntype[gunno][3] = 170;
    _root.guntype[gunno][4] = 1150;
    _root.guntype[gunno][5] = 510000;
    _root.guntype[gunno][6] = "Neutron Pulse";
    _root.totalsmallships = 16;
    _root.shiptype = new Array();
    currentship = 0;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Human Scout";
    _root.shiptype[currentship][1] = 75000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 9;
    _root.shiptype[currentship][2][0][1] = -5;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -10;
    _root.shiptype[currentship][2][1][1] = -5;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 40;
    _root.shiptype[currentship][3][1] = 95;
    _root.shiptype[currentship][3][2] = 90;
    _root.shiptype[currentship][3][3] = 1000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 115;
    _root.shiptype[currentship][3][7] = 3;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 40;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 2;
    _root.shiptype[currentship][6][1] = 2;
    _root.shiptype[currentship][6][2] = 3;
    _root.shiptype[currentship][6][3] = 3;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -5;
    _root.shiptype[currentship][7][0][5] = 6;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Human Frigate";
    _root.shiptype[currentship][1] = 250000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 7;
    _root.shiptype[currentship][2][0][1] = -3;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -1;
    _root.shiptype[currentship][2][1][1] = -7;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -8;
    _root.shiptype[currentship][2][2][1] = -3;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 40;
    _root.shiptype[currentship][3][1] = 90;
    _root.shiptype[currentship][3][2] = 90;
    _root.shiptype[currentship][3][3] = 1500;
    _root.shiptype[currentship][3][4] = 12;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 110;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 70;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 3;
    _root.shiptype[currentship][6][1] = 3;
    _root.shiptype[currentship][6][2] = 4;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 5;
    _root.shiptype[currentship][7][0][3] = 0;
    _root.shiptype[currentship][7][0][5] = 6;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -5;
    _root.shiptype[currentship][7][1][3] = 0;
    _root.shiptype[currentship][7][1][5] = 6;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Light Freighter";
    _root.shiptype[currentship][1] = 450000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 10;
    _root.shiptype[currentship][2][0][1] = -2;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -10;
    _root.shiptype[currentship][2][1][1] = -2;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 35;
    _root.shiptype[currentship][3][1] = 80;
    _root.shiptype[currentship][3][2] = 75;
    _root.shiptype[currentship][3][3] = 5000;
    _root.shiptype[currentship][3][4] = 11;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 95;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 250;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = 8;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -5;
    _root.shiptype[currentship][7][0][5] = 12;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Human Hornet";
    _root.shiptype[currentship][1] = 1250000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 14;
    _root.shiptype[currentship][2][0][1] = 0;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 7;
    _root.shiptype[currentship][2][1][1] = -2;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -7;
    _root.shiptype[currentship][2][2][1] = -2;
    _root.shiptype[currentship][2][3] = new Array();
    _root.shiptype[currentship][2][3][0] = -14;
    _root.shiptype[currentship][2][3][1] = 0;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 38;
    _root.shiptype[currentship][3][1] = 83;
    _root.shiptype[currentship][3][2] = 105;
    _root.shiptype[currentship][3][3] = 7500;
    _root.shiptype[currentship][3][4] = 9;
    _root.shiptype[currentship][3][5] = 0.250000;
    _root.shiptype[currentship][3][6] = 98;
    _root.shiptype[currentship][3][7] = 7;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 50;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 7;
    _root.shiptype[currentship][7][0][3] = -2;
    _root.shiptype[currentship][7][0][5] = 10;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -7;
    _root.shiptype[currentship][7][1][3] = -2;
    _root.shiptype[currentship][7][1][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Tuskan Scout";
    _root.shiptype[currentship][1] = 70000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 5;
    _root.shiptype[currentship][2][0][1] = -10;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -5;
    _root.shiptype[currentship][2][1][1] = -10;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 50;
    _root.shiptype[currentship][3][1] = 95;
    _root.shiptype[currentship][3][2] = 85;
    _root.shiptype[currentship][3][3] = 1500;
    _root.shiptype[currentship][3][4] = 9;
    _root.shiptype[currentship][3][5] = 0.250000;
    _root.shiptype[currentship][3][6] = 105;
    _root.shiptype[currentship][3][7] = 2;
    _root.shiptype[currentship][3][8] = true;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 30;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 2;
    _root.shiptype[currentship][6][1] = 4;
    _root.shiptype[currentship][6][2] = 4;
    _root.shiptype[currentship][6][3] = 3;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -3;
    _root.shiptype[currentship][7][0][5] = 5;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Tuskan Freighter";
    _root.shiptype[currentship][1] = 950000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 0;
    _root.shiptype[currentship][2][0][1] = -10;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 25;
    _root.shiptype[currentship][3][1] = 78;
    _root.shiptype[currentship][3][2] = 70;
    _root.shiptype[currentship][3][3] = 5000;
    _root.shiptype[currentship][3][4] = 11;
    _root.shiptype[currentship][3][5] = 0.500000;
    _root.shiptype[currentship][3][6] = 93;
    _root.shiptype[currentship][3][7] = 4;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 400;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -10;
    _root.shiptype[currentship][5][0][1] = 3;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 10;
    _root.shiptype[currentship][5][1][1] = 3;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -3;
    _root.shiptype[currentship][7][0][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Tuskan Interceptor";
    _root.shiptype[currentship][1] = 600000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 11;
    _root.shiptype[currentship][2][0][1] = -9;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 6;
    _root.shiptype[currentship][2][1][1] = -11;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -6;
    _root.shiptype[currentship][2][2][1] = -11;
    _root.shiptype[currentship][2][3] = new Array();
    _root.shiptype[currentship][2][3][0] = -11;
    _root.shiptype[currentship][2][3][1] = -9;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 38;
    _root.shiptype[currentship][3][1] = 85;
    _root.shiptype[currentship][3][2] = 85;
    _root.shiptype[currentship][3][3] = 3500;
    _root.shiptype[currentship][3][4] = 9;
    _root.shiptype[currentship][3][5] = 0.250000;
    _root.shiptype[currentship][3][6] = 103;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 50;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 5;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 11;
    _root.shiptype[currentship][7][0][3] = -9;
    _root.shiptype[currentship][7][0][5] = 8;
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = -11;
    _root.shiptype[currentship][7][0][3] = -9;
    _root.shiptype[currentship][7][0][5] = 8;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Tuskan Avenger";
    _root.shiptype[currentship][1] = 1955000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 7;
    _root.shiptype[currentship][2][0][1] = -18;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -7;
    _root.shiptype[currentship][2][1][1] = -18;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 33;
    _root.shiptype[currentship][3][1] = 75;
    _root.shiptype[currentship][3][2] = 70;
    _root.shiptype[currentship][3][3] = 21000;
    _root.shiptype[currentship][3][4] = 6;
    _root.shiptype[currentship][3][5] = 0.450000;
    _root.shiptype[currentship][3][6] = 88;
    _root.shiptype[currentship][3][7] = 10;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 150;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -13;
    _root.shiptype[currentship][5][0][1] = 0;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 13;
    _root.shiptype[currentship][5][1][1] = 0;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 7;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -3;
    _root.shiptype[currentship][7][0][5] = 10;
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = -12;
    _root.shiptype[currentship][7][0][3] = 0;
    _root.shiptype[currentship][7][0][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Feril Raider";
    _root.shiptype[currentship][1] = 140000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 11;
    _root.shiptype[currentship][2][0][1] = 0;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -11;
    _root.shiptype[currentship][2][1][1] = 0;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 60;
    _root.shiptype[currentship][3][1] = 90;
    _root.shiptype[currentship][3][2] = 90;
    _root.shiptype[currentship][3][3] = 2500;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.150000;
    _root.shiptype[currentship][3][6] = 102;
    _root.shiptype[currentship][3][7] = 3;
    _root.shiptype[currentship][3][8] = true;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 30;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 2;
    _root.shiptype[currentship][6][1] = 4;
    _root.shiptype[currentship][6][2] = 4;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 8;
    _root.shiptype[currentship][7][0][3] = 5;
    _root.shiptype[currentship][7][0][5] = 3;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -8;
    _root.shiptype[currentship][7][1][3] = 5;
    _root.shiptype[currentship][7][1][5] = 3;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Feril Freighter";
    _root.shiptype[currentship][1] = 2250000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 7;
    _root.shiptype[currentship][2][0][1] = -11;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -7;
    _root.shiptype[currentship][2][1][1] = -11;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 30;
    _root.shiptype[currentship][3][1] = 73;
    _root.shiptype[currentship][3][2] = 65;
    _root.shiptype[currentship][3][3] = 6000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.500000;
    _root.shiptype[currentship][3][6] = 87;
    _root.shiptype[currentship][3][7] = 6;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 1000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -12;
    _root.shiptype[currentship][5][0][1] = 10;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 12;
    _root.shiptype[currentship][5][1][1] = 10;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 5;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -11;
    _root.shiptype[currentship][7][0][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Feril Mirauder";
    _root.shiptype[currentship][1] = 1650000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 14;
    _root.shiptype[currentship][2][0][1] = -2;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -14;
    _root.shiptype[currentship][2][1][1] = -2;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 50;
    _root.shiptype[currentship][3][1] = 85;
    _root.shiptype[currentship][3][2] = 70;
    _root.shiptype[currentship][3][3] = 5500;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.200000;
    _root.shiptype[currentship][3][6] = 97;
    _root.shiptype[currentship][3][7] = 6;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 175;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -8;
    _root.shiptype[currentship][5][0][1] = 4;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 8;
    _root.shiptype[currentship][5][1][1] = 4;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 5;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 8;
    _root.shiptype[currentship][7][0][3] = 4;
    _root.shiptype[currentship][7][0][5] = 6;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -8;
    _root.shiptype[currentship][7][1][3] = 4;
    _root.shiptype[currentship][7][1][5] = 6;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Feril Assaulter";
    _root.shiptype[currentship][1] = 2750000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 14;
    _root.shiptype[currentship][2][0][1] = 4;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 9;
    _root.shiptype[currentship][2][1][1] = -7;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -9;
    _root.shiptype[currentship][2][2][1] = -7;
    _root.shiptype[currentship][2][3] = new Array();
    _root.shiptype[currentship][2][3][0] = -14;
    _root.shiptype[currentship][2][3][1] = 4;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 40;
    _root.shiptype[currentship][3][1] = 86;
    _root.shiptype[currentship][3][2] = 60;
    _root.shiptype[currentship][3][3] = 5500;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.320000;
    _root.shiptype[currentship][3][6] = 103;
    _root.shiptype[currentship][3][7] = 8;
    _root.shiptype[currentship][3][8] = true;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 200;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 5;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 10;
    _root.shiptype[currentship][7][0][3] = 5;
    _root.shiptype[currentship][7][0][5] = 10;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -10;
    _root.shiptype[currentship][7][1][3] = 5;
    _root.shiptype[currentship][7][1][5] = 10;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Alkari Runner";
    _root.shiptype[currentship][1] = 795000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 0;
    _root.shiptype[currentship][2][0][1] = -10;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 60;
    _root.shiptype[currentship][3][1] = 115;
    _root.shiptype[currentship][3][2] = 80;
    _root.shiptype[currentship][3][3] = 2000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 130;
    _root.shiptype[currentship][3][7] = 2;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 30;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = 7;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 2;
    _root.shiptype[currentship][6][1] = 4;
    _root.shiptype[currentship][6][2] = 3;
    _root.shiptype[currentship][6][3] = 4;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 12;
    _root.shiptype[currentship][7][0][3] = 0;
    _root.shiptype[currentship][7][0][5] = 4;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -12;
    _root.shiptype[currentship][7][1][3] = 0;
    _root.shiptype[currentship][7][1][5] = 4;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Alkari Hauler";
    _root.shiptype[currentship][1] = 3595000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 30;
    _root.shiptype[currentship][3][1] = 65;
    _root.shiptype[currentship][3][2] = 45;
    _root.shiptype[currentship][3][3] = 12000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.550000;
    _root.shiptype[currentship][3][6] = 87;
    _root.shiptype[currentship][3][7] = 4;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 1500;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 15;
    _root.shiptype[currentship][5][0][1] = 4;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 0;
    _root.shiptype[currentship][5][1][1] = -8;
    _root.shiptype[currentship][5][2] = new Array();
    _root.shiptype[currentship][5][2][0] = -15;
    _root.shiptype[currentship][5][2][1] = 4;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 6;
    _root.shiptype[currentship][6][2] = 7;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 12;
    _root.shiptype[currentship][7][0][3] = 0;
    _root.shiptype[currentship][7][0][5] = 4;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -12;
    _root.shiptype[currentship][7][1][3] = 0;
    _root.shiptype[currentship][7][1][5] = 4;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Alkari Chaser";
    _root.shiptype[currentship][1] = 2795000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 8;
    _root.shiptype[currentship][2][0][1] = 0;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 0;
    _root.shiptype[currentship][2][1][1] = -12;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -8;
    _root.shiptype[currentship][2][2][1] = 0;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 80;
    _root.shiptype[currentship][3][1] = 100;
    _root.shiptype[currentship][3][2] = 75;
    _root.shiptype[currentship][3][3] = 7000;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.300000;
    _root.shiptype[currentship][3][6] = 110;
    _root.shiptype[currentship][3][7] = 4;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 150;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = 10;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 6;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 12;
    _root.shiptype[currentship][7][0][3] = 3;
    _root.shiptype[currentship][7][0][5] = 8;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -12;
    _root.shiptype[currentship][7][1][3] = 3;
    _root.shiptype[currentship][7][1][5] = 8;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Alkari DeathWing";
    _root.shiptype[currentship][1] = 4595000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 10;
    _root.shiptype[currentship][2][0][1] = -11;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 0;
    _root.shiptype[currentship][2][1][1] = -18;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -10;
    _root.shiptype[currentship][2][2][1] = -11;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 30;
    _root.shiptype[currentship][3][1] = 75;
    _root.shiptype[currentship][3][2] = 60;
    _root.shiptype[currentship][3][3] = 10500;
    _root.shiptype[currentship][3][4] = 10;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 87;
    _root.shiptype[currentship][3][7] = 8;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = false;
    _root.shiptype[currentship][4] = 200;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 4;
    _root.shiptype[currentship][6][1] = 6;
    _root.shiptype[currentship][6][2] = 6;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 14;
    _root.shiptype[currentship][7][0][3] = -3;
    _root.shiptype[currentship][7][0][5] = 6;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = 7;
    _root.shiptype[currentship][7][1][3] = 0;
    _root.shiptype[currentship][7][1][5] = 6;
    _root.shiptype[currentship][7][2] = new Array();
    _root.shiptype[currentship][7][2][2] = -7;
    _root.shiptype[currentship][7][2][3] = 0;
    _root.shiptype[currentship][7][2][5] = 6;
    _root.shiptype[currentship][7][3] = new Array();
    _root.shiptype[currentship][7][3][2] = -14;
    _root.shiptype[currentship][7][3][3] = -3;
    _root.shiptype[currentship][7][3][5] = 6;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Large Freighter";
    _root.shiptype[currentship][1] = 8000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 20;
    _root.shiptype[currentship][3][1] = 85;
    _root.shiptype[currentship][3][2] = 50;
    _root.shiptype[currentship][3][3] = 30000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.500000;
    _root.shiptype[currentship][3][6] = 88;
    _root.shiptype[currentship][3][7] = 2;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 5000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = -12.700000;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = -3.500000;
    _root.shiptype[currentship][5][1][1] = 20;
    _root.shiptype[currentship][5][2] = new Array();
    _root.shiptype[currentship][5][2][0] = 3.500000;
    _root.shiptype[currentship][5][2][1] = 20;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 7;
    _root.shiptype[currentship][6][2] = 8;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 0;
    _root.shiptype[currentship][7][0][3] = -20;
    _root.shiptype[currentship][7][0][5] = 30;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Super Freighter";
    _root.shiptype[currentship][1] = 52000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 15;
    _root.shiptype[currentship][3][1] = 75;
    _root.shiptype[currentship][3][2] = 40;
    _root.shiptype[currentship][3][3] = 68000;
    _root.shiptype[currentship][3][4] = 3;
    _root.shiptype[currentship][3][5] = 0.650000;
    _root.shiptype[currentship][3][6] = 82;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 14000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = -9.500000;
    _root.shiptype[currentship][5][0][1] = -32;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 9.500000;
    _root.shiptype[currentship][5][1][1] = -32;
    _root.shiptype[currentship][5][2] = new Array();
    _root.shiptype[currentship][5][2][0] = -10;
    _root.shiptype[currentship][5][2][1] = 27;
    _root.shiptype[currentship][5][3] = new Array();
    _root.shiptype[currentship][5][3][0] = 10;
    _root.shiptype[currentship][5][3][1] = 27;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 8;
    _root.shiptype[currentship][6][1] = 8;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = -9.500000;
    _root.shiptype[currentship][7][0][3] = -32;
    _root.shiptype[currentship][7][0][5] = 30;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = 9.500000;
    _root.shiptype[currentship][7][1][3] = -32;
    _root.shiptype[currentship][7][1][5] = 30;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Armed Freighter";
    _root.shiptype[currentship][1] = 26000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 16.500000;
    _root.shiptype[currentship][2][0][1] = -20;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -16.500000;
    _root.shiptype[currentship][2][1][1] = -20;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 25;
    _root.shiptype[currentship][3][1] = 80;
    _root.shiptype[currentship][3][2] = 50;
    _root.shiptype[currentship][3][3] = 43000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.450000;
    _root.shiptype[currentship][3][6] = 88;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 7000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = -2.500000;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 0;
    _root.shiptype[currentship][5][1][1] = 18;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 7;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 5;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 20;
    _root.shiptype[currentship][7][0][3] = -10;
    _root.shiptype[currentship][7][0][5] = 30;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -20;
    _root.shiptype[currentship][7][1][3] = -10;
    _root.shiptype[currentship][7][1][5] = 30;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Light Destroyer";
    _root.shiptype[currentship][1] = 19000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 16;
    _root.shiptype[currentship][2][0][1] = -15;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -16;
    _root.shiptype[currentship][2][1][1] = -15;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 25;
    _root.shiptype[currentship][3][1] = 75;
    _root.shiptype[currentship][3][2] = 60;
    _root.shiptype[currentship][3][3] = 41000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.350000;
    _root.shiptype[currentship][3][6] = 88;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 2000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = -2;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 0;
    _root.shiptype[currentship][5][1][1] = 14;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 5;
    _root.shiptype[currentship][6][1] = 8;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 10;
    _root.shiptype[currentship][7][0][3] = -10;
    _root.shiptype[currentship][7][0][5] = 15;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -10;
    _root.shiptype[currentship][7][1][3] = -10;
    _root.shiptype[currentship][7][1][5] = 15;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Missile Destroyer";
    _root.shiptype[currentship][1] = 26000000;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 25;
    _root.shiptype[currentship][3][1] = 73;
    _root.shiptype[currentship][3][2] = 70;
    _root.shiptype[currentship][3][3] = 38000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.300000;
    _root.shiptype[currentship][3][6] = 85;
    _root.shiptype[currentship][3][7] = 5;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 1500;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 0;
    _root.shiptype[currentship][5][0][1] = -2;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = 0;
    _root.shiptype[currentship][5][1][1] = 14;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 7;
    _root.shiptype[currentship][6][1] = 8;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 15;
    _root.shiptype[currentship][7][0][3] = -14;
    _root.shiptype[currentship][7][0][5] = 50;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -15;
    _root.shiptype[currentship][7][1][3] = -14;
    _root.shiptype[currentship][7][1][5] = 50;
    _root.shiptype[currentship][7][2] = new Array();
    _root.shiptype[currentship][7][2][2] = 21;
    _root.shiptype[currentship][7][2][3] = -14;
    _root.shiptype[currentship][7][2][5] = 50;
    _root.shiptype[currentship][7][3] = new Array();
    _root.shiptype[currentship][7][3][2] = -21;
    _root.shiptype[currentship][7][3][3] = -10;
    _root.shiptype[currentship][7][3][5] = 50;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Bird of Prey";
    _root.shiptype[currentship][1] = 63000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 18;
    _root.shiptype[currentship][2][0][1] = -6;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = 11;
    _root.shiptype[currentship][2][1][1] = -17;
    _root.shiptype[currentship][2][2] = new Array();
    _root.shiptype[currentship][2][2][0] = -11;
    _root.shiptype[currentship][2][2][1] = -17;
    _root.shiptype[currentship][2][3] = new Array();
    _root.shiptype[currentship][2][3][0] = -18;
    _root.shiptype[currentship][2][3][1] = -6;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 15;
    _root.shiptype[currentship][3][1] = 70;
    _root.shiptype[currentship][3][2] = 65;
    _root.shiptype[currentship][3][3] = 45000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.500000;
    _root.shiptype[currentship][3][6] = 110;
    _root.shiptype[currentship][3][7] = 4;
    _root.shiptype[currentship][3][8] = true;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 100;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 7;
    _root.shiptype[currentship][6][1] = 8;
    _root.shiptype[currentship][6][2] = 9;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = -25;
    _root.shiptype[currentship][7][0][3] = 10;
    _root.shiptype[currentship][7][0][5] = 15;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = 25;
    _root.shiptype[currentship][7][1][3] = 10;
    _root.shiptype[currentship][7][1][5] = 15;
    _root.shiptype[currentship][7][2] = new Array();
    _root.shiptype[currentship][7][2][2] = -14;
    _root.shiptype[currentship][7][2][3] = 7;
    _root.shiptype[currentship][7][2][5] = 15;
    _root.shiptype[currentship][7][3] = new Array();
    _root.shiptype[currentship][7][3][2] = 14;
    _root.shiptype[currentship][7][3][3] = 7;
    _root.shiptype[currentship][7][3][5] = 15;
    ++currentship;
    ++currentship;
    ++currentship;
    ++currentship;
    _root.shiptype[currentship] = new Array();
    _root.shiptype[currentship][0] = "Desolator";
    _root.shiptype[currentship][1] = 145000000;
    _root.shiptype[currentship][2] = new Array();
    _root.shiptype[currentship][2][0] = new Array();
    _root.shiptype[currentship][2][0][0] = 14;
    _root.shiptype[currentship][2][0][1] = -30;
    _root.shiptype[currentship][2][1] = new Array();
    _root.shiptype[currentship][2][1][0] = -14;
    _root.shiptype[currentship][2][1][1] = -30;
    _root.shiptype[currentship][3] = new Array();
    _root.shiptype[currentship][3][0] = 10;
    _root.shiptype[currentship][3][1] = 62;
    _root.shiptype[currentship][3][2] = 45;
    _root.shiptype[currentship][3][3] = 165000;
    _root.shiptype[currentship][3][4] = 5;
    _root.shiptype[currentship][3][5] = 0.600000;
    _root.shiptype[currentship][3][6] = 73;
    _root.shiptype[currentship][3][7] = 10;
    _root.shiptype[currentship][3][8] = false;
    _root.shiptype[currentship][3][10] = true;
    _root.shiptype[currentship][4] = 5000;
    _root.shiptype[currentship][5] = new Array();
    _root.shiptype[currentship][5][0] = new Array();
    _root.shiptype[currentship][5][0][0] = 23;
    _root.shiptype[currentship][5][0][1] = -8;
    _root.shiptype[currentship][5][1] = new Array();
    _root.shiptype[currentship][5][1][0] = -23;
    _root.shiptype[currentship][5][1][1] = -8;
    _root.shiptype[currentship][5][2] = new Array();
    _root.shiptype[currentship][5][2][0] = 25;
    _root.shiptype[currentship][5][2][1] = 20;
    _root.shiptype[currentship][5][3] = new Array();
    _root.shiptype[currentship][5][3][0] = -25;
    _root.shiptype[currentship][5][3][1] = 20;
    _root.shiptype[currentship][6] = new Array();
    _root.shiptype[currentship][6][0] = 8;
    _root.shiptype[currentship][6][1] = 9;
    _root.shiptype[currentship][6][2] = 10;
    _root.shiptype[currentship][6][3] = 6;
    _root.shiptype[currentship][7] = new Array();
    _root.shiptype[currentship][7][0] = new Array();
    _root.shiptype[currentship][7][0][2] = 7;
    _root.shiptype[currentship][7][0][3] = -10;
    _root.shiptype[currentship][7][0][5] = 40;
    _root.shiptype[currentship][7][1] = new Array();
    _root.shiptype[currentship][7][1][2] = -7;
    _root.shiptype[currentship][7][1][3] = -10;
    _root.shiptype[currentship][7][1][5] = 40;
    _root.shiptype[currentship][7][2] = new Array();
    _root.shiptype[currentship][7][2][2] = 7;
    _root.shiptype[currentship][7][2][3] = 6;
    _root.shiptype[currentship][7][2][5] = 40;
    _root.shiptype[currentship][7][3] = new Array();
    _root.shiptype[currentship][7][3][2] = -7;
    _root.shiptype[currentship][7][3][3] = 6;
    _root.shiptype[currentship][7][3][5] = 40;
    _root.tradegoods = new Array();
    _root.tradegoods[0] = new Array();
    _root.tradegoods[0][0] = "Food";
    _root.tradegoods[1] = new Array();
    _root.tradegoods[1][0] = "Wood";
    _root.tradegoods[2] = new Array();
    _root.tradegoods[2][0] = "Iron";
    _root.tradegoods[3] = new Array();
    _root.tradegoods[3][0] = "Machinery";
    _root.tradegoods[4] = new Array();
    _root.tradegoods[4][0] = "Tools";
    _root.tradegoods[5] = new Array();
    _root.tradegoods[5][0] = "Spare Parts";
    _root.tradegoods[6] = new Array();
    _root.tradegoods[6][0] = "Liquor";
    for (i = 0; i < _root.tradegoods.length; i++)
    {
        _root.playershipstatus[4][1][i] = 0;
    } // end of for
    _root.playernameinput = "Enter Name Here";
    _root.gamechatinfo = new Array();
    _root.gamechatinfo[0] = new Array();
    _root.gamechatinfo[0][0] = "";
    _root.gamechatinfo[1] = new Array();
    _root.gamechatinfo[1][0] = new Array();
    _root.gamechatinfo[1][0][0] = "Press enter to type and send a message, F12 for help";
    _root.gamechatinfo[1][0][1] = _root.systemchattextcolor;
    _root.gamechatinfo[1][1] = new Array();
    _root.gamechatinfo[1][1][0] = "Hello,Press D to dock in game, \'HOME\' to extend text";
    _root.gamechatinfo[1][1][1] = _root.systemchattextcolor;
    _root.gamechatinfo[2] = new Array();
    _root.gamechatinfo[2][0] = 5;
    _root.gamechatinfo[2][1] = 17;
    _root.gamechatinfo[2][2] = false;
    _root.gamechatinfo[2][3] = 0;
    _root.gamechatinfo[2][4] = 300000;
    _root.gamechatinfo[3] = new Array();
    _root.gamechatinfo[3][0] = "";
    _root.gamechatinfo[3][1] = false;
    _root.gamechatinfo[4] = false;
    _root.gamechatinfo[5] = "";
    _root.gamechatinfo[6] = new Array();
    _root.gamechatinfo[7] = new Array();
    _root.gamechatinfo[7][0] = new Array();
    _root.gamechatinfo[7][0][0] = -50000;
    _root.gamechatinfo[7][0][1] = -40000;
    _root.gamechatinfo[7][0][2] = -30000;
    _root.gamechatinfo[7][0][3] = -20000;
    _root.gamechatinfo[7][1] = 3000;
    _root.gamechatinfo[7][2] = 10000;
    _root.gameplaystatus = new Array();
    _root.gameplaystatus[0] = new Array();
    _root.gameplaystatus[0][0] = 4;
    _root.gameplaystatus[1] = new Array();
    _root.gameplaystatus[1][0] = 0;
    _root.gameplaystatus[1][1] = 0;
    _root.gameplaystatus[1][2] = 0;
    _root.gameplaystatus[1][3] = 0;
    _root.framestojumpgunfire = _root.gameplaystatus[0][0] + 1;
    _root.currentonlineplayers = new Array();
    _root.currentgamestatus = "";
    _root.aimessages = new Array();
    _root.aimessages[0] = new Array();
    _root.aimessages[0][0] = "Hey Baby, Say Hi to Elvis for me!!!!";
    _root.aimessages[0][1] = "This won\'t hurt a bit, I promise!!!";
    _root.aimessages[0][2] = "I hope you can put up a challenge, The others were boring";
    _root.aimessages[0][3] = "I hope you don\'t whine as much as my last victim";
    _root.aimessages[0][4] = "You look kind of cute, for a dead man";
    _root.aimessages[0][5] = "I almost feel sorry for you, oh well, EAT SOME OF THIS!!";
    _root.aimessages[0][6] = "I wonder if space is cold as they say it is. When you explode, could you tell me?";
    _root.aimessages[0][7] = "Your death will mean so much to me, It will get me into the clan.";
    _root.aimessages[0][8] = "I already have enough friends, So I can just finish you off";
    _root.aimessages[1] = new Array();
    _root.aimessages[1][0] = "WHAT THE?? How did you pull this one off?!?!!?";
    _root.aimessages[1][1] = "AWWWW CRAP! I only had 10 more payments left on this baby";
    _root.aimessages[1][2] = "Darn, looks like you know what your doing... AHHHHHHHH!!!!!";
    _root.aimessages[1][3] = "I COMING ELVIS, IM COMING!";
    _root.aimessages[1][4] = "Well, I guess another one bites the dust";
    _root.aimessages[1][5] = "Couldn\'t you have taken it easy on me??";
    _root.aimessages[1][6] = "Good Game Son,  Good Game!";
    _root.aimessages[1][7] = "Nice fight, Looks like you are the better pilot";
    _root.aimessages[1][8] = "Couldn\'t we have just been friends?";
    _root.ainames = new Array();
    _root.ainames[0] = "Vader";
    _root.ainames[1] = "Scorpio";
    _root.ainames[2] = "Kirk";
    _root.ainames[3] = "Maniac";
    _root.ainames[4] = "Valerie";
    _root.ainames[5] = "Kitty";
    _root.ainames[6] = "Charlie";
    _root.ainames[7] = "Borris";
    _root.ainames[8] = "Melissa";
    _root.ainames[9] = "Borris";
    _root.ainames[10] = "The Evil One";
    _root.currentailvl = 0;
    _root.targetinfo = new Array();
    _root.targetinfo[0] = "None";
    _root.targetinfo[1] = 0;
    _root.targetinfo[2] = 0;
    _root.targetinfo[3] = 0;
    _root.targetinfo[4] = 0;
    _root.targetinfo[5] = 0;
    _root.targetinfo[6] = 0;
    _root.keywaspressed = false;
    _root.sectorinformation[0] = new Array();
    _root.sectorinformation[0][0] = 50;
    _root.sectorinformation[0][1] = 50;
    _root.sectorinformation[1] = new Array();
    _root.sectorinformation[1][0] = 1000;
    _root.sectorinformation[1][1] = 1000;
    _root.portandsystem = null;
    if (_root.isgamerunningfromremote == false)
    {
        newzoneVars = new XML();
        newzoneVars.load("getzone.php");
        newzoneVars.onLoad = function (success)
        {
            loadedvars = String(newzoneVars);
            if (loadedvars < 1)
            {
                loadedvars = 200;
            } // end if
            _root.portandsystem = Number(loadedvars);
        };
    } // end if
}

// [onClipEvent of sprite 1271 in frame 1]
onClipEvent (load)
{
    scenes = new Array();
    scenes[0] = 11000;
    scenes[1] = 5000;
    scenes[2] = 6000;
    scenes[3] = 3000;
    scenes[4] = 8000;
    scenes[5] = 3500;
    scenes[6] = 4000;
    scenes[7] = 6000;
    currentscene = 0;
    timetillnextscene = Number(getTimer()) + scenes[0];
}

// [onClipEvent of sprite 1271 in frame 1]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (timetillnextscene < currenttime)
    {
        ++currentscene;
        if (currentscene > scenes.length - 1)
        {
            currentscene = 0;
        } // end if
        timetillnextscene = currenttime + scenes[currentscene];
        scenes[0] = 15000;
    } // end if
}

// [onClipEvent of sprite 1271 in frame 1]
onClipEvent (load)
{
    function toptenprocess(toptennames)
    {
        newinfo = toptennames.split("~");
        i = 0;
        topten = "Top Ten Players:\r";
        while (i < newinfo.length - 1)
        {
            currentthread = newinfo[i].split("`");
            topten = topten + (i + 1 + ": " + currentthread[0] + " - " + Math.floor(Number(currentthread[1]) / _root.scoreratiomodifier) + "\r");
            ++i;
        } // end while
    } // End of the function
    topten = "Loading Top Ten";
    myLoadVars = new XML();
    myLoadVars.load(_root.pathtoaccounts + "accounts.php?mode=topten");
    myLoadVars.onLoad = function (success)
    {
        toptennames = String(myLoadVars);
        toptenprocess(toptennames);
    };
}

// [onClipEvent of sprite 1387 in frame 1]
onClipEvent (load)
{
    this._xscale = 70;
    this._yscale = 70;
}

// [onClipEvent of sprite 1389 in frame 1]
onClipEvent (load)
{
    function func_maplisterkey(incomingvariable)
    {
        if (_root.playershipstatus[5][1] != null)
        {
            keypressed = Key.getCode();
            if (incomingvariable == "buttonpress")
            {
                keypressed = "119";
            } // end if
            if (keypressed == "119")
            {
                if (_root.mapdsiplayed == false)
                {
                    _root.mapdsiplayed = true;
                    _root.attachMovie("ingamemap", "ingamemap", 9999999);
                }
                else
                {
                    _root.mapdsiplayed = false;
                    _root.createEmptyMovieClip("blanker", 9999999);
                } // end if
            } // end if
        } // end else if
    } // End of the function
    themapkeyListener = new Object();
    themapkeyListener.onKeyDown = function ()
    {
        func_maplisterkey();
    };
    Key.addListener(themapkeyListener);
}

// [onClipEvent of sprite 1392 in frame 1]
onClipEvent (load)
{
    function saveplayersgame(sender)
    {
        _root.func_checkfortoomuchaddedandset();
        _root.savegamesenderfrom = sender;
        if (savestatus == "Save Game")
        {
            savestatus = sender.savestatus = "Saving";
            variablestosave = "PI" + _root.playershipstatus[3][0] + "`ST" + _root.playershipstatus[5][0] + "`SG" + _root.playershipstatus[2][0] + "`EG" + _root.playershipstatus[1][0] + "`EC" + _root.playershipstatus[1][5] + "`CR" + _root.playershipstatus[3][1] + "`SE" + _root.playershipstatus[5][1] + "`" + _root.playershipstatus[4][0];
            for (currenthardpoint = 0; currenthardpoint < _root.playershipstatus[0].length; currenthardpoint++)
            {
                variablestosave = variablestosave + ("`HP" + currenthardpoint + "G" + _root.playershipstatus[0][currenthardpoint][0]);
            } // end of for
            for (currentturretpoint = 0; currentturretpoint < _root.playershipstatus[8].length; currentturretpoint++)
            {
                variablestosave = variablestosave + ("`TT" + currentturretpoint + "G" + _root.playershipstatus[8][currentturretpoint][0]);
            } // end of for
            for (currentcargo = 0; currentcargo < _root.playershipstatus[4][1].length; currentcargo++)
            {
                if (_root.playershipstatus[4][1][currentcargo] > 0)
                {
                    variablestosave = variablestosave + ("`CO" + currentcargo + "A" + _root.playershipstatus[4][1][currentcargo]);
                } // end if
            } // end of for
            for (spno = 0; spno < _root.playershipstatus[11][1].length; spno++)
            {
                variablestosave = variablestosave + ("`SP" + _root.playershipstatus[11][1][spno][0] + "Q" + _root.playershipstatus[11][1][spno][1]);
            } // end of for
            variablestosave = variablestosave + "~";
            if (_root.isplayeraguest == false)
            {
                _root.saveplayerextraships();
                if (_root.lastplayerssavedinfosent == "info=" + variablestosave + "&score=" + _root.playershipstatus[5][9] + "&shipsdata=" + _root.playersextrashipsdata)
                {
                    sender.savestatus = "No Updates";
                    savestatus = "Save Game";
                }
                else
                {
                    _root.lastplayerssavedinfosent = "info=" + variablestosave + "&score=" + _root.playershipstatus[5][9] + "&shipsdata=" + _root.playersextrashipsdata;
                    datatosend = "savegame`~:" + _root.playershipstatus[3][2] + ":" + _root.playershipstatus[3][3] + ":" + variablestosave + ":" + _root.playershipstatus[5][9] + ":" + _root.playersextrashipsdata + ":" + _root.playershipstatus[3][1] + ":" + _root.playershipstatus[5][19] + ":";
                    accountserv = new XMLSocket();
                    if (_root.isgamerunningfromremote == false)
                    {
                        currenturl = String(_root._url);
                        if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
                        {
                            accountserv.connect("", _root.accountserverport);
                        } // end if
                    }
                    else
                    {
                        accountserv.connect(_root.accountserveraddy, _root.accountserverport);
                    } // end else if
                    accountserv.onConnect = function (success)
                    {
                        okbutton._visible = true;
                        accountserv.send(datatosend);
                    };
                    accountserv.onXML = xmlhandler;
                } // end if
            } // end if
        } // end else if
    } // End of the function
    function xmlhandler(doc)
    {
        loadedvars = String(doc);
        info = loadedvars.split("~");
        newinfo = info[0].split("`");
        if (newinfo[0] == "savesuccess")
        {
            _root.savegamescript.savestatus = "Save Game";
            _root.savegamesenderfrom.savestatus = "Game Saved";
            _root.playershipstatus[5][19] = newinfo[1];
        }
        else
        {
            _root.savegamescript.savestatus = _root.savegamesenderfrom.savestatus = "Save Failed";
            _root.func_disconnectuser("savefailure");
        } // end else if
    } // End of the function
    this._visible = false;
    status = "N/A";
    savestatus = "Save Game";
}

// [onClipEvent of sprite 1407 in frame 10]
onClipEvent (load)
{
    function func_flagcontrols(method, xcoord, ycoord)
    {
        if (method == "ADD")
        {
            this.gamebackground.attachMovie("kingflaggameicon", "kflag", 8001);
            this.gamebackground.kflag._x = xcoord;
            this.gamebackground.kflag._y = ycoord;
        }
        else
        {
            removeMovieClip (this.gamebackground.kflag);
        } // end else if
    } // End of the function
    function func_drawneutralship(currentno)
    {
        globalshipname = _root.neutralships[currentno][10];
        this.attachMovie("shiptypeaineutral", globalshipname, 600 + _root.neutralships[currentno][13]);
        this[globalshipname].globalshipname = globalshipname;
        this[globalshipname]._x = -1000;
        this[globalshipname]._y = -1000;
    } // End of the function
    function func_removeneutralship(currentno)
    {
        globalshipname = _root.neutralships[currentno][10];
        removeMovieClip (this[globalshipname]);
    } // End of the function
    function func_meteordraw(method, meteorid)
    {
        if (method == "ADD")
        {
            ++_root.currentmeteordrawlevel;
            if (_root.currentmeteordrawlevel > _root.maxmeteordrawlevel)
            {
                _root.currentmeteordrawlevel = 0;
            } // end if
            meteorlvl = _root.currentmeteordrawlevel;
            location = null;
            for (qq = 0; qq < _root.incomingmeteor.length; qq++)
            {
                if (meteorid == _root.incomingmeteor[qq][0])
                {
                    location = qq;
                    break;
                } // end if
            } // end of for
            if (location != null)
            {
                meteortype = _root.incomingmeteor[location][7];
                velocity = _root.destroyingmeteor[meteortype][0];
                relativefacing = _root.incomingmeteor[location][3];
                movementofanobjectwiththrust();
                ymovement = -velocity * _root.cosines[this.relativefacing];
                xmovement = velocity * _root.sines[this.relativefacing];
                this.gamebackground.attachMovie("meteorscript", "meteor" + meteorid, 8050 + meteorlvl);
                this.gamebackground["meteor" + meteorid].meteorid = meteorid;
                this.gamebackground["meteor" + meteorid]._x = _root.incomingmeteor[location][1];
                this.gamebackground["meteor" + meteorid]._y = _root.incomingmeteor[location][2];
                this.gamebackground["meteor" + meteorid].xmovement = xmovement;
                this.gamebackground["meteor" + meteorid].ymovement = ymovement;
                this.gamebackground["meteor" + meteorid].meteortype = meteortype;
                this.gamebackground["meteor" + meteorid].meteorlife = Number(_root.incomingmeteor[location][6]);
            } // end if
        }
        else
        {
            removeMovieClip (this.gamebackground["meteor" + meteorid]);
        } // end else if
    } // End of the function
    function makebackgroundstars()
    {
        backgroundstar = new Array();
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        playerslastxcoord = playersxcoord;
        playerslastycoord = playersycoord;
        for (i = 0; i < totalstars; i++)
        {
            backgroundstar[i] = new Array(2);
            backgroundstar[i][0] = random(doublegameareawidth) - Math.round(doublegameareawidth / 2) + playersxcoord;
            backgroundstar[i][1] = random(doublegameareaheight) - Math.round(doublegameareaheight / 2) + playersycoord;
            this.gamebackground.attachMovie("backgroundstar" + Math.round(Math.random() * _root.totalstartypes), "backgroundstar" + String(i), i + 500);
            setProperty("gamebackground.backgroundstar" + i, _x, backgroundstar[i][0]);
            setProperty("gamebackground.backgroundstar" + i, _y, backgroundstar[i][1]);
        } // end of for
    } // End of the function
    function adjuststars(playersxcoord, playersycoord)
    {
        xcoordadjustment = playersxcoord - playerslastxcoord;
        ycoordadjustment = playersycoord - playerslastycoord;
        playerslastxcoord = playersxcoord;
        playerslastycoord = playersycoord;
        leftofviewcheck = playersxcoord - gameareawidth;
        rightofviewcheck = playersxcoord + gameareawidth;
        topofviewcheck = playersycoord - gameareaheight;
        bottomofviewcheck = playersycoord + gameareaheight;
        for (i = 0; i < totalstars; i++)
        {
            redrawingstar = false;
            if (backgroundstar[i][0] < leftofviewcheck)
            {
                backgroundstar[i][0] = backgroundstar[i][0] + doublegameareawidth;
                this.gamebackground["backgroundstar" + i]._x = backgroundstar[i][0];
            }
            else if (backgroundstar[i][0] > rightofviewcheck)
            {
                backgroundstar[i][0] = backgroundstar[i][0] + -doublegameareawidth;
                this.gamebackground["backgroundstar" + i]._x = backgroundstar[i][0];
            } // end else if
            if (backgroundstar[i][1] < topofviewcheck)
            {
                backgroundstar[i][1] = backgroundstar[i][1] + doublegameareaheight;
                this.gamebackground["backgroundstar" + i]._y = backgroundstar[i][1];
                continue;
            } // end if
            if (backgroundstar[i][1] > bottomofviewcheck)
            {
                backgroundstar[i][1] = backgroundstar[i][1] + -doublegameareaheight;
                this.gamebackground["backgroundstar" + i]._y = backgroundstar[i][1];
            } // end if
        } // end of for
    } // End of the function
    for (iii = 0; iii < _root.neutralships.length; iii++)
    {
        func_drawneutralship(iii);
    } // end of for
    _root.otherplayership = new Array();
    _root.currentplayershotsfired = 0;
    this._x = _root.leftindentofgamearea + _root.gameareawidth / 2;
    this._y = _root.topindentofgamearea + _root.gameareaheight / 2;
    this.gameareawidth = _root.gameareawidth;
    this.gameareaheight = _root.gameareaheight;
    doublegameareawidth = gameareawidth * 2;
    doublegameareaheight = gameareaheight * 2;
    playersshiptype = _root.playershipstatus[5][0];
    this.attachMovie("shiptype" + playersshiptype, "playership", 1000);
    for (i = 0; i < _root.playershipstatus[8].length; i++)
    {
        if (_root.playershipstatus[8][i][0] != "none" || !isNaN(_root.playershipstatus[8][i][0]))
        {
            this.playership.attachMovie("turrettype0", "turret" + i, 250 + i);
            this.playership["turret" + i].shottype = _root.playershipstatus[8][i][0];
            this.playership["turret" + i].xonship = _root.shiptype[playersshiptype][5][i][0];
            this.playership["turret" + i].yonship = _root.shiptype[playersshiptype][5][i][1];
            this.playership["turret" + i]._x = this.playership["turret" + i].xonship;
            this.playership["turret" + i]._y = this.playership["turret" + i].yonship;
        } // end if
    } // end of for
    for (ii = 0; ii < _root.guntype.length; ii++)
    {
        _root["guntype" + ii + "sound"] = new Sound();
        _root["guntype" + ii + "sound"].attachSound("guntype" + ii + "sound");
        _root["guntype" + ii + "sound"].setVolume("75");
    } // end of for
    _root.missilefiresound = new Sound();
    _root.missilefiresound.attachSound("missilefiresound");
    _root.missilefiresound.setVolume("75");
    _root.aimessageradiostatic = new Sound();
    _root.aimessageradiostatic.attachSound("radiostaticsound");
    _root.aimessageradiostatic.setVolume("75");
    this.playershipfacing = _root.playershipfacing;
    this.playershiprotation = _root.playershiprotation;
    this.playershipvelocity = _root.playershipvelocity;
    this.playershipmaxvelocity = _root.playershipmaxvelocity;
    this.playershipacceleration = _root.playershipacceleration;
    this.totalstars = _root.totalstars;
    this.shipcoordinatex = _root.shipcoordinatex;
    this.shipcoordinatey = _root.shipcoordinatey;
    this.playershotsfired = new Array();
    _root.attachMovie("onlineplayerslist", "onlineplayerslist", 99982);
    setProperty("_root.onlineplayerslist", _x, 0);
    setProperty("_root.onlineplayerslist", _y, 105);
    this.halfgameareawidth = Math.round(gameareawidth / 2) + 1;
    this.halfgameareaheight = Math.round(gameareaheight / 2) + 1;
}

// [onClipEvent of sprite 1407 in frame 10]
onClipEvent (load)
{
    this.createEmptyMovieClip("gamebackground", 10);
    makebackgroundstars();
    if (_root.kingofflag[0] == null)
    {
        if (_root.kingofflag[2] != null && _root.kingofflag[3] != null)
        {
            _root.func_kingofflagcommand("CREATE", _root.kingofflag[2], _root.kingofflag[3]);
        } // end if
    } // end if
    for (jjj = 0; jjj < _root.incomingmeteor.length; jjj++)
    {
        meteorid = _root.incomingmeteor[jjj][0];
        func_meteordraw("ADD", meteorid);
    } // end of for
    currentframe = 0;
    updateinterval = 80;
}

// [onClipEvent of sprite 1407 in frame 10]
onClipEvent (enterFrame)
{
    lasttime = curenttime;
    curenttime = getTimer();
    currenttimechangeratio = (curenttime - lasttime) * 0.001000;
    _root.currenttimechangeratio = currenttimechangeratio;
    _root.curenttime = curenttime;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    ++currentframe;
    if (currentframe > updateinterval)
    {
        currentframe = 0;
        adjuststars(playersxcoord, playersycoord);
    } // end if
    this.gamebackground._x = -playersxcoord;
    this.gamebackground._y = -playersycoord;
}

// [onClipEvent of sprite 1420 in frame 10]
onClipEvent (load)
{
    currentylocationofitem = 0;
    weaponactivationspacing = 45;
    shieldactivationspacing = 45;
    this._x = _root.leftindentofgamearea + _root.gameareawidth - 25;
    this._y = _root.topindentofgamearea;
    this._height = gameareaheight;
    currentylocationofitem = currentylocationofitem + shieldactivationspacing;
}

// [onClipEvent of sprite 1495 in frame 10]
onClipEvent (load)
{
    function displaystats()
    {
        if (lastshownshield != _root.playershipstatus[2][1])
        {
            lastshownshield = _root.playershipstatus[2][1];
            this.shield = Math.round(lastshownshield);
            this.shieldbar.showshield(shield);
        } // end if
        if (lastshownenergy != _root.playershipstatus[1][1])
        {
            lastshownenergy = _root.playershipstatus[1][1];
            this.energy = Math.round(lastshownenergy);
            this.energybar.showenergy(energy);
        } // end if
        currentstructure = _root.playershipstatus[2][5];
        if (lastshownstructure != currentstructure)
        {
            this.structure = Math.round(currentstructure);
            this.structurebar.structure = laststructure;
            laststructure = currentstructure;
        } // end if
        if (_root.isplayeremp)
        {
            downtime = Math.ceil((_root.playerempend - currenttime) / 1000);
            this.energy = "OFFLINE " + downtime;
            _root.playershipstatus[1][1] = 0;
            this.shield = "OFFLINE " + downtime;
            _root.playershipstatus[2][1] = 0;
        } // end if
        if (_root.playershipstatus[5][20] == true)
        {
            timeleft = (_root.playershipstatus[5][21] - getTimer()) / 1000;
            if (timeleft < 0)
            {
                _root.playershipstatus[5][20] = false;
                this.shield = Math.round(lastshownshield);
            }
            else
            {
                this.shield = "INVULNERABLE " + Math.round(timeleft);
            } // end if
        } // end else if
    } // End of the function
    playercurrentenergygenerator = _root.playershipstatus[1][0];
    energyrechargerate = _root.energygenerators[playercurrentenergygenerator][0];
    playercurrentenergyenergycapacitor = _root.playershipstatus[1][5];
    maxenergy = _root.energycapacitors[playercurrentenergyenergycapacitor][0];
    playercurrentshieldgenerator = _root.playershipstatus[2][0];
    energydrainedbyshieldgenatfull = _root.shieldgenerators[playercurrentshieldgenerator][3];
    shieldrechargerate = _root.shieldgenerators[playercurrentshieldgenerator][1];
    maxshieldstrength = _root.shieldgenerators[playercurrentshieldgenerator][0];
    baseshieldgendrain = _root.shieldgenerators[playercurrentshieldgenerator][2];
    lastshieldstrength = 0;
    lastenergyamount = 0;
    displayupdaterate = 3;
    currentdisplayframe = 0;
    showupdaterate = 5;
    currentshowupdate = 0;
    lasttimerrefreshed = getTimer();
    _root.isplayeremp = false;
    _root.playerempend = 0;
    displaystats();
}

// [onClipEvent of sprite 1495 in frame 10]
onClipEvent (enterFrame)
{
    ++currentshowupdate;
    if (currentshowupdate > showupdaterate)
    {
        currentshowupdate = 0;
        displaystats();
    } // end if
    if (_root.playershipstatus[5][4] == "dead")
    {
        _root.playershipstatus[2][1] = 0;
        _root.playershipstatus[1][1] = 0;
        _root.playershipstatus[2][5] = 0;
        this.flagwindow._visible = false;
        displaystats();
    }
    else
    {
        ++currentdisplayframe;
        if (currentdisplayframe > displayupdaterate)
        {
            currentdisplayframe = 0;
            currenttime = getTimer();
            currenttimechangeratio = (currenttime - lasttimerrefreshed) / 1000;
            lasttimerrefreshed = currenttime;
            if (_root.isplayeremp)
            {
                currentshieldstrength = 0;
                currentenergy = 0;
                if (_root.playerempend < currenttime)
                {
                    _root.isplayeremp = false;
                } // end if
            }
            else
            {
                currentshieldstrength = _root.playershipstatus[2][1];
                if (currentshieldstrength < 0)
                {
                    currentshieldstrength = 0;
                } // end if
                currentshieldstrength = currentshieldstrength + shieldrechargerate * currenttimechangeratio;
                if (_root.playershipstatus[2][2] == "OFF")
                {
                    currentshieldstrength = 0;
                }
                else if (_root.playershipstatus[2][2] == "HALF")
                {
                    if (currentshieldstrength > Math.round(maxshieldstrength / 2))
                    {
                        currentshieldstrength = Math.round(maxshieldstrength / 2);
                    } // end if
                }
                else if (currentshieldstrength > maxshieldstrength)
                {
                    currentshieldstrength = maxshieldstrength;
                } // end else if
                _root.playershipstatus[2][1] = currentshieldstrength;
                currentenergy = _root.playershipstatus[1][1];
                currentenergy = currentenergy + _root.energygenerators[playercurrentenergygenerator][0] * currenttimechangeratio;
                if (_root.playershipstatus[2][2] != "OFF")
                {
                    eval(currentenergy = currentenergy - baseshieldgendrain * currenttimechangeratio)(currentenergy = currentenergy - currentshieldstrength / maxshieldstrength * (energydrainedbyshieldgenatfull * currenttimechangeratio));
                } // end if
                if (currentenergy < 0)
                {
                    currentenergy = 0;
                } // end if
                if (currentenergy > maxenergy)
                {
                    currentenergy = maxenergy;
                } // end if
                _root.playershipstatus[1][1] = currentenergy;
            } // end else if
            if (lastshieldstrength != currentshieldstrength)
            {
                lastshieldstrength = currentshieldstrength;
            } // end if
            if (lastenergyamount != currentenergy)
            {
                lastenergyamount = currentenergy;
            } // end if
        } // end if
    } // end else if
}

// [onClipEvent of sprite 1498 in frame 10]
onClipEvent (load)
{
    function func_isplayeronmap()
    {
        onmap = false;
        playsxsector = _root.playershipstatus[6][0];
        playsysector = _root.playershipstatus[6][1];
        if (playsxsector >= 0 && playsxsector <= _root.sectorinformation[0][0])
        {
            if (playsysector >= 0 && playsysector <= _root.sectorinformation[0][1])
            {
                onmap = true;
            } // end if
        } // end if
        return (onmap);
    } // End of the function
    this.shipcoordinatex = _root.shipcoordinatex;
    this.shipcoordinatey = _root.shipcoordinatey;
    xwidthofasector = _root.sectorinformation[1][0];
    ywidthofasector = _root.sectorinformation[1][1];
    xsector = Math.ceil(shipcoordinatex / xwidthofasector);
    ysector = Math.ceil(shipcoordinatey / ywidthofasector);
    this._visible = false;
    updateinterval = 3000;
    timeintervalcheck = getTimer() + updateinterval;
    pathtosectorfile = "./systems/system" + _root.playershipstatus[5][1] + "/";
    _root.otherplayership = new Array();
    currentotherplayshot = 0;
    justenteredgame = true;
    this.gameareawidth = _root.gameareawidth;
    this.gameareaheight = _root.gameareaheight;
    lowestvolume = 50;
    lowestvolumelength = Math.sqrt(gameareawidth / 2 * (gameareawidth / 2) + gameareaheight / 2 * (gameareaheight / 2));
    currentsectoris = xsector + "`" + ysector;
    lastsectoris = "NEW`";
    errorinformation = "PI`" + lastsectoris + "`" + currentsectoris + "`" + _root.errorchecknumber + "~";
    information = errorinformation + "PI" + "`" + _root.playershipstatus[3][0] + "`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + Math.round(_root.playershipfacing) + "`" + Math.round(_root.playershipvelocity) + "`" + keysbeingpressed + "`" + _root.playershipstatus[5][15] + Number.NaN;
    _root.mysocket.send(information);
    _root.errorcheckedmessage(errorinformation, _root.errorchecknumber);
    lastsectoris = currentsectoris;
    _root.ainformationtosend = "";
    aicheckinterval = 18000;
    lastaichecktime = getTimer() + aicheckinterval;
    timetoredrawship = 7000;
    curenttime = getTimer();
}

// [onClipEvent of sprite 1498 in frame 10]
onClipEvent (enterFrame)
{
    function checkforloadingnewai()
    {
        oddsthataiwillappear = 16;
        checktheodds = Math.random() * 100;
        if (checktheodds < oddsthataiwillappear)
        {
            maxaitoappear = 2;
            aitoappear = Math.round(Math.random() * (maxaitoappear - 1)) + 1;
            _root.bringinaiships(aitoappear, "RANDOM");
        } // end if
    } // End of the function
    function othermissilefiremovement()
    {
        cc = 0;
        _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "otherfire", "othermissilefire" + _root.othermissilefire[lastvar][8], 3000 + _root.othermissilefire[lastvar][8]);
        while (cc < _root.othermissilefire.length)
        {
            if (_root.othermissilefire[cc][5] < curenttime)
            {
                removeMovieClip ("othermissilefire" + _root.othermissilefire[cc][8]);
                _root.othergunfire.splice(cc, 1);
                --i;
            } // end if
            ++cc;
        } // end while
    } // End of the function
    function checkothershiptimouts()
    {
        for (qz = 0; qz < _root.otherplayership.length; qz++)
        {
            if (_root.otherplayership[qz][6] < _root.curenttime)
            {
                removeMovieClip ("_root.gamedisplayarea.otherplayership" + _root.otherplayership[qz][0]);
                removeMovieClip ("_root.gamedisplayarea.nametag" + _root.otherplayership[qz][0]);
                _root.otherplayership.splice(qz, 1);
            } // end if
        } // end of for
    } // End of the function
    function movementofanobjectwiththrust()
    {
        if (relativefacing == 0)
        {
            ymovement = -velocity;
            xmovement = 0;
        } // end if
        if (velocity != 0)
        {
            this.relativefacing = Math.round(this.relativefacing);
            ymovement = Math.round(-velocity * _root.cosines[this.relativefacing]);
            xmovement = Math.round(velocity * _root.sines[this.relativefacing]);
        }
        else
        {
            ymovement = 0;
            xmovement = 0;
        } // end else if
    } // End of the function
    function getaiinformation()
    {
        aiinformation = "";
        for (zza = 0; zza < _root.aishipshosted.length; zza++)
        {
            aiinformation = aiinformation + ("AI`" + _root.aishipshosted[zza][0] + "`" + Math.round(_root.aishipshosted[zza][1]) + "`" + Math.round(_root.aishipshosted[zza][2]) + "`" + _root.aishipshosted[zza][3] + "`" + Math.round(_root.aishipshosted[zza][4]) + "`" + _root.aishipshosted[zza][5] + "~");
        } // end of for
    } // End of the function
    currenttimechangeratio = _root.currenttimechangeratio;
    curenttime = getTimer();
    if (_root.afterburnerinuse != lastafterburnerinuse)
    {
        _root.keywaspressed = true;
        lastafterburnerinuse = _root.afterburnerinuse;
    } // end if
    if (curenttime >= timeintervalcheck || _root.keywaspressed == true)
    {
        currentsectoris = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
        lastsectoris = _root.playershipstatus[6][2] + "`" + _root.playershipstatus[6][3];
        if (lastsectoris == currentsectoris)
        {
            lastsectoris = "FALSE";
        }
        else if (lastsectoris != currentsectoris)
        {
            _root.playershipstatus[6][2] = _root.playershipstatus[6][0];
            _root.playershipstatus[6][3] = _root.playershipstatus[6][1];
        } // end else if
        _root.keywaspressed = false;
        if (lastsectoris != "FALSE" || _root.otherplayership.length > 0 || curenttime >= timeintervalcheck)
        {
            timeintervalcheck = curenttime + updateinterval;
            currentkeyedshipmovement();
            getaiinformation();
            information = "PI`" + lastsectoris + "`" + currentsectoris + "~PI" + "`" + _root.playershipstatus[3][0] + "`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + Math.round(_root.playershipfacing) + "`" + Math.round(_root.playershipvelocity) + "`" + keysbeingpressed + "`" + _root.playershipstatus[5][15] + "`" + _root.func_globaltimestamp() + "~" + _root.gunfireinformation + _root.ainformationtosend;
            _root.gunfireinformation = "";
            _root.ainformationtosend = "";
            if (_root.playershipstatus[5][4] != "dead")
            {
                _root.mysocket.send(information);
            } // end if
        } // end if
    } // end if
    if (_root.gunfireinformation != "" || _root.ainformationtosend != "")
    {
        if (_root.otherplayership.length > 0)
        {
            currentsectoris = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
            information = "PI`FALSE`~" + _root.gunfireinformation + _root.ainformationtosend;
            _root.gunfireinformation = "";
            _root.mysocket.send(information);
        } // end if
    } // end if
    _root.gunfireinformation = "";
    _root.ainformationtosend = "";
    if (_root.teamdeathmatch != true && _root.isgameracingzone != true)
    {
        if (curenttime > lastaichecktime && func_isplayeronmap())
        {
            lastaichecktime = curenttime + aicheckinterval;
            if (_root.aishipshosted.length < 1 && _root.isaiturnedon == true && _root.hostileplayerships == false)
            {
                checkforloadingnewai();
            } // end if
        } // end if
    } // end if
    checkothershiptimouts();
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (load)
{
    function func_sendstatuscheck(pilotno)
    {
        datatosend = "SP`" + pilotno + "~STATS`GET`" + _root.playershipstatus[3][0] + "~";
        _root.mysocket.send(datatosend);
    } // End of the function
    function attachtargetship(currenttargetshiptype)
    {
        this.attachMovie("shiptype" + currenttargetshiptype, "targetship", 1);
        if (this.targetship._height > 50 || this.targetship._width > 50)
        {
            if (this.targetship._height > this.targetship._width)
            {
                change = this.targetship._height - 50;
                this.targetship._height = this.targetship._height - change;
                this.targetship._width = this.targetship._width - change;
            }
            else
            {
                change = this.targetship._width - 50;
                this.targetship._height = this.targetship._height - change;
                this.targetship._width = this.targetship._width - change;
            } // end if
        } // end else if
        setProperty("targetship", _x, "153");
        setProperty("targetship", _y, "22");
        targetpointer._visible = true;
    } // End of the function
    function func_setlocalnpcs()
    {
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        minrangeforrad = 1500;
        neutralaiarray = new Array();
        for (q = 0; q < _root.neutralships.length; q++)
        {
            labell = _root.neutralships[q][10];
            xlocc = _root.gamedisplayarea[labell].xcoord;
            ylocc = _root.gamedisplayarea[labell].ycoord;
            playerdistancefromship = Math.round(Math.sqrt((playersxcoord - xlocc) * (playersxcoord - xlocc) + (playersycoord - ylocc) * (playersycoord - ylocc)));
            if (playerdistancefromship < 2000)
            {
                neutralaiarray[neutralaiarray.length] = _root.neutralships[q];
            } // end if
        } // end of for
    } // End of the function
    lasttarget = -1;
    currenttargetname = "No Target";
    currenttargetbounty = "";
    istkeypressed = false;
    targettype = "none";
    _root.targetinfo[0] = "None";
    _root.targetinfo[1] = 0;
    _root.targetinfo[2] = 0;
    _root.targetinfo[3] = "";
    this.currenttargetname.html = true;
    shielddisp = "";
    structdisp = "";
    targetName = new Color("currenttargetname");
    targetName.setRGB(16777215);
    _root.targetinfo[10] = "enemy";
    lasttarget = -1;
    lasttargetshiptype = null;
    currenttargetdist = "";
    targetpointer._visible = false;
    currentinterval = 0;
    intervalwait = 30;
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (keyDown)
{
    if (Key.isDown(84) && _root.gamechatinfo[4] == false)
    {
        if (istkeypressed == false)
        {
            func_setlocalnpcs();
            if (_root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length > 0)
            {
                ++lasttarget;
                if (lasttarget >= _root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length)
                {
                    lasttarget = 0;
                } // end if
                if (lasttarget < _root.otherplayership.length)
                {
                    currenttargetname = _root.otherplayership[lasttarget][10];
                    currenttargetshiptype = _root.otherplayership[lasttarget][11];
                    currenttargetbounty = _root.otherplayership[lasttarget][12];
                    targettype = "other";
                    attachtargetship(currenttargetshiptype);
                    pilotno = _root.otherplayership[lasttarget][0];
                    func_sendstatuscheck(pilotno);
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length)
                {
                    aitargetnumber = lasttarget - int(_root.otherplayership.length);
                    currenttargetname = _root.aishipshosted[aitargetnumber][7];
                    currenttargetshiptype = _root.aishipshosted[aitargetnumber][6];
                    currenttargetbounty = "400";
                    targettype = "hosted";
                    attachtargetship(currenttargetshiptype);
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length)
                {
                    neuttargetnumber = lasttarget - _root.otherplayership.length - _root.aishipshosted.length;
                    currenttargetname = neutralaiarray[neuttargetnumber][11];
                    currenttargetshiptype = neutralaiarray[neuttargetnumber][4];
                    currenttargetbounty = "N/A";
                    targettype = "neutral";
                    attachtargetship(currenttargetshiptype);
                }
                else
                {
                    currentsectioritemnumber = lasttarget - (_root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length);
                    currenttargetname = _root.sectoritemsfortarget[currentsectioritemnumber][0];
                    currenttargetbounty = "UNKNOWN";
                    targettype = "sectoritem";
                    removeMovieClip (targetship);
                } // end else if
                _root.targetinfo[0] = currenttargetname;
                checkorsettarget();
            }
            else
            {
                settonotarget();
            } // end if
        } // end else if
        istkeypressed = true;
    }
    else
    {
        istkeypressed = false;
    } // end else if
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (keyUp)
{
    if (Key.isDown(84))
    {
        istkeypressed = true;
    }
    else
    {
        istkeypressed = false;
    } // end else if
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (enterFrame)
{
    ++currentinterval;
    if (currentinterval > intervalwait)
    {
        currentinterval = 0;
        func_setlocalnpcs();
        if (_root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length < 1)
        {
            if (lasttarget > -1)
            {
                settonotarget();
                lasttarget = -1;
            } // end if
        }
        else
        {
            checkorsettarget();
        } // end if
    } // end else if
}

// [onClipEvent of sprite 1514 in frame 10]
onClipEvent (load)
{
    function settonotarget()
    {
        currenttargetname = "No Target";
        currenttargetbounty = "";
        removeMovieClip (targetship);
        lasttarget = -1;
        _root.targetinfo[0] = "None";
        _root.targetinfo[1] = 0;
        _root.targetinfo[2] = 0;
        _root.targetinfo[3] = "";
        lasttargetshiptype = "";
        currenttargetdist = "";
        targetpointer._visible = false;
        _root.targetinfo[10] = "enemy";
        shielddisp = "";
        structdisp = "";
    } // End of the function
    function checkorsettarget()
    {
        func_setlocalnpcs();
        if (_root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length > 0)
        {
            if (lasttarget == -1)
            {
                lasttarget = 0;
                if (lasttarget < _root.otherplayership.length)
                {
                    currenttargetname = _root.otherplayership[lasttarget][10];
                    currenttargetshiptype = _root.otherplayership[lasttarget][11];
                    currenttargetbounty = _root.otherplayership[lasttarget][12];
                    targettype = "other";
                    pilotno = _root.otherplayership[lasttarget][0];
                    func_sendstatuscheck(pilotno);
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length)
                {
                    aitargetnumber = lasttarget - int(_root.otherplayership.length);
                    currenttargetname = _root.aishipshosted[aitargetnumber][7];
                    currenttargetshiptype = _root.aishipshosted[aitargetnumber][6];
                    currenttargetbounty = _root.aishipshosted[aitargetnumber][10];
                    targettype = "hosted";
                }
                else if (lasttarget < _root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length)
                {
                    neuttargetnumber = lasttarget - _root.otherplayership.length - _root.aishipshosted.length;
                    currenttargetname = neutralaiarray[neuttargetnumber][11];
                    currenttargetshiptype = neutralaiarray[neuttargetnumber][4];
                    currenttargetbounty = "N/A";
                    targettype = "neutral";
                    attachtargetship(currenttargetshiptype);
                }
                else
                {
                    currentsectioritemnumber = lasttarget - (_root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length);
                    currenttargetname = _root.sectoritemsfortarget[currentsectioritemnumber][0];
                    currenttargetbounty = "UNKNOWN";
                    targettype = "sectoritem";
                    removeMovieClip (targetship);
                } // end else if
                _root.targetinfo[0] = currenttargetname;
                attachtargetship(currenttargetshiptype);
            }
            else if (currenttargetname != _root.otherplayership[lasttarget][10] || currenttargetname != _root.aishipshosted[aitargetnumber][7] || currenttargetname != neutralaiarray[neuttargetnumber][11] || currenttargetname != _root.sectoritemsfortarget[currentsectioritemnumber][0])
            {
                namestillexists = false;
                for (jjcq = 0; jjcq < _root.otherplayership.length + _root.aishipshosted.length + _root.sectoritemsfortarget.length + neutralaiarray.length; jjcq++)
                {
                    if (jjcq < _root.otherplayership.length)
                    {
                        if (currenttargetname == _root.otherplayership[jjcq][10])
                        {
                            namestillexists = true;
                            lasttarget = jjcq;
                        } // end if
                        continue;
                    } // end if
                    if (jjcq < _root.otherplayership.length + _root.aishipshosted.length)
                    {
                        aitargetnumber = jjcq - _root.otherplayership.length;
                        if (currenttargetname == _root.aishipshosted[aitargetnumber][7])
                        {
                            namestillexists = true;
                            lasttarget = jjcq;
                            jjcq = 99999;
                        } // end if
                        continue;
                    } // end if
                    if (jjcq < _root.otherplayership.length + _root.aishipshosted.length + neutralaiarray.length)
                    {
                        neuttargetnumber = jjcq - _root.otherplayership.length - _root.aishipshosted.length;
                        if (String(currenttargetname) == String(neutralaiarray[neuttargetnumber][11]))
                        {
                            namestillexists = true;
                            lasttarget = jjcq;
                            jjcq = 99999;
                        } // end if
                        continue;
                    } // end if
                    currentsectioritemnumber = jjcq - _root.otherplayership.length - _root.aishipshosted.length - neutralaiarray.length;
                    if (currenttargetname == _root.sectoritemsfortarget[currentsectioritemnumber][0])
                    {
                        namestillexists = true;
                        lasttarget = jjcq;
                        jjcq = 99999;
                    } // end if
                } // end of for
                if (namestillexists != true)
                {
                    settonotarget();
                    currentinterval = 9999;
                } // end if
            } // end else if
            if (_root.targetinfo[0] != "None")
            {
                if (targettype == "other")
                {
                    _root.targetinfo[1] = _root.otherplayership[lasttarget][1];
                    _root.targetinfo[2] = _root.otherplayership[lasttarget][2];
                    _root.targetinfo[5] = _root.otherplayership[lasttarget][5];
                    _root.targetinfo[6] = _root.otherplayership[lasttarget][4];
                    _root.targetinfo[7] = "otherplayership";
                    _root.targetinfo[8] = lasttarget;
                    _root.gamedisplayarea["otherplayership" + _root.otherplayership[lasttarget][0]].attachMovie("shiptargeted", "shiptargeted", 52);
                    set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[lasttarget][0] + ".shiptargeted.shipid", _root.targetinfo[0]);
                    targetcursorColor = new Color("_root.gamedisplayarea.otherplayership" + _root.otherplayership[lasttarget][0] + ".shiptargeted");
                    if (_root.playershipstatus[5][2] != _root.otherplayership[lasttarget][13] || _root.playershipstatus[5][2] == "N/A")
                    {
                        _root.targetinfo[10] = "enemy";
                        targetcursorColor.setRGB(16711680);
                    }
                    else
                    {
                        _root.targetinfo[10] = "friend";
                        targetcursorColor.setRGB(16776960);
                    } // end else if
                    if (_root.otherplayership[lasttarget][16] == "C" || _root.otherplayership[lasttarget][16] == "S")
                    {
                        func_targetlocation(true);
                    }
                    else
                    {
                        func_targetlocation(false);
                    } // end if
                } // end else if
                if (targettype == "hosted")
                {
                    _root.targetinfo[1] = _root.aishipshosted[aitargetnumber][1];
                    _root.targetinfo[2] = _root.aishipshosted[aitargetnumber][2];
                    _root.targetinfo[5] = _root.aishipshosted[aitargetnumber][3];
                    _root.targetinfo[6] = _root.aishipshosted[aitargetnumber][4];
                    _root.targetinfo[7] = "aiship";
                    _root.targetinfo[8] = aitargetnumber;
                    _root.targetinfo[10] = "enemy";
                    _root.gamedisplayarea["aiship" + _root.aishipshosted[aitargetnumber][0]].attachMovie("shiptargeted", "shiptargeted", 52);
                    set("_root.gamedisplayarea.aiship" + _root.aishipshosted[aitargetnumber][0] + ".shiptargeted.shipid", _root.targetinfo[0]);
                    targetcursorColor = new Color("_root.gamedisplayarea.aiship" + _root.aishipshosted[aitargetnumber][0] + ".shiptargeted");
                    targetcursorColor.setRGB(16711680);
                    func_targetlocation(false);
                } // end if
                if (targettype == "neutral")
                {
                    labell = neutralaiarray[neuttargetnumber][10];
                    _root.targetinfo[1] = _root.gamedisplayarea[labell].xcoord;
                    _root.targetinfo[2] = _root.gamedisplayarea[labell].ycoord;
                    _root.targetinfo[5] = _root.gamedisplayarea[labell].currentvelocity;
                    _root.targetinfo[6] = _root.gamedisplayarea[labell].rotation;
                    _root.targetinfo[7] = "neutral";
                    _root.targetinfo[8] = lasttarget;
                    _root.gamedisplayarea[labell].attachMovie("shiptargeted", "shiptargeted", 52);
                    set("_root.gamedisplayarea." + labell + ".shiptargeted.shipid", _root.targetinfo[0]);
                    targetcursorColor = new Color("_root.gamedisplayarea." + labell + ".shiptargeted");
                    targetcursorColor.setRGB(16711680);
                    func_targetlocation(false);
                } // end if
                if (targettype == "sectoritem")
                {
                    _root.targetinfo[1] = _root.sectoritemsfortarget[currentsectioritemnumber][1];
                    _root.targetinfo[2] = _root.sectoritemsfortarget[currentsectioritemnumber][2];
                    _root.targetinfo[5] = 0;
                    _root.targetinfo[6] = 0;
                    _root.targetinfo[7] = "sectoritem";
                    _root.targetinfo[8] = currentsectioritemnumber;
                    _root.targetinfo[10] = "enemy";
                    if (_root.playershipstatus[5][10] == _root.targetinfo[0])
                    {
                        _root.targetinfo[10] = "friend";
                    }
                    else if (_root.teamdeathmatch == true)
                    {
                        if (_root.targetinfo[0] == _root.teambases[_root.playershipstatus[5][2]][0])
                        {
                            _root.targetinfo[10] = "friend";
                        } // end if
                    } // end else if
                    func_targetlocation(false);
                    removeMovieClip (targetship);
                } // end if
            } // end if
            if (currenttargetbounty != _root.otherplayership[lasttarget][12] && targettype == "other")
            {
                currenttargetbounty = _root.otherplayership[lasttarget][12];
            }
            else if (currenttargetbounty != _root.aishipshosted[aitargetnumber][10] && targettype == "hosted")
            {
                currenttargetbounty = _root.aishipshosted[aitargetnumber][10];
            } // end if
        } // end else if
        if (_root.targetinfo[10] == "enemy")
        {
            targetName.setRGB(16711680);
        }
        else
        {
            targetName.setRGB(16776960);
        } // end else if
        if (targettype == "other")
        {
            if (_root.otherplayership[lasttarget][40] == null)
            {
                pilotno = _root.otherplayership[lasttarget][0];
                func_sendstatuscheck(pilotno);
                _root.otherplayership[lasttarget][40] = "waiting";
                structdisp = "Waiting";
            }
            else if (_root.otherplayership[lasttarget][40] != "waiting")
            {
                currentshield = Math.round(_root.otherplayership[lasttarget][41] + (getTimer() - _root.otherplayership[lasttarget][44]) * _root.otherplayership[lasttarget][42] / 1000);
                _root.otherplayership[lasttarget][44] = getTimer();
                if (currentshield > _root.otherplayership[lasttarget][43])
                {
                    currentshield = _root.otherplayership[lasttarget][43];
                } // end if
                _root.otherplayership[lasttarget][41] = currentshield;
                shielddisp = currentshield;
                structdisp = _root.otherplayership[lasttarget][40];
            } // end else if
        }
        else if (targettype == "hosted")
        {
            if (_root.aishipshosted[aitargetnumber][20] > 0)
            {
                shielddisp = Math.round(_root.aishipshosted[aitargetnumber][22]);
                structdisp = Math.round(_root.aishipshosted[aitargetnumber][20]);
                if (structdisp < 0)
                {
                    structdisp = 0;
                } // end if
            } // end if
        }
        else if (targettype == "sectoritem")
        {
            shielddisp = Math.round(_root.sectoritemsfortarget[currentsectioritemnumber][5]);
            structdisp = Math.round(_root.sectoritemsfortarget[currentsectioritemnumber][6]);
            if (structdisp < 0)
            {
                structdisp = 0;
            } // end if
        }
        else if (targettype == "neutral")
        {
            labell = neutralaiarray[neuttargetnumber][10];
            shielddisp = Math.round(_root.gamedisplayarea[labell].currentshipshield);
            structdisp = Math.round(_root.gamedisplayarea[labell].shiphealth);
            if (structdisp < 0)
            {
                structdisp = 0;
            } // end else if
        } // end else if
    } // End of the function
    function func_targetlocation(iscloakorstealth)
    {
        if (iscloakorstealth == false)
        {
            playersxcoord = _root.shipcoordinatex;
            playersycoord = _root.shipcoordinatey;
            currenttargetdist = _root.targetinfo[3] = Math.round(Math.sqrt((playersxcoord - _root.targetinfo[1]) * (playersxcoord - _root.targetinfo[1]) + (playersycoord - _root.targetinfo[2]) * (playersycoord - _root.targetinfo[2])));
            playeranglefromship = 180 - Math.atan2(_root.targetinfo[1] - playersxcoord, _root.targetinfo[2] - playersycoord) / 0.017453;
            targetpointer._visible = true;
            targetpointer._rotation = playeranglefromship;
            playeranglefromship = playeranglefromship - _root.playershipfacing;
            if (playeranglefromship > 360)
            {
                playeranglefromship = playeranglefromship - 360;
            } // end if
            if (playeranglefromship < 0)
            {
                playeranglefromship = playeranglefromship + 360;
            } // end if
            _root.targetinfo[4] = Math.round(playeranglefromship);
        }
        else if (iscloakorstealth == true)
        {
            targetpointer._visible = false;
            currenttargetdist = "HIDDEN";
            _root.targetinfo[3] = 9999999999.000000;
        } // end else if
    } // End of the function
}

// [onClipEvent of sprite 1522 in frame 10]
onClipEvent (load)
{
    this._visible = false;
    pingintervalcheck = 8000;
    lastpingcheck = getTimer() + 2000;
}

// [onClipEvent of sprite 1522 in frame 10]
onClipEvent (enterFrame)
{
    curenttime = getTimer();
    if (curenttime > lastpingcheck)
    {
        lastpingcheck = curenttime + pingintervalcheck;
        information = "PING`" + curenttime + "~";
        _root.mysocket.send(information);
    } // end if
    for (i = 0; i < _root.errorcheckdata.length; i++)
    {
        if (curenttime > _root.errorcheckdata[i][1])
        {
            _root.mysocket.send(_root.errorcheckdata[i][0]);
            _root.errorcheckdata[i][1] = curenttime + _root.errorcheckdelay;
        } // end if
    } // end of for
}

// [onClipEvent of sprite 1525 in frame 10]
onClipEvent (load)
{
    function func_spawnameteor(spawnchance)
    {
        minspawnchance = 0.820000;
        maxmeteors = 2;
        if (spawnchance > minspawnchance && _root.incomingmeteor.length < maxmeteors)
        {
            _root.func_beginmeteor();
        } // end if
    } // End of the function
    function func_randompiratebase(spawnchance)
    {
        numberofpirates = 0;
        for (i = 0; i < _root.playersquadbases.length; i++)
        {
            if (_root.playersquadbases[i][0].substr(0, 3) == "*AI")
            {
                ++numberofpirates;
            } // end if
        } // end of for
        if (numberofpirates < maxpiratebases && spawnchance > minpiratebaseodds)
        {
            for (attempts = 0; attempts < maxtriesforpiratebase; attempts++)
            {
                xsector = Math.round(Math.random() * _root.sectorinformation[0][0]);
                ysector = Math.round(Math.random() * _root.sectorinformation[0][1]);
                gridsaroundtaken = false;
                for (i = 0; i < _root.sectormapitems.length; i++)
                {
                    if (_root.sectormapitems[i][10] == xsector || _root.sectormapitems[i][10] == xsector - 1 || _root.sectormapitems[i][10] == xsector + 1)
                    {
                        if (_root.sectormapitems[i][11] == ysector || _root.sectormapitems[i][11] == ysector - 1 || _root.sectormapitems[i][11] == ysector + 1)
                        {
                            gridsaroundtaken = true;
                        } // end if
                    } // end if
                } // end of for
                for (i = 0; i < _root.playersquadbases.length; i++)
                {
                    if (_root.playersquadbases[i][4] == xsector || _root.playersquadbases[i][4] == xsector - 1 || _root.playersquadbases[i][4] == xsector + 1)
                    {
                        if (_root.playersquadbases[i][5] == ysector || _root.playersquadbases[i][5] == ysector - 1 || _root.playersquadbases[i][5] == ysector + 1)
                        {
                            gridsaroundtaken = true;
                        } // end if
                    } // end if
                } // end of for
                if (gridsaroundtaken == false)
                {
                    break;
                } // end if
            } // end of for
            if (gridsaroundtaken == false)
            {
                squadbasetype = 0;
                func_createapiratebase(squadbasetype, xsector, ysector);
            } // end if
        } // end if
    } // End of the function
    function func_createapiratebase(squadbasetype, xsector, ysector)
    {
        function func_setuppiratebase(squadbasetype, baseidname)
        {
            guntypes = _root.squadbaseinfo[squadbasetype][2][1];
            this.newbaseGuns = new XML();
            this.newbaseGuns.load(_root.pathtoaccounts + "squadbases.php?mode=changegun&baseid=" + baseidname + "&gunone=" + func_piratehardpoint(squadbasetype) + "&guntwo=" + func_piratehardpoint(squadbasetype) + "&gunthree=" + func_piratehardpoint(squadbasetype) + "&gunfour=" + func_piratehardpoint(squadbasetype));
            this.newbaseShield = new XML();
            this.newbaseShield.load(_root.pathtoaccounts + "squadbases.php?mode=newshield&baseid=" + baseidname + "&newshield=" + _root.squadbaseinfo[squadbasetype][1][3]);
            this.newbaseStruct = new XML();
            this.newbaseStruct.load(_root.pathtoaccounts + "squadbases.php?mode=repairbase&baseid=" + baseidname + "&struct=" + _root.squadbaseinfo[squadbasetype][3][1] + "&maxstruct=" + _root.squadbaseinfo[squadbasetype][3][1]);
            this.newbaseMissile = new XML();
            this.newbaseMissile.load(_root.pathtoaccounts + "squadbases.php?mode=missileshot&baseid=" + baseidname + "&shots=-" + 10000);
        } // End of the function
        xcoord = xsector * _root.sectorinformation[1][0] - Math.round(_root.sectorinformation[1][0] / 2);
        ycoord = ysector * _root.sectorinformation[1][1] - Math.round(_root.sectorinformation[1][1] / 2);
        baseidname = "*AI" + (Math.round(Math.random() * 89) + 10);
        currentsystem = _root.playershipstatus[5][1];
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadbases.php?mode=createbase&baseid=" + baseidname + "&system=" + currentsystem + "&xcoord=" + xcoord + "&ycoord=" + ycoord);
        newsquadVars.onLoad = function (success)
        {
            datatosend = "SA~NEWS`SB`CREATED`" + baseidname + "`" + basetype + "`" + xcoord + "`" + ycoord + "~";
            _root.mysocket.send(datatosend);
            func_setuppiratebase(squadbasetype, baseidname);
        };
    } // End of the function
    function func_piratehardpoint(squadbasetype)
    {
        guntype = _root.squadbaseinfo[squadbasetype][2][1] - Math.round(Math.random() * 2);
        return (guntype);
    } // End of the function
    function checkforrepstarbases()
    {
        repairedabase = false;
        newsmessage = "";
        for (ww = 0; ww < _root.starbaselocation.length; ww++)
        {
            currenttime = getTimer();
            if (_root.starbaselocation[ww][5] != "ACTIVE")
            {
                rerpairtime = Number(_root.starbaselocation[ww][5]);
                if (rerpairtime < currenttime && !isNaN(rerpairtime))
                {
                    repairedabase = true;
                    _root.starbaselocation[ww][5] = "ACTIVE";
                    newsmessage = newsmessage + ("Starbase " + _root.starbaselocation[ww][0] + " has been repaired. ");
                } // end if
            } // end if
        } // end of for
        if (repairedabase == true)
        {
            _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
            xsector = _root.playershipstatus[6][0];
            ysector = _root.playershipstatus[6][1];
            _root.gamedisplayarea.keyboardscript.sectoritemsdisp(xsector, ysector);
        } // end if
        func_checkforpriceresets();
    } // End of the function
    randomeventcheckinterval = 200000;
    nextcheckat = getTimer() + 60000;
    minpiratebaseodds = 0.850000;
    maxpiratebases = 3;
    maxtriesforpiratebase = 50;
    setInterval(checkforrepstarbases, 20000);
    lastime = getTimer();
}

// [onClipEvent of sprite 1525 in frame 10]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (_root.gamechatinfo[2][2] == true)
    {
        if (_root.gamechatinfo[2][3] < currenttime)
        {
            _root.gamechatinfo[2][2] = false;
            _root.gamechatinfo[2][3] = 0;
            colourtouse = _root.systemchattextcolor;
            message = "HOST: You Are No Longer Muted";
            _root.enterintochat(message, colourtouse);
        } // end if
    } // end if
    if (_root.teamdeathmatch != true && _root.isgameracingzone != true)
    {
        timelapsed = currenttime - lastime;
        lastime = currenttime;
        if (nextcheckat < getTimer())
        {
            _root.func_checkfortoomuchaddedandset();
            nextcheckat = getTimer() + randomeventcheckinterval;
            whattospawn = Math.round(Math.random() * 5);
            if (whattospawn == 0)
            {
                spawnchance = Math.random();
                func_randompiratebase(spawnchance);
            }
            else if (whattospawn == 1)
            {
                spawnchance = Math.random();
                func_spawnameteor(spawnchance);
            }
            else if (whattospawn == 2 || whattospawn == 3)
            {
                spawnchance = Math.random();
                if (spawnchance > 0.300000)
                {
                    func_creationofaship();
                } // end if
            }
            else
            {
                spawnchance = Math.random();
                if (spawnchance > 0.300000)
                {
                    func_createapricechange();
                } // end else if
            } // end else if
        } // end else if
        if (_root.incomingmeteor.length > 0)
        {
            timechange = timelapsed / 1000;
            for (ii = 0; ii < _root.incomingmeteor.length; ii++)
            {
                if (currenttime > _root.incomingmeteor[ii][12])
                {
                    _root.func_meteorhitbase(_root.incomingmeteor[ii][0]);
                    _root.incomingmeteor.splice(ii, 1);
                    --ii;
                    continue;
                } // end if
                _root.incomingmeteor[ii][1] = _root.incomingmeteor[ii][1] + _root.incomingmeteor[ii][10] * timechange;
                _root.incomingmeteor[ii][2] = _root.incomingmeteor[ii][2] + _root.incomingmeteor[ii][11] * timechange;
                if (_root.incomingmeteor[ii][9] != false)
                {
                    if (_root.incomingmeteor[ii][9] < currenttime)
                    {
                        datatosend = "MET`DM`" + _root.incomingmeteor[ii][0] + "`" + _root.incomingmeteor[ii][6] + "`" + _root.playershipstatus[3][0] + "`" + _root.errorchecknumber + "~";
                        _root.mysocket.send(datatosend);
                        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
                        _root.incomingmeteor[ii][9] = false;
                    } // end if
                } // end if
            } // end of for
        } // end if
        if (_root.neutralships.length > 0)
        {
            for (jjj = 0; jjj < _root.neutralships.length; jjj++)
            {
                if (_root.neutralships[jjj][7] < currenttime)
                {
                    func_removeaship(_root.neutralships[jjj][10]);
                } // end if
            } // end of for
        } // end if
    } // end if
}

// [onClipEvent of sprite 1529 in frame 11]
onClipEvent (load)
{
    function dropaiships()
    {
        i = 0;
        updatedata = "";
        while (i < _root.aishipshosted.length)
        {
            updatedata = updatedata + (_root.aishipshosted[i][0] + "`" + _root.aishipshosted[i][1] + "`" + _root.aishipshosted[i][2] + "~");
            ++i;
        } // end while
        pathtosectorfile = _root.pathtosectorfile;
        if (i > 0)
        {
            aidropsending = new XML();
            aidropsending.load(pathtosectorfile + "airun.php?mode=drop&aidata=" + updatedata);
            aidropsending.onLoad = function (success)
            {
            };
        } // end if
        _root.aishipshosted = null;
    } // End of the function
    _root.playershipstatus[2][5] = 0;
    _root.playershipstatus[5][4] = "dead";
    removeMovieClip ("_root.gamedisplayarea.playership");
    _root.gamedisplayarea.attachMovie("shiptypedead", "playership", 1000);
    _root.playershipvelocity = 0;
    _root.playersmission = new Array();
    bountychanging = Number(_root.playershipstatus[5][3] + _root.playershipstatus[5][8]);
    if (bountychanging < 1 || bountychanging > 100000000 || isNaN(bountychanging))
    {
        _root.playershipstatus[5][3] = 0;
        _root.playershipstatus[5][8] = 0;
    } // end if
    if (_root.playershipstatus[3][0] == _root.playershipstatus[5][5])
    {
        schange = (_root.playershipstatus[5][3] + _root.playershipstatus[5][8]) * -1;
    }
    else
    {
        schange = _root.playershipstatus[5][3] + _root.playershipstatus[5][8];
    } // end else if
    if (_root.playershipstatus[3][0] == _root.playershipstatus[5][5])
    {
        tb = 0;
    }
    else
    {
        tb = _root.playershipstatus[5][3] + _root.playershipstatus[5][8];
    } // end else if
    datatosend = "TC`" + _root.playershipstatus[3][0] + "`SD`" + _root.playershipstatus[5][5] + "`" + tb + "`" + Math.round(_root.playershipstatus[5][3] * _root.playershipstatus[5][6]) + "~";
    _root.mysocket.send(datatosend);
    _root.playershipstatus[5][3] = 0;
    _root.playershipstatus[5][8] = 0;
    _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
}

// [onClipEvent of sprite 1529 in frame 11]
on (release)
{
    _root.func_main_clicksound();
    if (_root.teamdeathmatch == true)
    {
        _root.gotoAndStop("teambase");
    }
    else if (_root.isgameracingzone == true)
    {
        _root.gotoAndStop("racingzonescreen");
    }
    else if (_root.playershipstatus[4][0].substr(0, 2) == "SB")
    {
        _root.gotoAndStop("stardock");
    }
    else if (_root.playershipstatus[4][0].substr(0, 2) == "PL")
    {
        _root.gotoAndStop("planet");
    } // end else if
}

// [onClipEvent of sprite 1529 in frame 11]
onClipEvent (load)
{
    function loadplayerdata(loadedvars)
    {
        newinfo = loadedvars.split("~");
        i = 0;
        otherplayership = new Array();
        while (i < newinfo.length - 1)
        {
            if (newinfo[i].substr(0, 2) == "PI")
            {
                currentthread = newinfo[i].split("`");
                if (currentthread[1].substr(0, 2) == "ST")
                {
                    _root.playershipstatus[5][0] = int(currentthread[1].substr("2"));
                    _root.playershipstatus[2][0] = int(currentthread[2].substr("2"));
                    _root.playershipstatus[1][0] = int(currentthread[3].substr("2"));
                    _root.playershipstatus[1][5] = int(currentthread[4].substr("2"));
                    _root.playershipstatus[3][1] = int(currentthread[5].substr("2"));
                    _root.playershipstatus[5][1] = int(currentthread[6].substr("2"));
                    _root.playershipstatus[4][0] = currentthread[7];
                    tr = 8;
                    _root.playershipstatus[0] = new Array();
                    while (currentthread[tr].substr(0, 2) == "HP")
                    {
                        guninfo = currentthread[tr].split("G");
                        currenthardpoint = guninfo[0].substr("2");
                        _root.playershipstatus[0][currenthardpoint] = new Array();
                        if (isNaN(guninfo[1]))
                        {
                            guninfo[1] = "none";
                        } // end if
                        _root.playershipstatus[0][currenthardpoint][0] = guninfo[1];
                        ++tr;
                    } // end while
                    _root.playershipstatus[8] = new Array();
                    while (currentthread[tr].substr(0, 2) == "TT")
                    {
                        guninfo = currentthread[tr].split("G");
                        currentturretpoint = guninfo[0].substr("2");
                        _root.playershipstatus[8][currentturretpoint] = new Array();
                        if (isNaN(guninfo[1]))
                        {
                            guninfo[1] = "none";
                        } // end if
                        _root.playershipstatus[8][currentturretpoint][0] = guninfo[1];
                        ++tr;
                    } // end while
                    while (currentthread[tr].substr(0, 2) == "CO")
                    {
                        cargoinfo = currentthread[tr].split("A");
                        currentcargotype = cargoinfo[0].substr("2");
                        _root.playershipstatus[4][1][currentcargotype] = int(cargoinfo[1]);
                        ++tr;
                    } // end while
                } // end if
            } // end if
            ++i;
        } // end while
    } // End of the function
    if (_root.playerscurrentextrashipno == "capital")
    {
        _parent.capitalinfo = "Capital Ship Has Been Lost";
        for (i = 0; i < _root.maxextraships; i++)
        {
            if (_root.extraplayerships[i][0] != null)
            {
                _root.playerscurrentextrashipno = i;
                break;
            } // end if
        } // end of for
        _root.changetonewship(_root.playerscurrentextrashipno);
        _root.initializemissilebanks();
        _root.savegamescript.saveplayersgame(this);
        datatosend = "TC`" + _root.playershipstatus[3][0] + "`SC`" + _root.extraplayerships[_root.playerscurrentextrashipno][0] + "`" + _root.errorchecknumber + "~";
        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
        _root.mysocket.send(datatosend);
    }
    else
    {
        _parent.capitalinfo = "";
    } // end else if
}

// [onClipEvent of sprite 1579 in frame 12]
onClipEvent (load)
{
    setProperty("_root.onlineplayerslist", _x, 0);
    setProperty("_root.onlineplayerslist", _y, 0);
    _root.playershipstatus[5][15] = "";
    clearInterval(_root.specialstimers[1]);
    clearInterval(_root.specialstimers[2]);
    _root._quality = "HIGH";
    if (_root.playerjustloggedin == false)
    {
        _root.hasplayerbeenlistedasdocked = false;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
        _root.playersdockedtime = 999999999;
        dockinginfosent = getTimer();
        dockinginfoerrorcheck = 10000;
    }
    else
    {
        dockinginfosent = 999999999;
        _root.hasplayerbeenlistedasdocked = true;
        _root.hassafeleavebeeddisp = true;
    } // end else if
    _root.hassafeleavebeeddisp = false;
    if (_root.playershipstatus[5][25] == "CAPLEFT")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Illegal Cap Left Detected \r Capital Ship Lost";
        _root.playershipstatus[5][25] = "YES";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
    _root.aishipshosted = new Array();
    this.mapdsiplayed = false;
}

// [onClipEvent of sprite 1579 in frame 12]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (_root.hasplayerbeenlistedasdocked == false && currenttime > dockinginfoerrorcheck + dockinginfosent && _root.playerjustloggedin == false)
    {
        dockinginfosent = currenttime;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
    } // end if
    if (_root.hasplayerbeenlistedasdocked == true && _root.hassafeleavebeeddisp == false && _root.playerscurrentextrashipno == "capital" && _root.playershipstatus[5][25] == "YES")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Safe to Exit Game \r With Capital Ship";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
}

// [onClipEvent of sprite 1579 in frame 12]
onClipEvent (keyDown)
{
    lastkeypressed = Key.getCode();
    if (lastkeypressed == "SDFdsf" && _root.mainscreenstarbase.shopselection == "none")
    {
        if (this.mapdsiplayed == false)
        {
            this.mapdsiplayed = true;
            _root.attachMovie("ingamemap", "ingamemap", 9999999);
            _root.mainscreenstarbase.gotoAndStop(2);
        }
        else if (this.mapdsiplayed == true)
        {
            this.mapdsiplayed = false;
            _root.createEmptyMovieClip("blanker", 9999999);
            _root.mainscreenstarbase.gotoAndStop(1);
            removeMovieClip ("ingamemap");
        } // end if
    } // end else if
    if (lastkeypressed == "123" && _root.mapdsiplayed == false)
    {
        if (_root.mainscreenstarbase.shopselection == "none")
        {
            if (currenthelpframedisplayed == 0)
            {
                _root.mapdsiplayed = false;
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "starbase" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "tradegoods")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "tradegoods" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "mission")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "mission" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "extraships")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "extraships" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "prefences")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "preferences" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "ship")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "ship" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "hardware")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "hardware" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end if
    } // end else if
    lastkeypressed = "";
}

// [onClipEvent of sprite 1579 in frame 12]
onClipEvent (load)
{
    function starbaseexitposition()
    {
        jjj = 0;
        databeenupdated = false;
        while (jjj < _root.starbaselocation.length && databeenupdated == false)
        {
            if (_root.starbaselocation[jjj][0] == _root.playershipstatus[4][0])
            {
                databeenupdated = true;
                dockingrange = 0;
                _root.shipcoordinatex = _root.starbaselocation[jjj][1] + dockingrange * 2 * Math.random() - dockingrange;
                _root.shipcoordinatey = _root.starbaselocation[jjj][2] + dockingrange * 2 * Math.random() - dockingrange;
            } // end if
            ++jjj;
        } // end while
    } // End of the function
    function determinegoods(loadedvars)
    {
        goodsinfo = loadedvars.split("~");
        newinfo = goodsinfo[0].split("`");
        i = 0;
        currenttradegood = 0;
        _root.starbasetradeitem = new Array();
        while (i < newinfo.length - 1)
        {
            if (newinfo[i].charAt(0) == "S")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "selling";
                ++currenttradegood;
            } // end if
            if (newinfo[i].charAt(0) == "B")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "buying";
                ++currenttradegood;
            } // end if
            ++i;
        } // end while
        _root.randomegameevents.func_altertradegooodpricing(_root.playershipstatus[4][0]);
    } // End of the function
    starbaseexitposition();
    _root.mainscreenstarbase.shopselection = "none";
    currenthelpframedisplayed = 0;
    removeMovieClip ("_root.toprightinformation");
    newtradeVars = new XML();
    newtradeVars.load(_root.pathtoaccounts + "tradegoods.php?system=" + _root.playershipstatus[5][1] + "&mode=prices&starbase=" + _root.playershipstatus[4][0]);
    newtradeVars.onLoad = function (success)
    {
        loadedvars = String(newtradeVars);
        if (loadedvars.length > 1)
        {
            determinegoods(loadedvars);
            
        } // end if
    };
    _root.mainscreenstarbase.welcometostarbase = "Welcome to StarBase " + _root.playershipstatus[4][0];
}

// [onClipEvent of sprite 1579 in frame 12]
onClipEvent (load)
{
    function enterintochat(info, colourtouse)
    {
        _root.gamechatinfo[1].splice(0, 0, 0);
        _root.gamechatinfo[1][0] = new Array();
        _root.gamechatinfo[1][0][0] = info;
        _root.gamechatinfo[1][0][1] = colourtouse;
        if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
        {
            _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
        } // end if
        _root.refreshchatdisplay = true;
    } // End of the function
    if (_root.playersmission[0][0][0] == "Cargorun")
    {
        currentlydockedbase = _root.playershipstatus[4][0];
        basesformission = _root.playersmission[0][0][1].split("`");
        if (_root.playersmission[0][4] == false)
        {
            if (currentlydockedbase == basesformission[0])
            {
                _root.playersmission[0][4] = true;
                enterintochat(" Cargo Picked Up! Mission begins once you exit the base.", _root.systemchattextcolor);
            } // end if
        }
        else if (currentlydockedbase != basesformission[1])
        {
            _root.playersmission = new Array();
            enterintochat(" Cargo Mission Failed!", _root.systemchattextcolor);
        }
        else
        {
            rewardamounts = _root.playersmission[0][2].split("`");
            if (_root.playersmission[0][4] == true)
            {
                reward = Number(rewardamounts[0]) + Number(rewardamounts[1]);
            }
            else
            {
                reward = Number(rewardamounts[0]);
            } // end else if
            enterintochat(" Cargo Mission Passed! Total Reward:" + reward, _root.systemchattextcolor);
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + reward;
            scoreaward = Math.round(Number(rewardamounts[0]) / _root.missiontoactualscoremodifier);
            information = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + scoreaward + "~";
            _root.mysocket.send(information);
            _root.playersmission = new Array();
        } // end else if
    } // end else if
}

// [onClipEvent of sprite 1752 in frame 14]
onClipEvent (load)
{
    setProperty("_root.onlineplayerslist", _x, 0);
    setProperty("_root.onlineplayerslist", _y, 0);
    _root.playershipstatus[5][15] = "";
    clearInterval(_root.specialstimers[1]);
    clearInterval(_root.specialstimers[2]);
    _root._quality = "HIGH";
    if (_root.playerjustloggedin == false)
    {
        _root.hasplayerbeenlistedasdocked = false;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
        _root.playersdockedtime = 999999999;
        dockinginfosent = getTimer();
        dockinginfoerrorcheck = 10000;
    }
    else
    {
        dockinginfosent = 999999999;
        _root.hasplayerbeenlistedasdocked = true;
    } // end else if
    _root.hassafeleavebeeddisp = false;
    if (_root.playershipstatus[5][25] == "CAPLEFT")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Illegal Cap Left Detected \r Capital Ship Lost";
        _root.playershipstatus[5][25] = "YES";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
    _root.aishipshosted = new Array();
    this.mapdsiplayed = false;
}

// [onClipEvent of sprite 1752 in frame 14]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (_root.hasplayerbeenlistedasdocked == false && currenttime > dockinginfoerrorcheck + dockinginfosent && _root.playerjustloggedin == false)
    {
        dockinginfosent = currenttime;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
    } // end if
    if (_root.hasplayerbeenlistedasdocked == true && _root.hassafeleavebeeddisp == false && _root.playerscurrentextrashipno == "capital" && _root.playershipstatus[5][25] == "YES")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Safe to Exit Game \r With Capital Ship";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
}

// [onClipEvent of sprite 1752 in frame 14]
onClipEvent (keyDown)
{
    lastkeypressed = Key.getCode();
    if (lastkeypressed == "SDFdsf" && _root.mainscreenstarbase.shopselection == "none")
    {
        if (this.mapdsiplayed == false)
        {
            this.mapdsiplayed = true;
            _root.attachMovie("ingamemap", "ingamemap", 9999999);
            _root.mainscreenstarbase.gotoAndStop(2);
        }
        else if (this.mapdsiplayed == true)
        {
            this.mapdsiplayed = false;
            _root.createEmptyMovieClip("blanker", 9999999);
            _root.mainscreenstarbase.gotoAndStop(1);
            removeMovieClip ("ingamemap");
        } // end if
    } // end else if
    if (lastkeypressed == "123" && _root.mapdsiplayed == false)
    {
        if (_root.mainscreenstarbase.shopselection == "none")
        {
            if (currenthelpframedisplayed == 0)
            {
                _root.mapdsiplayed = false;
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "starbase" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "tradegoods")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "tradegoods" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "mission")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "mission" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "extraships")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "extraships" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "prefences")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "preferences" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "ship")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "ship" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "hardware")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "hardware" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end if
    } // end else if
    lastkeypressed = "";
}

// [onClipEvent of sprite 1752 in frame 14]
onClipEvent (load)
{
    function starbaseexitposition()
    {
        jjj = 0;
        databeenupdated = false;
        while (jjj < _root.starbaselocation.length && databeenupdated == false)
        {
            if (_root.starbaselocation[jjj][0] == _root.playershipstatus[4][0])
            {
                databeenupdated = true;
                dockingrange = 0;
                _root.shipcoordinatex = _root.starbaselocation[jjj][1] + dockingrange * 2 * Math.random() - dockingrange;
                _root.shipcoordinatey = _root.starbaselocation[jjj][2] + dockingrange * 2 * Math.random() - dockingrange;
            } // end if
            ++jjj;
        } // end while
    } // End of the function
    function determinegoods(loadedvars)
    {
        goodsinfo = loadedvars.split("~");
        newinfo = goodsinfo[0].split("`");
        i = 0;
        currenttradegood = 0;
        _root.starbasetradeitem = new Array();
        while (i < newinfo.length - 1)
        {
            if (newinfo[i].charAt(0) == "S")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "selling";
                ++currenttradegood;
            } // end if
            if (newinfo[i].charAt(0) == "B")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "buying";
                ++currenttradegood;
            } // end if
            ++i;
        } // end while
    } // End of the function
    starbaseexitposition();
    _root.mainscreenstarbase.shopselection = "none";
    currenthelpframedisplayed = 0;
    removeMovieClip ("_root.toprightinformation");
    newtradeVars = new XML();
    newtradeVars.load(_root.pathtoaccounts + "tradegoods.php?system=" + _root.playershipstatus[5][1] + "&mode=prices&starbase=" + _root.playershipstatus[4][0]);
    newtradeVars.onLoad = function (success)
    {
        loadedvars = String(newtradeVars);
        if (loadedvars.length > 1)
        {
            determinegoods(loadedvars);
            
        } // end if
    };
    _root.mainscreenstarbase.welcometostarbase = "Welcome to StarBase " + _root.playershipstatus[4][0];
}

// [onClipEvent of sprite 1752 in frame 14]
onClipEvent (load)
{
    function enterintochat(info, colourtouse)
    {
        _root.gamechatinfo[1].splice(0, 0, 0);
        _root.gamechatinfo[1][0] = new Array();
        _root.gamechatinfo[1][0][0] = info;
        _root.gamechatinfo[1][0][1] = colourtouse;
        if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
        {
            _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
        } // end if
        _root.refreshchatdisplay = true;
    } // End of the function
    if (_root.playersmission[0][0][0] == "Cargorun")
    {
        currentlydockedbase = _root.playershipstatus[4][0];
        basesformission = _root.playersmission[0][0][1].split("`");
        if (_root.playersmission[0][4] == false)
        {
            if (currentlydockedbase == basesformission[0])
            {
                _root.playersmission[0][4] = true;
                enterintochat(" Cargo Picked Up! Mission begins once you exit the base.", _root.systemchattextcolor);
            } // end if
        }
        else if (currentlydockedbase != basesformission[1])
        {
            _root.playersmission = new Array();
            enterintochat(" Cargo Mission Failed!", _root.systemchattextcolor);
        }
        else
        {
            rewardamounts = _root.playersmission[0][2].split("`");
            if (_root.playersmission[0][4] == true)
            {
                reward = Number(rewardamounts[0]) + Number(rewardamounts[1]);
            }
            else
            {
                reward = Number(rewardamounts[0]);
            } // end else if
            enterintochat(" Cargo Mission Passed! Total Reward:" + reward, _root.systemchattextcolor);
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + reward;
            scoreaward = Math.round(Number(rewardamounts[0]) / _root.missiontoactualscoremodifier);
            information = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + scoreaward + "~";
            _root.mysocket.send(information);
            _root.playersmission = new Array();
        } // end else if
    } // end else if
}

// [onClipEvent of sprite 1773 in frame 15]
onClipEvent (load)
{
    setProperty("_root.onlineplayerslist", _x, 0);
    setProperty("_root.onlineplayerslist", _y, 0);
    _root.playershipstatus[5][15] = "";
    clearInterval(_root.specialstimers[1]);
    clearInterval(_root.specialstimers[2]);
    _root._quality = "HIGH";
    if (_root.playerjustloggedin == false)
    {
        _root.hasplayerbeenlistedasdocked = false;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
        _root.playersdockedtime = 999999999;
        dockinginfosent = getTimer();
        dockinginfoerrorcheck = 10000;
    }
    else
    {
        dockinginfosent = 999999999;
        _root.hasplayerbeenlistedasdocked = true;
    } // end else if
    _root.hassafeleavebeeddisp = false;
    if (_root.playershipstatus[5][25] == "CAPLEFT")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Illegal Cap Left Detected \r Capital Ship Lost";
        _root.playershipstatus[5][25] = "YES";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
    _root.aishipshosted = new Array();
    this.mapdsiplayed = false;
}

// [onClipEvent of sprite 1773 in frame 15]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (_root.hasplayerbeenlistedasdocked == false && currenttime > dockinginfoerrorcheck + dockinginfosent && _root.playerjustloggedin == false)
    {
        dockinginfosent = currenttime;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
    } // end if
    if (_root.hasplayerbeenlistedasdocked == true && _root.hassafeleavebeeddisp == false && _root.playerscurrentextrashipno == "capital" && _root.playershipstatus[5][25] == "YES")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Safe to Exit Game \r With Capital Ship";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
}

// [onClipEvent of sprite 1773 in frame 15]
onClipEvent (keyDown)
{
    lastkeypressed = Key.getCode();
    if (lastkeypressed == "SDFdsf" && _root.mainscreenstarbase.shopselection == "none")
    {
        if (this.mapdsiplayed == false)
        {
            this.mapdsiplayed = true;
            _root.attachMovie("ingamemap", "ingamemap", 9999999);
            _root.mainscreenstarbase.gotoAndStop(2);
        }
        else if (this.mapdsiplayed == true)
        {
            this.mapdsiplayed = false;
            _root.createEmptyMovieClip("blanker", 9999999);
            _root.mainscreenstarbase.gotoAndStop(1);
            removeMovieClip ("ingamemap");
        } // end if
    } // end else if
    if (lastkeypressed == "123" && _root.mapdsiplayed == false)
    {
        if (_root.mainscreenstarbase.shopselection == "none")
        {
            if (currenthelpframedisplayed == 0)
            {
                _root.mapdsiplayed = false;
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "starbase" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "tradegoods")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "tradegoods" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "mission")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "mission" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "extraships")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "extraships" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "prefences")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "preferences" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "ship")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "ship" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "hardware")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "hardware" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end if
    } // end else if
    lastkeypressed = "";
}

// [onClipEvent of sprite 1773 in frame 15]
onClipEvent (load)
{
    function starbaseexitposition()
    {
        jjj = 0;
        databeenupdated = false;
        while (jjj < _root.starbaselocation.length && databeenupdated == false)
        {
            if (_root.starbaselocation[jjj][0] == _root.playershipstatus[4][0])
            {
                databeenupdated = true;
                dockingrange = 0;
                _root.shipcoordinatex = _root.starbaselocation[jjj][1] + dockingrange * 2 * Math.random() - dockingrange;
                _root.shipcoordinatey = _root.starbaselocation[jjj][2] + dockingrange * 2 * Math.random() - dockingrange;
            } // end if
            ++jjj;
        } // end while
    } // End of the function
    function determinegoods(loadedvars)
    {
        goodsinfo = loadedvars.split("~");
        newinfo = goodsinfo[0].split("`");
        i = 0;
        currenttradegood = 0;
        _root.starbasetradeitem = new Array();
        while (i < newinfo.length - 1)
        {
            if (newinfo[i].charAt(0) == "S")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "selling";
                ++currenttradegood;
            } // end if
            if (newinfo[i].charAt(0) == "B")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "buying";
                ++currenttradegood;
            } // end if
            ++i;
        } // end while
    } // End of the function
    starbaseexitposition();
    _root.mainscreenstarbase.shopselection = "none";
    currenthelpframedisplayed = 0;
    removeMovieClip ("_root.toprightinformation");
    newtradeVars = new XML();
    newtradeVars.load(_root.pathtoaccounts + "tradegoods.php?system=" + _root.playershipstatus[5][1] + "&mode=prices&starbase=" + _root.playershipstatus[4][0]);
    newtradeVars.onLoad = function (success)
    {
        loadedvars = String(newtradeVars);
        if (loadedvars.length > 1)
        {
            determinegoods(loadedvars);
            
        } // end if
    };
    _root.mainscreenstarbase.welcometostarbase = "Welcome to StarBase " + _root.playershipstatus[4][0];
}

// [onClipEvent of sprite 1773 in frame 15]
onClipEvent (load)
{
    function enterintochat(info, colourtouse)
    {
        _root.gamechatinfo[1].splice(0, 0, 0);
        _root.gamechatinfo[1][0] = new Array();
        _root.gamechatinfo[1][0][0] = info;
        _root.gamechatinfo[1][0][1] = colourtouse;
        if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
        {
            _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
        } // end if
        _root.refreshchatdisplay = true;
    } // End of the function
    if (_root.playersmission[0][0][0] == "Cargorun")
    {
        currentlydockedbase = _root.playershipstatus[4][0];
        basesformission = _root.playersmission[0][0][1].split("`");
        if (_root.playersmission[0][4] == false)
        {
            if (currentlydockedbase == basesformission[0])
            {
                _root.playersmission[0][4] = true;
                enterintochat(" Cargo Picked Up! Mission begins once you exit the base.", _root.systemchattextcolor);
            } // end if
        }
        else if (currentlydockedbase != basesformission[1])
        {
            _root.playersmission = new Array();
            enterintochat(" Cargo Mission Failed!", _root.systemchattextcolor);
        }
        else
        {
            rewardamounts = _root.playersmission[0][2].split("`");
            if (_root.playersmission[0][4] == true)
            {
                reward = Number(rewardamounts[0]) + Number(rewardamounts[1]);
            }
            else
            {
                reward = Number(rewardamounts[0]);
            } // end else if
            enterintochat(" Cargo Mission Passed! Total Reward:" + reward, _root.systemchattextcolor);
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + reward;
            scoreaward = Math.round(Number(rewardamounts[0]) / _root.missiontoactualscoremodifier);
            information = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + scoreaward + "~";
            _root.mysocket.send(information);
            _root.playersmission = new Array();
        } // end else if
    } // end else if
}

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (load)
{
    _root.playershipstatus[5][15] = "";
    clearInterval(_root.specialstimers[1]);
    clearInterval(_root.specialstimers[2]);
    _root.attachMovie("onlineplayerslist", "onlineplayerslist", 99982);
    setProperty("_root.onlineplayerslist", _x, 0);
    setProperty("_root.onlineplayerslist", _y, 0);
    if (_root.playerjustloggedin == false)
    {
        _root.hasplayerbeenlistedasdocked = false;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
        _root.playersdockedtime = 999999999;
        dockinginfosent = getTimer();
        dockinginfoerrorcheck = 10000;
    }
    else
    {
        dockinginfosent = 999999999;
        _root.hasplayerbeenlistedasdocked = true;
    } // end else if
    _root.hassafeleavebeeddisp = false;
    if (_root.playershipstatus[5][25] == "CAPLEFT")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Illegal Cap Left Detected \r Capital Ship Lost";
        _root.playershipstatus[5][25] = "YES";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
    _root.aishipshosted = new Array();
    this.mapdsiplayed = false;
}

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (enterFrame)
{
    currenttime = getTimer();
    if (_root.hasplayerbeenlistedasdocked == false && currenttime > dockinginfoerrorcheck + dockinginfosent && _root.playerjustloggedin == false)
    {
        dockinginfosent = currenttime;
        information = "PI`CLEAR`~";
        _root.mysocket.send(information);
    } // end if
    if (_root.hasplayerbeenlistedasdocked == true && _root.hassafeleavebeeddisp == false && _root.playerscurrentextrashipno == "capital" && _root.playershipstatus[5][25] == "YES")
    {
        _root.hassafeleavebeeddisp = true;
        _parent.attachMovie("safetoexitwithcapbox", "safetoexitwithcapbox", 1555);
        _parent.safetoexitwithcapbox.information = "Safe to Exit Game \r With Capital Ship";
        _parent.safetoexitwithcapbox._x = 450;
        _parent.safetoexitwithcapbox._y = 75;
    } // end if
}

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (keyDown)
{
    lastkeypressed = Key.getCode();
    if (lastkeypressed == "SDFdsf" && _root.mainscreenstarbase.shopselection == "none")
    {
        if (this.mapdsiplayed == false)
        {
            this.mapdsiplayed = true;
            _root.attachMovie("ingamemap", "ingamemap", 9999999);
            _root.mainscreenstarbase.gotoAndStop(2);
        }
        else if (this.mapdsiplayed == true)
        {
            this.mapdsiplayed = false;
            _root.createEmptyMovieClip("blanker", 9999999);
            _root.mainscreenstarbase.gotoAndStop(1);
            removeMovieClip ("ingamemap");
        } // end if
    } // end else if
    if (lastkeypressed == "123" && _root.mapdsiplayed == false)
    {
        if (_root.mainscreenstarbase.shopselection == "none")
        {
            if (currenthelpframedisplayed == 0)
            {
                _root.mapdsiplayed = false;
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "starbase" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "tradegoods")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "tradegoods" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "mission")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "mission" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "extraships")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "extraships" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "prefences")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "preferences" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "ship")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "ship" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end else if
        if (_root.mainscreenstarbase.shopselection == "hardware")
        {
            if (currenthelpframedisplayed == 0)
            {
                currenthelpframedisplayed = 1;
                _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                _root.ingamehelp.helpmode = "hardware" + currenthelpframedisplayed;
            }
            else if (currenthelpframedisplayed == 1)
            {
                currenthelpframedisplayed = 0;
                _root.ingamehelp.gotoAndPlay("close");
            } // end if
        } // end if
    } // end else if
    lastkeypressed = "";
}

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (load)
{
    function starbaseexitposition()
    {
        jjj = 0;
        databeenupdated = false;
        while (jjj < _root.starbaselocation.length && databeenupdated == false)
        {
            if (_root.starbaselocation[jjj][0] == _root.playershipstatus[4][0])
            {
                databeenupdated = true;
                dockingrange = 0;
                _root.shipcoordinatex = _root.starbaselocation[jjj][1] + dockingrange * 2 * Math.random() - dockingrange;
                _root.shipcoordinatey = _root.starbaselocation[jjj][2] + dockingrange * 2 * Math.random() - dockingrange;
            } // end if
            ++jjj;
        } // end while
    } // End of the function
    function determinegoods(loadedvars)
    {
        goodsinfo = loadedvars.split("~");
        newinfo = goodsinfo[0].split("`");
        i = 0;
        currenttradegood = 0;
        _root.starbasetradeitem = new Array();
        while (i < newinfo.length - 1)
        {
            if (newinfo[i].charAt(0) == "S")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "selling";
                ++currenttradegood;
            } // end if
            if (newinfo[i].charAt(0) == "B")
            {
                _root.starbasetradeitem[currenttradegood] = new Array();
                _root.starbasetradeitem[currenttradegood][0] = i;
                _root.starbasetradeitem[currenttradegood][1] = Number(newinfo[i].substr("1"));
                _root.starbasetradeitem[currenttradegood][2] = "buying";
                ++currenttradegood;
            } // end if
            ++i;
        } // end while
        _root.randomegameevents.func_altertradegooodpricing(_root.playershipstatus[4][0]);
    } // End of the function
    starbaseexitposition();
    _root.mainscreenstarbase.shopselection = "none";
    currenthelpframedisplayed = 0;
    removeMovieClip ("_root.toprightinformation");
    newtradeVars = new XML();
    newtradeVars.load(_root.pathtoaccounts + "tradegoods.php?system=" + _root.playershipstatus[5][1] + "&mode=prices&starbase=" + _root.playershipstatus[4][0]);
    newtradeVars.onLoad = function (success)
    {
        loadedvars = String(newtradeVars);
        if (loadedvars.length > 1)
        {
            determinegoods(loadedvars);
            
        } // end if
    };
    _root.mainscreenstarbase.welcometostarbase = "Welcome to StarBase " + _root.playershipstatus[4][0];
}

// [onClipEvent of sprite 1792 in frame 16]
onClipEvent (load)
{
    function enterintochat(info, colourtouse)
    {
        _root.gamechatinfo[1].splice(0, 0, 0);
        _root.gamechatinfo[1][0] = new Array();
        _root.gamechatinfo[1][0][0] = info;
        _root.gamechatinfo[1][0][1] = colourtouse;
        if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
        {
            _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
        } // end if
        _root.refreshchatdisplay = true;
    } // End of the function
    if (_root.playersmission[0][0][0] == "Cargorun")
    {
        currentlydockedbase = _root.playershipstatus[4][0];
        basesformission = _root.playersmission[0][0][1].split("`");
        if (_root.playersmission[0][4] == false)
        {
            if (currentlydockedbase == basesformission[0])
            {
                _root.playersmission[0][4] = true;
                enterintochat(" Cargo Picked Up! Mission begins once you exit the base.", _root.systemchattextcolor);
            } // end if
        }
        else if (currentlydockedbase != basesformission[1])
        {
            _root.playersmission = new Array();
            enterintochat(" Cargo Mission Failed!", _root.systemchattextcolor);
        }
        else
        {
            rewardamounts = _root.playersmission[0][2].split("`");
            if (_root.playersmission[0][4] == true)
            {
                reward = Number(rewardamounts[0]) + Number(rewardamounts[1]);
            }
            else
            {
                reward = Number(rewardamounts[0]);
            } // end else if
            enterintochat(" Cargo Mission Passed! Total Reward:" + reward, _root.systemchattextcolor);
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + reward;
            scoreaward = Math.round(Number(rewardamounts[0]) / _root.missiontoactualscoremodifier);
            information = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + scoreaward + "~";
            _root.mysocket.send(information);
            _root.playersmission = new Array();
        } // end else if
    } // end else if
}

// [Action in Frame 1]
function func_checkfortoomuchaddedandset()
{
    if (_root.playershipstatus[3][1] < 1)
    {
        _root.playershipstatus[3][1] = 1;
    } // end if
    oldnumbs = func_converttonumberfund(_root.oldcodedfunds);
    if (_root.playershipstatus[3][1] - oldnumbs > maxjfundsjump)
    {
        _root.playershipstatus[3][1] = Number(oldnumbs);
    }
    else
    {
        func_setoldfundsuptocurrentammount();
    } // end else if
} // End of the function
function func_setoldfundsuptocurrentammount()
{
    _root.oldcodedfunds = func_converttocodedfund(_root.playershipstatus[3][1]);
} // End of the function
function func_converttocodedfund(anumber)
{
    anumber = String(anumber);
    letteredfund = "";
    for (qtt = 0; qtt < anumber.length; qtt++)
    {
        numcharactertouse = anumber.charAt(qtt);
        letteredfund = letteredfund + fundcodedconversionchars[Number(numcharactertouse)];
    } // end of for
    return (letteredfund);
} // End of the function
function func_converttonumberfund(astring)
{
    astring = String(astring);
    stringnumberfund = "";
    for (qtt = 0; qtt < astring.length; qtt++)
    {
        lettertocheck = astring.charAt(qtt);
        for (jjp = 0; jjp <= fundcodedconversionchars.length; jjp++)
        {
            if (lettertocheck == fundcodedconversionchars[jjp])
            {
                stringnumberfund = stringnumberfund + String(jjp);
                jjp = 999;
            } // end if
        } // end of for
    } // end of for
    return (Number(stringnumberfund));
} // End of the function
maxjfundsjump = 25000000;
_root.oldcodedfunds = "";
fundcodedconversionchars = new Array("A", "B", "T", "Q", "R", "S", "P", "M", "N", "E");

function func_disconnectuser(reason)
{
    _root.controlledserverclose = true;
    _root.gameerror = reason;
    _root.func_closegameserver();
} // End of the function

function func_keyboardkeysound()
{
    keyboardkeysound.start();
} // End of the function
function func_main_clicksound()
{
    if (_root.soundvolume != "off")
    {
        main_clicksound.start();
    } // end if
} // End of the function
function func_minor_clicksound()
{
    if (_root.soundvolume != "off")
    {
        minor_clicksound.start();
    } // end if
} // End of the function
function func_enterpresssound()
{
    if (_root.soundvolume != "off")
    {
        enterpresssound.start();
    } // end if
} // End of the function
function func_login_button_press_sound()
{
    login_button_press_sound.start();
} // End of the function
function func_connecting_sound()
{
    connecting_sound.start();
} // End of the function
function func_error_sound()
{
    if (_root.soundvolume != "off")
    {
        error_sound.start();
    } // end if
} // End of the function
function func_login_but_pops_sound()
{
    if (_root.soundvolume != "off")
    {
        login_but_pops_sound.start();
    } // end if
} // End of the function
function func_login_opening_sound()
{
    if (_root.soundvolume != "off")
    {
        login_opening_sound.start();
    } // end if
} // End of the function
function func_keyboardkeysound()
{
    keyboardkeysound.start();
} // End of the function
function func_main_clicksound()
{
    if (_root.soundvolume != "off")
    {
        main_clicksound.start();
    } // end if
} // End of the function
function func_minor_clicksound()
{
    if (_root.soundvolume != "off")
    {
        minor_clicksound.start();
    } // end if
} // End of the function
function func_enterpresssound()
{
    if (_root.soundvolume != "off")
    {
        enterpresssound.start();
    } // end if
} // End of the function
function func_connecting_sound()
{
    connecting_sound.start();
} // End of the function
function func_error_sound()
{
    if (_root.soundvolume != "off")
    {
        error_sound.start();
    } // end if
} // End of the function
_root.musicvolume = "on";
keyboardkeysound = new Sound();
keyboardkeysound.attachSound("typekeypress.wav");
main_clicksound = new Sound();
main_clicksound.attachSound("main click.wav");
minor_clicksound = new Sound();
minor_clicksound.attachSound("minor click.wav");
enterpresssound = new Sound();
enterpresssound.attachSound("enterpress.wav");
login_button_press_sound = new Sound();
login_button_press_sound.attachSound("login button press.mp3");
connecting_sound = new Sound();
connecting_sound.attachSound("Conneting Sound.mp3");
error_sound = new Sound();
error_sound.attachSound("error.wav");
login_but_pops_sound = new Sound();
login_but_pops_sound.attachSound("login but pops.wav");
login_opening_sound = new Sound();
login_opening_sound.attachSound("opening.wav");
keyboardkeysound = new Sound();
keyboardkeysound.attachSound("typekeypress.wav");
main_clicksound = new Sound();
main_clicksound.attachSound("main click.wav");
minor_clicksound = new Sound();
minor_clicksound.attachSound("minor click.wav");
enterpresssound = new Sound();
enterpresssound.attachSound("enterpress.wav");
connecting_sound = new Sound();
connecting_sound.attachSound("Conneting Sound.mp3");
error_sound = new Sound();
error_sound.attachSound("error.wav");

function enterbadwords()
{
    badwords = new Array();
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "fuck";
    badwords[loc][1] = "f**k";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "shit";
    badwords[loc][1] = "s**t";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "asshole";
    badwords[loc][1] = "a**h*le";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "nigger";
    badwords[loc][1] = "ni**er";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "whore";
    badwords[loc][1] = "wh**e";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "cunt";
    badwords[loc][1] = "c*nt";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "cock";
    badwords[loc][1] = "c*ck";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "pussy";
    badwords[loc][1] = "pu**y";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "fag";
    badwords[loc][1] = "f*g";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "bitch";
    badwords[loc][1] = "b**ch";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "rape";
    badwords[loc][1] = "r*pe";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "homo";
    badwords[loc][1] = "h*m*";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "jackass";
    badwords[loc][1] = "j**k*ss";
    loc = badwords.length;
    badwords[loc] = new Array();
    badwords[loc][0] = "fuk";
    badwords[loc][1] = "f*k";
} // End of the function
function badwordfiltering(phraseinput)
{
    phrasetocheckwith = String(phraseinput.toLowerCase());
    for (qq = 0; qq < phraseinput.length; qq++)
    {
        for (jj = 0; jj < badwords.length; jj++)
        {
            if (phrasetocheckwith.substr(qq, badwords[jj][0].length) == badwords[jj][0])
            {
                beginnign = phraseinput.substr(0, qq);
                ending = phraseinput.substr(qq + badwords[jj][0].length);
                phraseinput = beginnign + badwords[jj][1] + ending;
                break;
            } // end if
        } // end of for
    } // end of for
    return (phraseinput);
} // End of the function
function func_checkothercharacters(phraseoutput)
{
    for (qq = 0; qq < phraseoutput.length; qq++)
    {
        for (jj = 0; jj < replacewithchar.length; jj++)
        {
            if (phraseoutput.substr(qq, replacewithchar[jj][0].length) == replacewithchar[jj][0])
            {
                beginnign = phraseoutput.substr(0, qq);
                ending = phraseoutput.substr(qq + replacewithchar[jj][0].length);
                phraseoutput = beginnign + replacewithchar[jj][1] + ending;
                if (qq >= phraseoutput.length)
                {
                    qq = 10000;
                } // end if
                break;
            } // end if
        } // end of for
    } // end of for
    return (phraseoutput);
} // End of the function
enterbadwords();
replacewithchar = new Array();
loc = replacewithchar.length;
replacewithchar[loc] = new Array();
replacewithchar[loc][0] = "&apos;";
replacewithchar[loc][1] = "\'";
loc = replacewithchar.length;
replacewithchar[loc] = new Array();
replacewithchar[loc][0] = "&quot;";
replacewithchar[loc][1] = "\"";
loc = replacewithchar.length;
replacewithchar[loc] = new Array();
replacewithchar[loc][0] = "&gt;";
replacewithchar[loc][1] = ">";
loc = replacewithchar.length;
replacewithchar[loc] = new Array();
replacewithchar[loc][0] = "&amp;";
replacewithchar[loc][1] = "&";

function func_beginmeteor()
{
    meteortarget = null;
    checkattempts = 30;
    for (ww = 0; ww < checkattempts; ww++)
    {
        targetnumber = Math.round(Math.random() * (_root.starbaselocation.length - 1));
        if (_root.starbaselocation[targetnumber][5] == "ACTIVE" && _root.starbaselocation[targetnumber][0].substr(0, 2) == "SB")
        {
            meteortarget = targetnumber;
            break;
        } // end if
    } // end of for
    if (meteortarget != null && _root.incomingmeteor.length < 4)
    {
        targetname = _root.starbaselocation[targetnumber][0];
        meteortype = Math.round(Math.random() * (destroyingmeteor.length - 1));
        meteorincomingangle = Math.round(Math.random() * 360);
        idtosend = "*M" + Math.round(Math.random() * 99);
        meteorlife = destroyingmeteor[meteortype][3];
        meteormovingagle = meteorincomingangle - 180;
        meteorlifetimeinsec = Math.round(destroyingmeteor[meteortype][1] / 1000);
        if (meteormovingagle < 0)
        {
            meteormovingagle = meteormovingagle + 360;
        } // end if
        timetillhit = Math.round(destroyingmeteor[meteortype][1] / 1000);
        datatosend = "MET`CREATE`" + idtosend + "`" + meteormovingagle + "`" + targetname + "`" + meteortype + "`" + meteorlife + "`" + timetillhit + "`" + _root.errorchecknumber + "~";
        _root.mysocket.send(datatosend);
        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
    } // end if
} // End of the function
function func_meteoricoming(meteorid, meteorheading, targetname, meteortype, meterorlife, timeelapsed)
{
    meteorid = String(meteorid);
    idtaken = false;
    for (qqq = 0; qqq < _root.incomingmeteor.length; qqq++)
    {
        if (meteorid == _root.incomingmeteor[qqq][0])
        {
            idtaken = true;
            break;
        } // end if
    } // end of for
    if (idtaken == false)
    {
        if (_root.incomingmeteor.length < 1)
        {
            _root.incomingmeteor = new Array();
        } // end if
        meteortype = Number(meteortype);
        timeelapsed = Number(timeelapsed);
        meteorheading = Number(meteorheading);
        for (ww = 0; ww < _root.starbaselocation.length; ww++)
        {
            if (String(targetname) == _root.starbaselocation[ww][0])
            {
                targetnumber = ww;
                break;
            } // end if
        } // end of for
        timetillhit = destroyingmeteor[meteortype][1] - timeelapsed * 1000;
        meterortargetx = _root.starbaselocation[targetnumber][1];
        meterortargety = _root.starbaselocation[targetnumber][2];
        velocity = destroyingmeteor[meteortype][0];
        ymovementpersec = -velocity * _root.cosines[meteorheading];
        xmovementpersec = velocity * _root.sines[meteorheading];
        currenty = Number(meterortargety) - destroyingmeteor[meteortype][1] / 1000 * ymovementpersec;
        currentx = Number(meterortargetx) - destroyingmeteor[meteortype][1] / 1000 * xmovementpersec;
        location = _root.incomingmeteor.length;
        _root.incomingmeteor[location] = new Array();
        _root.incomingmeteor[location][0] = meteorid;
        _root.incomingmeteor[location][1] = currentx;
        _root.incomingmeteor[location][2] = currenty;
        _root.incomingmeteor[location][3] = meteorheading;
        _root.incomingmeteor[location][4] = targetname;
        _root.incomingmeteor[location][5] = timetillimpact;
        _root.incomingmeteor[location][6] = meterorlife;
        _root.incomingmeteor[location][7] = meteortype;
        _root.incomingmeteor[location][8] = getTimer();
        _root.incomingmeteor[location][9] = getTimer() + (destroyingmeteor[meteortype][1] - timeelapsed * 1000) - timetocheckmeteorbeforehitting;
        _root.incomingmeteor[location][10] = xmovementpersec;
        _root.incomingmeteor[location][11] = ymovementpersec;
        _root.incomingmeteor[location][12] = getTimer() + (destroyingmeteor[meteortype][1] - timeelapsed * 1000);
        _root.incomingmeteor[location][13] = destroyingmeteor[meteortype][2];
        timetillhitleft = Math.round((_root.incomingmeteor[location][12] - getTimer()) / 1000);
        message = "Meteor Located: Target " + targetname + " ETA: " + timetillhitleft + " s, Check Map for location.";
        _root.enterintochat(message, _root.systemchattextcolor);
        _root.gamedisplayarea.func_meteordraw("ADD", meteorid);
        newsmessage = "There are reports of a meteor heading towards " + targetname + ". Its arrival time is " + " in " + timetillhitleft + " seconds. It will destroy the base if not stopped!";
        _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
    } // end if
} // End of the function
function func_meteorhitbase(meteorid)
{
    for (wq = 0; wq < _root.incomingmeteor.length; wq++)
    {
        if (meteorid == _root.incomingmeteor[wq][0])
        {
            destroyedbase = _root.incomingmeteor[wq][4];
            destroyedtime = getTimer() + _root.incomingmeteor[wq][13];
            for (hfd = 0; hfd < _root.starbaselocation.length; hfd++)
            {
                if (destroyedbase == _root.starbaselocation[hfd][0])
                {
                    if (_root.starbaselocation[hfd][5] == "ACTIVE")
                    {
                        _root.starbaselocation[hfd][5] = destroyedtime;
                        message = "Starbase " + destroyedbase + " was destroyed by a meteor for " + Math.round(_root.incomingmeteor[wq][13] / 1000) + " seconds";
                        _root.enterintochat(message, _root.systemchattextcolor);
                        _root.gamedisplayarea.func_meteordraw("REMOVE", meteorid);
                        newsmessage = "Starbase " + targetname + " has been destroyed by a meteor. It will take " + Math.round(_root.incomingmeteor[wq][13] / 1000) + " seconds until it will be repaired.";
                        _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
                        _root.gamedisplayarea.gamebackground[destroyedbase].basedestroyed = true;
                    }
                    else
                    {
                        message = "Starbase " + destroyedbase + " was already destroyed by a meteor";
                        _root.enterintochat(message, _root.systemchattextcolor);
                        _root.gamedisplayarea.func_meteordraw("REMOVE", meteorid);
                    } // end else if
                    break;
                } // end if
            } // end of for
            break;
        } // end if
    } // end of for
} // End of the function
function func_meteordamaged(meteorid, destroyedbase)
{
    _root.gamedisplayarea.func_meteordraw("REMOVE", meteorid);
    message = "A meteor was destroyed";
    _root.enterintochat(message, _root.systemchattextcolor);
} // End of the function
function func_removemeteor(meteorid)
{
} // End of the function
function func_meteorlife(meteorid, meteorlife)
{
    meteorlife = Number(meteorlife);
    for (location = 0; location < _root.incomingmeteor.length; location++)
    {
        if (_root.incomingmeteor[location][0] == meteorid)
        {
            if (_root.incomingmeteor[location][6] > meteorlife)
            {
                _root.incomingmeteor[location][6] = meteorlife;
                _root.gamedisplayarea.gamebackground["meteor" + meteorid].func_currentlife(meteorlife);
                break;
            } // end if
        } // end if
    } // end of for
} // End of the function
function func_meteordestroyer(meteorid, killer)
{
    for (location = 0; location < _root.incomingmeteor.length; location++)
    {
        if (_root.incomingmeteor[location][0] == meteorid)
        {
            _root.gamedisplayarea.func_meteordraw("DESTROYED", meteorid);
            meteortype = _root.incomingmeteor[location][7];
            target = _root.incomingmeteor[location][4];
            newsmessage = "A meteor heading towards Starbase " + targetname + " has been destroyed by " + func_playersrealname(killer) + " for " + destroyingmeteor[meteortype][4] + " funds and " + destroyingmeteor[meteortype][5] + " score";
            _root.func_messangercom(newsmessage, "NEWS", "BEGIN");
            message = "A meteor was destroyed by " + func_playersrealname(killer);
            _root.enterintochat(message, _root.systemchattextcolor);
            _root.incomingmeteor.splice(location, 1);
            --location;
            if (String(_root.playershipstatus[3][0]) == String(killer))
            {
                datatosend = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + destroyingmeteor[meteortype][5] * _root.scoreratiomodifier;
                "~";
                _root.mysocket.send(datatosend);
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(destroyingmeteor[meteortype][4]);
                _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
            } // end if
        } // end if
    } // end of for
} // End of the function
currentno = 0;
currentmeteordrawlevel = 0;
maxmeteordrawlevel = 20;
timetocheckmeteorbeforehitting = 12000;
destroyingmeteor = new Array();
destroyingmeteor[currentno] = new Array();
destroyingmeteor[currentno][0] = 30;
destroyingmeteor[currentno][1] = 120000;
destroyingmeteor[currentno][2] = 180000;
destroyingmeteor[currentno][3] = 50000;
destroyingmeteor[currentno][4] = 65000;
destroyingmeteor[currentno][5] = 150;
++currentno;
destroyingmeteor[currentno] = new Array();
destroyingmeteor[currentno][0] = 20;
destroyingmeteor[currentno][1] = 180000;
destroyingmeteor[currentno][2] = 240000;
destroyingmeteor[currentno][3] = 100000;
destroyingmeteor[currentno][4] = 130000;
destroyingmeteor[currentno][5] = 220;

function changetonewship(extrano)
{
    i = _root.extraplayerships[extrano][0];
    if (_root.playershipstatus[5][0] == i)
    {
    }
    else
    {
        _root.playershipstatus[5][0] = i;
        _root.playershipstatus[4][1] = new Array();
    } // end else if
    _root.playershiprotation = _root.shiptype[i][3][2];
    _root.playershipmaxvelocity = _root.shiptype[i][3][1];
    _root.playershipacceleration = _root.shiptype[i][3][0];
    _root.playershipstatus[2][4] = _root.shiptype[i][3][3];
    _root.playershipstatus[0] = new Array();
    for (currentharpoint = 0; currentharpoint < _root.shiptype[i][2].length; currentharpoint++)
    {
        _root.playershipstatus[0][currentharpoint] = new Array();
        _root.playershipstatus[0][currentharpoint][0] = _root.extraplayerships[extrano][4][currentharpoint];
        _root.playershipstatus[0][currentharpoint][1] = 0;
        _root.playershipstatus[0][currentharpoint][2] = _root.shiptype[i][2][currentharpoint][0];
        _root.playershipstatus[0][currentharpoint][3] = _root.shiptype[i][2][currentharpoint][1];
    } // end of for
    currentturretpoint = 0;
    _root.playershipstatus[8] = new Array();
    while (currentturretpoint < _root.shiptype[i][5].length)
    {
        _root.playershipstatus[8][currentturretpoint] = new Array();
        _root.playershipstatus[8][currentturretpoint][0] = _root.extraplayerships[extrano][5][currentturretpoint];
        ++currentturretpoint;
    } // end while
    _root.playershipstatus[11] = new Array();
    _root.playershipstatus[11][0] = new Array();
    _root.playershipstatus[11][0][0] = 0;
    _root.playershipstatus[11][0][1] = 0;
    _root.playershipstatus[11][1] = new Array();
    _root.playershipstatus[11][2] = new Array();
    _root.playershipstatus[11][2][0] = new Array();
    _root.playershipstatus[11][2][0][0] = 0;
    _root.playershipstatus[11][2][1] = new Array();
    _root.playershipstatus[11][2][1][0] = 0;
    _root.playershipstatus[11][2][2] = new Array();
    _root.playershipstatus[11][2][2][0] = 0;
    for (spno = 0; spno < _root.extraplayerships[extrano][11][1].length; spno++)
    {
        _root.playershipstatus[11][1][spno] = new Array();
        _root.playershipstatus[11][1][spno][0] = _root.extraplayerships[extrano][11][1][spno][0];
        _root.playershipstatus[11][1][spno][1] = _root.extraplayerships[extrano][11][1][spno][1];
    } // end of for
    _root.playershipstatus[2][0] = _root.extraplayerships[extrano][1];
    _root.playershipstatus[1][0] = _root.extraplayerships[extrano][2];
    _root.playershipstatus[1][5] = _root.extraplayerships[extrano][3];
} // End of the function
function savecurrenttoextraship(extrano)
{
    _root.extraplayerships[extrano][0] = _root.playershipstatus[5][0];
    i = _root.playershipstatus[5][0];
    _root.extraplayerships[extrano][4] = new Array();
    for (currentharpoint = 0; currentharpoint < _root.shiptype[i][2].length; currentharpoint++)
    {
        _root.extraplayerships[extrano][4][currentharpoint] = _root.playershipstatus[0][currentharpoint][0];
    } // end of for
    currentturretpoint = 0;
    _root.extraplayerships[extrano][5] = new Array();
    while (currentturretpoint < _root.shiptype[i][5].length)
    {
        _root.extraplayerships[extrano][5][currentturretpoint] = _root.playershipstatus[8][currentturretpoint][0];
        ++currentturretpoint;
    } // end while
    spno = 0;
    _root.extraplayerships[extrano][11] = new Array();
    _root.extraplayerships[extrano][11][1] = new Array();
    while (spno < _root.playershipstatus[11][1].length)
    {
        _root.extraplayerships[extrano][11][1][spno] = new Array();
        _root.extraplayerships[extrano][11][1][spno][0] = _root.playershipstatus[11][1][spno][0];
        _root.extraplayerships[extrano][11][1][spno][1] = _root.playershipstatus[11][1][spno][1];
        ++spno;
    } // end while
    _root.extraplayerships[extrano][1] = _root.playershipstatus[2][0];
    _root.extraplayerships[extrano][2] = _root.playershipstatus[1][0];
    _root.extraplayerships[extrano][3] = _root.playershipstatus[1][5];
} // End of the function
function loadplayerextraships(loadedvars)
{
    newinfo = loadedvars.split("~");
    i = 0;
    _root.extraplayerships = new Array();
    for (i = 0; i < maxextraships; i++)
    {
        _root.extraplayerships[i] = new Array();
        _root.extraplayerships[i][0] = null;
    } // end of for
    _root.playerscurrentextrashipno = newinfo[0].substr(2);
    for (i = 0; i + 1 < newinfo.length - 1; i++)
    {
        if (newinfo[i + 1].substr(0, 2) == "SH")
        {
            currentthread = newinfo[i + 1].split("`");
            if (currentthread[1].substr(0, 2) == "ST")
            {
                currentshipid = Number(currentthread[0].substr("2"));
                _root.extraplayerships[currentshipid] = new Array();
                _root.extraplayerships[currentshipid][0] = int(currentthread[1].substr("2"));
                _root.extraplayerships[currentshipid][1] = int(currentthread[2].substr("2"));
                _root.extraplayerships[currentshipid][2] = int(currentthread[3].substr("2"));
                _root.extraplayerships[currentshipid][3] = int(currentthread[4].substr("2"));
                tr = 5;
                _root.extraplayerships[currentshipid][4] = new Array();
                for (currentharpoint = 0; currentharpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][2].length; currentharpoint++)
                {
                    _root.extraplayerships[currentshipid][4][currentharpoint] = "none";
                } // end of for
                while (currentthread[tr].substr(0, 2) == "HP")
                {
                    guninfo = currentthread[tr].split("G");
                    currenthardpoint = guninfo[0].substr("2");
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.extraplayerships[currentshipid][4][currenthardpoint] = guninfo[1];
                    ++tr;
                } // end while
                _root.extraplayerships[currentshipid][5] = new Array();
                currentturretpoint = 0;
                _root.extraplayerships[currentshipid][5] = new Array();
                while (currentturretpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][5].length)
                {
                    _root.extraplayerships[currentshipid][5][currentturretpoint] = "none";
                    ++currentturretpoint;
                } // end while
                while (currentthread[tr].substr(0, 2) == "TT")
                {
                    guninfo = currentthread[tr].split("G");
                    currentturretpoint = guninfo[0].substr("2");
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.extraplayerships[currentshipid][5][currentturretpoint] = guninfo[1];
                    ++tr;
                } // end while
                spno = 0;
                _root.extraplayerships[currentshipid][11] = new Array();
                _root.extraplayerships[currentshipid][11][1] = new Array();
                while (currentthread[tr].substr(0, 2) == "SP")
                {
                    cargoinfo = currentthread[tr].split("Q");
                    currentspecialtype = Number(cargoinfo[0].substr("2"));
                    specialquantity = Number(cargoinfo[1]);
                    _root.extraplayerships[currentshipid][11][1][spno] = new Array();
                    _root.extraplayerships[currentshipid][11][1][spno][0] = currentspecialtype;
                    _root.extraplayerships[currentshipid][11][1][spno][1] = specialquantity;
                    ++spno;
                    ++tr;
                } // end while
            } // end if
        } // end if
    } // end of for
    if (_root.playerscurrentextrashipno != "capital")
    {
        if (isNaN(Number(_root.playerscurrentextrashipno)) || Number(_root.playerscurrentextrashipno) > maxextraships - 1)
        {
            for (i = 0; i < maxextraships; i++)
            {
                if (_root.extraplayerships[i][0] != null && !isNaN(Number(_root.playerscurrentextrashipno)))
                {
                    _root.playerscurrentextrashipno = i;
                    break;
                } // end if
            } // end of for
        } // end if
        _root.changetonewship(_root.playerscurrentextrashipno);
    } // end if
} // End of the function
function saveplayerextraships()
{
    saveinfo = "";
    saveinfo = saveinfo + ("NO" + _root.playerscurrentextrashipno + "~");
    for (currentshipid = 0; currentshipid < maxextraships; currentshipid++)
    {
        if (_root.extraplayerships[currentshipid][0] != null)
        {
            saveinfo = saveinfo + ("SH" + currentshipid + "`");
            saveinfo = saveinfo + ("ST" + _root.extraplayerships[currentshipid][0] + "`");
            saveinfo = saveinfo + ("SG" + _root.extraplayerships[currentshipid][1] + "`");
            saveinfo = saveinfo + ("EG" + _root.extraplayerships[currentshipid][2] + "`");
            saveinfo = saveinfo + ("EC" + _root.extraplayerships[currentshipid][3] + "`");
            for (currentharpoint = 0; currentharpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][2].length; currentharpoint++)
            {
                saveinfo = saveinfo + ("HP" + currentharpoint + "G" + _root.extraplayerships[currentshipid][4][currentharpoint] + "`");
            } // end of for
            for (currentturretpoint = 0; currentturretpoint < _root.shiptype[_root.extraplayerships[currentshipid][0]][5].length; currentturretpoint++)
            {
                saveinfo = saveinfo + ("TT" + currentturretpoint + "G" + _root.extraplayerships[currentshipid][5][currentturretpoint] + "`");
            } // end of for
            for (spno = 0; spno < _root.extraplayerships[currentshipid][11][1].length; spno++)
            {
                saveinfo = saveinfo + ("SP" + _root.extraplayerships[currentshipid][11][1][spno][0] + "Q" + _root.extraplayerships[currentshipid][11][1][spno][1] + "`");
            } // end of for
            saveinfo = saveinfo + "~";
        } // end if
    } // end of for
    _root.playersextrashipsdata = saveinfo;
} // End of the function
Stage.showMenu = false;
maxextraships = 3;

_root.specialshipitems = new Array();
_root.pulsarseldammodier = 1;
_root.pulsarsinzone = false;
itemno = 0;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Flare 1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 200;
_root.specialshipitems[itemno][3] = 10000;
_root.specialshipitems[itemno][4] = 3000;
_root.specialshipitems[itemno][5] = "FLARE";
_root.specialshipitems[itemno][6] = 100000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "specialflare1";
_root.specialshipitems[itemno][10] = 0.120000;
_root.specialshipitems[itemno][11] = 1000;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Flare 2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 400;
_root.specialshipitems[itemno][3] = 10000;
_root.specialshipitems[itemno][4] = 4000;
_root.specialshipitems[itemno][5] = "FLARE";
_root.specialshipitems[itemno][6] = 280000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "specialflare2";
_root.specialshipitems[itemno][10] = 0.230000;
_root.specialshipitems[itemno][11] = 1500;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Flare 3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 600;
_root.specialshipitems[itemno][3] = 10000;
_root.specialshipitems[itemno][4] = 5000;
_root.specialshipitems[itemno][5] = "FLARE";
_root.specialshipitems[itemno][6] = 750000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "specialflare3";
_root.specialshipitems[itemno][10] = 0.330000;
_root.specialshipitems[itemno][11] = 2000;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Detector 1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 100;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "DETECTOR";
_root.specialshipitems[itemno][6] = 350000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.200000;
_root.specialshipitems[itemno][11] = 10000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Detector 2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 220;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "DETECTOR";
_root.specialshipitems[itemno][6] = 750000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.300000;
_root.specialshipitems[itemno][11] = 8000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Detector 3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 325;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "DETECTOR";
_root.specialshipitems[itemno][6] = 2150000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.400000;
_root.specialshipitems[itemno][11] = 7000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Stealth 1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 75;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "STEALTH";
_root.specialshipitems[itemno][6] = 250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.600000;
_root.specialshipitems[itemno][11] = 20000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Stealth 2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 175;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "STEALTH";
_root.specialshipitems[itemno][6] = 1250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.750000;
_root.specialshipitems[itemno][11] = 15000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Stealth 3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 250;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "STEALTH";
_root.specialshipitems[itemno][6] = 2500000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.900000;
_root.specialshipitems[itemno][11] = 10000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Cloak+Stealth 1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 150;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "CLOAK";
_root.specialshipitems[itemno][6] = 500000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.660000;
_root.specialshipitems[itemno][11] = 9000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Cloak+Stealth 2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 350;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "CLOAK";
_root.specialshipitems[itemno][6] = 1750000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.850000;
_root.specialshipitems[itemno][11] = 7500;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Cloak+Stealth 3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 650;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = -1;
_root.specialshipitems[itemno][5] = "CLOAK";
_root.specialshipitems[itemno][6] = 4200000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "";
_root.specialshipitems[itemno][10] = 0.950000;
_root.specialshipitems[itemno][11] = 6000;
_root.specialshipitems[itemno][4] = _root.specialshipitems[itemno][11];
_root.specialshipitems[itemno][3] = _root.specialshipitems[itemno][11];
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Pulsar-1";
_root.specialshipitems[itemno][1] = 3;
_root.specialshipitems[itemno][2] = 400;
_root.specialshipitems[itemno][3] = 1000;
_root.specialshipitems[itemno][4] = 5000;
_root.specialshipitems[itemno][5] = "PULSAR";
_root.specialshipitems[itemno][6] = 50000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "pulsar1";
_root.specialshipitems[itemno][10] = 0.950000;
_root.specialshipitems[itemno][11] = 4000;
_root.specialshipitems[itemno][12] = 150;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Pulsar-2";
_root.specialshipitems[itemno][1] = 2;
_root.specialshipitems[itemno][2] = 600;
_root.specialshipitems[itemno][3] = 1500;
_root.specialshipitems[itemno][4] = 7000;
_root.specialshipitems[itemno][5] = "PULSAR";
_root.specialshipitems[itemno][6] = 125000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "pulsar2";
_root.specialshipitems[itemno][10] = 0.950000;
_root.specialshipitems[itemno][11] = 7000;
_root.specialshipitems[itemno][12] = 225;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Pulsar-3";
_root.specialshipitems[itemno][1] = 1;
_root.specialshipitems[itemno][2] = 750;
_root.specialshipitems[itemno][3] = 2000;
_root.specialshipitems[itemno][4] = 15000;
_root.specialshipitems[itemno][5] = "PULSAR";
_root.specialshipitems[itemno][6] = 250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "pulsar3";
_root.specialshipitems[itemno][10] = 0.950000;
_root.specialshipitems[itemno][11] = 9500;
_root.specialshipitems[itemno][12] = 300;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Shield-1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 5;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 15000;
_root.specialshipitems[itemno][5] = "RECHARGESHIELD";
_root.specialshipitems[itemno][6] = 650000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "reshield1";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Shield-2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 12;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 25000;
_root.specialshipitems[itemno][5] = "RECHARGESHIELD";
_root.specialshipitems[itemno][6] = 1250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "reshield2";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Shield-3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 20;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 50000;
_root.specialshipitems[itemno][5] = "RECHARGESHIELD";
_root.specialshipitems[itemno][6] = 2500000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "reshield3";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Struct-1";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 5;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 25000;
_root.specialshipitems[itemno][5] = "RECHARGESTRUCT";
_root.specialshipitems[itemno][6] = 1250000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "restruct1";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Struct-2";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 12;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 35000;
_root.specialshipitems[itemno][5] = "RECHARGESTRUCT";
_root.specialshipitems[itemno][6] = 2750000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "restruct2";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Re-Struct-3";
_root.specialshipitems[itemno][1] = -1;
_root.specialshipitems[itemno][2] = 20;
_root.specialshipitems[itemno][3] = -1;
_root.specialshipitems[itemno][4] = 50000;
_root.specialshipitems[itemno][5] = "RECHARGESTRUCT";
_root.specialshipitems[itemno][6] = 5890000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "restruct3";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 0;
_root.specialshipitems[itemno][12] = 0;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Mine-1";
_root.specialshipitems[itemno][1] = 15;
_root.specialshipitems[itemno][2] = 0;
_root.specialshipitems[itemno][3] = 5000;
_root.specialshipitems[itemno][4] = 3000;
_root.specialshipitems[itemno][5] = "MINES";
_root.specialshipitems[itemno][6] = 50000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "mine1";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 2000;
_root.specialshipitems[itemno][12] = 5000;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Mine-2";
_root.specialshipitems[itemno][1] = 10;
_root.specialshipitems[itemno][2] = 0;
_root.specialshipitems[itemno][3] = 5000;
_root.specialshipitems[itemno][4] = 5500;
_root.specialshipitems[itemno][5] = "MINES";
_root.specialshipitems[itemno][6] = 95000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "mine2";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 2000;
_root.specialshipitems[itemno][12] = 10000;
++itemno;
_root.specialshipitems[itemno] = new Array();
_root.specialshipitems[itemno][0] = "Mine-3";
_root.specialshipitems[itemno][1] = 5;
_root.specialshipitems[itemno][2] = 0;
_root.specialshipitems[itemno][3] = 5000;
_root.specialshipitems[itemno][4] = 8000;
_root.specialshipitems[itemno][5] = "MINES";
_root.specialshipitems[itemno][6] = 135000;
_root.specialshipitems[itemno][7] = itemno;
_root.specialshipitems[itemno][8] = "mine3";
_root.specialshipitems[itemno][10] = 1;
_root.specialshipitems[itemno][11] = 2000;
_root.specialshipitems[itemno][12] = 25000;

function func_racinggamefinished()
{
    _root.racinginformation[0] = 0;
    _root.racingcheckpoints = new Array();
    colourtouse = _root.systemchattextcolor;
    message = "HOST: Race is Completed";
    _root.enterintochat(message, colourtouse);
    _root.gotoAndStop("racingzonescreen");
} // End of the function
function func_racinggamefinisher(playerloc, playerplaced)
{
    playerloc = Number(playerloc);
    playerplaced = Number(playerplaced);
    for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
    {
        if (playerloc == _root.currentonlineplayers[jjjj][0])
        {
            name = _root.currentonlineplayers[jjjj][1];
            break;
        } // end if
    } // end of for
    colourtouse = _root.systemchattextcolor;
    message = "HOST: Player " + name + " has finished with Rank # " + (playerplaced + 1);
    if (playerplaced == 0)
    {
        message = message + (", for " + _root.func_formatnumber(_root.racinginformation[1]) + " funds");
        if (_root.playershipstatus[3][0] == playerloc)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(_root.racinginformation[1]);
        } // end if
    } // end if
    _root.enterintochat(message, colourtouse);
} // End of the function
function func_createracinggamepoints(navpoints, racestarttime, reward)
{
    racingcheckpoints = new Array();
    for (rac = 0; rac < navpoints.length; rac++)
    {
        racingcheckpoints[rac] = new Array();
        racingcheckpoints[rac][0] = navpoints[rac];
        racingcheckpoints[rac][1] = false;
    } // end of for
    racinggameinprogress = true;
    distance = 0;
    lastx = 0;
    lasty = 0;
    for (rac = 0; rac < navpoints.length; rac++)
    {
        for (sect = 0; sect < _root.sectormapitems.length; sect++)
        {
            if (_root.sectormapitems[sect][0] == racingcheckpoints[rac][0])
            {
                xloc = _root.sectormapitems[sect][1];
                yloc = _root.sectormapitems[sect][2];
                xdiff = lastx - xloc;
                ydiff = lasty - yloc;
                lasty = yloc;
                lastx = xloc;
                distance = distance + Math.sqrt(xdiff * xdiff + ydiff * ydiff);
                break;
            } // end if
        } // end of for
    } // end of for
    racetotaldistance = distance;
    racestarttime = Number(racestarttime);
    racinginformation[1] = finalracegamereward = Number(reward);
    if (racestarttime > 0)
    {
        colourtouse = _root.systemchattextcolor;
        message = "HOST: Race Starting in " + racestarttime + " seconds, reward is " + _root.func_formatnumber(finalracegamereward) + " funds";
        _root.enterintochat(message, colourtouse);
        racinginformation[0] = getTimer() + racestarttime * 1000;
    }
    else
    {
        colourtouse = _root.systemchattextcolor;
        message = "HOST: Race Started " + Math.abs(racestarttime) + " seconds ago, reward is " + _root.func_formatnumber(finalracegamereward) + " funds";
        _root.enterintochat(message, colourtouse);
        racinginformation[0] = 0;
    } // end else if
} // End of the function
function func_getracereward(distancebeingtraveled)
{
    fundsperunitoftravel = 5;
    timesfundsforextraplayer = 0.300000;
    basefunds = distancebeingtraveled * fundsperunitoftravel;
    fundswithplayers = basefunds * (timesfundsforextraplayer * _root.currentonlineplayers.length + 0.700000);
    return (Math.round(fundswithplayers));
} // End of the function
isgameracingzone = false;
racegamepacespeed = 50;
racegamestartdelay = 30;
racinginformation = new Array();
racinginformation[0] = 0;
racinginformation[1] = 95;
racinginformation[2] = true;

function sendnewdmgame()
{
    info = "";
    for (xx = 0; xx < _root.teambases.length; xx++)
    {
        info = info + ("`" + _root.teambasetypes[_root.teambases[xx][3]][0]);
    } // end of for
    datatosend = "DM`NG" + info + "~";
    _root.mysocket.send(datatosend);
} // End of the function
function deathfgamerewards()
{
    return (_root.currentonlineplayers.length * _root.teamdeathmatchinfo[1] + _root.teamdeathmatchinfo[0]);
} // End of the function
function deathsgamerewards()
{
    return (Math.floor((_root.currentonlineplayers.length * _root.teamdeathmatchinfo[1] + _root.teamdeathmatchinfo[0]) / 1000));
} // End of the function
_root.teamdeathmatch = false;
_root.teamdeathmatchinfo = new Array();
_root.teamdeathmatchinfo[0] = 300000;
_root.teamdeathmatchinfo[1] = 100000;
_root.teambasetypes = new Array();
_root.teambasetypes[0] = new Array();
_root.teambasetypes[0][0] = 300000;
_root.teambasetypes[0][1] = 800;
_root.teambasetypes[0][2] = 10000;
_root.teambasetypes[0][3] = 300000;
_root.teambasetypes[0][4] = 20000;
squadwarinfo = new Array();
squadwarinfo[0] = false;
squadwarinfo[1] = new Array();

function func_playercreatekflag()
{
    minusers = 4;
    location = _root.fun_kingofflagrandomcoords();
    xcoord = location[0];
    ycoord = location[1];
    if (_root.kingofflag[0] == null && _root.kingofflag[2] == null)
    {
        if (_root.currentonlineplayers.length >= minusers)
        {
            datatosend = "KFLAG`CREATE`" + xcoord + "`" + ycoord + "~";
            _root.mysocket.send(datatosend);
        }
        else
        {
            message = "HOST: Need at least " + minusers + " players to start the game";
            _root.enterintochat(message, _root.systemchattextcolor);
        } // end else if
    }
    else
    {
        if (_root.kingofflag[0] == null)
        {
            xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
            ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
            message = "HOST: Flag Already in game at X:" + xgrid + " Y:" + ygrid;
        }
        else
        {
            xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
            ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
            message = "HOST: Flag Already in game, held by " + _root.func_playersrealname(_root.kingofflag[0]) + " last report at X:" + xgrid + " Y:" + ygrid;
        } // end else if
        _root.enterintochat(message, _root.systemchattextcolor);
    } // end else if
} // End of the function
function func_playersrealname(playerid)
{
    for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
    {
        if (playerid == _root.currentonlineplayers[jjjj][0])
        {
            return (_root.currentonlineplayers[jjjj][1]);
            break;
        } // end if
    } // end of for
} // End of the function
function fun_kingofflagrandomcoords()
{
    xwidthofsector = _root.sectorinformation[1][0];
    ywidthofsector = _root.sectorinformation[1][1];
    xgrid = Math.round(Math.random() * Number(_root.sectorinformation[0][0] - 2)) + 1;
    xcoord = xwidthofsector * xgrid - Math.round(xwidthofsector / 2);
    ygrid = Math.round(Math.random() * Number(_root.sectorinformation[0][1] - 2)) + 1;
    ycoord = ywidthofsector * ygrid - Math.round(ywidthofsector / 2);
    location = new Array();
    location[0] = xcoord;
    location[1] = ycoord;
    return (location);
} // End of the function
function func_kingofflagcommand(method, var1, var2, var3)
{
    if (method == "TRANS")
    {
        currenthostalreadysent = false;
        if (String(var1) == String(_root.kingofflag[0]))
        {
            currenthostalreadysent = true;
        } // end if
        _root.kingofflag[0] = var1;
        _root.kingofflag[1] = getTimer() + _root.kingofflag[10];
        _root.gamedisplayarea.func_flagcontrols("REMOVE");
        if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
        {
            _root.game_top_bar.flagwindow.func_runflagscript();
        } // end if
        if (currenthostalreadysent != true)
        {
            message = "HOST: " + func_playersrealname(_root.kingofflag[0]) + " has the flag.";
            _root.enterintochat(message, _root.systemchattextcolor);
        } // end if
    }
    else if (method == "END")
    {
        func_kingofflagreset();
        scorereward = Number(var2);
        if (isNaN(scorereward))
        {
            scorereward = 0;
        } // end if
        fundreward = Number(var3);
        if (isNaN(fundreward))
        {
            fundreward = 0;
        } // end if
        message = "HOST: " + func_playersrealname(var1) + " won the flagging game for " + Math.floor(scorereward / _root.scoreratiomodifier) + " score and " + _root.func_formatnumber(fundreward) + " funds.";
        _root.enterintochat(message, _root.systemchattextcolor);
        for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
        {
            if (String(var1) == String(_root.currentonlineplayers[jjjj][0]))
            {
                _root.currentonlineplayers[jjjj][5] = Number(_root.currentonlineplayers[jjjj][5]) + Number(scorereward);
                break;
            } // end if
        } // end of for
        if (String(_root.playershipstatus[3][0]) == String(var1))
        {
            _root.playershipstatus[5][9] = Number(_root.playershipstatus[5][9]) + Number(scorereward);
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(fundreward);
            _root.toprightinformation.playerscore = "Score: " + Math.floor(Number(_root.playershipstatus[5][9]) / _root.scoreratiomodifier);
            _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
        } // end if
        _root.refreshonlinelist = true;
    }
    else if (method == "DROP")
    {
        _root.kingofflag[0] = null;
        _root.kingofflag[1] = null;
        _root.kingofflag[2] = Number(var1);
        _root.kingofflag[3] = Number(var2);
        _root.gamedisplayarea.func_flagcontrols("ADD", _root.kingofflag[2], _root.kingofflag[3]);
        xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
        ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
        message = "HOST: Flag located at X:" + xgrid + " Y:" + ygrid;
        _root.enterintochat(message, _root.systemchattextcolor);
    }
    else if (method == "CREATE")
    {
        _root.kingofflag[0] = null;
        _root.kingofflag[1] = null;
        _root.kingofflag[2] = Number(var1);
        _root.kingofflag[3] = Number(var2);
        _root.gamedisplayarea.func_flagcontrols("ADD", _root.kingofflag[2], _root.kingofflag[3]);
        xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
        ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
        message = "HOST: Flag located at X:" + xgrid + " Y:" + ygrid;
        _root.enterintochat(message, _root.systemchattextcolor);
    }
    else if (method == "INFO")
    {
        flagtime = Math.round((_root.kingofflag[1] - getTimer()) / 1000);
        _root.kingofflag[2] = Number(var1);
        _root.kingofflag[3] = Number(var2);
        xgrid = Math.ceil(Number(_root.kingofflag[2]) / _root.sectorinformation[1][0]);
        ygrid = Math.ceil(Number(_root.kingofflag[3]) / _root.sectorinformation[1][1]);
        name = func_playersrealname(playerid);
        message = "HOST: " + _root.func_playersrealname(_root.kingofflag[0]) + " has the flag at X:" + xgrid + " Y:" + ygrid + " with " + flagtime + " seconds left";
        _root.enterintochat(message, _root.systemchattextcolor);
    } // end else if
} // End of the function
function func_kingofflagreset()
{
    _root.kingofflag = new Array();
    _root.kingofflag[0] = null;
    _root.kingofflag[1] = null;
    _root.kingofflag[2] = null;
    _root.kingofflag[3] = null;
    _root.kingofflag[10] = 120000;
    _root.kingofflag[11] = 50;
    _root.kingofflag[12] = 15;
    _root.kingofflag[13] = 30000;
    _root.kingofflag[14] = 15000;
    _root.kingofflag[15] = 20000;
    _root.kingofflag[16] = false;
} // End of the function
func_kingofflagreset();

_root.keypressdelay = 0;
_root.shipositiondelay = 300;
_root.errorcheckdelay = 7000;
_root.errorcheckdata = new Array();
_root.errorchecknumber = 0;
_root._x = 0;
_root._y = 0;
_root.cosines = new Array();
for (j = 0; j <= 360; j++)
{
    _root.cosines[j] = parseFloat(Math.cos(0.017453 * j));
} // end of for
_root.sines = new Array();
for (j = 0; j <= 360; j++)
{
    _root.sines[j] = parseFloat(Math.sin(0.017453 * j));
} // end of for
stop ();

// [Action in Frame 3]

// [Action in Frame 5]
function errorcheckedmessage(datatosend, errorno)
{
    currentloc = _root.errorcheckdata.length;
    _root.errorcheckdata[currentloc] = new Array();
    _root.errorcheckdata[currentloc][0] = datatosend;
    _root.errorcheckdata[currentloc][1] = getTimer() + _root.errorcheckdelay;
    _root.errorcheckdata[currentloc][2] = errorno;
    ++_root.errorchecknumber;
    if (_root.errorchecknumber > 100)
    {
        _root.errorchecknumber = 0;
    } // end if
} // End of the function
function initializemissilebanks()
{
    _root.playershipstatus[10] = 0;
    _root.playershipstatus[7] = new Array();
    playershiptype = _root.playershipstatus[5][0];
    for (bankno = 0; bankno < _root.shiptype[playershiptype][7].length; bankno++)
    {
        _root.playershipstatus[7][bankno] = new Array();
        _root.playershipstatus[7][bankno][0] = 0;
        _root.playershipstatus[7][bankno][1] = 0;
        _root.playershipstatus[7][bankno][2] = _root.shiptype[playershiptype][7][bankno][2];
        _root.playershipstatus[7][bankno][3] = _root.shiptype[playershiptype][7][bankno][3];
        _root.playershipstatus[7][bankno][4] = new Array();
        for (abc = 0; abc < _root.missile.length; abc++)
        {
            _root.playershipstatus[7][bankno][4][abc] = 0;
        } // end of for
        _root.playershipstatus[7][bankno][5] = _root.shiptype[playershiptype][7][bankno][5];
    } // end of for
} // End of the function
function loadnewaiintogame(ailoadedvars)
{
    newinfo = ailoadedvars.split("~");
    _root.aishipsinfo = new Array();
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentai = i;
        currentthread = newinfo[i].split("`");
        _root.aishipsinfo[currentai] = new Array();
        _root.aishipsinfo[currentai][0] = currentthread[0];
        _root.currentgamestatus = _root.currentgamestatus + (" \r " + _root.aishipsinfo[currentai][0]);
        _root.aishipsinfo[currentai][1] = int(currentthread[1]);
        _root.currentgamestatus = _root.currentgamestatus + (" \r " + _root.aishipsinfo[currentai][1]);
    } // end of for
} // End of the function
function checkplayersdockedbase()
{
    baseexists = false;
    for (jjj = 0; jjj < _root.starbaselocation.length; jjj++)
    {
        if (_root.starbaselocation[jjj][0] == _root.playershipstatus[4][0])
        {
            baseexists = true;
            break;
        } // end if
    } // end of for
    if (baseexists == false)
    {
        noofbases = _root.starbaselocation.length - 1;
        basetouse = Math.round(Math.random() * noofbases);
        _root.playershipstatus[4][0] = _root.starbaselocation[basetouse][0];
    } // end if
} // End of the function
function loadsectormap(loadedvars)
{
    newinfo = loadedvars.split("~");
    i = 0;
    _root.sectormapitems = new Array();
    _root.starbaselocation = new Array();
    _root.teambases = new Array();
    while (i < newinfo.length - 1)
    {
        if (newinfo[i].substr(0, 2) == "SB" || newinfo[i].substr(0, 2) == "PL")
        {
            _root.sectormapitems[i] = new Array();
            currentthread = newinfo[i].split("`");
            currentstarbase = _root.starbaselocation.length;
            _root.starbaselocation[currentstarbase] = new Array();
            _root.starbaselocation[currentstarbase][0] = currentthread[0];
            _root.sectormapitems[i][0] = currentthread[0];
            _root.sectormapitems[i][1] = Number(currentthread[1].substr("1"));
            _root.starbaselocation[currentstarbase][1] = Number(currentthread[1].substr("1"));
            _root.sectormapitems[i][2] = Number(currentthread[2].substr("1"));
            _root.starbaselocation[currentstarbase][2] = Number(currentthread[2].substr("1"));
            _root.sectormapitems[i][3] = int(currentthread[3].substr("1"));
            _root.starbaselocation[currentstarbase][3] = int(currentthread[3].substr("1"));
            _root.starbaselocation[currentstarbase][4] = int(currentthread[4].substr("1"));
            _root.sectormapitems[i][6] = int(currentthread[4].substr("1"));
            _root.starbaselocation[currentstarbase][5] = currentthread[5];
            _root.starbaselocation[currentstarbase][9] = 0;
            _root.sectormapitems[i][9] = 0;
            _root.sectormapitems[i][4] = "OFF";
            _root.sectormapitems[i][10] = Math.ceil(Number(_root.sectormapitems[i][1]) / _root.sectorinformation[1][0]);
            _root.sectormapitems[i][11] = Math.ceil(Number(_root.sectormapitems[i][2]) / _root.sectorinformation[1][1]);
        } // end if
        if (newinfo[i].substr(0, 2) == "TB")
        {
            _root.teamdeathmatch = true;
            _root.sectormapitems[i] = new Array();
            currentthread = newinfo[i].split("`");
            currentteambases = _root.teambases.length;
            _root.teambases[currentteambases] = new Array();
            _root.teambases[currentteambases][0] = currentthread[0];
            _root.sectormapitems[i][0] = currentthread[0];
            _root.sectormapitems[i][1] = int(currentthread[1].substr("1"));
            _root.teambases[currentteambases][1] = Number(currentthread[1].substr("1"));
            _root.sectormapitems[i][2] = int(currentthread[2].substr("1"));
            _root.teambases[currentteambases][2] = Number(currentthread[2].substr("1"));
            _root.sectormapitems[i][3] = int(currentthread[3].substr("1"));
            _root.teambases[currentteambases][3] = int(currentthread[3].substr("1"));
            _root.teambases[currentteambases][4] = Number(currentthread[4].substr("1"));
            _root.sectormapitems[i][6] = int(currentthread[4].substr("1"));
            _root.teambases[currentteambases][5] = currentthread[5];
            _root.teambases[currentteambases][9] = 0;
            _root.sectormapitems[i][9] = 0;
            _root.teambases[currentteambases][10] = "N/A";
            _root.teambases[currentteambases][11] = _root.teambasetypes[_root.teambases[currentteambases][3]][2];
            _root.sectormapitems[i][4] = "OFF";
            _root.sectormapitems[i][10] = Math.ceil(Number(_root.sectormapitems[i][1]) / _root.sectorinformation[1][0]);
            _root.sectormapitems[i][11] = Math.ceil(Number(_root.sectormapitems[i][2]) / _root.sectorinformation[1][1]);
        } // end if
        if (newinfo[i].substr(0, 2) == "AS")
        {
            _root.sectormapitems[i] = new Array();
            currentthread = newinfo[i].split("`");
            _root.sectormapitems[i][0] = currentthread[0];
            _root.sectormapitems[i][1] = int(currentthread[1].substr("1"));
            _root.sectormapitems[i][2] = int(currentthread[2].substr("1"));
            _root.sectormapitems[i][3] = int(currentthread[3].substr("1"));
            _root.sectormapitems[i][4] = "OFF";
            _root.sectormapitems[i][10] = Math.ceil(Number(_root.sectormapitems[i][1]) / _root.sectorinformation[1][0]);
            _root.sectormapitems[i][11] = Math.ceil(Number(_root.sectormapitems[i][2]) / _root.sectorinformation[1][1]);
        } // end if
        if (newinfo[i].substr(0, 2) == "NP")
        {
            _root.sectormapitems[i] = new Array();
            currentthread = newinfo[i].split("`");
            _root.sectormapitems[i][0] = currentthread[0];
            _root.sectormapitems[i][1] = Number(currentthread[1].substr("1"));
            _root.sectormapitems[i][2] = Number(currentthread[2].substr("1"));
            _root.sectormapitems[i][3] = Number(currentthread[3].substr("1"));
            _root.sectormapitems[i][4] = "OFF";
            tester = Number(currentthread[4]);
            if (isNaN(tester))
            {
                _root.sectormapitems[i][5] = currentthread[4];
            }
            else
            {
                _root.sectormapitems[i][5] = "Jump Point to System: " + currentthread[4];
                _root.sectormapitems[i][9] = tester;
            } // end else if
            _root.sectormapitems[i][10] = Math.ceil(Number(_root.sectormapitems[i][1]) / _root.sectorinformation[1][0]);
            _root.sectormapitems[i][11] = Math.ceil(Number(_root.sectormapitems[i][2]) / _root.sectorinformation[1][1]);
        } // end if
        if (newinfo[i].substr(0, 2) == "SI")
        {
            currentthread = newinfo[i].split("`");
            if (currentthread[1].substr(0, 2) == "XL")
            {
                _root.sectorinformation = new Array();
                _root.sectorinformation[0] = new Array();
                _root.sectorinformation[0][0] = Number(currentthread[1].substr("2"));
                _root.sectorinformation[0][1] = Number(currentthread[2].substr("2"));
                _root.sectorinformation[1] = new Array();
                _root.sectorinformation[1][0] = Number(currentthread[3].substr("2"));
                _root.sectorinformation[1][1] = Number(currentthread[4].substr("2"));
                zoneinfo = currentthread[6].split(",");
                for (jjt = 0; jjt < zoneinfo.length; jjt++)
                {
                    if (zoneinfo[jjt] == "PULSAR")
                    {
                        _root.pulsarsinzone = true;
                    } // end if
                    if (zoneinfo[jjt] == "RACING")
                    {
                        isgameracingzone = true;
                    } // end if
                } // end of for
            } // end if
        } // end if
        ++i;
    } // end while
} // End of the function
_root.starbasestayhostiletime = 15000;
pathtoaifile = "./ai/";
_root.attachMovie("chatdialogue", "chatdialogue", 99983);
setProperty("_root.chatdialogue", _x, 130);
setProperty("_root.chatdialogue", _y, 500);
initializemissilebanks();
_root.playershipstatus[5][1] = 500;
if (_root.isgamerunningfromremote == false)
{
    _root.playershipstatus[5][1] = _root.portandsystem;
} // end if
if (_root.jumpinginfo.length > 1)
{
    _root.playershipstatus[5][1] = _root.portandsystem = _root.jumpinginfo[0];
} // end if
_root.currentgamestatus = _root.currentgamestatus + ("Loading System " + _root.playershipstatus[5][1] + "\r");
if (_root.isgamerunningfromremote == false)
{
    currenturl = String(_root._url);
    filler = currenturl.split("/");
    if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
    {
        loadVariables("http://www.gecko-games.com/stellar/gameserv" + portandsystem + ".php", _root.scoreClip, "POST";
    }
    else if (currenturl.substr(0, 32) == "http://www.stellar-conflicts.com")
    {
        loadVariables("http://www.stellar-conflicts.com/gametesting/gameserv" + portandsystem + ".php", _root.scoreClip, "POST";
    } // end else if
}
else
{
    loadVariables("http://localhost/september/gameserv6.php", _root.scoreClip, "POST";
} // end else if
newsystemVars = new XML();
newsystemVars.load(_root.pathtoaccounts + "systems.php?system=" + _root.playershipstatus[5][1]);
newsystemVars.onLoad = function (success)
{
    loadedvars = String(newsystemVars);
    if (loadedvars.length > 5)
    {
        loadsectormap(loadedvars);
        _root.currentgamestatus = _root.currentgamestatus + ("System " + _root.playershipstatus[5][1] + " loaded\r\r");
        checkplayersdockedbase();
        _root.play();
    }
    else
    {
        _root.currentgamestatus = _root.currentgamestatus + "Failed to load system\r";
    } // end else if
};
stop ();

// [Action in Frame 6]
currentship = 0;
_root.escortshiptype[currentship] = new Array();
_root.escortshiptype[currentship][0] = "Human Scout";
_root.escortshiptype[currentship][1] = 75000;
_root.escortshiptype[currentship][2] = new Array();
_root.escortshiptype[currentship][2][0] = new Array();
_root.escortshiptype[currentship][2][0][0] = 9;
_root.escortshiptype[currentship][2][0][1] = -5;
_root.escortshiptype[currentship][2][1] = new Array();
_root.escortshiptype[currentship][2][1][0] = -10;
_root.escortshiptype[currentship][2][1][1] = -5;
_root.escortshiptype[currentship][3] = new Array();
_root.escortshiptype[currentship][3][0] = 40;
_root.escortshiptype[currentship][3][1] = 95;
_root.escortshiptype[currentship][3][2] = 90;
_root.escortshiptype[currentship][3][3] = 1000;
_root.escortshiptype[currentship][3][4] = 10;
_root.escortshiptype[currentship][3][5] = 0.350000;
_root.escortshiptype[currentship][3][6] = 115;
_root.escortshiptype[currentship][3][7] = 3;
_root.escortshiptype[currentship][3][8] = false;
_root.escortshiptype[currentship][4] = 40;
_root.escortshiptype[currentship][6] = new Array();
_root.escortshiptype[currentship][6][0] = 2;
_root.escortshiptype[currentship][6][1] = 2;
_root.escortshiptype[currentship][6][2] = 3;
_root.escortshiptype[currentship][6][3] = 3;
_root.escortshiptype[currentship][7] = new Array();
_root.escortshiptype[currentship][7][0] = new Array();
_root.escortshiptype[currentship][7][0][2] = 0;
_root.escortshiptype[currentship][7][0][3] = -5;
_root.escortshiptype[currentship][7][0][5] = 6;

function loadsquadbases()
{
    newsystemVars = new XML();
    newsystemVars.load(_root.pathtoaccounts + "squadbases.php?mode=load&system=" + _root.playershipstatus[5][1]);
    newsystemVars.onLoad = function (success)
    {
        loadedvars = String(newsystemVars);
        loadsectormap(loadedvars);
        _root.currentgamestatus = _root.currentgamestatus + "Squad Bases Loaded\r\r";
        if (squadbasesalreadyloaded == false)
        {
            squadbasesalreadyloaded = true;
            _root.play();
        } // end if
    };
} // End of the function
function loadsectormap(loadedvars)
{
    newinfo = loadedvars.split("~");
    i = 0;
    _root.playersquadbases = new Array();
    while (i < newinfo.length - 1)
    {
        currentthread = newinfo[i].split("`");
        currentsquadbase = _root.playersquadbases.length;
        _root.playersquadbases[currentsquadbase] = new Array();
        _root.playersquadbases[currentsquadbase][0] = currentthread[0];
        _root.playersquadbases[currentsquadbase][1] = Number(currentthread[1]);
        _root.playersquadbases[currentsquadbase][2] = Number(currentthread[2]);
        _root.playersquadbases[currentsquadbase][3] = Number(currentthread[3]);
        _root.playersquadbases[currentsquadbase][4] = Math.ceil(Number(currentthread[2]) / _root.sectorinformation[1][0]);
        _root.playersquadbases[currentsquadbase][5] = Math.ceil(Number(currentthread[3]) / _root.sectorinformation[1][1]);
        _root.playersquadbases[currentsquadbase][10] = false;
        ++i;
    } // end while
} // End of the function
_root.playersquadbaseslvl = 0;
_root.currentgamestatus = _root.currentgamestatus + "Loading Squad Bases: \r";
loadsquadbases();
squadbasesalreadyloaded = false;
_root.squadbaseinfo = new Array();
squadbasetype = 0;
_root.squadbaseinfo[squadbasetype] = new Array();
_root.squadbaseinfo[squadbasetype][1] = new Array();
_root.squadbaseinfo[squadbasetype][1][0] = 10;
_root.squadbaseinfo[squadbasetype][1][1] = 8;
_root.squadbaseinfo[squadbasetype][1][2] = 5;
_root.squadbaseinfo[squadbasetype][1][3] = _root.shieldgenerators.length - 4;
_root.squadbaseinfo[squadbasetype][2] = new Array();
_root.squadbaseinfo[squadbasetype][2][0] = 2;
_root.squadbaseinfo[squadbasetype][2][1] = _root.guntype.length - 3;
_root.squadbaseinfo[squadbasetype][2][2] = 4;
_root.squadbaseinfo[squadbasetype][3] = new Array();
_root.squadbaseinfo[squadbasetype][3][0] = 2;
_root.squadbaseinfo[squadbasetype][3][1] = 125000;
_root.squadbaseinfo[squadbasetype][4] = new Array();
_root.squadbaseinfo[squadbasetype][4][0] = 1250;
++squadbasetype;
_root.squadbaseinfo[squadbasetype] = new Array();
_root.squadbaseinfo[squadbasetype][1] = new Array();
_root.squadbaseinfo[squadbasetype][1][0] = 10;
_root.squadbaseinfo[squadbasetype][1][1] = 7;
_root.squadbaseinfo[squadbasetype][1][2] = 4;
_root.squadbaseinfo[squadbasetype][1][3] = _root.shieldgenerators.length - 3;
_root.squadbaseinfo[squadbasetype][2] = new Array();
_root.squadbaseinfo[squadbasetype][2][0] = 2;
_root.squadbaseinfo[squadbasetype][2][1] = _root.guntype.length - 2;
_root.squadbaseinfo[squadbasetype][2][2] = 4;
_root.squadbaseinfo[squadbasetype][3] = new Array();
_root.squadbaseinfo[squadbasetype][3][0] = 2;
_root.squadbaseinfo[squadbasetype][3][1] = 500000;
_root.squadbaseinfo[squadbasetype][4] = new Array();
_root.squadbaseinfo[squadbasetype][4][0] = 1000;
_root.squadbaseinfo[squadbasetype][5] = new Array();
_root.squadbaseinfo[squadbasetype][5][0] = new Array();
_root.squadbaseinfo[squadbasetype][5][0][0] = 23000000;
_root.squadbaseinfo[squadbasetype][5][1] = new Array();
++squadbasetype;
_root.squadbaseinfo[squadbasetype] = new Array();
_root.squadbaseinfo[squadbasetype][1] = new Array();
_root.squadbaseinfo[squadbasetype][1][0] = 15;
_root.squadbaseinfo[squadbasetype][1][1] = 5;
_root.squadbaseinfo[squadbasetype][1][2] = 3;
_root.squadbaseinfo[squadbasetype][1][3] = _root.shieldgenerators.length - 2;
_root.squadbaseinfo[squadbasetype][2] = new Array();
_root.squadbaseinfo[squadbasetype][2][0] = 3;
_root.squadbaseinfo[squadbasetype][2][1] = _root.guntype.length - 1;
_root.squadbaseinfo[squadbasetype][2][2] = 5;
_root.squadbaseinfo[squadbasetype][3] = new Array();
_root.squadbaseinfo[squadbasetype][3][0] = 3;
_root.squadbaseinfo[squadbasetype][3][1] = 900000;
_root.squadbaseinfo[squadbasetype][4] = new Array();
_root.squadbaseinfo[squadbasetype][4][0] = 750;
_root.squadbaseinfo[squadbasetype][5] = new Array();
_root.squadbaseinfo[squadbasetype][5][0] = new Array();
_root.squadbaseinfo[squadbasetype][5][0][0] = 59000000;
_root.squadbaseinfo[squadbasetype][5][1] = new Array();
stop ();

// [Action in Frame 7]
function func_globaltimestamp()
{
    globaltimestamp = String(getTimer() + _root.clocktimediff);
    globaltimestamp = Number(globaltimestamp.substr(globaltimestamp.length - 4));
    globaltimestamp = globaltimestamp + _root.keypressdelay;
    if (globaltimestamp > 10000)
    {
        globaltimestamp = globaltimestamp - 10000;
    } // end if
    return (globaltimestamp);
} // End of the function

function enterintochat(info, colourtouse)
{
    _root.gamechatinfo[1].splice(0, 0, 0);
    _root.gamechatinfo[1][0] = new Array();
    _root.gamechatinfo[1][0][0] = info;
    _root.gamechatinfo[1][0][1] = colourtouse;
    if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
    {
        _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
    } // end if
    _root.refreshchatdisplay = true;
} // End of the function
function func_ignoreplayer(playerthatsaidit)
{
    ignoringplayer = false;
    for (ignoreing = 0; ignoreing < _root.gamechatinfo[6].length; ignoreing++)
    {
        if (playerthatsaidit == _root.gamechatinfo[6][ignoreing])
        {
            ignoringplayer = true;
            break;
        } // end if
    } // end of for
    return (ignoringplayer);
} // End of the function
function bringinaiships(noofenemyaiships, typeofscript)
{
    weakesthardpoint = 999999;
    for (i = 0; i < _root.playershipstatus[0].length; i++)
    {
        if (_root.playershipstatus[0][i][1] != "none" && weakesthardpoint > _root.playershipstatus[0][i][0])
        {
            weakesthardpoint = _root.playershipstatus[0][i][0];
        } // end if
    } // end of for
    guntypetouse = weakesthardpoint - 2;
    if (guntypetouse < 0 || guntypetouse > 5000)
    {
        guntypetouse = 0;
    } // end if
    if (typeofscript != "RANDOM")
    {
        ++guntypetouse;
    } // end if
    for (i = 0; i < noofenemyaiships; i++)
    {
        shiptypetouse = _root.playershipstatus[5][0];
        shieldgentouse = _root.playershipstatus[1][0];
        if (_root.playerscurrentextrashipno == "capital")
        {
            shiptypetouse = Math.round(Math.random() * 2) * 4 + 3;
            guntypetouse = _root.shiptype[shiptypetouse][6][3];
            shieldgentouse = _root.shiptype[shiptypetouse][6][0];
        } // end if
        currentai = int(_root.aishipshosted.length);
        if (currentai < 1)
        {
            _root.aishipshosted = new Array();
            currentai = 0;
        } // end if
        _root.aishipshosted[currentai] = new Array();
        _root.aishipshosted[currentai][0] = Math.round(Math.random() * 100) + 10;
        _root.aishipshosted[currentai][1] = _root.shipcoordinatex + Math.random() * 750 - 340;
        _root.aishipshosted[currentai][2] = _root.shipcoordinatey + Math.random() * 750 - 340;
        _root.aishipshosted[currentai][3] = 0;
        _root.aishipshosted[currentai][4] = 20;
        _root.aishipshosted[currentai][5] = "";
        _root.aishipshosted[currentai][6] = shiptypetouse;
        _root.aishipshosted[currentai][7] = "ai" + _root.ainames[Math.round(Math.random() * (_root.ainames.length - 1))];
        _root.aishipshosted[currentai][8] = shieldgentouse;
        _root.aishipshosted[currentai][9] = guntypetouse;
        _root.aishipshosted[currentai][10] = 0;
        _root.gamedisplayarea.attachMovie("shiptypeai", "aiship" + _root.aishipshosted[currentai][0], 11000 + _root.aishipshosted[currentai][0]);
        setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _x, _root.aishipshosted[currentai][1] - _root.shipcoordinatex);
        setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _y, _root.aishipshosted[currentai][2] - _root.shipcoordinatey);
        setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _rotation, _root.aishipshosted[currentai][3]);
        set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".shipid", _root.aishipshosted[currentai][0]);
        if (typeofscript != "RANDOM")
        {
            if (_root.playershipstatus[5][9] / _root.scoreratiomodifier > 1999)
            {
                set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".turrettype", 5);
                continue;
            } // end if
            if (_root.playershipstatus[5][9] / _root.scoreratiomodifier > 999)
            {
                set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".turrettype", 4);
                continue;
            } // end if
            if (_root.playershipstatus[5][9] / _root.scoreratiomodifier > 499)
            {
                set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".turrettype", 3);
            } // end if
        } // end if
    } // end of for
} // End of the function
_root.currentonlineplayers = new Array();
_root.attachMovie("onlineplayerslist", "onlineplayerslist", 99982);
setProperty("_root.onlineplayerslist", _x, 0);
setProperty("_root.onlineplayerslist", _y, 0);

// [Action in Frame 8]
function func_formatnumber(numbertofromat)
{
    numberisnegative = false;
    newnumber = "";
    if (numbertofromat < 0)
    {
        numberisnegative = true;
    } // end if
    numbertofromat = Math.abs(numbertofromat);
    numbertofromat = String(numbertofromat);
    if (numbertofromat.length > 3)
    {
        newnumber = numbertofromat.substr(numbertofromat.length - 3);
        for (numbertofromat = Number(numbertofromat); numbertofromat > 999; numbertofromat = Number(numbertofromat))
        {
            if (numbertofromat > 999)
            {
                numbertofromat = Math.floor(Number(numbertofromat) / 1000);
                numbertofromat = String(numbertofromat);
                if (numbertofromat.length > 3)
                {
                    newnumber = numbertofromat.substr(numbertofromat.length - 3) + "," + newnumber;
                    continue;
                } // end if
                newnumber = numbertofromat + "," + newnumber;
            } // end if
        } // end of for
    }
    else
    {
        newnumber = numbertofromat;
    } // end else if
    if (numberisnegative == true)
    {
        newnumber = "-" + newnumber;
    } // end if
    return (newnumber);
} // End of the function

function initializemissions()
{
    _root.missionsavailable = new Array();
    currentmission = 0;
    for (i = 0; i < _root.sectormapitems.length; i++)
    {
        if (_root.sectormapitems[i][0].substr(0, 2) == "NP")
        {
            _root.missionsavailable[currentmission] = new Array();
            _root.missionsavailable[currentmission][0] = "Recon`" + _root.sectormapitems[i][0];
            _root.missionsavailable[currentmission][1] = _root.sectormapitems[i][1];
            _root.missionsavailable[currentmission][2] = _root.sectormapitems[i][2];
            _root.missionsavailable[currentmission][3] = 20000;
            _root.missionsavailable[currentmission][4] = 600;
            _root.missionsavailable[currentmission][5] = 2;
            _root.missionsavailable[currentmission][6] = 35000;
            ++currentmission;
            _root.missionsavailable[currentmission] = new Array();
            _root.missionsavailable[currentmission][0] = "Patrol`" + _root.sectormapitems[i][0];
            _root.missionsavailable[currentmission][1] = _root.sectormapitems[i][1];
            _root.missionsavailable[currentmission][2] = _root.sectormapitems[i][2];
            _root.missionsavailable[currentmission][3] = 40000;
            _root.missionsavailable[currentmission][4] = 550;
            _root.missionsavailable[currentmission][5] = 2;
            _root.missionsavailable[currentmission][6] = 65500;
            ++currentmission;
        } // end if
    } // end of for
    for (i = 0; i < _root.starbaselocation.length; i++)
    {
        for (j = 0; j < _root.starbaselocation.length; j++)
        {
            if (_root.starbaselocation[i][0] != _root.starbaselocation[j][0])
            {
                _root.missionsavailable[currentmission] = new Array();
                _root.missionsavailable[currentmission][0] = "Cargorun`" + _root.starbaselocation[i][0] + "`" + _root.starbaselocation[j][0];
                _root.missionsavailable[currentmission][1] = "";
                _root.missionsavailable[currentmission][2] = "";
                distancebetweenbases = Math.sqrt(Math.pow(_root.starbaselocation[i][1] - _root.starbaselocation[j][1], 2) + Math.pow(_root.starbaselocation[i][2] - _root.starbaselocation[j][2], 2));
                _root.missionsavailable[currentmission][3] = Math.round(distancebetweenbases / 0.055000);
                _root.missionsavailable[currentmission][4] = 600;
                _root.missionsavailable[currentmission][5] = 2;
                _root.missionsavailable[currentmission][6] = Math.round(50000 * (distancebetweenbases / 100 / 60));
                _root.missionsavailable[currentmission][7] = 40000;
                ++currentmission;
            } // end if
        } // end of for
    } // end of for
} // End of the function
function func_assmissionbeg(name)
{
    namefound = false;
    for (jj = 0; jj < _root.currentonlineplayers.length; jj++)
    {
        if (_root.currentonlineplayers[jj][1] == name)
        {
            namefound = true;
            break;
        } // end if
    } // end of for
    if (namefound)
    {
        _root.enterintochat(" Assassinate Mission has began, Assassinate " + name + ".", _root.systemchattextcolor);
    }
    else
    {
        _root.enterintochat(" Assassinate Mission can not start, " + name + " is no longer here.", _root.systemchattextcolor);
        _root.playersmission = new Array();
    } // end else if
} // End of the function
function func_playerleftass(leavinname)
{
    if (_root.playersmission[0][0][0] == "assasinate" && leavinname == _root.playersmission[0][0][1])
    {
        _root.playersmission = new Array();
        _root.enterintochat(" Assassinate Mission has ended, " + leavinname + " has left.", _root.systemchattextcolor);
    } // end if
} // End of the function
function func_playerkilled(playerkilled)
{
    if (playerkilled == _root.playersmission[0][0][1] && _root.playersmission[0][0][0] == "assasinate")
    {
        _root.rightside.missionbox.missionscripts.func_assasskilled();
    } // end if
} // End of the function
_root.playersmission = new Array();
initializemissions();

// [Action in Frame 9]
function func_neutralships(currentthread)
{
    if (currentthread[1] == "death")
    {
        shipid = String(currentthread[2]);
        for (tw = 0; tw < _root.neutralships.length; tw++)
        {
            if (shipid == String(_root.neutralships[tw][10]))
            {
                shipname = _root.neutralships[tw][11];
                _root.neutralships.splice(tw, 1);
                killerid = Number(currentthread[3]);
                for (jj = 0; jj < _root.currentonlineplayers.length && dataupdated == false; jj++)
                {
                    if (killerid == String(_root.currentonlineplayers[jj][0]))
                    {
                        killerrealname = _root.currentonlineplayers[jj][1];
                    } // end if
                } // end of for
                message = "HOST: " + shipname + " (" + currentthread[4] + ") killed by " + killerrealname;
                _root.enterintochat(message, _root.regularchattextcolor);
                break;
            } // end if
        } // end of for
    }
    else if (currentthread[1] == "create")
    {
        _root.randomegameevents.func_creationofnpcaiships(currentthread[2], currentthread[3], currentthread[4], currentthread[5], currentthread[6], currentthread[7], currentthread[8], currentthread[9], currentthread[10]);
    } // end else if
} // End of the function
function errorcontrolchecking(errorno)
{
    for (q = 0; q < _root.errorcheckdata.length; q++)
    {
        if (_root.errorcheckdata[q][2] == errorno)
        {
            _root.errorcheckdata.splice(q, 1);
            break;
        } // end if
    } // end of for
} // End of the function
function func_statsshiphit(currentthread)
{
    if (currentthread[0] == "GH" || currentthread[0] == "MH")
    {
        playergothit = currentthread[1];
        for (jj = 0; jj < _root.otherplayership.length; jj++)
        {
            if (_root.otherplayership[jj][0] == playergothit)
            {
                if (currentthread[4] == "SH")
                {
                    _root.otherplayership[jj][41] = Number(currentthread[5]);
                }
                else if (currentthread[4] == "ST")
                {
                    _root.otherplayership[jj][40] = Number(currentthread[5]);
                    if (_root.otherplayership[jj][40] < 0)
                    {
                        _root.otherplayership[jj][40] = 0;
                    } // end if
                    _root.otherplayership[jj][41] = 0;
                } // end else if
                _root.otherplayership[jj][44] = getTimer();
            } // end if
        } // end of for
    } // end if
} // End of the function
function func_otherplayersai(currentthread)
{
    currentothership = _root.otherplayership.length;
    currentname = currentthread[1];
    qz = 0;
    doesplayerexist = false;
    while (qz < _root.otherplayership.length && doesplayerexist == false)
    {
        if (currentname == _root.otherplayership[qz][0])
        {
            doesplayerexist = true;
            currentothership = qz;
            _root.otherplayership[currentothership][0] = currentthread[1];
            recievedxpos = Number(currentthread[2]);
            currentxposition = _root.otherplayership[currentothership][1];
            currentloc = _root.otherplayership[currentothership][30].length;
            _root.otherplayership[currentothership][30][currentloc] = new Array();
            _root.otherplayership[currentothership][30][currentloc][0] = recievedxpos;
            recievedypos = Number(currentthread[3]);
            _root.otherplayership[currentothership][30][currentloc][1] = recievedypos;
            _root.otherplayership[currentothership][30][currentloc][2] = Number(currentthread[4]);
            _root.otherplayership[currentothership][30][currentloc][3] = Number(currentthread[5]);
            _root.otherplayership[currentothership][30][currentloc][6] = new Array();
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = _root.otherplayership[currentothership][30][currentloc][6][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = _root.otherplayership[currentothership][30][currentloc][6][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][1] = _root.otherplayership[currentothership][30][currentloc][6][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][1] = _root.otherplayership[currentothership][30][currentloc][6][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            systemtime = String(getTimer() + _root.clocktimediff);
            systemtime = Number(systemtime.substr(systemtime.length - 4));
            jumpahead = systemtime - Number(currentthread[8]);
            if (jumpahead < -1000)
            {
                jumpahead = jumpahead + 10000;
            } // end if
            jumptime = _root.shipositiondelay - jumpahead + getTimer();
            _root.otherplayership[currentothership][30][currentloc][9] = jumptime;
            _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        } // end if
        ++qz;
    } // end while
    if (doesplayerexist == false)
    {
        currentothership = _root.otherplayership.length;
        _root.otherplayership[currentothership] = new Array();
        _root.otherplayership[currentothership][0] = currentthread[1];
        _root.otherplayership[currentothership][1] = int(currentthread[2]);
        _root.otherplayership[currentothership][2] = int(currentthread[3]);
        _root.otherplayership[currentothership][3] = int(currentthread[4]);
        _root.otherplayership[currentothership][4] = int(currentthread[5]);
        _root.otherplayership[currentothership][5] = new Array();
        _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        _root.otherplayership[currentothership][10] = _root.otherplayership[currentothership][0];
        _root.otherplayership[currentothership][11] = Number(currentthread[7]);
        _root.otherplayership[currentothership][12] = 400;
        _root.otherplayership[currentothership][7] = _root.curenttime + shiptimeoutime * 2;
        _root.otherplayership[currentothership][13] = "N/A";
        _root.otherplayership[currentothership][15] = "alive";
        for (jjj = 0; jjj < currentthread[6].length; jjj++)
        {
            if (currentthread[6].charAt(jjj) == "U")
            {
                _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
            } // end if
            if (currentthread[6].charAt(jjj) == "D")
            {
                _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
            } // end if
            if (currentthread[6].charAt(jjj) == "L")
            {
                _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
            } // end if
            if (currentthread[6].charAt(jjj) == "R")
            {
                _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
            } // end if
        } // end of for
        ++_root.currentailvl;
        if (_root.currentailvl > 100)
        {
            _root.currentailvl = 1;
        } // end if
        _root.gamedisplayarea.attachMovie("shiptype" + _root.otherplayership[currentothership][11], "otherplayership" + _root.otherplayership[currentothership][0], 11100 + _root.currentailvl);
        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
        set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".shiptype", Number(_root.otherplayership[currentothership][11]));
        set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".gothit", false);
    } // end if
} // End of the function
function otherpilot(currentthread)
{
    currentothership = _root.otherplayership.length;
    currentname = currentthread[1];
    qz = 0;
    doesplayerexist = false;
    while (qz < _root.otherplayership.length && doesplayerexist == false)
    {
        if (currentname == _root.otherplayership[qz][0])
        {
            doesplayerexist = true;
            currentothership = qz;
            _root.otherplayership[currentothership][0] = currentthread[1];
            allowance = int(currentthread[5]) / 7;
            recievedxpos = int(currentthread[2]);
            currentloc = _root.otherplayership[currentothership][30].length;
            _root.otherplayership[currentothership][30][currentloc] = new Array();
            _root.otherplayership[currentothership][30][currentloc][0] = recievedxpos;
            recievedypos = int(currentthread[3]);
            _root.otherplayership[currentothership][30][currentloc][1] = recievedypos;
            _root.otherplayership[currentothership][30][currentloc][2] = int(currentthread[4]);
            _root.otherplayership[currentothership][30][currentloc][3] = Number(currentthread[5]);
            _root.otherplayership[currentothership][30][currentloc][6] = new Array();
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = _root.otherplayership[currentothership][30][currentloc][6][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][0] = _root.otherplayership[currentothership][30][currentloc][6][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][1] = _root.otherplayership[currentothership][30][currentloc][6][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][30][currentloc][6][1] = _root.otherplayership[currentothership][30][currentloc][6][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            if (_root.otherplayership[currentothership][16] != currentthread[7])
            {
                newaction = String(currentthread[7]);
                if (newaction == "C" && _root.otherplayership[currentothership][16] != "D")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, false);
                    _root.gamedisplayarea.attachMovie("shipactivateothercloak", "shipactivateothercloak", 69);
                    _root.gamedisplayarea.shipactivateothercloak._x = _root.otherplayership[currentothership][1];
                    _root.gamedisplayarea.shipactivateothercloak._y = _root.otherplayership[currentothership][2];
                    _root.otherplayership[currentothership][16] = newaction;
                }
                else if (newaction == "S" && _root.otherplayership[currentothership][16] != "D")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, true);
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _alpha, 100);
                    _root.otherplayership[currentothership][16] = "S";
                }
                else if (newaction != "S" && newaction != "C")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, true);
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _alpha, 100);
                    if (_root.otherplayership[currentothership][16] == "C")
                    {
                        _root.gamedisplayarea["otherplayership" + _root.otherplayership[currentothership][0]].attachMovie("shipactivateotherdecloak", "shipactivateotherdecloak", 69);
                        
                    } // end if
                    _root.otherplayership[currentothership][16] = newaction;
                } // end else if
            } // end else if
            systemtime = String(getTimer() + _root.clocktimediff);
            systemtime = Number(systemtime.substr(systemtime.length - 4));
            jumpahead = systemtime - Number(currentthread[8]);
            if (jumpahead < -1000)
            {
                jumpahead = jumpahead + 10000;
            } // end if
            jumptime = _root.shipositiondelay - jumpahead + getTimer();
            _root.otherplayership[currentothership][30][currentloc][9] = jumptime;
            _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        } // end if
        ++qz;
    } // end while
    if (doesplayerexist == false)
    {
        currentothership = qz;
        _root.otherplayership[currentothership] = new Array();
        _root.otherplayership[currentothership][0] = currentthread[1];
        _root.otherplayership[currentothership][1] = int(currentthread[2]);
        _root.otherplayership[currentothership][2] = int(currentthread[3]);
        _root.otherplayership[currentothership][3] = int(currentthread[4]);
        _root.otherplayership[currentothership][4] = int(currentthread[5]);
        _root.otherplayership[currentothership][5] = new Array();
        _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        _root.otherplayership[currentothership][7] = _root.curenttime + shiptimeoutime * 2;
        _root.otherplayership[currentothership][30] = new Array();
        jjjj = 0;
        dataupdated = false;
        while (jjjj < _root.currentonlineplayers.length && dataupdated == false)
        {
            if (_root.otherplayership[currentothership][0] == _root.currentonlineplayers[jjjj][0])
            {
                dataupdated = true;
                _root.otherplayership[currentothership][10] = _root.currentonlineplayers[jjjj][1];
                _root.otherplayership[currentothership][11] = _root.currentonlineplayers[jjjj][2];
                _root.otherplayership[currentothership][12] = _root.currentonlineplayers[jjjj][3];
                _root.otherplayership[currentothership][13] = _root.currentonlineplayers[jjjj][4];
            } // end if
            ++jjjj;
        } // end while
        if (dataupdated == false)
        {
            _root.otherplayership.splice(currentothership, 1);
        }
        else
        {
            _root.otherplayership[currentothership][15] = "alive";
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][5][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            _root.otherplayership[currentothership][16] = String(currentthread[7]);
            _root.gamedisplayarea.attachMovie("shiptype" + _root.otherplayership[currentothership][11], "otherplayership" + _root.otherplayership[currentothership][0], 2300 + Number(_root.otherplayership[currentothership][0]));
            if (_root.otherplayership[currentothership][16] == "C")
            {
                setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, false);
            } // end if
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".shiptype", Number(_root.otherplayership[currentothership][11]));
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".gothit", false);
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".isplayervisible", true);
            _root.otherplayership[currentothership][40] = null;
            _root.otherplayership[currentothership][41] = null;
            _root.otherplayership[currentothership][42] = null;
            _root.otherplayership[currentothership][43] = null;
            _root.otherplayership[currentothership][44] = null;
            _root.otherplayership[currentothership][46] = null;
        } // end if
    } // end else if
} // End of the function
function func_closegameserver()
{
    information = "QUIT`" + _root.errorchecknumber + "`~";
    _root.mysocket.send(information);
    _root.errorcheckedmessage(information, _root.errorchecknumber);
} // End of the function
function login()
{
    _root.currentonlineplayers = new Array();
    information = "LOGIN`" + _root.playershipstatus[3][0] + "`" + _root.playershipstatus[3][2] + "`" + _root.playershipstatus[5][0] + "`" + _root.playershipstatus[5][3] + "`" + _root.playershipstatus[5][2] + "`" + _root.playershipstatus[5][9] + "`" + _root.playershipstatus[5][10] + "`" + _root.playershipstatus[3][3] + "~";
    _root.mysocket.send(information);
} // End of the function
function xmlhandler(doc)
{
    loadedvars = String(doc);
    dataprocess(loadedvars);
} // End of the function
function dataprocess(myLoadVars)
{
    curenttime = getTimer();
    gunshot0fired = false;
    gunshot1fired = false;
    gunshot2fired = false;
    gunshot3fired = false;
    newinfo = loadedvars.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentthread = newinfo[i].split("`");
        if (currentthread[0] == "PI" && !isNaN(currentthread[1]) && _root.playershipstatus[3][0] != Number(currentthread[1]))
        {
            otherpilot(currentthread);
        }
        else if (currentthread[0] == "AI")
        {
            func_otherplayersai(currentthread);
        }
        else if (currentthread[0] == "MI")
        {
            xcoord = Number(currentthread[2]);
            ycoord = Number(currentthread[3]);
            missilename = currentthread[1] + "n" + Number(currentthread[7]);
            setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _rotation, Number(currentthread[5]));
            setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _x, xcoord - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _y, ycoord - _root.shipcoordinatey);
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".xposition", xcoord);
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".yposition", ycoord);
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".missiletype", Number(currentthread[6]));
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".velocity", Number(currentthread[4]));
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".rotation", Number(currentthread[5]));
            set("_root.gamedisplayarea.othermissilefire" + missilename + ".currentkeypresses", currentthread[8]);
        } // end else if
        if (currentthread[0] == "MF")
        {
            if (_root.soundvolume != "off")
            {
                _root.missilefiresound.start();
            } // end if
            missileno = Number(currentthread[6]);
            if (_root.othermissilefire.length < 1)
            {
                _root.othermissilefire = new Array();
            } // end if
            if (_root.missile[missileno][8] == "SEEKER" || _root.missile[missileno][8] == "EMP" || _root.missile[missileno][8] == "DISRUPTER")
            {
                if (_root.playershipstatus[3][0] == Number(currentthread[9]))
                {
                    ++currentothermissileshot;
                    if (currentothermissileshot >= 1000)
                    {
                        currentothermissileshot = 1;
                    } // end if
                    lastvar = _root.othermissilefire.length;
                    _root.othermissilefire[lastvar] = new Array();
                    _root.othermissilefire[lastvar][0] = currentthread[1];
                    _root.othermissilefire[lastvar][1] = int(currentthread[2]);
                    _root.othermissilefire[lastvar][2] = int(currentthread[3]);
                    velocity = int(currentthread[4]);
                    _root.othermissilefire[lastvar][6] = int(currentthread[5]);
                    relativefacing = _root.othermissilefire[lastvar][6];
                    movementofanobjectwiththrust();
                    _root.othermissilefire[lastvar][3] = xmovement;
                    _root.othermissilefire[lastvar][4] = ymovement;
                    _root.othermissilefire[lastvar][7] = int(currentthread[6]);
                    _root.othermissilefire[lastvar][8] = currentothermissileshot;
                    _root.othermissilefire[lastvar][9] = int(currentthread[7]);
                    _root.othermissilefire[lastvar][10] = "other";
                    _root.othermissilefire[lastvar][5] = _root.curenttime + _root.missile[_root.othermissilefire[lastvar][7]][2];
                    systemtime = String(getTimer() + _root.clocktimediff);
                    systemtime = Number(systemtime.substr(systemtime.length - 4));
                    jumpahead = systemtime - Number(currentthread[8]);
                    if (jumpahead < -1000)
                    {
                        jumpahead = jumpahead + 10000;
                    } // end if
                    jumpahead = (jumpahead + _root.shipositiondelay) / 1000;
                    _root.othermissilefire[lastvar][1] = _root.othermissilefire[lastvar][1] + _root.othermissilefire[lastvar][3] * jumpahead;
                    _root.othermissilefire[lastvar][2] = _root.othermissilefire[lastvar][2] + _root.othermissilefire[lastvar][4] * jumpahead;
                    _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "fire", "othermissilefire" + _root.othermissilefire[lastvar][8], 999000 + _root.othermissilefire[lastvar][8]);
                    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _rotation, _root.othermissilefire[lastvar][6]);
                    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _x, _root.othermissilefire[lastvar][1] - _root.shipcoordinatex);
                    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _y, _root.othermissilefire[lastvar][2] - _root.shipcoordinatey);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xposition", _root.othermissilefire[lastvar][1]);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".yposition", _root.othermissilefire[lastvar][2]);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".missiletype", _root.othermissilefire[lastvar][7]);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".velocity", int(currentthread[4]));
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".seeking", "HOST");
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".shooterid", _root.othermissilefire[lastvar][0]);
                    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".globalid", _root.othermissilefire[lastvar][9]);
                    _root.othermissilefire[lastvar][11] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._width / 2;
                    _root.othermissilefire[lastvar][12] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._height / 2;
                    _root.othermissilefire[lastvar][13] = "SEEKING";
                }
                else
                {
                    ++currentothermissileshot;
                    if (currentothermissileshot >= 1000)
                    {
                        currentothermissileshot = 1;
                    } // end if
                    systemtime = String(getTimer() + _root.clocktimediff);
                    systemtime = Number(systemtime.substr(systemtime.length - 4));
                    jumpahead = systemtime - Number(currentthread[8]);
                    if (jumpahead < -1000)
                    {
                        jumpahead = jumpahead + 10000;
                    } // end if
                    jumpahead = (jumpahead + _root.shipositiondelay) / 1000;
                    relativefacing = Number(currentthread[5]);
                    movementofanobjectwiththrust();
                    xcoord = Number(currentthread[2]) + xmovement * jumpahead;
                    ycoord = Number(currentthread[3]) + ymovement * jumpahead;
                    missilename = currentthread[1] + "n" + Number(currentthread[7]);
                    _root.gamedisplayarea.attachMovie("missile" + Number(currentthread[6]) + "fire", "othermissilefire" + missilename, 999000 + currentothermissileshot);
                    setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _rotation, Number(currentthread[5]));
                    setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _x, xcoord - _root.shipcoordinatex);
                    setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _y, ycoord - _root.shipcoordinatey);
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".xposition", xcoord);
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".yposition", ycoord);
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".missiletype", Number(currentthread[6]));
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".velocity", Number(currentthread[4]));
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".seeking", "OTHER");
                    set("_root.gamedisplayarea.othermissilefire" + missilename + ".rotation", Number(currentthread[5]));
                    _root.othermissilefire[lastvar][13] = "NOTSEEKING";
                } // end else if
            }
            else
            {
                ++currentothermissileshot;
                if (currentothermissileshot >= 1000)
                {
                    currentothermissileshot = 1;
                } // end if
                lastvar = _root.othermissilefire.length;
                _root.othermissilefire[lastvar] = new Array();
                _root.othermissilefire[lastvar][0] = currentthread[1];
                _root.othermissilefire[lastvar][1] = int(currentthread[2]);
                _root.othermissilefire[lastvar][2] = int(currentthread[3]);
                velocity = int(currentthread[4]);
                _root.othermissilefire[lastvar][6] = int(currentthread[5]);
                relativefacing = _root.othermissilefire[lastvar][6];
                movementofanobjectwiththrust();
                _root.othermissilefire[lastvar][3] = xmovement;
                _root.othermissilefire[lastvar][4] = ymovement;
                _root.othermissilefire[lastvar][7] = int(currentthread[6]);
                _root.othermissilefire[lastvar][8] = currentothermissileshot;
                _root.othermissilefire[lastvar][9] = int(currentthread[7]);
                _root.othermissilefire[lastvar][10] = "other";
                _root.othermissilefire[lastvar][5] = _root.curenttime + _root.missile[_root.othermissilefire[lastvar][7]][2];
                systemtime = String(getTimer() + _root.clocktimediff);
                systemtime = Number(systemtime.substr(systemtime.length - 4));
                jumpahead = systemtime - Number(currentthread[8]);
                if (jumpahead < -1000)
                {
                    jumpahead = jumpahead + 10000;
                } // end if
                jumpahead = (jumpahead + _root.shipositiondelay) / 1000;
                _root.othermissilefire[lastvar][1] = _root.othermissilefire[lastvar][1] + _root.othermissilefire[lastvar][3] * jumpahead;
                _root.othermissilefire[lastvar][2] = _root.othermissilefire[lastvar][2] + _root.othermissilefire[lastvar][4] * jumpahead;
                _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "fire", "othermissilefire" + _root.othermissilefire[lastvar][8], 999000 + _root.othermissilefire[lastvar][8]);
                setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _rotation, _root.othermissilefire[lastvar][6]);
                setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _x, _root.othermissilefire[lastvar][1] - _root.shipcoordinatex);
                setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _y, _root.othermissilefire[lastvar][2] - _root.shipcoordinatey);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xmovement", _root.othermissilefire[lastvar][3]);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".ymovement", _root.othermissilefire[lastvar][4]);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xposition", _root.othermissilefire[lastvar][1]);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".yposition", _root.othermissilefire[lastvar][2]);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".turning", "0");
                _root.othermissilefire[lastvar][11] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._width / 2;
                _root.othermissilefire[lastvar][12] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._height / 2;
                _root.othermissilefire[lastvar][13] = "NOTSEEKING";
            } // end else if
        }
        else if (currentthread[0] == "GF")
        {
            if (_root.othergunfire.length < 1)
            {
                _root.othergunfire = new Array();
            } // end if
            ++_root.currentotherplayshot;
            if (_root.currentotherplayshot >= 500)
            {
                _root.currentotherplayshot = 1;
            } // end if
            lastvar = _root.othergunfire.length;
            ++_root.othergunfirecount;
            _root.othergunfire[lastvar] = new Array();
            _root.othergunfire[lastvar][0] = currentthread[1];
            _root.othergunfire[lastvar][1] = int(currentthread[2]);
            _root.othergunfire[lastvar][2] = int(currentthread[3]);
            velocity = int(currentthread[4]);
            _root.othergunfire[lastvar][6] = int(currentthread[5]);
            relativefacing = _root.othergunfire[lastvar][6];
            movementofanobjectwiththrust();
            _root.othergunfire[lastvar][3] = xmovement;
            _root.othergunfire[lastvar][4] = ymovement;
            _root.othergunfire[lastvar][7] = int(currentthread[6]);
            _root.othergunfire[lastvar][8] = _root.currentotherplayshot;
            _root.othergunfire[lastvar][9] = int(currentthread[7]);
            _root.othergunfire[lastvar][10] = "other";
            _root.othergunfire[lastvar][5] = _root.curenttime + _root.guntype[_root.othergunfire[lastvar][7]][1] * 1000;
            systemtime = String(getTimer() + _root.clocktimediff);
            systemtime = Number(systemtime.substr(systemtime.length - 4));
            jumpahead = systemtime - Number(currentthread[8]);
            if (jumpahead < -1000)
            {
                jumpahead = jumpahead + 10000;
            } // end if
            jumpahead = (jumpahead + _root.shipositiondelay) / 1000;
            _root.othergunfire[lastvar][1] = _root.othergunfire[lastvar][1] + _root.othergunfire[lastvar][3] * jumpahead;
            _root.othergunfire[lastvar][2] = _root.othergunfire[lastvar][2] + _root.othergunfire[lastvar][4] * jumpahead;
            _root.gamedisplayarea.attachMovie("guntype" + _root.othergunfire[lastvar][7] + "fire", "othergunfire" + _root.othergunfire[lastvar][8], 3500 + _root.othergunfire[lastvar][8]);
            setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _rotation, _root.othergunfire[lastvar][6]);
            setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _x, _root.othergunfire[lastvar][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _y, _root.othergunfire[lastvar][2] - _root.shipcoordinatey);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".xmovement", _root.othergunfire[lastvar][3]);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".ymovement", _root.othergunfire[lastvar][4]);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".xposition", _root.othergunfire[lastvar][1]);
            set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".yposition", _root.othergunfire[lastvar][2]);
            _root.othergunfire[lastvar][11] = _root.gamedisplayarea["othergunfire" + _root.othergunfire[lastvar][8]]._width / 2;
            _root.othergunfire[lastvar][12] = _root.gamedisplayarea["othergunfire" + _root.othergunfire[lastvar][8]]._height / 2;
            if (_root.soundvolume != "off")
            {
                _root["guntype" + _root.othergunfire[lastvar][7] + "sound"].start();
            } // end if
        }
        else if (currentthread[0] == "GH")
        {
            func_statsshiphit(currentthread);
            playerwhogothit = currentthread[1];
            shooterofbullet = Number(currentthread[2]);
            shooternumberfire = Number(currentthread[3]);
            if (playerwhogothit == "SQ")
            {
                basethatgothit = String(currentthread[4]);
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othergunfire.length; cc++)
                    {
                        if (_root.othergunfire[cc][0] == shooterofbullet && _root.othergunfire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
                            gunshottype = _root.othergunfire[cc][7];
                            shotdamage = _root.guntype[gunshottype][4];
                            _root.othergunfire.splice(cc, 1);
                            cc = 2000;
                            _root.gamedisplayarea.gamebackground["playerbase" + basethatgothit].func_takeonothershots(shotdamage);
                        } // end if
                    } // end of for
                } // end if
            }
            else if (playerwhogothit == "TB")
            {
                basethatgothit = _root.teambases[Number(currentthread[4])][0];
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othergunfire.length; cc++)
                    {
                        if (_root.othergunfire[cc][0] == shooterofbullet && _root.othergunfire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
                            gunshottype = _root.othergunfire[cc][7];
                            shotdamage = _root.guntype[gunshottype][4];
                            _root.othergunfire.splice(cc, 1);
                            cc = 2000;
                            _root.gamedisplayarea.gamebackground["teambase" + basethatgothit].func_takeonothershots(shotdamage);
                        } // end if
                    } // end of for
                } // end if
            }
            else if (playerwhogothit == "MET")
            {
                meteor = String(currentthread[4]);
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othergunfire.length; cc++)
                    {
                        if (_root.othergunfire[cc][0] == shooterofbullet && _root.othergunfire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
                            gunshottype = _root.othergunfire[cc][7];
                            shotdamage = _root.guntype[gunshottype][4];
                            _root.othergunfire.splice(cc, 1);
                            cc = 2000;
                        } // end if
                    } // end of for
                } // end if
            }
            else
            {
                set("_root.gamedisplayarea.otherplayership" + playerwhogothit + ".gothit", true);
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othergunfire.length; cc++)
                    {
                        if (_root.othergunfire[cc][0] == shooterofbullet && _root.othergunfire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
                            _root.othergunfire.splice(cc, 1);
                            cc = 2000;
                        } // end if
                    } // end of for
                }
                else if (_root.playershipstatus[3][0] == shooterofbullet)
                {
                    removeMovieClip ("_root.gamedisplayarea.playergunfire" + shooternumberfire);
                } // end else if
            } // end else if
        }
        else if (currentthread[0] == "STATS")
        {
            func_statuscheck(currentthread);
        }
        else if (currentthread[0] == "NEUT")
        {
            func_neutralships(currentthread);
        }
        else if (currentthread[0] == "SP")
        {
            if (currentthread[1] == "MINEHIT")
            {
                mineid = currentthread[2];
                set("_root.gamedisplayarea.playermine" + mineid + ".justgothit", true);
            }
            else
            {
                playerid = currentthread[1];
                specialtype = Number(currentthread[2]);
                specialx = Number(currentthread[3]);
                specialy = Number(currentthread[4]);
                velocity = Number(currentthread[5]);
                rotation = Number(currentthread[6]);
                bringinspecial(playerid, specialtype, specialx, specialy, velocity, rotation);
            } // end else if
        }
        else if (currentthread[0] == "MH")
        {
            func_statsshiphit(currentthread);
            playerwhogothit = currentthread[1];
            shooterofbullet = currentthread[2];
            shooternumberfire = int(currentthread[3]);
            if (playerwhogothit == "SQ")
            {
                basethatgothit = String(currentthread[4]);
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othermissilefire.length; cc++)
                    {
                        if (_root.othermissilefire[cc][0] == shooterofbullet && _root.othermissilefire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[cc][8]);
                            missileshottype = _root.othermissilefire[cc][7];
                            shotdamage = _root.missile[missileshottype][4];
                            _root.othermissilefire.splice(cc, 1);
                            cc = 2000;
                            _root.gamedisplayarea.gamebackground["playerbase" + basethatgothit].func_takeonothershots(shotdamage);
                        } // end if
                        otherplayerdamage;
                    } // end of for
                } // end if
            }
            else if (playerwhogothit == "TB")
            {
                basethatgothit = _root.teambases[Number(currentthread[4])][0];
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othermissilefire.length; cc++)
                    {
                        if (_root.othermissilefire[cc][0] == shooterofbullet && _root.othermissilefire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[cc][8]);
                            missileshottype = _root.othermissilefire[cc][7];
                            shotdamage = _root.missile[missileshottype][4];
                            _root.othermissilefire.splice(cc, 1);
                            cc = 2000;
                            _root.gamedisplayarea.gamebackground["teambase" + basethatgothit].func_takeonothershots(shotdamage);
                        } // end if
                        otherplayerdamage;
                    } // end of for
                } // end if
            }
            else if (playerwhogothit == "MET")
            {
                basethatgothit = String(currentthread[4]);
                shotdamage = 0;
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othermissilefire.length; cc++)
                    {
                        if (_root.othermissilefire[cc][0] == shooterofbullet && _root.othermissilefire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[cc][8]);
                            missileshottype = _root.othermissilefire[cc][7];
                            shotdamage = _root.missile[missileshottype][4];
                            _root.othermissilefire.splice(cc, 1);
                            cc = 2000;
                        } // end if
                        otherplayerdamage;
                    } // end of for
                } // end if
            }
            else
            {
                set("_root.gamedisplayarea.otherplayership" + playerwhogothit + ".gothit", true);
                if (_root.playershipstatus[3][0] != shooterofbullet)
                {
                    for (cc = 0; cc < _root.othermissilefire.length; cc++)
                    {
                        if (_root.othermissilefire[cc][0] == shooterofbullet && _root.othermissilefire[cc][9] == shooternumberfire)
                        {
                            removeMovieClip ("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[cc][8]);
                            _root.othermissilefire.splice(cc, 1);
                            cc = 2000;
                        } // end if
                    } // end of for
                } // end if
                if (_root.playershipstatus[3][0] == shooterofbullet)
                {
                    removeMovieClip ("_root.gamedisplayarea.playergunfire" + shooternumberfire);
                } // end if
                missilename = currentthread[2] + "n" + Number(currentthread[3]);
                removeMovieClip ("_root.gamedisplayarea.othermissilefire" + missilename);
            } // end else if
        }
        else if (currentthread[0] == "RG")
        {
            if (currentthread[1] == "NG")
            {
                temp = new Array();
                for (jt = 4; jt < currentthread.length; jt++)
                {
                    temp[jt - 4] = Number(currentthread[jt]);
                } // end of for
                _root.func_createracinggamepoints(temp, currentthread[2], currentthread[3]);
            }
            else if (currentthread[1] == "FG")
            {
                playerloc = Number(currentthread[2]);
                playerplaced = Number(currentthread[3]);
                _root.func_racinggamefinisher(playerloc, playerplaced);
            }
            else if (currentthread[1] == "EG")
            {
                _root.func_racinggamefinished();
            } // end else if
        }
        else if (currentthread[0] == "SQW")
        {
            if (currentthread[1] == "STARTING")
            {
                message = "HOST: A Squad War Between ";
                _root.squadwarinfo[0] = true;
                _root.squadwarinfo[2] = false;
                _root.squadwarinfo[1] = new Array();
                for (jt = 2; jt < currentthread.length; jt++)
                {
                    _root.squadwarinfo[1][_root.squadwarinfo[1].length] = currentthread[jt];
                    message = message + currentthread[jt];
                    if (jt != currentthread.length - 1)
                    {
                        message = message + " and ";
                    } // end if
                } // end of for
                message = message + " is about to begin!";
                _root.enterintochat(message, _root.systemchattextcolor);
                _root.gotoAndStop("teambase");
            }
            else if (currentthread[1] == "BEGIN")
            {
                _root.squadwarinfo[2] = true;
                message = "HOST: Squad War Has Now Started!";
                _root.enterintochat(message, _root.systemchattextcolor);
            }
            else if (currentthread[1] == "MODENDED")
            {
                _root.squadwarinfo[0] = false;
                _root.squadwarinfo[2] = false;
                message = "HOST: Squad War Was Ended By A Mod";
                _root.enterintochat(message, _root.systemchattextcolor);
                _root.playershipstatus[5][2] = "N/A";
                for (jt = 0; jt < _root.currentonlineplayers.length; jt++)
                {
                    _root.currentonlineplayers[jt][4] = "N/A";
                } // end of for
                _root.gotoAndStop("teambase");
            }
            else if (currentthread[1] == "INPROGRESS")
            {
                message = "HOST: A Squad War Between ";
                _root.squadwarinfo[0] = true;
                _root.squadwarinfo[2] = true;
                _root.squadwarinfo[1] = new Array();
                for (jt = 2; jt < currentthread.length; jt++)
                {
                    _root.squadwarinfo[1][_root.squadwarinfo[1].length] = currentthread[jt];
                    message = message + currentthread[jt];
                    if (jt != currentthread.length - 1)
                    {
                        message = message + " and ";
                    } // end if
                } // end of for
                message = message + " is already progress.";
                _root.enterintochat(message, _root.systemchattextcolor);
                _root.gotoAndStop("teambase");
            } // end else if
        }
        else if (currentthread[0] == "DM")
        {
            if (currentthread[1] == "BH")
            {
                _root.teambases[Number(currentthread[2])][10] = _root.teambases[Number(currentthread[2])][10] - Number(currentthread[3]);
                if (_root.teambases[Number(currentthread[2])][10] < 1)
                {
                    _root.teambases[Number(currentthread[2])][10] = 1;
                } // end if
            }
            else if (currentthread[1] == "NG")
            {
                for (jt = 2; jt < currentthread.length; jt++)
                {
                    _root.teambases[jt - 2][10] = Number(currentthread[jt]);
                    _root.teambases[jt - 2][11] = _root.teambasetypes[_root.teambases[jt - 2][3]][2];
                    if (_root.teambases[jt - 2][10] < 1)
                    {
                        _root.teambases[jt - 2][10] = 1;
                    } // end if
                } // end of for
            }
            else if (currentthread[1] == "EG")
            {
                if (_root.teamdeathmatch == true)
                {
                    basekillingplayer = Number(currentthread[3]);
                    basekilled = Number(currentthread[2]);
                    looseteamname = _root.teambases[basekilled][0].substr(2);
                    if (_root.squadwarinfo[0] == true)
                    {
                        looseteamname = _root.squadwarinfo[1][basekilled];
                    } // end if
                    _root.teambases[basekilled][10] = 0;
                    for (jt = 0; jt < _root.currentonlineplayers.length; jt++)
                    {
                        if (_root.currentonlineplayers[jt][0] == basekillingplayer)
                        {
                            winteamno = Number(_root.currentonlineplayers[jt][4]);
                            winplayname = _root.currentonlineplayers[jt][1];
                            winteamname = _root.teambases[winteamno][0].substr(2);
                            if (_root.squadwarinfo[0] == true)
                            {
                                winteamname = _root.squadwarinfo[1][winteamno];
                            } // end if
                        } // end if
                    } // end of for
                    rewardf = _root.deathfgamerewards();
                    colourtouse = _root.systemchattextcolor;
                    message = "HOST: Player " + winplayname + " of Team " + winteamname + " has destroyed " + looseteamname + "\'s Base";
                    _root.enterintochat(message, colourtouse);
                    message = "HOST: Reward for Team " + winteamname + " is " + _root.func_formatnumber(rewardf) + " Funds";
                    _root.enterintochat(message, colourtouse);
                    if (_root.playershipstatus[5][2] == winteamno)
                    {
                        _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(rewardf);
                    } // end if
                    _root.playershipstatus[5][2] = "N/A";
                    for (jt = 0; jt < _root.currentonlineplayers.length; jt++)
                    {
                        _root.currentonlineplayers[jt][4] = "N/A";
                    } // end of for
                    if (_root.squadwarinfo[0] == true)
                    {
                        _root.squadwarinfo[0] = false;
                        _root.squadwarinfo[2] = false;
                    } // end if
                    _root.gotoAndStop("teambase");
                } // end else if
            } // end else if
        }
        else if (currentthread[0] == "CH")
        {
            if (currentthread[2] == "M" || currentthread[2] == "PM" || currentthread[2] == "TM" || currentthread[2] == "SM" || currentthread[2] == "AM")
            {
                ignoringplayer = false;
                for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
                {
                    playerthatsaidit = currentthread[1];
                    if (currentthread[1] == "HOST")
                    {
                        message = "HOST: " + _root.func_checkothercharacters(currentthread[3]);
                        break;
                        continue;
                    } // end if
                    if (currentthread[1] == _root.currentonlineplayers[jjjj][0])
                    {
                        playerthatsaidit = _root.currentonlineplayers[jjjj][1];
                        if (currentthread[2] == "PM")
                        {
                            _root.gamechatinfo[5] = playerthatsaidit;
                        } // end if
                        if (currentthread[2] != "AM")
                        {
                            ignoringplayer = _root.func_ignoreplayer(playerthatsaidit);
                        }
                        else
                        {
                            ignoringplayer = false;
                        } // end else if
                        if (ignoringplayer != true)
                        {
                            message = playerthatsaidit + ": " + _root.func_checkothercharacters(currentthread[3]);
                        } // end if
                        break;
                    } // end if
                } // end of for
                if (ignoringplayer != true)
                {
                    if (currentthread[2] == "M")
                    {
                        colourtouse = _root.regularchattextcolor;
                    } // end if
                    if (currentthread[2] == "PM")
                    {
                        colourtouse = _root.privatechattextcolor;
                    } // end if
                    if (currentthread[2] == "TM")
                    {
                        colourtouse = _root.teamchattextcolor;
                    } // end if
                    if (currentthread[2] == "SM")
                    {
                        colourtouse = _root.squadchattextcolor;
                    } // end if
                    if (currentthread[2] == "AM")
                    {
                        colourtouse = _root.arenachattextcolor;
                    } // end if
                    _root.enterintochat(message, colourtouse);
                } // end if
            }
            else if (currentthread[2] == "WM")
            {
                message = "HOST: Your Base Has Been Attacked";
                colourtouse = _root.squadchattextcolor;
                _root.enterintochat(message, colourtouse);
            } // end else if
            _root.refreshchatdisplay = true;
        }
        else if (currentthread[0] == "ECC")
        {
            errorno = Number(currentthread[1]);
            errorcontrolchecking(errorno);
        }
        else if (currentthread[0] == "HC")
        {
            currentai = int(_root.aishipshosted.length);
            if (currentai < 1)
            {
                _root.aishipshosted = new Array();
                currentai = 0;
            } // end if
            _root.aishipshosted[currentai] = new Array();
            _root.aishipshosted[currentai][0] = Number(currentthread[1]);
            _root.aishipshosted[currentai][1] = Number(currentthread[2]);
            _root.aishipshosted[currentai][2] = Number(currentthread[3]);
            _root.aishipshosted[currentai][3] = Number(currentthread[4]);
            _root.aishipshosted[currentai][4] = Number(currentthread[5]);
            _root.aishipshosted[currentai][5] = "";
            _root.aishipshosted[currentai][6] = _root.aishipsinfo[_root.aishipshosted[currentai][0]][1];
            _root.aishipshosted[currentai][7] = _root.aishipsinfo[_root.aishipshosted[currentai][0]][0];
            _root.gamedisplayarea.attachMovie("shiptype" + _root.aishipshosted[currentai][6] + "ai", "aiship" + _root.aishipshosted[currentai][0], 11000 + _root.aishipshosted[currentai][0]);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _x, _root.aishipshosted[currentai][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _y, _root.aishipshosted[currentai][2] - _root.shipcoordinatey);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _rotation, _root.aishipshosted[currentai][3]);
            set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".shipid", _root.aishipshosted[currentai][0]);
        }
        else if (currentthread[0] == "AIIN")
        {
            currentai = int(_root.aishipshosted.length);
            if (currentai < 1)
            {
                _root.aishipshosted = new Array();
                currentai = 0;
            } // end if
            _root.aishipshosted[currentai] = new Array();
            _root.aishipshosted[currentai][0] = Number(currentthread[1]);
            _root.aishipshosted[currentai][1] = _root.shipcoordinatex + Math.random() * 750 - 340;
            _root.aishipshosted[currentai][2] = _root.shipcoordinatey + Math.random() * 750 - 340;
            _root.aishipshosted[currentai][3] = Math.random() * 360;
            _root.aishipshosted[currentai][4] = 0;
            _root.aishipshosted[currentai][5] = "";
            _root.aishipshosted[currentai][6] = _root.aishipsinfo[_root.aishipshosted[currentai][0]][1];
            _root.aishipshosted[currentai][7] = _root.aishipsinfo[_root.aishipshosted[currentai][0]][0];
            _root.gamedisplayarea.attachMovie("shiptype" + _root.aishipshosted[currentai][6] + "ai", "aiship" + _root.aishipshosted[currentai][0], 11000 + _root.aishipshosted[currentai][0]);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _x, _root.aishipshosted[currentai][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0], _y, _root.aishipshosted[currentai][2] - _root.shipcoordinatey);
            set("_root.gamedisplayarea.aiship" + _root.aishipshosted[currentai][0] + ".shipid", _root.aishipshosted[currentai][0]);
        }
        else if (currentthread[0] == "TO")
        {
            currentplayerid = currentthread[1];
            for (jj = 0; jj < _root.currentonlineplayers.length; jj++)
            {
                if (currentplayerid == _root.currentonlineplayers[jj][0])
                {
                    leavingplayername = _root.currentonlineplayers[jj][1];
                    _root.func_playerleftass(leavingplayername);
                    _root.currentonlineplayers.splice(jj, 1);
                } // end if
            } // end of for
            _root.refreshonlinelist = true;
            _root.gamechatinfo[1].splice(0, 0, 0);
            _root.gamechatinfo[1][0] = new Array();
            _root.gamechatinfo[1][0][0] = "Host: " + leavingplayername + " has left";
            _root.gamechatinfo[1][0][1] = _root.regularchattextcolor;
            if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
            {
                _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
            } // end if
            _root.refreshchatdisplay = true;
        }
        else if (currentthread[0] == "TC")
        {
            currentplayerid = currentthread[1];
            if (currentthread[2] == "DK")
            {
                if (currentplayerid == _root.playershipstatus[3][0])
                {
                    _root.hasplayerbeenlistedasdocked = true;
                    _root.playersdockedtime = getTimer();
                }
                else
                {
                    jjjj = 0;
                    dataupdated = false;
                    while (jjjj < _root.otherplayership.length && dataupdated == false)
                    {
                        if (currentplayerid == _root.otherplayership[jjjj][0])
                        {
                            currentothership = jjjj;
                            _root.otherplayership[jjjj][15] = "dead";
                            dataupdated = true;
                            _root.gamedisplayarea.attachMovie("shiptypedock", "otherplayership" + _root.otherplayership[currentothership][0], 2300 + Number(_root.otherplayership[currentothership][0]));
                            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
                            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
                            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
                            _root.otherplayership[currentothership][6] = _root.curenttime + 1200;
                            _root.otherplayership[zz][15] = "docking";
                        } // end if
                        ++jjjj;
                    } // end while
                } // end if
            } // end else if
            if (currentthread[2] == "SC")
            {
                currentthread[3] = Number(currentthread[3]);
                if (isNaN(currentthread[3]))
                {
                    currentthread[3] = 0;
                } // end if
                jj = 0;
                dataupdated = false;
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (currentplayerid == _root.currentonlineplayers[jj][0])
                    {
                        _root.currentonlineplayers[jj][2] = Number(currentthread[3]);
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
            } // end if
            if (currentthread[2] == "TM")
            {
                jj = 0;
                dataupdated = false;
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (currentplayerid == _root.currentonlineplayers[jj][0])
                    {
                        _root.currentonlineplayers[jj][4] = currentthread[3];
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                if (_root.playershipstatus[3][0] == currentthread[1])
                {
                    _root.playershipstatus[5][2] = currentthread[3];
                    _root.toprightinformation.playerteam = "Team: " + _root.playershipstatus[5][2];
                } // end if
            } // end if
            if (currentthread[2] == "SCORE")
            {
                currentthread[3] = Number(currentthread[3]);
                if (isNaN(currentthread[3]) || Number(currentthread[3] < 0))
                {
                    currentthread[3] = 0;
                } // end if
                jj = 0;
                dataupdated = false;
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (currentplayerid == _root.currentonlineplayers[jj][0])
                    {
                        _root.currentonlineplayers[jj][5] = Number(_root.currentonlineplayers[jj][5]) + Number(currentthread[3]);
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                if (_root.playershipstatus[3][0] == currentthread[1])
                {
                    _root.playershipstatus[5][9] = Number(_root.playershipstatus[5][9]) + Number(currentthread[3]);
                    _root.toprightinformation.playerscore = "Score: " + Math.floor(Number(_root.playershipstatus[5][9]) / _root.scoreratiomodifier);
                } // end if
            } // end if
            if (currentthread[2] == "SD")
            {
                if (String(currentthread[1]) == String(999999))
                {
                    aiwaskilled = true;
                }
                else
                {
                    aiwaskilled = false;
                } // end else if
                jjjj = 0;
                dataupdated = false;
                while (jjjj < _root.otherplayership.length && dataupdated == false)
                {
                    if (currentplayerid == _root.otherplayership[jjjj][0])
                    {
                        currentothership = jjjj;
                        _root.otherplayership[jjjj][15] = "dead";
                        dataupdated = true;
                        _root.gamedisplayarea.attachMovie("shiptypedead", "otherplayership" + _root.otherplayership[currentothership][0], 2300 + Number(_root.otherplayership[currentothership][0]));
                        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
                        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
                        setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
                        _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
                        _root.otherplayership[zz][15] = "dieing";
                    } // end if
                    ++jjjj;
                } // end while
                if (Number(currentthread[4]) < 0 || Number(currentthread[4]) > 10000000)
                {
                    currentthread[4] = 0;
                } // end if
                extrafundsmultiplier = 10;
                fundsworth = Number(currentthread[4]) * extrafundsmultiplier;
                if (_root.playershipstatus[3][0] == currentthread[3])
                {
                    playerkilled = true;
                    _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + fundsworth;
                    _root.playershipstatus[5][8] = Number(_root.playershipstatus[5][8]) + Math.round(Number(currentthread[4]) * _root.playershipstatus[5][6]);
                    if (aiwaskilled != true)
                    {
                        _root.playershipstatus[5][9] = Number(_root.playershipstatus[5][9]) + Number(currentthread[4]);
                    } // end if
                    _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
                    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
                    if (_root.playertargetrange > Number(currentthread[4]) && currentplayerid != "999999" && _root.playershipstatus[3][0] != currentthread[1])
                    {
                        scoreloss = Math.round((Number(_root.playershipstatus[5][3]) - Number(currentthread[4])) * _root.scoreratiomodifier / 3);
                        bountytoadd = (Number(_root.playershipstatus[5][3]) - Number(currentthread[4])) * 3;
                        if (bountytoadd > 10000)
                        {
                            bountytoadd = 10000;
                        } // end if
                        if (bountytoadd > 0)
                        {
                            message = "NEWS`NBASH``" + _root.playershipstatus[3][0] + "`" + currentthread[1] + "`" + "0" + "`" + bountytoadd + "`" + "~";
                            _root.mysocket.send(message);
                        } // end if
                    } // end if
                }
                else
                {
                    playerkilled = false;
                } // end else if
                jj = 0;
                dataupdated = false;
                killerid = String(currentthread[3]);
                killerrealname = "AI";
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (killerid == String(_root.currentonlineplayers[jj][0]))
                    {
                        _root.currentonlineplayers[jj][3] = Number(_root.currentonlineplayers[jj][3]) + Math.round(Number(currentthread[4]) * _root.playershipstatus[5][6]);
                        if (aiwaskilled != true)
                        {
                            _root.currentonlineplayers[jj][5] = _root.currentonlineplayers[jj][5] + Number(currentthread[4]);
                        } // end if
                        killerrealname = _root.currentonlineplayers[jj][1];
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                jjjj = 0;
                dataupdated = false;
                while (jjjj < _root.otherplayership.length && dataupdated == false)
                {
                    if (killerid == String(_root.otherplayership[jjjj][0]))
                    {
                        _root.otherplayership[jjjj][12] = _root.currentonlineplayers[jj - 1][3];
                        dataupdated = true;
                        set("_root.gamedisplayarea.nametag" + _root.otherplayership[jjjj][0] + ".nametag", "" + _root.otherplayership[jjjj][10] + " (" + _root.otherplayership[jjjj][12] + ")");
                    } // end if
                    ++jjjj;
                } // end while
                jj = 0;
                dataupdated = false;
                killedid = String(currentthread[1]);
                deadguysname = "AI";
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (killedid == String(_root.currentonlineplayers[jj][0]))
                    {
                        _root.currentonlineplayers[jj][3] = 0;
                        deadguysname = _root.currentonlineplayers[jj][1];
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                message = "HOST: " + deadguysname + " (" + currentthread[4] + " BTY, " + fundsworth + " Funds) killed by " + killerrealname;
                _root.enterintochat(message, _root.regularchattextcolor);
                _root.refreshonlinelist = true;
                if (playerkilled == true)
                {
                    _root.func_playerkilled(deadguysname);
                } // end if
            } // end if
            if (currentthread[2] == "BC")
            {
                jj = 0;
                dataupdated = false;
                while (jj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (currentplayerid == _root.currentonlineplayers[jj][0])
                    {
                        _root.currentonlineplayers[jj][3] = Number(currentthread[3]);
                        dataupdated = true;
                    } // end if
                    ++jj;
                } // end while
                jjjj = 0;
                dataupdated = false;
                while (jjjj < _root.otherplayership.length && dataupdated == false)
                {
                    if (currentplayerid == _root.otherplayership[jjjj][0])
                    {
                        _root.otherplayership[jjjj][12] = Number(currentthread[3]);
                        set("_root.gamedisplayarea.nametag" + _root.otherplayership[jjjj][0] + ".nametag", "" + _root.otherplayership[jjjj][10] + " (" + _root.otherplayership[jjjj][12] + ")");
                        dataupdated = true;
                    } // end if
                    ++jjjj;
                } // end while
                if (_root.playershipstatus[3][0] == currentplayerid)
                {
                    _root.playershipstatus[5][8] = Number(currentthread[3]) - Number(_root.playershipstatus[5][3]);
                } // end if
            } // end if
            _root.refreshonlinelist = true;
            _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
            _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
            _root.toprightinformation.playerscore = "Score: " + Math.floor(Number(_root.playershipstatus[5][9]) / _root.scoreratiomodifier);
        }
        else if (currentthread[0] == "PING")
        {
            currentthread = newinfo[i].split("`");
            _root.gameplaystatus[1][0] = getTimer() - Number(currentthread[1]);
            _root.pinginformation = "Ping: " + _root.gameplaystatus[1][0] + "ms Current";
        }
        else if (currentthread[0] == "TI" && currentthread[1] != _root.playershipstatus[3][0])
        {
            currentthread = newinfo[i].split("`");
            idlocation = currentthread[1];
            realname = String(currentthread[2]);
            replacedaspot = false;
            for (qq = 0; qq < _root.currentonlineplayers.length; qq++)
            {
                if (String(_root.currentonlineplayers[qq][0]) == String(idlocation) || String(_root.currentonlineplayers[qq][1]) == String(realname))
                {
                    _root.currentonlineplayers.splice(qq, 1);
                    --qq;
                } // end if
            } // end of for
            lastspot = _root.currentonlineplayers.length;
            _root.currentonlineplayers[lastspot] = new Array();
            _root.currentonlineplayers[lastspot][0] = currentthread[1];
            _root.currentonlineplayers[lastspot][1] = currentthread[2];
            _root.currentonlineplayers[lastspot][2] = currentthread[3];
            _root.currentonlineplayers[lastspot][3] = currentthread[4];
            _root.currentonlineplayers[lastspot][4] = currentthread[5];
            _root.currentonlineplayers[lastspot][5] = Number(currentthread[6]);
            if (currentthread[2] == _root.playershipstatus[3][2] && _root.playershipstatus[3][2] == _root.playershipstatus[3][0])
            {
                _root.playershipstatus[3][0] = currentthread[1];
            } // end if
            _root.refreshonlinelist = true;
            if (_root.playershipstatus[3][0] != _root.playershipstatus[3][2])
            {
                _root.gamechatinfo[1].splice(0, 0, 0);
                _root.gamechatinfo[1][0] = new Array();
                _root.gamechatinfo[1][0][0] = "Host: " + currentthread[2] + " has entered";
                _root.gamechatinfo[1][0][1] = _root.regularchattextcolor;
                if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
                {
                    _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
                } // end if
                _root.refreshchatdisplay = true;
            } // end else if
        } // end else if
        if (currentthread[0] == "LI")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[3][0] = currentthread[1];
            _root.playershipstatus[5][19] = currentthread[2];
            if (_root.playershipstatus[5][25] == "NO" && _root.playerscurrentextrashipno == "capital")
            {
                for (qqt = 0; qqt < _root.maxextraships; qqt++)
                {
                    if (_root.extraplayerships[qqt][0] != null)
                    {
                        _root.playerscurrentextrashipno = qqt;
                        break;
                    } // end if
                } // end of for
                _root.changetonewship(_root.playerscurrentextrashipno);
                _root.initializemissilebanks();
                _root.savegamescript.saveplayersgame(this);
                _root.playershipstatus[5][25] = "CAPLEFT";
            } // end if
            _root.currentgamestatus = _root.currentgamestatus + "Synchronizing..";
            _root.attachMovie("gametimersync", "gametimersync", 540);
            continue;
        } // end if
        if (currentthread[0] == "CLOCK")
        {
            if (clocktimes.length <= clockchecks)
            {
                timedifference = Number(currentthread[1]);
                clocktimes[clocktimes.length] = timedifference;
                _root.currentgamestatus = _root.currentgamestatus + (_root.clockchecks - _root.clocktimes.length + ".");
                if (clocktimes.length == clockchecks)
                {
                    this.gametimersync.removeMovieClip();
                    _root.currentgamestatus = _root.currentgamestatus + "\r";
                    totaltime = 0;
                    for (q = 0; q < clocktimes.length; q++)
                    {
                        totaltime = totaltime + clocktimes[q];
                    } // end of for
                    _root.clocktimediff = Math.round(totaltime / clocktimes.length);
                    if (_root.jumpinginfo.length > 1)
                    {
                        message = "Jumping System Complete, Now in system " + _root.jumpinginfo[0];
                        _root.enterintochat(message, _root.systemchattextcolor);
                        _root.jumpinginfo.length = null;
                    } // end if
                    if (_root.teamdeathmatch == true)
                    {
                        _root.gotoAndStop("teambase");
                    }
                    else if (_root.isgameracingzone == true)
                    {
                        _root.gotoAndStop("racingzonescreen");
                    }
                    else if (_root.playershipstatus[4][0].substr(0, 2) == "SB")
                    {
                        _root.gotoAndStop("stardock");
                    }
                    else if (_root.playershipstatus[4][0].substr(0, 2) == "PL")
                    {
                        _root.gotoAndStop("planet");
                    } // end else if
                } // end else if
            } // end else if
            continue;
        } // end if
        if (currentthread[0] == "NEWS")
        {
            if (currentthread[1] == "BOUNTY")
            {
                playerwhosgettingbty = Number(currentthread[2]);
                amountofbty = Number(currentthread[3]);
                playerwhoboughtbty = currentthread[4];
                gettingtruename = currentthread[5];
                if (isNaN(amountofbty))
                {
                    amountofbty = 0;
                } // end if
                if (_root.playershipstatus[3][0] == playerwhosgettingbty)
                {
                    _root.playershipstatus[5][8] = _root.playershipstatus[5][8] + amountofbty;
                    if (_root.isplayeraguest == false)
                    {
                        newaccountVars = new XML();
                        newaccountVars.load(_root.pathtoaccounts + "accounts.php?name=" + _root.playershipstatus[3][2] + "&pass=" + _root.playershipstatus[3][3] + "&bty=" + _root.playershipstatus[5][8]);
                        newaccountVars.onLoad = function (success)
                        {
                        };
                    } // end if
                    datatosend = "TC`" + _root.playershipstatus[3][0] + "`BC`" + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8])) + "~";
                    _root.mysocket.send(datatosend);
                } // end if
                message = "Player " + playerwhoboughtbty + " has placed an additional bounty of " + amountofbty + " on player " + gettingtruename;
                _root.func_messangercom(message, "NEWS", "BEGIN");
            } // end if
            if (currentthread[1] == "MES")
            {
                message = currentthread[2];
                message = currentthread[2];
                _root.func_messangercom(message, "NEWS", "BEGIN");
            } // end if
            if (currentthread[1] == "SB")
            {
                if (currentthread[2] == "DESTROYED")
                {
                    baseidname = currentthread[3];
                    for (jj = 0; jj < _root.playersquadbases.length; jj++)
                    {
                        if (_root.playersquadbases[jj][0] == baseidname)
                        {
                            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]].basedestroyed = true;
                            _root.playersquadbases.splice(jj, 1);
                        } // end if
                    } // end of for
                } // end if
                if (currentthread[2] == "CREATED")
                {
                    if (_root.playersquadbases.length < 1)
                    {
                        _root.playersquadbases = new Array();
                    } // end if
                    currentsquadbase = _root.playersquadbases.length;
                    _root.playersquadbases[currentsquadbase] = new Array();
                    _root.playersquadbases[currentsquadbase][0] = currentthread[3];
                    _root.playersquadbases[currentsquadbase][1] = Number(currentthread[4]);
                    _root.playersquadbases[currentsquadbase][2] = Number(currentthread[5]);
                    _root.playersquadbases[currentsquadbase][3] = Number(currentthread[6]);
                    _root.playersquadbases[currentsquadbase][4] = Math.ceil(Number(currentthread[5]) / _root.sectorinformation[1][0]);
                    _root.playersquadbases[currentsquadbase][5] = Math.ceil(Number(currentthread[6]) / _root.sectorinformation[1][1]);
                    if (_root.playersquadbases[currentsquadbase][0].substr(0, 3) == "*AI")
                    {
                        message = "An AI Base has been detected at X:" + _root.playersquadbases[currentsquadbase][4] + " Y:" + _root.playersquadbases[currentsquadbase][5];
                        message = message + "\rA Bounty of 65,000 has been placed on its elimination";
                        ainame = "NEWS";
                        _root.func_messangercom(message, "NEWS", "BEGIN");
                    } // end if
                    _root.testtt = _root.playersquadbases.length;
                } // end if
            } // end if
            if (currentthread[1] == "SQUADBD")
            {
                killersid = Number(currentthread[3]);
                bountytoadd = Number(currentthread[4]);
                baseidname = String(currentthread[2]);
                basedestroyed(killersid, bountytoadd, baseidname);
            } // end if
            if (currentthread[1] == "SQUADUPG")
            {
                baseid = String(currentthread[2]);
                mode = String(currentthread[3]);
                if (mode == "baselvl")
                {
                    newbaselevel = Number(currentthread[4]);
                    for (jj = 0; jj < _root.playersquadbases.length; jj++)
                    {
                        if (baseid == _root.playersquadbases[jj][0])
                        {
                            _root.playersquadbases[jj][1] = newbaselevel;
                            break;
                        } // end if
                    } // end of for
                    _root.gamedisplayarea.keyboardscript.func_redrawsquadbase(baseid, "LEVEL");
                } // end if
            } // end if
            if (currentthread[1] == "TRANSFER")
            {
                fundstoadd = Number(currentthread[3]);
                donater = Number(currentthread[4]);
                donatersname = null;
                for (jjjj = 0; jjjj < _root.currentonlineplayers.length; jjjj++)
                {
                    if (donater == _root.currentonlineplayers[jjjj][0])
                    {
                        donatersname = _root.currentonlineplayers[jjjj][1];
                    } // end if
                } // end of for
                if (donatersname != null)
                {
                    if (fundstoadd > 5000000)
                    {
                        fundstoadd = 5000000;
                    } // end if
                    if (fundstoadd < 0)
                    {
                        fundstoadd = 0;
                    } // end if
                    if (!isNaN(fundstoadd))
                    {
                        _root.playershipstatus[3][1] = Number(fundstoadd) + Number(_root.playershipstatus[3][1]);
                        _root.func_setoldfundsuptocurrentammount();
                        _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
                        message = "You Just Received " + fundstoadd + " funds from Player " + donatersname;
                        ainame = "NEWS";
                        _root.func_messangercom(message, "NEWS", "BEGIN");
                    } // end if
                } // end if
            } // end if
            if (currentthread[1] == "NBASH")
            {
                attacker = currentthread[3];
                bashed = currentthread[4];
                scoreloss = currentthread[5];
                btyincrease = currentthread[6];
                location = currentthread[7];
                for (jj = 0; jj < _root.currentonlineplayers.length; jj++)
                {
                    if (attacker == _root.currentonlineplayers[jj][0])
                    {
                        attackersname = _root.currentonlineplayers[jj][1];
                        _root.currentonlineplayers[jj][3] = _root.currentonlineplayers[jj][3] + Number(btyincrease);
                        _root.currentonlineplayers[jj][5] = _root.currentonlineplayers[jj][5] - Number(scoreloss);
                        dataupdated = true;
                    } // end if
                    if (bashed == _root.currentonlineplayers[jj][0])
                    {
                        bashedname = _root.currentonlineplayers[jj][1];
                    } // end if
                } // end of for
                message = attackersname + " has just bashed " + bashedname + " at Coordinates " + location + "\r" + attackersname + " has lost " + Math.round(scoreloss / _root.scoreratiomodifier) + " points and has gained a bounty increase of " + btyincrease;
                _root.func_messangercom(message, "NEWS", "BEGIN");
                if (_root.playershipstatus[3][0] == currentthread[3])
                {
                    _root.playershipstatus[5][8] = Number(_root.playershipstatus[5][8]) + Number(btyincrease);
                    _root.playershipstatus[5][9] = Number(_root.playershipstatus[5][9]) - Number(scoreloss);
                    _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
                    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
                } // end if
                _root.refreshonlinelist = true;
            } // end if
            if (currentthread[1] == "PC")
            {
                _root.randomegameevents.func_pricechangeeffect(currentthread[2], currentthread[3], currentthread[4], currentthread[5], currentthread[6]);
            } // end if
            continue;
        } // end if
        if (currentthread[0] == "KFLAG")
        {
            _root.func_kingofflagcommand(currentthread[1], currentthread[2], currentthread[3], currentthread[4]);
            continue;
        } // end if
        if (currentthread[0] == "ADMIN")
        {
            if (currentthread[1] == "MUTE")
            {
                if (_root.gamechatinfo[2][2] == true)
                {
                    _root.gamechatinfo[2][2] = false;
                    colourtouse = _root.systemchattextcolor;
                    message = "HOST: You Are No Longer Muted";
                    _root.enterintochat(message, colourtouse);
                }
                else
                {
                    _root.gamechatinfo[2][2] = true;
                    _root.gamechatinfo[2][3] = getTimer() + _root.gamechatinfo[2][4];
                    colourtouse = _root.systemchattextcolor;
                    message = "HOST: Muted for " + Math.ceil(_root.gamechatinfo[2][4] / 60000) + " minuites by " + currentthread[2];
                    _root.enterintochat(message, colourtouse);
                } // end if
            } // end else if
            if (currentthread[1] == "TRANSFER")
            {
                fundstoadd = Number(currentthread[3]);
                donater = Number(currentthread[4]);
                donatersname = "*AN OP*";
                if (!isNaN(fundstoadd))
                {
                    _root.playershipstatus[3][1] = Number(fundstoadd) + Number(_root.playershipstatus[3][1]);
                    _root.func_setoldfundsuptocurrentammount();
                    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
                    message = "You Just Received " + fundstoadd + " funds from Player " + donatersname;
                    ainame = "NEWS";
                    _root.func_messangercom(message, "NEWS", "BEGIN");
                    _root.savegamescript.saveplayersgame(this);
                } // end if
            } // end if
            continue;
        } // end if
        if (currentthread[0] == "ERROR")
        {
            if (currentthread[1] == "TOOMANYUSERS")
            {
                _root.gameerror = "toomanyplayers";
                _root.controlledserverclose = true;
                _root.gotoAndStop("gameclose");
            } // end if
            if (currentthread[1] == "KICKED")
            {
                _root.controlledserverclose = true;
                _root.gameerror = "kicked";
                _root.gameerrordoneby = currentthread[2];
                _root.func_closegameserver();
            } // end if
            if (currentthread[1] == "SHUTDOWN")
            {
                _root.controlledserverclose = true;
                _root.gameerror = "servershutdown";
                _root.func_closegameserver();
            } // end if
            if (currentthread[1] == "NAMEINUSE")
            {
                _root.controlledserverclose = true;
                _root.gameerror = "nameinuse";
                _root.func_closegameserver();
            } // end if
            if (currentthread[1] == "BANNED")
            {
                _root.controlledserverclose = true;
                _root.gameerror = "banned";
                _root.timebannedfor = currentthread[2];
                _root.gameerrordoneby = currentthread[3];
                _root.func_closegameserver();
            } // end if
            continue;
        } // end if
        if (currentthread[0] == "MET")
        {
            if (currentthread[1] == "CR")
            {
                meteorid = currentthread[2];
                meteorheading = currentthread[3];
                targetname = currentthread[4];
                meteortype = currentthread[5];
                meterorlife = currentthread[6];
                timeelapsed = currentthread[7];
                _root.func_meteoricoming(meteorid, meteorheading, targetname, meteortype, meterorlife, timeelapsed);
            }
            else if (currentthread[1] == "LF")
            {
                meteorid = currentthread[2];
                meteorlife = currentthread[3];
                _root.func_meteorlife(meteorid, meteorlife);
            } // end else if
            if (currentthread[1] == "DEST")
            {
                meteorid = currentthread[2];
                killer = currentthread[3];
                _root.func_meteordestroyer(meteorid, killer);
            } // end if
        } // end if
    } // end of for
} // End of the function
function movementofanobjectwiththrust()
{
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    } // end if
    if (velocity != 0)
    {
        ymovement = -velocity * Math.cos(0.017453 * this.relativefacing);
        xmovement = velocity * Math.sin(0.017453 * this.relativefacing);
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function func_messangercom(message, nameofsender, transmissiontype)
{
    message = _root.func_checkothercharacters(message);
    _root.aimessagesbox.gotoAndPlay(2);
    _root.aimessagesbox.aimessage = message;
    _root.aimessagesbox.ainame = nameofsender;
} // End of the function
function basedestroyed(killersid, bountytoadd, baseidname)
{
    _root.gamedisplayarea.keyboardscript.func_setsectortargetitem("REMOVE", baseidname);
    for (jj = 0; jj < _root.playersquadbases.length; jj++)
    {
        if (_root.playersquadbases[jj][0] == baseidname)
        {
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]].basedestroyed = true;
            _root.playersquadbases.splice(jj, 1);
        } // end if
    } // end of for
    if (Number(currentthread[4]) < 0)
    {
        currentthread[4] = 0;
    } // end if
    if (_root.playershipstatus[3][0] == killersid)
    {
        _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Number(bountytoadd);
        _root.playershipstatus[5][8] = Number(_root.playershipstatus[5][8]) + Math.round(Number(bountytoadd) * _root.playershipstatus[5][6]);
        _root.toprightinformation.playerbounty = "Bounty: " + (Number(_root.playershipstatus[5][3]) + Number(_root.playershipstatus[5][8]));
        _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
    } // end if
    jj = 0;
    dataupdated = false;
    killerid = String(killersid);
    while (jj < _root.currentonlineplayers.length && dataupdated == false)
    {
        if (killerid == String(_root.currentonlineplayers[jj][0]))
        {
            _root.currentonlineplayers[jj][3] = Number(_root.currentonlineplayers[jj][3]) + Math.round(Number(bountytoadd) * _root.playershipstatus[5][6]);
            killerrealname = _root.currentonlineplayers[jj][1];
            dataupdated = true;
        } // end if
        ++jj;
    } // end while
    jjjj = 0;
    dataupdated = false;
    while (jjjj < _root.otherplayership.length && dataupdated == false)
    {
        if (killerid == String(_root.otherplayership[jjjj][0]))
        {
            _root.otherplayership[jjjj][12] = _root.currentonlineplayers[jj - 1][3];
            dataupdated = true;
            set("_root.gamedisplayarea.nametag" + _root.otherplayership[jjjj][0] + ".nametag", "" + _root.otherplayership[jjjj][10] + " (" + _root.otherplayership[jjjj][12] + ")");
        } // end if
        ++jjjj;
    } // end while
    _root.refreshonlinelist = true;
    message = "Player " + killerrealname + " Just Killed Squad Base " + baseidname + ".";
    _root.gamechatinfo[1].splice(0, 0, 0);
    _root.gamechatinfo[1][0] = new Array();
    _root.gamechatinfo[1][0][0] = "HOST: " + message;
    _root.gamechatinfo[1][0][1] = _root.systemchattextcolor;
    if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
    {
        _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
    } // end if
    _root.refreshchatdisplay = true;
} // End of the function
function bringinspecial(playerid, specialno, specialx, specialy, velocity, rotation)
{
    typeofspecial = _root.specialshipitems[specialno][5];
    if (typeofspecial == "FLARE")
    {
        i = _root.currentplayershotsfired;
        ++_root.currentplayershotsfired;
        if (_root.currentplayershotsfired > 999)
        {
            _root.currentplayershotsfired = 0;
        } // end if
        _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialno][8], "playerflare" + i, 5000 + i);
        setProperty("_root.gamedisplayarea.playerflare" + i, _x, specialx);
        setProperty("_root.gamedisplayarea.playerflare" + i, _y, specialy);
        set("_root.gamedisplayarea.playerflare" + i + ".xposition", specialx);
        set("_root.gamedisplayarea.playerflare" + i + ".yposition", specialy);
        set("_root.gamedisplayarea.playerflare" + i + ".removetime", getTimer() + _root.specialshipitems[specialno][3]);
    }
    else if (typeofspecial == "PULSAR")
    {
        i = _root.currentplayershotsfired;
        ++_root.currentplayershotsfired;
        if (_root.currentplayershotsfired > 999)
        {
            _root.currentplayershotsfired = 0;
        } // end if
        _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialno][8], "selfdest" + i, 5000 + i);
        setProperty("_root.gamedisplayarea.selfdest" + i, _x, specialx);
        setProperty("_root.gamedisplayarea.selfdest" + i, _y, specialy);
        set("_root.gamedisplayarea.selfdest" + i + ".xposition", specialx);
        set("_root.gamedisplayarea.selfdest" + i + ".yposition", specialy);
        set("_root.gamedisplayarea.selfdest" + i + ".totaltime", _root.specialshipitems[specialno][3]);
        set("_root.gamedisplayarea.selfdest" + i + ".finishsize", _root.specialshipitems[specialno][12]);
        set("_root.gamedisplayarea.selfdest" + i + ".damage", _root.specialshipitems[specialno][11]);
        set("_root.gamedisplayarea.selfdest" + i + ".playerid", playerid);
    }
    else if (typeofspecial == "RECHARGESHIELD")
    {
        pilotno = playerid;
        shieldcharge = specialx;
        currentthread = new Array(null, "RECHARGESHIELD", pilotno, shieldcharge);
        func_statuscheck(currentthread);
    }
    else if (typeofspecial == "RECHARGESTRUCT")
    {
        pilotno = playerid;
        structcharge = specialx;
        currentthread = new Array(null, "RECHARGESTRUCT", pilotno, structcharge);
        func_statuscheck(currentthread);
    }
    else if (typeofspecial == "MINES")
    {
        mineid = playerid;
        minesplitinfo = playerid.split("a");
        playerid = Number(minesplitinfo[0]);
        i = _root.currentplayershotsfired;
        ++_root.currentplayershotsfired;
        if (_root.currentplayershotsfired > 999)
        {
            _root.currentplayershotsfired = 0;
        } // end if
        _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialno][8], "playermine" + mineid, 5000 + i);
        setProperty("_root.gamedisplayarea.playermine" + mineid, _x, specialx);
        setProperty("_root.gamedisplayarea.playermine" + mineid, _y, specialy);
        set("_root.gamedisplayarea.playermine" + mineid + ".mineid", mineid);
        set("_root.gamedisplayarea.playermine" + mineid + ".xposition", specialx);
        set("_root.gamedisplayarea.playermine" + mineid + ".yposition", specialy);
        set("_root.gamedisplayarea.playermine" + mineid + ".removetime", getTimer() + _root.specialshipitems[specialno][3]);
        set("_root.gamedisplayarea.playermine" + mineid + ".damage", _root.specialshipitems[specialno][12]);
        set("_root.gamedisplayarea.playermine" + mineid + ".playerfrom", playerid);
        ishostilemine = func_isotherplayeroneyourteam(playerid);
        set("_root.gamedisplayarea.playermine" + mineid + ".ishostilemine", ishostilemine);
    } // end else if
} // End of the function
function func_isotherplayeroneyourteam(otherplayerid)
{
    yteam = _root.playershipstatus[5][2];
    if (yteam == "N/A")
    {
        return (true);
    }
    else
    {
        totalplayers = _root.currentonlineplayers.length;
        for (q = 0; q < totalplayers; q++)
        {
            if (_root.currentonlineplayers[q][0] == otherplayerid)
            {
                opponenteam = _root.currentonlineplayers[q][4];
                break;
            } // end if
        } // end of for
        if (opponenteam != yteam)
        {
            return (true);
        }
        else
        {
            return (false);
        } // end else if
    } // end else if
} // End of the function
function func_statuscheck(currentthread)
{
    if (currentthread[1] == "GET")
    {
        pilotno = currentthread[2];
        datatosend = "SP`" + pilotno + "~STATS`INFO`" + _root.playershipstatus[3][0] + "`" + _root.playershipstatus[2][0] + "`" + Math.round(_root.playershipstatus[2][1]) + "`" + Math.round(_root.playershipstatus[2][5]) + "~";
        _root.mysocket.send(datatosend);
    }
    else if (currentthread[1] == "INFO")
    {
        currentpilot = Number(currentthread[2]);
        for (jj = 0; jj < _root.otherplayership.length; jj++)
        {
            if (_root.otherplayership[jj][0] == currentpilot)
            {
                _root.otherplayership[jj][40] = Number(currentthread[5]);
                _root.otherplayership[jj][41] = Number(currentthread[4]);
                _root.otherplayership[jj][46] = Number(currentthread[3]);
                _root.otherplayership[jj][42] = _root.shieldgenerators[_root.otherplayership[jj][46]][1];
                _root.otherplayership[jj][43] = _root.shieldgenerators[_root.otherplayership[jj][46]][0];
                _root.otherplayership[jj][44] = getTimer();
            } // end if
        } // end of for
    }
    else if (currentthread[1] == "RECHARGESHIELD")
    {
        currentpilot = Number(currentthread[2]);
        for (jj = 0; jj < _root.otherplayership.length; jj++)
        {
            if (_root.otherplayership[jj][0] == currentpilot)
            {
                _root.otherplayership[jj][41] = _root.otherplayership[jj][41] + Number(currentthread[3]);
            } // end if
        } // end of for
    }
    else if (currentthread[1] == "RECHARGESTRUCT")
    {
        _root.testtt = currentpilot + "`" + currentthread[4];
        currentpilot = Number(currentthread[2]);
        for (jj = 0; jj < _root.otherplayership.length; jj++)
        {
            if (_root.otherplayership[jj][0] == currentpilot)
            {
                _root.otherplayership[jj][40] = _root.otherplayership[jj][40] + Number(currentthread[3]);
            } // end if
        } // end of for
    } // end else if
} // End of the function
framestobuffer = 4;
_root.othershipbuffer = framestobuffer;
shiptimeoutime = 3500;
timeintervalcheck = getTimer();
pathtosectorfile = "./systems/system" + _root.playershipstatus[5][1] + "/";
_root.otherplayership = new Array();
_root.currentotherplayshot = 0;
currentothermissileshot = 0;
justenteredgame = true;
clocktimes = new Array();
_root.clocktimediff = 0;
clockchecks = 10;
if (_root.isgamerunningfromremote == true)
{
    clockchecks = 2;
} // end if
this.gameareawidth = _root.gameareawidth;
this.gameareaheight = _root.gameareaheight;
lowestvolume = 50;
lowestvolumelength = Math.sqrt(gameareawidth / 2 * (gameareawidth / 2) + gameareaheight / 2 * (gameareaheight / 2));
_root.mysocket = new XMLSocket();
_root.currentgamestatus = _root.currentgamestatus + "Connecting to Game Server \r";
_root.currentgamestatus = _root.currentgamestatus + "Logging In \r";
if (_root.playershipstatus[3][2] == "Enter Name")
{
    _root.gotoAndStop("loginscreen");
}
else if (_root.isgamerunningfromremote == false)
{
    currenturl = String(_root._url);
    if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
    {
        portouse = 2 + String(_root.portandsystem);
        _root.mysocket.connect("", portouse);
    }
    else if (currenturl.substr(0, 32) == "http://www.stellar-conflicts.com")
    {
        portouse = 2 + String(_root.portandsystem);
        _root.mysocket.connect("", portouse);
    } // end else if
}
else
{
    _root.mysocket.connect("217.160.243.182", 2500);
} // end else if
_root.mysocket.onConnect = function (success)
{
    if (success)
    {
        _root.playerjustloggedin = false;
        login();
    }
    else
    {
        _root.status = "Failed";
        _root.gameerror = "failedtologin";
        _root.gotoAndPlay("gameclose");
    } // end else if
};
_root.mysocket.onClose = function (success)
{
    _root.errorcheckdata = new Array();
    if (_root.jumpinginfo.length > 1)
    {
    }
    else if (_root.controlledserverclose == true)
    {
        _root.gotoAndStop("gameclose");
    }
    else
    {
        _root.currentgamestatus = "Connection Broken\rAttempting to Reconnect\r\r";
        _root.playershipstatus[3][0] = null;
        _root.reconnectingfromgame = true;
        _root.gotoAndStop("beforeserverconnect");
    } // end else if
};
_root.mysocket.onXML = xmlhandler;

function otherpilotdfgdf(currentthread)
{
    currentothership = _root.otherplayership.length;
    currentname = currentthread[1];
    qz = 0;
    doesplayerexist = false;
    while (qz < _root.otherplayership.length && doesplayerexist == false)
    {
        if (currentname == _root.otherplayership[qz][0])
        {
            doesplayerexist = true;
            currentothership = qz;
            _root.otherplayership[currentothership][0] = currentthread[1];
            allowance = int(currentthread[5]) / 7;
            recievedxpos = int(currentthread[2]);
            _root.otherplayership[currentothership][30] = recievedxpos;
            recievedypos = int(currentthread[3]);
            _root.otherplayership[currentothership][31] = recievedypos;
            _root.otherplayership[currentothership][32] = int(currentthread[4]);
            _root.otherplayership[currentothership][25] = Math.floor((_root.otherplayership[currentothership][21] - _root.otherplayership[currentothership][1]) / framestobuffer);
            _root.otherplayership[currentothership][26] = Math.floor((_root.otherplayership[currentothership][22] - _root.otherplayership[currentothership][2]) / framestobuffer);
            _root.otherplayership[currentothership][27] = _root.otherplayership[currentothership][23] - _root.otherplayership[currentothership][3];
            if (_root.otherplayership[currentothership][27] > 180)
            {
                _root.otherplayership[currentothership][27] = (_root.otherplayership[currentothership][27] - 360) * -1;
            }
            else if (_root.otherplayership[currentothership][27] < -180)
            {
                _root.otherplayership[currentothership][27] = (_root.otherplayership[currentothership][27] + 360) * -1;
            } // end else if
            _root.otherplayership[currentothership][27] = Math.floor(_root.otherplayership[currentothership][27] / framestobuffer);
            _root.otherplayership[currentothership][33] = Number(currentthread[5]);
            _root.otherplayership[currentothership][36] = new Array();
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][36][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][36][0] = _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][36][0] = _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][36][1] = _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][36][1] = _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            if (_root.otherplayership[currentothership][16] != currentthread[7])
            {
                newaction = String(currentthread[7]);
                if (newaction == "C" && _root.otherplayership[currentothership][16] != "D")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, false);
                    _root.gamedisplayarea["otherplayership" + _root.otherplayership[currentothership][0]].attachMovie("shipactivateothercloak", "shipactivateothercloak", 69);
                    _root.otherplayership[currentothership][16] = newaction;
                }
                else if (newaction == "S" && _root.otherplayership[currentothership][16] != "D")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, true);
                    _root.otherplayership[currentothership][16] = newaction;
                }
                else if (newaction != "S" && newaction != "C")
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, true);
                    if (root.otherplayership[currentothership][16] == "C")
                    {
                        _root.gamedisplayarea["otherplayership" + _root.otherplayership[currentothership][0]].attachMovie("shipactivateotherdecloak", "shipactivateotherdecloak", 69);
                        
                    } // end if
                    _root.otherplayership[currentothership][16] = newaction;
                } // end else if
            } // end else if
            systemtime = String(getTimer() + _root.clocktimediff);
            systemtime = Number(systemtime.substr(systemtime.length - 4));
            jumpahead = systemtime - Number(currentthread[8]);
            if (jumpahead < -1000)
            {
                jumpahead = jumpahead + 10000;
            } // end if
            jumptime = jumpahead + getTimer();
            _root.otherplayership[currentothership][39] = Number(currentthread[8]);
            _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        } // end if
        ++qz;
    } // end while
    if (doesplayerexist == false)
    {
        currentothership = qz;
        _root.otherplayership[currentothership] = new Array();
        _root.otherplayership[currentothership][0] = currentthread[1];
        _root.otherplayership[currentothership][1] = int(currentthread[2]);
        _root.otherplayership[currentothership][2] = int(currentthread[3]);
        _root.otherplayership[currentothership][3] = int(currentthread[4]);
        _root.otherplayership[currentothership][4] = int(currentthread[5]);
        _root.otherplayership[currentothership][5] = new Array();
        _root.otherplayership[currentothership][6] = _root.curenttime + shiptimeoutime;
        _root.otherplayership[currentothership][7] = _root.curenttime + shiptimeoutime * 2;
        jjjj = 0;
        dataupdated = false;
        while (jjjj < _root.currentonlineplayers.length && dataupdated == false)
        {
            if (_root.otherplayership[currentothership][0] == _root.currentonlineplayers[jjjj][0])
            {
                dataupdated = true;
                _root.otherplayership[currentothership][10] = _root.currentonlineplayers[jjjj][1];
                _root.otherplayership[currentothership][11] = _root.currentonlineplayers[jjjj][2];
                _root.otherplayership[currentothership][12] = _root.currentonlineplayers[jjjj][3];
                _root.otherplayership[currentothership][13] = _root.currentonlineplayers[jjjj][4];
            } // end if
            ++jjjj;
        } // end while
        if (dataupdated == false)
        {
            _root.otherplayership.splice(currentothership, 1);
        }
        else
        {
            _root.otherplayership[currentothership][15] = "alive";
            for (jjj = 0; jjj < currentthread[6].length; jjj++)
            {
                if (currentthread[6].charAt(jjj) == "S")
                {
                    _root.otherplayership[currentothership][5][0] = "S";
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "U")
                {
                    _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] + _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "D")
                {
                    _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][5][0] - _root.shiptype[_root.otherplayership[currentothership][11]][3][0];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "L")
                {
                    _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] - _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                    continue;
                } // end if
                if (currentthread[6].charAt(jjj) == "R")
                {
                    _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][5][1] + _root.shiptype[_root.otherplayership[currentothership][11]][3][2];
                } // end if
            } // end of for
            _root.otherplayership[currentothership][16] = currentthread[7];
            _root.gamedisplayarea.attachMovie("shiptype" + _root.otherplayership[currentothership][11], "otherplayership" + _root.otherplayership[currentothership][0], 2300 + Number(_root.otherplayership[currentothership][0]));
            if (_root.otherplayership[currentothership][16] == "C")
            {
                setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _visible, false);
            } // end if
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _x, _root.otherplayership[currentothership][1] - _root.shipcoordinatex);
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _y, _root.otherplayership[currentothership][2] - _root.shipcoordinatey);
            setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _rotation, _root.otherplayership[currentothership][2]);
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".shiptype", Number(_root.otherplayership[currentothership][11]));
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".gothit", false);
            set("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0] + ".isplayervisible", true);
        } // end if
    } // end else if
} // End of the function

stop ();

stop ();

// [Action in Frame 10]
_root.maingameplayingframe = _root._currentframe;

stop ();

// [Action in Frame 11]
stop ();

// [Action in Frame 12]
for (jj = 0; jj < _root.playersquadbases.length; jj++)
{
    delete _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]];
} // end of for

stop ();

// [Action in Frame 13]
stop ();

stop ();

// [Action in Frame 14]
_root.sendnewdmgame();

// [Action in Frame 15]
stop ();

// [Action in Frame 16]
stop ();

// [Action in Frame 17]
savestatus = "";
removeMovieClip ("_root.toprightinformation");
removeMovieClip ("_root.onlineplayerslist");
removeMovieClip ("_root.chatdialogue");
if (_root.gameerror == "failedtologin")
{
    _root.message = " Failed to Login \rProbable Reasons For This - \r1. Server is down, or could not connect to server \r2. You are not connected to the internet or information cannot  \r\tPass through a firewall";
    if (_root.reconnectingfromgame == true)
    {
        _root.savegamescript.saveplayersgame(this);
    } // end if
}
else if (_root.gameerror == "hostclosedconnection")
{
    _root.savegamescript.saveplayersgame(this);
    _root.message = " Connection Has Closed \rYou Have lost the connection to the server \rProbable Reasons For This - \r1. Server shut down \r2. Your connection to the internet was broken \r";
}
else if (_root.gameerror == "toomanyplayers")
{
    _root.message = " Too Many Players \rProbable Reasons For This - \r1. There are too many players, Try again later \r";
}
else if (_root.gameerror == "servershutdown")
{
    _root.savegamescript.saveplayersgame(this);
    _root.message = " The server was shut down \rProbable Reasons For This - \r1. An Admin needed to restart the server. \rIt will take a few minuites for the server to reload \rYou need to close this window and try reloading form the Homepage.";
}
else if (_root.gameerror == "nameinuse")
{
    _root.message = " Your Name is Already in Use \rProbable Reasons For This - \r1. Your name is already in use in this zone. \rIt will take a few minuites for you to time out if disconnected  \r";
}
else if (_root.gameerror == "kicked")
{
    _root.savegamescript.saveplayersgame(this);
    _root.message = " You were booted by " + _root.gameerrordoneby + "! \r" + "Probable Reasons For This - \r" + "1. You Have been a very bad person \r" + "2. Game Error \r";
}
else if (_root.gameerror == "banned")
{
    bannedtime = Number(_root.timebannedfor);
    diff = bannedtime * 60;
    daysDiff = Math.floor(diff / 60 / 60 / 24);
    diff = diff - daysDiff * 60 * 60 * 24;
    hrsDiff = Math.floor(diff / 60 / 60);
    diff = diff - hrsDiff * 60 * 60;
    minsDiff = Math.floor(diff / 60);
    diff = diff - minsDiff * 60;
    secsDiff = diff;
    bannedtime = daysDiff + " Days, " + hrsDiff + " Hours, " + minsDiff + " Minutes";
    _root.message = " Your Have Been Banned for " + bannedtime + " by " + _root.gameerrordoneby + "\r" + "Probable Reasons For This - \r" + "1. You are probably not welcomed here. \r" + "2. This Point of Access is banned. \r" + "3. In case of an error you can post in the forum. \r";
}
else if (_root.gameerror == "fpsstopped")
{
    _root.savegamescript.saveplayersgame(this);
    _root.message = " Your Frame Rate is Too Slow! \rProbable Reasons For This - \r1. You computer was running to slow \r2. You tried to cheat \r";
}
else if (_root.gameerror == "savefailure")
{
    _root.message = " Your Game Failed to Save! \rProbable Reasons For This - \r1. Your Save Transmission Failed \r2. You are Playing With This Account in Another Zone at the Same Time \r\rIf this continues you may end up being banned, /r and you will have to contact the webmaster to become unbanned.";
}
else if (_root.gameerror == "FLOODING")
{
    _root.message = " Your Must Stop Flooding \rProbable Reasons For This - \r1. Your Are Flooding the Chat \r\rIf this continues you may end up being reported and being banned.";
}
else
{
    _root.message = " Error Occured, If you connected to the server and immediately went to this screen after the countdown report BUG#111 to Kyled on the forums.\r If you just finished loggin into your account and did not conect to the server report BUG#222 to Kyled on the forums.";
} // end else if
_root.createEmptyMovieClip("blanker", 9999999);
_root.createEmptyMovieClip("blanker", 10);
stop ();
