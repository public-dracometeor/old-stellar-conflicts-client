﻿// Action script...

// [onClipEvent of sprite 890 in frame 1]
onClipEvent (load)
{
    function drawgrid()
    {
        for (x = 0; x <= noofxsectors; x++)
        {
            ++currentdepth;
            xstart = xsecwidth * x + gridxindent + xsecwidth / 2;
            ystart = gridyindent - 10;
            this.attachMovie("ingamemapgridlabel", "ingamemapgridlabel" + currentdepth, currentdepth);
            setProperty("ingamemapgridlabel" + currentdepth, _x, xstart);
            setProperty("ingamemapgridlabel" + currentdepth, _y, ystart);
            set("ingamemapgridlabel" + currentdepth + ".label", x);
        } // end of for
        for (y = 0; y <= noofxsectors; y++)
        {
            ++currentdepth;
            ystart = ysecwidth * y + gridyindent + ysecwidth / 2;
            xstart = gridxindent - 20;
            this.attachMovie("ingamemapgridlabel", "ingamemapgridlabel" + currentdepth, currentdepth);
            setProperty("ingamemapgridlabel" + currentdepth, _x, xstart);
            setProperty("ingamemapgridlabel" + currentdepth, _y, ystart);
            set("ingamemapgridlabel" + currentdepth + ".label", y);
        } // end of for
        for (x = 0; x <= noofxsectors; x++)
        {
            xstart = xsecwidth * x + gridxindent;
            for (y = 0; y <= noofysectors; y++)
            {
                ++currentdepth;
                ystart = ysecwidth * y + gridyindent;
                this.attachMovie("ingamemapsecsquare", "ingamemapsecsquare" + currentdepth, currentdepth);
                setProperty("ingamemapsecsquare" + currentdepth, _x, xstart);
                setProperty("ingamemapsecsquare" + currentdepth, _y, ystart);
                setProperty("ingamemapsecsquare" + currentdepth, _width, xsecwidth);
                setProperty("ingamemapsecsquare" + currentdepth, _height, ysecwidth);
            } // end of for
        } // end of for
    } // End of the function
    function setupthewaypoints()
    {
        for (i = 0; i < _root.playersquadbases.length; i++)
        {
            ++currentdepth;
            this.attachMovie("ingamemapsquadbase", "ingamemapsquadbase" + currentdepth, currentdepth);
            setProperty("ingamemapsquadbase" + currentdepth, _x, _root.playersquadbases[i][2] * xratio + gridxindent + xsecwidth);
            setProperty("ingamemapsquadbase" + currentdepth, _y, _root.playersquadbases[i][3] * yratio + gridyindent + ysecwidth);
            set("ingamemapsquadbase" + currentdepth + ".label", _root.playersquadbases[i][0]);
            set("ingamemapsquadbase" + currentdepth + ".xlocation", _root.playersquadbases[i][2]);
            set("ingamemapsquadbase" + currentdepth + ".ylocation", _root.playersquadbases[i][3]);
            set("ingamemapsquadbase" + currentdepth + ".information", "Squad Base Owned by Squad " + _root.playersquadbases[i][0] + "\r\rCurrent Level:" + _root.playersquadbases[i][1]);
            if (isNaN(_root.playersquadbases[i][2]) || isNaN(_root.playersquadbases[i][3]))
            {
                setProperty("ingamemapsquadbase" + currentdepth, _visible, false);
            } // end if
        } // end of for
        for (i = 0; i < _root.starbaselocation.length; i++)
        {
            ++currentdepth;
            this.attachMovie("ingamemapstarbasemarker", "ingamemapstarbasemarker" + currentdepth, currentdepth);
            setProperty("ingamemapstarbasemarker" + currentdepth, _x, _root.starbaselocation[i][1] * xratio + gridxindent + xsecwidth);
            setProperty("ingamemapstarbasemarker" + currentdepth, _y, _root.starbaselocation[i][2] * yratio + gridyindent + ysecwidth);
            set("ingamemapstarbasemarker" + currentdepth + ".label", _root.starbaselocation[i][0]);
            set("ingamemapstarbasemarker" + currentdepth + ".xlocation", _root.starbaselocation[i][1]);
            set("ingamemapstarbasemarker" + currentdepth + ".ylocation", _root.starbaselocation[i][2]);
            set("ingamemapstarbasemarker" + currentdepth + ".isactive", _root.starbaselocation[i][5]);
        } // end of for
        for (i = 0; i < _root.sectormapitems.length; i++)
        {
            if (_root.sectormapitems[i][0].substr(0, 2) == "TB")
            {
                ++currentdepth;
                this.attachMovie("ingamemapnavpoint", "teambase" + currentdepth, currentdepth);
                setProperty("teambase" + currentdepth, _x, _root.sectormapitems[i][1] * xratio + gridxindent + xsecwidth);
                setProperty("teambase" + currentdepth, _y, _root.sectormapitems[i][2] * yratio + gridyindent + ysecwidth);
                set("teambase" + currentdepth + ".label", "Team Base " + _root.sectormapitems[i][0].substr(2));
                set("teambase" + currentdepth + ".xlocation", _root.sectormapitems[i][1]);
                set("teambase" + currentdepth + ".ylocation", _root.sectormapitems[i][2]);
                set("teambase" + currentdepth + ".information", _root.sectormapitems[i][5]);
            } // end if
        } // end of for
        for (i = 0; i < _root.sectormapitems.length; i++)
        {
            if (_root.sectormapitems[i][0].substr(0, 2) == "NP")
            {
                ++currentdepth;
                this.attachMovie("ingamemapnavpoint", "ingamemapnavpoint" + currentdepth, currentdepth);
                setProperty("ingamemapnavpoint" + currentdepth, _x, _root.sectormapitems[i][1] * xratio + gridxindent + xsecwidth);
                setProperty("ingamemapnavpoint" + currentdepth, _y, _root.sectormapitems[i][2] * yratio + gridyindent + ysecwidth);
                set("ingamemapnavpoint" + currentdepth + ".label", "Nav " + _root.sectormapitems[i][0].substr(2));
                set("ingamemapnavpoint" + currentdepth + ".xlocation", _root.sectormapitems[i][1]);
                set("ingamemapnavpoint" + currentdepth + ".ylocation", _root.sectormapitems[i][2]);
                set("ingamemapnavpoint" + currentdepth + ".information", _root.sectormapitems[i][5]);
            } // end if
        } // end of for
    } // End of the function
    function setupplayerslocater()
    {
        ++currentdepth;
        this.attachMovie("ingamemapplayership", "ingamemapplayership", currentdepth);
        setProperty("ingamemapplayership", _x, _root.shipcoordinatex * xratio + gridxindent + xsecwidth);
        setProperty("ingamemapplayership", _y, _root.shipcoordinatey * yratio + gridyindent + ysecwidth);
        setProperty("ingamemapplayership", _rotation, _root.playershipfacing);
    } // End of the function
    gridxindent = 40;
    gridyindent = 60;
    gridsize_x = 500;
    gridsize_y = 375;
    noofxsectors = _root.sectorinformation[0][0];
    noofysectors = _root.sectorinformation[0][1];
    if (_root.playersdestination[0] != null)
    {
        if (_root.playersdestination[0].substr(0, 2) == "SB")
        {
            _parent.destination = _root.playersdestination[0];
        }
        else if (_root.playersdestination[0].substr(0, 2) == "PL")
        {
            _parent.destination = _root.playersdestination[0].substr(2);
        } // end else if
    }
    else
    {
        _parent.destination = "None";
    } // end else if
    _parent.xrange = null;
    _parent.yrange = null;
    xsecwidth = gridsize_x / noofxsectors;
    ysecwidth = gridsize_y / noofysectors;
    ingamexwidthofasec = _root.sectorinformation[1][0];
    ingameywidthofasec = _root.sectorinformation[1][1];
    xratio = xsecwidth / ingamexwidthofasec;
    yratio = ysecwidth / ingameywidthofasec;
    currentdepth = 50;
    drawgrid();
    setupthewaypoints();
    setupplayerslocater();
}

// [onClipEvent of sprite 890 in frame 1]
onClipEvent (enterFrame)
{
    if (_parent.xrange != null)
    {
        xlocation = _parent.xrange;
        ylocation = _parent.yrange;
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        playerdistancefrompoint = Math.round(Math.sqrt((playersxcoord - xlocation) * (playersxcoord - xlocation) + (playersycoord - ylocation) * (playersycoord - ylocation)));
        _parent.range = "Range: " + playerdistancefrompoint;
    } // end if
    setProperty("ingamemapplayership", _x, _root.shipcoordinatex * xratio + gridxindent + xsecwidth);
    setProperty("ingamemapplayership", _y, _root.shipcoordinatey * yratio + gridyindent + ysecwidth);
    setProperty("ingamemapplayership", _rotation, _root.playershipfacing);
    for (qq = 0; qq < _root.incomingmeteor.length; qq++)
    {
        metid = _root.incomingmeteor[qq][0];
        this.attachMovie("ingamemapmeteormarker", "meteor" + metid, currentdepth + qq + 1);
        setProperty("meteor" + metid, _x, _root.incomingmeteor[qq][1] * xratio + gridxindent + xsecwidth);
        setProperty("meteor" + metid, _y, _root.incomingmeteor[qq][2] * yratio + gridyindent + ysecwidth);
        setProperty("meteor" + metid, _rotation, _root.incomingmeteor[qq][3]);
    } // end of for
}

// [Action in Frame 1]
this._x = 100;
this._y = 0;
destination = "Unknown";
range = "Range:";
stop ();
