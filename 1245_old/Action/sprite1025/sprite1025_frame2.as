﻿// Action script...

// [Action in Frame 2]
this.attachMovie("meteortype" + meteortype, "meteor", 1);
lastime = getTimer();
hittestframe = 0;
hittestint = 5;
xposition = this._x;
yposition = this._y;
this.onEnterFrame = function ()
{
    currenttime = getTimer();
    timechange = (currenttime - lastime) / 1000;
    lastime = currenttime;
    xposition = xposition + xmovement * timechange;
    yposition = yposition + ymovement * timechange;
    this._x = xposition;
    this._y = yposition;
    ++hittestframe;
    if (hittestframe > hittestint)
    {
        hittestframe = 0;
        playershittest();
    } // end if
};
stop ();
