﻿// Action script...

// [Action in Frame 1]
function sendmeteorlife(life)
{
    if (structuredisp <= 0)
    {
        if (meteordeathsentyet == false)
        {
            datatosend = "MET`DM`" + meteorid + "`" + life + "`" + _root.playershipstatus[3][0] + "`" + _root.errorchecknumber + "~";
            _root.mysocket.send(datatosend);
            _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
            meteordeathsentyet = true;
        } // end if
    }
    else
    {
        datatosend = "MET`DM`" + meteorid + "`" + life + "`" + _root.playershipstatus[3][0] + "~";
        _root.mysocket.send(datatosend);
    } // end else if
} // End of the function
function func_takeonothershots(otherplayerdamage)
{
    structuredisp = structuredisp - otherplayerdamage;
} // End of the function
function func_currentlife(meteorlife)
{
    structuredisp = meteorlife - damagebyplayersofar;
    damagebyplayersofar = 0;
} // End of the function
function playershittest()
{
    didplayerhitbase = false;
    numberofplayershots = _root.playershotsfired.length;
    if (numberofplayershots > 0)
    {
        halfshipswidth = this.meteor._width / 2;
        halfshipsheight = this.meteor._height / 2;
        this.relativex = this._x - _root.shipcoordinatex;
        this.relativey = this._y - _root.shipcoordinatey;
    } // end if
    for (i = 0; i < numberofplayershots; i++)
    {
        bulletshotid = _root.playershotsfired[i][0];
        bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
        bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
        if (bulletsx != null)
        {
            if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
            {
                didplayerhitbase = true;
                if (_root.playershotsfired[i][13] == "GUNS")
                {
                    playerdamaged = playerdamaged - _root.guntype[_root.playershotsfired[i][6]][4];
                    _root.gunfireinformation = _root.gunfireinformation + ("GH`MET`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[i][0] + "`" + baseidname + "~");
                }
                else if (_root.playershotsfired[i][13] == "MISSILE")
                {
                    playerdamaged = playerdamaged - _root.missile[_root.playershotsfired[i][6]][4];
                    _root.gunfireinformation = _root.gunfireinformation + ("MH`MET`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[i][0] + "`" + baseidname + "~");
                } // end else if
                totalstructdamage = totalstructdamage - playerdamaged;
                structuredisp = structuredisp + playerdamaged;
                damagebyplayersofar = damagebyplayersofar - playerdamaged;
                playerdamaged = 0;
                removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                _root.playershotsfired[i][5] = 0;
            } // end if
        } // end if
    } // end of for
    if (Number(structuredisp) <= 0 || damagebyplayersofar > 15000)
    {
        if (didplayerhitbase == true)
        {
            laststructupdate = strucupdateint + getTimer();
            meteorlife = structuredisp;
            sendmeteorlife(meteorlife);
            damagebyplayersofar = 0;
        } // end if
    } // end if
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function
currentdamge = 0;
damagebyplayersofar = 0;
structuredisp = meteorlife;
meteordeathsentyet = false;
