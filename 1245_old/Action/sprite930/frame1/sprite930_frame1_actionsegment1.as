﻿// Action script...

function func_scrollwindow(direction)
{
    if (direction == "up")
    {
        textinfo.scroll = textinfo.scroll - 1;
    }
    else if (direction == "down")
    {
        textinfo.scroll = textinfo.scroll + 1;
    } // end else if
    if (textinfo.scroll == textinfo.maxscroll)
    {
        downscroll._visible = false;
    }
    else
    {
        downscroll._visible = true;
    } // end else if
    if (textinfo.scroll == 1)
    {
        upscroll._visible = false;
    }
    else
    {
        upscroll._visible = true;
    } // end else if
} // End of the function
motion = false;
activated = false;
refreshrate = 12000;
nextrefresh = getTimer();
this.onEnterFrame = function ()
{
    if (motion)
    {
        if (activated)
        {
            textinfo.scroll = 0;
            nextFrame ();
        }
        else
        {
            prevFrame ();
        } // end if
    } // end else if
    if (activated)
    {
        nowtime = getTimer();
        if (nowtime > nextrefresh)
        {
            nextrefresh = nowtime + refreshrate;
            lastingo = textinfo;
            textinfo = _root.randomegameevents.func_printoutdealsforlist();
            if (lastinfo != textinfo)
            {
                textinfo.scroll = 0;
                func_scrollwindow();
                lastinfo = textinfo;
            } // end if
        } // end if
    } // end if
};
stop ();
