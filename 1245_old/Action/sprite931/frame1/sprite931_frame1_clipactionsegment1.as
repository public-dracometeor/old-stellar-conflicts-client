﻿// Action script...

// [onClipEvent of sprite 890 in frame 1]
onClipEvent (enterFrame)
{
    if (_parent.xrange != null)
    {
        xlocation = _parent.xrange;
        ylocation = _parent.yrange;
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        playerdistancefrompoint = Math.round(Math.sqrt((playersxcoord - xlocation) * (playersxcoord - xlocation) + (playersycoord - ylocation) * (playersycoord - ylocation)));
        _parent.range = "Range: " + playerdistancefrompoint;
    } // end if
    setProperty("ingamemapplayership", _x, _root.shipcoordinatex * xratio + gridxindent + xsecwidth);
    setProperty("ingamemapplayership", _y, _root.shipcoordinatey * yratio + gridyindent + ysecwidth);
    setProperty("ingamemapplayership", _rotation, _root.playershipfacing);
    for (qq = 0; qq < _root.incomingmeteor.length; qq++)
    {
        metid = _root.incomingmeteor[qq][0];
        this.attachMovie("ingamemapmeteormarker", "meteor" + metid, currentdepth + qq + 1);
        setProperty("meteor" + metid, _x, _root.incomingmeteor[qq][1] * xratio + gridxindent + xsecwidth);
        setProperty("meteor" + metid, _y, _root.incomingmeteor[qq][2] * yratio + gridyindent + ysecwidth);
        setProperty("meteor" + metid, _rotation, _root.incomingmeteor[qq][3]);
    } // end of for
}
