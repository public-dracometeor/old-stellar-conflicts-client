﻿// Action script...

// [Action in Frame 1]
thiscolor = new Color(this);
starcolor = 16776960;
endcolour = 16777215;
increments = 17;
currentcolour = starcolor;
incremuntsup = true;
thiscolor.setRGB(starcolor);
this.onEnterFrame = function ()
{
    if (incremuntsup == true)
    {
        currentcolour = currentcolour + increments;
        if (currentcolour >= endcolour)
        {
            incremuntsup = false;
        } // end if
    }
    else
    {
        currentcolour = currentcolour - increments;
        if (currentcolour <= starcolor)
        {
            incremuntsup = true;
        } // end if
    } // end else if
    thiscolor.setRGB(currentcolour);
};
