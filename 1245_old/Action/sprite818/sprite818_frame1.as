﻿// Action script...

// [Action in Frame 1]
function func_attachturrets()
{
    turrettype = guntype;
    noofturrets = _root.shiptype[shiptype][5].length;
    for (qq = 0; qq < noofturrets; qq++)
    {
        this.attachMovie("turrettypeaiship", "turrettypeaiship" + qq, 200 + qq);
        this["turrettypeaiship" + qq].shottype = turrettype;
        this["turrettypeaiship" + qq]._x = _root.shiptype[shiptype][5][qq][0];
        this["turrettypeaiship" + qq]._y = _root.shiptype[shiptype][5][qq][1];
    } // end of for
    coords = "X:" + _root.playershipstatus[6][0] + " Y:" + _root.playershipstatus[6][1];
    idno = shipname.substr(4);
    endxcoord = _root.neutralships[loc][2];
    endycoord = _root.neutralships[loc][3];
    destination = "Unknown";
    for (qt = 0; qt < _root.starbaselocation.length; qt++)
    {
        if (_root.starbaselocation[qt][1] == endxcoord && _root.starbaselocation[qt][2] == endycoord)
        {
            destination = _root.starbaselocation[qt][0];
            if (destination.substr(0, 2) == "PL")
            {
                destination = "Planet " + destination.substr(2);
            } // end if
            break;
        } // end if
    } // end of for
    globalmessage = "The following is a Mayday Message.. This is Cargo Ship " + idno + " requesting help... under... attack from " + _root.playershipstatus[3][2] + ".  I am at " + coords + " heading... to " + destination;
    datatosend = "NEWS`MES`" + globalmessage + "~";
    _root.mysocket.send(datatosend);
} // End of the function
function adddamage(damagetoadd, damagefrom)
{
    damagetoadd = Number(damagetoadd);
    currentshipshield = currentshipshield - damagetoadd;
    if (currentshipshield < 0)
    {
        shiphealth = shiphealth + currentshipshield;
        currentshipshield = 0;
    } // end if
    if (shiphealth <= 0)
    {
        currentshipstatus = "dieing";
        i = 999999;
        shipkillerid = damagefrom;
    } // end if
    this.ship.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
    this.ship.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
} // End of the function
function movementofanobjectwiththrust()
{
    relativefacing = this.rotation;
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    }
    else if (relativefacing == 90)
    {
        ymovement = 0;
        xmovement = velocity;
    }
    else if (relativefacing == 270)
    {
        ymovement = 0;
        xmovement = -velocity;
    }
    else if (relativefacing == 180)
    {
        ymovement = velocity;
        xmovement = 0;
    }
    else if (velocity != 0)
    {
        this.relativefacing = this.relativefacing;
        ymovement = -velocity * Math.cos(this.relativefacing * radianmultiplier);
        xmovement = velocity * Math.sin(this.relativefacing * radianmultiplier);
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function deathinfosend()
{
    datatosend = "SA~NEUT`death`" + shipid + "`" + _root.playershipstatus[3][0] + "`" + String(shipbounty) + "`" + Math.round(shipbounty * _root.playershipstatus[5][6]) + "`" + shipbounty + "~";
    _root.mysocket.send(datatosend);
} // End of the function
function dropai(reason)
{
    datatosend = "AI`drop`" + shipid;
    "~";
    _root.mysocket.send(datatosend);
    if (reason == "otherships")
    {
        message = "Cya Mate, Looks like another player wants to battle you!";
        _root.func_messangercom(message, shipname, "BEGIN");
    } // end if
    if (reason == "friendlyfire")
    {
        message = "Hey, I\'m not going to play with your friend!";
        _root.func_messangercom(message, shipname, "BEGIN");
    } // end if
    for (i = 0; i < _root.aishipshosted.length; i++)
    {
        if (shipid == _root.aishipshosted[i][0])
        {
            _root.aishipshosted.splice(i, 1);
            this.gotoAndPlay("dropaiship");
            i = 999999;
        } // end if
    } // end of for
} // End of the function
function otherplayershipshots()
{
    for (cc = 0; cc < _root.othergunfire.length; cc++)
    {
        if (this.hitTest("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]) == true && currentshipstatus == "alive" && _root.othergunfire[cc][10] != "AI")
        {
            playerwhoshothitid = _root.othergunfire[cc][0];
            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
            _root.gunfireinformation = _root.gunfireinformation + ("GH`AI" + globalid + "`" + _root.othergunfire[cc][0] + "`" + _root.othergunfire[cc][9] + "~");
            gunshothitplayertype = _root.othergunfire[cc][7];
            _root.othergunfire.splice(cc, 1);
            --cc;
            currentshipshield = currentshipshield - _root.guntype[gunshothitplayertype][4];
            if (currentshipshield < 0)
            {
                shiphealth = shiphealth + currentshipshield;
                currentshipshield = 0;
            } // end if
            if (shiphealth <= 0 && currentshipstatus == "alive")
            {
                currentshipstatus = "dieing";
                shipkillerid = playerwhoshothitid;
                continue;
            } // end if
            this.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
            this.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
        } // end if
    } // end of for
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function
function func_begininteraction()
{
    message = "Leave me Alone, And I will leave you alone.";
    _root.func_messangercom(message, shipname, "BEGIN");
} // End of the function
shipspeedaleration = _root.shipspeedalterationforneuts;
maxplayerrangebeforeinteraction = 1000;
interactedwithplay = false;
radianmultiplier = 0.017453;
currenttimechangeratio = _root.currenttimechangeratio;
currenthostplayer = _root.playershipstatus[3][0];
lastshooter = new Array();
currentgunfireshot = 0;
shipkillerid = null;
shipid = globalshipname;
for (i = 0; i < _root.neutralships.length; i++)
{
    if (shipid == _root.neutralships[i][10])
    {
        loc = i;
        currentshipidno = i;
        shiptype = _root.neutralships[loc][4];
        shipname = _root.neutralships[loc][11];
        shieldgenerator = _root.shiptype[shiptype][6][0] - 1;
        guntype = _root.shiptype[shiptype][6][3] - 1;
        travelspeed = _root.shiptype[shiptype][3][1];
        currentvelocity = travelspeed / shipspeedaleration;
        startxcoord = _root.neutralships[loc][0];
        startycoord = _root.neutralships[loc][1];
        endxcoord = _root.neutralships[loc][2];
        endycoord = _root.neutralships[loc][3];
        totalrangeleft = Math.round(Math.sqrt((startxcoord - endxcoord) * (startxcoord - endxcoord) + (startycoord - endycoord) * (startycoord - endycoord)));
        angletotravel = 360 - Math.atan2(startxcoord - endxcoord, startycoord - endycoord) / 0.017453;
        if (angletotravel > 360)
        {
            angletotravel = angletotravel - 360;
        }
        else if (angletotravel < 0)
        {
            angletotravel = angletotravel + 360;
        } // end else if
        this.rotation = angletotravel;
        this._rotation = angletotravel;
        if (_root.neutralships[loc][6] != "N/A" && _root.neutralships[loc][6] < getTimer())
        {
            relativefacing = angletotravel;
            velocity = travelspeed * ((getTimer() - _root.neutralships[loc][6]) / 1000) / shipspeedaleration;
            movementofanobjectwiththrust();
        } // end if
        startedtime = _root.neutralships[loc][6];
        this.xcoord = startxcoord + xmovement;
        this._x = this.xcoord - _root.shipcoordinatex;
        this.ycoord = startycoord + ymovement;
        this._y = this.ycoord - _root.shipcoordinatey;
        i = 99999;
    } // end if
} // end of for
this.ainame = shipname;
distancetodropai = 12000;
maxshipshield = _root.shieldgenerators[shieldgenerator][0];
shipshieldreplenish = Math.round(_root.shieldgenerators[shieldgenerator][1]);
currentshipshield = maxshipshield;
currentshipstatus = "alive";
rotationspeed = _root.shiptype[shiptype][3][2];
acceleration = Math.round(_root.shiptype[shiptype][3][0]);
maxvelocity = Math.round(_root.shiptype[shiptype][3][1]) / shipspeedaleration;
rotationspeed = _root.shiptype[shiptype][3][2];
shiphealth = _root.shiptype[shiptype][3][3];
noofhardpoints = _root.shiptype[shiptype][2].length;
hardpoints = new Array();
for (i = 0; i < noofhardpoints; i++)
{
    hardpoints[i] = new Array();
    hardpoints[i][0] = guntype;
    hardpoints[i][1] = 0;
    hardpoints[i][2] = _root.shiptype[shiptype][2][i][0];
    hardpoints[i][3] = _root.shiptype[shiptype][2][i][1];
    hardpoints[i][4] = Math.round(_root.guntype[hardpoints[i][0]][2] * 1000);
    hardpoints[i][5] = Math.round(_root.guntype[hardpoints[i][0]][0]);
    hardpoints[i][6] = _root.guntype[hardpoints[i][0]][3];
} // end of for
this.attachMovie("shiptype" + shiptype, "ship", 1);
totalshipworth = _root.shiptype[shiptype][1];
for (currenthardpoint = 0; currenthardpoint < hardpoints.length; currenthardpoint++)
{
    currentguntype = hardpoints[0][currenthardpoint][0];
    if (!isNaN(currentguntype))
    {
        totalshipworth = totalshipworth + _root.guntype[currentguntype][5];
    } // end if
} // end of for
totalshipworth = totalshipworth + _root.shieldgenerators[shieldgenerator][5];
shipbounty = Math.round(totalshipworth / _root.playersworthtobtymodifire);
