﻿// Action script...

// [Action in Frame 2]
timeshit = 0;
hitstillhostile = 2;
shipnothostile = true;
curenttime = _root.curenttime;
shipdisrupted = false;
shipemped = false;
hitteestintervals = 4;
currentframeforhittest = Math.round(Math.random() * hitteestintervals);
nextupdateframe = 0;
framedelay = 7;
nextupdateframe = Math.round(Math.random() * framedelay);
movementupdateinterval = 40;
nextmovementtime = curenttime + movementupdateinterval;
lastshipdisplaytime = curenttime;
globalid = shipid;
this.onEnterFrame = function ()
{
    curenttime = _root.curenttime;
    ++currentframeforhittest;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    playerdistancefromship = Math.round(Math.sqrt((playersxcoord - this.xcoord) * (playersxcoord - this.xcoord) + (playersycoord - this.ycoord) * (playersycoord - this.ycoord)));
    if (playerdistancefromship < 2000 && currentshipstatus == "alive" && currentframeforhittest > hitteestintervals)
    {
        currentframeforhittest = 0;
        shieldtimechangeratio = (curenttime - lastshieldtime) / 1000;
        lastshieldtime = curenttime;
        if (shipemped == true)
        {
            currentshipshield = 0;
        }
        else if (currentshipshield < maxshipshield)
        {
            currentshipshield = currentshipshield + shipshieldreplenish * shieldtimechangeratio;
            if (currentshipshield > maxshipshield)
            {
                currentshipshield = maxshipshield;
            } // end if
        } // end else if
        numberofplayershots = _root.playershotsfired.length;
        if (numberofplayershots > 0)
        {
            halfshipswidth = this.ship._width / 2;
            halfshipsheight = this.ship._height / 2;
            this.relativex = this._x;
            this.relativey = this._y;
        } // end if
        for (i = 0; i < numberofplayershots; i++)
        {
            bulletshotid = _root.playershotsfired[i][0];
            bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
            bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
            if (bulletsx != null)
            {
                if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
                {
                    ++timeshit;
                    if (timeshit >= hitstillhostile && shipnothostile)
                    {
                        shipnothostile = false;
                        func_attachturrets();
                    } // end if
                    if (_root.playershotsfired[i][13] == "GUNS")
                    {
                        currentshipshield = currentshipshield - _root.guntype[_root.playershotsfired[i][6]][4];
                        _root.gunfireinformation = _root.gunfireinformation + ("GH`AI" + globalid + "`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[cc][0] + "~");
                    }
                    else if (_root.playershotsfired[i][13] == "MISSILE")
                    {
                        missiletype = _root.playershotsfired[i][6];
                        currentshipshield = currentshipshield - _root.missile[missiletype][4];
                        if (_root.missile[missiletype][8] == "EMP")
                        {
                        } // end if
                        if (_root.missile[missiletype][8] == "DISRUPTER")
                        {
                        } // end if
                    } // end else if
                    if (currentshipshield < 0)
                    {
                        shiphealth = shiphealth + currentshipshield;
                        currentshipshield = 0;
                    } // end if
                    if (shiphealth <= 0)
                    {
                        currentshipstatus = "dieing";
                        i = 999999;
                        shipkillerid = currenthostplayer;
                    } // end if
                    removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                    _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                    lastshooter[2] = lastshooter[1];
                    lastshooter[1] = lastshooter[0];
                    lastshooter[0] = currenthostplayer;
                    _root.playershotsfired[i][5] = 0;
                    this.ship.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
                    this.ship.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
                } // end if
            } // end if
        } // end of for
        otherplayershipshots();
    } // end if
    if (currentshipstatus == "dieing")
    {
        deathinfosend();
        this.xcoord = xcoord;
        this.ycoord = ycoord;
        currentshipstatus = "dead";
        numberoftaunts = _root.aimessages[1].length - 1;
        tauntnumber = Math.round(numberoftaunts * Math.random());
        message = _root.aimessages[1][tauntnumber];
        _root.func_messangercom(message, shipname, "END");
        tw = 0;
        this.ship.removeMovieClip();
        this.attachMovie("shiptype" + shiptype + "shield", "shipshield", 200);
        this.shipshield._alpha = 0;
        this.gotoAndPlay("dieing");
    } // end if
    if (currentshipstatus == "alive")
    {
        ++nextupdateframe;
        if (nextupdateframe > framedelay)
        {
            nextupdateframe = 0;
            if (shipemped == true)
            {
                if (timetillempends < getTimer())
                {
                    shipemped = false;
                } // end if
            } // end if
        } // end if
    } // end if
    if (nextmovementtime < curenttime)
    {
        nextmovementtime = curenttime + movementupdateinterval;
        playersxcoord = _root.shipcoordinatex;
        playersycoord = _root.shipcoordinatey;
        playerdistancefromship = Math.round(Math.sqrt((playersxcoord - this.xcoord) * (playersxcoord - this.xcoord) + (playersycoord - this.ycoord) * (playersycoord - this.ycoord)));
        if (interactedwithplay == false)
        {
            if (playerdistancefromship < maxplayerrangebeforeinteraction)
            {
                func_begininteraction();
                interactedwithplay = true;
            } // end if
        } // end if
        this.rotation = relativefacing = angletotravel;
        velocity = travelspeed * ((getTimer() - startedtime) / 1000) / shipspeedaleration;
        movementofanobjectwiththrust();
        this.xcoord = startxcoord + xmovement;
        this._x = this.xcoord - _root.shipcoordinatex;
        this.ycoord = startycoord + ymovement;
        this._y = this.ycoord - _root.shipcoordinatey;
    } // end if
};
stop ();
