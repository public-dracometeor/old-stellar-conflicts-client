﻿// Action script...

// [Action in Frame 1]
if (_root.soundvolume != "off")
{
    explosionsound = new Sound();
    explosionsound.attachSound("minedropped.wav");
    explosionsound.start();
} // end if
hittestframes = 3;
track = 0;
this._alpha = 75;
this.canhurt = false;
activationtime = getTimer() + 1000;
this.onEnterFrame = function ()
{
    this._x = xposition - _root.shipcoordinatex;
    this._y = yposition - _root.shipcoordinatey;
    this._rotation = this._rotation + 5;
    if (getTimer() > removetime)
    {
        removeMovieClip (this);
    } // end if
    if (canhurt == false)
    {
        if (activationtime < getTimer())
        {
            canhurt = true;
            this._alpha = 100;
        } // end if
    } // end if
    if (ishostilemine)
    {
        ++track;
        if (track > hittestframes)
        {
            if (this.hitTest(_root.gamedisplayarea.playership))
            {
                if (canhurt)
                {
                    _root.gamedisplayarea.adddamage(damage, playerfrom);
                } // end if
                _root.gunfireinformation = _root.gunfireinformation + ("SP`MINEHIT`" + mineid + "~");
                removeMovieClip (this);
            } // end if
            track = 0;
        } // end if
    } // end if
    if (justgothit)
    {
        this._rotation = 0;
        this.onEnterFrame = function ()
        {
            this._x = xposition - _root.shipcoordinatex;
            this._y = yposition - _root.shipcoordinatey;
        };
        this.play();
    } // end if
};
stop ();
