﻿// Action script...

// [onClipEvent of sprite 1224 in frame 1]
onClipEvent (load)
{
    lastdrainrate = 0;
    currentframe = 0;
    updateinterval = 50;
    lastshieldsetting = null;
}

// [onClipEvent of sprite 1224 in frame 1]
onClipEvent (enterFrame)
{
    ++currentframe;
    if (currentframe > updateinterval)
    {
        currentframe = 0;
        if (lastshieldsetting != _root.playershipstatus[2][2])
        {
            lastshieldsetting = _root.playershipstatus[2][2];
            if (lastshieldsetting == "FULL")
            {
                _parent.full.gotoAndStop(1);
                _parent.half.gotoAndStop(2);
                _parent.off.gotoAndStop(2);
            } // end if
            if (lastshieldsetting == "HALF")
            {
                _parent.full.gotoAndStop(2);
                _parent.half.gotoAndStop(1);
                _parent.off.gotoAndStop(2);
            } // end if
            if (lastshieldsetting == "OFF")
            {
                _parent.full.gotoAndStop(2);
                _parent.half.gotoAndStop(2);
                _parent.off.gotoAndStop(1);
            } // end if
        } // end if
        drainrate = 0;
        if (_root.playershipstatus[2][2] != "OFF")
        {
            drainrate = drainrate + _root.shieldgenerators[_root.playershipstatus[2][0]][2];
            drainrate = drainrate + _root.playershipstatus[2][1] / _root.shieldgenerators[_root.playershipstatus[2][0]][0] * _root.shieldgenerators[_root.playershipstatus[2][0]][3];
        } // end if
        drainrate = Math.round(drainrate);
        if (lastdrainrate != drainrate)
        {
            lastdrainrate = drainrate;
            _parent.drainrate = "Drain:" + drainrate + "/s";
        } // end if
    } // end if
}

// [onClipEvent of sprite 1229 in frame 1]
on (release)
{
    _root.playershipstatus[2][2] = "FULL";
}

// [onClipEvent of sprite 1234 in frame 1]
on (release)
{
    _root.playershipstatus[2][2] = "OFF";
}

// [onClipEvent of sprite 1237 in frame 1]
on (release)
{
    _root.playershipstatus[2][2] = "HALF";
}

// [Action in Frame 1]
stop ();
