﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "EXIT";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent._parent.gotoAndStop("squadbasemenu");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.changeleader._visible = true;
    _parent.changeleader.commandchanging = "Change Second To:";
    _parent.changeleader.changeleadnameto = "ENTERNAME";
    _parent.changeleader.changeleadernamebut.leaderposition = "SECOND";
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.changeleader._visible = true;
    _parent.changeleader.commandchanging = "Change Third To:";
    _parent.changeleader.changeleadnameto = "ENTERNAME";
    _parent.changeleader.changeleadernamebut.leaderposition = "THIRD";
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.changemessage._x = -360;
    _parent.changemessage._y = 5;
    _parent.changemessage._visible = true;
    _parent.changemessage.newsquadmessage = _parent.squadmessage;
    this._visible = false;
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "TREATIES";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.gotoAndStop("treaties");
}

// [Action in Frame 1]
function initializesetup()
{
    secondin._visible = false;
    thirdin._visible = false;
    this.alterations._visible = false;
    this.changemessagebut._visible = false;
    this.changeleader._visible = false;
    this.changemessage._visible = false;
    playersname = _root.playershipstatus[3][2];
    squadmemberlist = "Loading";
    creatorname = "Loading";
    secondname = "Loading";
    thirdname = "Loading";
    noofmembers = "N/A";
    currentpass = "Loading";
    squadmembers = new Array();
    squadmessage = "Loading";
    newsquadVars = new XML();
    newsquadVars.load(_root.pathtoaccounts + "squadscommand.php?mode=squadinfo&sname=" + _root.playershipstatus[5][10] + "&spass=" + _root.playershipstatus[5][13]);
    newsquadVars.onLoad = function (success)
    {
        loadedinfo = String(newsquadVars);
        newinfo = loadedinfo.split("~");
        noofsquad = 0;
        for (i = 0; i < newinfo.length - 1; i++)
        {
            currentthread = newinfo[i].split("`");
            if (currentthread[0] == "P")
            {
                squadmembers[noofsquad] = currentthread[1];
                ++noofsquad;
            } // end if
            if (currentthread[0] == "2ND")
            {
                secondname = currentthread[1];
                if (secondname.length < 1)
                {
                    secondname = "NONE";
                } // end if
            } // end if
            if (currentthread[0] == "3RD")
            {
                thirdname = currentthread[1];
                if (thirdname.length < 1)
                {
                    thirdname = "NONE";
                } // end if
            } // end if
            if (currentthread[0] == "CR")
            {
                creatorname = currentthread[1];
            } // end if
            if (currentthread[0] == "PASS")
            {
                squadpassword = currentthread[1];
            } // end if
            if (currentthread[0] == "MESS")
            {
                squadmessage = currentthread[1];
            } // end if
        } // end of for
        if (playersname == creatorname)
        {
            setvisiblescreator();
        } // end if
        if (playersname == secondname || playersname == thirdname || playersname == creatorname)
        {
            setvisiblesleaders();
        } // end if
        displaysquadlist();
    };
} // End of the function
function setvisiblescreator()
{
    secondin._visible = true;
    thirdin._visible = true;
} // End of the function
function setvisiblesleaders()
{
    this.alterations._visible = true;
    this.alterations.currentpass = squadpassword;
    this.changemessagebut._visible = true;
} // End of the function
function displaysquadlist()
{
    squadmemberlist = "";
    for (jj = 0; jj < squadmembers.length; jj++)
    {
        squadmemberlist = squadmemberlist + (jj + 1 + ": " + squadmembers[jj] + "\r");
    } // end of for
    noofmembers = squadmembers.length;
} // End of the function
initializesetup();
stop ();
