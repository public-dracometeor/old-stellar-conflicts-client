﻿// Action script...

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("allie2");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("allie3");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "MAIN";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.gotoAndStop(1);
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("enemy2");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("enemy3");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("allie1");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("enemy1");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("basestatus");
}

// [Action in Frame 2]
function initializesetup()
{
    playersname = _root.playershipstatus[3][2];
    allie1 = "Loading";
    allie2 = "Loading";
    allie3 = "Loading";
    enemy1 = "Loading";
    enemy2 = "Loading";
    enemy3 = "Loading";
    basestatus = "Loading";
    commandchanging = "Select";
    this.changer._visible = false;
    newsquadVarss = new XML();
    newsquadVarss.load(_root.pathtoaccounts + "squadscommand.php?mode=treaties&sname=" + _root.playershipstatus[5][10]);
    newsquadVarss.onLoad = function (success)
    {
        loadedinfo = String(newsquadVarss);
        processdata(loadedinfo);
    };
} // End of the function
function processdata(loadedinfo)
{
    newinfo = loadedinfo.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentthread = newinfo[i].split("`");
        if (currentthread[0] == "AL1")
        {
            allie1 = currentthread[1];
            if (allie1.length < 1)
            {
                allie1 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "AL2")
        {
            allie2 = currentthread[1];
            if (allie2.length < 1)
            {
                allie2 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "AL3")
        {
            allie3 = currentthread[1];
            if (allie3.length < 1)
            {
                allie3 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "EN1")
        {
            enemy1 = currentthread[1];
            if (enemy1.length < 1)
            {
                enemy1 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "EN2")
        {
            enemy2 = currentthread[1];
            if (enemy2.length < 1)
            {
                enemy2 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "EN3")
        {
            enemy3 = currentthread[1];
            if (enemy3.length < 1)
            {
                enemy3 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "BST")
        {
            basestatus = currentthread[1];
            if (basestatus.length < 1)
            {
                basestatus = "HOSTILE";
            } // end if
        } // end if
    } // end of for
} // End of the function
function change(changing)
{
    if (changing == "allie1")
    {
        chaningto = "ALLIE 1";
    } // end if
    if (changing == "allie2")
    {
        chaningto = "ALLIE 2";
    } // end if
    if (changing == "allie3")
    {
        chaningto = "ALLIE 3";
    } // end if
    if (changing == "enemy1")
    {
        chaningto = "ENEMY 1";
    } // end if
    if (changing == "enemy2")
    {
        chaningto = "ENEMY 2";
    } // end if
    if (changing == "enemy3")
    {
        chaningto = "ENEMY 3";
    } // end if
    if (changing == "enemy3")
    {
        chaningto = "ENEMY 3";
    } // end if
    if (changing == "basestatus")
    {
        chaningto = "Base Setting";
    } // end if
    this.changer._visible = true;
    this.changer.commandchanging = "Changing " + chaningto + " To";
    this.changer.changinto = "";
    this.changer.field = changing;
} // End of the function
function changething(field, newvar)
{
    field = field.toUpperCase();
    newvar = newvar.toUpperCase();
    newsquadVarss = new XML();
    newsquadVarss.load(_root.pathtoaccounts + "squadscommand.php?mode=treatieschange&sname=" + _root.playershipstatus[5][10] + "&varchan=" + field + "&newvar=" + newvar + "&spass=" + _root.playershipstatus[5][13]);
    newsquadVarss.onLoad = function (success)
    {
        loadedinfo = String(newsquadVarss);
        processdata(loadedinfo);
    };
} // End of the function
initializesetup();
stop ();
