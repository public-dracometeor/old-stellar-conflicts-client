﻿// Action script...

// [onClipEvent of sprite 1160 in frame 1]
onClipEvent (load)
{
    this.button = "Z";
    this.textfieldlink = "specialoneinfo";
}

// [onClipEvent of sprite 1160 in frame 1]
onClipEvent (load)
{
    this.button = "X";
    this.textfieldlink = "specialtwoinfo";
}

// [onClipEvent of sprite 1160 in frame 1]
onClipEvent (load)
{
    this.button = "S";
    this.textfieldlink = "specialthreeinfo";
}

// [Action in Frame 1]
function specialsinfo(info, newstatus, nexttime, specialno)
{
    if (specialno == 1)
    {
        specialoneinfo = info;
        if (newstatus == "RELOAD")
        {
            specialonebutton.reloadtime = nexttime;
            displayspecials(1);
            specialonebutton.gotoAndStop("RELOAD");
        } // end if
        if (newstatus == "FAILED")
        {
            specialonebutton.reloadtime = nexttime;
            displayspecials(1);
            specialonebutton.gotoAndStop("RELOAD");
        } // end if
        if (newstatus == "ON")
        {
            specialonebutton.gotoAndStop("ON");
            displayspecials(1);
        } // end if
        if (newstatus == "OFF")
        {
            displayspecials(1);
            specialonebutton.gotoAndStop("OFF");
        } // end if
    } // end if
    if (specialno == 2)
    {
        specialtwoinfo = info;
        if (newstatus == "RELOAD")
        {
            specialtwobutton.reloadtime = nexttime;
            specialtwobutton.gotoAndStop("RELOAD");
            displayspecials(2);
        } // end if
        if (newstatus == "FAILED")
        {
            specialtwobutton.reloadtime = nexttime;
            specialtwobutton.gotoAndStop("RELOAD");
            displayspecials(2);
        } // end if
        if (newstatus == "ON")
        {
            specialtwobutton.gotoAndStop("ON");
            displayspecials(2);
        } // end if
        if (newstatus == "OFF")
        {
            specialtwobutton.gotoAndStop("OFF");
            displayspecials(2);
        } // end if
    } // end if
    if (specialno == 3)
    {
        specialthreeinfo = info;
        if (newstatus == "RELOAD")
        {
            specialthreebutton.reloadtime = nexttime;
            specialthreebutton.gotoAndStop("RELOAD");
            displayspecials(3);
        } // end if
        if (newstatus == "FAILED")
        {
            specialthreebutton.reloadtime = nexttime;
            specialthreebutton.gotoAndStop("RELOAD");
            displayspecials(3);
        } // end if
        if (newstatus == "ON")
        {
            specialthreebutton.gotoAndStop("ON");
            displayspecials(3);
        } // end if
        if (newstatus == "OFF")
        {
            specialthreebutton.gotoAndStop("OFF");
            displayspecials(3);
        } // end if
    } // end if
} // End of the function
function refreshspecialoneup()
{
    specialoneinfo = "";
    itemfound = false;
    if (_root.playershipstatus[11][0][0] == null)
    {
        _root.playershipstatus[11][0][0] = 0;
    } // end if
    for (i = _root.playershipstatus[11][0][0] + 1; i != _root.playershipstatus[11][0][0]; i++)
    {
        if (i >= _root.playershipstatus[11][1].length)
        {
            i = 0;
        } // end if
        if (_root.playershipstatus[11][1].length == 0)
        {
            itemfound = false;
            break;
        } // end if
        if (_root.playershipstatus[11][1].length <= 2 && _root.playershipstatus[11][0][1] != null && _root.playershipstatus[11][0][2] != null)
        {
            itemfound = false;
            break;
        } // end if
        if (_root.playershipstatus[11][1].length <= 1 && (_root.playershipstatus[11][0][1] != null || _root.playershipstatus[11][0][2] != null))
        {
            itemfound = false;
            break;
        } // end if
        if (i != _root.playershipstatus[11][0][1] && i != _root.playershipstatus[11][0][2])
        {
            _root.playershipstatus[11][0][0] = i;
            itemfound = true;
            break;
        } // end if
    } // end of for
    if (itemfound == false)
    {
        _root.playershipstatus[11][0][0] = null;
        specialone = "None";
    }
    else
    {
        displayspecials(1);
    } // end else if
} // End of the function
function refreshspecialtwoup()
{
    specialtwoinfo = "";
    itemfound = false;
    if (_root.playershipstatus[11][0][1] == null)
    {
        _root.playershipstatus[11][0][1] = 0;
    } // end if
    for (i = _root.playershipstatus[11][0][1] + 1; i != _root.playershipstatus[11][0][1]; i++)
    {
        if (i >= _root.playershipstatus[11][1].length)
        {
            i = 0;
        } // end if
        if (_root.playershipstatus[11][1].length == 0)
        {
            itemfound = false;
            break;
        } // end if
        if (_root.playershipstatus[11][1].length <= 2 && _root.playershipstatus[11][0][0] != null && _root.playershipstatus[11][0][2] != null)
        {
            itemfound = false;
            break;
        } // end if
        if (_root.playershipstatus[11][1].length <= 1 && (_root.playershipstatus[11][0][0] != null || _root.playershipstatus[11][0][2] != null))
        {
            itemfound = false;
            break;
        } // end if
        if (i != _root.playershipstatus[11][0][0] && i != _root.playershipstatus[11][0][2])
        {
            _root.playershipstatus[11][0][1] = i;
            itemfound = true;
            break;
        } // end if
    } // end of for
    if (itemfound == false)
    {
        _root.playershipstatus[11][0][1] = null;
        specialtwo = "None";
    }
    else
    {
        displayspecials(2);
    } // end else if
} // End of the function
function refreshspecialthreeup()
{
    specialthreeinfo = "";
    itemfound = false;
    if (_root.playershipstatus[11][0][2] == null)
    {
        _root.playershipstatus[11][0][2] = 0;
    } // end if
    for (i = _root.playershipstatus[11][0][2] + 1; i != _root.playershipstatus[11][0][2]; i++)
    {
        if (i >= _root.playershipstatus[11][1].length)
        {
            i = 0;
        } // end if
        if (_root.playershipstatus[11][1].length == 0)
        {
            itemfound = false;
            break;
        } // end if
        if (_root.playershipstatus[11][1].length <= 2 && _root.playershipstatus[11][0][0] != null && _root.playershipstatus[11][0][1] != null)
        {
            itemfound = false;
            break;
        } // end if
        if (_root.playershipstatus[11][1].length <= 1 && (_root.playershipstatus[11][0][0] != null || _root.playershipstatus[11][0][1] != null))
        {
            itemfound = false;
            break;
        } // end if
        if (i != _root.playershipstatus[11][0][0] && i != _root.playershipstatus[11][0][1])
        {
            _root.playershipstatus[11][0][2] = i;
            itemfound = true;
            break;
        } // end if
    } // end of for
    if (itemfound == false)
    {
        _root.playershipstatus[11][0][2] = null;
        specialthree = "None";
    }
    else
    {
        displayspecials(3);
    } // end else if
} // End of the function
function displayspecials(number)
{
    if (number == 1)
    {
        itemlocation = _root.playershipstatus[11][0][0];
        itemtype = _root.playershipstatus[11][1][itemlocation][0];
        specialone = _root.specialshipitems[itemtype][0];
        if (_root.playershipstatus[11][1][itemlocation][1] >= 0)
        {
            specialone = specialone + (" (" + _root.playershipstatus[11][1][itemlocation][1] + ")");
        } // end if
    } // end if
    if (number == 2)
    {
        itemlocation = _root.playershipstatus[11][0][1];
        itemtype = _root.playershipstatus[11][1][itemlocation][0];
        specialtwo = _root.specialshipitems[itemtype][0];
        if (_root.playershipstatus[11][1][itemlocation][1] >= 0)
        {
            specialtwo = specialtwo + (" (" + _root.playershipstatus[11][1][itemlocation][1] + ")");
        } // end if
    } // end if
    if (number == 3)
    {
        itemlocation = _root.playershipstatus[11][0][2];
        itemtype = _root.playershipstatus[11][1][itemlocation][0];
        specialthree = _root.specialshipitems[itemtype][0];
        if (_root.playershipstatus[11][1][itemlocation][1] >= 0)
        {
            specialthree = specialthree + (" (" + _root.playershipstatus[11][1][itemlocation][1] + ")");
        } // end if
    } // end if
} // End of the function
specialoneinfo = "";
specialtwoinfo = "";
specialthreeinfo = "";
refreshspecialoneup();
refreshspecialtwoup();
refreshspecialthreeup();
stop ();
