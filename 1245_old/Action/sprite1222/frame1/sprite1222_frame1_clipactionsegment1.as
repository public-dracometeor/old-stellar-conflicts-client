﻿// Action script...

// [onClipEvent of sprite 1221 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    if (this.autoswitch == false)
    {
        _root.missile_autoswitch = this.autoswitch = true;
        _parent.autoswitch = true;
        this.gotoAndStop(2);
    }
    else if (this.autoswitch == true)
    {
        _root.missile_autoswitch = this.autoswitch = false;
        _parent.autoswitch = false;
        this.gotoAndStop(1);
    } // end else if
}
