﻿// Action script...

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "UPGRADE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    turretnumber = _parent.turretnumber;
    upgradetolvl = _parent.upgradetolvl;
    upgradecost = _parent.upgradecost;
    if (turretnumber > 0 && !isNaN(upgradetolvl) && upgradecost <= _root.playershipstatus[3][1])
    {
        turretnumbertoupg = turretnumber - 1;
        turrets = new Array();
        turrets[0] = _parent._parent.turrets[0];
        turrets[1] = _parent._parent.turrets[1];
        turrets[2] = _parent._parent.turrets[2];
        turrets[3] = _parent._parent.turrets[3];
        turrets[4] = _parent._parent.turrets[4];
        upgradeturretto = upgradetolvl;
        turrets[turretnumbertoupg] = upgradeturretto;
        this.newbaseVars = new XML();
        this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=changegun&baseid=" + _parent._parent.baseidname + "&gunone=" + turrets[0] + "&guntwo=" + turrets[1] + "&gunthree=" + turrets[2] + "&gunfour=" + turrets[3] + "&gunfive=" + turrets[4]);
        this.newbaseVars.onLoad = function (success)
        {
            loadedvars = String(newbaseVars);
            if (loadedvars == "destroyed")
            {
                _parent.gotoAndStop("initialize");
            }
            else if (loadedvars == "completed")
            {
                _parent.costinvolved = upgradecost;
                _parent.successful = true;
                _parent._parent.turrets[0] = turrets[0];
                _parent._parent.turret1disp = "1: " + _root.guntype[turrets[0]][6];
                _parent._parent.turrets[1] = turrets[1];
                _parent._parent.turret2disp = "2: " + _root.guntype[turrets[1]][6];
                _parent._parent.turrets[2] = turrets[2];
                _parent._parent.turret3disp = "3: " + _root.guntype[turrets[2]][6];
                _parent._parent.turrets[3] = turrets[3];
                _parent._parent.turret4disp = "4: " + _root.guntype[turrets[3]][6];
                _parent._parent.turrets[4] = turrets[4];
                _parent._parent.turret5disp = "5: " + _root.guntype[turrets[4]][6];
                _parent.gotoAndStop("completed");
            }
            else
            {
                _parent.costinvolved = upgradecost;
                _parent.successful = false;
                _parent.gotoAndStop("completed");
            } // end else if
        };
    } // end if
}

// [onClipEvent of sprite 1065 in frame 3]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 3]
on (release)
{
    if (_parent.turretnumber > 0 && _parent.changeturretonumber > 0)
    {
        turrets = new Array();
        turrets[0] = _parent._parent.turrets[0];
        turrets[1] = _parent._parent.turrets[1];
        turrets[2] = _parent._parent.turrets[2];
        turrets[3] = _parent._parent.turrets[3];
        turrets[4] = _parent._parent.turrets[4];
        changingturretfrom = _parent.turretnumber - 1;
        changingturretto = _parent.changeturretonumber - 1;
        turretfiller = turrets[changingturretto];
        turrets[changingturretto] = turrets[changingturretfrom];
        turrets[changingturretfrom] = turretfiller;
        this.newbaseVars = new XML();
        this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=changegun&baseid=" + _parent._parent.baseidname + "&gunone=" + turrets[0] + "&guntwo=" + turrets[1] + "&gunthree=" + turrets[2] + "&gunfour=" + turrets[3] + "&gunfive=" + turrets[4]);
        this.newbaseVars.onLoad = function (success)
        {
            loadedvars = String(newbaseVars);
            if (loadedvars == "destroyed")
            {
                _parent.gotoAndStop("initialize");
            }
            else if (loadedvars == "completed")
            {
                _parent.costinvolved = "NONE";
                _parent.successful = true;
                _parent._parent.turrets[0] = turrets[0];
                _parent._parent.turret1disp = "1: " + _root.guntype[turrets[0]][6];
                _parent._parent.turrets[1] = turrets[1];
                _parent._parent.turret2disp = "2: " + _root.guntype[turrets[1]][6];
                _parent._parent.turrets[2] = turrets[2];
                _parent._parent.turret3disp = "3: " + _root.guntype[turrets[2]][6];
                _parent._parent.turrets[3] = turrets[3];
                _parent._parent.turret4disp = "4: " + _root.guntype[turrets[3]][6];
                _parent._parent.turrets[4] = turrets[4];
                _parent._parent.turret5disp = "5: " + _root.guntype[turrets[4]][6];
                _parent.gotoAndStop("completed");
            }
            else
            {
                _parent.costinvolved = "NONE";
                _parent.successful = false;
                _parent.gotoAndStop("completed");
            } // end else if
        };
    } // end if
}

// [onClipEvent of sprite 1065 in frame 4]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 4]
on (release)
{
    newshieldgen = _parent.changeshieldto - 1;
    upgradecost = _parent.upgradecost;
    if (_parent.upgradecost > 0)
    {
        if (upgradecost <= _root.playershipstatus[3][1])
        {
            this.newbaseVars = new XML();
            this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=newshield&baseid=" + _parent._parent.baseidname + "&newshield=" + newshieldgen);
            this.newbaseVars.onLoad = function (success)
            {
                loadedvars = String(newbaseVars);
                if (loadedvars == "destroyed")
                {
                    _parent.gotoAndStop("initialize");
                }
                else if (loadedvars == "completed")
                {
                    _parent.costinvolved = upgradecost;
                    _parent.successful = true;
                    _parent._parent.shieldgenerator = newshieldgen;
                    _parent._parent.generatordisp = _root.shieldgenerators[newshieldgen][4];
                    _parent._parent.maxchargedisp = "Max Charge: " + _root.shieldgenerators[newshieldgen][0] * 10;
                    _parent._parent.chargeratedisp = "Charge Rate: " + _root.shieldgenerators[newshieldgen][1] * 5 + "/sec";
                    _parent.gotoAndStop("completed");
                }
                else
                {
                    _parent.costinvolved = upgradecost;
                    _parent.successful = false;
                    _parent.gotoAndStop("completed");
                } // end else if
            };
        } // end if
    } // end if
}

// [onClipEvent of sprite 1065 in frame 5]
onClipEvent (load)
{
    this.label = "REPAIR";
}

// [onClipEvent of sprite 1065 in frame 5]
on (release)
{
    repaircost = _parent.repaircost;
    if (_parent.repaircost > 0)
    {
        if (_parent.repaircost <= _root.playershipstatus[3][1])
        {
            this.newbaseVars = new XML();
            this.newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=repairbase&baseid=" + _parent._parent.baseidname + "&struct=" + _parent.amouttorepair + "&maxstruct=" + _parent.maxstructure);
            this.newbaseVars.onLoad = function (success)
            {
                loadedvars = String(newbaseVars);
                if (loadedvars == "destroyed")
                {
                    _parent.gotoAndStop("initialize");
                }
                else if (!isNaN(Number(loadedvars)) && loadedvars != "")
                {
                    _parent.costinvolved = repaircost;
                    _parent.successful = true;
                    _parent._parent.totalstructure = Number(loadedvars);
                    _parent._parent.structuredisp = "Structure: " + _parent._parent.totalstructure + " / " + _parent._parent.maxstructure;
                    _parent.gotoAndStop("completed");
                }
                else
                {
                    _parent.costinvolved = repaircost;
                    _parent.successful = false;
                    _parent.gotoAndStop("completed");
                } // end else if
            };
        } // end if
    } // end if
}

// [onClipEvent of sprite 1065 in frame 6]
onClipEvent (load)
{
    this.label = "BUY";
}

// [onClipEvent of sprite 1065 in frame 6]
on (release)
{
    missilecosts = _parent.missilecosts;
    missilestobuy = _parent.missileshotsbuying;
    if (_parent.missilecosts > 0)
    {
        if (_parent.missilecosts <= _root.playershipstatus[3][1])
        {
            this.newbaseVars = new XML();
            newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=missileshot&baseid=" + _parent._parent.baseidname + "&shots=-" + missilestobuy);
            newbaseVars.onLoad = function (success)
            {
                loadedvars = String(newbaseVars);
                if (loadedvars == "destroyed")
                {
                    _parent.gotoAndStop("initialize");
                }
                else if (!isNaN(Number(loadedvars)) && loadedvars != "")
                {
                    _parent.costinvolved = missilecosts;
                    _parent.successful = true;
                    _parent._parent.missilerounds = Number(loadedvars);
                    _parent._parent.missileammodisp = "Shots: " + _parent._parent.missilerounds;
                    _parent.gotoAndStop("completed");
                }
                else
                {
                    _parent.costinvolved = missilecosts;
                    _parent.successful = false;
                    _parent.gotoAndStop("completed");
                } // end else if
            };
        } // end if
    } // end if
}

// [Action in Frame 1]
_parent.fundsdisplay.funds = _root.playershipstatus[3][1];
this.onEnterFrame = function ()
{
};

stop ();

// [Action in Frame 2]
costfromnormalgunratio = _root.squadbaseinfo[squadbasetype][2][0];
upgcost = "Cost: ";
maxupgllvl = _root.squadbaseinfo[squadbasetype][2][1];
upgradetolvl = 0;
turretnumber = 1;
maxturrets = _root.squadbaseinfo[squadbasetype][2][2];
currentlevel = _parent.turrets[turretnumber - 1];
lastturretnumber = -1;
gunname = "";
this.onEnterFrame = function ()
{
    if (lastturretnumber != turretnumber)
    {
        turretnumber = Math.round(turretnumber);
        if (isNaN(turretnumber) || turretnumber < 1 || turretnumber > maxturrets)
        {
            turretnumber = "";
        } // end if
        if (turretnumber > 0 && turretnumber < maxupgllvl + 1)
        {
            currentlevel = "Current Level: " + _parent.turrets[turretnumber - 1];
            upgradetolvl = "";
        } // end if
        lastturretnumber = turretnumber;
    } // end if
    if (upgradetolvl != lastupgradetolvl)
    {
        upgradetolvl = Math.round(upgradetolvl);
        currentturretlvl = _parent.turrets[turretnumber - 1];
        if (isNaN(currentturretlvl))
        {
            currentturretlvl = -1;
        } // end if
        if (isNaN(upgradetolvl) || upgradetolvl <= currentturretlvl || upgradetolvl > maxupgllvl)
        {
            upgradetolvl = "";
        } // end if
        if (upgradetolvl > maxupgllvl)
        {
            upgradetolvl = "";
        } // end if
        upgradecost = _root.guntype[upgradetolvl][5] * costfromnormalgunratio - _root.guntype[currentturretlvl][5] * costfromnormalgunratio;
        gunname = _root.guntype[upgradetolvl][6];
        upgcost = "Cost: " + upgradecost;
        lastupgradetolvl = upgradetolvl;
    } // end if
};
stop ();

// [Action in Frame 3]
costfromnormalgunratio = 10;
upgcost = "Cost: ";
maxupgllvl = _root.guntype.length - 1;
upgradetolvl = 0;
turretnumber = 1;
changeturretonumber = 2;
maxturrets = 4;
currentlevel = _parent.turrets[turretnumber - 1];
this.onEnterFrame = function ()
{
    if (lastturretnumber != turretnumber)
    {
        turretnumber = Math.round(turretnumber);
        if (isNaN(turretnumber) || turretnumber < 1 || turretnumber > maxturrets)
        {
            turretnumber = "";
        } // end if
        if (turretnumber > 0 && turretnumber < maxupgllvl + 1)
        {
        } // end if
        lastturretnumber = turretnumber;
        lastchangeturretonumber = "";
    } // end if
    if (changeturretonumber != lastchangeturretonumber)
    {
        changeturretonumber = Math.round(changeturretonumber);
        if (isNaN(changeturretonumber) || changeturretonumber < 1 || changeturretonumber > maxturrets || changeturretonumber == turretnumber)
        {
            changeturretonumber = "";
        } // end if
        if (changeturretonumber > 0 && changeturretonumber < maxupgllvl + 1)
        {
        } // end if
        lastchangeturretonumber = changeturretonumber;
    } // end if
};
stop ();

// [Action in Frame 4]
maxgenlvl = _root.squadbaseinfo[squadbasetype][1][3];
maxlvldisp = "Level " + (maxgenlvl + 1);
currentgenlvl = _parent.shieldgenerator;
changeshieldto = currentgenlvl + 2;
genlvldisp = _root.shieldgenerators[currentgenlvl][4];
costmultiplier = _root.squadbaseinfo[squadbasetype][1][0];
this.onEnterFrame = function ()
{
    if (changeshieldto != lastchangeshieldto)
    {
        if (isNaN(changeshieldto))
        {
            changeshieldto = -1;
        } // end if
        if (changeshieldto - 1 <= currentgenlvl || changeshieldto - 1 > maxgenlvl)
        {
            changeshieldto = "";
        } // end if
        upgradecost = _root.shieldgenerators[changeshieldto - 1][5] * costmultiplier - _root.shieldgenerators[currentgenlvl][5] * costmultiplier;
        shieldcostdisp = "Cost: " + upgradecost;
        lastchangeshieldto = changeshieldto;
    } // end if
};
stop ();

// [Action in Frame 5]
repaircostperstruct = _root.squadbaseinfo[squadbasetype][3][0];
maxstructure = _parent.maxstructure;
currentstructure = _parent.totalstructure;
amouttorepair = maxstructure - currentstructure;
this.onEnterFrame = function ()
{
    if (amouttorepair != lastamouttorepair)
    {
        if (isNaN(amouttorepair))
        {
            amouttorepair = 0;
        } // end if
        if (amouttorepair < 0)
        {
            amouttorepair = 0;
        } // end if
        if (amouttorepair > maxstructure)
        {
            amouttorepair = maxstructure - currentstructure;
        } // end if
        repaircost = amouttorepair * repaircostperstruct;
        repaircostdisp = "Cost: " + repaircost;
        lastamouttorepair = amouttorepair;
    } // end if
};
stop ();

// [Action in Frame 6]
missileshotsleft = _parent.missilerounds;
costpermissile = _root.squadbaseinfo[squadbasetype][4][0];
missileshotsbuying = "";
missilecostdisp = "Cost: ";
this.onEnterFrame = function ()
{
    if (missileshotsbuying != lastmissileshotsbuying)
    {
        if (isNaN(missileshotsbuying))
        {
            missileshotsbuying = 0;
        } // end if
        if (missileshotsbuying < 0)
        {
            missileshotsbuying = 0;
        } // end if
        missilecosts = missileshotsbuying * costpermissile;
        missilecostdisp = "Cost: " + missilecosts;
        lastmissileshotsbuying = missileshotsbuying;
    } // end if
};
stop ();

// [Action in Frame 7]
if (successful == true)
{
    resultdisp = "The Operation was completed Successfully \r";
    if (costinvolved != "NONE")
    {
        _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - costinvolved;
        _parent.fundsdisplay.funds = _root.playershipstatus[3][1];
        _root.savegamescript.saveplayersgame(this);
        resultdisp = resultdisp + (costinvolved + " Funds have been used.");
    } // end if
}
else
{
    resultdisp = "The Operation was NOT completed\r";
    if (costinvolved != "NONE")
    {
        resultdisp = resultdisp + (costinvolved + " Funds were not used.\r");
    } // end if
    resultdisp = resultdisp + "Please Try Again.";
} // end else if
this.onEnterFrame = function ()
{
};
stop ();
