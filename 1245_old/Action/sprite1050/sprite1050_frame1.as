﻿// Action script...

// [Action in Frame 1]
origdamage = damage;
if (Number(_root.playershipstatus[3][0]) == Number(playerid))
{
    isplayers = true;
    damage = Math.round(damage / 3);
} // end if
finishtime = getTimer() + totaltime;
finishsize = Number(finishsize);
this.onEnterFrame = function ()
{
    this._x = xposition - _root.shipcoordinatex;
    this._y = yposition - _root.shipcoordinatey;
    currentime = getTimer();
    if (currentime > finishtime)
    {
        for (jj = 0; jj < _root.aishipshosted.length; jj++)
        {
            xdist = xposition - _root.aishipshosted[jj][1];
            ydist = yposition - _root.aishipshosted[jj][2];
            rangetoai = Math.sqrt(xdist * xdist + ydist * ydist);
            if (rangetoai < radius)
            {
                _root.gamedisplayarea["aiship" + _root.aishipshosted[jj][0]].adddamage(origdamage, user);
            } // end if
        } // end of for
        this.removeMovieClip();
    }
    else
    {
        radius = Math.floor(finishsize - finishsize * ((finishtime - currentime) / totaltime));
        this._height = this._width = radius * 2;
        if (playerbeenhit != true)
        {
            xdist = this._x;
            ydist = this._y;
            rangetoplayer = Math.sqrt(xdist * xdist + ydist * ydist);
            if (rangetoplayer < radius)
            {
                playerbeenhit = true;
                if (isplayers)
                {
                    empendsat = getTimer() + damage;
                    if (_root.playerempend < empendsat)
                    {
                        _root.playerempend = empendsat;
                        _root.isplayeremp = true;
                    } // end if
                }
                else
                {
                    empendsat = getTimer() + damage;
                    if (_root.playerempend < empendsat)
                    {
                        _root.playerempend = empendsat;
                        _root.isplayeremp = true;
                    } // end if
                    if (_root.playerscurrentextrashipno != "capital")
                    {
                        disruptendsat = getTimer() + damage;
                        if (_root.playerdiruptend < disruptendsat)
                        {
                            _root.gamedisplayarea.keyboardscript.disruptplayersengines();
                            _root.playerdiruptend = disruptendsat;
                            _root.isplayerdisrupt = true;
                        } // end if
                    } // end if
                } // end if
            } // end if
        } // end else if
    } // end else if
};
stop ();
