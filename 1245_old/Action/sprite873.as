﻿// Action script...

// [Action in Frame 1]
if (helpmode == "ingame")
{
    gotoAndPlay(3);
} // end if
if (helpmode == "tradegoods1")
{
    gotoAndPlay(4);
} // end if
if (helpmode == "hardware1")
{
    gotoAndPlay(5);
} // end if
if (helpmode == "ship1")
{
    gotoAndPlay(6);
} // end if
if (helpmode == "starbase1")
{
    gotoAndPlay(7);
} // end if
if (helpmode == "mission1")
{
    gotoAndPlay(8);
} // end if
if (helpmode == "extraships1")
{
    gotoAndPlay(9);
} // end if
if (helpmode == "preferences1")
{
    gotoAndPlay(10);
} // end if

// [Action in Frame 2]
gotoAndPlay(1);

// [Action in Frame 3]
helptext = "\t\t\t\t\tIN GAME HELP (F12 to change and close) \r Ship Control\t\t\t\t  \t\t\t\t\t\t\t\t\tRadar\t\t\t\r Up Arrow - Increase Speed \t\t\t\t\t\t\t\t\tWhite = You\t\t\r Down Arrow - Decrease Speed\t\t\t\t\t\t\t\tRed = Enemy Ship \r Left/Right Arrow - Rotate ship \t\t\t\t\t\t\t\tPurple = Star Base \r D (while not typing) -  docks ship \t\t\t\t\t\t\tYellow = Team Member\r Control - Fires Activated Guns   Space (while not typing) - Fire Selected Missile \r   \r Right Menu ( Click button to change setting ) \r Shields  \t\t\t\t\t\t\t\t\t\t\t\t\t\tHardpoints (Guns) \r Click \'On\' button - Charge to full strength \t\t\t\t\tClick on weapon to turn on/off gun \r Click \'1/2\' button - Charge to half strength \t\t\t\t\r Click \'Off\' button - Shields are turned off\t\t\t\t\t\r *Shield drain is based on energy to operate generator \r and how much energy is needed to keep shield strength\r  \r Chat \t\t\t\t\t\t\t\t\t\t\t\t\t\t\tTarget Box \r Enter - Start typing text / Send Text \t\t\t\t\t\tT (while not typing) - Change target \r Home - Enlarges / Shrinks Text Message Area  \t\t\t*Will autoselect first available target\r ";
stop ();

// [Action in Frame 4]
helptext = "\t\t\t\tPurchase Trade Goods HELP (F12 to change and close) \r This is  Where You Can Purchase And Sell Trade Goods Between Bases \r Clicking on the \'Sell\' or \'Buy\' buttons to change the mode \r The Top Right number is how much funds you have\r \rBUYING /SELLING \r Item Column - name of item buying \r Price Column - Cost per unit to buy / sell \r Max Qty Column (while buying)- Maximum Items you can hold, or buy \r You Own - How units you hold of that item  \r Quantity - Enter how many units you wish to purchase / sell for the item on that row \r Click the blue \'BUY\'/\'SELL\' button to purchase / sell the item on that row \r \r EXIT BUTTON \r Click to exit and return to the main starbase screen \r ";
stop ();

// [Action in Frame 5]
helptext = "\t\t\tShip Hardware HELP (F12 to change and close) \r This is where you can refit your ship, click on a menu item on the left\t\rbar to purchase that type of item\t\r \r WEAPONS/GUNS \r To sell a weapon - click on it and drag it into the sell weapon box (bottom left) \r Then you will be asked if you want to sell it, you will receive 1/2 \r of the Guns original cost.  A \'No Weapon\' label replaces the gun. \r  To buy a weapon - you will need one free harpoint. If you do not have one, you will need \r to sell one as described above. Click on the \'BUY\' button in the box of the weapon  \r you wish to purchase \r  To move guns - Simply click and drag a gun from one hardpoint to another.\r The replaced gun will automatically go to the new open spot.  \r  \r  Missiles \r Click the Missile Bank to fill, and then buy your missiles \r \r SHEILD GENERATORS / ENERGY GENERATORS / ENERGY CAPACITORS \r  To buy one - Click \'BUY\' below the desired item, the price includes the sale of the old one \r \r ";
stop ();

// [Action in Frame 6]
helptext = "\t\tPurchase New Ship HELP (F12 to change and close) \r This is  Where You Can Purchase a New Ship\r \r The Top Right number is how much funds you have\r \rBUYING A NEW SHIP \r The ships\' name appears at the top with its cost and stats below it \r  Cost - The cost include the sale of the old ones and all items on it  \r If the Cost says \'OWNED\', this is the ship you currently own \r Purchase Ship Button - Click this button to puchase your new ship \r **WARNING** When your purchase a new ship, all of its hardware (guns, generators,etc.)\r will be changed into the lowest form that ship can use. It will be equipped with all \r of its hardpoints filled with the basic weapon \r \r EXIT BUTTON \r Click to exit and return to the main starbase screen \r ";
stop ();

// [Action in Frame 7]
helptext = "\t\t\tStarbase HELP (F12 to change and close) \rThis is Where You Can Purchase a New Ship, Upgrade your ship, and Buy Trage Goods  \r Simply Click on the icon for the desired operation\r \rBUY A NEW SHIP \r Allows you to purchase a new ship \r  \rPURCHASE TRADE GOODS \r Allows you to purchase and sell trade goods \r  \rSHIP HARDWARE \r This is where you can upgrade your ship, and set up your gun positions \r  \rEXIT BUTTON \rExits the Starbase and enter you into the game \r**You must wait approx. 5 seconds from entering the Starbase till you can exit \r \r Chat \t\t\t\t\t\t\t\t\t\t\t\t\t\t\tOnline Userlist - current players \r Enter - Start typing text / Send Text \t\t\t\t\t\tPage up - scrolls name list up \r Home - Enlarges / Shrinks Text Message Area  \t\t\tPage down - scrolls name list down  \r ";
stop ();

// [Action in Frame 8]
helptext = "\t\t\tMission HELP (F12 to change and close) \rThis is Where You Can Select a New Mission, Or Change To a New One  \r Simply Click on the select/change button to change accept/change your current mission\r \rRECON \r Stay within a certain range to for a set time limit to get the reward \r  \rPATROL \r You must be in the area, for the time required, with no Enemy Ships Present. \r If Enemy ships are present, you must destoy then or scare them off and then wait \rout the timer to complete the mission \r \r";
stop ();

// [Action in Frame 9]
helptext = "Hangar HELP (F12 to change and close) \r This is  Where You Change, Buy, or Sell your Ship\r \rCHANGING SHIPS \r Click the Change ship button to set this as your current ship  \r  The Ship Labeled Current is your current one.   \r \r BUYING A NEW SHIP \r If there is a Blank spot, clicking Purchase new ship will  \r  take you into a screen to buy a new ship.   \r \r SELL A SHIP \r Click to sell your ship, You will be offered to sell your ship \r for 1/2 of the ships worth, and 1/4 of the worth of the components. \r Click Yes to sell it, No to cancel. \r \r EXIT BUTTON \r Click to exit and return to the main starbase screen \r ";
stop ();

// [Action in Frame 10]
helptext = "Preferences HELP (F12 to change and close) \r This is  Where You Change, Change your team, Place Bounty on another player \r \t\t\t\t\t\t\tTurn Off/On the Sound, And Set the # of Stars \r\rSTARS \r Stars can slow down your system. Setting the number to 0 can  \r  help your system run faster.   \r \r Changing teams \r Teams under 10, are public, teams over 10 are private  \r  N/A means that you are not on any team \r  \rSOUND - Turning off sound can extremely help the speed of your system \r ";
stop ();

// [Action in Frame 22]
text = "";
stop ();
