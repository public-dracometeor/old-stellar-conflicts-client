﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "EXIT";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent._parent.gotoAndStop("squadbasemenu");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.changeleader._visible = true;
    _parent.changeleader.commandchanging = "Change Second To:";
    _parent.changeleader.changeleadnameto = "ENTERNAME";
    _parent.changeleader.changeleadernamebut.leaderposition = "SECOND";
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.changeleader._visible = true;
    _parent.changeleader.commandchanging = "Change Third To:";
    _parent.changeleader.changeleadnameto = "ENTERNAME";
    _parent.changeleader.changeleadernamebut.leaderposition = "THIRD";
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.changemessage._x = -360;
    _parent.changemessage._y = 5;
    _parent.changemessage._visible = true;
    _parent.changemessage.newsquadmessage = _parent.squadmessage;
    this._visible = false;
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "TREATIES";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.gotoAndStop("treaties");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("allie2");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("allie3");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "MAIN";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.gotoAndStop(1);
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("enemy2");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("enemy3");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("allie1");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("enemy1");
}

// [onClipEvent of sprite 1065 in frame 2]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 2]
on (release)
{
    _parent.change("basestatus");
}

// [Action in Frame 1]
function initializesetup()
{
    secondin._visible = false;
    thirdin._visible = false;
    this.alterations._visible = false;
    this.changemessagebut._visible = false;
    this.changeleader._visible = false;
    this.changemessage._visible = false;
    playersname = _root.playershipstatus[3][2];
    squadmemberlist = "Loading";
    creatorname = "Loading";
    secondname = "Loading";
    thirdname = "Loading";
    noofmembers = "N/A";
    currentpass = "Loading";
    squadmembers = new Array();
    squadmessage = "Loading";
    newsquadVars = new XML();
    newsquadVars.load(_root.pathtoaccounts + "squadscommand.php?mode=squadinfo&sname=" + _root.playershipstatus[5][10] + "&spass=" + _root.playershipstatus[5][13]);
    newsquadVars.onLoad = function (success)
    {
        loadedinfo = String(newsquadVars);
        newinfo = loadedinfo.split("~");
        noofsquad = 0;
        for (i = 0; i < newinfo.length - 1; i++)
        {
            currentthread = newinfo[i].split("`");
            if (currentthread[0] == "P")
            {
                squadmembers[noofsquad] = currentthread[1];
                ++noofsquad;
            } // end if
            if (currentthread[0] == "2ND")
            {
                secondname = currentthread[1];
                if (secondname.length < 1)
                {
                    secondname = "NONE";
                } // end if
            } // end if
            if (currentthread[0] == "3RD")
            {
                thirdname = currentthread[1];
                if (thirdname.length < 1)
                {
                    thirdname = "NONE";
                } // end if
            } // end if
            if (currentthread[0] == "CR")
            {
                creatorname = currentthread[1];
            } // end if
            if (currentthread[0] == "PASS")
            {
                squadpassword = currentthread[1];
            } // end if
            if (currentthread[0] == "MESS")
            {
                squadmessage = currentthread[1];
            } // end if
        } // end of for
        if (playersname == creatorname)
        {
            setvisiblescreator();
        } // end if
        if (playersname == secondname || playersname == thirdname || playersname == creatorname)
        {
            setvisiblesleaders();
        } // end if
        displaysquadlist();
    };
} // End of the function
function setvisiblescreator()
{
    secondin._visible = true;
    thirdin._visible = true;
} // End of the function
function setvisiblesleaders()
{
    this.alterations._visible = true;
    this.alterations.currentpass = squadpassword;
    this.changemessagebut._visible = true;
} // End of the function
function displaysquadlist()
{
    squadmemberlist = "";
    for (jj = 0; jj < squadmembers.length; jj++)
    {
        squadmemberlist = squadmemberlist + (jj + 1 + ": " + squadmembers[jj] + "\r");
    } // end of for
    noofmembers = squadmembers.length;
} // End of the function
initializesetup();
stop ();

// [Action in Frame 2]
function initializesetup()
{
    playersname = _root.playershipstatus[3][2];
    allie1 = "Loading";
    allie2 = "Loading";
    allie3 = "Loading";
    enemy1 = "Loading";
    enemy2 = "Loading";
    enemy3 = "Loading";
    basestatus = "Loading";
    commandchanging = "Select";
    this.changer._visible = false;
    newsquadVarss = new XML();
    newsquadVarss.load(_root.pathtoaccounts + "squadscommand.php?mode=treaties&sname=" + _root.playershipstatus[5][10]);
    newsquadVarss.onLoad = function (success)
    {
        loadedinfo = String(newsquadVarss);
        processdata(loadedinfo);
    };
} // End of the function
function processdata(loadedinfo)
{
    newinfo = loadedinfo.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentthread = newinfo[i].split("`");
        if (currentthread[0] == "AL1")
        {
            allie1 = currentthread[1];
            if (allie1.length < 1)
            {
                allie1 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "AL2")
        {
            allie2 = currentthread[1];
            if (allie2.length < 1)
            {
                allie2 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "AL3")
        {
            allie3 = currentthread[1];
            if (allie3.length < 1)
            {
                allie3 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "EN1")
        {
            enemy1 = currentthread[1];
            if (enemy1.length < 1)
            {
                enemy1 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "EN2")
        {
            enemy2 = currentthread[1];
            if (enemy2.length < 1)
            {
                enemy2 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "EN3")
        {
            enemy3 = currentthread[1];
            if (enemy3.length < 1)
            {
                enemy3 = "NONE";
            } // end if
        } // end if
        if (currentthread[0] == "BST")
        {
            basestatus = currentthread[1];
            if (basestatus.length < 1)
            {
                basestatus = "HOSTILE";
            } // end if
        } // end if
    } // end of for
} // End of the function
function change(changing)
{
    if (changing == "allie1")
    {
        chaningto = "ALLIE 1";
    } // end if
    if (changing == "allie2")
    {
        chaningto = "ALLIE 2";
    } // end if
    if (changing == "allie3")
    {
        chaningto = "ALLIE 3";
    } // end if
    if (changing == "enemy1")
    {
        chaningto = "ENEMY 1";
    } // end if
    if (changing == "enemy2")
    {
        chaningto = "ENEMY 2";
    } // end if
    if (changing == "enemy3")
    {
        chaningto = "ENEMY 3";
    } // end if
    if (changing == "enemy3")
    {
        chaningto = "ENEMY 3";
    } // end if
    if (changing == "basestatus")
    {
        chaningto = "Base Setting";
    } // end if
    this.changer._visible = true;
    this.changer.commandchanging = "Changing " + chaningto + " To";
    this.changer.changinto = "";
    this.changer.field = changing;
} // End of the function
function changething(field, newvar)
{
    field = field.toUpperCase();
    newvar = newvar.toUpperCase();
    newsquadVarss = new XML();
    newsquadVarss.load(_root.pathtoaccounts + "squadscommand.php?mode=treatieschange&sname=" + _root.playershipstatus[5][10] + "&varchan=" + field + "&newvar=" + newvar + "&spass=" + _root.playershipstatus[5][13]);
    newsquadVarss.onLoad = function (success)
    {
        loadedinfo = String(newsquadVarss);
        processdata(loadedinfo);
    };
} // End of the function
initializesetup();
stop ();
