﻿// Action script...

// [Action in Frame 1]
function fireanaiturretsbullet()
{
    xfireposition = _parent._x + this._x;
    yfireposition = _parent._y + this._y;
    velocity = gunshotvelocity;
    rotationtoplayer = 360 - Math.atan2(xdist, ydist) / mathconstant;
    if (rotationtoplayer > 360)
    {
        rotationtoplayer = rotationtoplayer - 360;
    } // end if
    this._rotation = rotationtoplayer;
    if (_root.othergunfire.length < 1)
    {
        _root.othergunfire = new Array();
    } // end if
    ++_root.currentotherplayshot;
    if (_root.currentotherplayshot >= 500)
    {
        _root.currentotherplayshot = 1;
    } // end if
    lastvar = _root.othergunfire.length;
    ++_root.othergunfirecount;
    _root.othergunfire[lastvar] = new Array();
    _root.othergunfire[lastvar][8] = _root.currentotherplayshot;
    shotinformation = shotinformation + "GF`";
    shotinformation = shotinformation + (_root.othergunfire[lastvar][0] = "ai" + shipname);
    shotinformation = shotinformation + "`";
    shotinformation = shotinformation + (_root.othergunfire[lastvar][1] = Math.round(xfireposition));
    shotinformation = shotinformation + "`";
    shotinformation = shotinformation + (_root.othergunfire[lastvar][2] = Math.round(yfireposition));
    shotinformation = shotinformation + "`";
    shotinformation = shotinformation + velocity;
    shotinformation = shotinformation + "`";
    shotinformation = shotinformation + (relativefacing = _root.othergunfire[lastvar][6] = rotationtoplayer);
    shotinformation = shotinformation + "`";
    shotinformation = shotinformation + (_root.othergunfire[lastvar][7] = shottype);
    shotinformation = shotinformation + "`";
    shotinformation = shotinformation + (_root.othergunfire[lastvar][9] = currentgunfireshot);
    shotinformation = shotinformation + "`~";
    _root.othergunfire[lastvar][5] = _root.guntype[shottype][1] * 1000 + _root.curenttime;
    _root.othergunfire[lastvar][10] = "AI";
    movementofanobjectwiththrust();
    _root.othergunfire[lastvar][3] = xmovement;
    _root.othergunfire[lastvar][4] = ymovement;
    ++currentgunfireshot;
    if (currentgunfireshot > 500)
    {
        currentgunfireshot = 0;
    } // end if
    _root.gamedisplayarea.attachMovie("guntype" + _root.othergunfire[lastvar][7] + "fire", "othergunfire" + _root.othergunfire[lastvar][8], 3500 + _root.othergunfire[lastvar][8]);
    setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _rotation, _root.othergunfire[lastvar][6]);
    setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _x, _root.othergunfire[lastvar][1] - _root.shipcoordinatex);
    setProperty("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8], _y, _root.othergunfire[lastvar][2] - _root.shipcoordinatey);
    set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".xmovement", _root.othergunfire[lastvar][3]);
    set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".ymovement", _root.othergunfire[lastvar][4]);
    set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".xposition", _root.othergunfire[lastvar][1]);
    set("_root.gamedisplayarea.othergunfire" + _root.othergunfire[lastvar][8] + ".yposition", _root.othergunfire[lastvar][2]);
    _root.othergunfire[lastvar][11] = _root.gamedisplayarea["othergunfire" + _root.othergunfire[lastvar][8]]._width / 2;
    _root.othergunfire[lastvar][12] = _root.gamedisplayarea["othergunfire" + _root.othergunfire[lastvar][8]]._height / 2;
    if (_root.soundvolume == "on")
    {
        _root["guntype" + _root.othergunfire[lastvar][7] + "sound"].start();
    } // end if
} // End of the function
function firingbulletstartlocation()
{
    rotationangle = initialangleforgun + _root.playershipfacing;
    xfirestart = -hardpointfromcenter * Math.sin(0.017453 * rotationangle);
    yfirestart = hardpointfromcenter * Math.cos(0.017453 * rotationangle);
} // End of the function
function movementofanobjectwiththrust()
{
    this.relativefacing = Math.round(this.relativefacing);
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    }
    else if (velocity != 0)
    {
        this.relativefacing = Math.round(this.relativefacing);
        ymovement = -velocity * _root.cosines[this.relativefacing];
        xmovement = velocity * _root.sines[this.relativefacing];
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function movementofaturretshot()
{
    shiprotation = Math.round(_parent._rotation);
    maxshotvelocityforshot = gunshotvelocity;
    angleofshotfromship = shiprotation - rotationtotarget;
    _root.tesstt = offsetangle;
    shotsanglefromship = offsetangle + angleofshotfromship;
    angletoshootbullet = Math.round(shiprotation - shotsanglefromship);
    turretrotation = angletoshootbullet;
    if (turretrotation > 360)
    {
        turretrotation = turretrotation - 360;
    }
    else if (turretrotation < 0)
    {
        turretrotation = turretrotation + 360;
    } // end else if
    shotsanglefromship = Math.round(shotsanglefromship);
    if (shotsanglefromship > 360)
    {
        shotsanglefromship = shotsanglefromship - 360;
    }
    else if (shotsanglefromship < 0)
    {
        shotsanglefromship = shotsanglefromship + 360;
    } // end else if
    shipsmomentum = _root.playershipvelocity * _root.cosines[shotsanglefromship];
    velocity = gunshotvelocity + shipsmomentum;
    relativefacing = turretrotation;
    movementofanobjectwiththrust();
} // End of the function
reloaddelay = _root.guntype[shottype][2] * 1000;
timetillnextshot = _root.curenttime + reloaddelay;
gunshotlifetime = _root.guntype[shottype][1] * 1000;
gunshotvelocity = _root.guntype[shottype][0];
energyusedpershot = _root.guntype[shottype][3];
rangetofireashot = gunshotvelocity * (gunshotlifetime / 1000) * 3 / 2;
mathconstant = 0.017453;
timecheckinterval = 700;
currenttimecheck = getTimer();
this.onEnterFrame = function ()
{
    currenttime = getTimer();
    if (currenttime > currenttimecheck)
    {
        currenttimecheck = currenttimecheck + timecheckinterval;
        xdist = _parent._x + this._x - _root.shipcoordinatex;
        ydist = _parent._y + this._y - _root.shipcoordinatey;
        rangetoplayer = Math.sqrt(xdist * xdist + ydist * ydist);
        if (rangetoplayer < rangetofireashot)
        {
            if (timetillnextshot < currenttime)
            {
                timetillnextshot = currenttime + reloaddelay;
                fireanaiturretsbullet();
            } // end if
        } // end if
    } // end if
};
stop ();
