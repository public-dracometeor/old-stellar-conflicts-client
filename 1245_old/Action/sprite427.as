﻿// Action script...

// [Action in Frame 1]
function displayinfo()
{
    playershiptype = _root.playershipstatus[5][0];
    titledisp = "No. of Specials (" + _root.playershipstatus[11][1].length + "/" + _root.shiptype[playershiptype][3][7] + ")";
    yspacing = 0;
    for (i = 0; i < _root.playershipstatus[11][1].length; i++)
    {
        this.attachMovie("hardwarescreenspecialsinfoitem", "hardwarescreenspecialsinfoitem" + i, 10 + i);
        this["hardwarescreenspecialsinfoitem" + i]._x = -73;
        this["hardwarescreenspecialsinfoitem" + i]._y = -40 + yspacing;
        this["hardwarescreenspecialsinfoitem" + i].specialitemlocation = i;
        yspacing = yspacing + (this["hardwarescreenspecialsinfoitem" + i]._height + 4);
    } // end of for
} // End of the function
displayinfo();
