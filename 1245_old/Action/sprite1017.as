﻿// Action script...

// [Action in Frame 1]
function sendouthatshipiishere()
{
    information = "NPC`ESCORT`" + npcnumber + "`" + shiptype + "`" + startx + "`" + starty + "`" + velocity + "`" + endx + "`" + endy + "`" + systemtime;
    _root.mysocket.send(information);
} // End of the function
function movementofanobjectwiththrust()
{
    relativefacing = this.rotation;
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    } // end if
    if (velocity != 0)
    {
        this.relativefacing = Math.round(this.relativefacing);
        ymovement = Math.round(-velocity * _root.cosines[this.relativefacing]);
        xmovement = Math.round(velocity * _root.sines[this.relativefacing]);
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function deathinfosend()
{
    _root.gamedisplayarea.attachMovie("aimessage", "aimessage", 94324);
    message = "AHHHHHHHHH";
    _root.gamedisplayarea.aimessage.aimessage = message;
    _root.gamedisplayarea.aimessage.ainame = shipname;
    tw = 0;
} // End of the function
function docktheship()
{
    if (playerthehost)
    {
        ainame = "AISHIP";
        message = "Thanks for the escort!";
        _root.postaimessage(message, ainame);
        removeMovieClip (this);
    }
    else
    {
        removeMovieClip (this);
    } // end else if
} // End of the function
function deathinfosend()
{
    datatosend = "AI`death`" + shipid + "`" + String(shipbounty) + "`" + Math.round(shipbounty * _root.playershipstatus[5][6]) + "`" + shipbounty + "~";
    _root.mysocket.send(datatosend);
    for (i = 0; i < _root.aishipshosted.length; i++)
    {
        if (shipid == _root.aishipshosted[i][0])
        {
            _root.aishipshosted.splice(i, 1);
            i = 999999;
        } // end if
    } // end of for
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function

// [Action in Frame 2]
function shipmovement()
{
    currenttime = getTimer();
    velocity = currentvelocity * ((currenttime - lastmovementtime) / 1000);
    movementofanobjectwiththrust();
    currentx = currentx + xmovement;
    currenty = currenty + ymovement;
    lastmovementtime = currenttime;
} // End of the function
function checkifreachdestination()
{
    currentxrangeleft = currentx - endx;
    currentyrangeleft = currenty - endy;
    if (currentxrangeleft > lastxrangeleft)
    {
        if (currentyrangeleft > lastyrangeleft)
        {
            docktheship();
        } // end if
    } // end if
    lastxrangeleft = currentxrangeleft;
    lastyrangeleft = currentyrangeleft;
} // End of the function
function proccessshipstuff()
{
    curenttime = getTimer();
    ++currentframeforhittest;
    if (currentshipstatus == "alive" && currentframeforhittest > hitteestintervals)
    {
        currentframeforhittest = 0;
        shieldtimechangeratio = (curenttime - lastshieldtime) / 1000;
        lastshieldtime = curenttime;
        if (shipemped)
        {
            currentshipshield = 0;
        }
        else if (currentshipshield < maxshipshield)
        {
            currentshipshield = currentshipshield + shipshieldreplenish * shieldtimechangeratio;
            if (currentshipshield > maxshipshield)
            {
                currentshipshield = maxshipshield;
            } // end if
        } // end else if
        playershipshots();
        otherplayershipshots();
    } // end if
    if (currentshipstatus == "dieing")
    {
        deathinfosend();
        currentshipstatus = "dead";
        this.ship.removeMovieClip();
        this.attachMovie("shiptype" + shiptype + "shield", "shipshield", 200);
        this.shipshield._alpha = 0;
        this.attachMovie("shipdeath", "shipdeath", 1);
    } // end if
} // End of the function
function playershipshots()
{
    numberofplayershots = _root.playershotsfired.length;
    if (numberofplayershots > 0)
    {
        halfshipswidth = this.ship._width / 2;
        halfshipsheight = this.ship._height / 2;
        this.relativex = this._x;
        this.relativey = this._y;
    } // end if
    for (i = 0; i < numberofplayershots; i++)
    {
        bulletshotid = _root.playershotsfired[i][0];
        bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
        bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
        if (bulletsx != null)
        {
            if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
            {
                if (_root.playershotsfired[i][13] == "GUNS")
                {
                    currentshipshield = currentshipshield - _root.guntype[_root.playershotsfired[i][6]][4];
                }
                else if (_root.playershotsfired[i][13] == "MISSILE")
                {
                    missiletype = _root.playershotsfired[i][6];
                    currentshipshield = currentshipshield - _root.missile[missiletype][4];
                    if (_root.missile[missiletype][8] == "EMP")
                    {
                        timetillempends = getTimer() + _root.missile[missiletype][9];
                        shipemped = true;
                    } // end if
                    if (_root.missile[missiletype][8] == "DISRUPTER")
                    {
                        timedisruptedends = getTimer() + _root.missile[missiletype][9];
                        shipdisrupted = true;
                    } // end if
                } // end else if
                if (currentshipshield < 0)
                {
                    shiphealth = shiphealth + currentshipshield;
                    currentshipshield = 0;
                } // end if
                if (shiphealth <= 0)
                {
                    currentshipstatus = "dieing";
                    i = 999999;
                    shipkillerid = currenthostplayer;
                } // end if
                removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                lastshooter[2] = lastshooter[1];
                lastshooter[1] = lastshooter[0];
                lastshooter[0] = currenthostplayer;
                _root.playershotsfired[i][5] = 0;
                this.ship.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
                this.ship.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
            } // end if
        } // end if
    } // end of for
} // End of the function
function otherplayershipshots()
{
    for (cc = 0; cc < _root.othergunfire.length; cc++)
    {
        if (this.hitTest("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]) == true && currentshipstatus == "alive")
        {
            playerwhoshothitid = _root.othergunfire[cc][0];
            removeMovieClip ("_root.gamedisplayarea.othergunfire" + _root.othergunfire[cc][8]);
            _root.bullethitinformation = _root.bullethitinformation + ("GH`AI" + shipid + "`" + _root.othergunfire[cc][0] + "`" + _root.othergunfire[cc][9] + "~");
            gunshothitplayertype = _root.othergunfire[cc][7];
            _root.othergunfire.splice(cc, 1);
            --cc;
            currentshipshield = currentshipshield - _root.guntype[gunshothitplayertype][4];
            if (currentshipshield < 0)
            {
                shiphealth = shiphealth + currentshipshield;
                currentshipshield = 0;
            } // end if
            if (shiphealth <= 0 && currentshipstatus == "alive")
            {
                currentshipstatus = "dieing";
                shipkillerid = playerwhoshothitid;
                continue;
            } // end if
            this.attachMovie("shiptype" + shiptype + "shield", "shipshield", 2);
            this.shipshield._alpha = currentshipshield / (maxshipshield * 0.750000) * 100;
        } // end if
    } // end of for
} // End of the function
lastrangetodock = 9999999999999.000000;
currentvelocity = Number(currentvelocity);
currenttime = getTimer();
velocity = currentvelocity * ((currenttime - starttime) / 1000);
lastmovementtime = currenttime;
rotation = shiprotation;
movementofanobjectwiththrust();
currentx = startx + xmovement;
currenty = starty + ymovement;
lastxrangeleft = currentx - endx;
lastyrangeleft = currenty - endy;
currentframeforhittest = 0;
hitteestintervals = 3;
if (playerthehost == true)
{
    sendouthatshipiishere();
    this.onEnterFrame = function ()
    {
        shipmovement();
        checkifreachdestination();
        proccessshipstuff();
    };
} // end if
if (otherplayerthehost == true)
{
    this.onEnterFrame = function ()
    {
        shipmovement();
        checkifreachdestination();
    };
} // end if
stop ();
