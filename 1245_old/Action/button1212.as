﻿// Action script...

on (release)
{
    lastbankselected = _root.playershipstatus[10];
    _root.playershipstatus[10] = _root.playershipstatus[10] + 1;
    bankno = _root.playershipstatus[10];
    if (_root.playershipstatus[10] > _root.playershipstatus[7].length - 1)
    {
        _root.playershipstatus[10] = 0;
        bankno = _root.playershipstatus[10];
    } // end if
    this.missilebank = "Missile Bank " + bankno;
    if (_root.playershipstatus[10] != lastbankselected)
    {
        missilesfound = false;
        startnumber = _root.missile.length;
        totalmisisletypes = _root.missile.length;
        for (i = startnumber + 1; i != startnumber; i++)
        {
            if (i >= totalmisisletypes)
            {
                i = -1;
                continue;
            } // end if
            if (_root.playershipstatus[7][bankno][4][i] > 0)
            {
                _root.playershipstatus[7][bankno][0] = i;
                missilename = _root.missile[i][7];
                missileqty = "Qty: " + _root.playershipstatus[7][bankno][4][i];
                _root.playershipstatus[7][bankno][0] = i;
                missilesfound = true;
                break;
            } // end if
        } // end of for
        if (missilesfound != true)
        {
            i = 0;
            _root.playershipstatus[7][bankno][0] = i;
            missilename = _root.missile[i][7];
            missileqty = "Qty: " + _root.playershipstatus[7][bankno][4][i];
            _root.playershipstatus[7][bankno][0] = i;
        } // end if
    } // end if
}
