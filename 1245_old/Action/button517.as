﻿// Action script...

on (release)
{
    _root.createEmptyMovieClip("blanker", 9999999);
    removeMovieClip ("ingamemap");
    if (_root.mainscreenstarbase.shopselection == "ship")
    {
        _root.mainscreenstarbase.shopselection = "extraships";
        _root.attachMovie("playerextraships", "playerextraships", 10);
    }
    else if (_root.mainscreenstarbase.shopselection == "hardware")
    {
        if (_root.playerscurrentextrashipno != "capital")
        {
            _root.savecurrenttoextraship(_root.playerscurrentextrashipno);
        } // end if
        removeMovieClip (_parent._parent);
        _root.mainscreenstarbase.play();
    }
    else
    {
        removeMovieClip (_parent._parent);
        _root.mainscreenstarbase.play();
    } // end else if
}
