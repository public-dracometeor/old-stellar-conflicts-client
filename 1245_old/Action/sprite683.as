﻿// Action script...

// [onClipEvent of sprite 682 in frame 1]
onClipEvent (load)
{
    removeMovieClip ("_root.starbasehardwarebutton");
    removeMovieClip ("_root.exitstarbasebutton");
    removeMovieClip ("_root.purchaseshipbutton");
    removeMovieClip ("_root.purchasetradegoodsbutton");
    this.attachMovie("purchasetradegoodsselectionbutton", "buybutton", 10);
    setProperty("buybutton", _y, 100);
    setProperty("buybutton", _x, 10);
    this.buybutton.actiontype = "Buy";
    this.attachMovie("purchasetradegoodsselectionbutton", "sellbutton", 11);
    setProperty("sellbutton", _y, 150);
    setProperty("sellbutton", _x, 10);
    this.sellbutton.actiontype = "Sell";
    this.attachMovie("shopexitbutton", "exitbutton", 12);
    setProperty("exitbutton", _y, 400);
    setProperty("exitbutton", _x, 10);
    this.attachMovie("starbasefundsdisplay", "funds", 121);
    this.funds._y = 15;
    this.funds._x = _root.gameareawidth - 100;
    currentitemdisplay = "Buy";
}

// [onClipEvent of sprite 682 in frame 1]
onClipEvent (enterFrame)
{
    itemspacing = 50;
    sellingyindent = 0;
    buyingyindent = 0;
    itemsx = 100;
    topitemy = 100;
    sellingyindent = topitemy;
    this._x = 0;
    this._y = 0;
    if (currentitemdisplay != null)
    {
        if (currentitemdisplay == "Buy")
        {
            setProperty("sellbutton", _alpha, "50");
            setProperty("buybutton", _alpha, "100");
        } // end if
        if (currentitemdisplay == "Sell")
        {
            setProperty("sellbutton", _alpha, "100");
            setProperty("buybutton", _alpha, "50");
        } // end if
        for (i = 0; i < _root.starbasetradeitem.length; i++)
        {
            removeMovieClip ("tradegooditem" + i);
        } // end of for
    } // end if
    for (i = 0; i < _root.starbasetradeitem.length; i++)
    {
        if (currentitemdisplay == "Buy")
        {
            this.attachMovie("purchasegoodsbuyheader", "purchasegoodsbuyheader", 50);
            setProperty("purchasegoodsbuyheader", _x, itemsx);
            setProperty("purchasegoodsbuyheader", _y, topitemy - itemspacing);
            if (_root.starbasetradeitem[i][2] == "selling")
            {
                this.attachMovie("tradegooditemlisting", "tradegooditem" + i, 100 + i);
                itemtype = _root.starbasetradeitem[i][0];
                set("tradegooditem" + i + ".itemtype", itemtype);
                set("tradegooditem" + i + ".itemname", _root.tradegoods[itemtype][0]);
                set("tradegooditem" + i + ".itemprice", _root.starbasetradeitem[i][1]);
                set("tradegooditem" + i + ".quantity", "0");
                set("tradegooditem" + i + ".functiontodo", "Buy");
                setProperty("tradegooditem" + i, _y, sellingyindent);
                setProperty("tradegooditem" + i, _x, itemsx);
                sellingyindent = sellingyindent + itemspacing;
            } // end if
            currentproccees = currentitemdisplay;
        } // end if
        if (currentitemdisplay == "Sell")
        {
            this.attachMovie("purchasegoodssellheader", "purchasegoodssellheader", 50);
            setProperty("purchasegoodssellheader", _x, itemsx);
            setProperty("purchasegoodssellheader", _y, topitemy - itemspacing);
            if (_root.starbasetradeitem[i][2] == "buying")
            {
                this.attachMovie("tradegooditemlisting", "tradegooditem" + i, 100 + i);
                itemtype = _root.starbasetradeitem[i][0];
                set("tradegooditem" + i + ".itemtype", itemtype);
                set("tradegooditem" + i + ".itemname", _root.tradegoods[itemtype][0]);
                set("tradegooditem" + i + ".itemprice", _root.starbasetradeitem[i][1]);
                set("tradegooditem" + i + ".quantity", "0");
                set("tradegooditem" + i + ".functiontodo", "Sell");
                setProperty("tradegooditem" + i, _y, sellingyindent);
                setProperty("tradegooditem" + i, _x, itemsx);
                sellingyindent = sellingyindent + itemspacing;
            } // end if
            currentproccees = currentitemdisplay;
        } // end if
    } // end of for
    currentitemdisplay = null;
}

// [onClipEvent of sprite 682 in frame 1]
onClipEvent (enterFrame)
{
    _parent.funds = _root.playershipstatus[3][1];
    maxcargo = _root.shiptype[_root.playershipstatus[5][0]][4];
    for (i = 0; i < _root.tradegoods.length; i++)
    {
        maxcargo = maxcargo - _root.playershipstatus[4][1][i];
    } // end of for
    for (i = 0; i < _root.starbasetradeitem.length; i++)
    {
        itemtype = _root.starbasetradeitem[i][0];
        if (_root.starbasetradeitem[i][2] == "selling" && currentproccees != "Sell")
        {
            itemtype = _root.starbasetradeitem[i][0];
            maxpurchaseprice = maxcargo * _root.starbasetradeitem[i][1];
            if (maxpurchaseprice <= _root.playershipstatus[3][1])
            {
                maxpurchaseofitems = maxcargo;
            }
            else
            {
                maxpurchaseofitems = Math.floor(_root.playershipstatus[3][1] / _root.starbasetradeitem[i][1]);
            } // end else if
            set("tradegooditem" + i + ".maxquantity", maxpurchaseofitems);
            set("tradegooditem" + i + ".itemowned", _root.playershipstatus[4][1][itemtype]);
        } // end if
        if (_root.starbasetradeitem[i][2] == "buying" && currentproccees == "Sell")
        {
            playerhasunits = _root.playershipstatus[4][1][itemtype];
            set("tradegooditem" + i + ".maxquantity", "");
            set("tradegooditem" + i + ".itemowned", _root.playershipstatus[4][1][itemtype]);
        } // end if
    } // end of for
}

// [Action in Frame 1]
stop ();
