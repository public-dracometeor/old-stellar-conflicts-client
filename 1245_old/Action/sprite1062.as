﻿// Action script...

// [Action in Frame 1]
this.onEnterFrame = function ()
{
    if (lastitemtype != itemtype)
    {
        this.attachMovie(itemtype, "shippicture", 1);
        if (this.shippicture._height > 60 || this.shippicture._width > 60)
        {
            if (this.shippicture._height > this.shippicture._width)
            {
                change = this.shippicture._height - 60;
                this.shippicture._height = this.shippicture._height - change;
                this.shippicture._width = this.shippicture._width - change;
            }
            else
            {
                change = this.shippicture._width - 60;
                this.shippicture._height = this.shippicture._height - change;
                this.shippicture._width = this.shippicture._width - change;
            } // end if
        } // end else if
        shippicture._x = 0;
        shippicture._y = -95;
        lastitemtype = itemtype;
    } // end if
};
