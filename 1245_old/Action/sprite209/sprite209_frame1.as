﻿// Action script...

// [Action in Frame 1]
function func_takeonothershots(otherplayerdamage)
{
    currentshieldstr = currentshieldstr - otherplayerdamage;
    if (currentshieldstr < 0)
    {
        currentshieldstr = 0;
    } // end if
    this.shieldimage.play();
    this.shieldimage._alpha = currentshieldstr / (maxshield * 0.750000) * 100;
} // End of the function
function playershittest()
{
    didplayerhitbase = false;
    numberofplayershots = _root.playershotsfired.length;
    if (numberofplayershots > 0)
    {
        halfshipswidth = 40;
        halfshipsheight = 40;
        this.relativex = this._x - _root.shipcoordinatex;
        this.relativey = this._y - _root.shipcoordinatey;
    } // end if
    for (i = 0; i < numberofplayershots; i++)
    {
        bulletshotid = _root.playershotsfired[i][0];
        bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
        bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
        if (bulletsx != null)
        {
            if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
            {
                didplayerhitbase = true;
                if (isbasehostile == false)
                {
                    --hitstillhostile;
                    if (hitstillhostile <= 0)
                    {
                        isbasehostile = true;
                        setupturrets();
                        message = "Friendly or not, You hit me enough times!!";
                        _root.func_messangercom(message, "SQUADBASE" + squadbasetype, "BEGIN");
                    } // end if
                } // end if
                if (_root.playershotsfired[i][13] == "GUNS")
                {
                    currentshieldstr = currentshieldstr - _root.guntype[_root.playershotsfired[i][6]][4];
                    _root.gunfireinformation = _root.gunfireinformation + ("GH`TB`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[i][0] + "`" + baseid + "~");
                }
                else if (_root.playershotsfired[i][13] == "MISSILE")
                {
                    currentshieldstr = currentshieldstr - _root.missile[_root.playershotsfired[i][6]][4];
                    _root.gunfireinformation = _root.gunfireinformation + ("MH`TB`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[i][0] + "`" + baseid + "~");
                } // end else if
                if (currentshieldstr < 0)
                {
                    totalstructdamage = totalstructdamage - currentshieldstr;
                    currentshieldstr = 0;
                } // end if
                removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                _root.playershotsfired[i][5] = 0;
                this.shieldimage.play();
                this.shieldimage._alpha = currentshieldstr / (maxshield * 0.750000) * 100;
            } // end if
        } // end if
    } // end of for
    if (_root.playershipstatus[5][2] == baseid)
    {
        totalstructdamage = 0;
    } // end if
    if (structuredisp - totalstructdamage < 0)
    {
        sendbasedeath();
    }
    else if (totalstructdamage > 0)
    {
        sendstructuredamage(totalstructdamage);
        totalstructdamage = 0;
    } // end else if
    func_settargetdisplayinfo(currentshieldstr, structuredisp);
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function
function fireamissile()
{
    this.missilexfire = this._x;
    this.missileyfire = this._y;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    playeranglefromship = 360 - Math.atan2(this.missilexfire - playersxcoord, this.missileyfire - playersycoord) / 0.017453;
    if (playeranglefromship > 360)
    {
        playeranglefromship = playeranglefromship - 360;
    } // end if
    if (playeranglefromship < 0)
    {
        playeranglefromship = playeranglefromship + 360;
    } // end if
    playeranglefromship = Math.round(playeranglefromship);
    ++_root.currentothermissileshot;
    if (_root.currentothermissileshot >= 1000)
    {
        _root.currentothermissileshot = 1;
    } // end if
    if (_root.othermissilefire.length < 1)
    {
        _root.othermissilefire = new Array();
    } // end if
    lastvar = _root.othermissilefire.length;
    _root.othermissilefire[lastvar] = new Array();
    _root.othermissilefire[lastvar][0] = 999999;
    _root.othermissilefire[lastvar][1] = this.missilexfire;
    _root.othermissilefire[lastvar][2] = this.missileyfire;
    velocity = _root.missile[missileshottype][0] * 1.500000;
    _root.othermissilefire[lastvar][6] = playeranglefromship;
    _root.othermissilefire[lastvar][7] = missileshottype;
    _root.othermissilefire[lastvar][8] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][9] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][10] = "other";
    _root.othermissilefire[lastvar][5] = _root.curenttime + _root.missile[missileshottype][2];
    _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "fire", "othermissilefire" + _root.othermissilefire[lastvar][8], 999000 + _root.othermissilefire[lastvar][8]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _rotation, _root.othermissilefire[lastvar][6]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _x, _root.othermissilefire[lastvar][1] - _root.shipcoordinatex);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _y, _root.othermissilefire[lastvar][2] - _root.shipcoordinatey);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xposition", _root.othermissilefire[lastvar][1]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".yposition", _root.othermissilefire[lastvar][2]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".missiletype", missileshottype);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".velocity", velocity);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".seeking", "HOST");
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".shooterid", _root.othermissilefire[lastvar][0]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".globalid", _root.othermissilefire[lastvar][9]);
    _root.othermissilefire[lastvar][11] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._width / 2;
    _root.othermissilefire[lastvar][12] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._height / 2;
    _root.othermissilefire[lastvar][13] = "SEEKING";
} // End of the function
stop ();

function chargeshields(timechange)
{
    if (currentshieldstr < maxshield)
    {
        currentshieldstr = currentshieldstr + shieldrechargerate * (timechange / 1000);
        if (basetakendamageyet == false)
        {
        } // end if
    } // end if
    if (currentshieldstr > maxshield)
    {
        currentshieldstr = maxshield;
    } // end if
} // End of the function
function loadbasevars(loadedvars)
{
    newinfo = loadedvars.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentthread = newinfo[i].split("`");
        turrets = new Array();
        turrets[0] = Number(currentthread[0]);
        turrets[1] = Number(currentthread[1]);
        turrets[2] = Number(currentthread[2]);
        turrets[3] = Number(currentthread[3]);
        turrets[4] = Number(currentthread[4]);
        shieldgenerator = Number(currentthread[5]);
        maxshield = _root.shieldgenerators[shieldgenerator][0] * _root.squadbaseinfo[squadbasetype][1][1];
        shieldrechargerate = _root.shieldgenerators[shieldgenerator][1] * _root.squadbaseinfo[squadbasetype][1][2];
        currentshieldstr = maxshield;
        totalstructure = Number(currentthread[6]);
        structuredisp = Number(totalstructure);
        missileshotsleft = Number(currentthread[7]);
        missileshottype = 9;
    } // end of for
} // End of the function
function setupturrets()
{
    if (!isNaN(turrets[0]))
    {
        this.attachMovie("turrettypeaibaseold", "turrettypeaibase0", 1);
        this.turrettypeaibase0.shottype = 4;
        this.turrettypeaibase0._x = -32;
        this.turrettypeaibase0._y = -32;
    } // end if
    if (!isNaN(turrets[1]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase1", 2);
        this.turrettypeaibase1.shottype = 6;
        this.turrettypeaibase1._x = 32;
        this.turrettypeaibase1._y = -32;
    } // end if
    if (!isNaN(turrets[2]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase2", 3);
        this.turrettypeaibase2.shottype = 6;
        this.turrettypeaibase2._x = -32;
        this.turrettypeaibase2._y = 32;
    } // end if
    if (!isNaN(turrets[3]))
    {
        this.attachMovie("turrettypeaibaseold", "turrettypeaibase3", 4);
        this.turrettypeaibase3.shottype = 4;
        this.turrettypeaibase3._x = 32;
        this.turrettypeaibase3._y = 32;
    } // end if
} // End of the function
stop ();

function sendstructuredamage(totalstructdamage)
{
    datatosend = "DM`BH`" + baseid + "`" + totalstructdamage;
    "`" + _root.playershipstatus[3][0] + "`" + "~";
    _root.mysocket.send(datatosend);
    totalstructdamage = 0;
} // End of the function
function sendbasedeath()
{
    dmsc = _root.deathfgamerewards();
    datatosend = "DM`BD`" + baseid + "`" + _root.playershipstatus[3][0] + "`" + String(basebountyworth) + "`" + Math.round(basebountyworth * _root.playershipstatus[5][6]) + "`" + dmsc + "`" + Number(_root.playershipstatus[5][2]) + "`";
    "~";
    _root.mysocket.send(datatosend);
} // End of the function
function func_settargetdisplayinfo(shield, struct)
{
    for (bn = 0; bn < _root.sectoritemsfortarget.length; bn++)
    {
        if (_root.sectoritemsfortarget[bn][0] == baseidname)
        {
            secloc = bn;
            break;
        } // end if
    } // end of for
    _root.sectoritemsfortarget[secloc][5] = shield;
    _root.testtt = secloc + "~" + _root.sectoritemsfortarget[secloc][5];
    _root.sectoritemsfortarget[secloc][6] = struct;
} // End of the function
newbaselifeVars = new XML();
structuredisp = "";
teambasetype = 0;
otherplayerdamage = 0;
for (i = 0; i < _root.teambases.length; i++)
{
    if (_root.teambases[i][0] == baseidname)
    {
        baseid = i;
        break;
    } // end if
} // end of for
baseid = baseid;
if (Number(baseid) != Number(_root.playershipstatus[5][2]))
{
    isbasehostile = true;
} // end if
if (isbasehostile == true)
{
    setupturrets();
    hitstillhostile = 0;
}
else
{
    hitstillhostile = 10;
} // end else if
basedestroyed = false;
basetakendamageyet = false;
basebountyworth = _root;
basebountyworth = _root.teambasetypes[teambasetype][4];
if (_root.squadwarinfo[0] == true)
{
    baseiddisp = _root.teambases[baseid][0].substr(2) + "/" + _root.squadwarinfo[1][baseid] + " (" + basebountyworth + ")";
}
else
{
    baseiddisp = _root.teambases[baseid][0].substr(2) + " (" + basebountyworth + ")";
} // end else if
hittestdelay = 550;
lasthittest = getTimer() + hittestdelay;
maxshield = _root.teambasetypes[_root.teambases[baseid][3]][2];
shieldrechargerate = _root.teambasetypes[_root.teambases[baseid][3]][1];
currentshieldstr = maxshield;
this.onEnterFrame = function ()
{
    if (basedestroyed == true)
    {
        removeMovieClip (this);
    } // end if
    currenttime = getTimer();
    if (lasthittest < currenttime)
    {
        structuredisp = Number(_root.teambases[baseid][10]);
        timechange = currenttime - (lasthittest - hittestdelay);
        chargeshields(timechange);
        lasthittest = currenttime + hittestdelay;
        playershittest();
    } // end if
};
stop ();
