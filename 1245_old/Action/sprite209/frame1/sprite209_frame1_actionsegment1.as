﻿// Action script...

function func_takeonothershots(otherplayerdamage)
{
    currentshieldstr = currentshieldstr - otherplayerdamage;
    if (currentshieldstr < 0)
    {
        currentshieldstr = 0;
    } // end if
    this.shieldimage.play();
    this.shieldimage._alpha = currentshieldstr / (maxshield * 0.750000) * 100;
} // End of the function
function playershittest()
{
    didplayerhitbase = false;
    numberofplayershots = _root.playershotsfired.length;
    if (numberofplayershots > 0)
    {
        halfshipswidth = 40;
        halfshipsheight = 40;
        this.relativex = this._x - _root.shipcoordinatex;
        this.relativey = this._y - _root.shipcoordinatey;
    } // end if
    for (i = 0; i < numberofplayershots; i++)
    {
        bulletshotid = _root.playershotsfired[i][0];
        bulletsx = _root.gamedisplayarea["playergunfire" + bulletshotid]._x;
        bulletsy = _root.gamedisplayarea["playergunfire" + bulletshotid]._y;
        if (bulletsx != null)
        {
            if (myhittest(relativex, relativey, halfshipswidth, halfshipsheight, bulletsx, bulletsy, _root.playershotsfired[i][11], _root.playershotsfired[i][12]))
            {
                didplayerhitbase = true;
                if (isbasehostile == false)
                {
                    --hitstillhostile;
                    if (hitstillhostile <= 0)
                    {
                        isbasehostile = true;
                        setupturrets();
                        message = "Friendly or not, You hit me enough times!!";
                        _root.func_messangercom(message, "SQUADBASE" + squadbasetype, "BEGIN");
                    } // end if
                } // end if
                if (_root.playershotsfired[i][13] == "GUNS")
                {
                    currentshieldstr = currentshieldstr - _root.guntype[_root.playershotsfired[i][6]][4];
                    _root.gunfireinformation = _root.gunfireinformation + ("GH`TB`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[i][0] + "`" + baseid + "~");
                }
                else if (_root.playershotsfired[i][13] == "MISSILE")
                {
                    currentshieldstr = currentshieldstr - _root.missile[_root.playershotsfired[i][6]][4];
                    _root.gunfireinformation = _root.gunfireinformation + ("MH`TB`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[i][0] + "`" + baseid + "~");
                } // end else if
                if (currentshieldstr < 0)
                {
                    totalstructdamage = totalstructdamage - currentshieldstr;
                    currentshieldstr = 0;
                } // end if
                removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                _root.gamedisplayarea.keyboardscript.playershotsfired[i][5] = 0;
                _root.playershotsfired[i][5] = 0;
                this.shieldimage.play();
                this.shieldimage._alpha = currentshieldstr / (maxshield * 0.750000) * 100;
            } // end if
        } // end if
    } // end of for
    if (_root.playershipstatus[5][2] == baseid)
    {
        totalstructdamage = 0;
    } // end if
    if (structuredisp - totalstructdamage < 0)
    {
        sendbasedeath();
    }
    else if (totalstructdamage > 0)
    {
        sendstructuredamage(totalstructdamage);
        totalstructdamage = 0;
    } // end else if
    func_settargetdisplayinfo(currentshieldstr, structuredisp);
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function
function fireamissile()
{
    this.missilexfire = this._x;
    this.missileyfire = this._y;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    playeranglefromship = 360 - Math.atan2(this.missilexfire - playersxcoord, this.missileyfire - playersycoord) / 0.017453;
    if (playeranglefromship > 360)
    {
        playeranglefromship = playeranglefromship - 360;
    } // end if
    if (playeranglefromship < 0)
    {
        playeranglefromship = playeranglefromship + 360;
    } // end if
    playeranglefromship = Math.round(playeranglefromship);
    ++_root.currentothermissileshot;
    if (_root.currentothermissileshot >= 1000)
    {
        _root.currentothermissileshot = 1;
    } // end if
    if (_root.othermissilefire.length < 1)
    {
        _root.othermissilefire = new Array();
    } // end if
    lastvar = _root.othermissilefire.length;
    _root.othermissilefire[lastvar] = new Array();
    _root.othermissilefire[lastvar][0] = 999999;
    _root.othermissilefire[lastvar][1] = this.missilexfire;
    _root.othermissilefire[lastvar][2] = this.missileyfire;
    velocity = _root.missile[missileshottype][0] * 1.500000;
    _root.othermissilefire[lastvar][6] = playeranglefromship;
    _root.othermissilefire[lastvar][7] = missileshottype;
    _root.othermissilefire[lastvar][8] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][9] = _root.currentothermissileshot;
    _root.othermissilefire[lastvar][10] = "other";
    _root.othermissilefire[lastvar][5] = _root.curenttime + _root.missile[missileshottype][2];
    _root.gamedisplayarea.attachMovie("missile" + _root.othermissilefire[lastvar][7] + "fire", "othermissilefire" + _root.othermissilefire[lastvar][8], 999000 + _root.othermissilefire[lastvar][8]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _rotation, _root.othermissilefire[lastvar][6]);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _x, _root.othermissilefire[lastvar][1] - _root.shipcoordinatex);
    setProperty("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8], _y, _root.othermissilefire[lastvar][2] - _root.shipcoordinatey);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".xposition", _root.othermissilefire[lastvar][1]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".yposition", _root.othermissilefire[lastvar][2]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".missiletype", missileshottype);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".velocity", velocity);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".seeking", "HOST");
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".shooterid", _root.othermissilefire[lastvar][0]);
    set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[lastvar][8] + ".globalid", _root.othermissilefire[lastvar][9]);
    _root.othermissilefire[lastvar][11] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._width / 2;
    _root.othermissilefire[lastvar][12] = _root.gamedisplayarea["othermissilefire" + _root.othermissilefire[lastvar][8]]._height / 2;
    _root.othermissilefire[lastvar][13] = "SEEKING";
} // End of the function
stop ();
