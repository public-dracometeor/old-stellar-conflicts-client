﻿// Action script...

function chargeshields(timechange)
{
    if (currentshieldstr < maxshield)
    {
        currentshieldstr = currentshieldstr + shieldrechargerate * (timechange / 1000);
        if (basetakendamageyet == false)
        {
        } // end if
    } // end if
    if (currentshieldstr > maxshield)
    {
        currentshieldstr = maxshield;
    } // end if
} // End of the function
function loadbasevars(loadedvars)
{
    newinfo = loadedvars.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentthread = newinfo[i].split("`");
        turrets = new Array();
        turrets[0] = Number(currentthread[0]);
        turrets[1] = Number(currentthread[1]);
        turrets[2] = Number(currentthread[2]);
        turrets[3] = Number(currentthread[3]);
        turrets[4] = Number(currentthread[4]);
        shieldgenerator = Number(currentthread[5]);
        maxshield = _root.shieldgenerators[shieldgenerator][0] * _root.squadbaseinfo[squadbasetype][1][1];
        shieldrechargerate = _root.shieldgenerators[shieldgenerator][1] * _root.squadbaseinfo[squadbasetype][1][2];
        currentshieldstr = maxshield;
        totalstructure = Number(currentthread[6]);
        structuredisp = Number(totalstructure);
        missileshotsleft = Number(currentthread[7]);
        missileshottype = 9;
    } // end of for
} // End of the function
function setupturrets()
{
    if (!isNaN(turrets[0]))
    {
        this.attachMovie("turrettypeaibaseold", "turrettypeaibase0", 1);
        this.turrettypeaibase0.shottype = 4;
        this.turrettypeaibase0._x = -32;
        this.turrettypeaibase0._y = -32;
    } // end if
    if (!isNaN(turrets[1]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase1", 2);
        this.turrettypeaibase1.shottype = 6;
        this.turrettypeaibase1._x = 32;
        this.turrettypeaibase1._y = -32;
    } // end if
    if (!isNaN(turrets[2]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase2", 3);
        this.turrettypeaibase2.shottype = 6;
        this.turrettypeaibase2._x = -32;
        this.turrettypeaibase2._y = 32;
    } // end if
    if (!isNaN(turrets[3]))
    {
        this.attachMovie("turrettypeaibaseold", "turrettypeaibase3", 4);
        this.turrettypeaibase3.shottype = 4;
        this.turrettypeaibase3._x = 32;
        this.turrettypeaibase3._y = 32;
    } // end if
} // End of the function
stop ();
