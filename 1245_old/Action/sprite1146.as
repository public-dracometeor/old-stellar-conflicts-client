﻿// Action script...

// [Action in Frame 1]
velocity = 0;
lastvelocity = -1;
updateinterval = 3;
currentupdateframe = 0;
maxvelocity = _root.playershipmaxvelocity;
this.velocitybar.gotoAndStop(1);
this.onEnterFrame = function ()
{
    ++currentupdateframe;
    if (currentupdateframe > updateinterval)
    {
        currentupdateframe = 0;
        currentvelocity = _root.playershipvelocity;
        if (currentvelocity != lastvelocity)
        {
            lastvelocity = currentvelocity;
            if (currentvelocity <= 0.400000)
            {
                this.velocitybar.gotoAndStop(1);
            }
            else if (currentvelocity <= maxvelocity)
            {
                thisscaled = Math.ceil(currentvelocity / maxvelocity * 24);
                this.velocitybar.gotoAndStop(thisscaled);
            }
            else
            {
                this.velocitybar.gotoAndStop(25);
            } // end else if
        } // end else if
        if (_root.isplayerdisrupt)
        {
            downtime = Math.ceil((_root.playerdiruptend - getTimer()) / 1000);
            velocity = "OFFLINE " + downtime;
            currentvelocity = -1;
        }
        else
        {
            velocity = Math.round(currentvelocity);
        } // end if
    } // end else if
};
