﻿// Action script...

on (release)
{
    squadname = squadname.toUpperCase();
    passwordd = passwordd.toUpperCase();
    newsquadVars = new XML();
    newsquadVars.load(_root.pathtoaccounts + "squads.php?mode=squadjoin&sname=" + squadname + "&spass=" + passwordd + "&name=" + _root.playershipstatus[3][2] + "&pass=" + _root.playershipstatus[3][3]);
    newsquadVars.onLoad = function (success)
    {
        loadedinfo = String(newsquadVars);
        if (loadedinfo == "success")
        {
            _root.playershipstatus[5][10] = squadname;
            _root.playershipstatus[5][13] = passwordd;
            datatosend = "SQUAD`" + _root.playershipstatus[3][0] + "`CHANGE`" + _root.playershipstatus[5][10] + "~";
            _root.mysocket.send(datatosend);
            gotoAndStop(1);
        }
        else if (loadedinfo == "toomany")
        {
            _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
            _parent.hardwarewarningbox.information = "Squad Full";
        }
        else
        {
            _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
            _parent.hardwarewarningbox.information = "Invalid Information";
        } // end else if
    };
}
