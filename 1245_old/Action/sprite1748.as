﻿// Action script...

// [onClipEvent of sprite 1746 in frame 1]
onClipEvent (load)
{
    function changeteam(newnumber)
    {
        if (_root.teamdeathmatch == true)
        {
            desiredteamtochangeto = newnumber;
        }
        else
        {
            desiredteamtochangeto = newnumber.substr(0, 4);
        } // end else if
        if (!isNaN(desiredteamtochangeto) || desiredteamtochangeto == "N/A")
        {
            datatosend = "TC`" + _root.playershipstatus[3][0] + "`TM`" + desiredteamtochangeto + "~";
            _root.mysocket.send(datatosend);
            _root.gamechatinfo[1].splice(0, 0, 0);
            _root.gamechatinfo[1][0] = new Array();
            _root.gamechatinfo[1][0][0] = " Team Changing to : " + _root.teambases[desiredteamtochangeto][0].substr(2);
            _root.gamechatinfo[1][0][1] = _root.systemchattextcolor;
            if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
            {
                _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
            } // end if
            _root.refreshchatdisplay = true;
            if (_root.teamdeathmatch == true)
            {
                _parent.displaystats();
            } // end if
        } // end if
    } // End of the function
    if (_root.playershipstatus[5][2] == "N/A")
    {
        _root.sendnewdmgame();
        textinfo = "GO ON TEAM";
    }
    else
    {
        textinfo = "CHANGE TEAM";
    } // end else if
}

// [onClipEvent of sprite 1746 in frame 1]
on (release)
{
    _root.func_main_clicksound();
    if (textinfo == "GO ON TEAM")
    {
        _parent.sendnewgame();
        if (_parent.teamsno[0] == _parent.teamsno[1])
        {
            teamno = Math.round(Math.random() * (_root.teambases.length - 1));
        }
        else if (_parent.teamsno[0] < _parent.teamsno[1])
        {
            teamno = 0;
        }
        else
        {
            teamno = 1;
        } // end else if
        changeteam(teamno);
    }
    else if (textinfo == "CHANGE TEAM")
    {
        teamno = -1;
        if (_parent.teamsno[0] < _parent.teamsno[1])
        {
            teamno = 0;
        }
        else if (_parent.teamsno[0] > _parent.teamsno[1])
        {
            teamno = 1;
        } // end else if
        if (teamno != _root.playershipstatus[5][2] & teamno > -1)
        {
            changeteam(teamno);
        }
        else
        {
            _parent.attachMovie("shipwarningbox", "shipwarningbox", 1555);
            _parent.shipwarningbox.information = "Would Uneven Teams";
        } // end else if
    } // end else if
}

// [Action in Frame 1]
function displaystats()
{
    for (loc = 0; loc < _root.currentonlineplayers.length; loc++)
    {
        if (_root.currentonlineplayers[loc][0] == _root.playershipstatus[3][0])
        {
            if (_root.playershipstatus[5][2] != _root.currentonlineplayers[loc][4])
            {
                _root.playershipstatus[5][2] = _root.currentonlineplayers[loc][4];
                this.butt.textinfo = "CHANGE TEAM";
            } // end if
        } // end if
    } // end of for
    if (_root.playershipstatus[5][2] != "N/A")
    {
        this.butt.textinfo = "CHANGE TEAM";
    }
    else
    {
        this.butt.textinfo = "GO ON TEAM";
    } // end else if
    information = "Teams: " + _root.teambases.length + "\r";
    teamsno = new Array();
    for (jj = 0; jj < _root.teambases.length; jj++)
    {
        teamsno[jj] = 0;
    } // end of for
    for (jj = 0; jj < _root.currentonlineplayers.length; jj++)
    {
        if (_root.currentonlineplayers[jj][4] != "N/A")
        {
            teamsno[_root.currentonlineplayers[jj][4]] = teamsno[_root.currentonlineplayers[jj][4]] + 1;
        } // end if
    } // end of for
    if (_root.squadwarinfo[0] == true)
    {
        information = information + "Team / Squad Size / Base Structure\r";
        for (jj = 0; jj < _root.teambases.length; jj++)
        {
            information = information + (" " + _root.teambases[jj][0].substr(2) + "  /  " + _root.squadwarinfo[1][jj] + ": " + teamsno[jj] + "  /  " + _root.teambases[jj][10] + "\r");
        } // end of for
        if (_root.playershipstatus[5][2] != "N/A")
        {
            yourteam = "Your Team: " + _root.teambases[_root.playershipstatus[5][2]][0].substr(2) + "  /  " + _root.squadwarinfo[1][_root.playershipstatus[5][2]] + "\r";
        }
        else
        {
            yourteam = "Your Team: " + _root.playershipstatus[5][2];
        } // end else if
    }
    else
    {
        information = information + "Team Size / Base Structure\r";
        for (jj = 0; jj < _root.teambases.length; jj++)
        {
            information = information + (" " + _root.teambases[jj][0].substr(2) + ": " + teamsno[jj] + "  " + _root.teambases[jj][10] + "\r");
        } // end of for
        if (_root.playershipstatus[5][2] != "N/A")
        {
            yourteam = "Your Team: " + _root.teambases[_root.playershipstatus[5][2]][0].substr(2) + "\r";
        }
        else
        {
            yourteam = "Your Team: " + _root.playershipstatus[5][2];
        } // end else if
    } // end else if
} // End of the function
displaystats();
counter = 0;
this.onEnterFrame = function ()
{
    ++counter;
    if (counter > 10)
    {
        displaystats();
        counter = 0;
    } // end if
};
