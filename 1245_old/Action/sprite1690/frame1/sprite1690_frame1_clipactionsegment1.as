﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    usertokick = _parent.kickauser;
    usertokick = usertokick.toUpperCase();
    squadmembers = _parent._parent.squadmembers;
    squadleaders = new Array();
    squadleaders[0] = _parent._parent.creatorname;
    squadleaders[1] = _parent._parent.secondname;
    squadleaders[2] = _parent._parent.thirdname;
    abletokickuser = true;
    for (i = 0; i < squadleaders.length; i++)
    {
        if (usertokick == squadleaders[i])
        {
            abletokickuser = false;
            _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
            _parent.hardwarewarningbox.information = "Not Leaders!";
        } // end if
    } // end of for
    isuseronsquad = false;
    for (i = 0; i < squadmembers.length; i++)
    {
        if (usertokick == squadmembers[i])
        {
            isuseronsquad = true;
            playerlocation = i;
            break;
        } // end if
    } // end of for
    if (isuseronsquad == false)
    {
        abletokickuser = false;
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "Not In Squad!";
    } // end if
    if (abletokickuser == true)
    {
        _parent.kickauser = " KICKING";
        newsquadVars = new XML();
        newsquadVars.load(_root.pathtoaccounts + "squadscommand.php?mode=kickuser&name=" + usertokick + "&sname=" + _root.playershipstatus[5][10] + "&spass=" + _root.playershipstatus[5][13]);
        newsquadVars.onLoad = function (success)
        {
            loadedinfo = String(newsquadVars);
            if (loadedinfo == "success")
            {
                _parent.kickauser = " KICKED";
                _parent._parent.squadmembers.splice(playerlocation, 1);
                _parent._parent.displaysquadlist();
            }
            else
            {
                _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
                _parent.hardwarewarningbox.information = "Could Not kick!";
            } // end else if
        };
    } // end if
}
