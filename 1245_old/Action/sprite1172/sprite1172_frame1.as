﻿// Action script...

// [onClipEvent of sprite 1171 in frame 1]
onClipEvent (load)
{
    nameandtype = "None";
    status = "None";
    if (_root.playersmission[0][0][0] != null)
    {
        missiontype = _root.playersmission[0][0][0];
        waypointname = _root.playersmission[0][0][1];
        missionx = _root.playersmission[0][1][0][1];
        missiony = _root.playersmission[0][1][0][2];
        rangetobefrompoint = _root.playersmission[0][4];
        timetostaythere = _root.playersmission[0][3];
        reward = _root.playersmission[0][2];
        nameandtype = missiontype + " at " + waypointname;
        status = "Pending";
        missonattempted = false;
        noofenemyaiships = _root.playersmission[0][1][0][4];
        if (missiontype == "Cargorun")
        {
            basesforcargo = waypointname.split("`");
            if (_root.playersmission[0][4] == true)
            {
                cargorunstarttime = getTimer();
                nameandtype = "Cargo to " + basesforcargo[1];
                basetogoto = basesforcargo[1];
            }
            else
            {
                nameandtype = "Cargo at " + basesforcargo[0];
                basetogoto = basesforcargo[0];
            } // end else if
            for (i = 0; i < _root.starbaselocation.length; i++)
            {
                if (_root.starbaselocation[i][0] == basetogoto)
                {
                    missionx = _root.starbaselocation[i][1];
                    missiony = _root.starbaselocation[i][2];
                } // end if
            } // end of for
        }
        else if (missiontype == "assasinate")
        {
            nameandtype = "Assassinate";
            status = _root.playersmission[0][0][1];
        } // end if
    } // end else if
    missiontimerstart = null;
    currentframe = 0;
    framestoupdate = 60;
    if (_root.isgameracingzone == true)
    {
        missiontype = "racezone";
        nameandtype = "";
        status = "Racing";
        for (i = 0; i < _root.racingcheckpoints.length; i++)
        {
            if (_root.racingcheckpoints[i][1] != true)
            {
                currentdirection = i;
                lastdirection = i - 1;
                break;
            } // end if
        } // end of for
    } // end if
}

// [onClipEvent of sprite 1171 in frame 1]
onClipEvent (enterFrame)
{
    ++currentframe;
    if (currentframe > framestoupdate)
    {
        currentframe = 0;
        if (missiontype == "Recon" || missiontype == "Patrol")
        {
            if (nameandtype != "None" && status != "Completed")
            {
                playersxcoord = _root.shipcoordinatex;
                playersycoord = _root.shipcoordinatey;
                range = Math.round(Math.sqrt((playersxcoord - this.missionx) * (playersxcoord - this.missionx) + (playersycoord - this.missiony) * (playersycoord - this.missiony)));
                if (range <= rangetobefrompoint)
                {
                    currenttime = _root.curenttime;
                    if (missiontimerstart == null)
                    {
                        if (missiontype == "Recon")
                        {
                            missiontimerstart = currenttime;
                            timetillcomplete = missiontimerstart + timetostaythere;
                            status = "In-progress " + (Math.round((timetillcomplete - currenttime) / 1000) + 1);
                        } // end if
                        if (missiontype == "Patrol")
                        {
                            if (_root.hostileplayerships != true && _root.aishipshosted.length == 0)
                            {
                                missiontimerstart = currenttime;
                                timetillcomplete = missiontimerstart + timetostaythere;
                                status = "In-progress " + (Math.round((timetillcomplete - currenttime) / 1000) + 1);
                            }
                            else if (_root.otherplayership.length > 0)
                            {
                                isthereashiptoclose = false;
                                missionx = _root.playersmission[0][1][0][1];
                                missiony = _root.playersmission[0][1][0][2];
                                for (q = 0; q < _root.otherplayership.length; q++)
                                {
                                    if (_root.playershipstatus[5][2] != _root.otherplayership[q][13] || _root.playershipstatus[5][2] == "N/A")
                                    {
                                        enemyrange = Math.round(Math.sqrt((missionx - _root.otherplayership[q][1]) * (missionx - _root.otherplayership[q][1]) + (missiony - _root.otherplayership[q][2]) * (missiony - _root.otherplayership[q][2])));
                                        if (enemyrange < rangetobefrompoint)
                                        {
                                            isthereashiptoclose = true;
                                            break;
                                        } // end if
                                    } // end if
                                } // end of for
                                if (_root.aishipshosted.length != 0)
                                {
                                    isthereashiptoclose = true;
                                } // end if
                                if (isthereashiptoclose == true)
                                {
                                    status = "Enemies ";
                                    missiontimerstart = null;
                                }
                                else
                                {
                                    missiontimerstart = currenttime;
                                    timetillcomplete = missiontimerstart + timetostaythere;
                                    status = "In-progress " + (Math.round((timetillcomplete - currenttime) / 1000) + 1);
                                } // end else if
                            }
                            else
                            {
                                status = "Interrupted ";
                                missiontimerstart = null;
                            } // end else if
                        } // end else if
                        if (missonattempted == false)
                        {
                            if (_root.hostileplayerships != true && _root.aishipshosted.length == 0)
                            {
                                missonattempted = true;
                                _root.bringinaiships(noofenemyaiships, "MISSION");
                            } // end if
                        } // end if
                    }
                    else if (currenttime <= timetillcomplete)
                    {
                        if (missiontype == "Recon")
                        {
                            status = "In-progress " + (Math.round((timetillcomplete - currenttime) / 1000) + 1);
                        }
                        else if (missiontype == "Patrol")
                        {
                            if (_root.hostileplayerships != true && _root.aishipshosted.length == 0)
                            {
                                status = "In-progress " + (Math.round((timetillcomplete - currenttime) / 1000) + 1);
                            }
                            else if (_root.otherplayership.length > 0)
                            {
                                isthereashiptoclose = false;
                                missionx = _root.playersmission[0][1][0][1];
                                missiony = _root.playersmission[0][1][0][2];
                                for (q = 0; q < _root.otherplayership.length; q++)
                                {
                                    if (_root.playershipstatus[5][2] != _root.otherplayership[q][13] || _root.playershipstatus[5][2] == "N/A")
                                    {
                                        enemyrange = Math.round(Math.sqrt((missionx - _root.otherplayership[q][1]) * (missionx - _root.otherplayership[q][1]) + (missiony - _root.otherplayership[q][2]) * (missiony - _root.otherplayership[q][2])));
                                        if (enemyrange < rangetobefrompoint)
                                        {
                                            isthereashiptoclose = true;
                                            break;
                                        } // end if
                                    } // end if
                                } // end of for
                                if (_root.aishipshosted.length != 0)
                                {
                                    isthereashiptoclose = true;
                                } // end if
                                if (isthereashiptoclose == true)
                                {
                                    missiontimerstart = null;
                                    timetillcomplete = null;
                                    status = "Interrupted! ";
                                }
                                else
                                {
                                    status = "In-progress " + (Math.round((timetillcomplete - currenttime) / 1000) + 1);
                                } // end else if
                            }
                            else
                            {
                                missiontimerstart = null;
                                timetillcomplete = null;
                                status = "Interrupted! ";
                            } // end else if
                        } // end else if
                    }
                    else if (currenttime > missiontimerstart + timetostaythere)
                    {
                        if (_root.playershipstatus[5][4] == "alive")
                        {
                            status = "Completed";
                            _root.gamedisplayarea.attachMovie("aimessage", "aimessage", 94324);
                            message = "You have completed the assigned mission. Your account has been awarded " + reward + " credits. Thank you for your help!";
                            _root.func_messangercom(message, "EMPLOYER", "BEGIN");
                            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + reward;
                            _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
                            scoreaward = Math.round(reward / _root.missiontoactualscoremodifier);
                            information = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + scoreaward + "~";
                            _root.mysocket.send(information);
                            _root.playersmission = new Array();
                        }
                        else
                        {
                            status = "Failed - Died";
                            _root.playersmission = new Array();
                        } // end else if
                    } // end else if
                }
                else if (missiontimerstart != null)
                {
                    missiontimerstart = null;
                    timetillcomplete = null;
                    status = "Pending";
                } // end if
            } // end else if
        }
        else if (missiontype == "Cargorun")
        {
            if (_root.playersmission[0][4] != false)
            {
                currenttime = _root.curenttime;
                bonustime = cargorunstarttime + _root.playersmission[0][3] - currenttime;
                if (bonustime > 0)
                {
                    status = "Bonus Time" + Math.round(bonustime / 1000);
                }
                else
                {
                    status = "In-Progress";
                    _root.playersmission[0][4] = "nobonus";
                } // end else if
            }
            else
            {
                status = "Pending";
            } // end else if
        }
        else if (missiontype == "racezone")
        {
            xcoord = _root.sectormapitems[_root.racingcheckpoints[currentdirection][0]][1];
            ycoord = _root.sectormapitems[_root.racingcheckpoints[currentdirection][0]][2];
            xdiff = xcoord - _root.shipcoordinatex;
            ydiff = ycoord - _root.shipcoordinatey;
            range = Math.sqrt(xdiff * xdiff + ydiff * ydiff);
            if (range < 150)
            {
                ++currentdirection;
                if (currentdirection >= _root.racingcheckpoints.length)
                {
                    _root.racingcheckpoints[currentdirection - 1][1] = true;
                    datatosend = "RG`FG`" + _root.playershipstatus[3][0] + "`" + _root.errorchecknumber + "~";
                    _root.mysocket.send(datatosend);
                    _root.gotoAndStop("racingzonescreen");
                } // end if
            } // end if
            if (lastdirection != currentdirection)
            {
                if (currentdirection > 0)
                {
                    _root.racingcheckpoints[currentdirection - 1][1] = true;
                } // end if
                dest = _root.sectormapitems[_root.racingcheckpoints[currentdirection][0]][0];
                if (dest.substr(0, 2) == "PL")
                {
                    dest = dest.substr(2);
                }
                else if (dest.substr(0, 2) == "NP")
                {
                    dest = "NAV " + dest.substr(2);
                } // end else if
                nameandtype = dest;
                lastdirection = currentdirection;
                _root.playersdestination[0] = dest;
                _root.playersdestination[1] = _root.sectormapitems[_root.racingcheckpoints[currentdirection][0]][1];
                _root.playersdestination[2] = _root.sectormapitems[_root.racingcheckpoints[currentdirection][0]][2];
            } // end if
        } // end else if
    } // end else if
    if (this.status == "Completed" || nameandtype == "None")
    {
        this._visible = false;
    }
    else
    {
        this._visible = true;
    } // end else if
}

// [Action in Frame 1]
stop ();
