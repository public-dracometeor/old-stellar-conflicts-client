﻿// Action script...

// [onClipEvent of sprite 1171 in frame 1]
onClipEvent (load)
{
    nameandtype = "None";
    status = "None";
    if (_root.playersmission[0][0][0] != null)
    {
        missiontype = _root.playersmission[0][0][0];
        waypointname = _root.playersmission[0][0][1];
        missionx = _root.playersmission[0][1][0][1];
        missiony = _root.playersmission[0][1][0][2];
        rangetobefrompoint = _root.playersmission[0][4];
        timetostaythere = _root.playersmission[0][3];
        reward = _root.playersmission[0][2];
        nameandtype = missiontype + " at " + waypointname;
        status = "Pending";
        missonattempted = false;
        noofenemyaiships = _root.playersmission[0][1][0][4];
        if (missiontype == "Cargorun")
        {
            basesforcargo = waypointname.split("`");
            if (_root.playersmission[0][4] == true)
            {
                cargorunstarttime = getTimer();
                nameandtype = "Cargo to " + basesforcargo[1];
                basetogoto = basesforcargo[1];
            }
            else
            {
                nameandtype = "Cargo at " + basesforcargo[0];
                basetogoto = basesforcargo[0];
            } // end else if
            for (i = 0; i < _root.starbaselocation.length; i++)
            {
                if (_root.starbaselocation[i][0] == basetogoto)
                {
                    missionx = _root.starbaselocation[i][1];
                    missiony = _root.starbaselocation[i][2];
                } // end if
            } // end of for
        }
        else if (missiontype == "assasinate")
        {
            nameandtype = "Assassinate";
            status = _root.playersmission[0][0][1];
        } // end if
    } // end else if
    missiontimerstart = null;
    currentframe = 0;
    framestoupdate = 60;
    if (_root.isgameracingzone == true)
    {
        missiontype = "racezone";
        nameandtype = "";
        status = "Racing";
        for (i = 0; i < _root.racingcheckpoints.length; i++)
        {
            if (_root.racingcheckpoints[i][1] != true)
            {
                currentdirection = i;
                lastdirection = i - 1;
                break;
            } // end if
        } // end of for
    } // end if
}
