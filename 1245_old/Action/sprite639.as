﻿// Action script...

// [onClipEvent of sprite 638 in frame 1]
onClipEvent (load)
{
    startingship = _root.playershipstatus[5][0];
    removeMovieClip ("_root.starbasehardwarebutton");
    removeMovieClip ("_root.exitstarbasebutton");
    removeMovieClip ("_root.purchaseshipbutton");
    removeMovieClip ("_root.purchasetradegoodsbutton");
    waitingforanswer = false;
    missionselection = null;
    chosenamission = false;
    shiplocationx = -_root.gameareawidth / 3 + 15;
    shiplocationy = -_root.gameareaheight / 8;
    shipspacingx = 150;
    shipspacingy = 200;
    this._y = _root.gameareaheight / 2;
    this._x = _root.gameareawidth / 2;
    this._height = _root.gameareaheight;
    this._width = _root.gameareawidth;
    this.attachMovie("shopexitbutton", "purchaseshipexitbutton", 150);
    this.purchaseshipexitbutton._y = _root.gameareaheight / 2 - 50;
    this.purchaseshipexitbutton._x = _root.gameareawidth / 2 - 100;
    this.attachMovie("starbasefundsdisplay", "funds", 121);
    this.funds._y = -_root.gameareaheight / 2 + 60;
    this.funds._x = _root.gameareawidth / 2 - 100;
    currentlvl = 1000;
}
