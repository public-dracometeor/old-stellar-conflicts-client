﻿// Action script...

// [onClipEvent of sprite 716 in frame 1]
onClipEvent (enterFrame)
{
    if (_root.refreshonlinelist == true)
    {
        _root.refreshonlinelist = false;
        if (currentdisplay == "bounty")
        {
            this.listing = "";
            this.listing = this.listing + ("Online System Players: " + _root.currentonlineplayers.length + " \r  ");
            this.listing = this.listing + "Page UP/DOWN to scroll \r";
            for (i = 0; i < _root.currentonlineplayers.length; i++)
            {
                this.listing = this.listing + (" " + _root.currentonlineplayers[i][1] + " " + "BTY:" + _root.currentonlineplayers[i][3] + " \r  ");
            } // end of for
        }
        else if (currentdisplay == "score")
        {
            this.listing = "";
            this.listing = this.listing + ("Online System Players: " + _root.currentonlineplayers.length + " \r  ");
            this.listing = this.listing + "Page UP/DOWN to scroll \r";
            for (i = 0; i < _root.currentonlineplayers.length; i++)
            {
                this.listing = this.listing + (_root.currentonlineplayers[i][0] + " " + _root.currentonlineplayers[i][4] + " " + _root.currentonlineplayers[i][1] + " " + " SCORE:" + Math.floor(_root.currentonlineplayers[i][5] / _root.scoreratiomodifier) + " \r  ");
            } // end of for
        }
        else if (currentdisplay == "both")
        {
            this.listing = "";
            this.listing = this.listing + ("Online System Players: " + _root.currentonlineplayers.length + " \r  ");
            this.listing = this.listing + "Page UP/DOWN to scroll \r";
            for (i = 0; i < _root.currentonlineplayers.length; i++)
            {
                if (_root.currentonlineplayers[i][4] != _root.playershipstatus[5][2] && _root.currentonlineplayers[i][4] != "N/A" && _root.currentonlineplayers[i][4] > 10)
                {
                    teamtodisplay = "XXXX";
                }
                else if (_root.teamdeathmatch == true && _root.currentonlineplayers[i][4] != "N/A")
                {
                    if (_root.squadwarinfo[0] == true)
                    {
                        teamtodisplay = _root.squadwarinfo[1][_root.currentonlineplayers[i][4]];
                    }
                    else
                    {
                        teamtodisplay = _root.teambases[_root.currentonlineplayers[i][4]][0].substr(2);
                    } // end else if
                }
                else
                {
                    teamtodisplay = _root.currentonlineplayers[i][4];
                } // end else if
                this.listing = this.listing + (" " + _root.currentonlineplayers[i][1] + " T:" + teamtodisplay + "\r" + "  SCORE:" + Math.floor(_root.currentonlineplayers[i][5] / _root.scoreratiomodifier) + " BTY:" + _root.currentonlineplayers[i][3] + "\r");
            } // end of for
        } // end else if
    } // end else if
}
