﻿// Action script...

if (_parent.ainame == "STARBASE")
{
    this.gotoAndStop("STARBASE");
}
else if (_parent.ainame == "EMPLOYER")
{
    this.gotoAndStop("EMPLOYER");
}
else if (_parent.ainame == "PLANET0")
{
    this.gotoAndStop("PLANET0");
}
else if (_parent.ainame == "PLANET1")
{
    this.gotoAndStop("PLANET1");
}
else if (_parent.ainame == "PLANET2")
{
    this.gotoAndStop("PLANET2");
}
else if (_parent.ainame == "PLANET3")
{
    this.gotoAndStop("PLANET3");
}
else if (_parent.ainame == "NEWS")
{
    this.gotoAndStop("NEWS");
}
else if (_parent.ainame == "INFO" || _parent.ainame == "HELP")
{
    this.gotoAndStop("INFO");
}
else if (_parent.ainame == "STARBASE1")
{
    this.gotoAndStop("STARBASE1");
}
else
{
    this.gotoAndStop("AIPILOT");
} // end else if
