﻿// Action script...

// [Action in Frame 1]
function movementofanobjectwiththrust()
{
    relativefacing = this.rotation;
    if (relativefacing == 0)
    {
        ymovement = -itemvelocity;
        xmovement = 0;
    } // end if
    if (velocity != 0)
    {
        this.relativefacing = Math.round(this.relativefacing);
        ymovement = Math.round(-itemvelocity * _root.cosines[this.relativefacing]);
        xmovement = Math.round(itemvelocity * _root.sines[this.relativefacing]);
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
rotation = Math.round(this._rotation);
if (this.rotation > 360)
{
    this.rotation = this.rotation - 360;
} // end if
if (this.rotation < 0)
{
    this.rotation = this.rotation + 360;
} // end if
if (this.seeking == "HOST")
{
    lastkeypresses = "";
    seekertime = 500;
    nextseektime = getTimer() + seekertime;
    rotationspeed = _root.missile[missiletype][1];
    this.onEnterFrame = function ()
    {
        currentshipdisplaychangeratio = currenttimechangeratio = _root.currenttimechangeratio;
        currenttime = getTimer();
        if (nextseektime < currenttime)
        {
            nextseektime = currenttime + seekertime;
            if (distracted)
            {
                playersxcoord = Number(flarex);
                playersycoord = Number(flarey);
            }
            else
            {
                playersxcoord = _root.shipcoordinatex;
                playersycoord = _root.shipcoordinatey;
            } // end else if
            playerdistancefromship = Math.round(Math.sqrt((playersxcoord - this.xposition) * (playersxcoord - this.xposition) + (playersycoord - this.yposition) * (playersycoord - this.yposition)));
            playeranglefromship = 360 - Math.atan2(this.xposition - playersxcoord, this.yposition - playersycoord) / 0.017453;
            playeranglefromship = playeranglefromship - rotation;
            if (playeranglefromship > 360)
            {
                playeranglefromship = playeranglefromship - 360;
            } // end if
            if (playeranglefromship < 0)
            {
                playeranglefromship = playeranglefromship + 360;
            } // end if
            currentkeypresses = "";
            this.rotating = 0;
            if (playeranglefromship < 10 || playeranglefromship >= 350)
            {
                if (playerdistancefromship < 30)
                {
                } // end if
            }
            else if (playeranglefromship <= 180)
            {
                this.rotating = 1;
                currentkeypresses = currentkeypresses + "R";
            }
            else
            {
                this.rotating = -1;
                currentkeypresses = currentkeypresses + "L";
            } // end else if
        } // end else if
        if (this.rotating < 0)
        {
            this.rotation = this.rotation + -rotationspeed * currentshipdisplaychangeratio;
            if (this.rotation > 360)
            {
                this.rotation = this.rotation - 360;
            } // end if
            if (this.rotation < 0)
            {
                this.rotation = this.rotation + 360;
            } // end if
        }
        else if (this.rotating > 0)
        {
            this.rotation = this.rotation + rotationspeed * currentshipdisplaychangeratio;
            if (this.rotation > 360)
            {
                this.rotation = this.rotation - 360;
            } // end if
            if (this.rotation < 0)
            {
                this.rotation = this.rotation + 360;
            } // end if
        } // end else if
        itemvelocity = Math.round(velocity * currentshipdisplaychangeratio);
        relativefacing = Math.round(this.rotation);
        movementofanobjectwiththrust();
        this.xposition = this.xposition + xmovement;
        this.yposition = this.yposition + ymovement;
        this._x = xposition - _root.shipcoordinatex;
        this._y = yposition - _root.shipcoordinatey;
        this._rotation = Math.round(this.rotation);
        if (currentkeypresses != lastkeypresses)
        {
            lastkeypresses = currentkeypresses;
            _root.gunfireinformation = _root.gunfireinformation + ("MI`" + shooterid + "`" + Math.round(xposition) + "`" + Math.round(yposition) + "`" + Math.round(velocity) + "`" + Math.round(this.rotation) + "`" + "`" + globalid + "`" + currentkeypresses + "~");
        } // end if
    };
} // end if
if (this.seeking == "AI")
{
    seekertime = 400;
    nextseektime = getTimer() + seekertime;
    rotationspeed = _root.missile[missiletype][1];
    this.onEnterFrame = function ()
    {
        currentshipdisplaychangeratio = currenttimechangeratio = _root.currenttimechangeratio;
        currenttime = getTimer();
        if (nextseektime < currenttime)
        {
            nextseektime = currenttime + seekertime;
            playersxcoord = _root.aishipshosted[aitargetnumber][1];
            playersycoord = _root.aishipshosted[aitargetnumber][2];
            playerdistancefromship = Math.round(Math.sqrt((playersxcoord - this.xposition) * (playersxcoord - this.xposition) + (playersycoord - this.yposition) * (playersycoord - this.yposition)));
            playeranglefromship = 360 - Math.atan2(this.xposition - playersxcoord, this.yposition - playersycoord) / 0.017453;
            playeranglefromship = playeranglefromship - rotation;
            if (playeranglefromship > 360)
            {
                playeranglefromship = playeranglefromship - 360;
            } // end if
            if (playeranglefromship < 0)
            {
                playeranglefromship = playeranglefromship + 360;
            } // end if
            this.rotating = 0;
            currentkeypresses = "";
            if (playeranglefromship < 10 || playeranglefromship >= 350)
            {
                if (playerdistancefromship < 30)
                {
                } // end if
            }
            else if (playeranglefromship <= 180)
            {
                this.rotating = 1;
                currentkeypresses = currentkeypresses + "R";
            }
            else
            {
                this.rotating = -1;
                currentkeypresses = currentkeypresses + "L";
            } // end else if
        } // end else if
        if (this.rotating < 0)
        {
            this.rotation = this.rotation + -rotationspeed * currentshipdisplaychangeratio;
            if (this.rotation > 360)
            {
                this.rotation = this.rotation - 360;
            } // end if
            if (this.rotation < 0)
            {
                this.rotation = this.rotation + 360;
            } // end if
        }
        else if (this.rotating > 0)
        {
            this.rotation = this.rotation + rotationspeed * currentshipdisplaychangeratio;
            if (this.rotation > 360)
            {
                this.rotation = this.rotation - 360;
            } // end if
            if (this.rotation < 0)
            {
                this.rotation = this.rotation + 360;
            } // end if
        } // end else if
        itemvelocity = Math.round(velocity * currentshipdisplaychangeratio);
        relativefacing = Math.round(this.rotation);
        movementofanobjectwiththrust();
        this.xposition = this.xposition + xmovement;
        this.yposition = this.yposition + ymovement;
        this._x = xposition - _root.shipcoordinatex;
        this._y = yposition - _root.shipcoordinatey;
        this._rotation = this.rotation;
    };
} // end if
if (this.seeking == "OTHER")
{
    rotationspeed = _root.missile[missiletype][1];
    timetillexpires = getTimer() + _root.missile[missiletype][2];
    this.onEnterFrame = function ()
    {
        currentshipdisplaychangeratio = currenttimechangeratio = _root.currenttimechangeratio;
        currenttime = getTimer();
        if (timetillexpires < currenttime)
        {
            removeMovieClip (this);
        } // end if
        if (currentkeypresses == "L")
        {
            this.rotation = this.rotation + -rotationspeed * currentshipdisplaychangeratio;
            if (this.rotation > 360)
            {
                this.rotation = this.rotation - 360;
            } // end if
            if (this.rotation < 0)
            {
                this.rotation = this.rotation + 360;
            } // end if
        }
        else if (currentkeypresses == "R")
        {
            this.rotation = this.rotation + rotationspeed * currentshipdisplaychangeratio;
            if (this.rotation > 360)
            {
                this.rotation = this.rotation - 360;
            } // end if
            if (this.rotation < 0)
            {
                this.rotation = this.rotation + 360;
            } // end if
        } // end else if
        itemvelocity = velocity * currentshipdisplaychangeratio;
        relativefacing = Math.round(this.rotation);
        movementofanobjectwiththrust();
        this.xposition = this.xposition + xmovement;
        this.yposition = this.yposition + ymovement;
        this._x = xposition - _root.shipcoordinatex;
        this._y = yposition - _root.shipcoordinatey;
        this._rotation = this.rotation;
    };
} // end if
stop ();
