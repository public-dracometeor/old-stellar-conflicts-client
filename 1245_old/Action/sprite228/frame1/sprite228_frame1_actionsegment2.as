﻿// Action script...

basedestroyed = false;
basetakendamageyet = false;
hittestdelay = 550;
lasthittest = getTimer() + hittestdelay;
hitstillhostile = 6;
misileupdateint = 10000;
lastmisileupdate = 0;
missileshotsleft = 90;
missileshottype = 9;
missilesfired = 0;
missilfireint = _root.missile[missileshottype][3] / 2;
timetillnextmissile = 0;
xsector = _root.playershipstatus[6][0];
ysector = _root.playershipstatus[6][1];
if (displaymessage == true)
{
    message = "Welcome to sector " + xsector + "," + ysector + ".  Home of Planet " + basename.substr(2) + ". \r";
    _root.func_messangercom(message, "PLANET2", "BEGIN");
} // end if
this.onEnterFrame = function ()
{
    if (basedestroyed == true)
    {
        removeMovieClip (this);
    } // end if
    currenttime = getTimer();
    if (lasthittest < currenttime)
    {
        timechange = currenttime - (lasthittest - hittestdelay);
        lasthittest = currenttime + hittestdelay;
        if (isbasehostile == false)
        {
            if (_root.kingofflag[0] == null)
            {
                playershittestneutral();
            } // end if
        }
        else if (isbasehostile == true)
        {
            playershittest();
            if (timetillnextmissile < currenttime)
            {
                if (_root.playershipstatus[2][5] > 0)
                {
                    fireamissile();
                    if (_root.soundvolume != "off")
                    {
                        _root.missilefiresound.start();
                    } // end if
                    timetillnextmissile = missilfireint + currenttime;
                } // end if
            } // end if
        } // end if
    } // end else if
};
stop ();
