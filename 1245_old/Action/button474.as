﻿// Action script...

on (release)
{
    missileselected = missileselected;
    quantity = Math.round(quantity);
    missilecost = missilecost;
    bankno = bankno;
    finalcost = missilecost * quantity;
    if (_root.playershipstatus[3][1] >= finalcost && quantity > 0)
    {
        _root.playershipstatus[7][bankno][4][missileselected] = _root.playershipstatus[7][bankno][4][missileselected] + quantity;
        _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - finalcost;
        _parent.redrawscreen = true;
        removeMovieClip (this);
    }
    else if (quantity < 1)
    {
        _parent.redrawscreen = true;
        removeMovieClip (this);
    } // end else if
}
