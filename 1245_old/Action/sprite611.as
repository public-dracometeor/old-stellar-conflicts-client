﻿// Action script...

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][10] == "NONE")
    {
        _parent.gotoAndStop("joinsquad");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Leave your squad First!";
    } // end else if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][10] != "NONE")
    {
        _parent.gotoAndStop("squadinfo");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Be In A Squad First!";
    } // end else if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][11] != true)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "This Is For Squad Creators ONLY!";
    }
    else if (_root.playershipstatus[5][10] != "NONE")
    {
        _parent.gotoAndStop("managesquad");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Be In A Squad First!";
    } // end else if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][10] == "NONE")
    {
        _parent.gotoAndStop("createsquad");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Leave your squad First!";
    } // end else if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][11] == true)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Destroy Your Squad First!";
    }
    else if (_root.playershipstatus[5][10] != "NONE")
    {
        _parent.gotoAndStop("leavesquad");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Be In A Squad First!";
    } // end else if
}

// [onClipEvent of sprite 589 in frame 3]
onClipEvent (load)
{
    xwidthofasector = _root.sectorinformation[1][0];
    ywidthofasector = _root.sectorinformation[1][1];
    _parent.members = "Loading";
    _parent.baseloc = "Loading";
    _parent.systemloc = "Loading";
    _parent.number = "N/A";
    newsquadVars = new XML();
    newsquadVars.load(_root.pathtoaccounts + "squads.php?mode=squadinfo&sname=" + _root.playershipstatus[5][10]);
    newsquadVars.onLoad = function (success)
    {
        noofsquad = 0;
        loadedinfo = String(newsquadVars);
        newinfo = loadedinfo.split("~");
        _parent.members = "";
        for (i = 0; i < newinfo.length - 1; i++)
        {
            currentthread = newinfo[i].split("`");
            if (currentthread[0] == "P")
            {
                ++noofsquad;
                _parent.members = _parent.members + (noofsquad + ": " + currentthread[1] + "\r");
            } // end if
        } // end of for
        _parent.number = noofsquad;
    };
    newbaseVars = new XML();
    newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=baselocate&baseid=" + _root.playershipstatus[5][10]);
    newbaseVars.onLoad = function (success)
    {
        loadedbaseinfo = String(newbaseVars);
        if (loadedbaseinfo == "nobase")
        {
            _parent.baseloc._visible = false;
            _parent.baseloc = "";
            _parent.createbase._visible = true;
            _parent.systemloc._visible = false;
            _parent.systemloc = "";
        }
        else
        {
            _parent.createbase._visible = false;
            basecoords = loadedbaseinfo.split("`");
            xcoord = Number(basecoords[0]);
            ycoord = Number(basecoords[1]);
            xgrid = Math.ceil(xcoord / xwidthofasector);
            ygrid = Math.ceil(ycoord / ywidthofasector);
            baseinsystem = basecoords[2];
            _parent.baseloc._visible = true;
            _parent.systemloc._visible = true;
            _parent.baseloc = "Grid X: " + xgrid + ", Y: " + ygrid;
            _parent.systemloc = "System: " + baseinsystem;
        } // end else if
    };
}

// [onClipEvent of sprite 589 in frame 3]
on (release)
{
    _parent.gotoAndStop("start");
}

// [onClipEvent of sprite 569 in frame 3]
onClipEvent (load)
{
    this.label = "CREATE BASE";
    this._visible = false;
    if (_root.teamdeathmatch == true || _root.isgameracingzone == true)
    {
        this.label = "NO BASES";
    } // end if
}

// [onClipEvent of sprite 569 in frame 3]
on (release)
{
    if (_root.teamdeathmatch != true && _root.isgameracingzone != true)
    {
        _parent.gotoAndStop("createbase");
        
    } // end if
}

// [onClipEvent of sprite 569 in frame 5]
on (release)
{
    squadname = squadname.toUpperCase();
    passwordd = passwordd.toUpperCase();
    wipebaselifeVars = new XML();
    wipebaselifeVars.load(_root.pathtoaccounts + "squadbases.php?mode=damagebase&struct=999999&baseid=" + _root.playershipstatus[5][10]);
    wipebaselifeVars.onLoad = function (success)
    {
    };
    newsquadVars = new XML();
    newsquadVars.load(_root.pathtoaccounts + "squads.php?mode=dissolvesquad&sname=" + _root.playershipstatus[5][10] + "&name=" + _root.playershipstatus[3][2] + "&spass=" + _root.playershipstatus[5][13]);
    newsquadVars.onLoad = function (success)
    {
        datatosend = "SA~NEWS`SB`DESTROYED`" + _root.playershipstatus[5][10] + "~";
        _root.mysocket.send(datatosend);
        _root.playershipstatus[5][10] = "NONE";
        _root.playershipstatus[5][11] = false;
        datatosend = "SQUAD`" + _root.playershipstatus[3][0] + "`CHANGE`" + _root.playershipstatus[5][10] + "~";
        _root.mysocket.send(datatosend);
        _root.playershipstatus[5][13] = "";
        _parent.gotoAndStop("start");
    };
}

// [onClipEvent of sprite 569 in frame 5]
on (release)
{
    wipebaselifeVars = new XML();
    wipebaselifeVars.load(_root.pathtoaccounts + "squadbases.php?mode=damagebase&struct=999999&baseid=" + _root.playershipstatus[5][10]);
    wipebaselifeVars.onLoad = function (success)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "Destroyed";
        datatosend = "SA~NEWS`SB`DESTROYED`" + _root.playershipstatus[5][10] + "~";
        _root.mysocket.send(datatosend);
    };
}

// [Action in Frame 1]
this.squadname = _root.playershipstatus[5][10];
stop ();

this.joinsquad.label = "Join A Squad";
this.squadinfo.label = "Squad Info";
this.createsquad.label = "Create A Squad";
this.managesquad.label = "Manage Squad";
this.leavesquad.label = "Leave Squad";
stop ();

// [Action in Frame 2]
this.squadname = "Enter Name";
this.passwordd = "Password";
stop ();

// [Action in Frame 3]
stop ();

// [Action in Frame 4]
this.squadname = "Enter Name";
this.passwordd = "Password";
stop ();

// [Action in Frame 5]
this.dissolve.label = "Dissolve Squad";
this.destorybase.label = "Destroy Base";
stop ();

// [Action in Frame 6]
this.name = _root.playershipstatus[5][10];
stop ();

// [Action in Frame 7]
this.maxcoords = "MAX X: " + _root.sectorinformation[0][0] + "\r" + "MAX Y: " + _root.sectorinformation[0][1];
maxxgrid = _root.sectorinformation[0][0] - 1;
maxygrid = _root.sectorinformation[0][1] - 1;
xgrid = 0;
ygrid = 0;
this.onEnterFrame = function ()
{
    if (xgrid > maxxgrid || xgrid < 1 || isNaN(xgrid))
    {
        xgrid = "";
    } // end if
    if (ygrid > maxygrid || ygrid < 1 || isNaN(ygrid))
    {
        ygrid = "";
    } // end if
};
