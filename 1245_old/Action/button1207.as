﻿// Action script...

on (release)
{
    startnumber = _root.playershipstatus[7][bankno][0];
    totalmisisletypes = _root.missile.length;
    for (i = startnumber + 1; i != startnumber; i++)
    {
        if (i >= totalmisisletypes)
        {
            i = -1;
            continue;
        } // end if
        if (_root.playershipstatus[7][bankno][4][i] > 0)
        {
            _root.playershipstatus[7][bankno][0] = i;
            missilename = _root.missile[i][7];
            missileqty = "Qty: " + _root.playershipstatus[7][bankno][4][i];
            _root.playershipstatus[7][bankno][0] = i;
            break;
        } // end if
    } // end of for
}
