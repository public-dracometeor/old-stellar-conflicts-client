﻿// Action script...

// [Action in Frame 1]
information = "Race Information\r\r";
startloc = _root.starbaselocation[0][0];
information = " Start: " + startloc + "\r";
distance = 0;
lasty = Number(_root.starbaselocation[0][2]);
lastx = Number(_root.starbaselocation[0][1]);
for (jj = 0; jj < _root.racingcheckpoints.length; jj++)
{
    dest = _root.sectormapitems[_root.racingcheckpoints[jj][0]][0];
    if (dest.substr(0, 2) == "PL")
    {
        dest = dest.substr(2);
    }
    else if (dest.substr(0, 2) == "NP")
    {
        dest = "NAV " + dest.substr(2);
    } // end else if
    information = information + (" " + (jj + 1) + ": " + dest + "\r");
    xloc = Number(_root.sectormapitems[_root.racingcheckpoints[jj][0]][1]);
    yloc = Number(_root.sectormapitems[_root.racingcheckpoints[jj][0]][2]);
    xdiff = lastx - xloc;
    ydiff = lasty - yloc;
    lasty = yloc;
    lastx = xloc;
    distance = distance + Math.sqrt(xdiff * xdiff + ydiff * ydiff);
} // end of for
information = information + ("\r\rDistance: " + _root.func_formatnumber(Math.round(distance)));
information = information + ("\rReward: " + _root.func_formatnumber(_root.racinginformation[1]));
timetillstart = "";
this.onEnterFrame = function ()
{
    startingtime = Math.floor((_root.racinginformation[0] - getTimer()) / 1000);
    if (startingtime > 0)
    {
        timetillstart = "Race Starting in " + startingtime + " seconds";
    }
    else
    {
        _root.gotoAndStop("maingameplaying");
    } // end else if
};
