﻿// Action script...

// [Action in Frame 49]
function func_setinputfield(fieldtosetto)
{
    curentinputfield = fieldtosetto;
    func_settextcolour(curentinputfield);
} // End of the function
function func_settextcolour(curentinputfield)
{
    if (curentinputfield == "name")
    {
        mov_signup.mov_password2.gotoAndStop(2);
        mov_signup.mov_password.gotoAndStop(2);
        mov_signup.mov_name.gotoAndStop(1);
    }
    else if (curentinputfield == "pass1")
    {
        mov_signup.mov_password2.gotoAndStop(2);
        mov_signup.mov_password.gotoAndStop(1);
        mov_signup.mov_name.gotoAndStop(2);
    }
    else if (curentinputfield == "pass2")
    {
        mov_signup.mov_password2.gotoAndStop(1);
        mov_signup.mov_password.gotoAndStop(2);
        mov_signup.mov_name.gotoAndStop(2);
    } // end else if
} // End of the function
function func_initialize()
{
    membernameinput = "";
    memberpassword = "";
    verifypassword = "";
    var_name = "";
    var_pass = "";
    var_verifypass = "";
    this.mov_signup.name.text = membernameinput;
    this.mov_signup.password.text = memberpassword;
    this.mov_signup.password2.text = var_verifypass;
    curentinputfield = "name";
    var_field = "name";
    if (_root.keyboardversion != true)
    {
        func_drawkeyboard();
    }
    else
    {
        var letter_array = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        var number_array = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        Key.removeListener(myListener);
        myListener = new Object();
        myListener.onKeyDown = function ()
        {
            asciikey = Number(Key.getAscii());
            test = asciikey;
            if (asciikey == 13)
            {
                func_submit();
            }
            else if (asciikey == 8)
            {
                func_delChar();
            }
            else if (asciikey >= 48 && asciikey <= 57)
            {
                addkeypress = number_array[asciikey - 48];
                func_addChar(addkeypress);
            }
            else if (asciikey >= 97 && asciikey <= 122)
            {
                addkeypress = letter_array[asciikey - 97];
                func_addChar(addkeypress);
            }
            else if (asciikey >= 65 && asciikey <= 90)
            {
                addkeypress = letter_array[asciikey - 65];
                func_addChar(addkeypress);
            } // end else if
        };
        Key.addListener(myListener);
    } // end else if
} // End of the function
function func_drawkeyboard()
{
    message = message + "\rInput a Name and press SUBMIT";
    message = message + "\rUse only letters A-Z, and numbers.\r";
    message = message + "\rUse only 3 to 11 characters.\r";
    membernameinput = "";
    memberpassword = "";
    verifypassword = "";
    var_name = "";
    var_pass = "";
    var_verifypass = "";
    this.mov_signup.name.text = membernameinput;
    this.mov_signup.password.text = memberpassword;
    this.mov_signup.password2.text = var_verifypass;
    var use = keyboard_array2;
    var_type = "alpha";
    curentinputfield = "name";
    var_yPos = -100;
    var_xStart = -150;
    var_xPos = -150;
    var_xMax = 875;
    var use = keyboard_array3;
    var_type = "alpha";
    var_field = "name";
    for (var_n = 0; var_n < use.length; var_n++)
    {
        if (use[var_n] == "<")
        {
            if (var_xPos != var_xStart)
            {
                var_yPos = var_yPos + 47;
                var_xPos = var_xStart + Number(getProperty("m_key0", _width)) / 2;
            } // end if
            continue;
        } // end if
        if (use[var_n] == "<<")
        {
            if (var_xPos != var_xStart)
            {
                var_yPos = var_yPos + 47;
                var_xPos = var_xStart;
            } // end if
            continue;
        } // end if
        if (use[var_n] == ">")
        {
            var_xPos = var_xPos + Number(getProperty("m_key0", _width)) / 2;
            continue;
        } // end if
        attachMovie("m_keyboard", "m_key" + var_n, 1000 + var_n);
        setProperty("m_key" add var_n, _x, Number(var_xPos));
        setProperty("m_key" + var_n, _y, var_yPos);
        var_xPos = var_xPos + Number(getProperty("m_key" add var_n, _width) - 1);
        if (var_xPos >= var_xMax)
        {
            var_yPos = var_yPos + Number(2 + (getProperty("m_key" + var_n, _height)));
            var_xPos = var_xStart;
        } // end if
        this["m_key" + var_n].var_key = use[var_n];
    } // end of for
    if (var_type == "alpha")
    {
        attachMovie("m_keyboard_space", "m_key" + var_n, 1000 + var_n);
        setProperty("m_key" add var_n, _x, Number(var_xPos + (getProperty("m_key" + var_n, _width)) / 4));
        setProperty("m_key" + var_n, _y, var_yPos);
        this["m_key" + var_n].var_key = " ";
        var_xPos = var_xPos + Number(getProperty("m_key" + var_n, _width));
        ++var_n;
    } // end if
    attachMovie("m_keyboard_del", "m_key" + var_n, 1000 + var_n);
    setProperty("m_key" add var_n, _x, Number(var_xPos));
    setProperty("m_key" + var_n, _y, var_yPos);
    var_xPos = var_xPos + Number(getProperty("m_key" + var_n, _width) - 1);
    ++var_n;
    attachMovie("m_keyboard_send", "m_key" + var_n, 1000 + var_n);
    setProperty("m_key" add var_n, _x, Number(var_xPos) + (getProperty("m_key" + var_n, _width)) / 4);
    setProperty("m_key" + var_n, _y, var_yPos);
    ++var_n;
    attachMovie("m_messagebox", "mov_messageBox", 1000 + var_n);
    setProperty("mov_messageBox", _x, 480);
    setProperty("mov_messageBox", _y, 350);
} // End of the function
function func_submit()
{
    if (membernameinput != "")
    {
        if (memberpassword != "")
        {
            if (verifypassword != "")
            {
                for (var_n = 0; var_n < use.length + 300; var_n++)
                {
                    removeMovieClip ("m_key" + var_n);
                } // end of for
                removeMovieClip ("mov_messageBox");
                this.func_createaccount(membernameinput, memberpassword, verifypassword);
            }
            else
            {
                curentinputfield = "pass2";
                func_settextcolour(curentinputfield);
                message = "\rVerify your Password and press SUBMIT";
                message = message + "\rUse only letters A-Z, and numbers.\r";
                message = message + "\rUse only 3 to 11 characters.\r";
            } // end else if
        }
        else
        {
            curentinputfield = "pass1";
            func_settextcolour(curentinputfield);
            message = "\rInput a Password and press SUBMIT";
            message = message + "\rUse only letters A-Z, and numbers.\r";
            message = message + "\rUse only 3 to 11 characters.\r";
        } // end else if
    }
    else
    {
        curentinputfield = "name";
        func_settextcolour(curentinputfield);
    } // end else if
} // End of the function
function func_addChar(var_char)
{
    _root.func_keyboardkeysound();
    if (curentinputfield == "name")
    {
        var_name = var_name + var_char;
        membernameinput = var_name.toUpperCase();
        this.mov_signup.name.text = membernameinput;
    }
    else if (curentinputfield == "pass1")
    {
        var_pass = var_pass + var_char;
        memberpassword = var_pass.toUpperCase();
        this.mov_signup.password.text = memberpassword;
    }
    else if (curentinputfield == "pass2")
    {
        var_verifypass = var_verifypass + var_char;
        verifypassword = var_verifypass.toUpperCase();
        this.mov_signup.password2.text = var_verifypass;
    } // end else if
} // End of the function
function func_delChar()
{
    _root.func_keyboardkeysound();
    if (curentinputfield == "name")
    {
        var_name = var_name.slice(0, Number(var_name.length - 1));
        membernameinput = var_name;
        this.mov_signup.name.text = membernameinput;
    }
    else if (curentinputfield == "pass1")
    {
        var_pass = var_pass.slice(0, Number(var_name.length - 1));
        memberpassword = var_pass;
        this.mov_signup.password.text = memberpassword;
    }
    else if (curentinputfield == "pass2")
    {
        var_verifypass = var_verifypass.slice(0, Number(var_name.length - 1));
        verifypassword = var_verifypass;
        this.mov_signup.password2.text = var_verifypass;
    } // end else if
} // End of the function
function func_createaccount(membernameinput, memberpassword, verifypassword)
{
    membernameinput = membernameinput.toUpperCase();
    memberpassword = memberpassword.toUpperCase();
    verifypassword = verifypassword.toUpperCase();
    email = _parent.email.toLowerCase();
    if (membernameinput.length > 11 || membernameinput.length < 3)
    {
        send_mov_message("Name must be at least 3 and no more than 11 Characters");
    }
    else if (membernameinput == "HOST")
    {
        send_mov_message("Illegal Name");
    }
    else if (membernameinput.charAt(0) == " " || membernameinput.charAt(membernameinput.length - 1) == " ")
    {
        send_mov_message("Enter a Name that does not begin or end with a space");
        func_initialize();
    }
    else if (checknameforlegitcharacters(membernameinput))
    {
        send_mov_message("Enter a Name that consists of only letters a-Z, and numbers");
        func_initialize();
    }
    else if (memberpassword.length > 10 || memberpassword.length < 3)
    {
        send_mov_message("Passwords must be at least 3 characters and no more than 10 characters");
        func_initialize();
    }
    else if (passwordhasspaces(memberpassword))
    {
        send_mov_message("Passwords have no spaces, and consists of only letters a-Z, and numbers");
        func_initialize();
    }
    else if (checknameforlegitcharacters(memberpassword))
    {
        send_mov_message("Passwords have no spaces, and consists of only letters a-Z, and numbers");
        func_initialize();
    }
    else if (verifypassword.toUpperCase() != memberpassword.toUpperCase())
    {
        send_mov_message("Passwords do not match");
        func_initialize();
    }
    else
    {
        send_mov_message("Creating Account");
        if (hascreationbeensubmited != true)
        {
            hascreationbeensubmited = true;
            accountcreation();
        }
        else
        {
            send_mov_message("Still Creating Your Account, Please Wait");
        } // end else if
    } // end else if
} // End of the function
function accountcreation()
{
    newaccountVars = new XML();
    newaccountVars.load(_root.pathtoaccounts + "accounts.php?mode=create&name=" + membernameinput + "&pass=" + memberpassword + "&info=" + _root.accountcreationsavedgame + "&score=0");
    newaccountVars.onLoad = function (success)
    {
        loadedvars = String(newaccountVars);
        info = loadedvars.split("~");
        loadedvars = info[0];
        if (loadedvars == "exists")
        {
            send_mov_message("Name Already Owned, Please Try A Different Name");
            hascreationbeensubmited = false;
            func_initialize();
        }
        else if (loadedvars == "created")
        {
            hascreationbeensubmited = false;
            _parent.memberlogin._visible = true;
            _parent.guestlogin._visible = true;
            _parent.memberlogin.membernameinput = membernameinput;
            _parent.memberlogin.memberpassword = memberpassword;
            send_mov_message("Your Account was Successfully Created, Please Close This Window and Login.");
        }
        else
        {
            hascreationbeensubmited = false;
            send_mov_message("Failure to Create account, Please try again");
            func_initialize();
        } // end else if
    };
} // End of the function
function func_login()
{
    datatosend = "create`" + membernameinput + "`" + memberpassword + "`" + memberemail + "~";
    accountserv = new XMLSocket();
    if (_root.isgamerunningfromremote == false)
    {
        currenturl = String(_root._url);
        if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
        {
            accountserv.connect("", _root.accountserverport);
        } // end if
    }
    else
    {
        accountserv.connect(_root.accountserveraddy, _root.accountserverport);
    } // end else if
    accountserv.onConnect = function (success)
    {
        accountserv.send(datatosend);
    };
    accountserv.onXML = xmlhandler;
} // End of the function
function xmlhandler(doc)
{
    loadedvars = String(doc);
    loadedvars = String(newaccountVars);
    info = loadedvars.split("~");
    loadedvars = info[0];
    if (loadedvars == "nameexists`")
    {
        send_mov_message("Name Already Owned, Please Try A Different Name");
        hascreationbeensubmited = false;
        func_initialize();
    }
    else if (loadedvars == "namecreated`")
    {
        hascreationbeensubmited = false;
        _parent.memberlogin._visible = true;
        _parent.guestlogin._visible = true;
        _parent.memberlogin.membernameinput = membernameinput;
        _parent.memberlogin.memberpassword = memberpassword;
        send_mov_message("Your Account was Successfully Created, Please Close This Window and Login.");
    } // end else if
} // End of the function
function passwordhasspaces(memberpassword)
{
    for (i = 0; i < memberpassword.length; i++)
    {
        if (memberpassword.charAt(i) == " ")
        {
            return (true);
        } // end if
    } // end of for
} // End of the function
function checknameforlegitcharacters(name)
{
    legitcharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    for (i = 0; i < name.length; i++)
    {
        illegalcharacter = true;
        for (j = 0; j < legitcharacters.length; j++)
        {
            if (name.charAt(i) == legitcharacters.charAt(j))
            {
                illegalcharacter = false;
            } // end if
        } // end of for
        if (illegalcharacter == true)
        {
            return (true);
            i = 99999;
        } // end if
    } // end of for
} // End of the function
function send_mov_message(messagetosend)
{
    this.mov_signup.func_message(messagetosend);
} // End of the function
Key.removeListener(myListener);
func_initialize();
stop ();
