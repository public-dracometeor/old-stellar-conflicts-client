﻿// Action script...

// [Action in Frame 60]
function func_initialize()
{
    curentinputfield = "name";
    guestnameinput = "";
    memberpassword = "";
    var_pass = "";
    if (_root.keyboardversion != true)
    {
        func_drawkeyboard();
    }
    else
    {
        _parent.message = _parent.message + "\rPress Enter to Log In";
        _parent.message = _parent.message + "\rGuests cannot change thier name.\r";
        _parent.message = _parent.message + "\rUse only 3 to 11 characters.\r";
        _parent.message = _parent.message + "\rYou will not require a password.\r";
        test = "ran1";
        var letter_array = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        var number_array = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        Key.removeListener(myListener);
        myListener = new Object();
        myListener.onKeyDown = function ()
        {
            asciikey = Number(Key.getAscii());
            if (asciikey == 13)
            {
                guestnameinput = var_name;
                func_submit();
            } // end if
        };
        Key.addListener(myListener);
    } // end else if
} // End of the function
function func_drawkeyboard()
{
    var keyboard_array1 = new Array("7", "8", "9", "<", "4", "5", "6", "<<", "1", "2", "3", "<", "0");
    var keyboard_array2 = new Array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "<", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "<", ">", "A", "S", "D", "F", "G", "H", "J", "K", "L", "<", ">", ">", "Z", "X", "C", "V", "B", "N", "M", "<<", ">", ">", "@", ".", "_", "-");
    var_yPos = -100;
    var_xStart = -150;
    var_xPos = -150;
    var_xMax = 875;
    var use = keyboard_array2;
    var_type = "alpha";
    for (var_n = 0; var_n < use.length; var_n++)
    {
        if (use[var_n] == "<")
        {
            if (var_xPos != var_xStart)
            {
                var_yPos = var_yPos + 47;
                var_xPos = var_xStart + Number(getProperty("m_key0", _width)) / 2;
            } // end if
            continue;
        } // end if
        if (use[var_n] == "<<")
        {
            if (var_xPos != var_xStart)
            {
                var_yPos = var_yPos + 47;
                var_xPos = var_xStart;
            } // end if
            continue;
        } // end if
        if (use[var_n] == ">")
        {
            var_xPos = var_xPos + Number(getProperty("m_key0", _width)) / 2;
            continue;
        } // end if
        attachMovie("m_keyboard", "m_key" + var_n, 1000 + var_n);
        setProperty("m_key" add var_n, _x, Number(var_xPos));
        setProperty("m_key" + var_n, _y, var_yPos);
        var_xPos = var_xPos + Number(getProperty("m_key" add var_n, _width) - 1);
        if (var_xPos >= var_xMax)
        {
            var_yPos = var_yPos + Number(2 + (getProperty("m_key" + var_n, _height)));
            var_xPos = var_xStart;
        } // end if
        this["m_key" + var_n].var_key = use[var_n];
    } // end of for
    if (var_type == "alpha")
    {
        attachMovie("m_keyboard_space", "m_key" + var_n, 1000 + var_n);
        setProperty("m_key" add var_n, _x, Number(var_xPos + (getProperty("m_key" + var_n, _width)) / 4));
        setProperty("m_key" + var_n, _y, var_yPos);
        this["m_key" + var_n].var_key = " ";
        var_xPos = var_xPos + Number(getProperty("m_key" + var_n, _width));
        ++var_n;
    } // end if
    attachMovie("m_keyboard_del", "m_key" + var_n, 1000 + var_n);
    setProperty("m_key" add var_n, _x, Number(var_xPos));
    setProperty("m_key" + var_n, _y, var_yPos);
    var_xPos = var_xPos + Number(getProperty("m_key" + var_n, _width) - 1);
    ++var_n;
    attachMovie("m_keyboard_send", "m_key" + var_n, 1000 + var_n);
    setProperty("m_key" add var_n, _x, Number(var_xPos) + (getProperty("m_key" + var_n, _width)) / 4);
    setProperty("m_key" + var_n, _y, var_yPos);
    ++var_n;
    attachMovie("m_messagebox", "mov_messageBox", 1000 + var_n);
    setProperty("mov_messageBox", _x, 480);
    setProperty("mov_messageBox", _y, 350);
} // End of the function
function func_submit()
{
    if (guestnameinput != "")
    {
        for (var_n = 0; var_n < use.length + 300; var_n++)
        {
            removeMovieClip ("m_key" + var_n);
        } // end of for
        removeMovieClip ("mov_messageBox");
        this.func_login(guestnameinput);
        
    } // end if
} // End of the function
function func_addChar(var_char)
{
    _root.func_keyboardkeysound();
    if (curentinputfield == "name")
    {
        var_name = var_name + var_char;
        guestnameinput = var_name.toUpperCase();
        mov_guests.name.text = var_name.toUpperCase();
    }
    else
    {
        var_pass = var_pass + var_char;
        memberpassword = var_pass.toUpperCase();
    } // end else if
} // End of the function
function func_delChar()
{
    _root.func_keyboardkeysound();
    if (curentinputfield == "name")
    {
        var_name = var_name.slice(0, Number(var_name.length - 1));
        guestnameinput = var_name;
        mov_guests.name.text = var_name;
    }
    else
    {
        var_pass = var_email.slice(0, Number(var_email.length - 1));
        memberpassword = var_pass;
    } // end else if
} // End of the function
function func_login(guestnameinput)
{
    guestnameinput = guestnameinput.toUpperCase();
    if (guestnameinput.length > 11 || guestnameinput.length < 3)
    {
        send_mov_message("Name must be at least 3 and no more than 11 Characters");
        func_initialize();
    }
    else if (guestnameinput.charAt(0) == " " || guestnameinput.charAt(membernameinput.length - 1) == " ")
    {
        send_mov_message("Enter a Name that does not begin or end with a space");
        func_initialize();
    }
    else if (checknameforlegitcharacters(guestnameinput))
    {
        send_mov_message("Enter a Name that consists of only letters a-Z, spaces and numbers");
        func_initialize();
    }
    else
    {
        _root.playershipstatus[3][2] = "#" + guestnameinput.toUpperCase();
        _root.playershipstatus[3][0] = _root.playershipstatus[3][2];
        loadplayerdata(loadedvars);
        _parent._parent.message = "Loggin In";
        _root.isplayeraguest = true;
        newaccountVars = new XML();
        newaccountVars.load(_root.pathtoaccounts + "accounts.php?mode=gueststamp&name=" + guestnameinput);
        newaccountVars.onLoad = function (success)
        {
        };
        login();
    } // end else if
} // End of the function
function passwordhasspaces(memberpassword)
{
    for (i = 0; i < memberpassword.length; i++)
    {
        if (memberpassword.charAt(i) == " ")
        {
            return (true);
        } // end if
    } // end of for
} // End of the function
function checknameforlegitcharacters(name)
{
    legitcharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890";
    for (i = 0; i < name.length; i++)
    {
        illegalcharacter = true;
        for (j = 0; j < legitcharacters.length; j++)
        {
            if (name.charAt(i) == legitcharacters.charAt(j))
            {
                illegalcharacter = false;
            } // end if
        } // end of for
        if (illegalcharacter == true)
        {
            return (true);
            i = 99999;
        } // end if
    } // end of for
} // End of the function
function login()
{
    loadplayerdata(_root.accountcreationsavedgame);
    loadedvars = "NO0~SH0`ST0`SG1`EG1`EC1`HP0G2`HP1G2~SH1`ST1`SG0`EG0`EC0`HP0G1`HP1G2`HP2G1~";
    _root.loadplayerextraships(loadedvars);
    this.play();
} // End of the function
function loadplayerdata(loadedvars)
{
    shipvarstoload = "";
    newinfo = loadedvars.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        if (newinfo[i].substr(0, 2) == "PI")
        {
            currentthread = newinfo[i].split("`");
            if (currentthread[1].substr(0, 2) == "ST")
            {
                _root.playershipstatus[5][0] = int(currentthread[1].substr("2"));
                _root.playershipstatus[2][0] = int(currentthread[2].substr("2"));
                _root.playershipstatus[1][0] = int(currentthread[3].substr("2"));
                _root.playershipstatus[1][5] = int(currentthread[4].substr("2"));
                _root.playershipstatus[3][1] = Number(currentthread[5].substr("2"));
                _root.playershipstatus[5][1] = currentthread[6].substr("2");
                _root.playershipstatus[4][0] = currentthread[7];
                tr = 8;
                _root.playershipstatus[0] = new Array();
                while (currentthread[tr].substr(0, 2) == "HP")
                {
                    guninfo = currentthread[tr].split("G");
                    currenthardpoint = guninfo[0].substr("2");
                    _root.playershipstatus[0][currenthardpoint] = new Array();
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.playershipstatus[0][currenthardpoint][0] = guninfo[1];
                    ++tr;
                } // end while
                _root.playershipstatus[8] = new Array();
                while (currentthread[tr].substr(0, 2) == "TT")
                {
                    guninfo = currentthread[tr].split("G");
                    currentturretpoint = guninfo[0].substr("2");
                    _root.playershipstatus[8][currentturretpoint] = new Array();
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.playershipstatus[8][currentturretpoint][0] = guninfo[1];
                    ++tr;
                } // end while
                while (currentthread[tr].substr(0, 2) == "CO")
                {
                    cargoinfo = currentthread[tr].split("A");
                    currentcargotype = cargoinfo[0].substr("2");
                    _root.playershipstatus[4][1][currentcargotype] = int(cargoinfo[1]);
                    ++tr;
                } // end while
                spno = 0;
                _root.playershipstatus[11][1] = new Array();
                while (currentthread[tr].substr(0, 2) == "SP")
                {
                    cargoinfo = currentthread[tr].split("Q");
                    _root.playershipstatus[11][1][spno] = new Array();
                    _root.playershipstatus[11][1][spno][0] = Number(cargoinfo[0].substr("2"));
                    _root.playershipstatus[11][1][spno][1] = Number(cargoinfo[1]);
                    ++spno;
                    ++tr;
                } // end while
            } // end if
        } // end if
        if (newinfo[i].substr(0, 5) == "score")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[5][9] = Number(currentthread[1]);
            if (isNaN(_root.playershipstatus[5][9]))
            {
                _root.playershipstatus[5][9] = Number(0);
            } // end if
        } // end if
        if (newinfo[i].substr(0, 2) == "NO")
        {
            shipvarstoload = shipvarstoload + (newinfo[i] + "~");
        } // end if
        if (newinfo[i].substr(0, 2) == "SH")
        {
            shipvarstoload = shipvarstoload + (newinfo[i] + "~");
        } // end if
        if (newinfo[i].substr(0, 3) == "bty")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[5][8] = Number(currentthread[1]);
            if (_root.playershipstatus[5][8] < 0)
            {
                _root.playershipstatus[5][8] = 0;
            } // end if
        } // end if
        if (newinfo[i].substr(0, 5) == "squad")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[5][10] = currentthread[1];
            _root.playershipstatus[5][13] = currentthread[3];
            _root.playershipstatus[5][11] = false;
            if (currentthread[2] == _root.playershipstatus[3][2])
            {
                _root.playershipstatus[5][11] = true;
            } // end if
        } // end if
        if (newinfo[i].substr(0, 4) == "fund")
        {
            currentthread = newinfo[i].split("`");
            playerfunds = Number(currentthread[1]);
            if (playerfunds != 0)
            {
                _root.playershipstatus[3][1] = playerfunds;
                _root.func_setoldfundsuptocurrentammount();
            } // end if
        } // end if
        if (newinfo[i].substr(0, 2) == "AD")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[5][12] = currentthread[1].toUpperCase();
        } // end if
        if (newinfo[i].substr(0, 6) == "BANNED")
        {
            currentthread = newinfo[i].split("`");
            _root.gameerror = "banned";
            _root.timebannedfor = currentthread[1];
            _root.gotoAndStop("gameclose");
        } // end if
        if (newinfo[i].substr(0, 2) == "EM")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[3][5] = String(currentthread[1]);
        } // end if
    } // end of for
    _root.loadplayerextraships(shipvarstoload);
} // End of the function
function send_mov_message(messagetosend)
{
    this.mov_guests.func_message(messagetosend);
} // End of the function
guestnameinput = "";
var_name = mov_guests.name.text = this.mov_mov_guests.name = "GUEST" + Math.round(Math.random() * 9999);
_parent.message = "";
var_yPos = -100;
var_xStart = -150;
var_xPos = -150;
var_xMax = 875;
var use = keyboard_array3;
var_type = "alpha";
var_field = "name";
func_initialize();
stop ();
