﻿// Action script...

// [Action in Frame 39]
function func_initialize()
{
    curentinputfield = "name";
    func_settextcolour(curentinputfield);
    this.mov_members.name.text = "";
    this.mov_members.password.text = "";
    this.mov_members.name.text = membernameinput;
    this.mov_members.password.text = memberpassword;
    var_name = membernameinput;
    var_pass = memberpassword;
    var_field = "name";
    if (_root.keyboardversion != true)
    {
        func_drawkeyboard();
    }
    else
    {
        test = "ran1";
        var letter_array = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        var number_array = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        Key.removeListener(myListener);
        myListener = new Object();
        myListener.onKeyDown = function ()
        {
            asciikey = Number(Key.getAscii());
            if (asciikey == 13)
            {
                func_submit();
            }
            else if (asciikey == 8)
            {
                func_delChar();
            }
            else if (asciikey >= 48 && asciikey <= 57)
            {
                addkeypress = number_array[asciikey - 48];
                func_addChar(addkeypress);
            }
            else if (asciikey >= 97 && asciikey <= 122)
            {
                addkeypress = letter_array[asciikey - 97];
                func_addChar(addkeypress);
            }
            else if (asciikey >= 65 && asciikey <= 90)
            {
                addkeypress = letter_array[asciikey - 65];
                func_addChar(addkeypress);
            }
            else if (asciikey == 32)
            {
                addkeypress = " ";
                func_addChar(addkeypress);
            } // end else if
        };
        Key.addListener(myListener);
    } // end else if
} // End of the function
function func_setinputfield(fieldtosetto)
{
    curentinputfield = fieldtosetto;
    func_settextcolour(curentinputfield);
} // End of the function
function func_settextcolour(curentinputfield)
{
    if (curentinputfield == "name")
    {
        mov_members.mov_password.gotoAndStop(2);
        mov_members.mov_name.gotoAndStop(1);
    }
    else if (curentinputfield == "pass")
    {
        mov_members.mov_password.gotoAndStop(1);
        mov_members.mov_name.gotoAndStop(2);
    } // end else if
} // End of the function
function func_drawkeyboard()
{
    var_yPos = -100;
    var_xStart = -150;
    var_xPos = -150;
    var_xMax = 875;
    var use = keyboard_array3;
    var_type = "alpha";
    for (var_n = 0; var_n < use.length; var_n++)
    {
        if (use[var_n] == "<")
        {
            if (var_xPos != var_xStart)
            {
                var_yPos = var_yPos + 47;
                var_xPos = var_xStart + Number(getProperty("m_key0", _width)) / 2;
            } // end if
            continue;
        } // end if
        if (use[var_n] == "<<")
        {
            if (var_xPos != var_xStart)
            {
                var_yPos = var_yPos + 47;
                var_xPos = var_xStart;
            } // end if
            continue;
        } // end if
        if (use[var_n] == ">")
        {
            var_xPos = var_xPos + Number(getProperty("m_key0", _width)) / 2;
            continue;
        } // end if
        attachMovie("m_keyboard", "m_key" + var_n, 1000 + var_n);
        setProperty("m_key" add var_n, _x, Number(var_xPos));
        setProperty("m_key" + var_n, _y, var_yPos);
        var_xPos = var_xPos + Number(getProperty("m_key" add var_n, _width) - 1);
        if (var_xPos >= var_xMax)
        {
            var_yPos = var_yPos + Number(2 + (getProperty("m_key" + var_n, _height)));
            var_xPos = var_xStart;
        } // end if
        this["m_key" + var_n].var_key = use[var_n];
    } // end of for
    if (var_type == "alpha")
    {
        attachMovie("m_keyboard_space", "m_key" + var_n, 1000 + var_n);
        setProperty("m_key" add var_n, _x, Number(var_xPos + (getProperty("m_key" + var_n, _width)) / 4));
        setProperty("m_key" + var_n, _y, var_yPos);
        this["m_key" + var_n].var_key = " ";
        var_xPos = var_xPos + Number(getProperty("m_key" + var_n, _width));
        ++var_n;
    } // end if
    attachMovie("m_keyboard_del", "m_key" + var_n, 1000 + var_n);
    setProperty("m_key" add var_n, _x, Number(var_xPos));
    setProperty("m_key" + var_n, _y, var_yPos);
    var_xPos = var_xPos + Number(getProperty("m_key" + var_n, _width) - 1);
    ++var_n;
    attachMovie("m_keyboard_send", "m_key" + var_n, 1000 + var_n);
    setProperty("m_key" add var_n, _x, Number(var_xPos) + (getProperty("m_key" + var_n, _width)) / 4);
    setProperty("m_key" + var_n, _y, var_yPos);
    ++var_n;
    attachMovie("m_messagebox", "mov_messageBox", 1000 + var_n);
    setProperty("mov_messageBox", _x, 480);
    setProperty("mov_messageBox", _y, 350);
} // End of the function
function func_submit()
{
    if (membernameinput != "")
    {
        if (memberpassword != "")
        {
            for (var_n = 0; var_n < use.length + 300; var_n++)
            {
                removeMovieClip ("m_key" + var_n);
            } // end of for
            removeMovieClip ("mov_messageBox");
            this.func_trytologin(membernameinput, memberpassword);
        }
        else
        {
            curentinputfield = "pass";
            func_settextcolour(curentinputfield);
        } // end else if
    }
    else
    {
        curentinputfield = "name";
        func_settextcolour(curentinputfield);
    } // end else if
} // End of the function
function func_addChar(var_char)
{
    _root.func_keyboardkeysound();
    if (curentinputfield == "name")
    {
        var_name = var_name + var_char;
        membernameinput = var_name.toUpperCase();
        this.mov_members.name.text = membernameinput;
    }
    else
    {
        var_pass = var_pass + var_char;
        memberpassword = var_pass.toUpperCase();
        this.mov_members.password.text = memberpassword;
    } // end else if
} // End of the function
function func_delChar()
{
    _root.func_keyboardkeysound();
    if (curentinputfield == "name")
    {
        var_name = var_name.slice(0, Number(var_name.length - 1));
        membernameinput = var_name;
        this.mov_members.name.text = membernameinput;
    }
    else
    {
        var_pass = var_email.slice(0, Number(var_email.length - 1));
        memberpassword = var_pass;
        this.mov_members.password.text = memberpassword;
    } // end else if
} // End of the function
function func_trytologin(membernameinput, memberpassword)
{
    membernameinput = membernameinput.toUpperCase();
    memberpassword = memberpassword.toUpperCase();
    if (membernameinput.length > 11 || membernameinput.length < 3)
    {
        send_mov_message("Name must be at least 3 and no more than 11 Characters");
        func_initialize();
    }
    else if (membernameinput.charAt(0) == " " || membernameinput.charAt(membernameinput.length - 1) == " ")
    {
        send_mov_message("Enter a Name that does not begin or end with a space");
        func_initialize();
    }
    else if (checknameforlegitcharacters(membernameinput))
    {
        send_mov_message(e = "Enter a Name that consists of only letters a-Z, spaces and numbers");
        func_initialize();
    }
    else if (memberpassword.length > 10 || memberpassword.length < 3)
    {
        send_mov_message("Passwords must be at least 3 characters and no more than 10 characters");
        func_initialize();
    }
    else if (passwordhasspaces(memberpassword))
    {
        send_mov_message("Passwords have no spaces, and consists of only letters a-Z, and numbers");
        func_initialize();
    }
    else if (checknameforlegitcharacters(memberpassword))
    {
        send_mov_message("Passwords have no spaces, and consists of only letters a-Z, and numbers");
        func_initialize();
    }
    else
    {
        _root.playershipstatus[3][2] = substring(membernameinput.toUpperCase(), 0, 11);
        _root.playershipstatus[3][0] = _root.playershipstatus[3][2];
        loadplayerdata(loadedvars);
        if (hasloginbeensubmited != true)
        {
            hasloginbeensubmited = true;
            func_login();
        }
        else
        {
            send_mov_message("Still Logging In, Please Wait");
        } // end else if
    } // end else if
} // End of the function
function passwordhasspaces(memberpassword)
{
    for (i = 0; i < memberpassword.length; i++)
    {
        if (memberpassword.charAt(i) == " ")
        {
            return (true);
        } // end if
    } // end of for
} // End of the function
function checknameforlegitcharacters(name)
{
    legitcharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890";
    for (i = 0; i < name.length; i++)
    {
        illegalcharacter = true;
        for (j = 0; j < legitcharacters.length; j++)
        {
            if (name.charAt(i) == legitcharacters.charAt(j))
            {
                illegalcharacter = false;
            } // end if
        } // end of for
        if (illegalcharacter == true)
        {
            return (true);
            i = 99999;
        } // end if
    } // end of for
} // End of the function
function func_login()
{
    datatosend = "load`" + membernameinput + "`" + memberpassword + "`~";
    accountserv = new XMLSocket();
    if (_root.isgamerunningfromremote == false)
    {
        currenturl = String(_root._url);
        if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
        {
            accountserv.connect("", _root.accountserverport);
        } // end if
    }
    else
    {
        accountserv.connect(_root.accountserveraddy, _root.accountserverport);
    } // end else if
    accountserv.onConnect = function (success)
    {
        accountserv.send(datatosend);
        _root.readytomoveon = 50;
    };
    accountserv.onClose = function ()
    {
        ++_root.readytomoveon;
        if (_root.readytomoveon >= 52)
        {
            nextFrame ();
        } // end if
    };
    accountserv.onXML = xmlhandler;
} // End of the function
function xmlhandler(doc)
{
    loadedvars = String(doc);
    if (loadedvars.length > 10 && loadedvars != "could not connect to MYSQL server")
    {
        send_mov_message("Information Received, Please Wait");
        loadplayerdata(loadedvars);
        _root.isplayeraguest = false;
        _root.playershipstatus[3][3] = memberpassword;
        checktoseeifhostofsquad();
        ++_root.readytomoveon;
        if (_root.readytomoveon >= 52)
        {
            nextFrame ();
        } // end if
    }
    else if (loadedvars == "could not connect to MYSQL server")
    {
        send_mov_message("Failed to log in, Accounts are down you may try to login as a guest");
    }
    else
    {
        send_mov_message("Failed to log in, Account Information is Incorrect, Server is Down, Or Not Connected to the Internet");
    } // end else if
    hasloginbeensubmited = false;
    func_initialize();
} // End of the function
function loadplayerdata(loadedvars)
{
    shipvarstoload = "";
    newinfo = loadedvars.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        if (newinfo[i].substr(0, 2) == "PI")
        {
            currentthread = newinfo[i].split("`");
            if (currentthread[1].substr(0, 2) == "ST")
            {
                _root.playershipstatus[5][0] = int(currentthread[1].substr("2"));
                _root.playershipstatus[2][0] = int(currentthread[2].substr("2"));
                _root.playershipstatus[1][0] = int(currentthread[3].substr("2"));
                _root.playershipstatus[1][5] = int(currentthread[4].substr("2"));
                _root.playershipstatus[3][1] = Number(currentthread[5].substr("2"));
                _root.playershipstatus[5][1] = currentthread[6].substr("2");
                _root.playershipstatus[4][0] = currentthread[7];
                tr = 8;
                _root.playershipstatus[0] = new Array();
                while (currentthread[tr].substr(0, 2) == "HP")
                {
                    guninfo = currentthread[tr].split("G");
                    currenthardpoint = guninfo[0].substr("2");
                    _root.playershipstatus[0][currenthardpoint] = new Array();
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.playershipstatus[0][currenthardpoint][0] = guninfo[1];
                    ++tr;
                } // end while
                _root.playershipstatus[8] = new Array();
                while (currentthread[tr].substr(0, 2) == "TT")
                {
                    guninfo = currentthread[tr].split("G");
                    currentturretpoint = guninfo[0].substr("2");
                    _root.playershipstatus[8][currentturretpoint] = new Array();
                    if (isNaN(guninfo[1]))
                    {
                        guninfo[1] = "none";
                    } // end if
                    _root.playershipstatus[8][currentturretpoint][0] = guninfo[1];
                    ++tr;
                } // end while
                while (currentthread[tr].substr(0, 2) == "CO")
                {
                    cargoinfo = currentthread[tr].split("A");
                    currentcargotype = cargoinfo[0].substr("2");
                    _root.playershipstatus[4][1][currentcargotype] = int(cargoinfo[1]);
                    ++tr;
                } // end while
                spno = 0;
                _root.playershipstatus[11][1] = new Array();
                while (currentthread[tr].substr(0, 2) == "SP")
                {
                    cargoinfo = currentthread[tr].split("Q");
                    _root.playershipstatus[11][1][spno] = new Array();
                    _root.playershipstatus[11][1][spno][0] = Number(cargoinfo[0].substr("2"));
                    _root.playershipstatus[11][1][spno][1] = Number(cargoinfo[1]);
                    ++spno;
                    ++tr;
                } // end while
            } // end if
        } // end if
        if (newinfo[i].substr(0, 5) == "score")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[5][9] = Number(currentthread[1]);
            if (isNaN(_root.playershipstatus[5][9]))
            {
                _root.playershipstatus[5][9] = Number(0);
            } // end if
        } // end if
        if (newinfo[i].substr(0, 2) == "NO")
        {
            shipvarstoload = shipvarstoload + (newinfo[i] + "~");
        } // end if
        if (newinfo[i].substr(0, 2) == "SH")
        {
            shipvarstoload = shipvarstoload + (newinfo[i] + "~");
        } // end if
        if (newinfo[i].substr(0, 3) == "bty")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[5][8] = Number(currentthread[1]);
            if (_root.playershipstatus[5][8] < 0)
            {
                _root.playershipstatus[5][8] = 0;
            } // end if
        } // end if
        if (newinfo[i].substr(0, 5) == "squad")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[5][10] = currentthread[1];
            _root.playershipstatus[5][13] = currentthread[3];
            _root.playershipstatus[5][11] = false;
            if (currentthread[2] == _root.playershipstatus[3][2])
            {
                _root.playershipstatus[5][11] = true;
            } // end if
        } // end if
        if (newinfo[i].substr(0, 4) == "fund")
        {
            currentthread = newinfo[i].split("`");
            playerfunds = Number(currentthread[1]);
            if (playerfunds != 0)
            {
                _root.playershipstatus[3][1] = playerfunds;
                _root.func_setoldfundsuptocurrentammount();
            } // end if
        } // end if
        if (newinfo[i].substr(0, 2) == "AD")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[5][12] = currentthread[1].toUpperCase();
        } // end if
        if (newinfo[i].substr(0, 3) == "SAF")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[5][25] = String(currentthread[1]);
        } // end if
        if (newinfo[i].substr(0, 6) == "BANNED")
        {
            currentthread = newinfo[i].split("`");
            _root.gameerror = "banned";
            _root.timebannedfor = currentthread[1];
            _root.gotoAndStop("gameclose");
        } // end if
        if (newinfo[i].substr(0, 2) == "EM")
        {
            currentthread = newinfo[i].split("`");
            _root.playershipstatus[3][5] = String(currentthread[1]);
        } // end if
    } // end of for
    _root.loadplayerextraships(shipvarstoload);
} // End of the function
function send_mov_message(messagetosend)
{
    this.mov_members.func_message(messagetosend);
} // End of the function
_parent.message = "";
this.mov_members.name.text = membernameinput;
this.mov_members.password.text = "";
func_initialize();
stop ();
