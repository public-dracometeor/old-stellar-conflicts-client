﻿// Action script...

// [Action in Frame 1]
thiscolor = new Color(this);
starcolor = 85;
endcolour = 255;
increments = 17;
currentcolour = starcolor;
incremuntsup = true;
thiscolor.setRGB(starcolor);

// [Action in Frame 2]
if (incremuntsup == true)
{
    currentcolour = currentcolour + increments;
    if (currentcolour >= endcolour)
    {
        incremuntsup = false;
    } // end if
}
else
{
    currentcolour = currentcolour - increments;
    if (currentcolour <= starcolor)
    {
        incremuntsup = true;
    } // end if
} // end else if
thiscolor.setRGB(currentcolour);

// [Action in Frame 6]
gotoAndPlay(2);
