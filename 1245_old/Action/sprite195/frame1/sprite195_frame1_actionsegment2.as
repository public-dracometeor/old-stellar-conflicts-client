﻿// Action script...

function chargeshields(timechange)
{
    if (currentshieldstr < maxshield)
    {
        currentshieldstr = currentshieldstr + shieldrechargerate * (timechange / 1000);
        if (basetakendamageyet == false)
        {
            basetakendamageyet = true;
            chattosend = "SQUAD`CH`" + baseidname + "`WM`" + "FILLER" + "~";
            _root.mysocket.send(chattosend);
        } // end if
    } // end if
    if (currentshieldstr > maxshield)
    {
        currentshieldstr = maxshield;
    } // end if
} // End of the function
function loadbasevars(loadedvars)
{
    newinfo = loadedvars.split("~");
    for (i = 0; i < newinfo.length - 1; i++)
    {
        currentthread = newinfo[i].split("`");
        turrets = new Array();
        turrets[0] = Number(currentthread[0]);
        turrets[1] = Number(currentthread[1]);
        turrets[2] = Number(currentthread[2]);
        turrets[3] = Number(currentthread[3]);
        turrets[4] = Number(currentthread[4]);
        if (baseiddisp.substr(0, 3) != "*AI")
        {
            for (ii = 0; ii < turrets.length; ii++)
            {
                if (isNaN(turrets[ii]))
                {
                    continue;
                } // end if
                basebountyworth = basebountyworth + _root.guntype[turrets[ii]][5] / 4;
            } // end of for
        } // end if
        basebountyworth = Math.floor(basebountyworth) + 2000;
        baseiddisp = squadid + " (" + basebountyworth + ")";
        shieldgenerator = Number(currentthread[5]);
        maxshield = _root.shieldgenerators[shieldgenerator][0] * _root.squadbaseinfo[squadbasetype][1][1];
        shieldrechargerate = _root.shieldgenerators[shieldgenerator][1] * _root.squadbaseinfo[squadbasetype][1][2];
        currentshieldstr = maxshield;
        totalstructure = Number(currentthread[6]);
        structuredisp = Number(totalstructure);
        missileshotsleft = Number(currentthread[7]);
        missileshottype = 9;
        hostilestatus = String(currentthread[8]);
        if (hostilestatus == "HOSTILE" || hostilestatus == "ENEMY")
        {
            message = "You Are Designated as Hostile Ship, Do not Approach This Base!";
            _root.func_messangercom(message, "SQUADBASE" + squadbasetype, "BEGIN");
            isbasehostile = true;
            hitstillhostile = 0;
            continue;
        } // end if
        isbasehostile = false;
        hitstillhostile = 4;
        message = "You Are Designated as Neutral or Friendly Ship, You May Approach This Base, But Do Not Attack It!";
        _root.func_messangercom(message, "SQUADBASE" + squadbasetype, "BEGIN");
    } // end of for
} // End of the function
function setupturrets()
{
    if (!isNaN(turrets[0]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase0", 1);
        this.turrettypeaibase0.shottype = turrets[0];
        this.turrettypeaibase0._x = -40;
        this.turrettypeaibase0._y = -40;
    } // end if
    if (!isNaN(turrets[1]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase1", 2);
        this.turrettypeaibase1.shottype = turrets[1];
        this.turrettypeaibase1._x = 40;
        this.turrettypeaibase1._y = -40;
    } // end if
    if (!isNaN(turrets[2]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase2", 3);
        this.turrettypeaibase2.shottype = turrets[2];
        this.turrettypeaibase2._x = -40;
        this.turrettypeaibase2._y = 40;
    } // end if
    if (!isNaN(turrets[3]))
    {
        this.attachMovie("turrettypeaibase", "turrettypeaibase3", 4);
        this.turrettypeaibase3.shottype = turrets[3];
        this.turrettypeaibase3._x = 40;
        this.turrettypeaibase3._y = 40;
    } // end if
} // End of the function
stop ();
