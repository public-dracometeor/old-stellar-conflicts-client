﻿// Action script...

// [Action in Frame 1]
function firingbulletstartlocation()
{
    rotationangle = initialangleforgun + _root.playershipfacing;
    xfirestart = -hardpointfromcenter * Math.sin(0.017453 * rotationangle);
    yfirestart = hardpointfromcenter * Math.cos(0.017453 * rotationangle);
    if (flipshot == true)
    {
        xfirestart = xfirestart * -1;
        yfirestart = yfirestart * -1;
    } // end if
} // End of the function
function movementofanobjectwiththrust()
{
    this.relativefacing = Math.round(this.relativefacing);
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    }
    else if (velocity != 0)
    {
        this.relativefacing = Math.round(this.relativefacing);
        ymovement = -velocity * _root.cosines[this.relativefacing];
        xmovement = velocity * _root.sines[this.relativefacing];
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function movementofaturretshot()
{
    shiprotation = Math.round(_root.playershipfacing);
    maxshotvelocityforshot = Math.sqrt(_root.playershipvelocity * _root.playershipvelocity + gunshotvelocity * gunshotvelocity);
    angleofshotfromship = shiprotation - rotationtotarget;
    shotsanglefromship = offsetangle + angleofshotfromship;
    angletoshootbullet = Math.round(shiprotation - shotsanglefromship);
    turretrotation = angletoshootbullet;
    if (turretrotation > 360)
    {
        turretrotation = turretrotation - 360;
    }
    else if (turretrotation < 0)
    {
        turretrotation = turretrotation + 360;
    } // end else if
    shotsanglefromship = Math.round(shotsanglefromship);
    if (shotsanglefromship > 360)
    {
        shotsanglefromship = shotsanglefromship - 360;
    }
    else if (shotsanglefromship < 0)
    {
        shotsanglefromship = shotsanglefromship + 360;
    } // end else if
    shipsmomentum = _root.playershipvelocity * _root.cosines[shotsanglefromship];
    velocity = gunshotvelocity + shipsmomentum;
    relativefacing = turretrotation;
    movementofanobjectwiththrust();
} // End of the function
function getautofireatship()
{
    targetno = _root.targetinfo[8];
    if (_root.targetinfo[7] == "otherplayership")
    {
        targetx = _root.otherplayership[targetno][1] - _root.shipcoordinatex;
        targety = _root.otherplayership[targetno][2] - _root.shipcoordinatey;
    }
    else if (_root.targetinfo[7] == "aiship")
    {
        targetx = _root.aishipshosted[targetno][1] - _root.shipcoordinatex;
        targety = _root.aishipshosted[targetno][2] - _root.shipcoordinatey;
    }
    else if (_root.targetinfo[7] == "sectoritem")
    {
        targetx = _root.targetinfo[1] - _root.shipcoordinatex;
        targety = _root.targetinfo[2] - _root.shipcoordinatey;
    } // end else if
    enemyanglefromship = 180 - Math.atan2(targetx, targety) / mathconstant;
    return (Math.round(enemyanglefromship));
} // End of the function
function getmanulafirelocation()
{
    targetx = _root.gamedisplayarea._xmouse;
    targety = _root.gamedisplayarea._ymouse;
    enemyanglefromship = 180 - Math.atan2(targetx, targety) / mathconstant;
    return (Math.round(enemyanglefromship));
} // End of the function
function fireaturretsbullet()
{
    firetype = _root.playershipstatus[9];
    if (firetype == "AUTO")
    {
        this.rotationtotarget = getautofireatship();
    } // end if
    if (firetype == "MANUAL")
    {
        this.rotationtotarget = getmanulafirelocation();
    } // end if
    if (this.rotationtotarget > 360)
    {
        this.rotationtotarget = this.rotationtotarget - 360;
    } // end if
    if (this.rotationtotarget < 0)
    {
        this.rotationtotarget = this.rotationtotarget + 360;
    } // end if
    currentnumber = _root.playershotsfired.length;
    if (currentnumber < 1)
    {
        _root.playershotsfired = new Array();
        currentnumber = 0;
    } // end if
    movementofaturretshot();
    xcoordadjustment = xmovement;
    ycoordadjustment = ymovement;
    if (_root.currentplayershotsfired > 999)
    {
        _root.currentplayershotsfired = 0;
    } // end if
    i = _root.currentplayershotsfired;
    xfireposition = xonship;
    yfireposition = yonship;
    this._rotation = rotationtotarget - _root.playershipfacing;
    firingbulletstartlocation();
    _root.playershotsfired[currentnumber] = new Array();
    _root.playershotsfired[currentnumber][0] = i;
    _root.playershotsfired[currentnumber][1] = Math.round(xfirestart + _root.shipcoordinatex);
    _root.playershotsfired[currentnumber][2] = Math.round(yfirestart + _root.shipcoordinatey);
    _root.playershotsfired[currentnumber][3] = Math.round(xcoordadjustment);
    _root.playershotsfired[currentnumber][4] = Math.round(ycoordadjustment);
    _root.playershotsfired[currentnumber][5] = currentime + gunshotlifetime;
    _root.playershotsfired[currentnumber][6] = shottype;
    _root.playershotsfired[currentnumber][7] = turretrotation;
    _root.playershotsfired[currentnumber][8] = currentime + reloaddelay;
    timetillnextshot = currentime + reloaddelay;
    _root.playershotsfired[currentnumber][13] = "GUNS";
    _root.gamedisplayarea.attachMovie("guntype" + shottype + "fire", "playergunfire" + i, 5000 + i);
    setProperty("_root.gamedisplayarea.playergunfire" + i, _rotation, _root.playershotsfired[currentnumber][7]);
    setProperty("_root.gamedisplayarea.playergunfire" + i, _x, _root.playershotsfired[currentnumber][1] - _root.shipcoordinatex);
    setProperty("_root.gamedisplayarea.playergunfire" + i, _y, _root.playershotsfired[currentnumber][2] - _root.shipcoordinatey);
    set("_root.gamedisplayarea.playergunfire" + i + ".xmovement", xcoordadjustment);
    set("_root.gamedisplayarea.playergunfire" + i + ".ymovement", ycoordadjustment);
    set("_root.gamedisplayarea.playergunfire" + i + ".xposition", _root.playershotsfired[currentnumber][1]);
    set("_root.gamedisplayarea.playergunfire" + i + ".yposition", _root.playershotsfired[currentnumber][2]);
    globaltimestamp = String(getTimer() + _root.clocktimediff);
    globaltimestamp = Number(globaltimestamp.substr(globaltimestamp.length - 4));
    if (globaltimestamp > 10000)
    {
        globaltimestamp = globaltimestamp - 10000;
    } // end if
    _root.gunfireinformation = _root.gunfireinformation + ("GF`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[currentnumber][1] + "`" + _root.playershotsfired[currentnumber][2] + "`" + Math.round(velocity) + "`" + Math.round(turretrotation) + "`" + _root.playershotsfired[currentnumber][6] + "`" + i + "`" + globaltimestamp + "~");
    _root.playershotsfired[currentnumber][11] = _root.gamedisplayarea["playergunfire" + i]._width / 2;
    _root.playershotsfired[currentnumber][12] = _root.gamedisplayarea["playergunfire" + i]._height / 2;
    ++_root.currentplayershotsfired;
    if (_root.soundvolume != "off")
    {
        _root["guntype" + shottype + "sound"].start();
    } // end if
} // End of the function
this._rotation = 180;
reloaddelay = _root.guntype[shottype][2] * 1000;
timetillnextshot = _root.curenttime + reloaddelay;
gunshotlifetime = _root.guntype[shottype][1] * 1000;
gunshotvelocity = _root.guntype[shottype][0];
energyusedpershot = _root.guntype[shottype][3];
rangetofireashot = gunshotvelocity * gunshotlifetime * 2 / 3;
xfireposition = -this._x;
yfireposition = this._y;
hardpointfromcenter = Math.sqrt(xfireposition * xfireposition + yfireposition * yfireposition);
initialangleforgun = Math.ASIN(xfireposition / hardpointfromcenter) / 0.017453;
if (yfireposition < 0)
{
    flipshot = true;
} // end if
mathconstant = 0.017453;
checktimeinterval = 300;
currentchecktime = getTimer() + checktimeinterval;
this.onEnterFrame = function ()
{
    currentime = getTimer();
    if (currentchecktime < currentime)
    {
        currentchecktime = currentime + checktimeinterval;
        firetype = _root.playershipstatus[9];
        if (_root.targetinfo[0] != "None" && _root.targetinfo[10] == "enemy" || firetype == "MANUAL")
        {
            if (_root.targetinfo[3] < 500 || firetype == "MANUAL")
            {
                if (_root.playershipstatus[9] != "OFF" && timetillnextshot < currentime)
                {
                    if (_root.playershipstatus[1][1] > energyusedpershot)
                    {
                        _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energyusedpershot;
                        if (_root.playershipstatus[5][15] == "C")
                        {
                            _root.gamedisplayarea.keyboardscript.turnofshipcloak();
                        } // end if
                        fireaturretsbullet();
                    } // end if
                } // end if
            } // end if
        } // end if
    } // end if
};
stop ();
