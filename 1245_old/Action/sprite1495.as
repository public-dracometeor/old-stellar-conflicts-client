﻿// Action script...

// [onClipEvent of sprite 1435 in frame 1]
onClipEvent (load)
{
    function func_updateflaginfo(flagtime)
    {
        if (flagtime > 8)
        {
            datatosend = "KFLAG`INFO`" + _root.playershipstatus[3][0] + "`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + _root.errorchecknumber + "`~";
            _root.mysocket.send(datatosend);
        } // end if
    } // End of the function
    function func_playeroutofmap()
    {
        playerxgrid = _root.playershipstatus[6][0];
        playerygrid = _root.playershipstatus[6][1];
        if (playerxgrid < 0 || playerxgrid > _root.sectorinformation[0][0])
        {
            outofbounds = true;
        }
        else if (playerygrid < 0 || playerygrid > _root.sectorinformation[0][1])
        {
            outofbounds = true;
        }
        else
        {
            outofbounds = false;
        } // end else if
        return (outofbounds);
    } // End of the function
    function func_runflagscript()
    {
        this._visible = true;
        currentnoofplayer = _root.currentonlineplayers.length;
        func_setrewards(currentnoofplayer);
        timeupdate = getTimer() + timeupdateint;
        this.onEnterFrame = function ()
        {
            if (timeupdate < getTimer())
            {
                func_updateflaginfo(flagtime);
                timeupdate = getTimer() + timeupdateint;
            } // end if
            ++currentupdateframe;
            if (currentupdateframe >= updateframeat)
            {
                currentupdateframe = 0;
                flagtime = Math.round((_root.kingofflag[1] - getTimer()) / 1000);
                if (currentnoofplayer != _root.currentonlineplayers.length)
                {
                    currentnoofplayer = _root.currentonlineplayers.length;
                    func_setrewards(currentnoofplayer);
                }
                else if (_root.playershipstatus[2][5] <= 0)
                {
                    if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
                    {
                        datatosend = "KFLAG`DROP`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + _root.errorchecknumber + "~";
                        _root.mysocket.send(datatosend);
                        this._visible = false;
                        this.onEnterFrame = function ()
                        {
                        };
                    } // end if
                }
                else if (flagtime < 0)
                {
                    flagtime = 0;
                    if (_root.playershipstatus[2][5] > 0 && _root.kingofflag[0] == _root.playershipstatus[3][0])
                    {
                        datatosend = "KFLAG`END`" + _root.playershipstatus[3][0] + "`" + scoreworth * _root.scoreratiomodifier + "`" + fundsworth + "`" + _root.errorchecknumber + "~";
                        _root.mysocket.send(datatosend);
                        this._visible = false;
                        this.onEnterFrame = function ()
                        {
                        };
                    } // end if
                }
                else if (_root.kingofflag[0] != _root.playershipstatus[3][0])
                {
                    this._visible = false;
                    this.onEnterFrame = function ()
                    {
                    };
                }
                else if (func_playeroutofmap() == true)
                {
                    message = "HOST: You Must Keep the Flag on the Map";
                    _root.enterintochat(message, _root.systemchattextcolor);
                    location = _root.fun_kingofflagrandomcoords();
                    xcoord = location[0];
                    ycoord = location[1];
                    datatosend = "KFLAG`DROP`" + Math.round(xcoord) + "`" + Math.round(ycoord) + "`" + _root.errorchecknumber + "~";
                    _root.mysocket.send(datatosend);
                    this._visible = false;
                    this.onEnterFrame = function ()
                    {
                    };
                } // end else if
            } // end else if
        };
    } // End of the function
    function func_setrewards(noofplayers)
    {
        scoreworth = _root.kingofflag[11] + _root.kingofflag[12] * noofplayers;
        fundsworth = _root.kingofflag[13] + _root.kingofflag[14] * noofplayers;
    } // End of the function
    this._visible = false;
    flagtime = "";
    scoreworth = "";
    fundsworth = "";
    currentupdateframe = 0;
    updateframeat = 40;
    timeupdate = 0;
    timeupdateint = _root.kingofflag[15];
}

// [onClipEvent of sprite 1465 in frame 1]
onClipEvent (load)
{
    function showenergy(energy)
    {
        this.scale = Math.ceil(energy / maxenergy * 28);
        if (scale < 1)
        {
            scale = 1;
        } // end if
        this.gotoAndStop(this.scale);
    } // End of the function
    this.gotoAndStop(28);
    currentenergycapacitor = _root.playershipstatus[1][5];
    maxenergy = _root.energycapacitors[currentenergycapacitor][0];
}

// [onClipEvent of sprite 1493 in frame 1]
onClipEvent (load)
{
    function showshield(shield)
    {
        scale = Math.ceil(shield / maxshieldstrength * 28);
        if (scale < 1)
        {
            scale = 1;
        } // end if
        this.gotoAndStop(this.scale);
    } // End of the function
    playercurrentshieldgenerator = _root.playershipstatus[2][0];
    maxshieldstrength = _root.shieldgenerators[playercurrentshieldgenerator][0];
    showshield(maxshieldstrength);
    this.gotoAndStop(28);
}

// [onClipEvent of sprite 1494 in frame 1]
onClipEvent (load)
{
    this.gotoAndStop(28);
    maxstructure = _root.shiptype[_root.playershipstatus[5][0]][3][3];
}

// [onClipEvent of sprite 1494 in frame 1]
onClipEvent (enterFrame)
{
    structure = _root.playershipstatus[2][5];
    if (laststructure != structure)
    {
        laststructure = structure;
        lastscale = scale;
        scale = Math.ceil(_root.playershipstatus[2][5] / maxstructure * 28);
        if (scale < 1)
        {
            scale = 1;
        } // end if
        if (lastscale != scale)
        {
            this.gotoAndStop(this.scale);
        } // end if
    } // end if
}
