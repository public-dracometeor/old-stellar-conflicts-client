﻿// Action script...

// [onClipEvent of sprite 641 in frame 1]
onClipEvent (load)
{
    function enterintochat(info, colourtouse)
    {
        _root.gamechatinfo[1].splice(0, 0, 0);
        _root.gamechatinfo[1][0] = new Array();
        _root.gamechatinfo[1][0][0] = info;
        _root.gamechatinfo[1][0][1] = colourtouse;
        if (_root.gamechatinfo[1].length > _root.gamechatinfo[2][1])
        {
            _root.gamechatinfo[1].splice(_root.gamechatinfo[2][1]);
        } // end if
        _root.refreshchatdisplay = true;
    } // End of the function
    function create_assassinationlist()
    {
        assasarray = new Array();
        if (_root.currentonlineplayers.length > 4)
        {
            for (i = 0; i < _root.currentonlineplayers.length; i++)
            {
                if (_root.currentonlineplayers[i][1] != _root.playershipstatus[3][2] && (_root.currentonlineplayers[i][4] != _root.playershipstatus[5][2] || _root.playershipstatus[5][2] == "N/A"))
                {
                    playname = _root.currentonlineplayers[i][1];
                    theirsocre = _root.currentonlineplayers[i][5];
                    basereward = 50000;
                    modifier = 0;
                    if (theirsocre > yourscore + 500000 * _root.scoreratiomodifier)
                    {
                        modifier = 60000;
                    }
                    else if (theirsocre > yourscore + 250000 * _root.scoreratiomodifier)
                    {
                        modifier = 40000;
                    }
                    else if (theirsocre > yourscore + 50000 * _root.scoreratiomodifier)
                    {
                        modifier = 20000;
                    }
                    else if (theirsocre < yourscore - 50000)
                    {
                        modifier = -20000;
                    } // end else if
                    reward = basereward + modifier;
                    assasarray[assasarray.length] = new Array("assasinate`" + playname, null, null, null, null, null, reward, null);
                } // end if
            } // end of for
        } // end if
    } // End of the function
    function func_findassasnameinarr(nametof)
    {
        splitter = _root.playersmission[0][0][0].split("`");
        name = splitter[1];
        loc = "N/A";
        for (i = 0; i < assasarray.length; i++)
        {
            spliiter = assasarray[i][0].split("`");
            assname = spliiter[1];
            if (nametof == assname)
            {
                loc = i;
                break;
            } // end if
        } // end of for
        return (loc);
    } // End of the function
    function drawmissions(currentleftmostitem)
    {
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < missionstoselect.length - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        if (maxitemstostoshow <= missionstoselect.length)
        {
            drawarrows(islefton, isrighton);
        } // end if
        for (i = 0; i < maxitemstostoshow && i < missionstoselect.length; i++)
        {
            if (missionstoselect[currentleftmostitem + i] < _root.missionsavailable.length)
            {
                missioninfosplit = _root.missionsavailable[missionstoselect[currentleftmostitem + i]][0].split("`");
                reward = _root.missionsavailable[missionstoselect[currentleftmostitem + i]][6];
            }
            else
            {
                missioninfosplit = assasarray[missionstoselect[currentleftmostitem + i] - _root.missionsavailable.length][0].split("`");
                reward = assasarray[missionstoselect[currentleftmostitem + i] - _root.missionsavailable.length][6];
            } // end else if
            if (missioninfosplit[1].substr(0, 2) == "NP")
            {
                missioninfosplit[1] = "Nav " + missioninfosplit[1].substr(2);
            } // end if
            this.attachMovie("newmissionchoicebox", "newmissionchoicebox" + i, 5000 + i);
            setProperty("newmissionchoicebox" + i, _x, shiplocationx + shipspacingx * i);
            setProperty("newmissionchoicebox" + i, _y, shiplocationy);
            set(this + ".newmissionchoicebox" + i + ".itemname", missioninfosplit[0] + " at " + missioninfosplit[1]);
            set(this + ".newmissionchoicebox" + i + ".missionno", missionstoselect[currentleftmostitem + i]);
            set(this + ".newmissionchoicebox" + i + ".itemcost", "Status: Available");
            set(this + ".newmissionchoicebox" + i + ".worth", "Reward: " + reward);
            if (missionstoselect[currentleftmostitem + i] == _root.playersmission[0][0][2])
            {
                set(this + ".newmissionchoicebox" + i + ".itemcost", "Status: Current");
            } // end if
            if (missioninfosplit[0] == "Recon")
            {
                set(this + ".newmissionchoicebox" + i + ".itemspecs", " In this Mission you must proceed to " + missioninfosplit[1] + ". Where you will do a Recon of this Area.  You must remain at " + missioninfosplit[1] + ", at a range of less than " + _root.missionsavailable[missionstoselect[currentleftmostitem + i]][4] + " for " + _root.missionsavailable[missionstoselect[currentleftmostitem + i]][3] / 1000 + " seconds, to complete this Mission." + " \r Elimination of Enemy ships is NOT require.");
                continue;
            } // end if
            if (missioninfosplit[0] == "Patrol")
            {
                set(this + ".newmissionchoicebox" + i + ".itemspecs", " In this Mission you must proceed to " + missioninfosplit[1] + ". Where you will do a Patrol of this Area.  You must remain at " + missioninfosplit[1] + ", at a range of less than " + _root.missionsavailable[missionstoselect[currentleftmostitem + i]][4] + " for " + _root.missionsavailable[missionstoselect[currentleftmostitem + i]][3] / 1000 + " seconds. No hostile ships must be within the range of " + _root.missionsavailable[missionstoselect[currentleftmostitem + i]][4] + " in order to complete this Mission." + " \rIf Hostile ships are present, you must scare them away, or destroy them!");
                continue;
            } // end if
            if (missioninfosplit[0] == "Cargorun")
            {
                set(this + ".newmissionchoicebox" + i + ".itemname", "Cargo Run To " + missioninfosplit[2]);
                destination = missioninfosplit[2];
                if (destination.substr(0, 2) == "PL")
                {
                    destination = destination.substr(2);
                } // end if
                set(this + ".newmissionchoicebox" + i + ".itemspecs", " In this Mission you begin at your current base. Where you will automatically pick up your cargo.  You must proceed to " + destination + " to automatically drop it off.  If you can do this run in under " + Math.floor(_root.missionsavailable[missionstoselect[currentleftmostitem + i]][3] / 1000) + " seconds, you will receive a bonus of " + _root.missionsavailable[missionstoselect[currentleftmostitem + i]][7] + " credits. " + " \r Docking at any other bases after you pick up your cargo, will result in a failure!");
                continue;
            } // end if
            if (missioninfosplit[0] == "assasinate")
            {
                set(this + ".newmissionchoicebox" + i + ".itemname", "Assassinate " + missioninfosplit[1]);
                set(this + ".newmissionchoicebox" + i + ".itemspecs", " In this Mission you will be required to assasinate a player in this zone. You will have to destroy " + missioninfosplit[1] + "\'s ship to complete this mission.  " + "If this player leaves this zone before you destroy them, this mission will end unsuccessfully.");
                continue;
            } // end if
            set(this + ".newmissionchoicebox" + i + ".itemspecs", "No Information Available");
        } // end of for
    } // End of the function
    function drawarrows(islefton, isrighton)
    {
        this.attachMovie("hardwareselectleft", "hardwareselectleft", 1001);
        this.hardwareselectleft._x = shiplocationx - 100;
        this.hardwareselectleft._y = shiplocationy;
        this.hardwareselectleft._alpha = 60;
        if (islefton == true)
        {
            this.hardwareselectleft._alpha = 100;
        } // end if
        this.attachMovie("hardwareselectright", "hardwareselectright", 1002);
        this.hardwareselectright._x = shiplocationx + 140 + shipspacingx * (maxitemstostoshow - 1);
        this.hardwareselectright._y = shiplocationy;
        this.hardwareselectright._alpha = 60;
        if (isrighton == true)
        {
            this.hardwareselectright._alpha = 100;
        } // end if
    } // End of the function
    removeMovieClip ("_root.starbasehardwarebutton");
    removeMovieClip ("_root.exitstarbasebutton");
    removeMovieClip ("_root.purchaseshipbutton");
    removeMovieClip ("_root.purchasetradegoodsbutton");
    waitingforanswer = false;
    missionselection = null;
    chosenamission = false;
    shiplocationx = -_root.gameareawidth / 3 + 15;
    shiplocationy = -_root.gameareaheight / 8;
    shipspacingx = 185;
    shipspacingy = 200;
    this._y = _root.gameareaheight / 2;
    this._x = _root.gameareawidth / 2;
    this._height = _root.gameareaheight;
    this._width = _root.gameareawidth;
    this.attachMovie("shopexitbutton", "purchaseshipexitbutton", 150);
    this.purchaseshipexitbutton._y = _root.gameareaheight / 2 - 50;
    this.purchaseshipexitbutton._x = _root.gameareawidth / 2 - 100;
    this.attachMovie("starbasefundsdisplay", "funds", 121);
    this.funds._y = -_root.gameareaheight / 2 + 15;
    this.funds._x = _root.gameareawidth / 2 - 100;
    maxitemstostoshow = 3;
    currentleftmostitem = 0;
    create_assassinationlist();
    missionstoselect = new Array();
    if (_root.playersmission.length > 0)
    {
        if (_root.playersmission[0][0][2] < _root.missionsavailable.length)
        {
            missionstoselect[0] = _root.playersmission[0][0][2];
        }
        else
        {
            locater = func_findassasnameinarr(_root.playersmission[0][0][1]);
            if (locater != "N/A")
            {
                missionstoselect[0] = _root.missionsavailable.length + locater;
            } // end if
        } // end if
    } // end else if
    for (i = _root.playersmission.length; i < 8; i++)
    {
        if (i == _root.missionsavailable.length + assasarray.length)
        {
            break;
        } // end if
        missionstoselect[i] = Math.round(Math.random() * (_root.missionsavailable.length + assasarray.length - 1));
        ismissiontaken = false;
        for (j = 0; j < missionstoselect.length - 1; j++)
        {
            if (missionstoselect[j] == missionstoselect[i])
            {
                ismissiontaken = true;
            } // end if
        } // end of for
        if (_root.missionsavailable[missionstoselect[i]][0].substr(0, 8) == "Cargorun")
        {
            currentinfo = _root.missionsavailable[missionstoselect[i]][0].split("`");
            if (currentinfo[1] != _root.playershipstatus[4][0])
            {
                ismissiontaken = true;
            } // end if
        } // end if
        if (ismissiontaken == true)
        {
            --i;
        } // end if
    } // end of for
    drawmissions(currentleftmostitem);
}

// [onClipEvent of sprite 641 in frame 1]
onClipEvent (enterFrame)
{
    if (moveitemsdisplayed != null)
    {
        currentleftmostitem = currentleftmostitem + moveitemsdisplayed;
        moveitemsdisplayed = null;
        if (currentleftmostitem < 0)
        {
            currentleftmostitem = 0;
        } // end if
        if (currentleftmostitem > _root.missionsavailable)
        {
            currentleftmostitem = currentleftmostitem - 1;
        } // end if
        drawmissions(currentleftmostitem);
    } // end if
}

// [onClipEvent of sprite 641 in frame 1]
onClipEvent (enterFrame)
{
    function acceptmissioin(i)
    {
        _root.playersmission = new Array();
        _root.playersmission[0] = new Array();
        _root.playersmission[0][0] = new Array();
        if (i < _root.missionsavailable.length)
        {
            missioninfo = _root.missionsavailable[i][0].split("`");
        }
        else
        {
            num = i - _root.missionsavailable.length;
            missioninfo = assasarray[num][0].split("`");
        } // end else if
        if (missioninfo[0] == "Patrol" || missioninfo[0] == "Recon")
        {
            if (missioninfo[1].substr(0, 2) == "NP")
            {
                missioninfo[1] = "Nav " + missioninfo[1].substr(2);
            } // end if
            _root.playersmission[0][0][0] = missioninfo[0];
            _root.playersmission[0][0][1] = missioninfo[1];
            _root.playersmission[0][0][2] = i;
            _root.playersmission[0][1] = new Array();
            _root.playersmission[0][1][0] = new Array();
            _root.playersmission[0][1][0][0] = "Nav1";
            _root.playersmission[0][1][0][1] = _root.missionsavailable[i][1];
            _root.playersmission[0][1][0][2] = _root.missionsavailable[i][2];
            _root.playersmission[0][1][0][3] = "Pending";
            _root.playersmission[0][1][0][4] = Math.round(Math.random() * Number(_root.missionsavailable[i][5]));
            if (missioninfo[0] == "Patrol" && _root.playersmission[0][1][0][4] <= 0)
            {
                _root.playersmission[0][1][0][4] = 1;
            } // end if
            _root.playersmission[0][2] = _root.missionsavailable[i][6];
            _root.playersmission[0][3] = _root.missionsavailable[i][3];
            _root.playersmission[0][4] = _root.missionsavailable[i][4];
        }
        else if (missioninfo[0] == "Cargorun")
        {
            _root.playersmission[0][0][0] = missioninfo[0];
            _root.playersmission[0][0][1] = missioninfo[1] + "`" + missioninfo[2];
            _root.playersmission[0][0][2] = i;
            _root.playersmission[0][2] = _root.missionsavailable[i][6] + "`" + _root.missionsavailable[i][7];
            _root.playersmission[0][3] = _root.missionsavailable[i][3];
            _root.playersmission[0][4] = false;
            if (missioninfo[1] == _root.playershipstatus[4][0])
            {
                _root.playersmission[0][4] = true;
                enterintochat(" Misson Cargo Received, Mission begins when you exit the base.", _root.systemchattextcolor);
            } // end if
            num = i - _root.missionsavailable.length;
            missioninfosplit = assasarray[num][0].split("`");
        }
        else if (missioninfo[0] == "assasinate")
        {
            _root.playersmission[0][0][0] = missioninfo[0];
            _root.playersmission[0][0][1] = missioninfo[1];
            _root.playersmission[0][0][2] = i;
            _root.playersmission[0][2] = assasarray[num][6];
            _root.func_assmissionbeg(missioninfo[1]);
        } // end else if
    } // End of the function
    if (itemselection != null)
    {
    } // end if
    if (itemselection != null && waitingforanswer != true)
    {
        removeMovieClip (this.shipwarningbox);
        this.attachMovie("buyingashipquestion", "buyingashipquestion", 9980);
        if (itemselection < _root.missionsavailable.length)
        {
            missioninfosplit = _root.missionsavailable[itemselection][0].split("`");
            if (missioninfosplit[1].substr(0, 2) == "NP")
            {
                missioninfosplit[1] = "Nav " + missioninfosplit[1].substr(2);
                missiontitle = missioninfosplit[0] + " at " + missioninfosplit[1];
            } // end if
        }
        else
        {
            num = itemselection - _root.missionsavailable.length;
            missioninfosplit = assasarray[num][0].split("`");
            missiontitle = "Assassinate " + missioninfosplit[1];
        } // end else if
        this.buyingashipquestion.question = "Accept Mission:\r" + missiontitle;
        waitingforanswer = true;
    } // end if
    if (this.buyingashipquestion.requestedanswer == true && waitingforanswer == true)
    {
        acceptmissioin(itemselection);
        removeMovieClip (this.buyingashipquestion);
        drawmissions(currentleftmostitem);
        itemselection = null;
        waitingforanswer = false;
    } // end if
    if (this.buyingashipquestion.requestedanswer == false && waitingforanswer == true)
    {
        waitingforanswer = false;
        removeMovieClip (this.buyingashipquestion);
        itemselection = null;
    } // end if
}

// [Action in Frame 1]
stop ();
