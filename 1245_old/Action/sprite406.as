﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    if (itemselection != null)
    {
        specialtype = itemselection.substring(0, 11);
        specialselected = itemselection.substring(11);
    } // end if
    if (itemselection != null && specialtype == "specialtype" && buyingaspecial != true)
    {
        noofspecials = _root.playershipstatus[11][1].length;
        shiptype = _root.playershipstatus[5][0];
        maxitems = _root.shiptype[shiptype][3][7];
        specialcost = _root.specialshipitems[specialselected][6];
        if (noofspecials >= maxitems)
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
            this.hardwarewarningbox.information = "Specials Full";
        }
        else if (_root.playershipstatus[3][1] < specialcost)
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
            this.hardwarewarningbox.information = "Not Enough Funds";
        }
        else
        {
            buyingaspecial = true;
            removeMovieClip (this.hardwarewarningbox);
            attachMovie("specialsinformationbutton", "buyingaspecialquestion", 9980);
            specialselected = Number(specialselected);
            this.buyingaspecialquestion.specialselected = specialselected;
            this.buyingaspecialquestion.specialcost = _root.specialshipitems[specialselected][6];
            this.buyingaspecialquestion.specialquantity = _root.specialshipitems[specialselected][1];
        } // end else if
        buyingaspecial = false;
        itemselection = null;
    } // end if
}

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (load)
{
    function hardwareshopmenulabelspecialsmenuopen(currentleftmostitem)
    {
        playershiptype = _root.playershipstatus[5][0];
        availableitems = new Array();
        _root.shiptype[currentship][3][8] = false;
        for (q = 0; q < _root.specialshipitems.length; q++)
        {
            if (_root.specialshipitems[q][5] == "CLOAK")
            {
                if (_root.shiptype[playershiptype][3][8] == true)
                {
                    availableitems[availableitems.length] = _root.specialshipitems[q];
                } // end if
                continue;
            } // end if
            availableitems[availableitems.length] = _root.specialshipitems[q];
        } // end of for
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < availableitems.length - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < availableitems.length; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", availableitems[currentleftmostitem + i][0]);
            set(this + ".hardwareitembox" + i + ".itemtype", "specialtype" + availableitems[currentleftmostitem + i][7]);
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + availableitems[currentleftmostitem + i][6]);
            if (availableitems[currentleftmostitem + i][5] == "FLARE")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Drain: " + availableitems[currentleftmostitem + i][2] + " \r" + "Reload :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r" + "Life Time: " + availableitems[currentleftmostitem + i][3] / 1000 + "/s \r" + "%Chance :" + availableitems[currentleftmostitem + i][10] * 100 + "% \r");
            }
            else if (availableitems[currentleftmostitem + i][5] == "MINES")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Reload :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r" + "Life Time: " + availableitems[currentleftmostitem + i][3] / 1000 + "/s \r" + "Damage :" + availableitems[currentleftmostitem + i][12] + " \r" + "Not Effective on Ai");
            }
            else if (availableitems[currentleftmostitem + i][5] == "DETECTOR" || availableitems[currentleftmostitem + i][5] == "STEALTH" || availableitems[currentleftmostitem + i][5] == "CLOAK")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Drain: " + availableitems[currentleftmostitem + i][2] + " \r" + "ReCharge :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r" + "Life Time: " + availableitems[currentleftmostitem + i][3] / 1000 + "/s \r" + "%Chance :" + availableitems[currentleftmostitem + i][10] * 100 + "% \r" + "CheckTime: " + availableitems[currentleftmostitem + i][11] / 1000 + "/s");
            }
            else if (availableitems[currentleftmostitem + i][5] == "PULSAR")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Need: " + availableitems[currentleftmostitem + i][2] + " \r" + "Shutdown Time: " + availableitems[currentleftmostitem + i][11] / 1000 + " s\r" + "Radius :" + availableitems[currentleftmostitem + i][12] + " \r" + "QTY: " + availableitems[currentleftmostitem + i][1]);
            } // end else if
            if (availableitems[currentleftmostitem + i][5] == "RECHARGESHIELD")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Conversion: \r 1 Energy:" + availableitems[currentleftmostitem + i][2] + " Shield \r" + "ReCharge :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r");
            } // end if
            if (availableitems[currentleftmostitem + i][5] == "RECHARGESTRUCT")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Conversion: \r 1 Energy:" + availableitems[currentleftmostitem + i][2] + " Struct \r" + "ReCharge :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r");
            } // end if
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreenspecialsinfo", "hardwarescreenspecialsinfo", 4);
        setProperty("shipblueprint.hardwarescreenspecialsinfo", _x, _root.gameareawidth / 2 - 100);
        setProperty("shipblueprint.hardwarescreenspecialsinfo", _y, -100);
    } // End of the function
    function hardwareshopmenulabelspecialsclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip ("hardwareinformationbutton");
        removeMovieClip ("buyingaspecialquestion");
        for (i = 0; i < _root.guntype.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
        itemselection = null;
        buyingaspecial = false;
    } // End of the function
}

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (load)
{
    turretcostmodifier = 2;
    removeMovieClip ("_root.starbasehardwarebutton");
    removeMovieClip ("_root.exitstarbasebutton");
    removeMovieClip ("_root.purchaseshipbutton");
    removeMovieClip ("_root.purchasetradegoodsbutton");
    moveitemsdisplayed = null;
    itemselection = null;
    playershiptype = _root.playershipstatus[5][0];
    sellingagun = false;
    leftopbarx = -_root.gameareawidth / 3;
    leftopbary = -_root.gameareaheight / 8;
    leftbarspacing = 42;
    shopitemsx = leftopbarx + 200;
    shopitemsy = leftopbary - 100;
    shopitemspacingx = 125;
    maxitemstostoshow = 3;
    this._y = _root.gameareaheight / 2;
    this._x = _root.gameareawidth / 2;
    this._height = _root.gameareaheight;
    this._width = _root.gameareawidth;
    playershiptype = _root.playershipstatus[5][0];
    this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
    setProperty("shipblueprint", _x, _root.gameareawidth / 8);
    setProperty("shipblueprint", _y, _root.gameareaheight / 8);
    if (_root.shiptype[playershiptype][5].length > 0)
    {
        this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelturrets", 98);
        this.hardwareshopmenulabelturrets.itemname = "Turrets";
        this.hardwareshopmenulabelturrets.operation = "hardwareshopmenulabelturrets";
        this.hardwareshopmenulabelturrets._y = leftopbary - (leftbarspacing + leftbarspacing);
        this.hardwareshopmenulabelturrets._x = leftopbarx;
    } // end if
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelspecials", 99);
    this.hardwareshopmenulabelspecials.itemname = "Specials";
    this.hardwareshopmenulabelspecials.operation = "hardwareshopmenulabelspecials";
    this.hardwareshopmenulabelspecials._y = leftopbary - leftbarspacing;
    this.hardwareshopmenulabelspecials._x = leftopbarx;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelguns", 100);
    this.hardwareshopmenulabelguns.itemname = "Weapons/Guns";
    this.hardwareshopmenulabelguns.operation = "hardwareshopmenulabelguns";
    this.hardwareshopmenulabelguns._y = leftopbary;
    this.hardwareshopmenulabelguns._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelmissiles", 106);
    this.hardwareshopmenulabelmissiles.itemname = "Missiles";
    this.hardwareshopmenulabelmissiles.operation = "hardwareshopmenulabelmissiles";
    this.hardwareshopmenulabelmissiles._y = leftopbary;
    this.hardwareshopmenulabelmissiles._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelshield", 101);
    this.hardwareshopmenulabelshield.itemname = "Shield Generators";
    this.hardwareshopmenulabelshield.operation = "hardwareshopmenulabelshield";
    this.hardwareshopmenulabelshield._y = leftopbary;
    this.hardwareshopmenulabelshield._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelenergy", 102);
    this.hardwareshopmenulabelenergy.itemname = "Energy Generators";
    this.hardwareshopmenulabelenergy.operation = "hardwareshopmenulabelenergy";
    this.hardwareshopmenulabelenergy._y = leftopbary;
    this.hardwareshopmenulabelenergy._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelcapacitors", 103);
    this.hardwareshopmenulabelcapacitors.itemname = "Energy Capacitors";
    this.hardwareshopmenulabelcapacitors.operation = "hardwareshopmenulabelcapacitors";
    this.hardwareshopmenulabelcapacitors._y = leftopbary;
    this.hardwareshopmenulabelcapacitors._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("shopexitbutton", "hardwareshopexitbutton", 120);
    this.hardwareshopexitbutton._x = _root.gameareawidth / 2 - 50;
    this.hardwareshopexitbutton._y = _root.gameareaheight / 2 - 70;
    this.attachMovie("starbasefundsdisplay", "funds", 121);
    this.funds._y = -_root.gameareaheight / 2 + 15;
    this.funds._x = _root.gameareawidth / 2 - 100;
    currentleftmostitem = 0;
    lasttleftmostitem = 0;
    operation = "hardwareshopmenulabelguns";
}

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    function drawarrows(islefton, isrighton)
    {
        this.attachMovie("hardwareselectleft", "hardwareselectleft", 1001);
        this.hardwareselectleft._x = shopitemsx - 80;
        this.hardwareselectleft._y = shopitemsy;
        this.hardwareselectleft._alpha = 60;
        if (islefton == true)
        {
            this.hardwareselectleft._alpha = 100;
        } // end if
        this.attachMovie("hardwareselectright", "hardwareselectright", 1002);
        this.hardwareselectright._x = shopitemsx + 80 + shopitemspacingx * (maxitemstostoshow - 1);
        this.hardwareselectright._y = shopitemsy;
        this.hardwareselectright._alpha = 60;
        if (isrighton == true)
        {
            this.hardwareselectright._alpha = 100;
        } // end if
    } // End of the function
    function hardwareshopmenulabelturretsmenuopen()
    {
        turretcostmodifier = 2;
        gunlocationmultiplier = _root.shiptype[playershiptype][3][4];
        shipstopgun = _root.shiptype[playershiptype][6][3] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < shipstopgun - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < shipstopgun; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.guntype[currentleftmostitem + i][6]);
            set(this + ".hardwareitembox" + i + ".itemtype", "turrettype" + (currentleftmostitem + i));
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + _root.guntype[currentleftmostitem + i][5] * turretcostmodifier);
            set(this + ".hardwareitembox" + i + ".itemspecs", "Damage: " + _root.guntype[currentleftmostitem + i][4] + " \r" + "Energy Drain: " + _root.guntype[currentleftmostitem + i][3] + " \r" + "Reload :" + _root.guntype[currentleftmostitem + i][2] + "/s \r" + "Life Time: " + _root.guntype[currentleftmostitem + i][1] + "/s \r" + "Speed :" + _root.guntype[currentleftmostitem + i][0] + "/s \r");
        } // end of for
        for (i = 0; i < _root.shiptype[playershiptype][5].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracket" + i, 5 + i);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _x, _root.shiptype[playershiptype][5][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _y, _root.shiptype[playershiptype][5][i][1] * gunlocationmultiplier);
            set(this.shipblueprint + ".hardwarescreengunbracket" + i + ".hardpointname", "Turret " + i);
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracketsell", 4);
        setProperty("shipblueprint.hardwarescreengunbracketsell", _x, _root.gameareawidth / 2 - 100);
        setProperty("shipblueprint.hardwarescreengunbracketsell", _y, -100);
        set(this.shipblueprint + ".hardwarescreengunbracketsell.hardpointname", "Sell Turret");
        for (i = 0; i < _root.shiptype[playershiptype][5].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[8][i][0], "gun" + i, 12 + i);
            setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][5][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][5][i][1] * gunlocationmultiplier);
            set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[8][i][0]][6]);
        } // end of for
        itemselection = null;
        buyingaturret = false;
        sellingaturret = false;
    } // End of the function
    function hardwareshopmenulabelturretsmenuclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip ("hardwareinformationbutton");
        removeMovieClip ("buyaturretquestion");
        for (i = 0; i < _root.guntype.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
        itemselection = null;
        buyingaturret = false;
        sellingaturret = false;
    } // End of the function
    function hardwareshopmenulabelgunsmenuopen(currentleftmostitem)
    {
        gunlocationmultiplier = _root.shiptype[playershiptype][3][4];
        shipstopgun = _root.shiptype[playershiptype][6][3] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < shipstopgun - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < shipstopgun; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.guntype[currentleftmostitem + i][6]);
            set(this + ".hardwareitembox" + i + ".itemtype", "guntype" + (currentleftmostitem + i));
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + _root.guntype[currentleftmostitem + i][5]);
            set(this + ".hardwareitembox" + i + ".itemspecs", "Damage: " + _root.guntype[currentleftmostitem + i][4] + " \r" + "Energy Drain: " + _root.guntype[currentleftmostitem + i][3] + " \r" + "Reload :" + _root.guntype[currentleftmostitem + i][2] + "/s \r" + "Life Time: " + _root.guntype[currentleftmostitem + i][1] + "/s \r" + "Speed :" + _root.guntype[currentleftmostitem + i][0] + "/s \r");
        } // end of for
        for (i = 0; i < _root.shiptype[playershiptype][2].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracket" + i, 5 + i);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
            set(this.shipblueprint + ".hardwarescreengunbracket" + i + ".hardpointname", "Hardpoint " + i);
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracketsell", 4);
        setProperty("shipblueprint.hardwarescreengunbracketsell", _x, _root.gameareawidth / 2 - 100);
        setProperty("shipblueprint.hardwarescreengunbracketsell", _y, -100);
        set(this.shipblueprint + ".hardwarescreengunbracketsell.hardpointname", "Sell Weapon");
        for (i = 0; i < _root.shiptype[playershiptype][2].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[0][i][0], "gun" + i, 12 + i);
            setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
            set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[0][i][0]][6]);
        } // end of for
        buyingagun = false;
        sellingagun = false;
    } // End of the function
    function hardwareshopmenulabelgunsmenuclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip ("hardwareinformationbutton");
        removeMovieClip ("buyagunquestion");
        for (i = 0; i < _root.guntype.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
        itemselection = null;
        buyingagun = false;
        sellingagun = false;
    } // End of the function
    function hardwareshopmenulabelmissilesopen(currentleftmostitem)
    {
        gunlocationmultiplier = _root.shiptype[playershiptype][3][4];
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < _root.missile.length - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < _root.missile.length; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.missile[currentleftmostitem + i][6]);
            set(this + ".hardwareitembox" + i + ".itemtype", "missiletype" + (currentleftmostitem + i));
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + _root.missile[currentleftmostitem + i][5]);
            if (_root.missile[currentleftmostitem + i][8] == "DUMBFIRE" || _root.missile[currentleftmostitem + i][8] == "SEEKER")
            {
                if (_root.missile[currentleftmostitem + i][8] == "DUMBFIRE")
                {
                    missileguidance = "Dumbfire";
                }
                else if (_root.missile[currentleftmostitem + i][8] == "SEEKER")
                {
                    missileguidance = "Seeker";
                } // end else if
                set(this + ".hardwareitembox" + i + ".itemspecs", "Damage: " + _root.missile[currentleftmostitem + i][4] + " \r" + "Reload :" + _root.missile[currentleftmostitem + i][3] / 1000 + "/s \r" + "Life Time: " + _root.missile[currentleftmostitem + i][2] / 1000 + "/s \r" + "Speed :" + _root.missile[currentleftmostitem + i][0] + "/s \r" + "Guidance: " + missileguidance);
            } // end if
            if (_root.missile[currentleftmostitem + i][8] == "EMP" || _root.missile[currentleftmostitem + i][8] == "DISRUPTER")
            {
                if (_root.missile[currentleftmostitem + i][8] == "EMP")
                {
                    missileguidance = "E.M.P.";
                }
                else if (_root.missile[currentleftmostitem + i][8] == "DISRUPTER")
                {
                    missileguidance = "Disrupter";
                } // end else if
                set(this + ".hardwareitembox" + i + ".itemspecs", "Reload :" + _root.missile[currentleftmostitem + i][3] / 1000 + "/s \r" + "Life Time: " + _root.missile[currentleftmostitem + i][2] / 1000 + "/s \r" + "Speed :" + _root.missile[currentleftmostitem + i][0] + "/s \r" + "Duration: " + Math.floor(_root.missile[currentleftmostitem + i][9] / 1000) + " \r" + "Guidance: " + missileguidance);
            } // end if
        } // end of for
        for (i = 0; i < _root.shiptype[playershiptype][7].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracket" + i, 5 + i);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _x, _root.shiptype[playershiptype][7][i][2] * gunlocationmultiplier);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _y, _root.shiptype[playershiptype][7][i][3] * gunlocationmultiplier);
            set(this.shipblueprint + ".hardwarescreengunbracket" + i + ".hardpointname", "Missile Bank " + i);
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreenmissilebankinfo", "hardwarescreenmissilebankinfo", 4);
        setProperty("shipblueprint.hardwarescreenmissilebankinfo", _x, _root.gameareawidth / 2 - 100);
        setProperty("shipblueprint.hardwarescreenmissilebankinfo", _y, -100);
        set(this.shipblueprint + ".hardwarescreenmissilebankinfo.bankno", currentmissilebank);
        for (i = 0; i < _root.shiptype[playershiptype][7].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreenmissilebankicon", "missilebank" + i, 12 + i);
            setProperty("shipblueprint.missilebank" + i, _x, _root.shiptype[playershiptype][7][i][2] * gunlocationmultiplier);
            setProperty("shipblueprint.missilebank" + i, _y, _root.shiptype[playershiptype][7][i][3] * gunlocationmultiplier);
            set("shipblueprint.missilebank" + i + ".itemname", "Bank " + i);
            set("shipblueprint.missilebank" + i + ".bankno", i);
            noofmissiles = 0;
            bankno = i;
            for (gg = 0; gg < _root.missile.length; gg++)
            {
                if (_root.playershipstatus[7][bankno][4][gg] > 0)
                {
                    noofmissiles = noofmissiles + _root.playershipstatus[7][bankno][4][gg];
                } // end if
            } // end of for
            set("shipblueprint.missilebank" + i + ".qty", "Missiles\r(" + noofmissiles + "/" + _root.playershipstatus[7][bankno][5] + ")");
        } // end of for
    } // End of the function
    function hardwareshopmenulabelmissilesclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip ("hardwareinformationbutton");
        removeMovieClip ("buyagunquestion");
        for (i = 0; i < _root.guntype.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
        itemselection = null;
        buyingamissile = false;
    } // End of the function
    function hardwareshopmenulabelenergygeneratoropen(currentleftmostitem)
    {
        topenergygen = _root.shiptype[playershiptype][6][2] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < topenergygen - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < topenergygen; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.energygenerators[currentleftmostitem + i][1]);
            set(this + ".hardwareitembox" + i + ".itemtype", "energygenerator" + (currentleftmostitem + i));
            currentvalueofgenerator = Math.round(_root.energygenerators[_root.playershipstatus[1][0]][2] / 2);
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + String(_root.energygenerators[currentleftmostitem + i][2] - currentvalueofgenerator));
            if (currentleftmostitem + i == _root.playershipstatus[1][0])
            {
                set(this + ".hardwareitembox" + i + ".itemcost", "OWNED");
            } // end if
            set(this + ".hardwareitembox" + i + ".itemspecs", "\rRecharge: " + _root.energygenerators[currentleftmostitem + i][0] + "/sec \r");
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "generatorbracket", 99);
        setProperty("shipblueprint.generatorbracket", _x, 0);
        setProperty("shipblueprint.generatorbracket", _y, 0);
        set(this.shipblueprint + ".generatorbracket.hardpointname", "Generator");
        this.shipblueprint.generatorbracket.attachMovie("hardwarescreengunbracketenergygen0", "generator", 99);
    } // End of the function
    function hardwareshopmenulabelenergygeneratorclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip (buyingageneratorquestion);
        buyingagenerator = false;
        removeMovieClip (this.hardwarewarningbox);
        for (i = 0; i < _root.energygenerators.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
    } // End of the function
    function hardwareshopmenulabelshieldgeneratoropen(currentleftmostitem)
    {
        topshieldgen = _root.shiptype[playershiptype][6][0] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < topshieldgen - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < topshieldgen; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.shieldgenerators[currentleftmostitem + i][4]);
            set(this + ".hardwareitembox" + i + ".itemtype", "shieldgenerator" + (currentleftmostitem + i));
            currentvalueofgenerator = Math.round(_root.shieldgenerators[_root.playershipstatus[2][0]][5] / 2);
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + String(_root.shieldgenerators[currentleftmostitem + i][5] - currentvalueofgenerator));
            if (currentleftmostitem + i == _root.playershipstatus[2][0])
            {
                set(this + ".hardwareitembox" + i + ".itemcost", "OWNED");
            } // end if
            set(this + ".hardwareitembox" + i + ".itemspecs", "Recharge: " + _root.shieldgenerators[currentleftmostitem + i][1] + " \r" + "Energy Drain/second: \r" + "Unit: " + _root.shieldgenerators[currentleftmostitem + i][2] + " \r" + "Full Charge: " + _root.shieldgenerators[currentleftmostitem + i][3] + " \r");
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "generatorbracket", 99);
        setProperty("shipblueprint.generatorbracket", _x, 0);
        setProperty("shipblueprint.generatorbracket", _y, 0);
        set(this.shipblueprint + ".generatorbracket.hardpointname", "Generator");
        this.shipblueprint.generatorbracket.attachMovie("hardwarescreengunbracketshieldtype0", "shieldgen", 1);
    } // End of the function
    function hardwareshopmenulabelshieldgeneratorclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip (buyingageneratorquestion);
        buyingashieldgenerator = false;
        removeMovieClip (this.hardwarewarningbox);
        for (i = 0; i < _root.energygenerators.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
    } // End of the function
    function hardwareshopmenulabelenergycapacitorsopen(currentleftmostitem)
    {
        topenergycap = _root.shiptype[playershiptype][6][1] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < topenergycap - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < topenergycap; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.energycapacitors[currentleftmostitem + i][1]);
            set(this + ".hardwareitembox" + i + ".itemtype", "energycapacitor" + (currentleftmostitem + i));
            currentvalueofgenerator = Math.round(_root.energycapacitors[_root.playershipstatus[1][5]][2] / 2);
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + String(_root.energycapacitors[currentleftmostitem + i][2] - currentvalueofgenerator));
            if (currentleftmostitem + i == _root.playershipstatus[1][5])
            {
                set(this + ".hardwareitembox" + i + ".itemcost", "OWNED");
            } // end if
            set(this + ".hardwareitembox" + i + ".itemspecs", "\rMax Charge: " + _root.energycapacitors[currentleftmostitem + i][0] + " \r");
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "generatorbracket", 99);
        setProperty("shipblueprint.generatorbracket", _x, 0);
        setProperty("shipblueprint.generatorbracket", _y, 0);
        set(this.shipblueprint + ".generatorbracket.hardpointname", "Capacitor");
        this.shipblueprint.generatorbracket.attachMovie("hardwarescreengunbracketenergycap0", "generator", 99);
    } // End of the function
    function hardwareshopmenulabelenergycapacitorsclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip (buyingageneratorquestion);
        buyingaenergycapacitor = false;
        removeMovieClip (this.hardwarewarningbox);
        for (i = 0; i < _root.energycapacitors.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
    } // End of the function
    if (operation != null)
    {
        this.hardwareshopmenulabelguns._alpha = 75;
        this.hardwareshopmenulabelmissiles._alpha = 75;
        this.hardwareshopmenulabelshield._alpha = 75;
        this.hardwareshopmenulabelenergy._alpha = 75;
        this.hardwareshopmenulabelcapacitors._alpha = 75;
        this.hardwareshopmenulabelturrets._alpha = 75;
        this.hardwareshopmenulabelspecials._alpha = 75;
        hardwareshopmenulabelspecialsmenuclose();
        hardwareshopmenulabelgunsmenuclose();
        hardwareshopmenulabelmissilesclose();
        hardwareshopmenulabelenergygeneratorclose();
        hardwareshopmenulabelshieldgeneratorclose();
        hardwareshopmenulabelenergycapacitorsclose();
        hardwareshopmenulabelturretsmenuclose();
        this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
        setProperty("shipblueprint", _x, _root.gameareawidth / 8);
        setProperty("shipblueprint", _y, _root.gameareaheight / 8);
        islefton = false;
        isrighton = false;
        if (operation == "hardwareshopmenulabelguns")
        {
            this.hardwareshopmenulabelguns._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelgunsmenuopen(currentleftmostitem);
            if (_root.guntype.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelspecials")
        {
            this.hardwareshopmenulabelspecials._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelspecialsmenuopen(currentleftmostitem);
        } // end if
        if (operation == "hardwareshopmenulabelmissiles")
        {
            this.hardwareshopmenulabelmissiles._alpha = 100;
            buyingamissile = false;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            currentmissilebank = 0;
            hardwareshopmenulabelmissilesopen(currentleftmostitem);
            if (_root.missile.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelturrets")
        {
            this.hardwareshopmenulabelturrets._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelturretsmenuopen(currentleftmostitem);
            if (_root.guntype.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelenergy")
        {
            this.hardwareshopmenulabelenergy._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelenergygeneratoropen(currentleftmostitem);
            if (_root.energygenerators.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelshield")
        {
            this.hardwareshopmenulabelshield._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelshieldgeneratoropen(currentleftmostitem);
            if (_root.shieldgenerators.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelcapacitors")
        {
            this.hardwareshopmenulabelcapacitors._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelenergycapacitorsopen(currentleftmostitem);
            if (_root.energycapacitors.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        currentoperation = operation;
        operation = null;
        redrawscreen = false;
        drawarrows(islefton, isrighton);
    } // end if
    if (moveitemsdisplayed != null)
    {
        currentleftmostitem = currentleftmostitem + moveitemsdisplayed;
        moveitemsdisplayed = null;
    } // end if
    if (currentoperation != null && (redrawscreen == true || currentleftmostitem != lasttleftmostitem))
    {
        if (currentoperation == "hardwareshopmenulabelguns")
        {
            hardwareshopmenulabelgunsmenuopen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelmissiles")
        {
            hardwareshopmenulabelmissilesopen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelspecials")
        {
            hardwareshopmenulabelspecialsmenuopen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelturrets")
        {
            hardwareshopmenulabelturretsmenuopen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelenergy")
        {
            hardwareshopmenulabelenergygeneratoropen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelshield")
        {
            hardwareshopmenulabelshieldgeneratoropen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelcapacitors")
        {
            hardwareshopmenulabelenergycapacitorsopen(currentleftmostitem);
        } // end if
        redrawscreen = false;
        lasttleftmostitem = currentleftmostitem;
    } // end if
}

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (mouseDown)
{
    if (currentoperation == "hardwareshopmenulabelguns")
    {
        i = 0;
        _root.attachMovie("radarblot", "testdot", 99999);
        setProperty("_root.testdot", _x, _root._xmouse);
        setProperty("_root.testdot", _y, _root._ymouse);
        while (i < _root.shiptype[playershiptype][2].length && buyingagun != true)
        {
            if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracket" + i) && sellingagun == false)
            {
                buyingagun = false;
                removeMovieClip (buyagunquestion);
                removeMovieClip (hardwarewarningbox);
                startDrag ("shipblueprint.gun" + i, false);
                hardpointmovedfrom = i;
            } // end if
            ++i;
        } // end while
        removeMovieClip (_root.testdot);
    } // end if
    if (currentoperation == "hardwareshopmenulabelturrets")
    {
        i = 0;
        _root.attachMovie("radarblot", "testdot", 99999);
        setProperty("_root.testdot", _x, _root._xmouse);
        setProperty("_root.testdot", _y, _root._ymouse);
        while (i < _root.shiptype[playershiptype][5].length && buyingaturret != true)
        {
            if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracket" + i) && sellingaturret == false)
            {
                buyingaturret = false;
                removeMovieClip (buyagunquestion);
                removeMovieClip (hardwarewarningbox);
                startDrag ("shipblueprint.gun" + i, false);
                hardpointmovedfrom = i;
            } // end if
            ++i;
        } // end while
        removeMovieClip (_root.testdot);
    } // end if
}

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (mouseUp)
{
    if (currentoperation == "hardwareshopmenulabelguns")
    {
        stopDrag ();
        _root.attachMovie("radarblot", "testdot", 99999);
        setProperty("_root.testdot", _x, _root._xmouse);
        setProperty("_root.testdot", _y, _root._ymouse);
        i = 0;
        hardpointmoved = false;
        while (i < _root.shiptype[playershiptype][2].length && sellingagun == false && buyingagun != true)
        {
            if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracket" + i))
            {
                if (i != hardpointselected)
                {
                    hardpointmoved = true;
                    hardpointmovedinto = i;
                    extraplayersgunarray = _root.playershipstatus[0].length;
                    _root.playershipstatus[0][extraplayersgunarray] = _root.playershipstatus[0][hardpointmovedfrom];
                    _root.playershipstatus[0][hardpointmovedfrom] = _root.playershipstatus[0][hardpointmovedinto];
                    _root.playershipstatus[0][hardpointmovedinto] = _root.playershipstatus[0][extraplayersgunarray];
                    _root.playershipstatus[0].splice(extraplayersgunarray, 1);
                    for (i = 0; i < _root.playershipstatus[0].length; i++)
                    {
                        this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[0][i][0], "gun" + i, 12 + i);
                        setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
                        setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
                        set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[0][i][0]][6]);
                    } // end of for
                } // end if
            } // end if
            ++i;
        } // end while
        i = hardpointmovedfrom;
        if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracketsell") && _root.playershipstatus[0][hardpointmovedfrom][0] != "none")
        {
            hardpointmoved = true;
            sellingagun = true;
            attachMovie("hardwareinformationbutton", "hardwareinformationbutton", 99998);
            this.hardwareinformationbutton.question = "Sell: " + _root.guntype[_root.playershipstatus[0][hardpointmovedfrom][0]][6] + " \r" + "For: " + Math.round(_root.guntype[_root.playershipstatus[0][hardpointmovedfrom][0]][5] / 2);
        } // end if
        if (hardpointmoved != true && sellingagun == false)
        {
            setProperty("shipblueprint.gun" + hardpointmovedfrom, _x, _root.shiptype[playershiptype][2][hardpointmovedfrom][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + hardpointmovedfrom, _y, _root.shiptype[playershiptype][2][hardpointmovedfrom][1] * gunlocationmultiplier);
        } // end if
        removeMovieClip (_root.testdot);
    } // end if
    if (currentoperation == "hardwareshopmenulabelturrets")
    {
        turretsmouseup();
    } // end if
}

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    if (currentoperation == "hardwareshopmenulabelturrets")
    {
        turretsproccessoneachframe();
    }
    else
    {
        if (itemselection != null && buyingagun != true && sellingagun != true)
        {
            itemgunselectiontype = itemselection.substring(0, 7);
            gunselectiontype = itemselection.substring(7);
            removeMovieClip (hardwarewarningbox);
        } // end if
        if (itemselection != null && (buyingagun == true || sellingagun == true))
        {
            itemselection = null;
        } // end if
        if (itemselection != null && itemgunselectiontype == "guntype" && buyingagun != true)
        {
            _parent.test = "ran";
            i = 0;
            emptygunlocationavailable = false;
            while (i < _root.playershipstatus[0].length && emptygunlocationavailable != true)
            {
                if (_root.playershipstatus[0][i][0] == "none")
                {
                    emptygunlocationavailable = true;
                    emptygunlocation = i;
                } // end if
                ++i;
            } // end while
            if (emptygunlocationavailable == true)
            {
                buyingagun = true;
                attachMovie("hardwareinformationbutton", "buyagunquestion", 99998);
                this.buyagunquestion.question = "Buy: " + _root.guntype[gunselectiontype][6] + " \r" + "For: " + _root.guntype[gunselectiontype][5];
            }
            else
            {
                this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
                this.hardwarewarningbox.information = "You Need An Open Hardpoint";
            } // end else if
            if (emptygunlocationavailable == false)
            {
                itemselection = null;
                buyingagun = false;
            } // end if
            itemselection = null;
        } // end if
        if (this.buyagunquestion.requestedanswer == true && buyingagun == true)
        {
            if (_root.playershipstatus[3][1] >= _root.guntype[gunselectiontype][5])
            {
                i = emptygunlocation;
                _root.playershipstatus[0][i][0] = gunselectiontype;
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - _root.guntype[gunselectiontype][5];
                buyingagun = false;
                removeMovieClip (this.buyagunquestion);
                itemselection = null;
                removeMovieClip ("shipblueprint.gun" + i);
                this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[0][i][0], "gun" + i, 12 + i);
                setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
                setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
                set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[0][i][0]][6]);
                emptygunlocationavailable = null;
            }
            else
            {
                this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
                this.hardwarewarningbox.information = "You Do Not Have Enough Funds";
                buyingagun = false;
                removeMovieClip (this.buyagunquestion);
                itemselection = null;
            } // end if
        } // end else if
        if (this.buyagunquestion.requestedanswer == false && buyingagun == true)
        {
            buyingagun = false;
            removeMovieClip (this.buyagunquestion);
            itemselection = null;
        } // end if
        if (sellingagun == true && this.hardwareinformationbutton.requestedanswer == true)
        {
            i = hardpointmovedfrom;
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Math.round(_root.guntype[_root.playershipstatus[0][hardpointmovedfrom][0]][5] / 2);
            _root.playershipstatus[0][i][0] = "none";
            this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[0][i][0], "gun" + i, 12 + i);
            setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
            removeMovieClip ("hardwareinformationbutton");
            sellingagun = false;
        } // end if
        if (sellingagun == true && this.hardwareinformationbutton.requestedanswer == false)
        {
            i = hardpointmovedfrom;
            setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
            set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[0][i][0]][6]);
            removeMovieClip ("hardwareinformationbutton");
            sellingagun = false;
        } // end if
    } // end else if
}

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    if (itemselection != null)
    {
        missiletype = itemselection.substring(0, 11);
        missileselected = itemselection.substring(11);
    } // end if
    if (itemselection != null && missiletype == "missiletype" && buyingamissile != true)
    {
        buyingamissile = true;
        removeMovieClip (this.hardwarewarningbox);
        attachMovie("hardwaremissileinformationbutton", "buyingamissilequestion", 9980);
        this.buyingamissilequestion.missileselected = Number(missileselected);
        this.buyingamissilequestion.bankno = Number(currentmissilebank);
        for (qq = 0; qq < _root.shiptype[playershiptype][7].length; qq++)
        {
            this.shipblueprint["missilebank" + qq]._visible = false;
        } // end of for
        buyingamissile = false;
        itemselection = null;
    } // end if
}

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    if (itemselection != null)
    {
        generatortype = itemselection.substring(0, 15);
        generatorselectiontype = itemselection.substring(15);
    } // end if
    if (itemselection != null && generatortype == "energygenerator" && buyingagenerator != true && generatorselectiontype != _root.playershipstatus[1][0])
    {
        _parent.test = "ran";
        buyingagenerator = true;
        removeMovieClip (this.hardwarewarningbox);
        attachMovie("hardwareinformationbutton", "buyingageneratorquestion", 9980);
        currentvalueofgenerator = Math.round(_root.energygenerators[_root.playershipstatus[1][0]][2] / 2);
        currentgeneratorupgradecost = _root.energygenerators[generatorselectiontype][2] - currentvalueofgenerator;
        this.buyingageneratorquestion.question = "Buy: " + _root.energygenerators[generatorselectiontype][1] + " \r" + "For: " + currentgeneratorupgradecost;
    } // end if
    if (this.buyingageneratorquestion.requestedanswer == true && buyingagenerator == true)
    {
        i = generatorselectiontype;
        currentvalueofgenerator = _root.energygenerators[_root.playershipstatus[1][0]][2] / 2;
        currentgeneratorupgradecost = _root.energygenerators[i][2] - currentvalueofgenerator;
        if (_root.playershipstatus[3][1] >= currentgeneratorupgradecost)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - currentgeneratorupgradecost;
            buyingagenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
            _root.playershipstatus[1][0] = generatorselectiontype;
            hardwareshopmenulabelenergyclose();
            this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
            setProperty("shipblueprint", _x, _root.gameareawidth / 8);
            setProperty("shipblueprint", _y, _root.gameareaheight / 8);
            redrawscreen = true;
        }
        else
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 9990);
            this.hardwarewarningbox.information = "You Do Not Have Enough Funds";
            buyingagenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
        } // end if
    } // end else if
    if (this.buyingageneratorquestion.requestedanswer == false && buyingagenerator == true)
    {
        buyingagenerator = false;
        removeMovieClip (this.buyingageneratorquestion);
        itemselection = null;
    } // end if
    if (itemselection != null)
    {
        generatortype = itemselection.substring(0, 15);
        generatorselectiontype = itemselection.substring(15);
    } // end if
    if (itemselection != null && generatortype == "shieldgenerator" && buyingashieldgenerator != true && generatorselectiontype != _root.playershipstatus[2][0])
    {
        buyingashieldgenerator = true;
        removeMovieClip (this.hardwarewarningbox);
        attachMovie("hardwareinformationbutton", "buyingageneratorquestion", 9980);
        currentvalueofgenerator = Math.round(_root.shieldgenerators[_root.playershipstatus[2][0]][5] / 2);
        currentgeneratorupgradecost = _root.shieldgenerators[generatorselectiontype][5] - currentvalueofgenerator;
        this.buyingageneratorquestion.question = "Buy: " + _root.shieldgenerators[generatorselectiontype][4] + " \r" + "For: " + currentgeneratorupgradecost;
    } // end if
    if (this.buyingageneratorquestion.requestedanswer == true && buyingashieldgenerator == true)
    {
        i = generatorselectiontype;
        currentvalueofgenerator = _root.shieldgenerators[_root.playershipstatus[2][0]][5] / 2;
        currentgeneratorupgradecost = _root.shieldgenerators[i][5] - currentvalueofgenerator;
        rec = _root.energygenerators[_root.playershipstatus[1][0]][0];
        dra = _root.shieldgenerators[i][2] + _root.shieldgenerators[i][3];
        if (rec < dra)
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 9990);
            this.hardwarewarningbox.information = "Shields Will Use Too Much Power, Need a Bigger Energy Generator First";
            buyingashieldgenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
        }
        else if (_root.playershipstatus[3][1] >= currentgeneratorupgradecost)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - currentgeneratorupgradecost;
            buyingashieldgenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
            _root.playershipstatus[2][0] = generatorselectiontype;
            hardwareshopmenulabelshieldgeneratorclose();
            this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
            setProperty("shipblueprint", _x, _root.gameareawidth / 8);
            setProperty("shipblueprint", _y, _root.gameareaheight / 8);
            redrawscreen = true;
        }
        else
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 9990);
            this.hardwarewarningbox.information = "You Do Not Have Enough Funds";
            buyingashieldgenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
        } // end else if
    } // end else if
    if (this.buyingageneratorquestion.requestedanswer == false && buyingashieldgenerator == true)
    {
        buyingashieldgenerator = false;
        removeMovieClip (this.buyingageneratorquestion);
        itemselection = null;
    } // end if
    if (itemselection != null)
    {
        generatortype = itemselection.substring(0, 15);
        generatorselectiontype = itemselection.substring(15);
    } // end if
    if (itemselection != null && generatortype == "energycapacitor" && buyingaenergycapacitor != true && generatorselectiontype != _root.playershipstatus[1][5])
    {
        _parent.test = "ran";
        buyingaenergycapacitor = true;
        removeMovieClip (this.hardwarewarningbox);
        attachMovie("hardwareinformationbutton", "buyingageneratorquestion", 9980);
        currentvalueofgenerator = Math.round(_root.energycapacitors[_root.playershipstatus[1][5]][2] / 2);
        currentgeneratorupgradecost = _root.energycapacitors[generatorselectiontype][2] - currentvalueofgenerator;
        this.buyingageneratorquestion.question = "Buy: " + _root.energycapacitors[generatorselectiontype][1] + " \r" + "For: " + currentgeneratorupgradecost;
    } // end if
    if (this.buyingageneratorquestion.requestedanswer == true && buyingaenergycapacitor == true)
    {
        i = generatorselectiontype;
        currentvalueofgenerator = _root.energycapacitors[_root.playershipstatus[1][5]][2] / 2;
        currentgeneratorupgradecost = _root.energycapacitors[i][2] - currentvalueofgenerator;
        if (_root.playershipstatus[3][1] >= currentgeneratorupgradecost)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - currentgeneratorupgradecost;
            buyingaenergycapacitor = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
            _root.playershipstatus[1][5] = generatorselectiontype;
            hardwareshopmenulabelenergycapacitorsclose();
            this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
            setProperty("shipblueprint", _x, _root.gameareawidth / 8);
            setProperty("shipblueprint", _y, _root.gameareaheight / 8);
            redrawscreen = true;
        }
        else
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 9990);
            this.hardwarewarningbox.information = "You Do Not Have Enough Funds";
            buyingaenergycapacitor = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
        } // end if
    } // end else if
    if (this.buyingageneratorquestion.requestedanswer == false && buyingagenerator == true)
    {
        buyingaenergycapacitor = false;
        removeMovieClip (this.buyingageneratorquestion);
        itemselection = null;
    } // end if
}

// [Action in Frame 1]
stop ();
