﻿// Action script...

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "REPAIR";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.basefunction.gotoAndStop("repairbase");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "UPGRADE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.basefunction.gotoAndStop("changeshield");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "UPGRADE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.basefunction.gotoAndStop("upgradeturret");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.basefunction.gotoAndStop("changeturret");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "EXIT";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent._parent.gotoAndStop("squadbasemenu");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "CHANGE";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.basefunction.gotoAndStop("missiledef");
}

// [onClipEvent of sprite 1065 in frame 1]
onClipEvent (load)
{
    this.label = "REFRESH";
}

// [onClipEvent of sprite 1065 in frame 1]
on (release)
{
    _parent.getbasevariables();
}

// [Action in Frame 1]
stop ();
