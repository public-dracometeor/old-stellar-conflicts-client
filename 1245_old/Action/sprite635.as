﻿// Action script...

// [Action in Frame 1]
function warning(warnmessage)
{
    this.attachMovie("shipwarningbox", "shipwarningbox", 1);
    this.shipwarningbox.information = warnmessage;
} // End of the function
email = _root.playershipstatus[3][5];
name = _root.playershipstatus[3][2];
newpass1 = "";
newpass2 = "";
stop ();

// [Action in Frame 2]
function xmlhandler(doc)
{
    loadedvars = String(doc);
    _root.testttt = loadedvars;
    if (loadedvars == "changefailed`~")
    {
        status = "Changes NOT Saved";
        _root.testtt = loadedvars;
    }
    else if (loadedvars == "changesuccess`~")
    {
        status = "Changes Saved";
        _root.playershipstatus[3][3] = newpass1;
        _root.playershipstatus[3][5] = email;
    } // end else if
} // End of the function
okbutton._visible = false;
status = "Sending Info\r\r";
newpass1 = newpass1.toUpperCase();
email = email.toLowerCase();
datatosend = "change`" + _root.playershipstatus[3][2] + "`" + _root.playershipstatus[3][3] + "`" + newpass1 + "`" + email + "`~";
accountserv = new XMLSocket();
if (_root.isgamerunningfromremote == false)
{
    currenturl = String(_root._url);
    if (currenturl.substr(0, 26) == "http://www.gecko-games.com")
    {
        accountserv.connect("", _root.accountserverport);
    } // end if
}
else
{
    accountserv.connect(_root.accountserveraddy, _root.accountserverport);
} // end else if
accountserv.onConnect = function (success)
{
    okbutton._visible = true;
    accountserv.send(datatosend);
};
accountserv.onXML = xmlhandler;
stop ();
