﻿// Action script...

// [Action in Frame 1]
counter = 0;
zoneselectionmenu._visible = false;

// [Action in Frame 2]
if (counter > 50)
{
    if (_root.isgamerunningfromremote == false)
    {
        if (_root.portandsystem == null || isNaN(_root.portandsystem))
        {
            zoneselectionmenu._visible = true;
        }
        else
        {
            gotoAndPlay(4);
        } // end else if
    }
    else
    {
        gotoAndPlay(4);
    } // end if
} // end else if

// [Action in Frame 3]
if (_root.reconnectingfromgame == true)
{
    gotoAndStop(81);
}
else
{
    ++counter;
    gotoAndPlay(2);
} // end else if

// [Action in Frame 17]
_root.func_login_opening_sound();

// [Action in Frame 81]
versionno = "VERSION " + _root.versionno;
stop ();
