﻿// Action script...

on (release)
{
    function passwordhasspaces(memberpassword)
    {
        for (i = 0; i < memberpassword.length; i++)
        {
            if (memberpassword.charAt(i) == " ")
            {
                return (true);
            } // end if
        } // end of for
    } // End of the function
    function checknameforlegitcharacters(name)
    {
        legitcharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890";
        for (i = 0; i < name.length; i++)
        {
            illegalcharacter = true;
            for (j = 0; j < legitcharacters.length; j++)
            {
                if (name.charAt(i) == legitcharacters.charAt(j))
                {
                    illegalcharacter = false;
                } // end if
            } // end of for
            if (illegalcharacter == true)
            {
                return (true);
                i = 99999;
            } // end if
        } // end of for
    } // End of the function
    newpass1 = newpass1.toUpperCase();
    newpass2 = newpass2.toUpperCase();
    if (newpass1 == "" && newpass2 == "" && email != _root.playershipstatus[3][5])
    {
        newpass1 = newpass2 = _root.playershipstatus[3][3].toUpperCase();
        gotoAndStop(2);
    } // end if
    if (newpass1.length > 10 || newpass1.length < 3)
    {
        warning("Passwords at least 3, no more than 10 characters");
    }
    else if (passwordhasspaces(newpass1))
    {
        warning("Passwords have no spaces");
    }
    else if (checknameforlegitcharacters(newpass1))
    {
        warning("Passwords only letters a-Z, and numbers");
    }
    else if (newpass1 != newpass2)
    {
        warning("Passwords do not match");
    }
    else
    {
        gotoAndStop(2);
    } // end else if
}
