﻿// Action script...

// [Action in Frame 1]
function turnofshipcloak()
{
    for (qqq = 0; qqq < _root.playershipstatus[11][0].length; qqq++)
    {
        if (_root.playershipstatus[11][0][qqq] != null)
        {
            specialno = qqq + 1;
            speciallocation = qqq;
            itemlocation = _root.playershipstatus[11][0][speciallocation];
            specialtouse = _root.playershipstatus[11][1][itemlocation][0];
            if (_root.specialshipitems[specialtouse][5] == "CLOAK")
            {
                clearInterval(_root.specialstimers[specialno]);
                nexttime = _root.playershipstatus[11][2][speciallocation][0];
                _root.rightside.specialsingame.specialsinfo("UNCLOAKING", "RELOAD", nexttime, specialno);
                _root.playershipstatus[5][15] = "";
            } // end if
            _parent.playership._alpha = 100;
        } // end if
    } // end of for
} // End of the function
function turnoffdetecting()
{
    for (j = 0; j < _root.otherplayership.length; j++)
    {
        if (_root.otherplayership[j][16] == "D")
        {
            _root.otherplayership[j][16] = "";
        } // end if
    } // end of for
} // End of the function
function turnoffstealth()
{
    if (_root.playershipstatus[5][15] == "S")
    {
        _root.playershipstatus[5][15] = "";
    } // end if
} // End of the function
function detectstealthcloak(chance, specialno, specialtouse, speciallocation, operation)
{
    energydrain = _root.specialshipitems[specialtouse][2];
    if (_root.playershipstatus[1][1] >= energydrain)
    {
        _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
        nexttime = getTimer() + _root.specialshipitems[specialtouse][4];
        _root.playershipstatus[11][2][speciallocation][0] = nexttime;
        if (operation == "DETECTOR")
        {
            detectotherships(chance, specialno);
            _root.rightside.specialsingame.specialsinfo("DETECT ON", "ON", nexttime, specialno);
        }
        else if (operation == "STEALTH")
        {
            stealthplayership(chance, specialno, nexttime);
        }
        else if (operation == "CLOAK")
        {
            cloakplayership(chance, specialno, nexttime);
        }
        else
        {
            clearInterval(_root.specialstimers[specialno]);
        } // end else if
    }
    else
    {
        info = "NO ENERGY";
        status = "RELOAD";
        nexttime = getTimer() + 200;
        specialno = specialno;
        _root.rightside.specialsingame.specialsinfo(info, status, nexttime, specialno);
        clearInterval(_root.specialstimers[specialno]);
        if (_root.specialshipitems[specialtouse][5] == "CLOAK")
        {
            turnofshipcloak();
        } // end if
        if (_root.specialshipitems[specialtouse][5] == "DETECTOR")
        {
            turnoffdetecting();
        } // end if
        if (_root.specialshipitems[specialtouse][5] == "STEALTH")
        {
            turnoffstealth();
        } // end if
        clearInterval(_root.specialstimers[specialno]);
    } // end else if
} // End of the function
function displayfailed(specialno, nexttime)
{
} // End of the function
function detectotherships(chance, specialno)
{
    _root.rightside.radarscreen.radarscreenarea.func_pingcircledetector();
    for (j = 0; j < _root.otherplayership.length; j++)
    {
        if (_root.otherplayership[j][16] == "D" || _root.otherplayership[j][16] == "C" || _root.otherplayership[j][16] == "S")
        {
            detectchance = Math.random();
            if (chance > detectchance)
            {
                setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[j][0], _visible, true);
                if (_root.otherplayership[j][16] == "C")
                {
                    _root.gamedisplayarea["otherplayership" + _root.otherplayership[j][0]].attachMovie("shipactivateotherdetect", "shipactivateotherdetect", 69);
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _alpha, cloakalpha);
                }
                else
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _alpha, 100);
                } // end else if
                _root.otherplayership[j][16] = "D";
                continue;
            } // end if
            if (_root.otherplayership[j][16] == "D")
            {
                _root.otherplayership[j][16] = "";
            } // end if
        } // end if
    } // end of for
} // End of the function
function stealthplayership(chance, specialno, nexttime)
{
    stealthchance = Math.random();
    if (chance > stealthchance)
    {
        _root.playershipstatus[5][15] = "S";
        _root.rightside.specialsingame.specialsinfo("STEALTH ON", "ON", nexttime, specialno);
        _parent.playership.attachMovie("shipactivatestealth", "shipactivatestealth", 10);
    }
    else
    {
        _root.rightside.specialsingame.specialsinfo("FAILED", "RELOAD", nexttime, specialno);
        turnoffstealth();
        clearInterval(_root.specialstimers[specialno]);
    } // end else if
} // End of the function
function cloakplayership(chance, specialno, nexttime)
{
    cloakchance = Math.random();
    flagreason = false;
    if (_root.kingofflag[16] == false)
    {
        if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
        {
            flagreason = true;
        } // end if
    } // end if
    if (chance > cloakchance && !flagreason)
    {
        _root.playershipstatus[5][15] = "C";
        _root.rightside.specialsingame.specialsinfo("CLOAK ON", "ON", nexttime, specialno);
        _parent.playership.attachMovie("shipactivatecloak", "shipactivatecloak", 10);
        _parent.playership._alpha = cloakalpha;
    }
    else
    {
        _root.playershipstatus[5][15] = "";
        if (flagreason)
        {
            _root.rightside.specialsingame.specialsinfo("HAVE FLAG", "RELOAD", nexttime, specialno);
        }
        else
        {
            _root.rightside.specialsingame.specialsinfo("CLOAK FAILED", "RELOAD", nexttime, specialno);
        } // end else if
        clearInterval(_root.specialstimers[specialno]);
        _parent.playership._alpha = 100;
    } // end else if
} // End of the function
function runflarescript(distractchance, flarex, flarey)
{
    for (j = 0; j < _root.othermissilefire.length; j++)
    {
        if (_root.othermissilefire[j][13] == "SEEKING")
        {
            missilechance = Math.random();
            if (distractchance > missilechance)
            {
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[j][8] + ".distracted", true);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[j][8] + ".flarex", flarex);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[j][8] + ".flarey", flarey);
            } // end if
        } // end if
    } // end of for
} // End of the function
function runspecialone(specialno)
{
    if (specialno == 1)
    {
        itemlocation = _root.playershipstatus[11][0][0];
        specialtouse = _root.playershipstatus[11][1][itemlocation][0];
        speciallocation = 0;
    } // end if
    if (specialno == 2)
    {
        itemlocation = _root.playershipstatus[11][0][1];
        specialtouse = _root.playershipstatus[11][1][itemlocation][0];
        speciallocation = 1;
    } // end if
    if (specialno == 3)
    {
        itemlocation = _root.playershipstatus[11][0][2];
        specialtouse = _root.playershipstatus[11][1][itemlocation][0];
        speciallocation = 2;
    } // end if
    currenttime = getTimer();
    if (specialtouse != null && _root.playershipstatus[11][2][speciallocation][0] < currenttime)
    {
        if (_root.specialshipitems[specialtouse][5] == "FLARE")
        {
            energydrain = _root.specialshipitems[specialtouse][2];
            if (_root.playershipstatus[1][1] >= energydrain && _root.playershipstatus[11][2][speciallocation][0] < currenttime)
            {
                _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
                nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
                _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
                playershipx = Math.round(_root.shipcoordinatex);
                playershipy = Math.round(_root.shipcoordinatey);
                ++_root.currentplayershotsfired;
                if (_root.currentplayershotsfired > 999)
                {
                    _root.currentplayershotsfired = 0;
                } // end if
                i = _root.currentplayershotsfired;
                _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialtouse][8], "playerflare" + i, 5000 + i);
                setProperty("_root.gamedisplayarea.playerflare" + i, _x, playershipx);
                setProperty("_root.gamedisplayarea.playerflare" + i, _y, playershipy);
                set("_root.gamedisplayarea.playerflare" + i + ".xposition", playershipx);
                set("_root.gamedisplayarea.playerflare" + i + ".yposition", playershipy);
                set("_root.gamedisplayarea.playerflare" + i + ".removetime", currenttime + _root.specialshipitems[specialtouse][3]);
                runflarescript(_root.specialshipitems[specialtouse][10], playershipx, playershipy);
                _root.gunfireinformation = _root.gunfireinformation + ("SP`" + _root.playershipstatus[3][0] + "`" + specialtouse + "`" + playershipx + "`" + playershipy + "~");
            } // end if
        }
        else if (_root.specialshipitems[specialtouse][5] == "PULSAR")
        {
            if (_root.pulsarsinzone != false)
            {
                if (_root.playershipstatus[11][1][itemlocation][1] > 0 && _root.playershipstatus[5][20] != true)
                {
                    energydrain = _root.specialshipitems[specialtouse][2];
                    if (func_checkifplayerindockingarea())
                    {
                        message = "You can not pulsar in the docking circle.";
                        _root.func_messangercom(message, "HELP", "BEGIN");
                    }
                    else if (_root.playershipstatus[1][1] >= energydrain && _root.playershipstatus[11][2][speciallocation][0] < currenttime)
                    {
                        --_root.playershipstatus[11][1][itemlocation][1];
                        _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
                        nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
                        _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
                        playershipx = Math.round(_root.shipcoordinatex);
                        playershipy = Math.round(_root.shipcoordinatey);
                        _root.bringinspecial(_root.playershipstatus[3][0], specialtouse, playershipx, playershipy, "true");
                        _root.gunfireinformation = _root.gunfireinformation + ("SP`" + _root.playershipstatus[3][0] + "`" + specialtouse + "`" + playershipx + "`" + playershipy + "~");
                    }
                    else if (_root.playershipstatus[1][1] < energydrain)
                    {
                        message = "You need to more energy in order to fire the pulsar.";
                        _root.func_messangercom(message, "HELP", "BEGIN");
                    } // end else if
                } // end else if
            }
            else
            {
                message = "Pulsars Are Not Usable In This Zone.";
                _root.func_messangercom(message, "HELP", "BEGIN");
            } // end else if
        }
        else if (_root.specialshipitems[specialtouse][5] == "DETECTOR" || _root.specialshipitems[specialtouse][5] == "STEALTH" || _root.specialshipitems[specialtouse][5] == "CLOAK")
        {
            clearInterval(_root.specialstimers[specialno]);
            chance = _root.specialshipitems[specialtouse][10];
            nextcheckwait = _root.specialshipitems[specialtouse][11];
            operation = _root.specialshipitems[specialtouse][5];
            _root.specialstimers[specialno] = setInterval(detectstealthcloak, nextcheckwait, chance, specialno, specialtouse, speciallocation, operation);
            detectstealthcloak(chance, specialno, specialtouse, speciallocation, operation);
        }
        else if (_root.specialshipitems[specialtouse][5] == "RECHARGESHIELD")
        {
            energydrain = Math.floor(_root.playershipstatus[1][1]);
            _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
            nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
            _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
            shieldrech = energydrain * _root.specialshipitems[specialtouse][2];
            shieldgen = _root.playershipstatus[2][0];
            maxshield = _root.shieldgenerators[shieldgen][0];
            curshieldstr = _root.playershipstatus[2][1];
            if (shieldrech + curshieldstr > maxshield)
            {
                shieldrech = Math.floor(maxshield - curshieldstr);
            } // end if
            _root.playershipstatus[2][1] = _root.playershipstatus[2][1] + shieldrech;
            _root.gunfireinformation = _root.gunfireinformation + ("SP`" + _root.playershipstatus[3][0] + "`" + specialtouse + "`" + shieldrech + "~");
        }
        else if (_root.specialshipitems[specialtouse][5] == "RECHARGESTRUCT")
        {
            energydrain = Math.floor(_root.playershipstatus[1][1]);
            _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
            nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
            _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
            structrep = energydrain * _root.specialshipitems[specialtouse][2];
            maxstructure = _root.shiptype[_root.playershipstatus[5][0]][3][3];
            curstruct = _root.playershipstatus[2][5];
            if (curstruct + structrep > maxstructure)
            {
                structrep = Math.floor(maxstructure - curstruct);
            } // end if
            _root.playershipstatus[2][5] = _root.playershipstatus[2][5] + structrep;
            _root.gunfireinformation = _root.gunfireinformation + ("SP`" + _root.playershipstatus[3][0] + "`" + specialtouse + "`" + structrep + "~");
            _parent.func_removedamages();
        }
        else if (_root.specialshipitems[specialtouse][5] == "MINES")
        {
            energydrain = _root.specialshipitems[specialtouse][2];
            if (_root.playershipstatus[5][15] == "C")
            {
                message = "You can not drop mines while you are cloaked.";
                _root.func_messangercom(message, "HELP", "BEGIN");
            }
            else if (_root.playershipstatus[11][1][itemlocation][1] > 0 && _root.playershipstatus[5][20] != true && _root.playershipstatus[2][5] > 0)
            {
                --_root.playershipstatus[11][1][itemlocation][1];
                nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
                _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
                playershipx = Math.round(_root.shipcoordinatex);
                playershipy = Math.round(_root.shipcoordinatey);
                ++_root.currentplayershotsfired;
                if (_root.currentplayershotsfired > 999)
                {
                    _root.currentplayershotsfired = 0;
                } // end if
                i = _root.currentplayershotsfired;
                mineid = _root.playershipstatus[3][0] + "a" + i;
                _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialtouse][8], "playermine" + mineid, 5000 + i);
                setProperty("_root.gamedisplayarea.playermine" + mineid, _x, playershipx);
                setProperty("_root.gamedisplayarea.playermine" + mineid, _y, playershipy);
                set("_root.gamedisplayarea.playermine" + mineid + ".xposition", playershipx);
                set("_root.gamedisplayarea.playermine" + mineid + ".yposition", playershipy);
                set("_root.gamedisplayarea.playermine" + mineid + ".removetime", currenttime + _root.specialshipitems[specialtouse][3]);
                set("_root.gamedisplayarea.playermine" + mineid + ".ishostilemine", false);
                set("_root.gamedisplayarea.playermine" + mineid + ".mineid", mineid);
                _root.gunfireinformation = _root.gunfireinformation + ("SP`" + mineid + "`" + specialtouse + "`" + playershipx + "`" + playershipy + "~");
            } // end else if
        } // end else if
    }
    else if (_root.specialshipitems[specialtouse][5] == "DETECTOR" || _root.specialshipitems[specialtouse][5] == "STEALTH" || _root.specialshipitems[specialtouse][5] == "CLOAK")
    {
        nexttime = _root.playershipstatus[11][2][speciallocation][0];
        _root.rightside.specialsingame.specialsinfo("SHUTTING OFF", "RELOAD", nexttime, specialno);
        clearInterval(_root.specialstimers[specialno]);
        if (_root.specialshipitems[specialtouse][5] == "STEALTH")
        {
            if (_root.playershipstatus[5][15] == "S")
            {
                turnoffstealth();
            } // end if
        } // end if
        if (_root.specialshipitems[specialtouse][5] == "CLOAK")
        {
            if (_root.playershipstatus[5][15] == "C")
            {
                turnofshipcloak();
            } // end if
        } // end if
        if (_root.specialshipitems[specialtouse][5] == "DETECTOR")
        {
            turnoffdetecting();
        } // end if
    } // end else if
} // End of the function
function func_checkifplayerindockingarea()
{
    nearabase = false;
    for (i = 0; i < _root.starbaselocation.length; i++)
    {
        if (_root.starbaselocation[i][5] == "ACTIVE" || Number(_root.starbaselocation[i][5]) < getTimer())
        {
            starbasex = _root.starbaselocation[i][1];
            starbasey = _root.starbaselocation[i][2];
            starbaseradius = _root.starbaselocation[i][4];
            relativestarbasex = starbasex - _root.shipcoordinatex;
            relativestarbasey = starbasey - _root.shipcoordinatey;
            if (relativestarbasex * relativestarbasex + relativestarbasey * relativestarbasey <= starbaseradius * starbaseradius)
            {
                nearabase = true;
                break;
            } // end if
        } // end if
    } // end of for
    return (nearabase);
} // End of the function
clearInterval(_root.specialstimers[1]);
clearInterval(_root.specialstimers[2]);
clearInterval(_root.specialstimers[3]);
_root.specialstimers = new Array();
_root.specialstimers[0] = 0;
_root.specialstimers[1] = 0;
_root.specialstimers[2] = 0;
_root.specialstimers[3] = 0;
cloakalpha = 60;
_root.playershipstatus[11][2][2][0] = 0;


function func_checkforjumping()
{
    for (i = 0; i < _root.sectormapitems.length; i++)
    {
        if (_root.sectormapitems[i][0].substr(0, 2) == "NP" && _root.sectormapitems[i][3] == 10)
        {
            navpointx = _root.sectormapitems[i][1];
            navpointy = _root.sectormapitems[i][2];
            jumpradius = 50;
            relativejumpx = navpointx - _root.shipcoordinatex;
            relativejumpy = navpointy - _root.shipcoordinatey;
            zonetogoto = _root.sectormapitems[i][9];
            if (relativejumpx * relativejumpx + relativestarbasey * relativejumpy <= jumpradius * jumpradius)
            {
                if (playershipvelocity > maxjumpvelocity)
                {
                    message = "You must have a velocity lass than " + maxjumpvelocity + " in order to use a Jump Point";
                    _root.func_messangercom(message, ainame, "BEGIN");
                    continue;
                } // end if
                if (_root.mapdsiplayed == false)
                {
                    _root.gamedisplayarea.testtt = "Jumping at " + _root.sectormapitems[i][0];
                    if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
                    {
                        datatosend = "KFLAG`DROP`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + _root.errorchecknumber + "~";
                        _root.mysocket.send(datatosend);
                        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
                    } // end if
                    removeMovieClip ("_root.toprightinformation");
                    information = "PI`CLEAR`~";
                    _root.mysocket.send(information);
                    _root.controlledserverclose = false;
                    _root.playershipstatus[3][0] = null;
                    _root.reconnectingfromgame = true;
                    goingtozone = zonetogoto;
                    _root.jumpinginfo = new Array(goingtozone, _root.portandsystem);
                    _root.portandsystem = goingtozone;
                    _root.currentgamestatus = "Jumping Sequence Initiated \r Attempting to Jump to system " + goingtozone + "\r\r";
                    _root.mysocket.close();
                    _root.gotoAndStop("jumpingzonestart");
                } // end if
            } // end if
        } // end if
    } // end of for
} // End of the function
maxjumpvelocity = maxdockingvelocity;
_root.jumpinginfo = new Array();

function func_setsectortargetitem(method, name, xcoord, ycoord, shield, struct)
{
    if (method == "ADD")
    {
        location = _root.sectoritemsfortarget.length;
        _root.sectoritemsfortarget[location] = new Array();
        _root.sectoritemsfortarget[location][0] = name;
        _root.sectoritemsfortarget[location][1] = xcoord;
        _root.sectoritemsfortarget[location][2] = ycoord;
        _root.sectoritemsfortarget[location][5] = "LOADING";
        _root.sectoritemsfortarget[location][6] = "LOADING";
    }
    else if (method == "REMOVE")
    {
        for (location = 0; location < _root.sectoritemsfortarget.length; location++)
        {
            if (name == _root.sectoritemsfortarget[location][0])
            {
                _root.sectoritemsfortarget.splice(location, 1);
                --location;
            } // end if
        } // end of for
    } // end else if
} // End of the function
function func_redrawsquadbase(basename, upgradetype)
{
    if (upgradetype == "LEVEL")
    {
        for (qq = 0; qq < _root.playersquadbases.length; qq++)
        {
            if (_root.playersquadbases[qq][0] == basename)
            {
                removeMovieClip (_root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[qq][0]]);
                _root.playersquadbases[qq][10] = false;
                break;
            } // end if
        } // end of for
        playersquadbasesdisplay(xsector, ysector);
    } // end if
} // End of the function
function playersquadbasesdisplay(xsector, ysector)
{
    for (jj = 0; jj < _root.playersquadbases.length; jj++)
    {
        squadbasexsector = _root.playersquadbases[jj][4];
        squadbaseysector = _root.playersquadbases[jj][5];
        if (squadbasexsector == xsector && squadbaseysector == ysector && _root.playersquadbases[jj][10] == false)
        {
            method = "ADD";
            name = _root.playersquadbases[jj][0];
            xcoord = _root.playersquadbases[jj][2];
            ycoord = _root.playersquadbases[jj][3];
            func_setsectortargetitem(method, name, xcoord, ycoord);
            _root.playersquadbases[jj][10] = true;
            basetype = _root.playersquadbases[jj][1];
            _root.gamedisplayarea.gamebackground.attachMovie("squadbasetype" + basetype, "playerbase" + _root.playersquadbases[jj][0], 550 + _root.playersquadbaseslvl);
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]]._x = _root.playersquadbases[jj][2];
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]]._y = _root.playersquadbases[jj][3];
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]].baseidname = _root.playersquadbases[jj][0];
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]].squadid = _root.playersquadbases[jj][0];
            ++_root.playersquadbaseslvl;
            if (_root.playersquadbaseslvl > 100)
            {
                _root.playersquadbaseslvl = 0;
            } // end if
            continue;
        } // end if
        removebase = true;
        if (_root.playersquadbases[jj][10] == true)
        {
            if (squadbasexsector == xsector || squadbasexsector == xsector - 1 || squadbasexsector == xsector + 1)
            {
                if (squadbaseysector == ysector || squadbaseysector == ysector - 1 || squadbaseysector == ysector + 1)
                {
                    removebase = false;
                } // end if
            } // end if
        } // end if
        if (removebase)
        {
            removeMovieClip (_root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]]);
            _root.playersquadbases[jj][10] = false;
            method = "REMOVE";
            name = _root.playersquadbases[jj][0];
            func_setsectortargetitem(method, name);
        } // end if
    } // end of for
    if (_root.teamdeathmatch == true)
    {
        teamsbasesdisplay(xsector, ysector);
    }
    else
    {
        sectoritemsdisp(xsector, ysector);
    } // end else if
    radarbackgrounditems(xsector, ysector);
} // End of the function
function teamsbasesdisplay(xsector, ysector)
{
    for (jj = 0; jj < _root.sectormapitems.length; jj++)
    {
        if (_root.sectormapitems[jj][0].substr(0, 2) == "TB")
        {
            squadbasexsector = _root.sectormapitems[jj][10];
            squadbaseysector = _root.sectormapitems[jj][11];
            if (squadbasexsector == xsector && squadbaseysector == ysector && _root.sectormapitems[jj][4] != "ON")
            {
                _root.sectormapitems[jj][4] = "ON";
                method = "ADD";
                name = _root.sectormapitems[jj][0];
                xcoord = _root.sectormapitems[jj][1];
                ycoord = _root.sectormapitems[jj][2];
                func_setsectortargetitem(method, name, xcoord, ycoord);
                basetype = _root.sectormapitems[jj][3];
                _root.gamedisplayarea.gamebackground.attachMovie("teambasetype0", "teambase" + _root.sectormapitems[jj][0], 550 + _root.playersquadbaseslvl);
                _root.gamedisplayarea.gamebackground["teambase" + _root.sectormapitems[jj][0]]._x = _root.sectormapitems[jj][1];
                _root.gamedisplayarea.gamebackground["teambase" + _root.sectormapitems[jj][0]]._y = _root.sectormapitems[jj][2];
                _root.gamedisplayarea.gamebackground["teambase" + _root.sectormapitems[jj][0]].baseidname = _root.sectormapitems[jj][0];
                ++_root.playersquadbaseslvl;
                if (_root.playersquadbaseslvl > 100)
                {
                    _root.playersquadbaseslvl = 0;
                } // end if
                continue;
            } // end if
            removebase = true;
            if (_root.sectormapitems[jj][4] == "ON")
            {
                if (squadbasexsector == xsector || squadbasexsector == xsector - 1 || squadbasexsector == xsector + 1)
                {
                    if (squadbaseysector == ysector || squadbaseysector == ysector - 1 || squadbaseysector == ysector + 1)
                    {
                        removebase = false;
                    } // end if
                } // end if
            } // end if
            if (removebase)
            {
                removeMovieClip (_root.gamedisplayarea.gamebackground["teambase" + _root.sectormapitems[jj][0]]);
                _root.sectormapitems[jj][4] = "OFF";
                method = "REMOVE";
                name = _root.sectormapitems[jj][0];
                func_setsectortargetitem(method, name);
            } // end if
        } // end if
    } // end of for
    playerteambasesdisplay(xsector, ysector);
    sectoritemsdisp(xsector, ysector);
    radarbackgrounditems(xsector, ysector);
} // End of the function
function radarbackgrounditems(xsector, ysector)
{
    _root.radarbackitems = new Array();
    for (i = 0; i < _root.sectormapitems.length; i++)
    {
        if (_root.sectormapitems[i][10] == xsector || _root.sectormapitems[i][10] == xsector - 1 || _root.sectormapitems[i][10] == xsector + 1)
        {
            if (_root.sectormapitems[i][11] == ysector || _root.sectormapitems[i][11] == ysector - 1 || _root.sectormapitems[i][11] == ysector + 1)
            {
                currentitem = _root.radarbackitems.length;
                _root.radarbackitems[currentitem] = new Array();
                if (_root.sectormapitems[i][0].substr(0, 2) == "SB" || _root.sectormapitems[i][0].substr(0, 2) == "PL")
                {
                    _root.radarbackitems[currentitem][0] = _root.sectormapitems[i][0];
                    _root.radarbackitems[currentitem][1] = _root.sectormapitems[i][1];
                    _root.radarbackitems[currentitem][2] = _root.sectormapitems[i][2];
                    _root.radarbackitems[currentitem][3] = "SB";
                    _root.radarbackitems[currentitem][4] = starbasedotcolour;
                } // end if
                if (_root.sectormapitems[i][0].substr(0, 2) == "NP")
                {
                    _root.radarbackitems[currentitem][0] = _root.sectormapitems[i][0];
                    _root.radarbackitems[currentitem][1] = _root.sectormapitems[i][1];
                    _root.radarbackitems[currentitem][2] = _root.sectormapitems[i][2];
                    _root.radarbackitems[currentitem][3] = "NP";
                    _root.radarbackitems[currentitem][4] = navpointdotcolour;
                } // end if
                if (_root.sectormapitems[i][0].substr(0, 2) == "TB")
                {
                    _root.radarbackitems[currentitem][0] = _root.sectormapitems[i][0];
                    _root.radarbackitems[currentitem][1] = _root.sectormapitems[i][1];
                    _root.radarbackitems[currentitem][2] = _root.sectormapitems[i][2];
                    _root.radarbackitems[currentitem][3] = "TB";
                    _root.radarbackitems[currentitem][4] = starbasedotcolour;
                } // end if
            } // end if
        } // end if
    } // end of for
    for (i = 0; i < _root.playersquadbases.length; i++)
    {
        if (_root.playersquadbases[i][4] == xsector || _root.playersquadbases[i][4] == xsector - 1 || _root.playersquadbases[i][4] == xsector + 1)
        {
            if (_root.playersquadbases[i][5] == ysector || _root.playersquadbases[i][5] == ysector - 1 || _root.playersquadbases[i][5] == ysector + 1)
            {
                currentitem = _root.radarbackitems.length;
                _root.radarbackitems[currentitem] = new Array();
                _root.radarbackitems[currentitem][0] = _root.playersquadbases[i][0];
                _root.radarbackitems[currentitem][1] = _root.playersquadbases[i][2];
                _root.radarbackitems[currentitem][2] = _root.playersquadbases[i][3];
                _root.radarbackitems[currentitem][3] = "SQUAD";
                _root.radarbackitems[currentitem][4] = squadbasedotcolour;
            } // end if
        } // end if
    } // end of for
} // End of the function
function sectoritemsdisp(xsector, ysector)
{
    sectoritems = new Array();
    totalsectoritems = _root.sectormapitems.length;
    for (i = 0; i < totalsectoritems; i++)
    {
        removeobject = true;
        if (_root.sectormapitems[i][10] == xsector)
        {
            if (_root.sectormapitems[i][11] == ysector)
            {
                removeobject = false;
                visibleitems = _root.playersquadbaseslvl;
                ++_root.playersquadbaseslvl;
                if (_root.playersquadbaseslvl > 100)
                {
                    _root.playersquadbaseslvl = 0;
                } // end if
                if (_root.sectormapitems[i][0].substr(0, 2) == "SB")
                {
                    for (jj = 0; jj < _root.starbaselocation.length; jj++)
                    {
                        if (_root.starbaselocation[jj][0] == _root.sectormapitems[i][0])
                        {
                            baseloc = jj;
                            break;
                        } // end if
                    } // end of for
                    if (_root.starbaselocation[baseloc][5] == "ACTIVE" || Number(_root.starbaselocation[baseloc][5]) < getTimer())
                    {
                        _root.gamedisplayarea.gamebackground.attachMovie("starbasetype" + _root.sectormapitems[i][3], _root.sectormapitems[i][0], 550 + visibleitems);
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._x = _root.sectormapitems[i][1];
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._y = _root.sectormapitems[i][2];
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].basename = _root.sectormapitems[i][0];
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0] + ".nametag"] = "STARBASE " + _root.sectormapitems[i][0];
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].baseidname = _root.sectormapitems[i][0];
                        if (_root.sectormapitems[i][9] > getTimer())
                        {
                            _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].isbasehostile = true;
                        }
                        else
                        {
                            _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].isbasehostile = false;
                        } // end else if
                        if (_root.sectormapitems[i][4] != "ON")
                        {
                            _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].displaymessage = true;
                            _root.sectormapitems[i][4] = "ON";
                        } // end if
                    }
                    else
                    {
                        _root.sectormapitems[i][4] = "OFF";
                    } // end if
                } // end else if
                if (_root.sectormapitems[i][0].substr(0, 2) == "PL")
                {
                    _root.gamedisplayarea.gamebackground.attachMovie("planettype" + _root.sectormapitems[i][3], _root.sectormapitems[i][0], 550 + visibleitems);
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._x = _root.sectormapitems[i][1];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._y = _root.sectormapitems[i][2];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].basename = _root.sectormapitems[i][0];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0] + ".nametag"] = "STARBASE " + _root.sectormapitems[i][0];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].baseidname = _root.sectormapitems[i][0];
                    if (_root.sectormapitems[i][9] > getTimer())
                    {
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].isbasehostile = true;
                    }
                    else
                    {
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].isbasehostile = false;
                    } // end else if
                    if (_root.sectormapitems[i][4] != "ON")
                    {
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].displaymessage = true;
                        _root.sectormapitems[i][4] = "ON";
                    } // end if
                } // end if
                if (_root.sectormapitems[i][0].substr(0, 2) == "NP")
                {
                    _root.gamedisplayarea.gamebackground.attachMovie("navpointtype" + _root.sectormapitems[i][3], _root.sectormapitems[i][0], 550 + visibleitems);
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._x = _root.sectormapitems[i][1];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._y = _root.sectormapitems[i][2];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0] + ".label"] = "NAV " + _root.sectormapitems[i][0].substr(2);
                    _root.sectormapitems[i][4] = "ON";
                } // end if
            } // end if
        } // end if
        if (removeobject == true)
        {
            removeMovieClip (_root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]);
        } // end if
    } // end of for
} // End of the function
starbasedotcolour = 16711935;
navpointdotcolour = 16777215;
squadbasedotcolour = 11184895;
_root.sectoritemsfortarget = new Array();
stop ();
