﻿// Action script...

function turnofshipcloak()
{
    for (qqq = 0; qqq < _root.playershipstatus[11][0].length; qqq++)
    {
        if (_root.playershipstatus[11][0][qqq] != null)
        {
            specialno = qqq + 1;
            speciallocation = qqq;
            itemlocation = _root.playershipstatus[11][0][speciallocation];
            specialtouse = _root.playershipstatus[11][1][itemlocation][0];
            if (_root.specialshipitems[specialtouse][5] == "CLOAK")
            {
                clearInterval(_root.specialstimers[specialno]);
                nexttime = _root.playershipstatus[11][2][speciallocation][0];
                _root.rightside.specialsingame.specialsinfo("UNCLOAKING", "RELOAD", nexttime, specialno);
                _root.playershipstatus[5][15] = "";
            } // end if
            _parent.playership._alpha = 100;
        } // end if
    } // end of for
} // End of the function
function turnoffdetecting()
{
    for (j = 0; j < _root.otherplayership.length; j++)
    {
        if (_root.otherplayership[j][16] == "D")
        {
            _root.otherplayership[j][16] = "";
        } // end if
    } // end of for
} // End of the function
function turnoffstealth()
{
    if (_root.playershipstatus[5][15] == "S")
    {
        _root.playershipstatus[5][15] = "";
    } // end if
} // End of the function
function detectstealthcloak(chance, specialno, specialtouse, speciallocation, operation)
{
    energydrain = _root.specialshipitems[specialtouse][2];
    if (_root.playershipstatus[1][1] >= energydrain)
    {
        _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
        nexttime = getTimer() + _root.specialshipitems[specialtouse][4];
        _root.playershipstatus[11][2][speciallocation][0] = nexttime;
        if (operation == "DETECTOR")
        {
            detectotherships(chance, specialno);
            _root.rightside.specialsingame.specialsinfo("DETECT ON", "ON", nexttime, specialno);
        }
        else if (operation == "STEALTH")
        {
            stealthplayership(chance, specialno, nexttime);
        }
        else if (operation == "CLOAK")
        {
            cloakplayership(chance, specialno, nexttime);
        }
        else
        {
            clearInterval(_root.specialstimers[specialno]);
        } // end else if
    }
    else
    {
        info = "NO ENERGY";
        status = "RELOAD";
        nexttime = getTimer() + 200;
        specialno = specialno;
        _root.rightside.specialsingame.specialsinfo(info, status, nexttime, specialno);
        clearInterval(_root.specialstimers[specialno]);
        if (_root.specialshipitems[specialtouse][5] == "CLOAK")
        {
            turnofshipcloak();
        } // end if
        if (_root.specialshipitems[specialtouse][5] == "DETECTOR")
        {
            turnoffdetecting();
        } // end if
        if (_root.specialshipitems[specialtouse][5] == "STEALTH")
        {
            turnoffstealth();
        } // end if
        clearInterval(_root.specialstimers[specialno]);
    } // end else if
} // End of the function
function displayfailed(specialno, nexttime)
{
} // End of the function
function detectotherships(chance, specialno)
{
    _root.rightside.radarscreen.radarscreenarea.func_pingcircledetector();
    for (j = 0; j < _root.otherplayership.length; j++)
    {
        if (_root.otherplayership[j][16] == "D" || _root.otherplayership[j][16] == "C" || _root.otherplayership[j][16] == "S")
        {
            detectchance = Math.random();
            if (chance > detectchance)
            {
                setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[j][0], _visible, true);
                if (_root.otherplayership[j][16] == "C")
                {
                    _root.gamedisplayarea["otherplayership" + _root.otherplayership[j][0]].attachMovie("shipactivateotherdetect", "shipactivateotherdetect", 69);
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _alpha, cloakalpha);
                }
                else
                {
                    setProperty("_root.gamedisplayarea.otherplayership" + _root.otherplayership[currentothership][0], _alpha, 100);
                } // end else if
                _root.otherplayership[j][16] = "D";
                continue;
            } // end if
            if (_root.otherplayership[j][16] == "D")
            {
                _root.otherplayership[j][16] = "";
            } // end if
        } // end if
    } // end of for
} // End of the function
function stealthplayership(chance, specialno, nexttime)
{
    stealthchance = Math.random();
    if (chance > stealthchance)
    {
        _root.playershipstatus[5][15] = "S";
        _root.rightside.specialsingame.specialsinfo("STEALTH ON", "ON", nexttime, specialno);
        _parent.playership.attachMovie("shipactivatestealth", "shipactivatestealth", 10);
    }
    else
    {
        _root.rightside.specialsingame.specialsinfo("FAILED", "RELOAD", nexttime, specialno);
        turnoffstealth();
        clearInterval(_root.specialstimers[specialno]);
    } // end else if
} // End of the function
function cloakplayership(chance, specialno, nexttime)
{
    cloakchance = Math.random();
    flagreason = false;
    if (_root.kingofflag[16] == false)
    {
        if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
        {
            flagreason = true;
        } // end if
    } // end if
    if (chance > cloakchance && !flagreason)
    {
        _root.playershipstatus[5][15] = "C";
        _root.rightside.specialsingame.specialsinfo("CLOAK ON", "ON", nexttime, specialno);
        _parent.playership.attachMovie("shipactivatecloak", "shipactivatecloak", 10);
        _parent.playership._alpha = cloakalpha;
    }
    else
    {
        _root.playershipstatus[5][15] = "";
        if (flagreason)
        {
            _root.rightside.specialsingame.specialsinfo("HAVE FLAG", "RELOAD", nexttime, specialno);
        }
        else
        {
            _root.rightside.specialsingame.specialsinfo("CLOAK FAILED", "RELOAD", nexttime, specialno);
        } // end else if
        clearInterval(_root.specialstimers[specialno]);
        _parent.playership._alpha = 100;
    } // end else if
} // End of the function
function runflarescript(distractchance, flarex, flarey)
{
    for (j = 0; j < _root.othermissilefire.length; j++)
    {
        if (_root.othermissilefire[j][13] == "SEEKING")
        {
            missilechance = Math.random();
            if (distractchance > missilechance)
            {
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[j][8] + ".distracted", true);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[j][8] + ".flarex", flarex);
                set("_root.gamedisplayarea.othermissilefire" + _root.othermissilefire[j][8] + ".flarey", flarey);
            } // end if
        } // end if
    } // end of for
} // End of the function
function runspecialone(specialno)
{
    if (specialno == 1)
    {
        itemlocation = _root.playershipstatus[11][0][0];
        specialtouse = _root.playershipstatus[11][1][itemlocation][0];
        speciallocation = 0;
    } // end if
    if (specialno == 2)
    {
        itemlocation = _root.playershipstatus[11][0][1];
        specialtouse = _root.playershipstatus[11][1][itemlocation][0];
        speciallocation = 1;
    } // end if
    if (specialno == 3)
    {
        itemlocation = _root.playershipstatus[11][0][2];
        specialtouse = _root.playershipstatus[11][1][itemlocation][0];
        speciallocation = 2;
    } // end if
    currenttime = getTimer();
    if (specialtouse != null && _root.playershipstatus[11][2][speciallocation][0] < currenttime)
    {
        if (_root.specialshipitems[specialtouse][5] == "FLARE")
        {
            energydrain = _root.specialshipitems[specialtouse][2];
            if (_root.playershipstatus[1][1] >= energydrain && _root.playershipstatus[11][2][speciallocation][0] < currenttime)
            {
                _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
                nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
                _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
                playershipx = Math.round(_root.shipcoordinatex);
                playershipy = Math.round(_root.shipcoordinatey);
                ++_root.currentplayershotsfired;
                if (_root.currentplayershotsfired > 999)
                {
                    _root.currentplayershotsfired = 0;
                } // end if
                i = _root.currentplayershotsfired;
                _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialtouse][8], "playerflare" + i, 5000 + i);
                setProperty("_root.gamedisplayarea.playerflare" + i, _x, playershipx);
                setProperty("_root.gamedisplayarea.playerflare" + i, _y, playershipy);
                set("_root.gamedisplayarea.playerflare" + i + ".xposition", playershipx);
                set("_root.gamedisplayarea.playerflare" + i + ".yposition", playershipy);
                set("_root.gamedisplayarea.playerflare" + i + ".removetime", currenttime + _root.specialshipitems[specialtouse][3]);
                runflarescript(_root.specialshipitems[specialtouse][10], playershipx, playershipy);
                _root.gunfireinformation = _root.gunfireinformation + ("SP`" + _root.playershipstatus[3][0] + "`" + specialtouse + "`" + playershipx + "`" + playershipy + "~");
            } // end if
        }
        else if (_root.specialshipitems[specialtouse][5] == "PULSAR")
        {
            if (_root.pulsarsinzone != false)
            {
                if (_root.playershipstatus[11][1][itemlocation][1] > 0 && _root.playershipstatus[5][20] != true)
                {
                    energydrain = _root.specialshipitems[specialtouse][2];
                    if (func_checkifplayerindockingarea())
                    {
                        message = "You can not pulsar in the docking circle.";
                        _root.func_messangercom(message, "HELP", "BEGIN");
                    }
                    else if (_root.playershipstatus[1][1] >= energydrain && _root.playershipstatus[11][2][speciallocation][0] < currenttime)
                    {
                        --_root.playershipstatus[11][1][itemlocation][1];
                        _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
                        nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
                        _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
                        playershipx = Math.round(_root.shipcoordinatex);
                        playershipy = Math.round(_root.shipcoordinatey);
                        _root.bringinspecial(_root.playershipstatus[3][0], specialtouse, playershipx, playershipy, "true");
                        _root.gunfireinformation = _root.gunfireinformation + ("SP`" + _root.playershipstatus[3][0] + "`" + specialtouse + "`" + playershipx + "`" + playershipy + "~");
                    }
                    else if (_root.playershipstatus[1][1] < energydrain)
                    {
                        message = "You need to more energy in order to fire the pulsar.";
                        _root.func_messangercom(message, "HELP", "BEGIN");
                    } // end else if
                } // end else if
            }
            else
            {
                message = "Pulsars Are Not Usable In This Zone.";
                _root.func_messangercom(message, "HELP", "BEGIN");
            } // end else if
        }
        else if (_root.specialshipitems[specialtouse][5] == "DETECTOR" || _root.specialshipitems[specialtouse][5] == "STEALTH" || _root.specialshipitems[specialtouse][5] == "CLOAK")
        {
            clearInterval(_root.specialstimers[specialno]);
            chance = _root.specialshipitems[specialtouse][10];
            nextcheckwait = _root.specialshipitems[specialtouse][11];
            operation = _root.specialshipitems[specialtouse][5];
            _root.specialstimers[specialno] = setInterval(detectstealthcloak, nextcheckwait, chance, specialno, specialtouse, speciallocation, operation);
            detectstealthcloak(chance, specialno, specialtouse, speciallocation, operation);
        }
        else if (_root.specialshipitems[specialtouse][5] == "RECHARGESHIELD")
        {
            energydrain = Math.floor(_root.playershipstatus[1][1]);
            _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
            nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
            _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
            shieldrech = energydrain * _root.specialshipitems[specialtouse][2];
            shieldgen = _root.playershipstatus[2][0];
            maxshield = _root.shieldgenerators[shieldgen][0];
            curshieldstr = _root.playershipstatus[2][1];
            if (shieldrech + curshieldstr > maxshield)
            {
                shieldrech = Math.floor(maxshield - curshieldstr);
            } // end if
            _root.playershipstatus[2][1] = _root.playershipstatus[2][1] + shieldrech;
            _root.gunfireinformation = _root.gunfireinformation + ("SP`" + _root.playershipstatus[3][0] + "`" + specialtouse + "`" + shieldrech + "~");
        }
        else if (_root.specialshipitems[specialtouse][5] == "RECHARGESTRUCT")
        {
            energydrain = Math.floor(_root.playershipstatus[1][1]);
            _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - energydrain;
            nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
            _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
            structrep = energydrain * _root.specialshipitems[specialtouse][2];
            maxstructure = _root.shiptype[_root.playershipstatus[5][0]][3][3];
            curstruct = _root.playershipstatus[2][5];
            if (curstruct + structrep > maxstructure)
            {
                structrep = Math.floor(maxstructure - curstruct);
            } // end if
            _root.playershipstatus[2][5] = _root.playershipstatus[2][5] + structrep;
            _root.gunfireinformation = _root.gunfireinformation + ("SP`" + _root.playershipstatus[3][0] + "`" + specialtouse + "`" + structrep + "~");
            _parent.func_removedamages();
        }
        else if (_root.specialshipitems[specialtouse][5] == "MINES")
        {
            energydrain = _root.specialshipitems[specialtouse][2];
            if (_root.playershipstatus[5][15] == "C")
            {
                message = "You can not drop mines while you are cloaked.";
                _root.func_messangercom(message, "HELP", "BEGIN");
            }
            else if (_root.playershipstatus[11][1][itemlocation][1] > 0 && _root.playershipstatus[5][20] != true && _root.playershipstatus[2][5] > 0)
            {
                --_root.playershipstatus[11][1][itemlocation][1];
                nexttime = _root.playershipstatus[11][2][speciallocation][0] = currenttime + _root.specialshipitems[specialtouse][4];
                _root.rightside.specialsingame.specialsinfo("RELOAD", "RELOAD", nexttime, specialno);
                playershipx = Math.round(_root.shipcoordinatex);
                playershipy = Math.round(_root.shipcoordinatey);
                ++_root.currentplayershotsfired;
                if (_root.currentplayershotsfired > 999)
                {
                    _root.currentplayershotsfired = 0;
                } // end if
                i = _root.currentplayershotsfired;
                mineid = _root.playershipstatus[3][0] + "a" + i;
                _root.gamedisplayarea.attachMovie(_root.specialshipitems[specialtouse][8], "playermine" + mineid, 5000 + i);
                setProperty("_root.gamedisplayarea.playermine" + mineid, _x, playershipx);
                setProperty("_root.gamedisplayarea.playermine" + mineid, _y, playershipy);
                set("_root.gamedisplayarea.playermine" + mineid + ".xposition", playershipx);
                set("_root.gamedisplayarea.playermine" + mineid + ".yposition", playershipy);
                set("_root.gamedisplayarea.playermine" + mineid + ".removetime", currenttime + _root.specialshipitems[specialtouse][3]);
                set("_root.gamedisplayarea.playermine" + mineid + ".ishostilemine", false);
                set("_root.gamedisplayarea.playermine" + mineid + ".mineid", mineid);
                _root.gunfireinformation = _root.gunfireinformation + ("SP`" + mineid + "`" + specialtouse + "`" + playershipx + "`" + playershipy + "~");
            } // end else if
        } // end else if
    }
    else if (_root.specialshipitems[specialtouse][5] == "DETECTOR" || _root.specialshipitems[specialtouse][5] == "STEALTH" || _root.specialshipitems[specialtouse][5] == "CLOAK")
    {
        nexttime = _root.playershipstatus[11][2][speciallocation][0];
        _root.rightside.specialsingame.specialsinfo("SHUTTING OFF", "RELOAD", nexttime, specialno);
        clearInterval(_root.specialstimers[specialno]);
        if (_root.specialshipitems[specialtouse][5] == "STEALTH")
        {
            if (_root.playershipstatus[5][15] == "S")
            {
                turnoffstealth();
            } // end if
        } // end if
        if (_root.specialshipitems[specialtouse][5] == "CLOAK")
        {
            if (_root.playershipstatus[5][15] == "C")
            {
                turnofshipcloak();
            } // end if
        } // end if
        if (_root.specialshipitems[specialtouse][5] == "DETECTOR")
        {
            turnoffdetecting();
        } // end if
    } // end else if
} // End of the function
function func_checkifplayerindockingarea()
{
    nearabase = false;
    for (i = 0; i < _root.starbaselocation.length; i++)
    {
        if (_root.starbaselocation[i][5] == "ACTIVE" || Number(_root.starbaselocation[i][5]) < getTimer())
        {
            starbasex = _root.starbaselocation[i][1];
            starbasey = _root.starbaselocation[i][2];
            starbaseradius = _root.starbaselocation[i][4];
            relativestarbasex = starbasex - _root.shipcoordinatex;
            relativestarbasey = starbasey - _root.shipcoordinatey;
            if (relativestarbasex * relativestarbasex + relativestarbasey * relativestarbasey <= starbaseradius * starbaseradius)
            {
                nearabase = true;
                break;
            } // end if
        } // end if
    } // end of for
    return (nearabase);
} // End of the function
clearInterval(_root.specialstimers[1]);
clearInterval(_root.specialstimers[2]);
clearInterval(_root.specialstimers[3]);
_root.specialstimers = new Array();
_root.specialstimers[0] = 0;
_root.specialstimers[1] = 0;
_root.specialstimers[2] = 0;
_root.specialstimers[3] = 0;
cloakalpha = 60;
_root.playershipstatus[11][2][2][0] = 0;
