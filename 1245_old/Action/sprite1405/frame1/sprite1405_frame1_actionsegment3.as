﻿// Action script...

function func_checkforjumping()
{
    for (i = 0; i < _root.sectormapitems.length; i++)
    {
        if (_root.sectormapitems[i][0].substr(0, 2) == "NP" && _root.sectormapitems[i][3] == 10)
        {
            navpointx = _root.sectormapitems[i][1];
            navpointy = _root.sectormapitems[i][2];
            jumpradius = 50;
            relativejumpx = navpointx - _root.shipcoordinatex;
            relativejumpy = navpointy - _root.shipcoordinatey;
            zonetogoto = _root.sectormapitems[i][9];
            if (relativejumpx * relativejumpx + relativestarbasey * relativejumpy <= jumpradius * jumpradius)
            {
                if (playershipvelocity > maxjumpvelocity)
                {
                    message = "You must have a velocity lass than " + maxjumpvelocity + " in order to use a Jump Point";
                    _root.func_messangercom(message, ainame, "BEGIN");
                    continue;
                } // end if
                if (_root.mapdsiplayed == false)
                {
                    _root.gamedisplayarea.testtt = "Jumping at " + _root.sectormapitems[i][0];
                    if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
                    {
                        datatosend = "KFLAG`DROP`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + _root.errorchecknumber + "~";
                        _root.mysocket.send(datatosend);
                        _root.errorcheckedmessage(datatosend, _root.errorchecknumber);
                    } // end if
                    removeMovieClip ("_root.toprightinformation");
                    information = "PI`CLEAR`~";
                    _root.mysocket.send(information);
                    _root.controlledserverclose = false;
                    _root.playershipstatus[3][0] = null;
                    _root.reconnectingfromgame = true;
                    goingtozone = zonetogoto;
                    _root.jumpinginfo = new Array(goingtozone, _root.portandsystem);
                    _root.portandsystem = goingtozone;
                    _root.currentgamestatus = "Jumping Sequence Initiated \r Attempting to Jump to system " + goingtozone + "\r\r";
                    _root.mysocket.close();
                    _root.gotoAndStop("jumpingzonestart");
                } // end if
            } // end if
        } // end if
    } // end of for
} // End of the function
maxjumpvelocity = maxdockingvelocity;
_root.jumpinginfo = new Array();
