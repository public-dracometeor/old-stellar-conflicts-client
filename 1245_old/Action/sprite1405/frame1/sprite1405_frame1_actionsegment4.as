﻿// Action script...

function func_setsectortargetitem(method, name, xcoord, ycoord, shield, struct)
{
    if (method == "ADD")
    {
        location = _root.sectoritemsfortarget.length;
        _root.sectoritemsfortarget[location] = new Array();
        _root.sectoritemsfortarget[location][0] = name;
        _root.sectoritemsfortarget[location][1] = xcoord;
        _root.sectoritemsfortarget[location][2] = ycoord;
        _root.sectoritemsfortarget[location][5] = "LOADING";
        _root.sectoritemsfortarget[location][6] = "LOADING";
    }
    else if (method == "REMOVE")
    {
        for (location = 0; location < _root.sectoritemsfortarget.length; location++)
        {
            if (name == _root.sectoritemsfortarget[location][0])
            {
                _root.sectoritemsfortarget.splice(location, 1);
                --location;
            } // end if
        } // end of for
    } // end else if
} // End of the function
function func_redrawsquadbase(basename, upgradetype)
{
    if (upgradetype == "LEVEL")
    {
        for (qq = 0; qq < _root.playersquadbases.length; qq++)
        {
            if (_root.playersquadbases[qq][0] == basename)
            {
                removeMovieClip (_root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[qq][0]]);
                _root.playersquadbases[qq][10] = false;
                break;
            } // end if
        } // end of for
        playersquadbasesdisplay(xsector, ysector);
    } // end if
} // End of the function
function playersquadbasesdisplay(xsector, ysector)
{
    for (jj = 0; jj < _root.playersquadbases.length; jj++)
    {
        squadbasexsector = _root.playersquadbases[jj][4];
        squadbaseysector = _root.playersquadbases[jj][5];
        if (squadbasexsector == xsector && squadbaseysector == ysector && _root.playersquadbases[jj][10] == false)
        {
            method = "ADD";
            name = _root.playersquadbases[jj][0];
            xcoord = _root.playersquadbases[jj][2];
            ycoord = _root.playersquadbases[jj][3];
            func_setsectortargetitem(method, name, xcoord, ycoord);
            _root.playersquadbases[jj][10] = true;
            basetype = _root.playersquadbases[jj][1];
            _root.gamedisplayarea.gamebackground.attachMovie("squadbasetype" + basetype, "playerbase" + _root.playersquadbases[jj][0], 550 + _root.playersquadbaseslvl);
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]]._x = _root.playersquadbases[jj][2];
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]]._y = _root.playersquadbases[jj][3];
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]].baseidname = _root.playersquadbases[jj][0];
            _root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]].squadid = _root.playersquadbases[jj][0];
            ++_root.playersquadbaseslvl;
            if (_root.playersquadbaseslvl > 100)
            {
                _root.playersquadbaseslvl = 0;
            } // end if
            continue;
        } // end if
        removebase = true;
        if (_root.playersquadbases[jj][10] == true)
        {
            if (squadbasexsector == xsector || squadbasexsector == xsector - 1 || squadbasexsector == xsector + 1)
            {
                if (squadbaseysector == ysector || squadbaseysector == ysector - 1 || squadbaseysector == ysector + 1)
                {
                    removebase = false;
                } // end if
            } // end if
        } // end if
        if (removebase)
        {
            removeMovieClip (_root.gamedisplayarea.gamebackground["playerbase" + _root.playersquadbases[jj][0]]);
            _root.playersquadbases[jj][10] = false;
            method = "REMOVE";
            name = _root.playersquadbases[jj][0];
            func_setsectortargetitem(method, name);
        } // end if
    } // end of for
    if (_root.teamdeathmatch == true)
    {
        teamsbasesdisplay(xsector, ysector);
    }
    else
    {
        sectoritemsdisp(xsector, ysector);
    } // end else if
    radarbackgrounditems(xsector, ysector);
} // End of the function
function teamsbasesdisplay(xsector, ysector)
{
    for (jj = 0; jj < _root.sectormapitems.length; jj++)
    {
        if (_root.sectormapitems[jj][0].substr(0, 2) == "TB")
        {
            squadbasexsector = _root.sectormapitems[jj][10];
            squadbaseysector = _root.sectormapitems[jj][11];
            if (squadbasexsector == xsector && squadbaseysector == ysector && _root.sectormapitems[jj][4] != "ON")
            {
                _root.sectormapitems[jj][4] = "ON";
                method = "ADD";
                name = _root.sectormapitems[jj][0];
                xcoord = _root.sectormapitems[jj][1];
                ycoord = _root.sectormapitems[jj][2];
                func_setsectortargetitem(method, name, xcoord, ycoord);
                basetype = _root.sectormapitems[jj][3];
                _root.gamedisplayarea.gamebackground.attachMovie("teambasetype0", "teambase" + _root.sectormapitems[jj][0], 550 + _root.playersquadbaseslvl);
                _root.gamedisplayarea.gamebackground["teambase" + _root.sectormapitems[jj][0]]._x = _root.sectormapitems[jj][1];
                _root.gamedisplayarea.gamebackground["teambase" + _root.sectormapitems[jj][0]]._y = _root.sectormapitems[jj][2];
                _root.gamedisplayarea.gamebackground["teambase" + _root.sectormapitems[jj][0]].baseidname = _root.sectormapitems[jj][0];
                ++_root.playersquadbaseslvl;
                if (_root.playersquadbaseslvl > 100)
                {
                    _root.playersquadbaseslvl = 0;
                } // end if
                continue;
            } // end if
            removebase = true;
            if (_root.sectormapitems[jj][4] == "ON")
            {
                if (squadbasexsector == xsector || squadbasexsector == xsector - 1 || squadbasexsector == xsector + 1)
                {
                    if (squadbaseysector == ysector || squadbaseysector == ysector - 1 || squadbaseysector == ysector + 1)
                    {
                        removebase = false;
                    } // end if
                } // end if
            } // end if
            if (removebase)
            {
                removeMovieClip (_root.gamedisplayarea.gamebackground["teambase" + _root.sectormapitems[jj][0]]);
                _root.sectormapitems[jj][4] = "OFF";
                method = "REMOVE";
                name = _root.sectormapitems[jj][0];
                func_setsectortargetitem(method, name);
            } // end if
        } // end if
    } // end of for
    playerteambasesdisplay(xsector, ysector);
    sectoritemsdisp(xsector, ysector);
    radarbackgrounditems(xsector, ysector);
} // End of the function
function radarbackgrounditems(xsector, ysector)
{
    _root.radarbackitems = new Array();
    for (i = 0; i < _root.sectormapitems.length; i++)
    {
        if (_root.sectormapitems[i][10] == xsector || _root.sectormapitems[i][10] == xsector - 1 || _root.sectormapitems[i][10] == xsector + 1)
        {
            if (_root.sectormapitems[i][11] == ysector || _root.sectormapitems[i][11] == ysector - 1 || _root.sectormapitems[i][11] == ysector + 1)
            {
                currentitem = _root.radarbackitems.length;
                _root.radarbackitems[currentitem] = new Array();
                if (_root.sectormapitems[i][0].substr(0, 2) == "SB" || _root.sectormapitems[i][0].substr(0, 2) == "PL")
                {
                    _root.radarbackitems[currentitem][0] = _root.sectormapitems[i][0];
                    _root.radarbackitems[currentitem][1] = _root.sectormapitems[i][1];
                    _root.radarbackitems[currentitem][2] = _root.sectormapitems[i][2];
                    _root.radarbackitems[currentitem][3] = "SB";
                    _root.radarbackitems[currentitem][4] = starbasedotcolour;
                } // end if
                if (_root.sectormapitems[i][0].substr(0, 2) == "NP")
                {
                    _root.radarbackitems[currentitem][0] = _root.sectormapitems[i][0];
                    _root.radarbackitems[currentitem][1] = _root.sectormapitems[i][1];
                    _root.radarbackitems[currentitem][2] = _root.sectormapitems[i][2];
                    _root.radarbackitems[currentitem][3] = "NP";
                    _root.radarbackitems[currentitem][4] = navpointdotcolour;
                } // end if
                if (_root.sectormapitems[i][0].substr(0, 2) == "TB")
                {
                    _root.radarbackitems[currentitem][0] = _root.sectormapitems[i][0];
                    _root.radarbackitems[currentitem][1] = _root.sectormapitems[i][1];
                    _root.radarbackitems[currentitem][2] = _root.sectormapitems[i][2];
                    _root.radarbackitems[currentitem][3] = "TB";
                    _root.radarbackitems[currentitem][4] = starbasedotcolour;
                } // end if
            } // end if
        } // end if
    } // end of for
    for (i = 0; i < _root.playersquadbases.length; i++)
    {
        if (_root.playersquadbases[i][4] == xsector || _root.playersquadbases[i][4] == xsector - 1 || _root.playersquadbases[i][4] == xsector + 1)
        {
            if (_root.playersquadbases[i][5] == ysector || _root.playersquadbases[i][5] == ysector - 1 || _root.playersquadbases[i][5] == ysector + 1)
            {
                currentitem = _root.radarbackitems.length;
                _root.radarbackitems[currentitem] = new Array();
                _root.radarbackitems[currentitem][0] = _root.playersquadbases[i][0];
                _root.radarbackitems[currentitem][1] = _root.playersquadbases[i][2];
                _root.radarbackitems[currentitem][2] = _root.playersquadbases[i][3];
                _root.radarbackitems[currentitem][3] = "SQUAD";
                _root.radarbackitems[currentitem][4] = squadbasedotcolour;
            } // end if
        } // end if
    } // end of for
} // End of the function
function sectoritemsdisp(xsector, ysector)
{
    sectoritems = new Array();
    totalsectoritems = _root.sectormapitems.length;
    for (i = 0; i < totalsectoritems; i++)
    {
        removeobject = true;
        if (_root.sectormapitems[i][10] == xsector)
        {
            if (_root.sectormapitems[i][11] == ysector)
            {
                removeobject = false;
                visibleitems = _root.playersquadbaseslvl;
                ++_root.playersquadbaseslvl;
                if (_root.playersquadbaseslvl > 100)
                {
                    _root.playersquadbaseslvl = 0;
                } // end if
                if (_root.sectormapitems[i][0].substr(0, 2) == "SB")
                {
                    for (jj = 0; jj < _root.starbaselocation.length; jj++)
                    {
                        if (_root.starbaselocation[jj][0] == _root.sectormapitems[i][0])
                        {
                            baseloc = jj;
                            break;
                        } // end if
                    } // end of for
                    if (_root.starbaselocation[baseloc][5] == "ACTIVE" || Number(_root.starbaselocation[baseloc][5]) < getTimer())
                    {
                        _root.gamedisplayarea.gamebackground.attachMovie("starbasetype" + _root.sectormapitems[i][3], _root.sectormapitems[i][0], 550 + visibleitems);
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._x = _root.sectormapitems[i][1];
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._y = _root.sectormapitems[i][2];
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].basename = _root.sectormapitems[i][0];
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0] + ".nametag"] = "STARBASE " + _root.sectormapitems[i][0];
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].baseidname = _root.sectormapitems[i][0];
                        if (_root.sectormapitems[i][9] > getTimer())
                        {
                            _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].isbasehostile = true;
                        }
                        else
                        {
                            _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].isbasehostile = false;
                        } // end else if
                        if (_root.sectormapitems[i][4] != "ON")
                        {
                            _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].displaymessage = true;
                            _root.sectormapitems[i][4] = "ON";
                        } // end if
                    }
                    else
                    {
                        _root.sectormapitems[i][4] = "OFF";
                    } // end if
                } // end else if
                if (_root.sectormapitems[i][0].substr(0, 2) == "PL")
                {
                    _root.gamedisplayarea.gamebackground.attachMovie("planettype" + _root.sectormapitems[i][3], _root.sectormapitems[i][0], 550 + visibleitems);
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._x = _root.sectormapitems[i][1];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._y = _root.sectormapitems[i][2];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].basename = _root.sectormapitems[i][0];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0] + ".nametag"] = "STARBASE " + _root.sectormapitems[i][0];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].baseidname = _root.sectormapitems[i][0];
                    if (_root.sectormapitems[i][9] > getTimer())
                    {
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].isbasehostile = true;
                    }
                    else
                    {
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].isbasehostile = false;
                    } // end else if
                    if (_root.sectormapitems[i][4] != "ON")
                    {
                        _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]].displaymessage = true;
                        _root.sectormapitems[i][4] = "ON";
                    } // end if
                } // end if
                if (_root.sectormapitems[i][0].substr(0, 2) == "NP")
                {
                    _root.gamedisplayarea.gamebackground.attachMovie("navpointtype" + _root.sectormapitems[i][3], _root.sectormapitems[i][0], 550 + visibleitems);
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._x = _root.sectormapitems[i][1];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]._y = _root.sectormapitems[i][2];
                    _root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0] + ".label"] = "NAV " + _root.sectormapitems[i][0].substr(2);
                    _root.sectormapitems[i][4] = "ON";
                } // end if
            } // end if
        } // end if
        if (removeobject == true)
        {
            removeMovieClip (_root.gamedisplayarea.gamebackground[_root.sectormapitems[i][0]]);
        } // end if
    } // end of for
} // End of the function
starbasedotcolour = 16711935;
navpointdotcolour = 16777215;
squadbasedotcolour = 11184895;
_root.sectoritemsfortarget = new Array();
stop ();
