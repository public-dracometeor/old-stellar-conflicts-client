﻿// Action script...

// [onClipEvent of sprite 1405 in frame 1]
onClipEvent (load)
{
    function movementofanobjectwiththrust()
    {
        if (relativefacing == 0)
        {
            ymovement = -velocity;
            xmovement = 0;
        } // end if
        if (velocity != 0)
        {
            this.relativefacing = Math.round(this.relativefacing);
            ymovement = -velocity * _root.cosines[this.relativefacing];
            xmovement = velocity * _root.sines[this.relativefacing];
        }
        else
        {
            ymovement = 0;
            xmovement = 0;
        } // end else if
    } // End of the function
    function disruptplayersengines()
    {
        isupkeypressed = false;
        isdownkeypressed = true;
        isshiftkeypressed = false;
        _root.keywaspressed = true;
    } // End of the function
    firstenter = true;
    _root.playershipstatus[5][20] = true;
    _root.playershipstatus[5][21] = getTimer() + 7000;
    _root.playershipstatus[6][0] = Math.ceil(shipcoordinatex / xwidthofasector);
    _root.playershipstatus[6][1] = Math.ceil(shipcoordinatey / ywidthofasector);
    xsector = _root.playershipstatus[6][0];
    ysector = _root.playershipstatus[6][1];
    playersquadbasesdisplay(xsector, ysector);
    lastsector = "";
    this.playershipmaxvelocity = _root.playershipmaxvelocity;
    this.playershipacceleration = _root.playershipacceleration;
    this.playershipfacing = _root.playershipfacing;
    relativefacing = this.playershipfacing;
    _root.gamedisplayarea.playership._rotation = _root.playershipfacing;
    timetillrotationadjust = getTimer();
    rotationadjusttime = 500;
    lastrotationtype = null;
    playersshiptype = _root.playershipstatus[5][0];
    this.playershiprotation = _root.shiptype[playersshiptype][3][2];
    this.afterburnerspeed = _root.shiptype[playersshiptype][3][6];
    _root.afterburnerinuse = false;
    playerrotationdegredation = _root.shiptype[playersshiptype][3][5];
    this._visible = false;
    secondlastturning = 0;
    lastturning = 0;
    guntype0sound = new Sound();
    guntype0sound.attachSound("guntype0sound");
    guntype1sound = new Sound();
    guntype1sound.attachSound("guntype1sound");
    guntype2sound = new Sound();
    guntype2sound.attachSound("guntype2sound");
    guntype3sound = new Sound();
    guntype3sound.attachSound("guntype3sound");
    this.framespersecond = _root.framespersecond;
    this.gameareawidth = _root.gameareawidth;
    this.gameareaheight = _root.gameareaheight;
    this.totalstars = _root.totalstars;
    this.halfgameareawidth = Math.round(gameareawidth / 2);
    this.halfgameareaheight = Math.round(gameareaheight / 2);
    xwidthofasector = _root.sectorinformation[1][0];
    ywidthofasector = _root.sectorinformation[1][1];
    isupkeypressed = false;
    isdownkeypressed = false;
    isleftkeypressed = false;
    isrightkeypressed = false;
    iscontrolkeypressed = false;
    isdkeypressed = false;
    isshiftkeypressed = false;
    isspacekeypressed = false;
    _root.currentplayershotsfired = 0;
    lasttimerotationchanged = 0;
    rotationbuffertime = 100;
    currentturning = 0;
    currenthelpframedisplayed = 0;
    maxdockingvelocity = 20;
    currentframeforcalc = 0;
    frameintforcalc = 25;
    rotationdelay = 1;
    currentrotationframe = 0;
    currenttimechangeratio = 0;
    curenttime = getTimer();
    lasttime = curenttime;
}

// [onClipEvent of sprite 1405 in frame 1]
onClipEvent (load)
{
    Key.removeListener(keyListener);
    keyListener = new Object();
    keyListener.onKeyUp = function ()
    {
        keyreleased = Key.getCode();
        lastkeypressed = null;
        if (keyreleased == "38")
        {
            if (isupkeypressed == true)
            {
                isupkeypressed = false;
                _root.keywaspressed = true;
            } // end if
        } // end if
        if (keyreleased == "40")
        {
            if (isdownkeypressed == true)
            {
                isdownkeypressed = false;
                _root.keywaspressed = true;
            } // end if
        } // end if
        if (keyreleased == "37")
        {
            if (isleftkeypressed == true)
            {
                isleftkeypressed = false;
                _root.keywaspressed = true;
            } // end if
        } // end if
        if (keyreleased == "39")
        {
            if (isrightkeypressed == true)
            {
                isrightkeypressed = false;
                _root.keywaspressed = true;
            } // end if
        } // end if
        if (keyreleased == "17")
        {
            if (iscontrolkeypressed == true)
            {
                iscontrolkeypressed = false;
            } // end if
        } // end if
        if (keyreleased == "68")
        {
            if (isdkeypressed == true)
            {
                isdkeypressed = false;
            } // end if
        } // end if
        if (keyreleased == "16")
        {
            if (isshiftkeypressed == true)
            {
                isshiftkeypressed = false;
            } // end if
        } // end if
        if (keyreleased == "32")
        {
            if (isspacekeypressed == true)
            {
                isspacekeypressed = false;
                spacekeyjustpressed = true;
            } // end if
        } // end if
    };
    keyListener.onKeyDown = function ()
    {
        keypressed = Key.getCode();
        if (lastkeypressed != keypressed)
        {
            lastkeypressed = keypressed;
            if (_root.isplayerdisrupt)
            {
            }
            else if (keypressed == "38")
            {
                if (isupkeypressed == false)
                {
                    isupkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "40")
            {
                if (isdownkeypressed == false)
                {
                    isdownkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "16")
            {
                if (isshiftkeypressed == false)
                {
                    isshiftkeypressed = true;
                } // end else if
            } // end else if
            if (keypressed == "37")
            {
                if (isleftkeypressed == false)
                {
                    isleftkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "39")
            {
                if (isrightkeypressed == false)
                {
                    isrightkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "17")
            {
                if (iscontrolkeypressed == false)
                {
                    iscontrolkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "88")
            {
                if (_root.gamechatinfo[4] == false)
                {
                    runspecialone(2);
                } // end if
            }
            else if (keypressed == "90")
            {
                if (_root.gamechatinfo[4] == false)
                {
                    runspecialone(1);
                } // end if
            }
            else if (keypressed == "83")
            {
                if (_root.gamechatinfo[4] == false)
                {
                    runspecialone(3);
                } // end if
            }
            else if (keypressed == "68")
            {
                if (isdkeypressed == false)
                {
                    isdkeypressed = true;
                } // end if
            }
            else if (keypressed == "74")
            {
                if (_root.gamechatinfo[4] == false)
                {
                    func_checkforjumping();
                } // end if
            }
            else if (keypressed == "32")
            {
                if (isspacekeypressed == false)
                {
                    isspacekeypressed = true;
                } // end if
            }
            else if (keypressed == "123")
            {
                if (currenthelpframedisplayed == 0)
                {
                    _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                    _root.ingamehelp.helpmode = "ingame";
                    currenthelpframedisplayed = 1;
                }
                else if (currenthelpframedisplayed == 1)
                {
                    currenthelpframedisplayed = 0;
                    _root.ingamehelp.gotoAndPlay("close");
                } // end else if
            } // end else if
        } // end else if
    };
    Key.addListener(keyListener);
}

// [onClipEvent of sprite 1405 in frame 1]
onClipEvent (enterFrame)
{
    function firingmissilestartlocation()
    {
        hardpointfromcenter = Math.sqrt(xfireposition * xfireposition + yfireposition * yfireposition);
        initialangleforgun = Math.ASIN(xfireposition / hardpointfromcenter) / 0.017453;
        if (_root.playershipstatus[7][g][2] >= 0)
        {
            if (_root.playershipstatus[7][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[7][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
        if (_root.playershipstatus[7][g][2] < 0)
        {
            if (_root.playershipstatus[7][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[7][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
    } // End of the function
    function firingbulletstartlocation()
    {
        hardpointfromcenter = Math.sqrt(xfireposition * xfireposition + yfireposition * yfireposition);
        initialangleforgun = Math.ASIN(xfireposition / hardpointfromcenter) / 0.017453;
        if (_root.playershipstatus[0][g][2] >= 0)
        {
            if (_root.playershipstatus[0][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[0][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
        if (_root.playershipstatus[0][g][2] < 0)
        {
            if (_root.playershipstatus[0][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[0][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
    } // End of the function
    function missilefirestartlocation()
    {
        hardpointfromcenter = Math.sqrt(xfireposition * xfireposition + yfireposition * yfireposition);
        initialangleforgun = Math.ASIN(xfireposition / hardpointfromcenter) / 0.017453;
        if (_root.playershipstatus[7][g][2] >= 0)
        {
            if (_root.playershipstatus[7][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[7][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
        if (_root.playershipstatus[7][g][2] < 0)
        {
            if (_root.playershipstatus[7][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[7][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
    } // End of the function
    function gunfiremovement()
    {
        for (i = 0; i < _root.playershotsfired.length; i++)
        {
            if (_root.playershotsfired[i][5] < curenttime)
            {
                removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                _root.playershotsfired.splice(i, 1);
                --i;
            } // end if
        } // end of for
    } // End of the function
    function findoutcurrentsector()
    {
        lastsector = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
        _root.playershipstatus[6][0] = Math.ceil(shipcoordinatex / xwidthofasector);
        _root.playershipstatus[6][1] = Math.ceil(shipcoordinatey / ywidthofasector);
        currentsector = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
        if (lastsector != currentsector)
        {
            xsector = _root.playershipstatus[6][0];
            ysector = _root.playershipstatus[6][1];
            playersquadbasesdisplay(xsector, ysector);
        } // end if
    } // End of the function
    lasttime = curenttime;
    curenttime = getTimer();
    currenttimechangeratio = (curenttime - lasttime) * 0.001000;
    if (_root.isplayerdisrupt)
    {
        if (_root.playerdiruptend < getTimer())
        {
            _root.isplayerdisrupt = false;
            isdownkeypressed = false;
        } // end if
    } // end if
    ++currentframeforcalc;
    if (currentframeforcalc > frameintforcalc)
    {
        currentframeforcalc = 0;
        avetimeperfram = (curenttime - lastimeframecaldone) / frameintforcalc;
        framepersec = Math.round(1000 / avetimeperfram);
        _root.gamedisplayarea.test2 = "Frame Rate: " + avetimeperfram + " ms." + "FPS: " + framepersec;
        if (framepersec < 5)
        {
            if (firstenter == false)
            {
                _root.controlledserverclose = true;
                _root.gameerror = "fpsstopped";
                _root.func_closegameserver();
            } // end if
        } // end if
        firstenter = false;
        lastimeframecaldone = curenttime;
    } // end if
    this.playershipfacing = _root.playershipfacing;
    this.playershipvelocity = _root.playershipvelocity;
    this.shipcoordinatex = _root.shipcoordinatex;
    this.shipcoordinatey = _root.shipcoordinatey;
    shotstobedeleted = new Array();
    if (_root.playershipstatus[5][4] == "alive")
    {
        if (isdkeypressed == true && _root.gamechatinfo[4] == false)
        {
            for (i = 0; i < _root.starbaselocation.length; i++)
            {
                if (_root.starbaselocation[i][5] == "ACTIVE" || Number(_root.starbaselocation[i][5]) < getTimer())
                {
                    starbasex = _root.starbaselocation[i][1];
                    starbasey = _root.starbaselocation[i][2];
                    starbaseradius = _root.starbaselocation[i][4];
                    relativestarbasex = starbasex - _root.shipcoordinatex;
                    relativestarbasey = starbasey - _root.shipcoordinatey;
                    if (relativestarbasex * relativestarbasex + relativestarbasey * relativestarbasey <= starbaseradius * starbaseradius)
                    {
                        if (_root.starbaselocation[i][0].substr(0, 2) == "SB")
                        {
                            ainame = "STARBASE";
                        }
                        else if (_root.starbaselocation[i][0].substr(0, 2) == "PL")
                        {
                            pltype = _root.starbaselocation[i][3];
                            ainame = "PLANET" + pltype;
                        } // end else if
                        if (playershipvelocity > maxdockingvelocity)
                        {
                            message = "You must have a velocity lass than " + maxdockingvelocity + " in order to dock at a Starbase";
                            _root.func_messangercom(message, ainame, "BEGIN");
                            continue;
                        } // end if
                        if (_root.starbaselocation[i][9] > getTimer())
                        {
                            message = "You Attack me and then want to dock? Maybe, later I\'ll let you do that!";
                            _root.func_messangercom(message, ainame, "BEGIN");
                            continue;
                        } // end if
                        if (_root.mapdsiplayed == false)
                        {
                            _root.playershipstatus[4][0] = _root.starbaselocation[i][0];
                            if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
                            {
                                datatosend = "KFLAG`DROP`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + _root.errorchecknumber + "~";
                                _root.mysocket.send(datatosend);
                            } // end if
                            removeMovieClip ("_root.toprightinformation");
                            if (_root.isgameracingzone == true)
                            {
                                _root.gotoAndStop("racingzonescreen");
                                continue;
                            } // end if
                            if (_root.starbaselocation[i][0].substr(0, 2) == "SB")
                            {
                                _root.gotoAndStop("stardock");
                                continue;
                            } // end if
                            if (_root.starbaselocation[i][0].substr(0, 2) == "PL")
                            {
                                _root.gotoAndStop("planet");
                            } // end if
                        } // end if
                    } // end if
                } // end if
            } // end of for
            for (jj = 0; jj < _root.playersquadbases.length; jj++)
            {
                if (_root.playersquadbases[jj][0] == _root.playershipstatus[5][10])
                {
                    starbasex = _root.playersquadbases[jj][2];
                    starbasey = _root.playersquadbases[jj][3];
                    starbaseradius = 100;
                    relativestarbasex = starbasex - _root.shipcoordinatex;
                    relativestarbasey = starbasey - _root.shipcoordinatey;
                    this.gamebackground["playerbase" + _root.playersquadbases[jj][0]].baseidname = _root.playersquadbases[jj][0];
                    this.gamebackground["playerbase" + _root.playersquadbases[jj][0]].squadid = _root.playersquadbases[jj][0];
                    if (relativestarbasex * relativestarbasex + relativestarbasey * relativestarbasey <= starbaseradius * starbaseradius)
                    {
                        if (playershipvelocity > maxdockingvelocity)
                        {
                            ainame = "SQUADBASE";
                            message = "You must have a velocity lass than " + maxdockingvelocity + " in order to dock at this Base";
                            _root.func_messangercom(message, ainame, "BEGIN");
                            continue;
                        } // end if
                        if (_root.mapdsiplayed == false)
                        {
                            _root.gamedisplayarea.test = "DOCKING at " + _root.starbaselocation[i][0];
                            _root.squadbasedockedat = _root.playersquadbases[jj][0];
                            removeMovieClip ("_root.toprightinformation");
                            _root.gotoAndStop("squadbase");
                        } // end if
                    } // end if
                } // end if
            } // end of for
            if (_root.teamdeathmatch == true)
            {
                jj = 0;
                basenearby = false;
                while (jj < _root.teambases.length)
                {
                    starbasex = _root.teambases[jj][1];
                    starbasey = _root.teambases[jj][2];
                    starbaseradius = _root.teambases[jj][4];
                    relativestarbasex = Number(starbasex) - Number(_root.shipcoordinatex);
                    relativestarbasey = Number(starbasey) - Number(_root.shipcoordinatey);
                    if (Math.sqrt(relativestarbasex * relativestarbasex + relativestarbasey * relativestarbasey) <= starbaseradius)
                    {
                        if (jj != _root.playershipstatus[5][2])
                        {
                            ainame = "HELP";
                            message = "You can only dock  at your teams base!";
                            _root.func_messangercom(message, ainame, "BEGIN");
                        }
                        else if (playershipvelocity > maxdockingvelocity)
                        {
                            ainame = "HELP";
                            message = "You must have a velocity lass than " + maxdockingvelocity + " in order to dock at this Base";
                            _root.func_messangercom(message, ainame, "BEGIN");
                        }
                        else if (_root.mapdsiplayed == false)
                        {
                            removeMovieClip ("_root.toprightinformation");
                            _root.gotoAndStop("teambase");
                        } // end else if
                        basenearby = true;
                        break;
                    } // end if
                    ++jj;
                } // end while
            } // end if
        } // end if
        if (isshiftkeypressed == true && _root.gamechatinfo[4] == false)
        {
            playershipvelocity = playershipvelocity + playershipacceleration * currenttimechangeratio * 2;
            if (playershipvelocity >= playershipmaxvelocity)
            {
                playershipvelocity = afterburnerspeed;
            } // end if
            _root.afterburnerinuse = true;
        }
        else
        {
            _root.afterburnerinuse = false;
        } // end else if
        if (_root.afterburnerinuse == false)
        {
            if (isupkeypressed == true)
            {
                playershipvelocity = playershipvelocity + playershipacceleration * currenttimechangeratio;
            } // end if
            if (isdownkeypressed == true)
            {
                if (playershipvelocity > 0 && playershipvelocity < playershipacceleration * currenttimechangeratio)
                {
                    playershipvelocity = 0;
                } // end if
                if (playershipvelocity > 0)
                {
                    playershipvelocity = playershipvelocity - playershipacceleration * currenttimechangeratio;
                } // end if
            } // end if
            if (playershipvelocity > playershipmaxvelocity)
            {
                playershipvelocity = playershipmaxvelocity;
            } // end if
        } // end if
        playershiprotating = 0;
        if (isrightkeypressed == true)
        {
            playershiprotating = playershiprotating + playershiprotation;
        } // end if
        if (isleftkeypressed == true)
        {
            playershiprotating = playershiprotating - playershiprotation;
        } // end if
        if (playershiprotating != 0)
        {
            playershiprotating = playershiprotating - playershipvelocity / playershipmaxvelocity * playerrotationdegredation * playershiprotating;
            newshiprotation = playershipfacing + playershiprotating * currenttimechangeratio;
            if (newshiprotation < 0)
            {
                newshiprotation = newshiprotation + 360;
            } // end if
            if (newshiprotation > 360)
            {
                newshiprotation = newshiprotation - 360;
            } // end if
            ++currentrotationframe;
            if (currentrotationframe > rotationdelay)
            {
                currentrotationframe = 0;
                _root.gamedisplayarea.playership._rotation = newshiprotation;
            } // end if
            _root.playershipfacing = newshiprotation;
            relativefacing = newshiprotation;
        } // end if
    } // end if
    velocity = playershipvelocity * currenttimechangeratio;
    movementofanobjectwiththrust();
    xcoordadjustment = xmovement;
    ycoordadjustment = ymovement;
    shipcoordinatex = shipcoordinatex + xcoordadjustment;
    shipcoordinatey = shipcoordinatey + ycoordadjustment;
    if (iscontrolkeypressed == true && _root.playershipstatus[5][4] == "alive")
    {
        for (g = 0; g < _root.playershipstatus[0].length; g++)
        {
            if (_root.playershipstatus[0][g][1] <= curenttime && _root.playershipstatus[1][1] > _root.playershipstatus[0][g][5] && _root.playershipstatus[0][g][0] != "none" && _root.playershipstatus[0][g][4] == "ON")
            {
                if (_root.playershipstatus[5][15] == "C")
                {
                    turnofshipcloak();
                } // end if
                _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - _root.playershipstatus[0][g][5];
                currentnumber = _root.playershotsfired.length;
                if (currentnumber < 1)
                {
                    _root.playershotsfired = new Array();
                    currentnumber = 0;
                } // end if
                gunshottypenumber = _root.playershipstatus[0][g][0];
                gunshotvelocity = _root.playershipstatus[0][g][7];
                relativefacing = playershipfacing;
                velocity = playershipvelocity + gunshotvelocity;
                movementofanobjectwiththrust();
                xcoordadjustment = xmovement;
                ycoordadjustment = ymovement;
                if (_root.currentplayershotsfired > 999)
                {
                    _root.currentplayershotsfired = 0;
                } // end if
                i = _root.currentplayershotsfired;
                xfireposition = _root.playershipstatus[0][g][2];
                yfireposition = _root.playershipstatus[0][g][3];
                firingbulletstartlocation();
                _root.playershotsfired[currentnumber] = new Array();
                _root.playershotsfired[currentnumber][0] = i;
                _root.playershotsfired[currentnumber][1] = Math.round(xfireposition + shipcoordinatex);
                _root.playershotsfired[currentnumber][2] = Math.round(yfireposition + shipcoordinatey);
                _root.playershotsfired[currentnumber][3] = Math.round(xcoordadjustment);
                _root.playershotsfired[currentnumber][4] = Math.round(ycoordadjustment);
                _root.playershotsfired[currentnumber][5] = curenttime + _root.playershipstatus[0][g][8];
                _root.playershotsfired[currentnumber][6] = _root.playershipstatus[0][g][0];
                _root.playershotsfired[currentnumber][7] = playershipfacing;
                _root.playershotsfired[currentnumber][8] = curenttime + _root.playershipstatus[0][g][6];
                _root.gamedisplayarea.attachMovie("guntype" + gunshottypenumber + "fire", "playergunfire" + i, 5000 + i);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _rotation, playershipfacing);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _x, _root.playershotsfired[currentnumber][1] - _root.shipcoordinatex);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _y, _root.playershotsfired[currentnumber][2] - _root.shipcoordinatey);
                set("_root.gamedisplayarea.playergunfire" + i + ".xmovement", xcoordadjustment);
                set("_root.gamedisplayarea.playergunfire" + i + ".ymovement", ycoordadjustment);
                set("_root.gamedisplayarea.playergunfire" + i + ".xposition", _root.playershotsfired[currentnumber][1]);
                set("_root.gamedisplayarea.playergunfire" + i + ".yposition", _root.playershotsfired[currentnumber][2]);
                _root.playershotsfired[currentnumber][13] = "GUNS";
                if (_root.soundvolume != "off")
                {
                    _root["guntype" + gunshottypenumber + "sound"].start();
                } // end if
                _root.playershipstatus[0][g][1] = _root.playershotsfired[currentnumber][8];
                globaltimestamp = String(getTimer() + _root.clocktimediff);
                globaltimestamp = Number(globaltimestamp.substr(globaltimestamp.length - 4));
                if (globaltimestamp > 10000)
                {
                    globaltimestamp = globaltimestamp - 10000;
                } // end if
                _root.gunfireinformation = _root.gunfireinformation + ("GF`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[currentnumber][1] + "`" + _root.playershotsfired[currentnumber][2] + "`" + Math.round(velocity) + "`" + Math.round(playershipfacing) + "`" + _root.playershotsfired[currentnumber][6] + "`" + i + "`" + globaltimestamp + "~");
                _root.playershotsfired[currentnumber][11] = _root.gamedisplayarea["playergunfire" + i]._width / 2;
                _root.playershotsfired[currentnumber][12] = _root.gamedisplayarea["playergunfire" + i]._height / 2;
                ++_root.currentplayershotsfired;
            } // end if
        } // end of for
    } // end if
    if (isspacekeypressed == true && _root.playershipstatus[5][4] == "alive" && _root.gamechatinfo[4] == false)
    {
        g = _root.playershipstatus[10];
        currentmisiletype = _root.playershipstatus[7][g][0];
        if (_root.playershipstatus[7][g][1] <= curenttime && _root.playershipstatus[7][g][0] != "none" && _root.playershipstatus[7][g][4][currentmisiletype] > 0 && spacekeyjustpressed == true)
        {
            if (_root.playershipstatus[5][15] == "C")
            {
                turnofshipcloak();
            } // end if
            spacekeyjustpressed = false;
            _root.playershipstatus[7][g][1] = curenttime + _root.missile[currentmisiletype][3];
            currentnumber = _root.playershotsfired.length;
            if (currentnumber < 1)
            {
                _root.playershotsfired = new Array();
                currentnumber = 0;
            } // end if
            missileshottypenumber = currentmisiletype;
            missileshotvelocity = _root.missile[currentmisiletype][0];
            relativefacing = playershipfacing;
            velocity = missileshotvelocity + playershipvelocity;
            movementofanobjectwiththrust();
            xcoordadjustment = xmovement;
            ycoordadjustment = ymovement;
            globaltimestamp = String(getTimer() + _root.clocktimediff);
            globaltimestamp = Number(globaltimestamp.substr(globaltimestamp.length - 4));
            if (globaltimestamp > 10000)
            {
                globaltimestamp = globaltimestamp - 10000;
            } // end if
            if (_root.currentplayershotsfired > 999)
            {
                _root.currentplayershotsfired = 0;
            } // end if
            i = _root.currentplayershotsfired;
            _root.playershotsfired[currentnumber] = new Array();
            _root.playershotsfired[currentnumber][0] = i;
            xfireposition = _root.playershipstatus[7][g][2];
            yfireposition = _root.playershipstatus[7][g][3];
            missilefirestartlocation();
            if (_root.missile[currentmisiletype][8] == "SEEKER" || _root.missile[currentmisiletype][8] == "EMP" || _root.missile[currentmisiletype][8] == "DISRUPTER")
            {
                if (_root.targetinfo[0] != "None" && _root.targetinfo[0].charAt(0) != "*" && _root.targetinfo[3] < 999999)
                {
                    if (_root.soundvolume != "off")
                    {
                        _root.missilefiresound.start();
                    } // end if
                    --_root.playershipstatus[7][g][4][currentmisiletype];
                    _root.rightside.Missilebank.refreshqty();
                    if (_root.targetinfo[7] == "aiship")
                    {
                        _root.playershotsfired[currentnumber][0] = _root.currentplayershotsfired;
                        _root.playershotsfired[currentnumber][1] = Math.round(xfireposition + shipcoordinatex);
                        _root.playershotsfired[currentnumber][2] = Math.round(yfireposition + shipcoordinatey);
                        _root.playershotsfired[currentnumber][3] = Math.round(xcoordadjustment);
                        _root.playershotsfired[currentnumber][4] = Math.round(ycoordadjustment);
                        _root.playershotsfired[currentnumber][5] = curenttime + _root.missile[currentmisiletype][2];
                        _root.playershotsfired[currentnumber][6] = currentmisiletype;
                        _root.playershotsfired[currentnumber][7] = playershipfacing;
                        gunshottypenumber = currentmisiletype;
                        _root.gamedisplayarea.attachMovie("missile" + currentmisiletype + "fire", "playergunfire" + i, 5000 + i);
                        setProperty("_root.gamedisplayarea.playergunfire" + i, _rotation, playershipfacing);
                        setProperty("_root.gamedisplayarea.playergunfire" + i, _x, _root.playershotsfired[currentnumber][1] - shipcoordinatex);
                        setProperty("_root.gamedisplayarea.playergunfire" + i, _y, _root.playershotsfired[currentnumber][2] - shipcoordinatey);
                        set("_root.gamedisplayarea.playergunfire" + i + ".xmovement", xcoordadjustment);
                        set("_root.gamedisplayarea.playergunfire" + i + ".ymovement", ycoordadjustment);
                        set("_root.gamedisplayarea.playergunfire" + i + ".xposition", _root.playershotsfired[currentnumber][1]);
                        set("_root.gamedisplayarea.playergunfire" + i + ".yposition", _root.playershotsfired[currentnumber][2]);
                        set("_root.gamedisplayarea.playergunfire" + i + ".missiletype", currentmisiletype);
                        set("_root.gamedisplayarea.playergunfire" + i + ".aitargetnumber", _root.targetinfo[8]);
                        set("_root.gamedisplayarea.playergunfire" + i + ".velocity", velocity);
                        set("_root.gamedisplayarea.playergunfire" + i + ".seeking", "AI");
                    }
                    else if (_root.targetinfo[7] == "otherplayership")
                    {
                        _root.playershotsfired[currentnumber][0] = _root.currentplayershotsfired;
                        _root.playershotsfired[currentnumber][1] = Math.round(xfireposition + shipcoordinatex);
                        _root.playershotsfired[currentnumber][2] = Math.round(yfireposition + shipcoordinatey);
                        _root.playershotsfired[currentnumber][3] = Math.round(xcoordadjustment);
                        _root.playershotsfired[currentnumber][4] = Math.round(ycoordadjustment);
                        _root.playershotsfired[currentnumber][5] = curenttime + _root.missile[currentmisiletype][2];
                        _root.playershotsfired[currentnumber][6] = currentmisiletype;
                        _root.playershotsfired[currentnumber][7] = playershipfacing;
                        gunshottypenumber = currentmisiletype;
                        _root.gunfireinformation = _root.gunfireinformation + ("MF`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[currentnumber][1] + "`" + _root.playershotsfired[currentnumber][2] + "`" + Math.round(velocity) + "`" + playershipfacing + "`" + currentmisiletype + "`" + i + "`" + globaltimestamp + "`" + _root.otherplayership[_root.targetinfo[8]][0] + "`" + "~");
                        missilename = _root.playershipstatus[3][0] + "n" + i;
                        _root.gamedisplayarea.attachMovie("missile" + _root.playershotsfired[currentnumber][6] + "fire", "othermissilefire" + missilename, 998000 + i);
                        setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _rotation, playershipfacing);
                        setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _x, _root.playershotsfired[currentnumber][1] - _root.shipcoordinatex);
                        setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _y, _root.playershotsfired[currentnumber][2] - _root.shipcoordinatey);
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".xposition", _root.playershotsfired[currentnumber][1]);
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".yposition", _root.playershotsfired[currentnumber][2]);
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".missiletype", currentmisiletype);
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".velocity", Math.round(velocity));
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".seeking", "OTHER");
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".rotation", playershipfacing);
                    } // end else if
                    _root.playershotsfired[currentnumber][13] = "MISSILE";
                    _root.playershotsfired[currentnumber][11] = _root.gamedisplayarea["playergunfire" + i]._width / 2;
                    _root.playershotsfired[currentnumber][12] = _root.gamedisplayarea["playergunfire" + i]._height / 2;
                    _root.playershipstatus[7][g][1] = curenttime + _root.missile[currentmisiletype][3];
                } // end if
            }
            else if (_root.missile[currentmisiletype][8] != "SEEKER")
            {
                if (_root.soundvolume != "off")
                {
                    _root.missilefiresound.start();
                } // end if
                --_root.playershipstatus[7][g][4][currentmisiletype];
                _root.rightside.Missilebank.refreshqty();
                _root.playershotsfired[currentnumber][0] = i;
                _root.playershotsfired[currentnumber][1] = Math.round(xfireposition + shipcoordinatex);
                _root.playershotsfired[currentnumber][2] = Math.round(yfireposition + shipcoordinatey);
                _root.playershotsfired[currentnumber][3] = Math.round(xcoordadjustment);
                _root.playershotsfired[currentnumber][4] = Math.round(ycoordadjustment);
                _root.playershotsfired[currentnumber][5] = curenttime + _root.missile[currentmisiletype][2];
                _root.playershotsfired[currentnumber][6] = currentmisiletype;
                _root.playershotsfired[currentnumber][7] = playershipfacing;
                gunshottypenumber = currentmisiletype;
                _root.gamedisplayarea.attachMovie("missile" + currentmisiletype + "fire", "playergunfire" + i, 5000 + i);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _rotation, playershipfacing);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _x, _root.playershotsfired[currentnumber][1] - shipcoordinatex);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _y, _root.playershotsfired[currentnumber][2] - shipcoordinatey);
                set("_root.gamedisplayarea.playergunfire" + i + ".xmovement", xcoordadjustment);
                set("_root.gamedisplayarea.playergunfire" + i + ".ymovement", ycoordadjustment);
                set("_root.gamedisplayarea.playergunfire" + i + ".xposition", _root.playershotsfired[currentnumber][1]);
                set("_root.gamedisplayarea.playergunfire" + i + ".yposition", _root.playershotsfired[currentnumber][2]);
                _root.playershotsfired[currentnumber][13] = "MISSILE";
                _root.playershotsfired[currentnumber][11] = _root.gamedisplayarea["playergunfire" + i]._width / 2;
                _root.playershotsfired[currentnumber][12] = _root.gamedisplayarea["playergunfire" + i]._height / 2;
                _root.playershipstatus[7][g][1] = curenttime + _root.missile[currentmisiletype][3];
                _root.gunfireinformation = _root.gunfireinformation + ("MF`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[currentnumber][1] + "`" + _root.playershotsfired[currentnumber][2] + "`" + Math.round(velocity) + "`" + playershipfacing + "`" + _root.playershotsfired[currentnumber][6] + "`" + i + "`" + globaltimestamp + "~");
            } // end else if
            ++_root.currentplayershotsfired;
        } // end if
    } // end if
    gunfiremovement();
    findoutcurrentsector();
    _root.playershipvelocity = this.playershipvelocity;
    _root.playershipmaxvelocity = this.playershipmaxvelocity;
    _root.playershipacceleration = _root.playershipacceleration;
    _root.shipcoordinatex = this.shipcoordinatex;
    _root.shipcoordinatey = this.shipcoordinatey;
}

// [onClipEvent of sprite 1406 in frame 1]
onClipEvent (load)
{
    this._visible = false;
    pathtosectorfile = "./systems/system100/";
    currenttime = 9000;
    checkrateai = 10000;
    checkrateaicheck = 0;
    _root.currentailvl = 0;
    timeouttimecheck = 0;
    timeoutinterval = 15000;
}

// [Action in Frame 1]
function func_adddamages()
{
    if (currentsparks < maxsparks)
    {
        ++currentsparks;
        this.playership.attachMovie("shipsparks", "shipsparks" + currentsparks, 66 + currentsparks);
        this.playership["shipsparks" + currentsparks]._x = Math.round(Math.random() * 20 - 10);
        this.playership["shipsparks" + currentsparks]._y = Math.round(Math.random() * 20 - 10);
    } // end if
    if (maxstructure / 2 > _root.playershipstatus[2][5])
    {
        if (Math.random() < 0.250000)
        {
            _root.rightside.radarscreen.radarscreenarea.func_damagetheradar();
            if (currentsparks < 4)
            {
                ++currentsparks;
                this.playership.attachMovie("shipsparks", "shipsparks" + currentsparks, 66 + currentsparks);
                this.playership["shipsparks" + currentsparks]._x = Math.round(Math.random() * 20 - 10);
                this.playership["shipsparks" + currentsparks]._y = Math.round(Math.random() * 20 - 10);
            } // end if
        } // end if
    } // end if
} // End of the function
function func_removedamages()
{
    currentsparks = 0;
    _root.rightside.radarscreen.radarscreenarea.func_repairtheradar();
    for (j = 0; j < maxsparks + 5; j++)
    {
        this.playership.createEmptyMovieClip("shipsparks" + j, 66 + j);
    } // end of for
} // End of the function
function adddamage(damagetoadd, damagefrom)
{
    if (_root.playershipstatus[5][20] != true)
    {
        _root.playershipstatus[2][1] = _root.playershipstatus[2][1] - damagetoadd;
        if (_root.playershipstatus[2][1] < 0)
        {
            _root.playershipstatus[2][5] = _root.playershipstatus[2][5] + _root.playershipstatus[2][1];
            _root.playershipstatus[2][1] = 0;
            _root.gunfireinformation = _root.gunfireinformation + ("GH`" + _root.playershipstatus[3][0] + "`" + "`" + "`ST`" + Math.round(_root.playershipstatus[2][5]) + "~");
        } // end if
        if (_root.playershipstatus[2][5] <= 0 && _root.playershipstatus[5][4] == "alive")
        {
            _root.playershipstatus[5][4] = "dead";
            _root.playershipstatus[5][5] = damagefrom;
            zzj = 0;
            dataupdated = false;
            while (zzj < _root.currentonlineplayers.length && dataupdated == false)
            {
                if (playerwhoshothitid == _root.currentonlineplayers[zzj][0])
                {
                    dataupdated = true;
                    _root.playershipstatus[5][7] = _root.currentonlineplayers[zzj][1];
                } // end if
                ++zzj;
            } // end while
            _root.playershipstatus[2][5] = 0;
            _root.gotoAndPlay("playerdeath");
        }
        else
        {
            this.playership.attachMovie("shiptype" + _root.playershipstatus[5][0] + "shield", "shipshield", 1);
        } // end if
    } // end else if
} // End of the function
function myhittest(firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    if (secitemx - sechalfwidth > firsthalfwidth)
    {
        return (false);
    }
    else if (secitemx + sechalfwidth < -firsthalfwidth)
    {
        return (false);
    }
    else if (secitemy - sechalfheight > firsthalfheight)
    {
        return (false);
    }
    else if (secitemy + sechalfheight < -firsthalfheight)
    {
        return (false);
    }
    else
    {
        return (true);
    } // end else if
} // End of the function
function othergunfiremovement()
{
    curenttime = getTimer();
    cc = 0;
    if (_root.othergunfire.length > 0)
    {
        playersx = _root.shipcoordinatex;
        playersy = _root.shipcoordinatey;
        halfplayerswidth = this.playership._width / 2;
        halfplayersheight = this.playership._height / 2;
    } // end if
    for (cc = 0; cc < _root.othergunfire.length; cc++)
    {
        if (_root.othergunfire[cc][5] < curenttime)
        {
            removeMovieClip ("othergunfire" + _root.othergunfire[cc][8]);
            _root.othergunfire.splice(cc, 1);
            --cc;
            continue;
        } // end if
        if (myhittest(halfplayerswidth, halfplayersheight, this["othergunfire" + _root.othergunfire[cc][8]]._x, this["othergunfire" + _root.othergunfire[cc][8]]._y, _root.othergunfire[cc][11], _root.othergunfire[cc][12]) && _root.playershipstatus[5][4] == "alive")
        {
            removeMovieClip ("othergunfire" + _root.othergunfire[cc][8]);
            playerwhoshothitid = _root.othergunfire[cc][0];
            gunshothitplayertype = _root.othergunfire[cc][7];
            if (_root.playershipstatus[5][20] != true)
            {
                _root.playershipstatus[2][1] = _root.playershipstatus[2][1] - _root.guntype[gunshothitplayertype][4];
            } // end if
            if (_root.playershipstatus[2][1] < 0)
            {
                func_adddamages();
                _root.playershipstatus[2][5] = _root.playershipstatus[2][5] + _root.playershipstatus[2][1];
                _root.playershipstatus[2][1] = 0;
                _root.gunfireinformation = _root.gunfireinformation + ("GH`" + _root.playershipstatus[3][0] + "`" + _root.othergunfire[cc][0] + "`" + _root.othergunfire[cc][9] + "`ST`" + Math.round(_root.playershipstatus[2][5]) + "~");
            }
            else
            {
                _root.gunfireinformation = _root.gunfireinformation + ("GH`" + _root.playershipstatus[3][0] + "`" + _root.othergunfire[cc][0] + "`" + _root.othergunfire[cc][9] + "`SH`" + Math.round(_root.playershipstatus[2][1]) + "~");
            } // end else if
            _root.othergunfire.splice(cc, 1);
            if (_root.playershipstatus[2][5] <= 0 && _root.playershipstatus[5][4] == "alive")
            {
                _root.playershipstatus[5][4] = "dead";
                _root.playershipstatus[5][5] = playerwhoshothitid;
                zzj = 0;
                dataupdated = false;
                while (zzj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (playerwhoshothitid == _root.currentonlineplayers[zzj][0])
                    {
                        dataupdated = true;
                        _root.playershipstatus[5][7] = _root.currentonlineplayers[zzj][1];
                    } // end if
                    ++zzj;
                } // end while
                _root.playershipstatus[2][5] = 0;
                _root.gotoAndPlay("playerdeath");
            }
            else
            {
                this.playership.attachMovie("shiptype" + _root.playershipstatus[5][0] + "shield", "shipshield", 1);
            } // end else if
            --cc;
        } // end if
    } // end of for
} // End of the function
function othermissilefiremovement()
{
    curenttime = getTimer();
    cc = 0;
    if (_root.othermissilefire.length > 0)
    {
        playersx = _root.shipcoordinatex;
        playersy = _root.shipcoordinatey;
        halfplayerswidth = this.playership._width / 2;
        halfplayersheight = this.playership._height / 2;
    } // end if
    for (cc = 0; cc < _root.othermissilefire.length; cc++)
    {
        if (_root.othermissilefire[cc][5] < curenttime)
        {
            removeMovieClip ("othermissilefire" + _root.othermissilefire[cc][8]);
            _root.othermissilefire.splice(cc, 1);
            --cc;
            continue;
        } // end if
        if (myhittest(halfplayerswidth, halfplayersheight, this["othermissilefire" + _root.othermissilefire[cc][8]]._x, this["othermissilefire" + _root.othermissilefire[cc][8]]._y, _root.othermissilefire[cc][11], _root.othermissilefire[cc][12]) && _root.playershipstatus[5][4] == "alive")
        {
            removeMovieClip ("othermissilefire" + _root.othermissilefire[cc][8]);
            playerwhoshothitid = _root.othermissilefire[cc][0];
            gunshothitplayertype = _root.othermissilefire[cc][7];
            if (_root.playershipstatus[5][20] != true)
            {
                if (_root.missile[gunshothitplayertype][8] == "EMP")
                {
                    empendsat = getTimer() + _root.missile[gunshothitplayertype][9];
                    if (_root.playerempend < empendsat)
                    {
                        _root.playerempend = empendsat;
                        _root.isplayeremp = true;
                    } // end if
                } // end if
                if (_root.missile[gunshothitplayertype][8] == "DISRUPTER")
                {
                    disruptendsat = getTimer() + _root.missile[gunshothitplayertype][9];
                    if (_root.playerdiruptend < disruptendsat)
                    {
                        _root.gamedisplayarea.keyboardscript.disruptplayersengines();
                        _root.playerdiruptend = disruptendsat;
                        _root.isplayerdisrupt = true;
                    } // end if
                } // end if
                _root.playershipstatus[2][1] = _root.playershipstatus[2][1] - _root.missile[gunshothitplayertype][4];
            } // end if
            if (_root.playershipstatus[2][1] < 0)
            {
                func_adddamages();
                _root.playershipstatus[2][5] = _root.playershipstatus[2][5] + _root.playershipstatus[2][1];
                _root.playershipstatus[2][1] = 0;
                _root.gunfireinformation = _root.gunfireinformation + ("MH`" + _root.playershipstatus[3][0] + "`" + _root.othermissilefire[cc][0] + "`" + _root.othermissilefire[cc][9] + "`ST`" + Math.round(_root.playershipstatus[2][5]) + "~");
            }
            else
            {
                _root.gunfireinformation = _root.gunfireinformation + ("MH`" + _root.playershipstatus[3][0] + "`" + _root.othermissilefire[cc][0] + "`" + _root.othermissilefire[cc][9] + "`SH`" + Math.round(_root.playershipstatus[2][1]) + "~");
            } // end else if
            _root.othermissilefire.splice(cc, 1);
            if (_root.playershipstatus[2][5] <= 0 && _root.playershipstatus[5][4] == "alive")
            {
                _root.playershipstatus[5][4] = "dead";
                _root.playershipstatus[5][5] = playerwhoshothitid;
                zzj = 0;
                dataupdated = false;
                while (zzj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (playerwhoshothitid == _root.currentonlineplayers[zzj][0])
                    {
                        dataupdated = true;
                        _root.playershipstatus[5][7] = _root.currentonlineplayers[zzj][1];
                    } // end if
                    ++zzj;
                } // end while
                _root.playershipstatus[2][5] = 0;
                _root.gotoAndPlay("playerdeath");
            }
            else
            {
                this.playership.attachMovie("shiptype" + _root.playershipstatus[5][0] + "shield", "shipshield", 1);
            } // end else if
            --cc;
        } // end if
    } // end of for
} // End of the function
currentsparks = 0;
maxstructure = _root.shiptype[_root.playershipstatus[5][0]][3][3];
maxsparks = 4;

function movementofanobjectwiththrust()
{
    this.relativefacing = Math.round(this.relativefacing);
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    }
    else if (velocity != 0)
    {
        this.relativefacing = Math.round(this.relativefacing);
        ymovement = -velocity * _root.cosines[this.relativefacing];
        xmovement = velocity * _root.sines[this.relativefacing];
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function checkothershipspositions()
{
    currenttime = getTimer();
    for (currentothership = 0; currentothership < _root.otherplayership.length; currentothership++)
    {
        for (currentloc = 0; currentloc < _root.otherplayership[currentothership][30].length; currentloc++)
        {
            if (_root.otherplayership[currentothership][30][currentloc][9] < currenttime)
            {
                _root.otherplayership[currentothership][1] = _root.otherplayership[currentothership][30][currentloc][0];
                _root.otherplayership[currentothership][2] = _root.otherplayership[currentothership][30][currentloc][1];
                _root.otherplayership[currentothership][3] = _root.otherplayership[currentothership][30][currentloc][2];
                _root.otherplayership[currentothership][4] = _root.otherplayership[currentothership][30][currentloc][3];
                _root.otherplayership[currentothership][5] = new Array();
                _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][30][currentloc][6][0];
                _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][30][currentloc][6][1];
                _root.otherplayership[currentothership][30].splice(currentloc, 1);
                --currentloc;
                continue;
            } // end if
            break;
        } // end of for
    } // end of for
} // End of the function
function movetheotherships(timeelapsed)
{
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    for (zz = 0; zz < _root.otherplayership.length; zz++)
    {
        if (_root.otherplayership[zz][5][0] == "S")
        {
            velocity = _root.otherplayership[zz][4] = _root.otherplayership[zz][4] + _root.shiptype[_root.otherplayership[zz][11]][3][0] * timeelapsed * 2;
            if (velocity > _root.shiptype[_root.otherplayership[zz][11]][3][6])
            {
                velocity = _root.otherplayership[zz][4] = _root.shiptype[_root.otherplayership[zz][11]][3][6];
            } // end if
        }
        else
        {
            _root.otherplayership[zz][4] = _root.otherplayership[zz][4] + _root.otherplayership[zz][5][0] * timeelapsed;
            velocity = _root.otherplayership[zz][4];
            if (velocity > _root.shiptype[_root.otherplayership[zz][11]][3][1])
            {
                velocity = _root.shiptype[_root.otherplayership[zz][11]][3][1];
                _root.otherplayership[zz][4] = velocity;
            } // end if
            if (velocity < 0)
            {
                velocity = 0;
                _root.otherplayership[zz][4] = 0;
            } // end if
        } // end else if
        otherplayershiprotating = _root.otherplayership[zz][5][1] * timeelapsed;
        otherplayershiprotating = otherplayershiprotating - velocity / _root.shiptype[_root.otherplayership[zz][11]][3][1] * _root.shiptype[_root.otherplayership[zz][11]][3][5] * otherplayershiprotating;
        _root.otherplayership[zz][3] = _root.otherplayership[zz][3] + otherplayershiprotating;
        if (_root.otherplayership[zz][3] > 360)
        {
            _root.otherplayership[zz][3] = _root.otherplayership[zz][3] - 360;
        } // end if
        if (_root.otherplayership[zz][3] < 0)
        {
            _root.otherplayership[zz][3] = _root.otherplayership[zz][3] + 360;
        } // end if
        relativefacing = _root.otherplayership[zz][3];
        velocity = velocity * timeelapsed;
        movementofanobjectwiththrust();
        _root.otherplayership[zz][1] = _root.otherplayership[zz][1] + xmovement;
        _root.otherplayership[zz][2] = _root.otherplayership[zz][2] + ymovement;
        this["otherplayership" + _root.otherplayership[zz][0]]._x = _root.otherplayership[zz][1] - playersxcoord;
        this["otherplayership" + _root.otherplayership[zz][0]]._y = _root.otherplayership[zz][2] - playersycoord;
        this["otherplayership" + _root.otherplayership[zz][0]]._rotation = Math.round(_root.otherplayership[zz][3]);
    } // end of for
} // End of the function
maxotherplayershipsrefreshinterval = 50;
lastotherplayershipsrefresh = getTimer();
hittesttime = getTimer();
hittestinterval = 60;
missilehittesttime = getTimer();
missilehittestinterval = 110;
this.onEnterFrame = function ()
{
    currenttime = getTimer();
    if (lastotherplayershipsrefresh < currenttime)
    {
        currentothershipstime = currenttime;
        lastotherplayershipsrefresh = currentothershipstime + maxotherplayershipsrefreshinterval;
        timeelapsed = (currentothershipstime - othershipslasttime) / 1000;
        othershipslasttime = currentothershipstime;
        checkothershipspositions();
        movetheotherships(timeelapsed);
    } // end if
    if (hittesttime < currenttime)
    {
        othergunfiremovement();
        hittesttime = currenttime + hittestinterval;
    } // end if
    if (missilehittesttime < currenttime)
    {
        othermissilefiremovement();
        missilehittesttime = currenttime + missilehittestinterval;
    } // end if
};
stop ();

stop ();
