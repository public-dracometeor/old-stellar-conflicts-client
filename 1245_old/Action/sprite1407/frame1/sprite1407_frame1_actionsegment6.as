﻿// Action script...

function movementofanobjectwiththrust()
{
    this.relativefacing = Math.round(this.relativefacing);
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    }
    else if (velocity != 0)
    {
        this.relativefacing = Math.round(this.relativefacing);
        ymovement = -velocity * _root.cosines[this.relativefacing];
        xmovement = velocity * _root.sines[this.relativefacing];
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function checkothershipspositions()
{
    currenttime = getTimer();
    for (currentothership = 0; currentothership < _root.otherplayership.length; currentothership++)
    {
        for (currentloc = 0; currentloc < _root.otherplayership[currentothership][30].length; currentloc++)
        {
            if (_root.otherplayership[currentothership][30][currentloc][9] < currenttime)
            {
                _root.otherplayership[currentothership][1] = _root.otherplayership[currentothership][30][currentloc][0];
                _root.otherplayership[currentothership][2] = _root.otherplayership[currentothership][30][currentloc][1];
                _root.otherplayership[currentothership][3] = _root.otherplayership[currentothership][30][currentloc][2];
                _root.otherplayership[currentothership][4] = _root.otherplayership[currentothership][30][currentloc][3];
                _root.otherplayership[currentothership][5] = new Array();
                _root.otherplayership[currentothership][5][0] = _root.otherplayership[currentothership][30][currentloc][6][0];
                _root.otherplayership[currentothership][5][1] = _root.otherplayership[currentothership][30][currentloc][6][1];
                _root.otherplayership[currentothership][30].splice(currentloc, 1);
                --currentloc;
                continue;
            } // end if
            break;
        } // end of for
    } // end of for
} // End of the function
function movetheotherships(timeelapsed)
{
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    for (zz = 0; zz < _root.otherplayership.length; zz++)
    {
        if (_root.otherplayership[zz][5][0] == "S")
        {
            velocity = _root.otherplayership[zz][4] = _root.otherplayership[zz][4] + _root.shiptype[_root.otherplayership[zz][11]][3][0] * timeelapsed * 2;
            if (velocity > _root.shiptype[_root.otherplayership[zz][11]][3][6])
            {
                velocity = _root.otherplayership[zz][4] = _root.shiptype[_root.otherplayership[zz][11]][3][6];
            } // end if
        }
        else
        {
            _root.otherplayership[zz][4] = _root.otherplayership[zz][4] + _root.otherplayership[zz][5][0] * timeelapsed;
            velocity = _root.otherplayership[zz][4];
            if (velocity > _root.shiptype[_root.otherplayership[zz][11]][3][1])
            {
                velocity = _root.shiptype[_root.otherplayership[zz][11]][3][1];
                _root.otherplayership[zz][4] = velocity;
            } // end if
            if (velocity < 0)
            {
                velocity = 0;
                _root.otherplayership[zz][4] = 0;
            } // end if
        } // end else if
        otherplayershiprotating = _root.otherplayership[zz][5][1] * timeelapsed;
        otherplayershiprotating = otherplayershiprotating - velocity / _root.shiptype[_root.otherplayership[zz][11]][3][1] * _root.shiptype[_root.otherplayership[zz][11]][3][5] * otherplayershiprotating;
        _root.otherplayership[zz][3] = _root.otherplayership[zz][3] + otherplayershiprotating;
        if (_root.otherplayership[zz][3] > 360)
        {
            _root.otherplayership[zz][3] = _root.otherplayership[zz][3] - 360;
        } // end if
        if (_root.otherplayership[zz][3] < 0)
        {
            _root.otherplayership[zz][3] = _root.otherplayership[zz][3] + 360;
        } // end if
        relativefacing = _root.otherplayership[zz][3];
        velocity = velocity * timeelapsed;
        movementofanobjectwiththrust();
        _root.otherplayership[zz][1] = _root.otherplayership[zz][1] + xmovement;
        _root.otherplayership[zz][2] = _root.otherplayership[zz][2] + ymovement;
        this["otherplayership" + _root.otherplayership[zz][0]]._x = _root.otherplayership[zz][1] - playersxcoord;
        this["otherplayership" + _root.otherplayership[zz][0]]._y = _root.otherplayership[zz][2] - playersycoord;
        this["otherplayership" + _root.otherplayership[zz][0]]._rotation = Math.round(_root.otherplayership[zz][3]);
    } // end of for
} // End of the function
maxotherplayershipsrefreshinterval = 50;
lastotherplayershipsrefresh = getTimer();
hittesttime = getTimer();
hittestinterval = 60;
missilehittesttime = getTimer();
missilehittestinterval = 110;
this.onEnterFrame = function ()
{
    currenttime = getTimer();
    if (lastotherplayershipsrefresh < currenttime)
    {
        currentothershipstime = currenttime;
        lastotherplayershipsrefresh = currentothershipstime + maxotherplayershipsrefreshinterval;
        timeelapsed = (currentothershipstime - othershipslasttime) / 1000;
        othershipslasttime = currentothershipstime;
        checkothershipspositions();
        movetheotherships(timeelapsed);
    } // end if
    if (hittesttime < currenttime)
    {
        othergunfiremovement();
        hittesttime = currenttime + hittestinterval;
    } // end if
    if (missilehittesttime < currenttime)
    {
        othermissilefiremovement();
        missilehittesttime = currenttime + missilehittestinterval;
    } // end if
};
stop ();
