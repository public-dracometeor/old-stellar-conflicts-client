﻿// Action script...

// [onClipEvent of sprite 1405 in frame 1]
onClipEvent (load)
{
    Key.removeListener(keyListener);
    keyListener = new Object();
    keyListener.onKeyUp = function ()
    {
        keyreleased = Key.getCode();
        lastkeypressed = null;
        if (keyreleased == "38")
        {
            if (isupkeypressed == true)
            {
                isupkeypressed = false;
                _root.keywaspressed = true;
            } // end if
        } // end if
        if (keyreleased == "40")
        {
            if (isdownkeypressed == true)
            {
                isdownkeypressed = false;
                _root.keywaspressed = true;
            } // end if
        } // end if
        if (keyreleased == "37")
        {
            if (isleftkeypressed == true)
            {
                isleftkeypressed = false;
                _root.keywaspressed = true;
            } // end if
        } // end if
        if (keyreleased == "39")
        {
            if (isrightkeypressed == true)
            {
                isrightkeypressed = false;
                _root.keywaspressed = true;
            } // end if
        } // end if
        if (keyreleased == "17")
        {
            if (iscontrolkeypressed == true)
            {
                iscontrolkeypressed = false;
            } // end if
        } // end if
        if (keyreleased == "68")
        {
            if (isdkeypressed == true)
            {
                isdkeypressed = false;
            } // end if
        } // end if
        if (keyreleased == "16")
        {
            if (isshiftkeypressed == true)
            {
                isshiftkeypressed = false;
            } // end if
        } // end if
        if (keyreleased == "32")
        {
            if (isspacekeypressed == true)
            {
                isspacekeypressed = false;
                spacekeyjustpressed = true;
            } // end if
        } // end if
    };
    keyListener.onKeyDown = function ()
    {
        keypressed = Key.getCode();
        if (lastkeypressed != keypressed)
        {
            lastkeypressed = keypressed;
            if (_root.isplayerdisrupt)
            {
            }
            else if (keypressed == "38")
            {
                if (isupkeypressed == false)
                {
                    isupkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "40")
            {
                if (isdownkeypressed == false)
                {
                    isdownkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "16")
            {
                if (isshiftkeypressed == false)
                {
                    isshiftkeypressed = true;
                } // end else if
            } // end else if
            if (keypressed == "37")
            {
                if (isleftkeypressed == false)
                {
                    isleftkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "39")
            {
                if (isrightkeypressed == false)
                {
                    isrightkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "17")
            {
                if (iscontrolkeypressed == false)
                {
                    iscontrolkeypressed = true;
                    _root.keywaspressed = true;
                } // end if
            }
            else if (keypressed == "88")
            {
                if (_root.gamechatinfo[4] == false)
                {
                    runspecialone(2);
                } // end if
            }
            else if (keypressed == "90")
            {
                if (_root.gamechatinfo[4] == false)
                {
                    runspecialone(1);
                } // end if
            }
            else if (keypressed == "83")
            {
                if (_root.gamechatinfo[4] == false)
                {
                    runspecialone(3);
                } // end if
            }
            else if (keypressed == "68")
            {
                if (isdkeypressed == false)
                {
                    isdkeypressed = true;
                } // end if
            }
            else if (keypressed == "74")
            {
                if (_root.gamechatinfo[4] == false)
                {
                    func_checkforjumping();
                } // end if
            }
            else if (keypressed == "32")
            {
                if (isspacekeypressed == false)
                {
                    isspacekeypressed = true;
                } // end if
            }
            else if (keypressed == "123")
            {
                if (currenthelpframedisplayed == 0)
                {
                    _root.attachMovie("ingamehelp", "ingamehelp", 9999999);
                    _root.ingamehelp.helpmode = "ingame";
                    currenthelpframedisplayed = 1;
                }
                else if (currenthelpframedisplayed == 1)
                {
                    currenthelpframedisplayed = 0;
                    _root.ingamehelp.gotoAndPlay("close");
                } // end else if
            } // end else if
        } // end else if
    };
    Key.addListener(keyListener);
}
