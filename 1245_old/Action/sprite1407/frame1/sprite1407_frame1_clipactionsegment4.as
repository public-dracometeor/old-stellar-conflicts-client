﻿// Action script...

// [onClipEvent of sprite 1405 in frame 1]
onClipEvent (load)
{
    function movementofanobjectwiththrust()
    {
        if (relativefacing == 0)
        {
            ymovement = -velocity;
            xmovement = 0;
        } // end if
        if (velocity != 0)
        {
            this.relativefacing = Math.round(this.relativefacing);
            ymovement = -velocity * _root.cosines[this.relativefacing];
            xmovement = velocity * _root.sines[this.relativefacing];
        }
        else
        {
            ymovement = 0;
            xmovement = 0;
        } // end else if
    } // End of the function
    function disruptplayersengines()
    {
        isupkeypressed = false;
        isdownkeypressed = true;
        isshiftkeypressed = false;
        _root.keywaspressed = true;
    } // End of the function
    firstenter = true;
    _root.playershipstatus[5][20] = true;
    _root.playershipstatus[5][21] = getTimer() + 7000;
    _root.playershipstatus[6][0] = Math.ceil(shipcoordinatex / xwidthofasector);
    _root.playershipstatus[6][1] = Math.ceil(shipcoordinatey / ywidthofasector);
    xsector = _root.playershipstatus[6][0];
    ysector = _root.playershipstatus[6][1];
    playersquadbasesdisplay(xsector, ysector);
    lastsector = "";
    this.playershipmaxvelocity = _root.playershipmaxvelocity;
    this.playershipacceleration = _root.playershipacceleration;
    this.playershipfacing = _root.playershipfacing;
    relativefacing = this.playershipfacing;
    _root.gamedisplayarea.playership._rotation = _root.playershipfacing;
    timetillrotationadjust = getTimer();
    rotationadjusttime = 500;
    lastrotationtype = null;
    playersshiptype = _root.playershipstatus[5][0];
    this.playershiprotation = _root.shiptype[playersshiptype][3][2];
    this.afterburnerspeed = _root.shiptype[playersshiptype][3][6];
    _root.afterburnerinuse = false;
    playerrotationdegredation = _root.shiptype[playersshiptype][3][5];
    this._visible = false;
    secondlastturning = 0;
    lastturning = 0;
    guntype0sound = new Sound();
    guntype0sound.attachSound("guntype0sound");
    guntype1sound = new Sound();
    guntype1sound.attachSound("guntype1sound");
    guntype2sound = new Sound();
    guntype2sound.attachSound("guntype2sound");
    guntype3sound = new Sound();
    guntype3sound.attachSound("guntype3sound");
    this.framespersecond = _root.framespersecond;
    this.gameareawidth = _root.gameareawidth;
    this.gameareaheight = _root.gameareaheight;
    this.totalstars = _root.totalstars;
    this.halfgameareawidth = Math.round(gameareawidth / 2);
    this.halfgameareaheight = Math.round(gameareaheight / 2);
    xwidthofasector = _root.sectorinformation[1][0];
    ywidthofasector = _root.sectorinformation[1][1];
    isupkeypressed = false;
    isdownkeypressed = false;
    isleftkeypressed = false;
    isrightkeypressed = false;
    iscontrolkeypressed = false;
    isdkeypressed = false;
    isshiftkeypressed = false;
    isspacekeypressed = false;
    _root.currentplayershotsfired = 0;
    lasttimerotationchanged = 0;
    rotationbuffertime = 100;
    currentturning = 0;
    currenthelpframedisplayed = 0;
    maxdockingvelocity = 20;
    currentframeforcalc = 0;
    frameintforcalc = 25;
    rotationdelay = 1;
    currentrotationframe = 0;
    currenttimechangeratio = 0;
    curenttime = getTimer();
    lasttime = curenttime;
}
