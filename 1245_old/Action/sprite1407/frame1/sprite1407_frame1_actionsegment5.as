﻿// Action script...

function func_adddamages()
{
    if (currentsparks < maxsparks)
    {
        ++currentsparks;
        this.playership.attachMovie("shipsparks", "shipsparks" + currentsparks, 66 + currentsparks);
        this.playership["shipsparks" + currentsparks]._x = Math.round(Math.random() * 20 - 10);
        this.playership["shipsparks" + currentsparks]._y = Math.round(Math.random() * 20 - 10);
    } // end if
    if (maxstructure / 2 > _root.playershipstatus[2][5])
    {
        if (Math.random() < 0.250000)
        {
            _root.rightside.radarscreen.radarscreenarea.func_damagetheradar();
            if (currentsparks < 4)
            {
                ++currentsparks;
                this.playership.attachMovie("shipsparks", "shipsparks" + currentsparks, 66 + currentsparks);
                this.playership["shipsparks" + currentsparks]._x = Math.round(Math.random() * 20 - 10);
                this.playership["shipsparks" + currentsparks]._y = Math.round(Math.random() * 20 - 10);
            } // end if
        } // end if
    } // end if
} // End of the function
function func_removedamages()
{
    currentsparks = 0;
    _root.rightside.radarscreen.radarscreenarea.func_repairtheradar();
    for (j = 0; j < maxsparks + 5; j++)
    {
        this.playership.createEmptyMovieClip("shipsparks" + j, 66 + j);
    } // end of for
} // End of the function
function adddamage(damagetoadd, damagefrom)
{
    if (_root.playershipstatus[5][20] != true)
    {
        _root.playershipstatus[2][1] = _root.playershipstatus[2][1] - damagetoadd;
        if (_root.playershipstatus[2][1] < 0)
        {
            _root.playershipstatus[2][5] = _root.playershipstatus[2][5] + _root.playershipstatus[2][1];
            _root.playershipstatus[2][1] = 0;
            _root.gunfireinformation = _root.gunfireinformation + ("GH`" + _root.playershipstatus[3][0] + "`" + "`" + "`ST`" + Math.round(_root.playershipstatus[2][5]) + "~");
        } // end if
        if (_root.playershipstatus[2][5] <= 0 && _root.playershipstatus[5][4] == "alive")
        {
            _root.playershipstatus[5][4] = "dead";
            _root.playershipstatus[5][5] = damagefrom;
            zzj = 0;
            dataupdated = false;
            while (zzj < _root.currentonlineplayers.length && dataupdated == false)
            {
                if (playerwhoshothitid == _root.currentonlineplayers[zzj][0])
                {
                    dataupdated = true;
                    _root.playershipstatus[5][7] = _root.currentonlineplayers[zzj][1];
                } // end if
                ++zzj;
            } // end while
            _root.playershipstatus[2][5] = 0;
            _root.gotoAndPlay("playerdeath");
        }
        else
        {
            this.playership.attachMovie("shiptype" + _root.playershipstatus[5][0] + "shield", "shipshield", 1);
        } // end if
    } // end else if
} // End of the function
function myhittest(firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    if (secitemx - sechalfwidth > firsthalfwidth)
    {
        return (false);
    }
    else if (secitemx + sechalfwidth < -firsthalfwidth)
    {
        return (false);
    }
    else if (secitemy - sechalfheight > firsthalfheight)
    {
        return (false);
    }
    else if (secitemy + sechalfheight < -firsthalfheight)
    {
        return (false);
    }
    else
    {
        return (true);
    } // end else if
} // End of the function
function othergunfiremovement()
{
    curenttime = getTimer();
    cc = 0;
    if (_root.othergunfire.length > 0)
    {
        playersx = _root.shipcoordinatex;
        playersy = _root.shipcoordinatey;
        halfplayerswidth = this.playership._width / 2;
        halfplayersheight = this.playership._height / 2;
    } // end if
    for (cc = 0; cc < _root.othergunfire.length; cc++)
    {
        if (_root.othergunfire[cc][5] < curenttime)
        {
            removeMovieClip ("othergunfire" + _root.othergunfire[cc][8]);
            _root.othergunfire.splice(cc, 1);
            --cc;
            continue;
        } // end if
        if (myhittest(halfplayerswidth, halfplayersheight, this["othergunfire" + _root.othergunfire[cc][8]]._x, this["othergunfire" + _root.othergunfire[cc][8]]._y, _root.othergunfire[cc][11], _root.othergunfire[cc][12]) && _root.playershipstatus[5][4] == "alive")
        {
            removeMovieClip ("othergunfire" + _root.othergunfire[cc][8]);
            playerwhoshothitid = _root.othergunfire[cc][0];
            gunshothitplayertype = _root.othergunfire[cc][7];
            if (_root.playershipstatus[5][20] != true)
            {
                _root.playershipstatus[2][1] = _root.playershipstatus[2][1] - _root.guntype[gunshothitplayertype][4];
            } // end if
            if (_root.playershipstatus[2][1] < 0)
            {
                func_adddamages();
                _root.playershipstatus[2][5] = _root.playershipstatus[2][5] + _root.playershipstatus[2][1];
                _root.playershipstatus[2][1] = 0;
                _root.gunfireinformation = _root.gunfireinformation + ("GH`" + _root.playershipstatus[3][0] + "`" + _root.othergunfire[cc][0] + "`" + _root.othergunfire[cc][9] + "`ST`" + Math.round(_root.playershipstatus[2][5]) + "~");
            }
            else
            {
                _root.gunfireinformation = _root.gunfireinformation + ("GH`" + _root.playershipstatus[3][0] + "`" + _root.othergunfire[cc][0] + "`" + _root.othergunfire[cc][9] + "`SH`" + Math.round(_root.playershipstatus[2][1]) + "~");
            } // end else if
            _root.othergunfire.splice(cc, 1);
            if (_root.playershipstatus[2][5] <= 0 && _root.playershipstatus[5][4] == "alive")
            {
                _root.playershipstatus[5][4] = "dead";
                _root.playershipstatus[5][5] = playerwhoshothitid;
                zzj = 0;
                dataupdated = false;
                while (zzj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (playerwhoshothitid == _root.currentonlineplayers[zzj][0])
                    {
                        dataupdated = true;
                        _root.playershipstatus[5][7] = _root.currentonlineplayers[zzj][1];
                    } // end if
                    ++zzj;
                } // end while
                _root.playershipstatus[2][5] = 0;
                _root.gotoAndPlay("playerdeath");
            }
            else
            {
                this.playership.attachMovie("shiptype" + _root.playershipstatus[5][0] + "shield", "shipshield", 1);
            } // end else if
            --cc;
        } // end if
    } // end of for
} // End of the function
function othermissilefiremovement()
{
    curenttime = getTimer();
    cc = 0;
    if (_root.othermissilefire.length > 0)
    {
        playersx = _root.shipcoordinatex;
        playersy = _root.shipcoordinatey;
        halfplayerswidth = this.playership._width / 2;
        halfplayersheight = this.playership._height / 2;
    } // end if
    for (cc = 0; cc < _root.othermissilefire.length; cc++)
    {
        if (_root.othermissilefire[cc][5] < curenttime)
        {
            removeMovieClip ("othermissilefire" + _root.othermissilefire[cc][8]);
            _root.othermissilefire.splice(cc, 1);
            --cc;
            continue;
        } // end if
        if (myhittest(halfplayerswidth, halfplayersheight, this["othermissilefire" + _root.othermissilefire[cc][8]]._x, this["othermissilefire" + _root.othermissilefire[cc][8]]._y, _root.othermissilefire[cc][11], _root.othermissilefire[cc][12]) && _root.playershipstatus[5][4] == "alive")
        {
            removeMovieClip ("othermissilefire" + _root.othermissilefire[cc][8]);
            playerwhoshothitid = _root.othermissilefire[cc][0];
            gunshothitplayertype = _root.othermissilefire[cc][7];
            if (_root.playershipstatus[5][20] != true)
            {
                if (_root.missile[gunshothitplayertype][8] == "EMP")
                {
                    empendsat = getTimer() + _root.missile[gunshothitplayertype][9];
                    if (_root.playerempend < empendsat)
                    {
                        _root.playerempend = empendsat;
                        _root.isplayeremp = true;
                    } // end if
                } // end if
                if (_root.missile[gunshothitplayertype][8] == "DISRUPTER")
                {
                    disruptendsat = getTimer() + _root.missile[gunshothitplayertype][9];
                    if (_root.playerdiruptend < disruptendsat)
                    {
                        _root.gamedisplayarea.keyboardscript.disruptplayersengines();
                        _root.playerdiruptend = disruptendsat;
                        _root.isplayerdisrupt = true;
                    } // end if
                } // end if
                _root.playershipstatus[2][1] = _root.playershipstatus[2][1] - _root.missile[gunshothitplayertype][4];
            } // end if
            if (_root.playershipstatus[2][1] < 0)
            {
                func_adddamages();
                _root.playershipstatus[2][5] = _root.playershipstatus[2][5] + _root.playershipstatus[2][1];
                _root.playershipstatus[2][1] = 0;
                _root.gunfireinformation = _root.gunfireinformation + ("MH`" + _root.playershipstatus[3][0] + "`" + _root.othermissilefire[cc][0] + "`" + _root.othermissilefire[cc][9] + "`ST`" + Math.round(_root.playershipstatus[2][5]) + "~");
            }
            else
            {
                _root.gunfireinformation = _root.gunfireinformation + ("MH`" + _root.playershipstatus[3][0] + "`" + _root.othermissilefire[cc][0] + "`" + _root.othermissilefire[cc][9] + "`SH`" + Math.round(_root.playershipstatus[2][1]) + "~");
            } // end else if
            _root.othermissilefire.splice(cc, 1);
            if (_root.playershipstatus[2][5] <= 0 && _root.playershipstatus[5][4] == "alive")
            {
                _root.playershipstatus[5][4] = "dead";
                _root.playershipstatus[5][5] = playerwhoshothitid;
                zzj = 0;
                dataupdated = false;
                while (zzj < _root.currentonlineplayers.length && dataupdated == false)
                {
                    if (playerwhoshothitid == _root.currentonlineplayers[zzj][0])
                    {
                        dataupdated = true;
                        _root.playershipstatus[5][7] = _root.currentonlineplayers[zzj][1];
                    } // end if
                    ++zzj;
                } // end while
                _root.playershipstatus[2][5] = 0;
                _root.gotoAndPlay("playerdeath");
            }
            else
            {
                this.playership.attachMovie("shiptype" + _root.playershipstatus[5][0] + "shield", "shipshield", 1);
            } // end else if
            --cc;
        } // end if
    } // end of for
} // End of the function
currentsparks = 0;
maxstructure = _root.shiptype[_root.playershipstatus[5][0]][3][3];
maxsparks = 4;
