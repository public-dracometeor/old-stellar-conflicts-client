﻿// Action script...

// [onClipEvent of sprite 1405 in frame 1]
onClipEvent (enterFrame)
{
    function firingmissilestartlocation()
    {
        hardpointfromcenter = Math.sqrt(xfireposition * xfireposition + yfireposition * yfireposition);
        initialangleforgun = Math.ASIN(xfireposition / hardpointfromcenter) / 0.017453;
        if (_root.playershipstatus[7][g][2] >= 0)
        {
            if (_root.playershipstatus[7][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[7][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
        if (_root.playershipstatus[7][g][2] < 0)
        {
            if (_root.playershipstatus[7][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[7][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
    } // End of the function
    function firingbulletstartlocation()
    {
        hardpointfromcenter = Math.sqrt(xfireposition * xfireposition + yfireposition * yfireposition);
        initialangleforgun = Math.ASIN(xfireposition / hardpointfromcenter) / 0.017453;
        if (_root.playershipstatus[0][g][2] >= 0)
        {
            if (_root.playershipstatus[0][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[0][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
        if (_root.playershipstatus[0][g][2] < 0)
        {
            if (_root.playershipstatus[0][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[0][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
    } // End of the function
    function missilefirestartlocation()
    {
        hardpointfromcenter = Math.sqrt(xfireposition * xfireposition + yfireposition * yfireposition);
        initialangleforgun = Math.ASIN(xfireposition / hardpointfromcenter) / 0.017453;
        if (_root.playershipstatus[7][g][2] >= 0)
        {
            if (_root.playershipstatus[7][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[7][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
        if (_root.playershipstatus[7][g][2] < 0)
        {
            if (_root.playershipstatus[7][g][3] >= 0)
            {
                finalanglefromcenter = playershipfacing + (90 - initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
            if (_root.playershipstatus[7][g][3] < 0)
            {
                finalanglefromcenter = playershipfacing + (270 + initialangleforgun);
                xfireposition = hardpointfromcenter * Math.cos(0.017453 * finalanglefromcenter);
                yfireposition = hardpointfromcenter * Math.sin(0.017453 * finalanglefromcenter);
            } // end if
        } // end if
    } // End of the function
    function gunfiremovement()
    {
        for (i = 0; i < _root.playershotsfired.length; i++)
        {
            if (_root.playershotsfired[i][5] < curenttime)
            {
                removeMovieClip ("_root.gamedisplayarea.playergunfire" + _root.playershotsfired[i][0]);
                _root.playershotsfired.splice(i, 1);
                --i;
            } // end if
        } // end of for
    } // End of the function
    function findoutcurrentsector()
    {
        lastsector = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
        _root.playershipstatus[6][0] = Math.ceil(shipcoordinatex / xwidthofasector);
        _root.playershipstatus[6][1] = Math.ceil(shipcoordinatey / ywidthofasector);
        currentsector = _root.playershipstatus[6][0] + "`" + _root.playershipstatus[6][1];
        if (lastsector != currentsector)
        {
            xsector = _root.playershipstatus[6][0];
            ysector = _root.playershipstatus[6][1];
            playersquadbasesdisplay(xsector, ysector);
        } // end if
    } // End of the function
    lasttime = curenttime;
    curenttime = getTimer();
    currenttimechangeratio = (curenttime - lasttime) * 0.001000;
    if (_root.isplayerdisrupt)
    {
        if (_root.playerdiruptend < getTimer())
        {
            _root.isplayerdisrupt = false;
            isdownkeypressed = false;
        } // end if
    } // end if
    ++currentframeforcalc;
    if (currentframeforcalc > frameintforcalc)
    {
        currentframeforcalc = 0;
        avetimeperfram = (curenttime - lastimeframecaldone) / frameintforcalc;
        framepersec = Math.round(1000 / avetimeperfram);
        _root.gamedisplayarea.test2 = "Frame Rate: " + avetimeperfram + " ms." + "FPS: " + framepersec;
        if (framepersec < 5)
        {
            if (firstenter == false)
            {
                _root.controlledserverclose = true;
                _root.gameerror = "fpsstopped";
                _root.func_closegameserver();
            } // end if
        } // end if
        firstenter = false;
        lastimeframecaldone = curenttime;
    } // end if
    this.playershipfacing = _root.playershipfacing;
    this.playershipvelocity = _root.playershipvelocity;
    this.shipcoordinatex = _root.shipcoordinatex;
    this.shipcoordinatey = _root.shipcoordinatey;
    shotstobedeleted = new Array();
    if (_root.playershipstatus[5][4] == "alive")
    {
        if (isdkeypressed == true && _root.gamechatinfo[4] == false)
        {
            for (i = 0; i < _root.starbaselocation.length; i++)
            {
                if (_root.starbaselocation[i][5] == "ACTIVE" || Number(_root.starbaselocation[i][5]) < getTimer())
                {
                    starbasex = _root.starbaselocation[i][1];
                    starbasey = _root.starbaselocation[i][2];
                    starbaseradius = _root.starbaselocation[i][4];
                    relativestarbasex = starbasex - _root.shipcoordinatex;
                    relativestarbasey = starbasey - _root.shipcoordinatey;
                    if (relativestarbasex * relativestarbasex + relativestarbasey * relativestarbasey <= starbaseradius * starbaseradius)
                    {
                        if (_root.starbaselocation[i][0].substr(0, 2) == "SB")
                        {
                            ainame = "STARBASE";
                        }
                        else if (_root.starbaselocation[i][0].substr(0, 2) == "PL")
                        {
                            pltype = _root.starbaselocation[i][3];
                            ainame = "PLANET" + pltype;
                        } // end else if
                        if (playershipvelocity > maxdockingvelocity)
                        {
                            message = "You must have a velocity lass than " + maxdockingvelocity + " in order to dock at a Starbase";
                            _root.func_messangercom(message, ainame, "BEGIN");
                            continue;
                        } // end if
                        if (_root.starbaselocation[i][9] > getTimer())
                        {
                            message = "You Attack me and then want to dock? Maybe, later I\'ll let you do that!";
                            _root.func_messangercom(message, ainame, "BEGIN");
                            continue;
                        } // end if
                        if (_root.mapdsiplayed == false)
                        {
                            _root.playershipstatus[4][0] = _root.starbaselocation[i][0];
                            if (String(_root.kingofflag[0]) == String(_root.playershipstatus[3][0]))
                            {
                                datatosend = "KFLAG`DROP`" + Math.round(_root.shipcoordinatex) + "`" + Math.round(_root.shipcoordinatey) + "`" + _root.errorchecknumber + "~";
                                _root.mysocket.send(datatosend);
                            } // end if
                            removeMovieClip ("_root.toprightinformation");
                            if (_root.isgameracingzone == true)
                            {
                                _root.gotoAndStop("racingzonescreen");
                                continue;
                            } // end if
                            if (_root.starbaselocation[i][0].substr(0, 2) == "SB")
                            {
                                _root.gotoAndStop("stardock");
                                continue;
                            } // end if
                            if (_root.starbaselocation[i][0].substr(0, 2) == "PL")
                            {
                                _root.gotoAndStop("planet");
                            } // end if
                        } // end if
                    } // end if
                } // end if
            } // end of for
            for (jj = 0; jj < _root.playersquadbases.length; jj++)
            {
                if (_root.playersquadbases[jj][0] == _root.playershipstatus[5][10])
                {
                    starbasex = _root.playersquadbases[jj][2];
                    starbasey = _root.playersquadbases[jj][3];
                    starbaseradius = 100;
                    relativestarbasex = starbasex - _root.shipcoordinatex;
                    relativestarbasey = starbasey - _root.shipcoordinatey;
                    this.gamebackground["playerbase" + _root.playersquadbases[jj][0]].baseidname = _root.playersquadbases[jj][0];
                    this.gamebackground["playerbase" + _root.playersquadbases[jj][0]].squadid = _root.playersquadbases[jj][0];
                    if (relativestarbasex * relativestarbasex + relativestarbasey * relativestarbasey <= starbaseradius * starbaseradius)
                    {
                        if (playershipvelocity > maxdockingvelocity)
                        {
                            ainame = "SQUADBASE";
                            message = "You must have a velocity lass than " + maxdockingvelocity + " in order to dock at this Base";
                            _root.func_messangercom(message, ainame, "BEGIN");
                            continue;
                        } // end if
                        if (_root.mapdsiplayed == false)
                        {
                            _root.gamedisplayarea.test = "DOCKING at " + _root.starbaselocation[i][0];
                            _root.squadbasedockedat = _root.playersquadbases[jj][0];
                            removeMovieClip ("_root.toprightinformation");
                            _root.gotoAndStop("squadbase");
                        } // end if
                    } // end if
                } // end if
            } // end of for
            if (_root.teamdeathmatch == true)
            {
                jj = 0;
                basenearby = false;
                while (jj < _root.teambases.length)
                {
                    starbasex = _root.teambases[jj][1];
                    starbasey = _root.teambases[jj][2];
                    starbaseradius = _root.teambases[jj][4];
                    relativestarbasex = Number(starbasex) - Number(_root.shipcoordinatex);
                    relativestarbasey = Number(starbasey) - Number(_root.shipcoordinatey);
                    if (Math.sqrt(relativestarbasex * relativestarbasex + relativestarbasey * relativestarbasey) <= starbaseradius)
                    {
                        if (jj != _root.playershipstatus[5][2])
                        {
                            ainame = "HELP";
                            message = "You can only dock  at your teams base!";
                            _root.func_messangercom(message, ainame, "BEGIN");
                        }
                        else if (playershipvelocity > maxdockingvelocity)
                        {
                            ainame = "HELP";
                            message = "You must have a velocity lass than " + maxdockingvelocity + " in order to dock at this Base";
                            _root.func_messangercom(message, ainame, "BEGIN");
                        }
                        else if (_root.mapdsiplayed == false)
                        {
                            removeMovieClip ("_root.toprightinformation");
                            _root.gotoAndStop("teambase");
                        } // end else if
                        basenearby = true;
                        break;
                    } // end if
                    ++jj;
                } // end while
            } // end if
        } // end if
        if (isshiftkeypressed == true && _root.gamechatinfo[4] == false)
        {
            playershipvelocity = playershipvelocity + playershipacceleration * currenttimechangeratio * 2;
            if (playershipvelocity >= playershipmaxvelocity)
            {
                playershipvelocity = afterburnerspeed;
            } // end if
            _root.afterburnerinuse = true;
        }
        else
        {
            _root.afterburnerinuse = false;
        } // end else if
        if (_root.afterburnerinuse == false)
        {
            if (isupkeypressed == true)
            {
                playershipvelocity = playershipvelocity + playershipacceleration * currenttimechangeratio;
            } // end if
            if (isdownkeypressed == true)
            {
                if (playershipvelocity > 0 && playershipvelocity < playershipacceleration * currenttimechangeratio)
                {
                    playershipvelocity = 0;
                } // end if
                if (playershipvelocity > 0)
                {
                    playershipvelocity = playershipvelocity - playershipacceleration * currenttimechangeratio;
                } // end if
            } // end if
            if (playershipvelocity > playershipmaxvelocity)
            {
                playershipvelocity = playershipmaxvelocity;
            } // end if
        } // end if
        playershiprotating = 0;
        if (isrightkeypressed == true)
        {
            playershiprotating = playershiprotating + playershiprotation;
        } // end if
        if (isleftkeypressed == true)
        {
            playershiprotating = playershiprotating - playershiprotation;
        } // end if
        if (playershiprotating != 0)
        {
            playershiprotating = playershiprotating - playershipvelocity / playershipmaxvelocity * playerrotationdegredation * playershiprotating;
            newshiprotation = playershipfacing + playershiprotating * currenttimechangeratio;
            if (newshiprotation < 0)
            {
                newshiprotation = newshiprotation + 360;
            } // end if
            if (newshiprotation > 360)
            {
                newshiprotation = newshiprotation - 360;
            } // end if
            ++currentrotationframe;
            if (currentrotationframe > rotationdelay)
            {
                currentrotationframe = 0;
                _root.gamedisplayarea.playership._rotation = newshiprotation;
            } // end if
            _root.playershipfacing = newshiprotation;
            relativefacing = newshiprotation;
        } // end if
    } // end if
    velocity = playershipvelocity * currenttimechangeratio;
    movementofanobjectwiththrust();
    xcoordadjustment = xmovement;
    ycoordadjustment = ymovement;
    shipcoordinatex = shipcoordinatex + xcoordadjustment;
    shipcoordinatey = shipcoordinatey + ycoordadjustment;
    if (iscontrolkeypressed == true && _root.playershipstatus[5][4] == "alive")
    {
        for (g = 0; g < _root.playershipstatus[0].length; g++)
        {
            if (_root.playershipstatus[0][g][1] <= curenttime && _root.playershipstatus[1][1] > _root.playershipstatus[0][g][5] && _root.playershipstatus[0][g][0] != "none" && _root.playershipstatus[0][g][4] == "ON")
            {
                if (_root.playershipstatus[5][15] == "C")
                {
                    turnofshipcloak();
                } // end if
                _root.playershipstatus[1][1] = _root.playershipstatus[1][1] - _root.playershipstatus[0][g][5];
                currentnumber = _root.playershotsfired.length;
                if (currentnumber < 1)
                {
                    _root.playershotsfired = new Array();
                    currentnumber = 0;
                } // end if
                gunshottypenumber = _root.playershipstatus[0][g][0];
                gunshotvelocity = _root.playershipstatus[0][g][7];
                relativefacing = playershipfacing;
                velocity = playershipvelocity + gunshotvelocity;
                movementofanobjectwiththrust();
                xcoordadjustment = xmovement;
                ycoordadjustment = ymovement;
                if (_root.currentplayershotsfired > 999)
                {
                    _root.currentplayershotsfired = 0;
                } // end if
                i = _root.currentplayershotsfired;
                xfireposition = _root.playershipstatus[0][g][2];
                yfireposition = _root.playershipstatus[0][g][3];
                firingbulletstartlocation();
                _root.playershotsfired[currentnumber] = new Array();
                _root.playershotsfired[currentnumber][0] = i;
                _root.playershotsfired[currentnumber][1] = Math.round(xfireposition + shipcoordinatex);
                _root.playershotsfired[currentnumber][2] = Math.round(yfireposition + shipcoordinatey);
                _root.playershotsfired[currentnumber][3] = Math.round(xcoordadjustment);
                _root.playershotsfired[currentnumber][4] = Math.round(ycoordadjustment);
                _root.playershotsfired[currentnumber][5] = curenttime + _root.playershipstatus[0][g][8];
                _root.playershotsfired[currentnumber][6] = _root.playershipstatus[0][g][0];
                _root.playershotsfired[currentnumber][7] = playershipfacing;
                _root.playershotsfired[currentnumber][8] = curenttime + _root.playershipstatus[0][g][6];
                _root.gamedisplayarea.attachMovie("guntype" + gunshottypenumber + "fire", "playergunfire" + i, 5000 + i);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _rotation, playershipfacing);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _x, _root.playershotsfired[currentnumber][1] - _root.shipcoordinatex);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _y, _root.playershotsfired[currentnumber][2] - _root.shipcoordinatey);
                set("_root.gamedisplayarea.playergunfire" + i + ".xmovement", xcoordadjustment);
                set("_root.gamedisplayarea.playergunfire" + i + ".ymovement", ycoordadjustment);
                set("_root.gamedisplayarea.playergunfire" + i + ".xposition", _root.playershotsfired[currentnumber][1]);
                set("_root.gamedisplayarea.playergunfire" + i + ".yposition", _root.playershotsfired[currentnumber][2]);
                _root.playershotsfired[currentnumber][13] = "GUNS";
                if (_root.soundvolume != "off")
                {
                    _root["guntype" + gunshottypenumber + "sound"].start();
                } // end if
                _root.playershipstatus[0][g][1] = _root.playershotsfired[currentnumber][8];
                globaltimestamp = String(getTimer() + _root.clocktimediff);
                globaltimestamp = Number(globaltimestamp.substr(globaltimestamp.length - 4));
                if (globaltimestamp > 10000)
                {
                    globaltimestamp = globaltimestamp - 10000;
                } // end if
                _root.gunfireinformation = _root.gunfireinformation + ("GF`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[currentnumber][1] + "`" + _root.playershotsfired[currentnumber][2] + "`" + Math.round(velocity) + "`" + Math.round(playershipfacing) + "`" + _root.playershotsfired[currentnumber][6] + "`" + i + "`" + globaltimestamp + "~");
                _root.playershotsfired[currentnumber][11] = _root.gamedisplayarea["playergunfire" + i]._width / 2;
                _root.playershotsfired[currentnumber][12] = _root.gamedisplayarea["playergunfire" + i]._height / 2;
                ++_root.currentplayershotsfired;
            } // end if
        } // end of for
    } // end if
    if (isspacekeypressed == true && _root.playershipstatus[5][4] == "alive" && _root.gamechatinfo[4] == false)
    {
        g = _root.playershipstatus[10];
        currentmisiletype = _root.playershipstatus[7][g][0];
        if (_root.playershipstatus[7][g][1] <= curenttime && _root.playershipstatus[7][g][0] != "none" && _root.playershipstatus[7][g][4][currentmisiletype] > 0 && spacekeyjustpressed == true)
        {
            if (_root.playershipstatus[5][15] == "C")
            {
                turnofshipcloak();
            } // end if
            spacekeyjustpressed = false;
            _root.playershipstatus[7][g][1] = curenttime + _root.missile[currentmisiletype][3];
            currentnumber = _root.playershotsfired.length;
            if (currentnumber < 1)
            {
                _root.playershotsfired = new Array();
                currentnumber = 0;
            } // end if
            missileshottypenumber = currentmisiletype;
            missileshotvelocity = _root.missile[currentmisiletype][0];
            relativefacing = playershipfacing;
            velocity = missileshotvelocity + playershipvelocity;
            movementofanobjectwiththrust();
            xcoordadjustment = xmovement;
            ycoordadjustment = ymovement;
            globaltimestamp = String(getTimer() + _root.clocktimediff);
            globaltimestamp = Number(globaltimestamp.substr(globaltimestamp.length - 4));
            if (globaltimestamp > 10000)
            {
                globaltimestamp = globaltimestamp - 10000;
            } // end if
            if (_root.currentplayershotsfired > 999)
            {
                _root.currentplayershotsfired = 0;
            } // end if
            i = _root.currentplayershotsfired;
            _root.playershotsfired[currentnumber] = new Array();
            _root.playershotsfired[currentnumber][0] = i;
            xfireposition = _root.playershipstatus[7][g][2];
            yfireposition = _root.playershipstatus[7][g][3];
            missilefirestartlocation();
            if (_root.missile[currentmisiletype][8] == "SEEKER" || _root.missile[currentmisiletype][8] == "EMP" || _root.missile[currentmisiletype][8] == "DISRUPTER")
            {
                if (_root.targetinfo[0] != "None" && _root.targetinfo[0].charAt(0) != "*" && _root.targetinfo[3] < 999999)
                {
                    if (_root.soundvolume != "off")
                    {
                        _root.missilefiresound.start();
                    } // end if
                    --_root.playershipstatus[7][g][4][currentmisiletype];
                    _root.rightside.Missilebank.refreshqty();
                    if (_root.targetinfo[7] == "aiship")
                    {
                        _root.playershotsfired[currentnumber][0] = _root.currentplayershotsfired;
                        _root.playershotsfired[currentnumber][1] = Math.round(xfireposition + shipcoordinatex);
                        _root.playershotsfired[currentnumber][2] = Math.round(yfireposition + shipcoordinatey);
                        _root.playershotsfired[currentnumber][3] = Math.round(xcoordadjustment);
                        _root.playershotsfired[currentnumber][4] = Math.round(ycoordadjustment);
                        _root.playershotsfired[currentnumber][5] = curenttime + _root.missile[currentmisiletype][2];
                        _root.playershotsfired[currentnumber][6] = currentmisiletype;
                        _root.playershotsfired[currentnumber][7] = playershipfacing;
                        gunshottypenumber = currentmisiletype;
                        _root.gamedisplayarea.attachMovie("missile" + currentmisiletype + "fire", "playergunfire" + i, 5000 + i);
                        setProperty("_root.gamedisplayarea.playergunfire" + i, _rotation, playershipfacing);
                        setProperty("_root.gamedisplayarea.playergunfire" + i, _x, _root.playershotsfired[currentnumber][1] - shipcoordinatex);
                        setProperty("_root.gamedisplayarea.playergunfire" + i, _y, _root.playershotsfired[currentnumber][2] - shipcoordinatey);
                        set("_root.gamedisplayarea.playergunfire" + i + ".xmovement", xcoordadjustment);
                        set("_root.gamedisplayarea.playergunfire" + i + ".ymovement", ycoordadjustment);
                        set("_root.gamedisplayarea.playergunfire" + i + ".xposition", _root.playershotsfired[currentnumber][1]);
                        set("_root.gamedisplayarea.playergunfire" + i + ".yposition", _root.playershotsfired[currentnumber][2]);
                        set("_root.gamedisplayarea.playergunfire" + i + ".missiletype", currentmisiletype);
                        set("_root.gamedisplayarea.playergunfire" + i + ".aitargetnumber", _root.targetinfo[8]);
                        set("_root.gamedisplayarea.playergunfire" + i + ".velocity", velocity);
                        set("_root.gamedisplayarea.playergunfire" + i + ".seeking", "AI");
                    }
                    else if (_root.targetinfo[7] == "otherplayership")
                    {
                        _root.playershotsfired[currentnumber][0] = _root.currentplayershotsfired;
                        _root.playershotsfired[currentnumber][1] = Math.round(xfireposition + shipcoordinatex);
                        _root.playershotsfired[currentnumber][2] = Math.round(yfireposition + shipcoordinatey);
                        _root.playershotsfired[currentnumber][3] = Math.round(xcoordadjustment);
                        _root.playershotsfired[currentnumber][4] = Math.round(ycoordadjustment);
                        _root.playershotsfired[currentnumber][5] = curenttime + _root.missile[currentmisiletype][2];
                        _root.playershotsfired[currentnumber][6] = currentmisiletype;
                        _root.playershotsfired[currentnumber][7] = playershipfacing;
                        gunshottypenumber = currentmisiletype;
                        _root.gunfireinformation = _root.gunfireinformation + ("MF`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[currentnumber][1] + "`" + _root.playershotsfired[currentnumber][2] + "`" + Math.round(velocity) + "`" + playershipfacing + "`" + currentmisiletype + "`" + i + "`" + globaltimestamp + "`" + _root.otherplayership[_root.targetinfo[8]][0] + "`" + "~");
                        missilename = _root.playershipstatus[3][0] + "n" + i;
                        _root.gamedisplayarea.attachMovie("missile" + _root.playershotsfired[currentnumber][6] + "fire", "othermissilefire" + missilename, 998000 + i);
                        setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _rotation, playershipfacing);
                        setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _x, _root.playershotsfired[currentnumber][1] - _root.shipcoordinatex);
                        setProperty("_root.gamedisplayarea.othermissilefire" + missilename, _y, _root.playershotsfired[currentnumber][2] - _root.shipcoordinatey);
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".xposition", _root.playershotsfired[currentnumber][1]);
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".yposition", _root.playershotsfired[currentnumber][2]);
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".missiletype", currentmisiletype);
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".velocity", Math.round(velocity));
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".seeking", "OTHER");
                        set("_root.gamedisplayarea.othermissilefire" + missilename + ".rotation", playershipfacing);
                    } // end else if
                    _root.playershotsfired[currentnumber][13] = "MISSILE";
                    _root.playershotsfired[currentnumber][11] = _root.gamedisplayarea["playergunfire" + i]._width / 2;
                    _root.playershotsfired[currentnumber][12] = _root.gamedisplayarea["playergunfire" + i]._height / 2;
                    _root.playershipstatus[7][g][1] = curenttime + _root.missile[currentmisiletype][3];
                } // end if
            }
            else if (_root.missile[currentmisiletype][8] != "SEEKER")
            {
                if (_root.soundvolume != "off")
                {
                    _root.missilefiresound.start();
                } // end if
                --_root.playershipstatus[7][g][4][currentmisiletype];
                _root.rightside.Missilebank.refreshqty();
                _root.playershotsfired[currentnumber][0] = i;
                _root.playershotsfired[currentnumber][1] = Math.round(xfireposition + shipcoordinatex);
                _root.playershotsfired[currentnumber][2] = Math.round(yfireposition + shipcoordinatey);
                _root.playershotsfired[currentnumber][3] = Math.round(xcoordadjustment);
                _root.playershotsfired[currentnumber][4] = Math.round(ycoordadjustment);
                _root.playershotsfired[currentnumber][5] = curenttime + _root.missile[currentmisiletype][2];
                _root.playershotsfired[currentnumber][6] = currentmisiletype;
                _root.playershotsfired[currentnumber][7] = playershipfacing;
                gunshottypenumber = currentmisiletype;
                _root.gamedisplayarea.attachMovie("missile" + currentmisiletype + "fire", "playergunfire" + i, 5000 + i);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _rotation, playershipfacing);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _x, _root.playershotsfired[currentnumber][1] - shipcoordinatex);
                setProperty("_root.gamedisplayarea.playergunfire" + i, _y, _root.playershotsfired[currentnumber][2] - shipcoordinatey);
                set("_root.gamedisplayarea.playergunfire" + i + ".xmovement", xcoordadjustment);
                set("_root.gamedisplayarea.playergunfire" + i + ".ymovement", ycoordadjustment);
                set("_root.gamedisplayarea.playergunfire" + i + ".xposition", _root.playershotsfired[currentnumber][1]);
                set("_root.gamedisplayarea.playergunfire" + i + ".yposition", _root.playershotsfired[currentnumber][2]);
                _root.playershotsfired[currentnumber][13] = "MISSILE";
                _root.playershotsfired[currentnumber][11] = _root.gamedisplayarea["playergunfire" + i]._width / 2;
                _root.playershotsfired[currentnumber][12] = _root.gamedisplayarea["playergunfire" + i]._height / 2;
                _root.playershipstatus[7][g][1] = curenttime + _root.missile[currentmisiletype][3];
                _root.gunfireinformation = _root.gunfireinformation + ("MF`" + _root.playershipstatus[3][0] + "`" + _root.playershotsfired[currentnumber][1] + "`" + _root.playershotsfired[currentnumber][2] + "`" + Math.round(velocity) + "`" + playershipfacing + "`" + _root.playershotsfired[currentnumber][6] + "`" + i + "`" + globaltimestamp + "~");
            } // end else if
            ++_root.currentplayershotsfired;
        } // end if
    } // end if
    gunfiremovement();
    findoutcurrentsector();
    _root.playershipvelocity = this.playershipvelocity;
    _root.playershipmaxvelocity = this.playershipmaxvelocity;
    _root.playershipacceleration = _root.playershipacceleration;
    _root.shipcoordinatex = this.shipcoordinatex;
    _root.shipcoordinatey = this.shipcoordinatey;
}
