﻿// Action script...

// [onClipEvent of sprite 589 in frame 3]
onClipEvent (load)
{
    xwidthofasector = _root.sectorinformation[1][0];
    ywidthofasector = _root.sectorinformation[1][1];
    _parent.members = "Loading";
    _parent.baseloc = "Loading";
    _parent.systemloc = "Loading";
    _parent.number = "N/A";
    newsquadVars = new XML();
    newsquadVars.load(_root.pathtoaccounts + "squads.php?mode=squadinfo&sname=" + _root.playershipstatus[5][10]);
    newsquadVars.onLoad = function (success)
    {
        noofsquad = 0;
        loadedinfo = String(newsquadVars);
        newinfo = loadedinfo.split("~");
        _parent.members = "";
        for (i = 0; i < newinfo.length - 1; i++)
        {
            currentthread = newinfo[i].split("`");
            if (currentthread[0] == "P")
            {
                ++noofsquad;
                _parent.members = _parent.members + (noofsquad + ": " + currentthread[1] + "\r");
            } // end if
        } // end of for
        _parent.number = noofsquad;
    };
    newbaseVars = new XML();
    newbaseVars.load(_root.pathtoaccounts + "squadbases.php?mode=baselocate&baseid=" + _root.playershipstatus[5][10]);
    newbaseVars.onLoad = function (success)
    {
        loadedbaseinfo = String(newbaseVars);
        if (loadedbaseinfo == "nobase")
        {
            _parent.baseloc._visible = false;
            _parent.baseloc = "";
            _parent.createbase._visible = true;
            _parent.systemloc._visible = false;
            _parent.systemloc = "";
        }
        else
        {
            _parent.createbase._visible = false;
            basecoords = loadedbaseinfo.split("`");
            xcoord = Number(basecoords[0]);
            ycoord = Number(basecoords[1]);
            xgrid = Math.ceil(xcoord / xwidthofasector);
            ygrid = Math.ceil(ycoord / ywidthofasector);
            baseinsystem = basecoords[2];
            _parent.baseloc._visible = true;
            _parent.systemloc._visible = true;
            _parent.baseloc = "Grid X: " + xgrid + ", Y: " + ygrid;
            _parent.systemloc = "System: " + baseinsystem;
        } // end else if
    };
}

// [onClipEvent of sprite 589 in frame 3]
on (release)
{
    _parent.gotoAndStop("start");
}

// [onClipEvent of sprite 569 in frame 3]
onClipEvent (load)
{
    this.label = "CREATE BASE";
    this._visible = false;
    if (_root.teamdeathmatch == true || _root.isgameracingzone == true)
    {
        this.label = "NO BASES";
    } // end if
}

// [onClipEvent of sprite 569 in frame 3]
on (release)
{
    if (_root.teamdeathmatch != true && _root.isgameracingzone != true)
    {
        _parent.gotoAndStop("createbase");
        
    } // end if
}

// [Action in Frame 3]
stop ();
