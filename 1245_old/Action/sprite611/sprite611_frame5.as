﻿// Action script...

// [onClipEvent of sprite 569 in frame 5]
on (release)
{
    squadname = squadname.toUpperCase();
    passwordd = passwordd.toUpperCase();
    wipebaselifeVars = new XML();
    wipebaselifeVars.load(_root.pathtoaccounts + "squadbases.php?mode=damagebase&struct=999999&baseid=" + _root.playershipstatus[5][10]);
    wipebaselifeVars.onLoad = function (success)
    {
    };
    newsquadVars = new XML();
    newsquadVars.load(_root.pathtoaccounts + "squads.php?mode=dissolvesquad&sname=" + _root.playershipstatus[5][10] + "&name=" + _root.playershipstatus[3][2] + "&spass=" + _root.playershipstatus[5][13]);
    newsquadVars.onLoad = function (success)
    {
        datatosend = "SA~NEWS`SB`DESTROYED`" + _root.playershipstatus[5][10] + "~";
        _root.mysocket.send(datatosend);
        _root.playershipstatus[5][10] = "NONE";
        _root.playershipstatus[5][11] = false;
        datatosend = "SQUAD`" + _root.playershipstatus[3][0] + "`CHANGE`" + _root.playershipstatus[5][10] + "~";
        _root.mysocket.send(datatosend);
        _root.playershipstatus[5][13] = "";
        _parent.gotoAndStop("start");
    };
}

// [onClipEvent of sprite 569 in frame 5]
on (release)
{
    wipebaselifeVars = new XML();
    wipebaselifeVars.load(_root.pathtoaccounts + "squadbases.php?mode=damagebase&struct=999999&baseid=" + _root.playershipstatus[5][10]);
    wipebaselifeVars.onLoad = function (success)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "Destroyed";
        datatosend = "SA~NEWS`SB`DESTROYED`" + _root.playershipstatus[5][10] + "~";
        _root.mysocket.send(datatosend);
    };
}

// [Action in Frame 5]
this.dissolve.label = "Dissolve Squad";
this.destorybase.label = "Destroy Base";
stop ();
