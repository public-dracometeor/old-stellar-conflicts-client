﻿// Action script...

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][11] != true)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "This Is For Squad Creators ONLY!";
    }
    else if (_root.playershipstatus[5][10] != "NONE")
    {
        _parent.gotoAndStop("managesquad");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Be In A Squad First!";
    } // end else if
}
