﻿// Action script...

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][10] == "NONE")
    {
        _parent.gotoAndStop("joinsquad");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Leave your squad First!";
    } // end else if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][10] != "NONE")
    {
        _parent.gotoAndStop("squadinfo");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Be In A Squad First!";
    } // end else if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][11] != true)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "This Is For Squad Creators ONLY!";
    }
    else if (_root.playershipstatus[5][10] != "NONE")
    {
        _parent.gotoAndStop("managesquad");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Be In A Squad First!";
    } // end else if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][10] == "NONE")
    {
        _parent.gotoAndStop("createsquad");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Leave your squad First!";
    } // end else if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.playershipstatus[5][11] == true)
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Destroy Your Squad First!";
    }
    else if (_root.playershipstatus[5][10] != "NONE")
    {
        _parent.gotoAndStop("leavesquad");
    }
    else
    {
        _parent.attachMovie("hardwarewarningbox", "hardwarewarningbox", 1);
        _parent.hardwarewarningbox.information = "You Must Be In A Squad First!";
    } // end else if
}

// [Action in Frame 1]
this.squadname = _root.playershipstatus[5][10];
stop ();

this.joinsquad.label = "Join A Squad";
this.squadinfo.label = "Squad Info";
this.createsquad.label = "Create A Squad";
this.managesquad.label = "Manage Squad";
this.leavesquad.label = "Leave Squad";
stop ();
