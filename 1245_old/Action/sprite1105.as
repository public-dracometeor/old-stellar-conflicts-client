﻿// Action script...

// [Action in Frame 1]
stop ();

// [Action in Frame 2]
timestarted = getTimer();
timetokeepit = 15000;
timetoclear = timestarted + timetokeepit;

// [Action in Frame 4]
if (timetoclear > getTimer())
{
    gotoAndPlay(3);
} // end if

// [Action in Frame 5]
