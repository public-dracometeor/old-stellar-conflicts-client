﻿// Action script...

// [Action in Frame 1]
function func_assasskilled()
{
    status = "Completed";
    reward = _root.playersmission[0][2];
    message = "You have assassinated " + _root.playersmission[0][0][1] + ". Your account has been awarded " + reward + " credits. Thank you for your help!";
    _root.func_messangercom(message, "EMPLOYER", "BEGIN");
    _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + reward;
    _root.toprightinformation.playerfunds = "Funds: " + _root.playershipstatus[3][1];
    scoreaward = Math.round(reward / _root.missiontoactualscoremodifier);
    information = "TC`" + _root.playershipstatus[3][0] + "`SCORE`" + scoreaward + "~";
    _root.mysocket.send(information);
    _root.playersmission = new Array();
} // End of the function
stop ();
