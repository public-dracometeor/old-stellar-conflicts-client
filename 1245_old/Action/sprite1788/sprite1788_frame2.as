﻿// Action script...

// [Action in Frame 2]
if (currentsparktime > sparktimeinterval)
{
    this._visible = true;
    thiscolor.setRGB(sparkcolour[Math.round(Math.random() * (sparkcolour.length - 1))]);
} // end if
if (currentsparktime > sparktimeinterval + sparkduration)
{
    this._visible = false;
    currentsparktime = Math.round(Math.random() * (sparktimeinterval / 2));
} // end if
++currentsparktime;
