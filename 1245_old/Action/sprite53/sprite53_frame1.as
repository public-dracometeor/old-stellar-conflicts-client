﻿// Action script...

// [Action in Frame 1]
xposition = this._x;
yposition = this._y;
maxsize = 60;
startsize = 20;
sizediff = maxsize - startsize;
startime = getTimer();
durationtime = 500;
timetoend = startime + durationtime;
this.onEnterFrame = function ()
{
    currenttime = getTimer();
    if (timetoend < currenttime)
    {
        removeMovieClip (this);
    }
    else
    {
        this._x = xposition - _root.shipcoordinatex;
        this._y = yposition - _root.shipcoordinatey;
        size = Math.round(maxsize - sizediff * ((timetoend - currenttime) / durationtime));
        this._height = this._width = size;
    } // end else if
};
stop ();
