﻿// Action script...

on (release)
{
    _parent._parent.destination = label;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    playerdistancefrompoint = Math.round(Math.sqrt((playersxcoord - xlocation) * (playersxcoord - xlocation) + (playersycoord - ylocation) * (playersycoord - ylocation)));
    _parent._parent.range = "Range: " + playerdistancefrompoint;
    _parent._parent.xrange = xlocation;
    _parent._parent.yrange = ylocation;
    _parent._parent.information = information;
    _root.playersdestination[0] = label;
    _root.playersdestination[1] = xlocation;
    _root.playersdestination[2] = ylocation;
}
