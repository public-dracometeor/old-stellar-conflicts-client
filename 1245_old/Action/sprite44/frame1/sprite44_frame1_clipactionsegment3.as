﻿// Action script...

// [onClipEvent of sprite 33 in frame 1]
onClipEvent (load)
{
    _parent.hardpointnumber = "Hardpoint: " + _parent.hardpoint;
    _parent.gunname = _root.guntype[_parent.gunlabel][6];
    if (_root.playershipstatus[0][_parent.hardpoint][4] == "ON")
    {
        _parent.offbutton.gotoAndStop(2);
        _parent.onbutton.gotoAndStop(1);
    } // end if
    if (_root.playershipstatus[0][_parent.hardpoint][4] == "OFF")
    {
        _parent.offbutton.gotoAndStop(1);
        _parent.onbutton.gotoAndStop(2);
    } // end if
}
