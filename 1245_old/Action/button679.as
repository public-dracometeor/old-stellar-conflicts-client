﻿// Action script...

on (release)
{
    function taketheorder()
    {
        newtradeVars = new XML();
        newtradeVars.load(_root.pathtoaccounts + "tradegoods.php?system=" + _root.playershipstatus[5][1] + "&mode=" + purchasemode + "&starbase=" + _root.playershipstatus[4][0] + "&quantity=" + quantity + "&itemno=" + itemtype);
        newtradeVars.onLoad = function (success)
        {
        };
    } // End of the function
    if (functiontodo == "Buy")
    {
        purchasemode = "buy";
        this.quantity = Math.floor(this.quantity);
        if (this.quantity > 0)
        {
            if (this.quantity > this.maxquantity)
            {
                this.quantity = this.maxquantity;
            } // end if
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - this.quantity * this.itemprice;
            _root.playershipstatus[4][1][this.itemtype] = _root.playershipstatus[4][1][this.itemtype] + this.quantity;
            taketheorder();
        } // end if
    } // end if
    if (functiontodo == "Sell")
    {
        purchasemode = "sell";
        this.quantity = Math.floor(this.quantity);
        if (this.quantity > 0)
        {
            if (this.quantity > this.itemowned)
            {
                this.quantity = this.itemowned;
            } // end if
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + this.quantity * this.itemprice;
            _root.func_setoldfundsuptocurrentammount();
            _root.playershipstatus[4][1][this.itemtype] = _root.playershipstatus[4][1][this.itemtype] - this.quantity;
            taketheorder();
        } // end if
    } // end if
    this.quantity = 0;
}
