﻿// Action script...

// [Action in Frame 1]
count = 0;
if (!isNaN(Number(_root.playershipstatus[3][1])))
{
    _root.lastplayersuccessfulfunds = _root.playershipstatus[3][1];
}
else if (isNaN(_root.playershipstatus[3][1]))
{
    if (!isNaN(Number(_root.lastplayersuccessfulfunds)))
    {
        _root.playershipstatus[3][1] = _root.lastplayersuccessfulfunds;
    } // end if
} // end else if
if (!isNaN(Number(_root.playershipstatus[5][9])))
{
    _root.lastplayersuccessfulscore = _root.playershipstatus[5][9];
}
else if (!isNaN(Number(_root.lastplayersuccessfulscore)))
{
    _root.playershipstatus[5][9] = _root.lastplayersuccessfulscore;
} // end else if
this.onEnterFrame = function ()
{
    this.funds = "Funds: " + _root.func_formatnumber(_root.playershipstatus[3][1]);
    if (isNaN(Number(_root.playershipstatus[3][1])))
    {
        if (!isNaN(Number(_root.lastplayersuccessfulfunds)))
        {
            _root.playershipstatus[3][1] = _root.lastplayersuccessfulfunds;
        } // end if
    } // end if
    ++count;
    if (count < 0 || count > 30)
    {
        count = 0;
        _root.func_checkfortoomuchaddedandset();
    } // end if
};
stop ();
