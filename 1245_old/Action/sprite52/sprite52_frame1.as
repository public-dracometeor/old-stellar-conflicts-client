﻿// Action script...

// [Action in Frame 1]
maxsize = 70;
startsize = 20;
sizediff = maxsize - startsize;
startime = getTimer();
durationtime = 250;
timetoend = startime + durationtime;
this.onEnterFrame = function ()
{
    currenttime = getTimer();
    if (timetoend < currenttime)
    {
        removeMovieClip (this);
    }
    else
    {
        size = Math.round(maxsize - sizediff * ((timetoend - currenttime) / durationtime));
        this._height = this._width = size;
    } // end else if
};
stop ();
