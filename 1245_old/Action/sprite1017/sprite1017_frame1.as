﻿// Action script...

// [Action in Frame 1]
function sendouthatshipiishere()
{
    information = "NPC`ESCORT`" + npcnumber + "`" + shiptype + "`" + startx + "`" + starty + "`" + velocity + "`" + endx + "`" + endy + "`" + systemtime;
    _root.mysocket.send(information);
} // End of the function
function movementofanobjectwiththrust()
{
    relativefacing = this.rotation;
    if (relativefacing == 0)
    {
        ymovement = -velocity;
        xmovement = 0;
    } // end if
    if (velocity != 0)
    {
        this.relativefacing = Math.round(this.relativefacing);
        ymovement = Math.round(-velocity * _root.cosines[this.relativefacing]);
        xmovement = Math.round(velocity * _root.sines[this.relativefacing]);
    }
    else
    {
        ymovement = 0;
        xmovement = 0;
    } // end else if
} // End of the function
function deathinfosend()
{
    _root.gamedisplayarea.attachMovie("aimessage", "aimessage", 94324);
    message = "AHHHHHHHHH";
    _root.gamedisplayarea.aimessage.aimessage = message;
    _root.gamedisplayarea.aimessage.ainame = shipname;
    tw = 0;
} // End of the function
function docktheship()
{
    if (playerthehost)
    {
        ainame = "AISHIP";
        message = "Thanks for the escort!";
        _root.postaimessage(message, ainame);
        removeMovieClip (this);
    }
    else
    {
        removeMovieClip (this);
    } // end else if
} // End of the function
function deathinfosend()
{
    datatosend = "AI`death`" + shipid + "`" + String(shipbounty) + "`" + Math.round(shipbounty * _root.playershipstatus[5][6]) + "`" + shipbounty + "~";
    _root.mysocket.send(datatosend);
    for (i = 0; i < _root.aishipshosted.length; i++)
    {
        if (shipid == _root.aishipshosted[i][0])
        {
            _root.aishipshosted.splice(i, 1);
            i = 999999;
        } // end if
    } // end of for
} // End of the function
function myhittest(firstitemx, firstitemy, firsthalfwidth, firsthalfheight, secitemx, secitemy, sechalfwidth, sechalfheight)
{
    xrange = firstitemx - secitemx;
    xhit = false;
    if (xrange <= 0)
    {
        if (firstitemx + firsthalfwidth >= secitemx - sechalfwidth)
        {
            xhit = true;
        } // end if
    }
    else if (firstitemx - firsthalfwidth <= secitemx + sechalfwidth)
    {
        xhit = true;
    } // end else if
    if (xhit == true)
    {
        yrange = firstitemy - secitemy;
        if (yrange < 0)
        {
            if (firstitemy + firsthalfheight >= secitemy - sechalfwidth)
            {
                return (true);
            } // end if
        }
        else if (yrange >= 0)
        {
            if (firstitemy - firsthalfheight <= secitemy + sechalfwidth)
            {
                return (true);
            } // end if
        }
        else
        {
            return (false);
        } // end else if
    }
    else
    {
        return (false);
    } // end else if
} // End of the function
