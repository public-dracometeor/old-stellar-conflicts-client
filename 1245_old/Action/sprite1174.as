﻿// Action script...

// [onClipEvent of sprite 33 in frame 1]
onClipEvent (load)
{
    this.attachMovie("shiptype" + _root.playershipstatus[5][0], "playershipicon", 10);
    if (this.playershipicon._height > 60 || this.playershipicon._width > 60)
    {
        if (this.playershipicon._height > this.playershipicon._width)
        {
            change = this.playershipicon._height - 60;
            this.playershipicon._height = this.playershipicon._height - change;
            this.playershipicon._width = this.playershipicon._width - change;
        }
        else
        {
            change = this.playershipicon._width - 60;
            this.playershipicon._height = this.playershipicon._height - change;
            this.playershipicon._width = this.playershipicon._width - change;
        } // end if
    } // end else if
    this.playershipicon._x = 50;
    this.playershipicon._y = 40;
    playershiptype = _root.playershipstatus[5][0];
    for (i = 0; i < _root.shiptype[playershiptype][2].length; i++)
    {
        if (!isNaN(_root.playershipstatus[0][i][0]))
        {
            this.playershipicon.attachMovie("hardpointactivation on-off", "gun" + i, i);
            this.playershipicon["gun" + i]._x = _root.shiptype[playershiptype][2][i][0];
            this.playershipicon["gun" + i]._y = _root.shiptype[playershiptype][2][i][1];
            this.playershipicon["gun" + i].hardpointno = i;
            if (_root.playershipstatus[0][i][4] == "ON")
            {
                this.playershipicon["gun" + i].gotoAndStop(1);
            }
            else
            {
                this.playershipicon["gun" + i].gotoAndStop(2);
            } // end else if
            this.playershipicon["gun" + i].onRelease = function ()
            {
                if (_root.playershipstatus[0][this.hardpointno][4] == "ON")
                {
                    this.gotoAndStop(2);
                    _root.playershipstatus[0][this.hardpointno][4] = "OFF";
                }
                else if (_root.playershipstatus[0][this.hardpointno][4] == "OFF")
                {
                    this.gotoAndStop(1);
                    _root.playershipstatus[0][this.hardpointno][4] = "ON";
                } // end else if
            };
        } // end if
    } // end of for
}

// [Action in Frame 1]
stop ();

stop ();
