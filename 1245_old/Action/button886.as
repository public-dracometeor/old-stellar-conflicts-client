﻿// Action script...

on (release)
{
    function determinegoods(loadedvars)
    {
        infosplit = loadedvars.split("~");
        newinfo = infosplit[0].split("`");
        for (i = 0; i < newinfo.length - 1; i++)
        {
            currentthread = newinfo[i].split("`");
            if (newinfo[i] == "S")
            {
                baseselling = baseselling + (_root.tradegoods[i][0] + " \r");
            } // end if
            if (newinfo[i] == "B")
            {
                basebuying = basebuying + (_root.tradegoods[i][0] + " \r");
            } // end if
        } // end of for
    } // End of the function
    _parent._parent.destination = labelname;
    playersxcoord = _root.shipcoordinatex;
    playersycoord = _root.shipcoordinatey;
    playerdistancefrompoint = Math.round(Math.sqrt((playersxcoord - xlocation) * (playersxcoord - xlocation) + (playersycoord - ylocation) * (playersycoord - ylocation)));
    _parent._parent.range = "Range: " + playerdistancefrompoint;
    _parent._parent.xrange = xlocation;
    _parent._parent.yrange = ylocation;
    _parent._parent.information = isactive;
    _root.playersdestination[0] = label;
    _root.playersdestination[1] = xlocation;
    _root.playersdestination[2] = ylocation;
    baseselling = "";
    basebuying = "";
    _parent._parent.information = "Loading \r   Information";
    for (jj = 0; jj < _root.starbaselocation.length; jj++)
    {
        if (_root.starbaselocation[jj][0] == labelname)
        {
            baseloc = jj;
            break;
        } // end if
    } // end of for
    if (_root.starbaselocation[baseloc][5] == "ACTIVE" || Number(_root.starbaselocation[baseloc][5]) < getTimer())
    {
        basestatus = "ACTIVE";
    }
    else
    {
        basestatus = "INACTIVE/DESTROYED";
    } // end else if
    newsystemVars = new XML();
    newsystemVars.load(_root.pathtoaccounts + "tradegoods.php?system=" + _root.playershipstatus[5][1] + "&mode=items&starbase=" + label);
    newsystemVars.onLoad = function (success)
    {
        loadedvars = String(newsystemVars);
        if (loadedvars.length > 1)
        {
            determinegoods(loadedvars);
            baseinformation = basestatus + "\r Selling: \r" + baseselling + " \r Buying: \r" + basebuying;
            _parent._parent.information = baseinformation;
        }
        else
        {
            _parent.message = "Failed to load system";
        } // end else if
    };
}
