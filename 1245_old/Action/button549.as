﻿// Action script...

on (release)
{
    _root.musicvolume = "off";
    _root.mainscreenstarbase.func_eriemusic();
    _root.func_minor_clicksound();
    this.value = offbuttonswitch;
    _root.soundvolume = offbuttonswitch;
    if (value == onbuttonswitch)
    {
        this.offbutton._alpha = 60;
        this.onbutton._alpha = 100;
    } // end if
    if (value == offbuttonswitch)
    {
        this.offbutton._alpha = 100;
        this.onbutton._alpha = 60;
    } // end if
}
