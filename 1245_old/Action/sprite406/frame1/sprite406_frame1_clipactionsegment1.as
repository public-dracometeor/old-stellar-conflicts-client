﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    if (itemselection != null)
    {
        generatortype = itemselection.substring(0, 15);
        generatorselectiontype = itemselection.substring(15);
    } // end if
    if (itemselection != null && generatortype == "energygenerator" && buyingagenerator != true && generatorselectiontype != _root.playershipstatus[1][0])
    {
        _parent.test = "ran";
        buyingagenerator = true;
        removeMovieClip (this.hardwarewarningbox);
        attachMovie("hardwareinformationbutton", "buyingageneratorquestion", 9980);
        currentvalueofgenerator = Math.round(_root.energygenerators[_root.playershipstatus[1][0]][2] / 2);
        currentgeneratorupgradecost = _root.energygenerators[generatorselectiontype][2] - currentvalueofgenerator;
        this.buyingageneratorquestion.question = "Buy: " + _root.energygenerators[generatorselectiontype][1] + " \r" + "For: " + currentgeneratorupgradecost;
    } // end if
    if (this.buyingageneratorquestion.requestedanswer == true && buyingagenerator == true)
    {
        i = generatorselectiontype;
        currentvalueofgenerator = _root.energygenerators[_root.playershipstatus[1][0]][2] / 2;
        currentgeneratorupgradecost = _root.energygenerators[i][2] - currentvalueofgenerator;
        if (_root.playershipstatus[3][1] >= currentgeneratorupgradecost)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - currentgeneratorupgradecost;
            buyingagenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
            _root.playershipstatus[1][0] = generatorselectiontype;
            hardwareshopmenulabelenergyclose();
            this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
            setProperty("shipblueprint", _x, _root.gameareawidth / 8);
            setProperty("shipblueprint", _y, _root.gameareaheight / 8);
            redrawscreen = true;
        }
        else
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 9990);
            this.hardwarewarningbox.information = "You Do Not Have Enough Funds";
            buyingagenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
        } // end if
    } // end else if
    if (this.buyingageneratorquestion.requestedanswer == false && buyingagenerator == true)
    {
        buyingagenerator = false;
        removeMovieClip (this.buyingageneratorquestion);
        itemselection = null;
    } // end if
    if (itemselection != null)
    {
        generatortype = itemselection.substring(0, 15);
        generatorselectiontype = itemselection.substring(15);
    } // end if
    if (itemselection != null && generatortype == "shieldgenerator" && buyingashieldgenerator != true && generatorselectiontype != _root.playershipstatus[2][0])
    {
        buyingashieldgenerator = true;
        removeMovieClip (this.hardwarewarningbox);
        attachMovie("hardwareinformationbutton", "buyingageneratorquestion", 9980);
        currentvalueofgenerator = Math.round(_root.shieldgenerators[_root.playershipstatus[2][0]][5] / 2);
        currentgeneratorupgradecost = _root.shieldgenerators[generatorselectiontype][5] - currentvalueofgenerator;
        this.buyingageneratorquestion.question = "Buy: " + _root.shieldgenerators[generatorselectiontype][4] + " \r" + "For: " + currentgeneratorupgradecost;
    } // end if
    if (this.buyingageneratorquestion.requestedanswer == true && buyingashieldgenerator == true)
    {
        i = generatorselectiontype;
        currentvalueofgenerator = _root.shieldgenerators[_root.playershipstatus[2][0]][5] / 2;
        currentgeneratorupgradecost = _root.shieldgenerators[i][5] - currentvalueofgenerator;
        rec = _root.energygenerators[_root.playershipstatus[1][0]][0];
        dra = _root.shieldgenerators[i][2] + _root.shieldgenerators[i][3];
        if (rec < dra)
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 9990);
            this.hardwarewarningbox.information = "Shields Will Use Too Much Power, Need a Bigger Energy Generator First";
            buyingashieldgenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
        }
        else if (_root.playershipstatus[3][1] >= currentgeneratorupgradecost)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - currentgeneratorupgradecost;
            buyingashieldgenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
            _root.playershipstatus[2][0] = generatorselectiontype;
            hardwareshopmenulabelshieldgeneratorclose();
            this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
            setProperty("shipblueprint", _x, _root.gameareawidth / 8);
            setProperty("shipblueprint", _y, _root.gameareaheight / 8);
            redrawscreen = true;
        }
        else
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 9990);
            this.hardwarewarningbox.information = "You Do Not Have Enough Funds";
            buyingashieldgenerator = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
        } // end else if
    } // end else if
    if (this.buyingageneratorquestion.requestedanswer == false && buyingashieldgenerator == true)
    {
        buyingashieldgenerator = false;
        removeMovieClip (this.buyingageneratorquestion);
        itemselection = null;
    } // end if
    if (itemselection != null)
    {
        generatortype = itemselection.substring(0, 15);
        generatorselectiontype = itemselection.substring(15);
    } // end if
    if (itemselection != null && generatortype == "energycapacitor" && buyingaenergycapacitor != true && generatorselectiontype != _root.playershipstatus[1][5])
    {
        _parent.test = "ran";
        buyingaenergycapacitor = true;
        removeMovieClip (this.hardwarewarningbox);
        attachMovie("hardwareinformationbutton", "buyingageneratorquestion", 9980);
        currentvalueofgenerator = Math.round(_root.energycapacitors[_root.playershipstatus[1][5]][2] / 2);
        currentgeneratorupgradecost = _root.energycapacitors[generatorselectiontype][2] - currentvalueofgenerator;
        this.buyingageneratorquestion.question = "Buy: " + _root.energycapacitors[generatorselectiontype][1] + " \r" + "For: " + currentgeneratorupgradecost;
    } // end if
    if (this.buyingageneratorquestion.requestedanswer == true && buyingaenergycapacitor == true)
    {
        i = generatorselectiontype;
        currentvalueofgenerator = _root.energycapacitors[_root.playershipstatus[1][5]][2] / 2;
        currentgeneratorupgradecost = _root.energycapacitors[i][2] - currentvalueofgenerator;
        if (_root.playershipstatus[3][1] >= currentgeneratorupgradecost)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - currentgeneratorupgradecost;
            buyingaenergycapacitor = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
            _root.playershipstatus[1][5] = generatorselectiontype;
            hardwareshopmenulabelenergycapacitorsclose();
            this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
            setProperty("shipblueprint", _x, _root.gameareawidth / 8);
            setProperty("shipblueprint", _y, _root.gameareaheight / 8);
            redrawscreen = true;
        }
        else
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 9990);
            this.hardwarewarningbox.information = "You Do Not Have Enough Funds";
            buyingaenergycapacitor = false;
            itemselection = null;
            removeMovieClip (this.buyingageneratorquestion);
        } // end if
    } // end else if
    if (this.buyingageneratorquestion.requestedanswer == false && buyingagenerator == true)
    {
        buyingaenergycapacitor = false;
        removeMovieClip (this.buyingageneratorquestion);
        itemselection = null;
    } // end if
}
