﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (mouseDown)
{
    if (currentoperation == "hardwareshopmenulabelguns")
    {
        i = 0;
        _root.attachMovie("radarblot", "testdot", 99999);
        setProperty("_root.testdot", _x, _root._xmouse);
        setProperty("_root.testdot", _y, _root._ymouse);
        while (i < _root.shiptype[playershiptype][2].length && buyingagun != true)
        {
            if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracket" + i) && sellingagun == false)
            {
                buyingagun = false;
                removeMovieClip (buyagunquestion);
                removeMovieClip (hardwarewarningbox);
                startDrag ("shipblueprint.gun" + i, false);
                hardpointmovedfrom = i;
            } // end if
            ++i;
        } // end while
        removeMovieClip (_root.testdot);
    } // end if
    if (currentoperation == "hardwareshopmenulabelturrets")
    {
        i = 0;
        _root.attachMovie("radarblot", "testdot", 99999);
        setProperty("_root.testdot", _x, _root._xmouse);
        setProperty("_root.testdot", _y, _root._ymouse);
        while (i < _root.shiptype[playershiptype][5].length && buyingaturret != true)
        {
            if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracket" + i) && sellingaturret == false)
            {
                buyingaturret = false;
                removeMovieClip (buyagunquestion);
                removeMovieClip (hardwarewarningbox);
                startDrag ("shipblueprint.gun" + i, false);
                hardpointmovedfrom = i;
            } // end if
            ++i;
        } // end while
        removeMovieClip (_root.testdot);
    } // end if
}
