﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (mouseUp)
{
    if (currentoperation == "hardwareshopmenulabelguns")
    {
        stopDrag ();
        _root.attachMovie("radarblot", "testdot", 99999);
        setProperty("_root.testdot", _x, _root._xmouse);
        setProperty("_root.testdot", _y, _root._ymouse);
        i = 0;
        hardpointmoved = false;
        while (i < _root.shiptype[playershiptype][2].length && sellingagun == false && buyingagun != true)
        {
            if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracket" + i))
            {
                if (i != hardpointselected)
                {
                    hardpointmoved = true;
                    hardpointmovedinto = i;
                    extraplayersgunarray = _root.playershipstatus[0].length;
                    _root.playershipstatus[0][extraplayersgunarray] = _root.playershipstatus[0][hardpointmovedfrom];
                    _root.playershipstatus[0][hardpointmovedfrom] = _root.playershipstatus[0][hardpointmovedinto];
                    _root.playershipstatus[0][hardpointmovedinto] = _root.playershipstatus[0][extraplayersgunarray];
                    _root.playershipstatus[0].splice(extraplayersgunarray, 1);
                    for (i = 0; i < _root.playershipstatus[0].length; i++)
                    {
                        this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[0][i][0], "gun" + i, 12 + i);
                        setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
                        setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
                        set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[0][i][0]][6]);
                    } // end of for
                } // end if
            } // end if
            ++i;
        } // end while
        i = hardpointmovedfrom;
        if (_root.testdot.hitTest(this + ".shipblueprint.hardwarescreengunbracketsell") && _root.playershipstatus[0][hardpointmovedfrom][0] != "none")
        {
            hardpointmoved = true;
            sellingagun = true;
            attachMovie("hardwareinformationbutton", "hardwareinformationbutton", 99998);
            this.hardwareinformationbutton.question = "Sell: " + _root.guntype[_root.playershipstatus[0][hardpointmovedfrom][0]][6] + " \r" + "For: " + Math.round(_root.guntype[_root.playershipstatus[0][hardpointmovedfrom][0]][5] / 2);
        } // end if
        if (hardpointmoved != true && sellingagun == false)
        {
            setProperty("shipblueprint.gun" + hardpointmovedfrom, _x, _root.shiptype[playershiptype][2][hardpointmovedfrom][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + hardpointmovedfrom, _y, _root.shiptype[playershiptype][2][hardpointmovedfrom][1] * gunlocationmultiplier);
        } // end if
        removeMovieClip (_root.testdot);
    } // end if
    if (currentoperation == "hardwareshopmenulabelturrets")
    {
        turretsmouseup();
    } // end if
}
