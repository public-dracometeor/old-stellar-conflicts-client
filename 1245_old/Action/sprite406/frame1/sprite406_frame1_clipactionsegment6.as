﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    function drawarrows(islefton, isrighton)
    {
        this.attachMovie("hardwareselectleft", "hardwareselectleft", 1001);
        this.hardwareselectleft._x = shopitemsx - 80;
        this.hardwareselectleft._y = shopitemsy;
        this.hardwareselectleft._alpha = 60;
        if (islefton == true)
        {
            this.hardwareselectleft._alpha = 100;
        } // end if
        this.attachMovie("hardwareselectright", "hardwareselectright", 1002);
        this.hardwareselectright._x = shopitemsx + 80 + shopitemspacingx * (maxitemstostoshow - 1);
        this.hardwareselectright._y = shopitemsy;
        this.hardwareselectright._alpha = 60;
        if (isrighton == true)
        {
            this.hardwareselectright._alpha = 100;
        } // end if
    } // End of the function
    function hardwareshopmenulabelturretsmenuopen()
    {
        turretcostmodifier = 2;
        gunlocationmultiplier = _root.shiptype[playershiptype][3][4];
        shipstopgun = _root.shiptype[playershiptype][6][3] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < shipstopgun - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < shipstopgun; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.guntype[currentleftmostitem + i][6]);
            set(this + ".hardwareitembox" + i + ".itemtype", "turrettype" + (currentleftmostitem + i));
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + _root.guntype[currentleftmostitem + i][5] * turretcostmodifier);
            set(this + ".hardwareitembox" + i + ".itemspecs", "Damage: " + _root.guntype[currentleftmostitem + i][4] + " \r" + "Energy Drain: " + _root.guntype[currentleftmostitem + i][3] + " \r" + "Reload :" + _root.guntype[currentleftmostitem + i][2] + "/s \r" + "Life Time: " + _root.guntype[currentleftmostitem + i][1] + "/s \r" + "Speed :" + _root.guntype[currentleftmostitem + i][0] + "/s \r");
        } // end of for
        for (i = 0; i < _root.shiptype[playershiptype][5].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracket" + i, 5 + i);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _x, _root.shiptype[playershiptype][5][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _y, _root.shiptype[playershiptype][5][i][1] * gunlocationmultiplier);
            set(this.shipblueprint + ".hardwarescreengunbracket" + i + ".hardpointname", "Turret " + i);
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracketsell", 4);
        setProperty("shipblueprint.hardwarescreengunbracketsell", _x, _root.gameareawidth / 2 - 100);
        setProperty("shipblueprint.hardwarescreengunbracketsell", _y, -100);
        set(this.shipblueprint + ".hardwarescreengunbracketsell.hardpointname", "Sell Turret");
        for (i = 0; i < _root.shiptype[playershiptype][5].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[8][i][0], "gun" + i, 12 + i);
            setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][5][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][5][i][1] * gunlocationmultiplier);
            set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[8][i][0]][6]);
        } // end of for
        itemselection = null;
        buyingaturret = false;
        sellingaturret = false;
    } // End of the function
    function hardwareshopmenulabelturretsmenuclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip ("hardwareinformationbutton");
        removeMovieClip ("buyaturretquestion");
        for (i = 0; i < _root.guntype.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
        itemselection = null;
        buyingaturret = false;
        sellingaturret = false;
    } // End of the function
    function hardwareshopmenulabelgunsmenuopen(currentleftmostitem)
    {
        gunlocationmultiplier = _root.shiptype[playershiptype][3][4];
        shipstopgun = _root.shiptype[playershiptype][6][3] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < shipstopgun - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < shipstopgun; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.guntype[currentleftmostitem + i][6]);
            set(this + ".hardwareitembox" + i + ".itemtype", "guntype" + (currentleftmostitem + i));
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + _root.guntype[currentleftmostitem + i][5]);
            set(this + ".hardwareitembox" + i + ".itemspecs", "Damage: " + _root.guntype[currentleftmostitem + i][4] + " \r" + "Energy Drain: " + _root.guntype[currentleftmostitem + i][3] + " \r" + "Reload :" + _root.guntype[currentleftmostitem + i][2] + "/s \r" + "Life Time: " + _root.guntype[currentleftmostitem + i][1] + "/s \r" + "Speed :" + _root.guntype[currentleftmostitem + i][0] + "/s \r");
        } // end of for
        for (i = 0; i < _root.shiptype[playershiptype][2].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracket" + i, 5 + i);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
            set(this.shipblueprint + ".hardwarescreengunbracket" + i + ".hardpointname", "Hardpoint " + i);
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracketsell", 4);
        setProperty("shipblueprint.hardwarescreengunbracketsell", _x, _root.gameareawidth / 2 - 100);
        setProperty("shipblueprint.hardwarescreengunbracketsell", _y, -100);
        set(this.shipblueprint + ".hardwarescreengunbracketsell.hardpointname", "Sell Weapon");
        for (i = 0; i < _root.shiptype[playershiptype][2].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[0][i][0], "gun" + i, 12 + i);
            setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
            set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[0][i][0]][6]);
        } // end of for
        buyingagun = false;
        sellingagun = false;
    } // End of the function
    function hardwareshopmenulabelgunsmenuclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip ("hardwareinformationbutton");
        removeMovieClip ("buyagunquestion");
        for (i = 0; i < _root.guntype.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
        itemselection = null;
        buyingagun = false;
        sellingagun = false;
    } // End of the function
    function hardwareshopmenulabelmissilesopen(currentleftmostitem)
    {
        gunlocationmultiplier = _root.shiptype[playershiptype][3][4];
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < _root.missile.length - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < _root.missile.length; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.missile[currentleftmostitem + i][6]);
            set(this + ".hardwareitembox" + i + ".itemtype", "missiletype" + (currentleftmostitem + i));
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + _root.missile[currentleftmostitem + i][5]);
            if (_root.missile[currentleftmostitem + i][8] == "DUMBFIRE" || _root.missile[currentleftmostitem + i][8] == "SEEKER")
            {
                if (_root.missile[currentleftmostitem + i][8] == "DUMBFIRE")
                {
                    missileguidance = "Dumbfire";
                }
                else if (_root.missile[currentleftmostitem + i][8] == "SEEKER")
                {
                    missileguidance = "Seeker";
                } // end else if
                set(this + ".hardwareitembox" + i + ".itemspecs", "Damage: " + _root.missile[currentleftmostitem + i][4] + " \r" + "Reload :" + _root.missile[currentleftmostitem + i][3] / 1000 + "/s \r" + "Life Time: " + _root.missile[currentleftmostitem + i][2] / 1000 + "/s \r" + "Speed :" + _root.missile[currentleftmostitem + i][0] + "/s \r" + "Guidance: " + missileguidance);
            } // end if
            if (_root.missile[currentleftmostitem + i][8] == "EMP" || _root.missile[currentleftmostitem + i][8] == "DISRUPTER")
            {
                if (_root.missile[currentleftmostitem + i][8] == "EMP")
                {
                    missileguidance = "E.M.P.";
                }
                else if (_root.missile[currentleftmostitem + i][8] == "DISRUPTER")
                {
                    missileguidance = "Disrupter";
                } // end else if
                set(this + ".hardwareitembox" + i + ".itemspecs", "Reload :" + _root.missile[currentleftmostitem + i][3] / 1000 + "/s \r" + "Life Time: " + _root.missile[currentleftmostitem + i][2] / 1000 + "/s \r" + "Speed :" + _root.missile[currentleftmostitem + i][0] + "/s \r" + "Duration: " + Math.floor(_root.missile[currentleftmostitem + i][9] / 1000) + " \r" + "Guidance: " + missileguidance);
            } // end if
        } // end of for
        for (i = 0; i < _root.shiptype[playershiptype][7].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreengunbracket", "hardwarescreengunbracket" + i, 5 + i);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _x, _root.shiptype[playershiptype][7][i][2] * gunlocationmultiplier);
            setProperty("shipblueprint.hardwarescreengunbracket" + i, _y, _root.shiptype[playershiptype][7][i][3] * gunlocationmultiplier);
            set(this.shipblueprint + ".hardwarescreengunbracket" + i + ".hardpointname", "Missile Bank " + i);
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreenmissilebankinfo", "hardwarescreenmissilebankinfo", 4);
        setProperty("shipblueprint.hardwarescreenmissilebankinfo", _x, _root.gameareawidth / 2 - 100);
        setProperty("shipblueprint.hardwarescreenmissilebankinfo", _y, -100);
        set(this.shipblueprint + ".hardwarescreenmissilebankinfo.bankno", currentmissilebank);
        for (i = 0; i < _root.shiptype[playershiptype][7].length; i++)
        {
            this.shipblueprint.attachMovie("hardwarescreenmissilebankicon", "missilebank" + i, 12 + i);
            setProperty("shipblueprint.missilebank" + i, _x, _root.shiptype[playershiptype][7][i][2] * gunlocationmultiplier);
            setProperty("shipblueprint.missilebank" + i, _y, _root.shiptype[playershiptype][7][i][3] * gunlocationmultiplier);
            set("shipblueprint.missilebank" + i + ".itemname", "Bank " + i);
            set("shipblueprint.missilebank" + i + ".bankno", i);
            noofmissiles = 0;
            bankno = i;
            for (gg = 0; gg < _root.missile.length; gg++)
            {
                if (_root.playershipstatus[7][bankno][4][gg] > 0)
                {
                    noofmissiles = noofmissiles + _root.playershipstatus[7][bankno][4][gg];
                } // end if
            } // end of for
            set("shipblueprint.missilebank" + i + ".qty", "Missiles\r(" + noofmissiles + "/" + _root.playershipstatus[7][bankno][5] + ")");
        } // end of for
    } // End of the function
    function hardwareshopmenulabelmissilesclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip ("hardwareinformationbutton");
        removeMovieClip ("buyagunquestion");
        for (i = 0; i < _root.guntype.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
        itemselection = null;
        buyingamissile = false;
    } // End of the function
    function hardwareshopmenulabelenergygeneratoropen(currentleftmostitem)
    {
        topenergygen = _root.shiptype[playershiptype][6][2] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < topenergygen - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < topenergygen; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.energygenerators[currentleftmostitem + i][1]);
            set(this + ".hardwareitembox" + i + ".itemtype", "energygenerator" + (currentleftmostitem + i));
            currentvalueofgenerator = Math.round(_root.energygenerators[_root.playershipstatus[1][0]][2] / 2);
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + String(_root.energygenerators[currentleftmostitem + i][2] - currentvalueofgenerator));
            if (currentleftmostitem + i == _root.playershipstatus[1][0])
            {
                set(this + ".hardwareitembox" + i + ".itemcost", "OWNED");
            } // end if
            set(this + ".hardwareitembox" + i + ".itemspecs", "\rRecharge: " + _root.energygenerators[currentleftmostitem + i][0] + "/sec \r");
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "generatorbracket", 99);
        setProperty("shipblueprint.generatorbracket", _x, 0);
        setProperty("shipblueprint.generatorbracket", _y, 0);
        set(this.shipblueprint + ".generatorbracket.hardpointname", "Generator");
        this.shipblueprint.generatorbracket.attachMovie("hardwarescreengunbracketenergygen0", "generator", 99);
    } // End of the function
    function hardwareshopmenulabelenergygeneratorclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip (buyingageneratorquestion);
        buyingagenerator = false;
        removeMovieClip (this.hardwarewarningbox);
        for (i = 0; i < _root.energygenerators.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
    } // End of the function
    function hardwareshopmenulabelshieldgeneratoropen(currentleftmostitem)
    {
        topshieldgen = _root.shiptype[playershiptype][6][0] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < topshieldgen - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < topshieldgen; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.shieldgenerators[currentleftmostitem + i][4]);
            set(this + ".hardwareitembox" + i + ".itemtype", "shieldgenerator" + (currentleftmostitem + i));
            currentvalueofgenerator = Math.round(_root.shieldgenerators[_root.playershipstatus[2][0]][5] / 2);
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + String(_root.shieldgenerators[currentleftmostitem + i][5] - currentvalueofgenerator));
            if (currentleftmostitem + i == _root.playershipstatus[2][0])
            {
                set(this + ".hardwareitembox" + i + ".itemcost", "OWNED");
            } // end if
            set(this + ".hardwareitembox" + i + ".itemspecs", "Recharge: " + _root.shieldgenerators[currentleftmostitem + i][1] + " \r" + "Energy Drain/second: \r" + "Unit: " + _root.shieldgenerators[currentleftmostitem + i][2] + " \r" + "Full Charge: " + _root.shieldgenerators[currentleftmostitem + i][3] + " \r");
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "generatorbracket", 99);
        setProperty("shipblueprint.generatorbracket", _x, 0);
        setProperty("shipblueprint.generatorbracket", _y, 0);
        set(this.shipblueprint + ".generatorbracket.hardpointname", "Generator");
        this.shipblueprint.generatorbracket.attachMovie("hardwarescreengunbracketshieldtype0", "shieldgen", 1);
    } // End of the function
    function hardwareshopmenulabelshieldgeneratorclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip (buyingageneratorquestion);
        buyingashieldgenerator = false;
        removeMovieClip (this.hardwarewarningbox);
        for (i = 0; i < _root.energygenerators.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
    } // End of the function
    function hardwareshopmenulabelenergycapacitorsopen(currentleftmostitem)
    {
        topenergycap = _root.shiptype[playershiptype][6][1] + 1;
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < topenergycap - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < topenergycap; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", _root.energycapacitors[currentleftmostitem + i][1]);
            set(this + ".hardwareitembox" + i + ".itemtype", "energycapacitor" + (currentleftmostitem + i));
            currentvalueofgenerator = Math.round(_root.energycapacitors[_root.playershipstatus[1][5]][2] / 2);
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + String(_root.energycapacitors[currentleftmostitem + i][2] - currentvalueofgenerator));
            if (currentleftmostitem + i == _root.playershipstatus[1][5])
            {
                set(this + ".hardwareitembox" + i + ".itemcost", "OWNED");
            } // end if
            set(this + ".hardwareitembox" + i + ".itemspecs", "\rMax Charge: " + _root.energycapacitors[currentleftmostitem + i][0] + " \r");
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreengunbracket", "generatorbracket", 99);
        setProperty("shipblueprint.generatorbracket", _x, 0);
        setProperty("shipblueprint.generatorbracket", _y, 0);
        set(this.shipblueprint + ".generatorbracket.hardpointname", "Capacitor");
        this.shipblueprint.generatorbracket.attachMovie("hardwarescreengunbracketenergycap0", "generator", 99);
    } // End of the function
    function hardwareshopmenulabelenergycapacitorsclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip (buyingageneratorquestion);
        buyingaenergycapacitor = false;
        removeMovieClip (this.hardwarewarningbox);
        for (i = 0; i < _root.energycapacitors.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
    } // End of the function
    if (operation != null)
    {
        this.hardwareshopmenulabelguns._alpha = 75;
        this.hardwareshopmenulabelmissiles._alpha = 75;
        this.hardwareshopmenulabelshield._alpha = 75;
        this.hardwareshopmenulabelenergy._alpha = 75;
        this.hardwareshopmenulabelcapacitors._alpha = 75;
        this.hardwareshopmenulabelturrets._alpha = 75;
        this.hardwareshopmenulabelspecials._alpha = 75;
        hardwareshopmenulabelspecialsmenuclose();
        hardwareshopmenulabelgunsmenuclose();
        hardwareshopmenulabelmissilesclose();
        hardwareshopmenulabelenergygeneratorclose();
        hardwareshopmenulabelshieldgeneratorclose();
        hardwareshopmenulabelenergycapacitorsclose();
        hardwareshopmenulabelturretsmenuclose();
        this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
        setProperty("shipblueprint", _x, _root.gameareawidth / 8);
        setProperty("shipblueprint", _y, _root.gameareaheight / 8);
        islefton = false;
        isrighton = false;
        if (operation == "hardwareshopmenulabelguns")
        {
            this.hardwareshopmenulabelguns._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelgunsmenuopen(currentleftmostitem);
            if (_root.guntype.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelspecials")
        {
            this.hardwareshopmenulabelspecials._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelspecialsmenuopen(currentleftmostitem);
        } // end if
        if (operation == "hardwareshopmenulabelmissiles")
        {
            this.hardwareshopmenulabelmissiles._alpha = 100;
            buyingamissile = false;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            currentmissilebank = 0;
            hardwareshopmenulabelmissilesopen(currentleftmostitem);
            if (_root.missile.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelturrets")
        {
            this.hardwareshopmenulabelturrets._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelturretsmenuopen(currentleftmostitem);
            if (_root.guntype.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelenergy")
        {
            this.hardwareshopmenulabelenergy._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelenergygeneratoropen(currentleftmostitem);
            if (_root.energygenerators.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelshield")
        {
            this.hardwareshopmenulabelshield._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelshieldgeneratoropen(currentleftmostitem);
            if (_root.shieldgenerators.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        if (operation == "hardwareshopmenulabelcapacitors")
        {
            this.hardwareshopmenulabelcapacitors._alpha = 100;
            currentleftmostitem = 0;
            lasttleftmostitem = 0;
            hardwareshopmenulabelenergycapacitorsopen(currentleftmostitem);
            if (_root.energycapacitors.length > 3)
            {
                isrighton = true;
            } // end if
        } // end if
        currentoperation = operation;
        operation = null;
        redrawscreen = false;
        drawarrows(islefton, isrighton);
    } // end if
    if (moveitemsdisplayed != null)
    {
        currentleftmostitem = currentleftmostitem + moveitemsdisplayed;
        moveitemsdisplayed = null;
    } // end if
    if (currentoperation != null && (redrawscreen == true || currentleftmostitem != lasttleftmostitem))
    {
        if (currentoperation == "hardwareshopmenulabelguns")
        {
            hardwareshopmenulabelgunsmenuopen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelmissiles")
        {
            hardwareshopmenulabelmissilesopen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelspecials")
        {
            hardwareshopmenulabelspecialsmenuopen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelturrets")
        {
            hardwareshopmenulabelturretsmenuopen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelenergy")
        {
            hardwareshopmenulabelenergygeneratoropen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelshield")
        {
            hardwareshopmenulabelshieldgeneratoropen(currentleftmostitem);
        } // end if
        if (currentoperation == "hardwareshopmenulabelcapacitors")
        {
            hardwareshopmenulabelenergycapacitorsopen(currentleftmostitem);
        } // end if
        redrawscreen = false;
        lasttleftmostitem = currentleftmostitem;
    } // end if
}
