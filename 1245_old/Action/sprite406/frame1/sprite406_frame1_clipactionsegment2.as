﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    if (itemselection != null)
    {
        missiletype = itemselection.substring(0, 11);
        missileselected = itemselection.substring(11);
    } // end if
    if (itemselection != null && missiletype == "missiletype" && buyingamissile != true)
    {
        buyingamissile = true;
        removeMovieClip (this.hardwarewarningbox);
        attachMovie("hardwaremissileinformationbutton", "buyingamissilequestion", 9980);
        this.buyingamissilequestion.missileselected = Number(missileselected);
        this.buyingamissilequestion.bankno = Number(currentmissilebank);
        for (qq = 0; qq < _root.shiptype[playershiptype][7].length; qq++)
        {
            this.shipblueprint["missilebank" + qq]._visible = false;
        } // end of for
        buyingamissile = false;
        itemselection = null;
    } // end if
}
