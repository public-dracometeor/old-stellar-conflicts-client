﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (load)
{
    turretcostmodifier = 2;
    removeMovieClip ("_root.starbasehardwarebutton");
    removeMovieClip ("_root.exitstarbasebutton");
    removeMovieClip ("_root.purchaseshipbutton");
    removeMovieClip ("_root.purchasetradegoodsbutton");
    moveitemsdisplayed = null;
    itemselection = null;
    playershiptype = _root.playershipstatus[5][0];
    sellingagun = false;
    leftopbarx = -_root.gameareawidth / 3;
    leftopbary = -_root.gameareaheight / 8;
    leftbarspacing = 42;
    shopitemsx = leftopbarx + 200;
    shopitemsy = leftopbary - 100;
    shopitemspacingx = 125;
    maxitemstostoshow = 3;
    this._y = _root.gameareaheight / 2;
    this._x = _root.gameareawidth / 2;
    this._height = _root.gameareaheight;
    this._width = _root.gameareawidth;
    playershiptype = _root.playershipstatus[5][0];
    this.attachMovie("shiptype" + playershiptype + "blueprint", "shipblueprint", 1);
    setProperty("shipblueprint", _x, _root.gameareawidth / 8);
    setProperty("shipblueprint", _y, _root.gameareaheight / 8);
    if (_root.shiptype[playershiptype][5].length > 0)
    {
        this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelturrets", 98);
        this.hardwareshopmenulabelturrets.itemname = "Turrets";
        this.hardwareshopmenulabelturrets.operation = "hardwareshopmenulabelturrets";
        this.hardwareshopmenulabelturrets._y = leftopbary - (leftbarspacing + leftbarspacing);
        this.hardwareshopmenulabelturrets._x = leftopbarx;
    } // end if
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelspecials", 99);
    this.hardwareshopmenulabelspecials.itemname = "Specials";
    this.hardwareshopmenulabelspecials.operation = "hardwareshopmenulabelspecials";
    this.hardwareshopmenulabelspecials._y = leftopbary - leftbarspacing;
    this.hardwareshopmenulabelspecials._x = leftopbarx;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelguns", 100);
    this.hardwareshopmenulabelguns.itemname = "Weapons/Guns";
    this.hardwareshopmenulabelguns.operation = "hardwareshopmenulabelguns";
    this.hardwareshopmenulabelguns._y = leftopbary;
    this.hardwareshopmenulabelguns._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelmissiles", 106);
    this.hardwareshopmenulabelmissiles.itemname = "Missiles";
    this.hardwareshopmenulabelmissiles.operation = "hardwareshopmenulabelmissiles";
    this.hardwareshopmenulabelmissiles._y = leftopbary;
    this.hardwareshopmenulabelmissiles._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelshield", 101);
    this.hardwareshopmenulabelshield.itemname = "Shield Generators";
    this.hardwareshopmenulabelshield.operation = "hardwareshopmenulabelshield";
    this.hardwareshopmenulabelshield._y = leftopbary;
    this.hardwareshopmenulabelshield._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelenergy", 102);
    this.hardwareshopmenulabelenergy.itemname = "Energy Generators";
    this.hardwareshopmenulabelenergy.operation = "hardwareshopmenulabelenergy";
    this.hardwareshopmenulabelenergy._y = leftopbary;
    this.hardwareshopmenulabelenergy._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("hardwareshopmenulabel", "hardwareshopmenulabelcapacitors", 103);
    this.hardwareshopmenulabelcapacitors.itemname = "Energy Capacitors";
    this.hardwareshopmenulabelcapacitors.operation = "hardwareshopmenulabelcapacitors";
    this.hardwareshopmenulabelcapacitors._y = leftopbary;
    this.hardwareshopmenulabelcapacitors._x = leftopbarx;
    leftopbary = leftopbary + leftbarspacing;
    this.attachMovie("shopexitbutton", "hardwareshopexitbutton", 120);
    this.hardwareshopexitbutton._x = _root.gameareawidth / 2 - 50;
    this.hardwareshopexitbutton._y = _root.gameareaheight / 2 - 70;
    this.attachMovie("starbasefundsdisplay", "funds", 121);
    this.funds._y = -_root.gameareaheight / 2 + 15;
    this.funds._x = _root.gameareawidth / 2 - 100;
    currentleftmostitem = 0;
    lasttleftmostitem = 0;
    operation = "hardwareshopmenulabelguns";
}
