﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (load)
{
    function hardwareshopmenulabelspecialsmenuopen(currentleftmostitem)
    {
        playershiptype = _root.playershipstatus[5][0];
        availableitems = new Array();
        _root.shiptype[currentship][3][8] = false;
        for (q = 0; q < _root.specialshipitems.length; q++)
        {
            if (_root.specialshipitems[q][5] == "CLOAK")
            {
                if (_root.shiptype[playershiptype][3][8] == true)
                {
                    availableitems[availableitems.length] = _root.specialshipitems[q];
                } // end if
                continue;
            } // end if
            availableitems[availableitems.length] = _root.specialshipitems[q];
        } // end of for
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < availableitems.length - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        drawarrows(islefton, isrighton);
        for (i = 0; i < maxitemstostoshow && i < availableitems.length; i++)
        {
            this.attachMovie("hardwareitembox", "hardwareitembox" + i, 5000 + i);
            setProperty("hardwareitembox" + i, _x, shopitemsx + shopitemspacingx * i);
            setProperty("hardwareitembox" + i, _y, shopitemsy);
            set(this + ".hardwareitembox" + i + ".itemname", availableitems[currentleftmostitem + i][0]);
            set(this + ".hardwareitembox" + i + ".itemtype", "specialtype" + availableitems[currentleftmostitem + i][7]);
            set(this + ".hardwareitembox" + i + ".itemcost", "Cost:" + availableitems[currentleftmostitem + i][6]);
            if (availableitems[currentleftmostitem + i][5] == "FLARE")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Drain: " + availableitems[currentleftmostitem + i][2] + " \r" + "Reload :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r" + "Life Time: " + availableitems[currentleftmostitem + i][3] / 1000 + "/s \r" + "%Chance :" + availableitems[currentleftmostitem + i][10] * 100 + "% \r");
            }
            else if (availableitems[currentleftmostitem + i][5] == "MINES")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Reload :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r" + "Life Time: " + availableitems[currentleftmostitem + i][3] / 1000 + "/s \r" + "Damage :" + availableitems[currentleftmostitem + i][12] + " \r" + "Not Effective on Ai");
            }
            else if (availableitems[currentleftmostitem + i][5] == "DETECTOR" || availableitems[currentleftmostitem + i][5] == "STEALTH" || availableitems[currentleftmostitem + i][5] == "CLOAK")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Drain: " + availableitems[currentleftmostitem + i][2] + " \r" + "ReCharge :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r" + "Life Time: " + availableitems[currentleftmostitem + i][3] / 1000 + "/s \r" + "%Chance :" + availableitems[currentleftmostitem + i][10] * 100 + "% \r" + "CheckTime: " + availableitems[currentleftmostitem + i][11] / 1000 + "/s");
            }
            else if (availableitems[currentleftmostitem + i][5] == "PULSAR")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Need: " + availableitems[currentleftmostitem + i][2] + " \r" + "Shutdown Time: " + availableitems[currentleftmostitem + i][11] / 1000 + " s\r" + "Radius :" + availableitems[currentleftmostitem + i][12] + " \r" + "QTY: " + availableitems[currentleftmostitem + i][1]);
            } // end else if
            if (availableitems[currentleftmostitem + i][5] == "RECHARGESHIELD")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Conversion: \r 1 Energy:" + availableitems[currentleftmostitem + i][2] + " Shield \r" + "ReCharge :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r");
            } // end if
            if (availableitems[currentleftmostitem + i][5] == "RECHARGESTRUCT")
            {
                set(this + ".hardwareitembox" + i + ".itemspecs", "Energy Conversion: \r 1 Energy:" + availableitems[currentleftmostitem + i][2] + " Struct \r" + "ReCharge :" + availableitems[currentleftmostitem + i][4] / 1000 + "/s \r");
            } // end if
        } // end of for
        this.shipblueprint.attachMovie("hardwarescreenspecialsinfo", "hardwarescreenspecialsinfo", 4);
        setProperty("shipblueprint.hardwarescreenspecialsinfo", _x, _root.gameareawidth / 2 - 100);
        setProperty("shipblueprint.hardwarescreenspecialsinfo", _y, -100);
    } // End of the function
    function hardwareshopmenulabelspecialsclose()
    {
        removeMovieClip (shipblueprint);
        removeMovieClip ("hardwareinformationbutton");
        removeMovieClip ("buyingaspecialquestion");
        for (i = 0; i < _root.guntype.length; i++)
        {
            removeMovieClip ("hardwareitembox" + i);
        } // end of for
        itemselection = null;
        buyingaspecial = false;
    } // End of the function
}
