﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    if (itemselection != null)
    {
        specialtype = itemselection.substring(0, 11);
        specialselected = itemselection.substring(11);
    } // end if
    if (itemselection != null && specialtype == "specialtype" && buyingaspecial != true)
    {
        noofspecials = _root.playershipstatus[11][1].length;
        shiptype = _root.playershipstatus[5][0];
        maxitems = _root.shiptype[shiptype][3][7];
        specialcost = _root.specialshipitems[specialselected][6];
        if (noofspecials >= maxitems)
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
            this.hardwarewarningbox.information = "Specials Full";
        }
        else if (_root.playershipstatus[3][1] < specialcost)
        {
            this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
            this.hardwarewarningbox.information = "Not Enough Funds";
        }
        else
        {
            buyingaspecial = true;
            removeMovieClip (this.hardwarewarningbox);
            attachMovie("specialsinformationbutton", "buyingaspecialquestion", 9980);
            specialselected = Number(specialselected);
            this.buyingaspecialquestion.specialselected = specialselected;
            this.buyingaspecialquestion.specialcost = _root.specialshipitems[specialselected][6];
            this.buyingaspecialquestion.specialquantity = _root.specialshipitems[specialselected][1];
        } // end else if
        buyingaspecial = false;
        itemselection = null;
    } // end if
}
