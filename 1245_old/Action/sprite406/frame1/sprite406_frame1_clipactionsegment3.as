﻿// Action script...

// [onClipEvent of sprite 405 in frame 1]
onClipEvent (enterFrame)
{
    if (currentoperation == "hardwareshopmenulabelturrets")
    {
        turretsproccessoneachframe();
    }
    else
    {
        if (itemselection != null && buyingagun != true && sellingagun != true)
        {
            itemgunselectiontype = itemselection.substring(0, 7);
            gunselectiontype = itemselection.substring(7);
            removeMovieClip (hardwarewarningbox);
        } // end if
        if (itemselection != null && (buyingagun == true || sellingagun == true))
        {
            itemselection = null;
        } // end if
        if (itemselection != null && itemgunselectiontype == "guntype" && buyingagun != true)
        {
            _parent.test = "ran";
            i = 0;
            emptygunlocationavailable = false;
            while (i < _root.playershipstatus[0].length && emptygunlocationavailable != true)
            {
                if (_root.playershipstatus[0][i][0] == "none")
                {
                    emptygunlocationavailable = true;
                    emptygunlocation = i;
                } // end if
                ++i;
            } // end while
            if (emptygunlocationavailable == true)
            {
                buyingagun = true;
                attachMovie("hardwareinformationbutton", "buyagunquestion", 99998);
                this.buyagunquestion.question = "Buy: " + _root.guntype[gunselectiontype][6] + " \r" + "For: " + _root.guntype[gunselectiontype][5];
            }
            else
            {
                this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
                this.hardwarewarningbox.information = "You Need An Open Hardpoint";
            } // end else if
            if (emptygunlocationavailable == false)
            {
                itemselection = null;
                buyingagun = false;
            } // end if
            itemselection = null;
        } // end if
        if (this.buyagunquestion.requestedanswer == true && buyingagun == true)
        {
            if (_root.playershipstatus[3][1] >= _root.guntype[gunselectiontype][5])
            {
                i = emptygunlocation;
                _root.playershipstatus[0][i][0] = gunselectiontype;
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - _root.guntype[gunselectiontype][5];
                buyingagun = false;
                removeMovieClip (this.buyagunquestion);
                itemselection = null;
                removeMovieClip ("shipblueprint.gun" + i);
                this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[0][i][0], "gun" + i, 12 + i);
                setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
                setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
                set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[0][i][0]][6]);
                emptygunlocationavailable = null;
            }
            else
            {
                this.attachMovie("hardwarewarningbox", "hardwarewarningbox", 99990);
                this.hardwarewarningbox.information = "You Do Not Have Enough Funds";
                buyingagun = false;
                removeMovieClip (this.buyagunquestion);
                itemselection = null;
            } // end if
        } // end else if
        if (this.buyagunquestion.requestedanswer == false && buyingagun == true)
        {
            buyingagun = false;
            removeMovieClip (this.buyagunquestion);
            itemselection = null;
        } // end if
        if (sellingagun == true && this.hardwareinformationbutton.requestedanswer == true)
        {
            i = hardpointmovedfrom;
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] + Math.round(_root.guntype[_root.playershipstatus[0][hardpointmovedfrom][0]][5] / 2);
            _root.playershipstatus[0][i][0] = "none";
            this.shipblueprint.attachMovie("hardwarescreengunbracketguntype" + _root.playershipstatus[0][i][0], "gun" + i, 12 + i);
            setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
            removeMovieClip ("hardwareinformationbutton");
            sellingagun = false;
        } // end if
        if (sellingagun == true && this.hardwareinformationbutton.requestedanswer == false)
        {
            i = hardpointmovedfrom;
            setProperty("shipblueprint.gun" + i, _x, _root.shiptype[playershiptype][2][i][0] * gunlocationmultiplier);
            setProperty("shipblueprint.gun" + i, _y, _root.shiptype[playershiptype][2][i][1] * gunlocationmultiplier);
            set("shipblueprint.gun" + i + ".itemname", _root.guntype[_root.playershipstatus[0][i][0]][6]);
            removeMovieClip ("hardwareinformationbutton");
            sellingagun = false;
        } // end if
    } // end else if
}
