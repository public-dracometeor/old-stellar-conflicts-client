﻿// Action script...

// [onClipEvent of sprite 644 in frame 1]
onClipEvent (enterFrame)
{
    function changetonewship(i)
    {
        _root.extraplayerships[newshipid] = new Array();
        _root.extraplayerships[newshipid][0] = i;
        currentharpoint = 0;
        _root.extraplayerships[newshipid][4] = new Array();
        while (currentharpoint < _root.shiptype[i][2].length)
        {
            _root.extraplayerships[newshipid][4][currentharpoint] = 0;
            ++currentharpoint;
        } // end while
        currentturretpoint = 0;
        _root.extraplayerships[newshipid][5] = new Array();
        while (currentturretpoint < _root.shiptype[i][5].length)
        {
            _root.extraplayerships[newshipid][5][currentturretpoint] = "none";
            ++currentturretpoint;
        } // end while
        _root.extraplayerships[newshipid][1] = 0;
        _root.extraplayerships[newshipid][2] = 0;
        _root.extraplayerships[newshipid][3] = 0;
    } // End of the function
    if (itemselection != null)
    {
        shipselectiontype = itemselection.substring(8);
    } // end if
    if (itemselection != null && buyingaship != true && shipselectiontype != _root.playershipstatus[5][0])
    {
        removeMovieClip (this.shipwarningbox);
        this.attachMovie("buyingashipquestion", "buyingashipquestion", 9980);
        currentshipupgradecost = _root.shiptype[shipselectiontype][1];
        this.buyingashipquestion.question = "Buy: " + _root.shiptyp[shipselectiontype][1] + " \r" + "For: " + currentshipupgradecost;
        buyingaship = true;
    } // end if
    if (this.buyingashipquestion.requestedanswer == true && buyingaship == true)
    {
        i = shipselectiontype;
        currentshipupgradecost = _root.shiptype[shipselectiontype][1];
        numberofguns = 0;
        if (_root.playershipstatus[3][1] >= currentshipupgradecost && numberofguns <= _root.shiptype[i][2].length)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - currentshipupgradecost;
            buyingaship = false;
            itemselection = null;
            removeMovieClip (this.buyingashipquestion);
            i = shipselectiontype;
            changetonewship(i);
            drawtheshipsforsale(currentleftmostitem);
        }
        else if (numberofguns > _root.shiptype[i][2].length)
        {
            this.attachMovie("shipwarningbox", "shipwarningbox", 9990);
            this.shipwarningbox.information = "You Need to sell " + (numberofguns - _root.shiptype[i][2].length) + " gun(s) first";
            buyingaship = false;
            itemselection = null;
            removeMovieClip (this.buyingashipquestion);
        }
        else
        {
            this.attachMovie("shipwarningbox", "shipwarningbox", 9990);
            this.shipwarningbox.information = "You Do Not Have Enough Funds";
            buyingaship = false;
            itemselection = null;
            removeMovieClip (this.buyingashipquestion);
        } // end else if
    } // end else if
    if (this.buyingashipquestion.requestedanswer == false && buyingaship == true)
    {
        buyingaship = false;
        removeMovieClip (this.buyingashipquestion);
        itemselection = null;
    } // end if
}
