﻿// Action script...

// [onClipEvent of sprite 644 in frame 1]
onClipEvent (load)
{
    function drawtheshipsforsale(currentleftmostitem)
    {
        if (currentleftmostitem > 0)
        {
            islefton = true;
        }
        else
        {
            islefton = false;
        } // end else if
        if (currentleftmostitem < _root.totalsmallships - maxitemstostoshow)
        {
            isrighton = true;
        }
        else
        {
            isrighton = false;
        } // end else if
        if (maxitemstostoshow <= _root.totalsmallships)
        {
            drawarrows(islefton, isrighton);
        } // end if
        for (i = 0; i < maxitemstostoshow && i < _root.totalsmallships; i++)
        {
            this.attachMovie("newshipitembox", "newshipitembox" + i, 5000 + i);
            setProperty("newshipitembox" + i, _x, shiplocationx + shipspacingx * i);
            setProperty("newshipitembox" + i, _y, shiplocationy);
            set(this + ".newshipitembox" + i + ".itemname", _root.shiptype[currentleftmostitem + i][0]);
            set(this + ".newshipitembox" + i + ".itemtype", "shiptype" + (currentleftmostitem + i));
            set(this + ".newshipitembox" + i + ".itemcost", "Cost:" + _root.shiptype[currentleftmostitem + i][1]);
            if (currentleftmostitem + i == _root.playershipstatus[5][0])
            {
                set(this + ".newshipitembox" + i + ".itemcost", "OWNED");
            } // end if
            set(this + ".newshipitembox" + i + ".itemspecs", "Hard Points: " + _root.shiptype[currentleftmostitem + i][2].length + " \r" + "Rotation Degrees / Second \r" + "  When Stationary: " + _root.shiptype[currentleftmostitem + i][3][2] + " \r" + "  Loss at Full Speed:" + "\r" + "   %" + _root.shiptype[currentleftmostitem + i][3][5] * 100 + " \r" + "Max Speed: " + _root.shiptype[currentleftmostitem + i][3][1] + "/ sec \r" + "Aft. Speed: " + _root.shiptype[currentleftmostitem + i][3][6] + "/ sec \r" + "Acceleration: " + _root.shiptype[currentleftmostitem + i][3][0] + "/ sec \r" + "Max Structure: " + _root.shiptype[currentleftmostitem + i][3][3] + " \r" + "Max Cargo: " + _root.shiptype[currentleftmostitem + i][4] + " \r" + "Turrets: " + (0 + _root.shiptype[currentleftmostitem + i][5].length) + " \r" + "Top Weapon: " + _root.guntype[_root.shiptype[currentleftmostitem + i][6][3]][6] + " \r" + "Max Shield: " + _root.shieldgenerators[_root.shiptype[currentleftmostitem + i][6][0]][0] + " \r" + "Max Energy: " + _root.energycapacitors[_root.shiptype[currentleftmostitem + i][6][1]][0] + " \r" + "Max Charge Rate: " + _root.energygenerators[_root.shiptype[currentleftmostitem + i][6][2]][0]);
        } // end of for
    } // End of the function
    function drawarrows(islefton, isrighton)
    {
        this.attachMovie("hardwareselectleft", "hardwareselectleft", 1001);
        this.hardwareselectleft._x = shiplocationx - 100;
        this.hardwareselectleft._y = shiplocationy;
        this.hardwareselectleft._alpha = 60;
        if (islefton == true)
        {
            this.hardwareselectleft._alpha = 100;
        } // end if
        this.attachMovie("hardwareselectright", "hardwareselectright", 1002);
        this.hardwareselectright._x = shiplocationx + 100 + shipspacingx * (maxitemstostoshow - 1);
        this.hardwareselectright._y = shiplocationy;
        this.hardwareselectright._alpha = 60;
        if (isrighton == true)
        {
            this.hardwareselectright._alpha = 100;
        } // end if
    } // End of the function
    this.newshipid = _root.newshipid;
    startingship = 0;
    itemselection = null;
    buyingaship = false;
    shiplocationx = -_root.gameareawidth / 3 + 15;
    shiplocationy = -_root.gameareaheight / 8;
    shipspacingx = 150;
    shipspacingy = 200;
    this._y = _root.gameareaheight / 2;
    this._x = _root.gameareawidth / 2;
    this._height = _root.gameareaheight;
    this._width = _root.gameareawidth;
    this.attachMovie("shopexitbutton", "purchaseshipexitbutton", 150);
    this.purchaseshipexitbutton._y = _root.gameareaheight / 2 - 50;
    this.purchaseshipexitbutton._x = _root.gameareawidth / 2 - 100;
    this.attachMovie("starbasefundsdisplay", "funds", 121);
    this.funds._y = -_root.gameareaheight / 2 + 15;
    this.funds._x = _root.gameareawidth / 2 - 100;
    maxitemstostoshow = 4;
    currentleftmostitem = 0;
    drawtheshipsforsale(currentleftmostitem);
}

// [onClipEvent of sprite 644 in frame 1]
onClipEvent (enterFrame)
{
    if (moveitemsdisplayed != null)
    {
        currentleftmostitem = currentleftmostitem + moveitemsdisplayed;
        moveitemsdisplayed = null;
        if (currentleftmostitem < 0)
        {
            currentleftmostitem = 0;
        } // end if
        if (currentleftmostitem > _root.shiptype.length - maxitemstostoshow)
        {
            currentleftmostitem = currentleftmostitem - 1;
        } // end if
        drawtheshipsforsale(currentleftmostitem);
    } // end if
}

// [onClipEvent of sprite 644 in frame 1]
onClipEvent (enterFrame)
{
    function changetonewship(i)
    {
        _root.extraplayerships[newshipid] = new Array();
        _root.extraplayerships[newshipid][0] = i;
        currentharpoint = 0;
        _root.extraplayerships[newshipid][4] = new Array();
        while (currentharpoint < _root.shiptype[i][2].length)
        {
            _root.extraplayerships[newshipid][4][currentharpoint] = 0;
            ++currentharpoint;
        } // end while
        currentturretpoint = 0;
        _root.extraplayerships[newshipid][5] = new Array();
        while (currentturretpoint < _root.shiptype[i][5].length)
        {
            _root.extraplayerships[newshipid][5][currentturretpoint] = "none";
            ++currentturretpoint;
        } // end while
        _root.extraplayerships[newshipid][1] = 0;
        _root.extraplayerships[newshipid][2] = 0;
        _root.extraplayerships[newshipid][3] = 0;
    } // End of the function
    if (itemselection != null)
    {
        shipselectiontype = itemselection.substring(8);
    } // end if
    if (itemselection != null && buyingaship != true && shipselectiontype != _root.playershipstatus[5][0])
    {
        removeMovieClip (this.shipwarningbox);
        this.attachMovie("buyingashipquestion", "buyingashipquestion", 9980);
        currentshipupgradecost = _root.shiptype[shipselectiontype][1];
        this.buyingashipquestion.question = "Buy: " + _root.shiptyp[shipselectiontype][1] + " \r" + "For: " + currentshipupgradecost;
        buyingaship = true;
    } // end if
    if (this.buyingashipquestion.requestedanswer == true && buyingaship == true)
    {
        i = shipselectiontype;
        currentshipupgradecost = _root.shiptype[shipselectiontype][1];
        numberofguns = 0;
        if (_root.playershipstatus[3][1] >= currentshipupgradecost && numberofguns <= _root.shiptype[i][2].length)
        {
            _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - currentshipupgradecost;
            buyingaship = false;
            itemselection = null;
            removeMovieClip (this.buyingashipquestion);
            i = shipselectiontype;
            changetonewship(i);
            drawtheshipsforsale(currentleftmostitem);
        }
        else if (numberofguns > _root.shiptype[i][2].length)
        {
            this.attachMovie("shipwarningbox", "shipwarningbox", 9990);
            this.shipwarningbox.information = "You Need to sell " + (numberofguns - _root.shiptype[i][2].length) + " gun(s) first";
            buyingaship = false;
            itemselection = null;
            removeMovieClip (this.buyingashipquestion);
        }
        else
        {
            this.attachMovie("shipwarningbox", "shipwarningbox", 9990);
            this.shipwarningbox.information = "You Do Not Have Enough Funds";
            buyingaship = false;
            itemselection = null;
            removeMovieClip (this.buyingashipquestion);
        } // end else if
    } // end else if
    if (this.buyingashipquestion.requestedanswer == false && buyingaship == true)
    {
        buyingaship = false;
        removeMovieClip (this.buyingashipquestion);
        itemselection = null;
    } // end if
}

// [Action in Frame 1]
stop ();
