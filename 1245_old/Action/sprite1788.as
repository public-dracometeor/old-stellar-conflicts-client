﻿// Action script...

// [Action in Frame 1]
this._visible = false;
sparkcolour = new Array();
sparkcolour[0] = 16711680;
sparkcolour[1] = 16737792;
sparkcolour[2] = 16777113;
sparkcolour[3] = 16776960;
sparkcolour[4] = 10027008;
thiscolor = new Color(this);
sparktimeinterval = 200;
sparkduration = 25;
currentsparktime = Math.round(Math.random() * (sparktimeinterval + sparkduration));
if (currentsparktime > sparktimeinterval)
{
    this._visible = true;
    thiscolor.setRGB(sparkcolour[Math.round(Math.random() * (sparkcolour.length - 1))]);
} // end if
++currentsparktime;

// [Action in Frame 2]
if (currentsparktime > sparktimeinterval)
{
    this._visible = true;
    thiscolor.setRGB(sparkcolour[Math.round(Math.random() * (sparkcolour.length - 1))]);
} // end if
if (currentsparktime > sparktimeinterval + sparkduration)
{
    this._visible = false;
    currentsparktime = Math.round(Math.random() * (sparktimeinterval / 2));
} // end if
++currentsparktime;

// [Action in Frame 3]
gotoAndPlay(2);
