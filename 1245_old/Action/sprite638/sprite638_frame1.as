﻿// Action script...

// [onClipEvent of sprite 543 in frame 1]
onClipEvent (load)
{
    this.label = "Total Stars";
    this.description = "Total Stars shown, less stars increase game speed";
    this.newnumber = _root.totalstars;
    this.rootvariable = "totalstars";
}

// [onClipEvent of sprite 550 in frame 1]
onClipEvent (load)
{
    this.label = "Sound";
    this.description = "Turn sounds on or off, Sounds can slow down game speed";
    this.value = _root.soundvolume;
    this.rootvariable = "soundvolume";
    this.onbuttonswitch = "on";
    this.offbuttonswitch = "off";
    if (value == onbuttonswitch)
    {
        this.offbutton._alpha = 60;
        this.onbutton._alpha = 100;
    } // end if
    if (value == offbuttonswitch)
    {
        this.offbutton._alpha = 100;
        this.onbutton._alpha = 60;
    } // end if
}

// [onClipEvent of sprite 543 in frame 1]
onClipEvent (load)
{
    if (_root.teamdeathmatch == true)
    {
        this._visible = false;
    } // end if
    this.label = "Team Number";
    this.description = "Enter Team Number or N/A, type // then message to send a team message";
    this.newnumber = _root.playershipstatus[5][2];
    this.rootvariable = "teamnumber";
}

// [onClipEvent of sprite 563 in frame 1]
onClipEvent (load)
{
    function purchasebounty()
    {
        if (this.amount > 0)
        {
            playerfound = false;
            for (j = 0; j < _root.currentonlineplayers.length; j++)
            {
                if (this.bountyplayer == _root.currentonlineplayers[j][1])
                {
                    playertosendbtyto = _root.currentonlineplayers[j][0];
                    playerfound = true;
                    j = 999999;
                } // end if
            } // end of for
            if (playerfound == true)
            {
                datatosend = "NEWS`BOUNTY`" + playertosendbtyto + "`" + this.amount + "`" + this.purchaser + "`" + this.bountyplayer + "`" + "~";
                _root.mysocket.send(datatosend);
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - this.amount * costperbty;
                this.attachMovie("shipwarningbox", "shipwarningbox", 1);
                this.shipwarningbox.information = "Bounty Purchase Successful";
            }
            else
            {
                this.shipwarningbox.information = "Unable - Player Offline";
            } // end if
        } // end else if
        saveplayersgame();
        cancelpurchase();
    } // End of the function
    function cancelpurchase()
    {
        this.bountyplayer = "Enter Name";
        this.amount = 0;
        this.finalcost = "Cost: 0";
        this.purchaser = "Anonymous";
        lastbountyplayer = "Enter Name";
        lastamount = 0;
    } // End of the function
    function saveplayersgame()
    {
        variablestosave = "PI" + _root.playershipstatus[3][0] + "`ST" + _root.playershipstatus[5][0] + "`SG" + _root.playershipstatus[2][0] + "`EG" + _root.playershipstatus[1][0] + "`EC" + _root.playershipstatus[1][5] + "`CR" + _root.playershipstatus[3][1] + "`SE" + _root.playershipstatus[5][1] + "`" + _root.playershipstatus[4][0];
        for (currenthardpoint = 0; currenthardpoint < _root.playershipstatus[0].length; currenthardpoint++)
        {
            variablestosave = variablestosave + ("`HP" + currenthardpoint + "G" + _root.playershipstatus[0][currenthardpoint][0]);
        } // end of for
        for (currentturretpoint = 0; currentturretpoint < _root.playershipstatus[8].length; currentturretpoint++)
        {
            variablestosave = variablestosave + ("`TT" + currentturretpoint + "G" + _root.playershipstatus[8][currentturretpoint][0]);
        } // end of for
        for (currentcargo = 0; currentcargo < _root.playershipstatus[4][1].length; currentcargo++)
        {
            if (_root.playershipstatus[4][1][currentcargo] > 0)
            {
                variablestosave = variablestosave + ("`CO" + currentcargo + "A" + _root.playershipstatus[4][1][currentcargo]);
            } // end if
        } // end of for
        variablestosave = variablestosave + "~";
        if (_root.isplayeraguest == false)
        {
            _root.saveplayerextraships();
            newaccountVars = new XML();
            newaccountVars.load(_root.pathtoaccounts + "accounts.php?mode=save&name=" + _root.playershipstatus[3][2] + "&pass=" + _root.playershipstatus[3][3] + "&info=" + variablestosave + "&score=" + _root.playershipstatus[5][9] + "&shipsdata=" + _root.playersextrashipsdata + "&funds=" + _root.playershipstatus[3][1]);
            newaccountVars.onLoad = function (success)
            {
                _root.lastplayerssavedinfo = variablestosave;
            };
        } // end if
        _root.lastplayerssavedinfo = variablestosave;
    } // End of the function
    this._visible = false;
    this.bountyplayer = "Enter Name";
    this.amount = 0;
    this.finalcost = "Cost: 0";
    this.purchaser = "Anonymous";
    lastbountyplayer = "Enter Name";
    lastamount = 0;
    costperbty = 50;
}

// [onClipEvent of sprite 563 in frame 1]
onClipEvent (enterFrame)
{
    if (this.amount != lastamount)
    {
        if (isNaN(this.amount))
        {
            this.amount = 0;
            this.finalcost = 0;
        }
        else if (this.amount * costperbty > _root.playershipstatus[3][1])
        {
            this.amount = lastamount;
        }
        else
        {
            this.amount = Math.round(this.amount);
            this.finalcost = "Cost: " + this.amount * costperbty;
            lastamount = this.amount;
        } // end else if
    } // end else if
    if (this.bountyplayer != lastbountyplayer)
    {
        this.bountyplayer = this.bountyplayer.toUpperCase();
        lastbountyplayer = this.bountyplayer;
        lengthofname = this.bountyplayer.length;
        if (lengthofname > 0)
        {
            namesfound = 0;
            idspottosendto = null;
            for (j = 0; j < _root.currentonlineplayers.length; j++)
            {
                if (this.bountyplayer == _root.currentonlineplayers[j][1].substr(0, lengthofname) && _root.currentonlineplayers[j][0] != _root.playershipstatus[3][0])
                {
                    idspottosendto = j;
                    ++namesfound;
                } // end if
            } // end of for
            if (namesfound < 1)
            {
                this.bountyplayer = "";
            }
            else if (namesfound == 1)
            {
                this.bountyplayer = _root.currentonlineplayers[idspottosendto][1];
            } // end if
        } // end if
    } // end else if
}

// [onClipEvent of sprite 620 in frame 1]
onClipEvent (load)
{
    function transferfunds()
    {
        fundsvailable = Number(_root.playershipstatus[3][1]);
        if (isNaN(fundsvailable))
        {
            this.attachMovie("shipwarningbox", "shipwarningbox", 1);
            this.shipwarningbox.information = "Funds are NaN";
        }
        else if (this.amount > 5000000)
        {
            this.attachMovie("shipwarningbox", "shipwarningbox", 1);
            this.shipwarningbox.information = "Max of 5 million at a time";
        }
        else if (this.amount > 0 && this.amount <= _root.playershipstatus[3][1])
        {
            playerfound = false;
            for (j = 0; j < _root.currentonlineplayers.length; j++)
            {
                if (this.bountyplayer == _root.currentonlineplayers[j][1])
                {
                    playertosendbtyto = _root.currentonlineplayers[j][0];
                    playerfound = true;
                    j = 999999;
                } // end if
            } // end of for
            if (playerfound == true)
            {
                datatosend = "NEWS`TRANSFER`" + playertosendbtyto + "`" + this.amount + "`" + _root.playershipstatus[3][0] + "`" + "~";
                _root.mysocket.send(datatosend);
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - this.amount;
                this.attachMovie("shipwarningbox", "shipwarningbox", 1);
                this.shipwarningbox.information = "Transfer Successful";
                _root.savegamescript.saveplayersgame(this);
            }
            else
            {
                this.attachMovie("shipwarningbox", "shipwarningbox", 1);
                this.shipwarningbox.information = "Unable - Player Offline";
            } // end else if
        } // end else if
        canceltransfer();
    } // End of the function
    function canceltransfer()
    {
        this.bountyplayer = "Enter Name";
        this.amount = 0;
        lastbountyplayer = "Enter Name";
    } // End of the function
    this.bountyplayer = "Enter Name";
    this.amount = 0;
    lastbountyplayer = "Enter Name";
    lastamount = 0;
}

// [onClipEvent of sprite 620 in frame 1]
onClipEvent (enterFrame)
{
    if (this.amount != lastamount)
    {
        if (isNaN(this.amount))
        {
            this.amount = 0;
            this.finalcost = 0;
        }
        else if (this.amount > _root.playershipstatus[3][1])
        {
            this.amount = _root.playershipstatus[3][1];
        }
        else
        {
            this.amount = Math.round(this.amount);
            lastamount = this.amount;
        } // end else if
    } // end else if
    if (this.bountyplayer != lastbountyplayer)
    {
        this.bountyplayer = this.bountyplayer.toUpperCase();
        lastbountyplayer = this.bountyplayer;
        lengthofname = this.bountyplayer.length;
        if (lengthofname > 0)
        {
            namesfound = 0;
            idspottosendto = null;
            for (j = 0; j < _root.currentonlineplayers.length; j++)
            {
                if (this.bountyplayer == _root.currentonlineplayers[j][1].substr(0, lengthofname) && _root.currentonlineplayers[j][0] != _root.playershipstatus[3][0])
                {
                    idspottosendto = j;
                    ++namesfound;
                } // end if
            } // end of for
            if (namesfound < 1)
            {
                this.bountyplayer = "";
            }
            else if (namesfound == 1)
            {
                this.bountyplayer = _root.currentonlineplayers[idspottosendto][1];
            } // end if
        } // end if
    } // end else if
}

// [onClipEvent of sprite 569 in frame 1]
onClipEvent (load)
{
    this.label = "Create King Flag";
    if (_root.teamdeathmatch == true || _root.isgameracingzone == true)
    {
        this._visible = false;
    } // end if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    _root.func_playercreatekflag();
}

// [onClipEvent of sprite 569 in frame 1]
onClipEvent (load)
{
    this.label = "Create Meteor";
    if (_root.teamdeathmatch == true || _root.isgameracingzone == true)
    {
        this._visible = false;
    } // end if
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.incomingmeteor.length < 1)
    {
        _root.func_beginmeteor();
    } // end if
}

// [onClipEvent of sprite 569 in frame 1]
onClipEvent (load)
{
    this.label = "Refresh Bases";
}

// [onClipEvent of sprite 569 in frame 1]
on (rollOver)
{
}

// [onClipEvent of sprite 569 in frame 1]
on (release)
{
    if (_root.teamdeathmatch != true || _root.isgameracingzone != true)
    {
        _root.loadsquadbases();
        this.label = "Refreshed";
    } // end if
}

// [Action in Frame 1]
if (_root.isplayeraguest == true)
{
    this.squadsettings._visible = false;
    this.transferfunds._visible = false;
    this.changeinfo._visible = false;
} // end if
stop ();
