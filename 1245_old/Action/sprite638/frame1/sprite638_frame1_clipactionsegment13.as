﻿// Action script...

// [onClipEvent of sprite 550 in frame 1]
onClipEvent (load)
{
    this.label = "Sound";
    this.description = "Turn sounds on or off, Sounds can slow down game speed";
    this.value = _root.soundvolume;
    this.rootvariable = "soundvolume";
    this.onbuttonswitch = "on";
    this.offbuttonswitch = "off";
    if (value == onbuttonswitch)
    {
        this.offbutton._alpha = 60;
        this.onbutton._alpha = 100;
    } // end if
    if (value == offbuttonswitch)
    {
        this.offbutton._alpha = 100;
        this.onbutton._alpha = 60;
    } // end if
}
