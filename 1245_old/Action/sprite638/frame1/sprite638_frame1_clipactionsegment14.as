﻿// Action script...

// [onClipEvent of sprite 543 in frame 1]
onClipEvent (load)
{
    this.label = "Total Stars";
    this.description = "Total Stars shown, less stars increase game speed";
    this.newnumber = _root.totalstars;
    this.rootvariable = "totalstars";
}
