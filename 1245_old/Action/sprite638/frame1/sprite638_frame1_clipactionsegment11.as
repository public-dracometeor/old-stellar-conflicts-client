﻿// Action script...

// [onClipEvent of sprite 563 in frame 1]
onClipEvent (load)
{
    function purchasebounty()
    {
        if (this.amount > 0)
        {
            playerfound = false;
            for (j = 0; j < _root.currentonlineplayers.length; j++)
            {
                if (this.bountyplayer == _root.currentonlineplayers[j][1])
                {
                    playertosendbtyto = _root.currentonlineplayers[j][0];
                    playerfound = true;
                    j = 999999;
                } // end if
            } // end of for
            if (playerfound == true)
            {
                datatosend = "NEWS`BOUNTY`" + playertosendbtyto + "`" + this.amount + "`" + this.purchaser + "`" + this.bountyplayer + "`" + "~";
                _root.mysocket.send(datatosend);
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - this.amount * costperbty;
                this.attachMovie("shipwarningbox", "shipwarningbox", 1);
                this.shipwarningbox.information = "Bounty Purchase Successful";
            }
            else
            {
                this.shipwarningbox.information = "Unable - Player Offline";
            } // end if
        } // end else if
        saveplayersgame();
        cancelpurchase();
    } // End of the function
    function cancelpurchase()
    {
        this.bountyplayer = "Enter Name";
        this.amount = 0;
        this.finalcost = "Cost: 0";
        this.purchaser = "Anonymous";
        lastbountyplayer = "Enter Name";
        lastamount = 0;
    } // End of the function
    function saveplayersgame()
    {
        variablestosave = "PI" + _root.playershipstatus[3][0] + "`ST" + _root.playershipstatus[5][0] + "`SG" + _root.playershipstatus[2][0] + "`EG" + _root.playershipstatus[1][0] + "`EC" + _root.playershipstatus[1][5] + "`CR" + _root.playershipstatus[3][1] + "`SE" + _root.playershipstatus[5][1] + "`" + _root.playershipstatus[4][0];
        for (currenthardpoint = 0; currenthardpoint < _root.playershipstatus[0].length; currenthardpoint++)
        {
            variablestosave = variablestosave + ("`HP" + currenthardpoint + "G" + _root.playershipstatus[0][currenthardpoint][0]);
        } // end of for
        for (currentturretpoint = 0; currentturretpoint < _root.playershipstatus[8].length; currentturretpoint++)
        {
            variablestosave = variablestosave + ("`TT" + currentturretpoint + "G" + _root.playershipstatus[8][currentturretpoint][0]);
        } // end of for
        for (currentcargo = 0; currentcargo < _root.playershipstatus[4][1].length; currentcargo++)
        {
            if (_root.playershipstatus[4][1][currentcargo] > 0)
            {
                variablestosave = variablestosave + ("`CO" + currentcargo + "A" + _root.playershipstatus[4][1][currentcargo]);
            } // end if
        } // end of for
        variablestosave = variablestosave + "~";
        if (_root.isplayeraguest == false)
        {
            _root.saveplayerextraships();
            newaccountVars = new XML();
            newaccountVars.load(_root.pathtoaccounts + "accounts.php?mode=save&name=" + _root.playershipstatus[3][2] + "&pass=" + _root.playershipstatus[3][3] + "&info=" + variablestosave + "&score=" + _root.playershipstatus[5][9] + "&shipsdata=" + _root.playersextrashipsdata + "&funds=" + _root.playershipstatus[3][1]);
            newaccountVars.onLoad = function (success)
            {
                _root.lastplayerssavedinfo = variablestosave;
            };
        } // end if
        _root.lastplayerssavedinfo = variablestosave;
    } // End of the function
    this._visible = false;
    this.bountyplayer = "Enter Name";
    this.amount = 0;
    this.finalcost = "Cost: 0";
    this.purchaser = "Anonymous";
    lastbountyplayer = "Enter Name";
    lastamount = 0;
    costperbty = 50;
}
