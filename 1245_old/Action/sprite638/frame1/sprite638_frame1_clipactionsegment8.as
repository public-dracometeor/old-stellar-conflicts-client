﻿// Action script...

// [onClipEvent of sprite 620 in frame 1]
onClipEvent (enterFrame)
{
    if (this.amount != lastamount)
    {
        if (isNaN(this.amount))
        {
            this.amount = 0;
            this.finalcost = 0;
        }
        else if (this.amount > _root.playershipstatus[3][1])
        {
            this.amount = _root.playershipstatus[3][1];
        }
        else
        {
            this.amount = Math.round(this.amount);
            lastamount = this.amount;
        } // end else if
    } // end else if
    if (this.bountyplayer != lastbountyplayer)
    {
        this.bountyplayer = this.bountyplayer.toUpperCase();
        lastbountyplayer = this.bountyplayer;
        lengthofname = this.bountyplayer.length;
        if (lengthofname > 0)
        {
            namesfound = 0;
            idspottosendto = null;
            for (j = 0; j < _root.currentonlineplayers.length; j++)
            {
                if (this.bountyplayer == _root.currentonlineplayers[j][1].substr(0, lengthofname) && _root.currentonlineplayers[j][0] != _root.playershipstatus[3][0])
                {
                    idspottosendto = j;
                    ++namesfound;
                } // end if
            } // end of for
            if (namesfound < 1)
            {
                this.bountyplayer = "";
            }
            else if (namesfound == 1)
            {
                this.bountyplayer = _root.currentonlineplayers[idspottosendto][1];
            } // end if
        } // end if
    } // end else if
}
