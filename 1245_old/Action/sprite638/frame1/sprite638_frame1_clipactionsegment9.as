﻿// Action script...

// [onClipEvent of sprite 620 in frame 1]
onClipEvent (load)
{
    function transferfunds()
    {
        fundsvailable = Number(_root.playershipstatus[3][1]);
        if (isNaN(fundsvailable))
        {
            this.attachMovie("shipwarningbox", "shipwarningbox", 1);
            this.shipwarningbox.information = "Funds are NaN";
        }
        else if (this.amount > 5000000)
        {
            this.attachMovie("shipwarningbox", "shipwarningbox", 1);
            this.shipwarningbox.information = "Max of 5 million at a time";
        }
        else if (this.amount > 0 && this.amount <= _root.playershipstatus[3][1])
        {
            playerfound = false;
            for (j = 0; j < _root.currentonlineplayers.length; j++)
            {
                if (this.bountyplayer == _root.currentonlineplayers[j][1])
                {
                    playertosendbtyto = _root.currentonlineplayers[j][0];
                    playerfound = true;
                    j = 999999;
                } // end if
            } // end of for
            if (playerfound == true)
            {
                datatosend = "NEWS`TRANSFER`" + playertosendbtyto + "`" + this.amount + "`" + _root.playershipstatus[3][0] + "`" + "~";
                _root.mysocket.send(datatosend);
                _root.playershipstatus[3][1] = _root.playershipstatus[3][1] - this.amount;
                this.attachMovie("shipwarningbox", "shipwarningbox", 1);
                this.shipwarningbox.information = "Transfer Successful";
                _root.savegamescript.saveplayersgame(this);
            }
            else
            {
                this.attachMovie("shipwarningbox", "shipwarningbox", 1);
                this.shipwarningbox.information = "Unable - Player Offline";
            } // end else if
        } // end else if
        canceltransfer();
    } // End of the function
    function canceltransfer()
    {
        this.bountyplayer = "Enter Name";
        this.amount = 0;
        lastbountyplayer = "Enter Name";
    } // End of the function
    this.bountyplayer = "Enter Name";
    this.amount = 0;
    lastbountyplayer = "Enter Name";
    lastamount = 0;
}
