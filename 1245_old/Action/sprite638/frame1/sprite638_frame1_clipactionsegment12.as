﻿// Action script...

// [onClipEvent of sprite 543 in frame 1]
onClipEvent (load)
{
    if (_root.teamdeathmatch == true)
    {
        this._visible = false;
    } // end if
    this.label = "Team Number";
    this.description = "Enter Team Number or N/A, type // then message to send a team message";
    this.newnumber = _root.playershipstatus[5][2];
    this.rootvariable = "teamnumber";
}
