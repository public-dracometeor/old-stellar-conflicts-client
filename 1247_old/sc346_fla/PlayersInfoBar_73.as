﻿package sc346_fla
{
    import flash.display.*;

    dynamic public class PlayersInfoBar_73 extends flash.display.MovieClip
    {
        public var structurebar:MovieClip;
        public var playersmaxstructure:Object;
        public var energy:Object;
        public var shieldbar:MovieClip;
        public var energybar:MovieClip;
        public var maxshieldstrength:Object;
        public var currentvelocity:Object;
        public var maxvelocity:Object;
        public var velocitybar:MovieClip;
        public var shield:Object;
        public var maxenergy:Object;
        public var structure:Object;

        public function PlayersInfoBar_73()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        public function showenergy(param1)
        {
            var _loc_2:* = Math.ceil(param1 / this.maxenergy * 28);
            if (_loc_2 < 1)
            {
                _loc_2 = 1;
            }
            this.energybar.gotoAndStop(_loc_2);
            return;
        }// end function

        public function func_showStruct(param1)
        {
            var _loc_2:* = Math.ceil(param1 / this.playersmaxstructure * 28);
            if (_loc_2 < 1)
            {
                _loc_2 = 1;
            }
            this.structurebar.gotoAndStop(_loc_2);
            return;
        }// end function

        public function func_showVelocity(param1)
        {
            var _loc_2:* = undefined;
            if (param1 <= 0.4)
            {
                this.velocitybar.gotoAndStop(1);
            }
            else if (param1 <= this.maxvelocity)
            {
                _loc_2 = Math.ceil(param1 / this.maxvelocity * 24);
                this.velocitybar.gotoAndStop(_loc_2);
            }
            else
            {
                this.velocitybar.gotoAndStop(25);
            }
            if (param1 < 0)
            {
            }
            return;
        }// end function

        function frame1()
        {
            this.energybar.gotoAndStop(28);
            this.energy = 1;
            this.shield = 1;
            this.shieldbar.gotoAndStop(28);
            this.structure = 1;
            this.structurebar.gotoAndStop(28);
            this.currentvelocity = 0;
            this.velocitybar.gotoAndStop(1);
            return;
        }// end function

        public function showshield(param1)
        {
            var _loc_2:* = Math.ceil(param1 / this.maxshieldstrength * 28);
            if (_loc_2 < 1)
            {
                _loc_2 = 1;
            }
            this.shieldbar.gotoAndStop(_loc_2);
            return;
        }// end function

        public function func_displaystats(param1, param2, param3, param4)
        {
            this.showenergy(param1);
            this.showshield(param2);
            this.func_showStruct(param3);
            this.func_showVelocity(param4);
            return;
        }// end function

    }
}
