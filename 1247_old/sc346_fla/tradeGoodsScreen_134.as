﻿package sc346_fla
{
    import flash.display.*;
    import flash.events.*;

    dynamic public class tradeGoodsScreen_134 extends flash.display.MovieClip
    {
        public var gamesetting:Object;
        public var MaxUnits:Object;
        public var FreeCargoRoom:Object;
        public var sellableitems:Object;
        public var CurrentMethod:Object;
        public var s0:MovieClip;
        public var s1:MovieClip;
        public var s2:MovieClip;
        public var s3:MovieClip;
        public var s4:MovieClip;
        public var s5:MovieClip;
        public var s6:MovieClip;
        public var squadoptions:MovieClip;
        public var mysocket:Object;
        public var s7:MovieClip;
        public var CurrentItemPrice:Object;
        public var CurrenCellSelected:Object;
        public var p0:MovieClip;
        public var p1:MovieClip;
        public var p2:MovieClip;
        public var p3:MovieClip;
        public var p4:MovieClip;
        public var p5:MovieClip;
        public var p6:MovieClip;
        public var p7:MovieClip;
        public var gameSounds:Object;
        public var playershipstatus:Object;
        public var blankersdafadsfdsaf:String;
        public var i0:MovieClip;
        public var i1:MovieClip;
        public var i2:MovieClip;
        public var i3:MovieClip;
        public var i4:MovieClip;
        public var i5:MovieClip;
        public var i6:MovieClip;
        public var transactionWindow:MovieClip;
        public var purchasableitems:Object;
        public var tradegoods:Object;
        public var exit_button:MovieClip;
        public var i7:MovieClip;
        public var shiptype:Object;
        public var TotalCargoRoom:Object;
        public var gameoptions:MovieClip;

        public function tradeGoodsScreen_134()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        public function func_setplayersFreeCargo()
        {
            this.FreeCargoRoom = this.TotalCargoRoom;
            var _loc_1:* = 0;
            while (_loc_1 < this.tradegoods.length)
            {
                
                if (!isNaN(this.playershipstatus[4][1][_loc_1]))
                {
                    this.FreeCargoRoom = this.FreeCargoRoom - this.playershipstatus[4][1][_loc_1];
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function OpenTrasactionWindow(param1, param2)
        {
            var _loc_3:* = undefined;
            this.func_setplayersFreeCargo();
            this.transactionWindow.gotoAndStop(1);
            this.transactionWindow.accept_Button.gotoAndStop(1);
            this.transactionWindow.cancel_Button.gotoAndStop(1);
            this.transactionWindow.accept_Button.actionlabel.text = "Accept";
            this.transactionWindow.accept_Button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_AcceptSale);
            this.transactionWindow.cancel_Button.actionlabel.text = "Cancel";
            this.transactionWindow.cancel_Button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_CancelSale);
            this.transactionWindow.finalAmount.text = "0";
            this.CurrenCellSelected = param2;
            if (param1 == "purchase")
            {
                this.CurrentMethod = "purchase";
                this.CurrentItemPrice = this.purchasableitems[this.CurrenCellSelected][1];
                this.transactionWindow.purchaseInformation.text = "Purchasing " + this.tradegoods[this.purchasableitems[this.CurrenCellSelected][0]].GoodsName + " at " + this.CurrentItemPrice + " each";
                _loc_3 = Math.floor(this.playershipstatus[3][1] / this.CurrentItemPrice);
                if (_loc_3 > this.FreeCargoRoom)
                {
                    _loc_3 = this.FreeCargoRoom;
                }
                this.MaxUnits = _loc_3;
                this.transactionWindow.pricingInformation.text = "You can purchase up to " + this.MaxUnits + " units, you have " + this.FreeCargoRoom + " spots free";
                this.transactionWindow.PurchaseOrSell.text = "Puchase:";
                this.transactionWindow.quatityEntered.text = "0";
                this.transactionWindow.TotalPrice.text = "Total Cost:";
            }
            else if (param1 == "sale")
            {
                this.CurrentMethod = "sale";
                this.CurrentItemPrice = this.sellableitems[this.CurrenCellSelected][1];
                this.MaxUnits = 0;
                if (this.playershipstatus[4][1][this.sellableitems[this.CurrenCellSelected][0]] != null)
                {
                    this.MaxUnits = this.playershipstatus[4][1][this.sellableitems[this.CurrenCellSelected][0]];
                }
                this.transactionWindow.purchaseInformation.text = "Selling " + this.tradegoods[this.sellableitems[this.CurrenCellSelected][0]].GoodsName + " at " + this.CurrentItemPrice + " each";
                this.transactionWindow.pricingInformation.text = "You have " + this.MaxUnits + " units to sell.";
                this.transactionWindow.PurchaseOrSell.text = "Sell:";
                this.transactionWindow.quatityEntered.text = "0";
                this.transactionWindow.TotalPrice.text = "Total Income:";
            }
            return;
        }// end function

        public function func_ListItemClick(event:MouseEvent) : void
        {
            var _loc_2:* = String(event.target.parent.name);
            var _loc_3:* = _loc_2.substr(0, 1);
            var _loc_4:* = Number(_loc_2.substr(1, 1));
            trace(_loc_2);
            if (_loc_3 == "p")
            {
                if (this.purchasableitems.length > _loc_4)
                {
                    this.func_setButtons();
                    this["p" + _loc_4].gotoAndStop(2);
                    this.OpenTrasactionWindow("purchase", _loc_4);
                    this.func_playRegularClick();
                }
            }
            else if (_loc_3 == "s")
            {
                if (this.sellableitems.length > _loc_4)
                {
                    this.func_setButtons();
                    this["s" + _loc_4].gotoAndStop(2);
                    this.OpenTrasactionWindow("sale", _loc_4);
                    this.func_playRegularClick();
                }
            }
            else if (_loc_3 == "i")
            {
            }
            return;
        }// end function

        public function func_resetTrasaction()
        {
            this.CurrentMethod = "";
            this.CurrenCellSelected = -1;
            this.func_setButtons();
            this.MaxUnits = -1;
            this.transactionWindow.gotoAndStop(2);
            return;
        }// end function

        public function func_setButtons()
        {
            var _loc_1:* = 0;
            _loc_1 = 0;
            while (_loc_1 < 8)
            {
                
                this["p" + _loc_1].gotoAndStop(1);
                this["s" + _loc_1].gotoAndStop(1);
                this["i" + _loc_1].gotoAndStop(1);
                if (this.purchasableitems.length > _loc_1)
                {
                    this["p" + _loc_1].itemText.text = this.tradegoods[this.purchasableitems[_loc_1][0]].GoodsName + " (" + this.purchasableitems[_loc_1][1] + ")";
                }
                else
                {
                    this["p" + _loc_1].itemText.text = "";
                }
                if (this.sellableitems.length > _loc_1)
                {
                    this["s" + _loc_1].itemText.text = this.tradegoods[this.sellableitems[_loc_1][0]].GoodsName + " (" + this.sellableitems[_loc_1][1] + ")";
                }
                else
                {
                    this["s" + _loc_1].itemText.text = "";
                }
                this["i" + _loc_1].itemText.text = "";
                _loc_1 = _loc_1 + 1;
            }
            var _loc_2:* = 0;
            _loc_1 = 0;
            while (_loc_1 < this.tradegoods.length)
            {
                
                if (this.playershipstatus[4][1][_loc_1] != null)
                {
                    if (this.playershipstatus[4][1][_loc_1] > 0)
                    {
                        this["i" + _loc_2].itemText.text = this.tradegoods[_loc_1].GoodsName + " (" + this.playershipstatus[4][1][_loc_1] + ")";
                        _loc_2 = _loc_2 + 1;
                        if (_loc_2 == 8)
                        {
                            break;
                        }
                    }
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_playRegularClick()
        {
            if (this.gamesetting.soundvolume)
            {
                this.gameSounds.regularClick.play();
            }
            return;
        }// end function

        public function parseInTradegoods(param1)
        {
            var _loc_5:* = undefined;
            var _loc_2:* = param1.split("`");
            var _loc_3:* = 3;
            var _loc_4:* = 0;
            while (_loc_3 < (_loc_2.length - 1))
            {
                
                _loc_5 = _loc_2[_loc_3].split(":");
                if (_loc_5[1] == "S")
                {
                    _loc_4 = this.purchasableitems.length;
                    this.purchasableitems[_loc_4] = new Array();
                    this.purchasableitems[_loc_4][0] = Number(_loc_5[0]);
                    this.purchasableitems[_loc_4][1] = Number(_loc_5[2]);
                }
                if (_loc_5[1] == "B")
                {
                    _loc_4 = this.sellableitems.length;
                    this.sellableitems[_loc_4] = new Array();
                    this.sellableitems[_loc_4][0] = Number(_loc_5[0]);
                    this.sellableitems[_loc_4][1] = Number(_loc_5[2]);
                }
                _loc_3 = _loc_3 + 1;
            }
            this.func_setButtons();
            return;
        }// end function

        public function func_setButtonsListeners()
        {
            var _loc_1:* = 0;
            _loc_1 = 0;
            while (_loc_1 < 8)
            {
                
                this["p" + _loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_ListItemClick);
                this["s" + _loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_ListItemClick);
                this["i" + _loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_ListItemClick);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function FormatNumber(param1) : String
        {
            param1 = Number(param1);
            if (isNaN(param1))
            {
                param1 = 0;
            }
            param1 = Math.round(param1);
            var _loc_2:* = false;
            var _loc_3:* = "";
            if (param1 < 0)
            {
                _loc_2 = true;
            }
            param1 = Math.abs(param1);
            param1 = String(param1);
            if (param1.length > 3)
            {
                _loc_3 = param1.substr(param1.length - 3);
                param1 = Number(param1);
                while (param1 > 999)
                {
                    
                    if (param1 > 999)
                    {
                        param1 = Math.floor(Number(param1) / 1000);
                        param1 = String(param1);
                        if (param1.length > 3)
                        {
                            _loc_3 = param1.substr(param1.length - 3) + "," + _loc_3;
                        }
                        else
                        {
                            _loc_3 = param1 + "," + _loc_3;
                        }
                        param1 = Number(param1);
                    }
                }
            }
            else
            {
                _loc_3 = param1;
            }
            if (_loc_2 == true)
            {
                _loc_3 = "-" + _loc_3;
            }
            return _loc_3;
        }// end function

        public function func_AcceptSale(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            if (this.CurrentMethod == "purchase")
            {
                this.playershipstatus[3][1] = this.playershipstatus[3][1] - Number(this.transactionWindow.quatityEntered.text) * this.CurrentItemPrice;
                if (isNaN(this.playershipstatus[4][1][this.purchasableitems[this.CurrenCellSelected][0]]) || this.playershipstatus[4][1][this.purchasableitems[this.CurrenCellSelected][0]] < 1)
                {
                    this.playershipstatus[4][1][this.purchasableitems[this.CurrenCellSelected][0]] = Number(this.transactionWindow.quatityEntered.text);
                }
                else
                {
                    this.playershipstatus[4][1][this.purchasableitems[this.CurrenCellSelected][0]] = this.playershipstatus[4][1][this.purchasableitems[this.CurrenCellSelected][0]] + Number(this.transactionWindow.quatityEntered.text);
                }
                trace("addedqty:" + this.playershipstatus[4][1][this.purchasableitems[this.CurrenCellSelected][0]] + "~" + Number(this.transactionWindow.quatityEntered.text) + "````" + this.playershipstatus[4][1]);
            }
            else if (this.CurrentMethod == "sale")
            {
                this.playershipstatus[3][1] = this.playershipstatus[3][1] + Number(this.transactionWindow.quatityEntered.text) * this.CurrentItemPrice;
                this.playershipstatus[4][1][this.sellableitems[this.CurrenCellSelected][0]] = this.playershipstatus[4][1][this.sellableitems[this.CurrenCellSelected][0]] - Number(this.transactionWindow.quatityEntered.text);
                trace("removedqty");
            }
            this.func_resetTrasaction();
            return;
        }// end function

        public function func_CancelSale(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_resetTrasaction();
            return;
        }// end function

        public function func_OnTradegoodsFrame(event:Event)
        {
            if (this.MaxUnits >= 0)
            {
                if (isNaN(Number(this.transactionWindow.quatityEntered.text)))
                {
                    this.transactionWindow.quatityEntered.text = "0";
                }
                this.transactionWindow.quatityEntered.text = Math.floor(Number(this.transactionWindow.quatityEntered.text));
                if (Number(this.transactionWindow.quatityEntered.text) < 0)
                {
                    this.transactionWindow.quatityEntered.text = "0";
                }
                if (Number(this.transactionWindow.quatityEntered.text) > this.MaxUnits)
                {
                    this.transactionWindow.quatityEntered.text = this.MaxUnits;
                }
                this.transactionWindow.finalAmount.text = this.FormatNumber(Number(this.transactionWindow.quatityEntered.text) * this.CurrentItemPrice);
            }
            return;
        }// end function

        function frame1()
        {
            this.blankersdafadsfdsaf = "";
            this.TotalCargoRoom = this.shiptype[this.playershipstatus[5][0]][4];
            this.CurrentMethod = "";
            this.CurrenCellSelected = -1;
            this.MaxUnits = -1;
            this.transactionWindow.gotoAndStop(2);
            addEventListener(Event.ENTER_FRAME, this.func_OnTradegoodsFrame);
            this.mysocket.send("TRG`INFO`BASE`" + this.playershipstatus[4][0] + "`~");
            trace("sent tradegoods for " + this.playershipstatus[4][0]);
            this.exit_button.actionlabel.text = "Exit";
            this.purchasableitems = new Array();
            this.sellableitems = new Array();
            this.func_setButtons();
            this.func_setButtonsListeners();
            stop();
            return;
        }// end function

    }
}
