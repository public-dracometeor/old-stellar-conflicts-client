﻿package sc346_fla
{
    import flash.display.*;

    dynamic public class aimessagepicturebox_25 extends flash.display.MovieClip
    {
        public var PictureToShow:Object;
        public var staticscreen:MovieClip;

        public function aimessagepicturebox_25()
        {
            addFrameScript(0, this.frame1, 38, this.frame39, 39, this.frame40, 40, this.frame41, 41, this.frame42, 42, this.frame43);
            return;
        }// end function

        function frame41()
        {
            stop();
            return;
        }// end function

        function frame42()
        {
            stop();
            return;
        }// end function

        function frame39()
        {
            if (this.PictureToShow == "INCOME")
            {
                gotoAndStop("INCOME");
            }
            else if (this.PictureToShow == "NEWS")
            {
                gotoAndStop("NEWS");
            }
            else if (this.PictureToShow == "INFO")
            {
                gotoAndStop("INFO");
            }
            else
            {
                stop();
            }
            return;
        }// end function

        function frame1()
        {
            return;
        }// end function

        function frame40()
        {
            stop();
            return;
        }// end function

        function frame43()
        {
            stop();
            return;
        }// end function

    }
}
