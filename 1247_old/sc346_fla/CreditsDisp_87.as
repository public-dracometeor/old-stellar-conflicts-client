﻿package sc346_fla
{
    import flash.display.*;
    import flash.text.*;

    dynamic public class CreditsDisp_87 extends flash.display.MovieClip
    {
        public var blankersdafadsfdsaf:String;
        public var credits:TextField;

        public function CreditsDisp_87()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        public function FormatNumber(param1) : String
        {
            param1 = Number(param1);
            if (isNaN(param1))
            {
                param1 = 0;
            }
            param1 = Math.round(param1);
            var _loc_2:* = false;
            var _loc_3:* = "";
            if (param1 < 0)
            {
                _loc_2 = true;
            }
            param1 = Math.abs(param1);
            param1 = String(param1);
            if (param1.length > 3)
            {
                _loc_3 = param1.substr(param1.length - 3);
                param1 = Number(param1);
                while (param1 > 999)
                {
                    
                    if (param1 > 999)
                    {
                        param1 = Math.floor(Number(param1) / 1000);
                        param1 = String(param1);
                        if (param1.length > 3)
                        {
                            _loc_3 = param1.substr(param1.length - 3) + "," + _loc_3;
                        }
                        else
                        {
                            _loc_3 = param1 + "," + _loc_3;
                        }
                        param1 = Number(param1);
                    }
                }
            }
            else
            {
                _loc_3 = param1;
            }
            if (_loc_2 == true)
            {
                _loc_3 = "-" + _loc_3;
            }
            return _loc_3;
        }// end function

        public function func_displayCredits(param1)
        {
            this.credits.text = "Credits: " + this.FormatNumber(param1);
            return;
        }// end function

        function frame1()
        {
            this.blankersdafadsfdsaf = "";
            this.credits.text = "";
            stop();
            return;
        }// end function

    }
}
