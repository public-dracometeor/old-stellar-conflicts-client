﻿package sc346_fla
{
    import flash.display.*;
    import flash.events.*;
    import flash.net.*;
    import flash.text.*;

    dynamic public class introselectionmovie_33 extends flash.display.MovieClip
    {
        public var lowsettings:SimpleButton;
        public var deaccel:TextField;
        public var back_button:SimpleButton;
        public var target:TextField;
        public var playGameButton:SimpleButton;
        public var continuebutton:SimpleButton;
        public var zoomout:TextField;
        public var gamesetting:Object;
        public var stellarLogo:SimpleButton;
        public var oldkeys:SimpleButton;
        public var newkeys:SimpleButton;
        public var missile:TextField;
        public var tright:TextField;
        public var afterburn:TextField;
        public var guns:TextField;
        public var geckoLogo:SimpleButton;
        public var zoomin:TextField;
        public var Eng1:MovieClip;
        public var Eng2:MovieClip;
        public var isFinished:Object;
        public var help:TextField;
        public var dock:TextField;
        public var forumsButton:SimpleButton;
        public var chat:TextField;
        public var gameSounds:Object;
        public var map:TextField;
        public var currkeystyle:TextField;
        public var highsetting:SimpleButton;
        public var playersList:TextField;
        public var accel:TextField;
        public var tleft:TextField;
        public var medsettings:SimpleButton;

        public function introselectionmovie_33()
        {
            addFrameScript(0, this.frame1, 1, this.frame2, 2, this.frame3, 3, this.frame4, 4, this.frame5, 5, this.frame6, 6, this.frame7);
            return;
        }// end function

        public function func_VisitGeckoGames(event:MouseEvent) : void
        {
            var _loc_2:* = new URLRequest("http://www.gecko-games.com");
            navigateToURL(_loc_2, "_blank");
            return;
        }// end function

        public function func_PlayGame(event:MouseEvent) : void
        {
            this.isFinished = true;
            this.func_playRegularClick();
            return;
        }// end function

        function frame5()
        {
            this.back_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_PreviousFrame);
            this.continuebutton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_NextFrame);
            stop();
            return;
        }// end function

        public function func_VisitStellarConflicts(event:MouseEvent) : void
        {
            var _loc_2:* = new URLRequest("http://www.stellar-conflicts.com");
            navigateToURL(_loc_2, "_blank");
            return;
        }// end function

        function frame3()
        {
            Object(root).gamesetting = this.gamesetting;
            Object(root).func_checkMusicSettings();
            this.currkeystyle.text = "New Style";
            this.displaykeys();
            stop();
            this.newkeys.addEventListener(MouseEvent.MOUSE_DOWN, this.func_SetToNew);
            this.oldkeys.addEventListener(MouseEvent.MOUSE_DOWN, this.func_SetToOld);
            this.continuebutton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_NextFrame);
            this.playGameButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_PlayGame);
            this.forumsButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_loadForums);
            return;
        }// end function

        public function func_SetToNew(event:MouseEvent) : void
        {
            this.currkeystyle.text = "New Style";
            Object(root).func_setToNewDefaultGameKeys();
            this.displaykeys();
            return;
        }// end function

        public function func_HighSettings(event:MouseEvent) : void
        {
            this.gamesetting.scrollingbckgrnd = true;
            this.gamesetting.showbckgrnd = true;
            this.gamesetting.totalstars = 20;
            this.gamesetting.soundvolume = true;
            this.gamesetting.gameMusic = true;
            this.gamesetting.CombatSounds = true;
            this.func_playRegularClick();
            nextFrame();
            return;
        }// end function

        public function returnKeyValueOfCode(param1)
        {
            var _loc_2:* = "";
            if (param1 == 65)
            {
                _loc_2 = "A";
            }
            else if (param1 == 66)
            {
                _loc_2 = "B";
            }
            else if (param1 == 67)
            {
                _loc_2 = "C";
            }
            else if (param1 == 68)
            {
                _loc_2 = "D";
            }
            else if (param1 == 69)
            {
                _loc_2 = "E";
            }
            else if (param1 == 70)
            {
                _loc_2 = "F";
            }
            else if (param1 == 71)
            {
                _loc_2 = "G";
            }
            else if (param1 == 72)
            {
                _loc_2 = "H";
            }
            else if (param1 == 73)
            {
                _loc_2 = "I";
            }
            else if (param1 == 74)
            {
                _loc_2 = "J";
            }
            else if (param1 == 75)
            {
                _loc_2 = "K";
            }
            else if (param1 == 76)
            {
                _loc_2 = "L";
            }
            else if (param1 == 77)
            {
                _loc_2 = "M";
            }
            else if (param1 == 78)
            {
                _loc_2 = "N";
            }
            else if (param1 == 79)
            {
                _loc_2 = "O";
            }
            else if (param1 == 80)
            {
                _loc_2 = "P";
            }
            else if (param1 == 81)
            {
                _loc_2 = "Q";
            }
            else if (param1 == 82)
            {
                _loc_2 = "R";
            }
            else if (param1 == 83)
            {
                _loc_2 = "S";
            }
            else if (param1 == 84)
            {
                _loc_2 = "T";
            }
            else if (param1 == 85)
            {
                _loc_2 = "U";
            }
            else if (param1 == 86)
            {
                _loc_2 = "V";
            }
            else if (param1 == 87)
            {
                _loc_2 = "W";
            }
            else if (param1 == 88)
            {
                _loc_2 = "X";
            }
            else if (param1 == 89)
            {
                _loc_2 = "Y";
            }
            else if (param1 == 90)
            {
                _loc_2 = "Z";
            }
            else if (param1 == 48)
            {
                _loc_2 = "0";
            }
            else if (param1 == 49)
            {
                _loc_2 = "1";
            }
            else if (param1 == 50)
            {
                _loc_2 = "2";
            }
            else if (param1 == 51)
            {
                _loc_2 = "3";
            }
            else if (param1 == 52)
            {
                _loc_2 = "4";
            }
            else if (param1 == 53)
            {
                _loc_2 = "5";
            }
            else if (param1 == 54)
            {
                _loc_2 = "6";
            }
            else if (param1 == 55)
            {
                _loc_2 = "7";
            }
            else if (param1 == 56)
            {
                _loc_2 = "8";
            }
            else if (param1 == 57)
            {
                _loc_2 = "9";
            }
            else if (param1 == 8)
            {
                _loc_2 = "Backspace";
            }
            else if (param1 == 9)
            {
                _loc_2 = "Tab";
            }
            else if (param1 == 13)
            {
                _loc_2 = "Enter";
            }
            else if (param1 == 16)
            {
                _loc_2 = "Shift";
            }
            else if (param1 == 17)
            {
                _loc_2 = "Control";
            }
            else if (param1 == 20)
            {
                _loc_2 = "Caps Lock";
            }
            else if (param1 == 27)
            {
                _loc_2 = "Escape";
            }
            else if (param1 == 32)
            {
                _loc_2 = "Space Bar";
            }
            else if (param1 == 33)
            {
                _loc_2 = "Page Up";
            }
            else if (param1 == 34)
            {
                _loc_2 = "Page Down";
            }
            else if (param1 == 35)
            {
                _loc_2 = "End";
            }
            else if (param1 == 36)
            {
                _loc_2 = "Home";
            }
            else if (param1 == 37)
            {
                _loc_2 = "Left Arrow";
            }
            else if (param1 == 38)
            {
                _loc_2 = "Up Arrow";
            }
            else if (param1 == 39)
            {
                _loc_2 = "Right Arrow";
            }
            else if (param1 == 40)
            {
                _loc_2 = "Down Arrow";
            }
            else if (param1 == 45)
            {
                _loc_2 = "Insert";
            }
            else if (param1 == 46)
            {
                _loc_2 = "Delete";
            }
            else if (param1 == 144)
            {
                _loc_2 = "Num Lock";
            }
            else if (param1 == 145)
            {
                _loc_2 = "ScrLck";
            }
            else if (param1 == 19)
            {
                _loc_2 = "Pause/Break";
            }
            else if (param1 == 186)
            {
                _loc_2 = "; or :";
            }
            else if (param1 == 187)
            {
                _loc_2 = "= or +";
            }
            else if (param1 == 189)
            {
                _loc_2 = "_ or -";
            }
            else if (param1 == 191)
            {
                _loc_2 = "/ or ?";
            }
            else if (param1 == 192)
            {
                _loc_2 = "` or ~";
            }
            else if (param1 == 219)
            {
                _loc_2 = "[ or {";
            }
            else if (param1 == 220)
            {
                _loc_2 = " or |";
            }
            else if (param1 == 221)
            {
                _loc_2 = "] or }";
            }
            else if (param1 == 222)
            {
                _loc_2 = "\" of \'";
            }
            else if (param1 == 188)
            {
                _loc_2 = ", or <";
            }
            else if (param1 == 190)
            {
                _loc_2 = ". or >";
            }
            else if (param1 == 191)
            {
                _loc_2 = "/ or ?";
            }
            return _loc_2;
        }// end function

        public function SetKeyConfiguration(param1)
        {
            this.displaykeys();
            return;
        }// end function

        public function displaykeys()
        {
            this.gamesetting = Object(root).gamesetting;
            this.accel.text = this.returnKeyValueOfCode(this.gamesetting.accelkey);
            this.tright.text = this.returnKeyValueOfCode(this.gamesetting.turnrightkey);
            this.tleft.text = this.returnKeyValueOfCode(this.gamesetting.turnleftkey);
            this.deaccel.text = this.returnKeyValueOfCode(this.gamesetting.deaccelkey);
            this.afterburn.text = this.returnKeyValueOfCode(this.gamesetting.afterburnerskey);
            this.guns.text = this.returnKeyValueOfCode(this.gamesetting.gunskey);
            this.missile.text = this.returnKeyValueOfCode(this.gamesetting.missilekey);
            this.dock.text = this.returnKeyValueOfCode(this.gamesetting.dockkey);
            this.target.text = this.returnKeyValueOfCode(this.gamesetting.targeterkey);
            this.map.text = this.returnKeyValueOfCode(this.gamesetting.MapKey);
            this.playersList.text = "O";
            this.chat.text = "ENTER";
            this.zoomin.text = ", or < ";
            this.zoomout.text = ". or > ";
            this.help.text = "H";
            return;
        }// end function

        public function func_SetToOld(event:MouseEvent) : void
        {
            this.currkeystyle.text = "Old Style";
            Object(root).func_setToOldGameKeys();
            this.displaykeys();
            return;
        }// end function

        public function func_playRegularClick()
        {
            if (this.gamesetting.soundvolume)
            {
                this.gamesetting.MenuChannelSound = this.gameSounds.regularClick.play();
                this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
            }
            return;
        }// end function

        public function func_loadForums(event:MouseEvent) : void
        {
            var _loc_2:* = new URLRequest("http://forum.gecko-games.com");
            navigateToURL(_loc_2, "_blank");
            this.func_playRegularClick();
            return;
        }// end function

        public function func_PreviousFrame(event:MouseEvent) : void
        {
            prevFrame();
            this.func_playRegularClick();
            return;
        }// end function

        public function func_NextFrame(event:MouseEvent) : void
        {
            nextFrame();
            this.func_playRegularClick();
            return;
        }// end function

        function frame2()
        {
            this.gamesetting = Object(root).gamesetting;
            this.highsetting.addEventListener(MouseEvent.MOUSE_DOWN, this.func_HighSettings);
            this.medsettings.addEventListener(MouseEvent.MOUSE_DOWN, this.func_MedSettings);
            this.lowsettings.addEventListener(MouseEvent.MOUSE_DOWN, this.func_LowSettings);
            return;
        }// end function

        function frame4()
        {
            this.back_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_PreviousFrame);
            this.continuebutton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_NextFrame);
            stop();
            return;
        }// end function

        function frame6()
        {
            this.back_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_PreviousFrame);
            this.continuebutton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_NextFrame);
            stop();
            return;
        }// end function

        function frame7()
        {
            this.back_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_PreviousFrame);
            this.playGameButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_PlayGame);
            stop();
            return;
        }// end function

        function frame1()
        {
            this.isFinished = false;
            stop();
            stop();
            this.stellarLogo.addEventListener(MouseEvent.MOUSE_DOWN, this.func_VisitStellarConflicts);
            this.geckoLogo.addEventListener(MouseEvent.MOUSE_DOWN, this.func_VisitGeckoGames);
            return;
        }// end function

        public function func_MedSettings(event:MouseEvent) : void
        {
            this.gamesetting.scrollingbckgrnd = false;
            this.gamesetting.showbckgrnd = true;
            this.gamesetting.totalstars = 15;
            this.gamesetting.soundvolume = true;
            this.gamesetting.gameMusic = true;
            this.gamesetting.CombatSounds = false;
            this.func_playRegularClick();
            nextFrame();
            return;
        }// end function

        public function func_LowSettings(event:MouseEvent) : void
        {
            this.gamesetting.scrollingbckgrnd = false;
            this.gamesetting.showbckgrnd = false;
            this.gamesetting.totalstars = 5;
            this.gamesetting.soundvolume = false;
            this.gamesetting.gameMusic = false;
            this.gamesetting.CombatSounds = false;
            this.func_playRegularClick();
            nextFrame();
            return;
        }// end function

    }
}
