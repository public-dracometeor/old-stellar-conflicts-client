﻿package sc346_fla
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;

    dynamic public class ShipHardwareScreen_116 extends flash.display.MovieClip
    {
        public var buyenergygen:MovieClip;
        public var missiles:MovieClip;
        public var gamesetting:Object;
        public var buyweapon:MovieClip;
        public var hardPointBox:Class;
        public var shieldgenerators:Object;
        public var TurrethardPointHolders:Object;
        public var informationWindow:MovieClip;
        public var selectedSpot:Object;
        public var EnergyGenPointHolders:Object;
        public var shipstopgun:Object;
        public var EnergyGenTypeBeingBought:Object;
        public var energycap:MovieClip;
        public var MissileDataHolders:Object;
        public var BluePrint:Class;
        public var numberOfTurrets:Object;
        public var numberOfMissilepoints:Object;
        public var specialshipitems:Object;
        public var currentvalueofgenerator:Object;
        public var guntype:Object;
        public var buyenergycap:MovieClip;
        public var specialsDisplay:MovieClip;
        public var q:Object;
        public var numberOfEnergyCapPoints:Object;
        public var energygenerators:Object;
        public var EnergyGenDataHolders:Object;
        public var sellingTurretPointBox:Object;
        public var availableitems:Object;
        public var currentlySelectedShieldGen:Object;
        public var numberOfHardpoints:Object;
        public var MissilePointHolders:Object;
        public var TurretsDisplayed:Object;
        public var currentlyDraggingGun:Object;
        public var GunsDisplayed:Object;
        public var playershipstatus:Object;
        public var EnergyCapDataHolders:Object;
        public var turretCostModifier:Object;
        public var missilepurchasewindow:MovieClip;
        public var specials:MovieClip;
        public var missilelocationmultiplier:Object;
        public var ShieldGenTypeBeingBought:Object;
        public var missileTypeBeingBought:Object;
        public var GunHolder:Object;
        public var TurretsHolder:Object;
        public var SpecialsDisplayed:Object;
        public var hardPointHolders:Object;
        public var missile:Object;
        public var gunBeingBought:Object;
        public var guns:MovieClip;
        public var MissilesDisplayed:Object;
        public var EnergyCapsDisplayed:Object;
        public var EnergyGensDisplayed:Object;
        public var ShieldGenPointHolders:Object;
        public var shieldgen:MovieClip;
        public var playershiptype:Object;
        public var TurretBeingBought:Object;
        public var SpecialListHolder:Object;
        public var shiptype:Object;
        public var energycapacitors:Object;
        public var buyshieldgen:MovieClip;
        public var buyspecial:MovieClip;
        public var turrets:MovieClip;
        public var currentlyDraggingTurret:Object;
        public var EnergyCapTypeBeingBought:Object;
        public var numberOfEnergyGenPoints:Object;
        public var currentlySeelctedMIssileBank:Object;
        public var currentvalueofEnergycap:Object;
        public var sellingHardPointBox:Object;
        public var maxBumberOfEnergySpecials:Object;
        public var DisplayScreen:MovieClip;
        public var ShieldGenDataHolders:Object;
        public var EnergyCapPointHolders:Object;
        public var buyingwindowaction:Object;
        public var energygen:MovieClip;
        public var DisplayBox:Class;
        public var numberOfShieldGenPoints:Object;
        public var shipstopgunTurret:Object;
        public var gameSounds:Object;
        public var shipblueprint:Object;
        public var exit_but:MovieClip;
        public var currentvalueofEnergygenerator:Object;
        public var sellweapon:MovieClip;
        public var ShieldGensDisplayed:Object;

        public function ShipHardwareScreen_116()
        {
            addFrameScript(0, this.frame1, 7, this.frame8, 18, this.frame19, 28, this.frame29, 44, this.frame45, 58, this.frame59, 76, this.frame77, 94, this.frame95);
            return;
        }// end function

        public function func_ResetButtonsToOff()
        {
            this.guns.gotoAndStop(2);
            this.missiles.gotoAndStop(2);
            this.shieldgen.gotoAndStop(2);
            this.energygen.gotoAndStop(2);
            this.energycap.gotoAndStop(2);
            this.specials.gotoAndStop(2);
            this.exit_but.gotoAndStop(2);
            this.turrets.gotoAndStop(2);
            try
            {
                Object(root).removeEventListener(MouseEvent.MOUSE_UP, this.func_SelectedHardpointReleased);
            }
            catch (error:Error)
            {
                try
                {
					Object(root).removeEventListener(MouseEvent.MOUSE_UP, this.func_SelectedTurretReleased);
                }
                catch (error:Error)
				{
				}
            }
            catch (error:Error)
            {
            }
            return;
        }// end function

        public function func_drawShieldGenDisplays()
        {
            this.buyshieldgen.visible = false;
            this.DisplayScreen.func_clearAllItems();
            this.func_InitShieldGensDisplay();
            this.func_ClearBlueBrint();
            this.func_drawShieldGenBrackets();
            this.func_drawShieldGenImages();
            return;
        }// end function

        public function func_drawEnergyCapDisplays()
        {
            this.buyenergycap.visible = false;
            this.DisplayScreen.func_clearAllItems();
            this.func_InitEnergyCapsDisplay();
            this.func_ClearBlueBrint();
            this.func_drawEnergyCapBrackets();
            this.func_drawEnergyCapImages();
            return;
        }// end function

        public function func_buyenergycap(event:MouseEvent) : void
        {
            this.EnergyCapTypeBeingBought = Number(event.target.parent.name);
            var _loc_2:* = this.energycapacitors[this.EnergyCapTypeBeingBought][2] - this.currentvalueofEnergycap;
            if (_loc_2 <= this.playershipstatus[3][1])
            {
                this.buyenergycap.question.text = "Buy: " + this.energycapacitors[this.EnergyCapTypeBeingBought][1] + " \r";
                this.buyenergycap.question.text = this.buyenergycap.question.text + ("For: " + _loc_2);
                this.buyenergycap.visible = true;
                this.func_playRegularClick();
            }
            else
            {
                this.buyenergycap.visible = false;
            }
            return;
        }// end function

        public function func_SelectedHardpointDrag(event:MouseEvent) : void
        {
            var _loc_2:* = Number(event.target.parent.name);
            if (!isNaN(_loc_2))
            {
                this.func_playRegularClick();
                this.currentlyDraggingGun = _loc_2;
                this.GunHolder[_loc_2].startDrag(true);
            }
            return;
        }// end function

        public function func_acceptEnergyGenPurchase(event:MouseEvent) : void
        {
            var _loc_2:* = this.energygenerators[this.EnergyGenTypeBeingBought][2] - this.currentvalueofEnergygenerator;
            this.playershipstatus[3][1] = this.playershipstatus[3][1] - _loc_2;
            this.playershipstatus[1][0] = this.EnergyGenTypeBeingBought;
            this.func_playRegularClick();
            this.func_drawEnergyGenDisplays();
            return;
        }// end function

        public function func_cancelSale(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_drawGunDisplays();
            return;
        }// end function

        public function func_DisplayBankInfo(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            this.missilepurchasewindow.visible = false;
            if (param1 < 0)
            {
                trace("1");
                this.informationWindow.information.text = "Select a Missile Bank";
            }
            else
            {
                trace("2");
                _loc_2 = "Selected Missile Bank: " + param1 + "\r";
                _loc_3 = "";
                _loc_4 = 0;
                _loc_5 = 0;
                while (_loc_5 < this.missile.length)
                {
                    
                    if (this.playershipstatus[7][param1][4][_loc_5] > 0)
                    {
                        _loc_3 = _loc_3 + (this.missile[_loc_5][6] + "  (" + this.playershipstatus[7][param1][4][_loc_5] + ")\r");
                        _loc_4 = _loc_4 + this.playershipstatus[7][param1][4][_loc_5];
                    }
                    _loc_5 = _loc_5 + 1;
                }
                _loc_6 = _loc_2;
                _loc_6 = _loc_6 + ("No. of Missiles (" + _loc_4 + "/" + this.playershipstatus[7][param1][5] + ")\r\r");
                _loc_6 = _loc_6 + ("Current Missiles Loaded\r" + _loc_3);
                this.informationWindow.information.text = _loc_6;
            }
            return;
        }// end function

        function frame19()
        {
            this.turretCostModifier = 2;
            this.DisplayScreen.func_clearAllItems();
            this.buyweapon.visible = false;
            this.sellweapon.visible = false;
            this.func_ClearBlueBrint();
            this.TurretsDisplayed = new Array();
            this.TurretsHolder = new Array();
            this.shipstopgunTurret = this.shiptype[this.playershiptype][6][3] + 1;
            this.currentlyDraggingTurret = -1;
            this.func_InitTurretsDisplay();
            this.numberOfTurrets = this.shiptype[this.playershiptype][5].length;
            this.TurrethardPointHolders = new Array();
            this.sellingTurretPointBox = new this.hardPointBox() as MovieClip;
            this.sellingTurretPointBox.hardpointname.text = "Sell Weapon";
            this.func_drawTurretDisplays();
            Object(root).addEventListener(MouseEvent.MOUSE_UP, this.func_SelectedTurretReleased);
            this.sellweapon.yes_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_acceptTurretSale);
            this.sellweapon.no_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_cancelTurretSale);
            this.TurretBeingBought = -1;
            this.buyweapon.yes_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_acceptTurretPurchase);
            this.buyweapon.no_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_cancelTurretPurchase);
            return;
        }// end function

        public function func_MissilesSelectionMade(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_ResetButtonsToOff();
            this.missiles.gotoAndStop(1);
            gotoAndStop("missiles");
            return;
        }// end function

        public function func_TurretsHolderImages()
        {
            var _loc_3:* = null;
            this.TurretsHolder = new Array();
            var _loc_1:* = this.shiptype[this.playershiptype][3][4];
            var _loc_2:* = 0;
            while (_loc_2 < this.numberOfTurrets)
            {
                
                if (!isNaN(this.playershipstatus[8][_loc_2][0]))
                {
                    _loc_3 = getDefinitionByName("hardwarescreengunbracketguntype" + this.playershipstatus[8][_loc_2][0]) as Class;
                }
                else
                {
                    _loc_3 = getDefinitionByName("hardwarescreengunbracketguntypenone") as Class;
                }
                this.TurretsHolder[_loc_2] = new _loc_3 as MovieClip;
                this.TurretsHolder[_loc_2].x = this.shiptype[this.playershiptype][5][_loc_2][0] * _loc_1;
                this.TurretsHolder[_loc_2].y = this.shiptype[this.playershiptype][5][_loc_2][1] * _loc_1;
                this.TurretsHolder[_loc_2].scaleX = 0.75;
                this.TurretsHolder[_loc_2].scaleY = 0.75;
                if (!isNaN(this.playershipstatus[8][_loc_2][0]))
                {
                    this.TurretsHolder[_loc_2].itemname.text = this.guntype[this.playershipstatus[8][_loc_2][0]][6];
                }
                this.TurretsHolder[_loc_2].name = _loc_2;
                this.shipblueprint.addChild(this.TurretsHolder[_loc_2]);
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_drawEnergyGenImages()
        {
            var _loc_2:* = null;
            this.EnergyGenDataHolders = new Array();
            var _loc_1:* = 0;
            while (_loc_1 < this.numberOfEnergyGenPoints)
            {
                
                _loc_2 = getDefinitionByName("hardwarescreengunbracketenergygen0") as Class;
                this.EnergyGenDataHolders[_loc_1] = new _loc_2 as MovieClip;
                this.EnergyGenDataHolders[_loc_1].x = 0;
                this.EnergyGenDataHolders[_loc_1].y = 0;
                this.EnergyGenDataHolders[_loc_1].scaleX = 0.75;
                this.EnergyGenDataHolders[_loc_1].scaleY = 0.75;
                this.EnergyGenDataHolders[_loc_1].itemname.text = this.energygenerators[this.playershipstatus[1][0]][1];
                this.EnergyGenDataHolders[_loc_1].name = _loc_1;
                this.shipblueprint.addChild(this.EnergyGenDataHolders[_loc_1]);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_InitTurretsDisplay()
        {
            var _loc_2:* = undefined;
            var _loc_1:* = 0;
            while (_loc_1 < this.shipstopgunTurret)
            {
                
                if (this.guntype[_loc_1] != null)
                {
                    _loc_2 = _loc_1;
                    this.TurretsDisplayed[_loc_1] = new Object();
                    this.TurretsDisplayed[_loc_1].gunNumber = _loc_2;
                    this.TurretsDisplayed[_loc_1].cost = this.guntype[_loc_2][5] * this.turretCostModifier;
                    this.TurretsDisplayed[_loc_1].gunName = this.guntype[_loc_2][6];
                    this.TurretsDisplayed[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
                    this.TurretsDisplayed[_loc_1].DisplayBox.itemname.text = this.guntype[_loc_2][6];
                    this.TurretsDisplayed[_loc_1].DisplayBox.itemcost.text = "Cost:" + this.guntype[_loc_2][5] * this.turretCostModifier;
                    this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text = "Shld/Struct/Pierce\r  ";
                    this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text + (this.guntype[_loc_2][4] + " / " + this.guntype[_loc_2][7] + " / " + this.guntype[_loc_2][8] + " \r");
                    this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Energy Drain: " + this.guntype[_loc_2][3] + " \r");
                    this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Life Time: " + this.guntype[_loc_2][1] + "/s \r");
                    this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Rel:" + this.guntype[_loc_2][2] + "/s  \r");
                    this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.TurretsDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Spd:" + this.guntype[_loc_2][0] + "/s ");
                    this.TurretsDisplayed[_loc_1].DisplayBox.name = _loc_2;
                    this.TurretsDisplayed[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN, this.func_BuyNewlTurret);
                    this.DisplayScreen.func_addItem(this.TurretsDisplayed[_loc_1].DisplayBox);
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_InitEnergyCapsDisplay()
        {
            var _loc_3:* = undefined;
            this.currentvalueofEnergycap = Math.round(this.energycapacitors[this.playershipstatus[1][5]][2] / 6);
            this.EnergyCapsDisplayed = new Array();
            var _loc_1:* = 0;
            var _loc_2:* = this.shiptype[this.playershiptype][6][1] + 1;
            while (_loc_1 < _loc_2)
            {
                
                _loc_3 = _loc_1;
                this.EnergyCapsDisplayed[_loc_1] = new Object();
                this.EnergyCapsDisplayed[_loc_1].CapNumber = _loc_3;
                this.EnergyCapsDisplayed[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
                this.EnergyCapsDisplayed[_loc_1].DisplayBox.itemname.text = this.energycapacitors[_loc_3][1];
                if (this.playershipstatus[1][5] == _loc_3)
                {
                    this.EnergyCapsDisplayed[_loc_1].DisplayBox.itemcost.text = "Current";
                }
                else
                {
                    this.EnergyCapsDisplayed[_loc_1].DisplayBox.itemcost.text = "Cost:" + (this.energycapacitors[_loc_3][2] - this.currentvalueofEnergycap);
                    this.EnergyCapsDisplayed[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN, this.func_buyenergycap);
                }
                this.EnergyCapsDisplayed[_loc_1].DisplayBox.name = _loc_3;
                this.EnergyCapsDisplayed[_loc_1].DisplayBox.itemspecs.text = "Max Charge: " + this.energycapacitors[_loc_3][0] + " \r";
                this.DisplayScreen.func_addItem(this.EnergyCapsDisplayed[_loc_1].DisplayBox);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_InitShieldGensDisplay()
        {
            var _loc_3:* = undefined;
            this.currentvalueofgenerator = Math.round(this.shieldgenerators[this.playershipstatus[2][0]][5] / 6);
            this.ShieldGensDisplayed = new Array();
            var _loc_1:* = 0;
            var _loc_2:* = this.shiptype[this.playershiptype][6][0] + 1;
            while (_loc_1 < _loc_2)
            {
                
                _loc_3 = _loc_1;
                this.ShieldGensDisplayed[_loc_1] = new Object();
                this.ShieldGensDisplayed[_loc_1].genNumber = _loc_3;
                this.ShieldGensDisplayed[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
                this.ShieldGensDisplayed[_loc_1].DisplayBox.itemname.text = this.shieldgenerators[_loc_3][4];
                if (this.playershipstatus[2][0] == _loc_3)
                {
                    this.ShieldGensDisplayed[_loc_1].DisplayBox.itemcost.text = "Current";
                }
                else
                {
                    this.ShieldGensDisplayed[_loc_1].DisplayBox.itemcost.text = "Cost:" + (this.shieldgenerators[_loc_3][5] - this.currentvalueofgenerator);
                    this.ShieldGensDisplayed[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN, this.func_BuyShieldGen);
                }
                this.ShieldGensDisplayed[_loc_1].DisplayBox.name = _loc_3;
                this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text = "Recharge: " + this.shieldgenerators[_loc_3][1] + " \r";
                this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text = this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text + "Energy Drain/second: \r";
                this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text = this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Unit: " + this.shieldgenerators[_loc_3][2] + " \r");
                this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text = this.ShieldGensDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Full Charge: " + this.shieldgenerators[_loc_3][3] + " \r");
                this.DisplayScreen.func_addItem(this.ShieldGensDisplayed[_loc_1].DisplayBox);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        function frame1()
        {
            this.DisplayBox = getDefinitionByName("hardwareitembox") as Class;
            this.hardPointBox = getDefinitionByName("hardwarescreengunbracket") as Class;
            this.playershiptype = this.playershipstatus[5][0];
            this.turrets.addEventListener(MouseEvent.MOUSE_DOWN, this.func_TurretsSelectionWasMade);
            this.guns.addEventListener(MouseEvent.MOUSE_DOWN, this.func_GunsSelectionMade);
            this.missiles.addEventListener(MouseEvent.MOUSE_DOWN, this.func_MissilesSelectionMade);
            this.shieldgen.addEventListener(MouseEvent.MOUSE_DOWN, this.func_ShieldSelectionMade);
            this.energygen.addEventListener(MouseEvent.MOUSE_DOWN, this.func_EnergyGenSelectionMade);
            this.energycap.addEventListener(MouseEvent.MOUSE_DOWN, this.func_EnergyCapSelectionMade);
            this.specials.addEventListener(MouseEvent.MOUSE_DOWN, this.func_SpecialsSelectionMade);
            this.turrets.label.text = "Turrets";
            this.guns.label.text = "Guns";
            this.missiles.label.text = "Missiles";
            this.shieldgen.label.text = "Shield Generators";
            this.energygen.label.text = "Energy Generators";
            this.energycap.label.text = "Energy Capacitors";
            this.specials.label.text = "Specials";
            this.exit_but.label.text = "Exit";
            this.func_ResetButtonsToOff();
            this.guns.gotoAndStop(1);
            this.BluePrint = getDefinitionByName("shiptype" + this.playershiptype + "blueprint") as Class;
            this.shipblueprint = new this.BluePrint() as MovieClip;
            addChild(this.shipblueprint);
            this.shipblueprint.x = 450;
            this.shipblueprint.y = 300;
            if (this.shiptype[this.playershiptype][5].length < 1)
            {
                this.turrets.visible = false;
            }
            return;
        }// end function

        public function func_drawEnergyGenBrackets()
        {
            this.EnergyGenPointHolders = new Array();
            var _loc_1:* = 0;
            while (_loc_1 < this.numberOfEnergyGenPoints)
            {
                
                this.EnergyGenPointHolders[_loc_1] = new this.hardPointBox() as MovieClip;
                this.EnergyGenPointHolders[_loc_1].x = 0;
                this.EnergyGenPointHolders[_loc_1].y = 0;
                this.EnergyGenPointHolders[_loc_1].hardpointname.text = "Generator";
                this.EnergyGenPointHolders[_loc_1].scaleX = 0.75;
                this.EnergyGenPointHolders[_loc_1].scaleY = 0.75;
                this.EnergyGenPointHolders[_loc_1].name = _loc_1;
                this.shipblueprint.addChild(this.EnergyGenPointHolders[_loc_1]);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_acceptShiledGenPurchase(event:MouseEvent) : void
        {
            var _loc_2:* = this.shieldgenerators[this.ShieldGenTypeBeingBought][5] - this.currentvalueofgenerator;
            this.playershipstatus[3][1] = this.playershipstatus[3][1] - _loc_2;
            this.playershipstatus[2][0] = this.ShieldGenTypeBeingBought;
            this.func_playRegularClick();
            this.func_drawShieldGenDisplays();
            return;
        }// end function

        function frame29()
        {
            this.DisplayScreen.func_clearAllItems();
            this.func_ClearBlueBrint();
            this.MissilesDisplayed = new Array();
            this.currentlySeelctedMIssileBank = 0;
            this.func_InitMissilesDisplay();
            this.missilelocationmultiplier = this.shiptype[this.playershiptype][3][4];
            this.numberOfMissilepoints = this.shiptype[this.playershiptype][7].length;
            this.MissilePointHolders = new Array();
            this.MissileDataHolders = new Array();
            this.func_drawMissileDisplays();
            this.missileTypeBeingBought = -1;
            this.missilepurchasewindow.yesbutton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_acceptMissilePurchase);
            this.missilepurchasewindow.nobutton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_cancelMissilePurchase);
            return;
        }// end function

        public function func_GunsSelectionWasMade()
        {
            this.func_ResetButtonsToOff();
            this.guns.gotoAndStop(1);
            gotoAndStop("guns");
            return;
        }// end function

        function frame8()
        {
            this.DisplayScreen.func_clearAllItems();
            this.buyweapon.visible = false;
            this.sellweapon.visible = false;
            this.func_ClearBlueBrint();
            this.GunsDisplayed = new Array();
            this.GunHolder = new Array();
            this.shipstopgun = this.shiptype[this.playershiptype][6][3] + 1;
            this.currentlyDraggingGun = -1;
            this.func_InitGunsDisplay();
            this.numberOfHardpoints = this.shiptype[this.playershiptype][2].length;
            this.hardPointHolders = new Array();
            this.sellingHardPointBox = new this.hardPointBox() as MovieClip;
            this.sellingHardPointBox.hardpointname.text = "Sell Weapon";
            this.func_drawGunDisplays();
            Object(root).addEventListener(MouseEvent.MOUSE_UP, this.func_SelectedHardpointReleased);
            this.sellweapon.yes_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_acceptSale);
            this.sellweapon.no_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_cancelSale);
            this.gunBeingBought = -1;
            this.buyweapon.yes_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_acceptGunPurchase);
            this.buyweapon.no_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_cancelGunPurchase);
            stop();
            return;
        }// end function

        public function func_ClearBlueBrint()
        {
            while (this.shipblueprint.numChildren > 1)
            {
                
                this.shipblueprint.removeChildAt(1);
            }
            return;
        }// end function

        public function func_acceptTurretSale(event:MouseEvent) : void
        {
            this.playershipstatus[3][1] = this.playershipstatus[3][1] + Math.round(this.guntype[this.playershipstatus[8][this.currentlyDraggingTurret][0]][5] / 6) * this.turretCostModifier;
            this.playershipstatus[8][this.currentlyDraggingTurret][0] = "none";
            this.func_playRegularClick();
            this.func_drawTurretDisplays();
            return;
        }// end function

        public function func_drawShieldGenImages()
        {
            var _loc_2:* = null;
            this.ShieldGenDataHolders = new Array();
            var _loc_1:* = 0;
            while (_loc_1 < this.numberOfShieldGenPoints)
            {
                
                _loc_2 = getDefinitionByName("hardwarescreengunbracketshieldtype0") as Class;
                this.ShieldGenDataHolders[_loc_1] = new _loc_2 as MovieClip;
                this.ShieldGenDataHolders[_loc_1].x = 0;
                this.ShieldGenDataHolders[_loc_1].y = 0;
                this.ShieldGenDataHolders[_loc_1].scaleX = 0.75;
                this.ShieldGenDataHolders[_loc_1].scaleY = 0.75;
                this.ShieldGenDataHolders[_loc_1].itemname.text = this.shieldgenerators[this.playershipstatus[2][0]][4];
                this.ShieldGenDataHolders[_loc_1].name = _loc_1;
                this.shipblueprint.addChild(this.ShieldGenDataHolders[_loc_1]);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_acceptEnergyCapPurchase(event:MouseEvent) : void
        {
            var _loc_2:* = this.energycapacitors[this.EnergyCapTypeBeingBought][2] - this.currentvalueofEnergycap;
            this.playershipstatus[3][1] = this.playershipstatus[3][1] - _loc_2;
            this.playershipstatus[1][5] = this.EnergyCapTypeBeingBought;
            this.func_playRegularClick();
            this.func_drawEnergyCapDisplays();
            return;
        }// end function

        public function func_InitMissilesDisplay()
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_1:* = 0;
            while (_loc_1 < this.missile.length)
            {
                
                _loc_2 = _loc_1;
                this.MissilesDisplayed[_loc_1] = new Object();
                this.GunsDisplayed[_loc_1] = new Object();
                this.MissilesDisplayed[_loc_1].missileNumber = _loc_2;
                this.MissilesDisplayed[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
                this.MissilesDisplayed[_loc_1].DisplayBox.itemname.text = this.missile[_loc_2][6];
                this.MissilesDisplayed[_loc_1].DisplayBox.itemcost.text = "Cost:" + this.missile[_loc_2][5];
                this.MissilesDisplayed[_loc_1].DisplayBox.name = _loc_2;
                this.MissilesDisplayed[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN, this.func_BuyMissiles);
                if (this.missile[_loc_2][8] == "EMP" || this.missile[_loc_2][8] == "DISRUPTER")
                {
                    if (this.missile[_loc_2][8] == "EMP")
                    {
                        _loc_3 = "E.M.P.";
                    }
                    else if (this.missile[_loc_2][8] == "DISRUPTER")
                    {
                        _loc_3 = "Disrupter";
                    }
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = "Reload :" + this.missile[_loc_2][3] / 1000 + "/s \r";
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Life Time: " + this.missile[_loc_2][2] / 1000 + "/s \r");
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Speed :" + this.missile[_loc_2][0] + "/s \r");
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Duration: " + Math.floor(this.missile[_loc_2][9] / 1000) + " \r");
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Guidance: " + _loc_3);
                }
                if (this.missile[_loc_2][8] == "DUMBFIRE" || this.missile[_loc_2][8] == "PIERCE")
                {
                    if (this.missile[_loc_2][8] == "DUMBFIRE")
                    {
                        _loc_3 = "Dumbfire";
                    }
                    else if (this.missile[_loc_2][8] == "SEEKER")
                    {
                        _loc_3 = "Seeker";
                    }
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = "Damage: " + this.missile[_loc_2][4] + " \r";
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Reload :" + this.missile[_loc_2][3] / 1000 + "/s \r");
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Life Time: " + this.missile[_loc_2][2] / 1000 + "/s \r");
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Speed :" + this.missile[_loc_2][0] + "/s \r");
                    this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text = this.MissilesDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Guidance: " + _loc_3);
                }
                this.DisplayScreen.func_addItem(this.MissilesDisplayed[_loc_1].DisplayBox);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_SpecialsSelectionMade(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_ResetButtonsToOff();
            this.specials.gotoAndStop(1);
            gotoAndStop("Specials");
            return;
        }// end function

        public function func_EnergyCapSelectionMade(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_ResetButtonsToOff();
            this.energycap.gotoAndStop(1);
            gotoAndStop("EnergyCapacitors");
            return;
        }// end function

        public function func_SelectedHardpointReleased(event:MouseEvent) : void
        {
            var TemGunHolder:*;
            var event:* = event;
            var gunReleasedon:*;
            try
            {
                gunReleasedon = Number(event.target.parent.name);
                if (this.currentlyDraggingGun != -1)
                {
                    if (!isNaN(gunReleasedon))
                    {
                        trace(gunReleasedon);
                        if (gunReleasedon == -1)
                        {
                            if (!isNaN(this.playershipstatus[0][this.currentlyDraggingGun][0]))
                            {
                                this.sellweapon.visible = true;
                                this.sellweapon.question.text = "Sell: " + this.guntype[this.playershipstatus[0][this.currentlyDraggingGun][0]][6] + " \r" + "For: " + Math.round(this.guntype[this.playershipstatus[0][this.currentlyDraggingGun][0]][5] / 6);
                                this.func_playRegularClick();
                                this.GunHolder[this.currentlyDraggingGun].stopDrag();
                            }
                            else
                            {
                                gunReleasedon = 0;
                            }
                        }
                        else if (this.currentlyDraggingGun != gunReleasedon)
                        {
                            TemGunHolder = this.playershipstatus[0][this.currentlyDraggingGun];
                            this.playershipstatus[0][this.currentlyDraggingGun] = this.playershipstatus[0][gunReleasedon];
                            this.playershipstatus[0][gunReleasedon] = TemGunHolder;
                            this.func_playRegularClick();
                        }
                    }
                }
                if (gunReleasedon != -1)
                {
                    this.func_drawGunDisplays();
                }
            }
            catch (error:Error)
            {
            }
            return;
        }// end function

        public function func_SelectedTurretDrag(event:MouseEvent) : void
        {
            var _loc_2:* = Number(event.target.parent.name);
            if (!isNaN(_loc_2))
            {
                this.func_playRegularClick();
                this.currentlyDraggingTurret = _loc_2;
                this.TurretsHolder[_loc_2].startDrag(true);
            }
            return;
        }// end function

        public function func_playRegularClick()
        {
            if (this.gamesetting.soundvolume)
            {
                this.gamesetting.MenuChannelSound = this.gameSounds.regularClick.play();
                this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
            }
            return;
        }// end function

        public function func_drawTurretDisplays()
        {
            this.func_ClearBlueBrint();
            this.func_TurretsHolderImages();
            this.func_drawTurretBrackets();
            this.currentlyDraggingTurret = -1;
            this.sellweapon.visible = false;
            return;
        }// end function

        public function func_cancelGunPurchase(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.buyweapon.visible = false;
            return;
        }// end function

        function frame59()
        {
            this.EnergyGensDisplayed = new Array();
            this.currentvalueofEnergygenerator = 0;
            this.numberOfEnergyGenPoints = 1;
            this.EnergyGenPointHolders = new Array();
            this.EnergyGenDataHolders = new Array();
            this.func_drawEnergyGenDisplays();
            this.EnergyGenTypeBeingBought = -1;
            this.buyenergygen.yes_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_acceptEnergyGenPurchase);
            this.buyenergygen.no_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_cancelEnergyGenPurchase);
            return;
        }// end function

        function frame45()
        {
            this.ShieldGensDisplayed = new Array();
            this.currentlySelectedShieldGen = 0;
            this.currentvalueofgenerator = 0;
            this.numberOfShieldGenPoints = 1;
            this.ShieldGenPointHolders = new Array();
            this.ShieldGenDataHolders = new Array();
            this.func_drawShieldGenDisplays();
            this.ShieldGenTypeBeingBought = -1;
            this.buyshieldgen.yes_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_acceptShiledGenPurchase);
            this.buyshieldgen.no_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_cancelShiledGenPurchase);
            return;
        }// end function

        public function func_acceptMissilePurchase(event:MouseEvent) : void
        {
            var _loc_2:* = 0;
            var _loc_3:* = Number(this.missilepurchasewindow.quantity.text);
            var _loc_4:* = this.missile[this.missileTypeBeingBought][5];
            trace(_loc_3 + "`" + _loc_4);
            if (!isNaN(_loc_3) && !isNaN(_loc_4))
            {
                trace("RAN");
                this.playershipstatus[3][1] = this.playershipstatus[3][1] - _loc_4;
                this.playershipstatus[7][this.currentlySeelctedMIssileBank][4][this.missileTypeBeingBought] = this.playershipstatus[7][this.currentlySeelctedMIssileBank][4][this.missileTypeBeingBought] + _loc_3;
            }
            this.func_playRegularClick();
            this.func_drawMissileDisplays();
            return;
        }// end function

        public function func_drawSpecialsDisplays()
        {
            while (this.specialsDisplay.numChildren > 2)
            {
                
                this.specialsDisplay.removeChildAt(2);
            }
            this.buyspecial.visible = false;
            this.DisplayScreen.func_clearAllItems();
            this.func_InitSpecialsDisplay();
            this.func_ClearBlueBrint();
            this.func_drawSpecialsOnShip();
            return;
        }// end function

        public function func_cancelEnergyGenPurchase(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_drawEnergyGenDisplays();
            return;
        }// end function

        public function func_drawEnergyCapBrackets()
        {
            this.EnergyCapPointHolders = new Array();
            var _loc_1:* = 0;
            while (_loc_1 < this.numberOfEnergyCapPoints)
            {
                
                this.EnergyCapPointHolders[_loc_1] = new this.hardPointBox() as MovieClip;
                this.EnergyCapPointHolders[_loc_1].x = 0;
                this.EnergyCapPointHolders[_loc_1].y = 0;
                this.EnergyCapPointHolders[_loc_1].hardpointname.text = "Capacitor";
                this.EnergyCapPointHolders[_loc_1].scaleX = 0.75;
                this.EnergyCapPointHolders[_loc_1].scaleY = 0.75;
                this.EnergyCapPointHolders[_loc_1].name = _loc_1;
                this.shipblueprint.addChild(this.EnergyCapPointHolders[_loc_1]);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_drawShieldGenBrackets()
        {
            this.ShieldGenPointHolders = new Array();
            var _loc_1:* = 0;
            while (_loc_1 < this.numberOfShieldGenPoints)
            {
                
                this.ShieldGenPointHolders[_loc_1] = new this.hardPointBox() as MovieClip;
                this.ShieldGenPointHolders[_loc_1].x = 0;
                this.ShieldGenPointHolders[_loc_1].y = 0;
                this.ShieldGenPointHolders[_loc_1].hardpointname.text = "Generator";
                this.ShieldGenPointHolders[_loc_1].scaleX = 0.75;
                this.ShieldGenPointHolders[_loc_1].scaleY = 0.75;
                this.ShieldGenPointHolders[_loc_1].name = _loc_1;
                this.shipblueprint.addChild(this.ShieldGenPointHolders[_loc_1]);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_BuyShieldGen(event:MouseEvent) : void
        {
            this.ShieldGenTypeBeingBought = Number(event.target.parent.name);
            var _loc_2:* = this.shieldgenerators[this.ShieldGenTypeBeingBought][5] - this.currentvalueofgenerator;
            if (_loc_2 <= this.playershipstatus[3][1])
            {
                this.buyshieldgen.question.text = "Buy: " + this.shieldgenerators[this.ShieldGenTypeBeingBought][4] + " \r";
                this.buyshieldgen.question.text = this.buyshieldgen.question.text + ("For: " + _loc_2);
                this.buyshieldgen.visible = true;
                this.func_playRegularClick();
            }
            else
            {
                this.buyshieldgen.visible = false;
            }
            return;
        }// end function

        function frame77()
        {
            this.EnergyCapsDisplayed = new Array();
            this.currentvalueofEnergycap = 0;
            this.numberOfEnergyCapPoints = 1;
            this.EnergyCapPointHolders = new Array();
            this.EnergyCapDataHolders = new Array();
            this.func_drawEnergyCapDisplays();
            this.EnergyCapTypeBeingBought = -1;
            this.buyenergycap.yes_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_acceptEnergyCapPurchase);
            this.buyenergycap.no_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_cancelEnergyCapPurchase);
            return;
        }// end function

        public function func_gunHolderImages()
        {
            var _loc_3:* = null;
            this.GunHolder = new Array();
            var _loc_1:* = this.shiptype[this.playershiptype][3][4];
            var _loc_2:* = 0;
            while (_loc_2 < this.numberOfHardpoints)
            {
                
                if (!isNaN(this.playershipstatus[0][_loc_2][0]))
                {
                    _loc_3 = getDefinitionByName("hardwarescreengunbracketguntype" + this.playershipstatus[0][_loc_2][0]) as Class;
                }
                else
                {
                    _loc_3 = getDefinitionByName("hardwarescreengunbracketguntypenone") as Class;
                }
                this.GunHolder[_loc_2] = new _loc_3 as MovieClip;
                this.GunHolder[_loc_2].x = this.shiptype[this.playershiptype][2][_loc_2][0] * _loc_1;
                this.GunHolder[_loc_2].y = this.shiptype[this.playershiptype][2][_loc_2][1] * _loc_1;
                this.GunHolder[_loc_2].scaleX = 0.75;
                this.GunHolder[_loc_2].scaleY = 0.75;
                if (!isNaN(this.playershipstatus[0][_loc_2][0]))
                {
                    this.GunHolder[_loc_2].itemname.text = this.guntype[this.playershipstatus[0][_loc_2][0]][6];
                }
                this.GunHolder[_loc_2].name = _loc_2;
                this.shipblueprint.addChild(this.GunHolder[_loc_2]);
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_MissileBankSelected(event:MouseEvent) : void
        {
            var bankSelected:*;
            var event:* = event;
            try
            {
                bankSelected = Number(event.target.parent.name);
                this.func_DisplayBankInfo(bankSelected);
                this.currentlySeelctedMIssileBank = bankSelected;
                this.func_playRegularClick();
            }
            catch (error:Error)
            {
            }
            return;
        }// end function

        public function func_cancelMissilePurchase(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_drawMissileDisplays();
            return;
        }// end function

        public function func_InitGunsDisplay()
        {
            var _loc_2:* = undefined;
            var _loc_1:* = 0;
            while (_loc_1 < this.shipstopgun)
            {
                
                if (this.guntype[_loc_1] != null)
                {
                    _loc_2 = _loc_1;
                    this.GunsDisplayed[_loc_1] = new Object();
                    this.GunsDisplayed[_loc_1].gunNumber = _loc_2;
                    this.GunsDisplayed[_loc_1].cost = this.guntype[_loc_2][5];
                    this.GunsDisplayed[_loc_1].gunName = this.guntype[_loc_2][6];
                    this.GunsDisplayed[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
                    this.GunsDisplayed[_loc_1].DisplayBox.itemname.text = this.guntype[_loc_2][6];
                    this.GunsDisplayed[_loc_1].DisplayBox.itemcost.text = "Cost:" + this.guntype[_loc_2][5];
                    this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text = "Shld/Struct/Pierce\r  ";
                    this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text + (this.guntype[_loc_2][4] + " / " + this.guntype[_loc_2][7] + " / " + this.guntype[_loc_2][8] + " \r");
                    this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Energy Drain: " + this.guntype[_loc_2][3] + " \r");
                    this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Life Time: " + this.guntype[_loc_2][1] + "/s \r");
                    this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Rel:" + this.guntype[_loc_2][2] + "/s  \r");
                    this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text = this.GunsDisplayed[_loc_1].DisplayBox.itemspecs.text + ("Spd:" + this.guntype[_loc_2][0] + "/s ");
                    this.GunsDisplayed[_loc_1].DisplayBox.name = _loc_2;
                    this.GunsDisplayed[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN, this.func_BuyNewWeapon);
                    this.DisplayScreen.func_addItem(this.GunsDisplayed[_loc_1].DisplayBox);
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_cancelShiledGenPurchase(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_drawShieldGenDisplays();
            return;
        }// end function

        function frame95()
        {
            this.SpecialsDisplayed = new Array();
            this.availableitems = new Array();
            this.q = 0;
            while (this.q < this.specialshipitems.length)
            {
                
                if (this.specialshipitems[this.q][5] == "CLOAK")
                {
                    if (this.shiptype[this.playershiptype][3][8] == true)
                    {
                        this.availableitems[this.availableitems.length] = this.specialshipitems[this.q];
                        this.availableitems[(this.availableitems.length - 1)][50] = this.q;
                    }
                }
                else if (this.specialshipitems[this.q][5] == "RAPIDMISSILE")
                {
                    if (this.shiptype[this.playershiptype][3][12] == true)
                    {
                        this.availableitems[this.availableitems.length] = this.specialshipitems[this.q];
                        this.availableitems[(this.availableitems.length - 1)][50] = this.q;
                    }
                }
                else if (this.specialshipitems[this.q][5] == "INVULNSHIELD")
                {
                    if (this.playershiptype > 3 && this.playershiptype < 8)
                    {
                    }
                }
                else if (this.specialshipitems[this.q][5] == "HSHIELD")
                {
                    if (this.shiptype[this.playershiptype][3][10])
                    {
                    }
                }
                else if (this.specialshipitems[this.q][5] == "JUMPDRIVE")
                {
                    if (this.shiptype[this.playershiptype][3][13] == true)
                    {
                    }
                }
                else if (this.specialshipitems[this.q][5] == "FLARE" || this.specialshipitems[this.q][5] == "MINES" || this.specialshipitems[this.q][5] == "PULSAR" || this.specialshipitems[this.q][5] == "WINGMAN")
                {
                }
                else
                {
                    this.availableitems[this.availableitems.length] = this.specialshipitems[this.q];
                    this.availableitems[(this.availableitems.length - 1)][50] = this.q;
                }
                var _loc_1:* = this;
                var _loc_2:* = this.q + 1;
                _loc_1.q = _loc_2;
            }
            this.maxBumberOfEnergySpecials = this.shiptype[this.playershiptype][3][7];
            this.SpecialListHolder = new Array();
            this.buyingwindowaction = "";
            this.selectedSpot = -1;
            this.func_drawSpecialsDisplays();
            this.buyspecial.yes_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_acceptQuestion);
            this.buyspecial.no_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_cancelQuestion);
            return;
        }// end function

        public function func_drawHardpointBrackets()
        {
            this.hardPointHolders = new Array();
            var _loc_1:* = this.shiptype[this.playershiptype][3][4];
            var _loc_2:* = 0;
            while (_loc_2 < this.numberOfHardpoints)
            {
                
                this.hardPointHolders[_loc_2] = new this.hardPointBox() as MovieClip;
                this.hardPointHolders[_loc_2].x = this.shiptype[this.playershiptype][2][_loc_2][0] * _loc_1;
                this.hardPointHolders[_loc_2].y = this.shiptype[this.playershiptype][2][_loc_2][1] * _loc_1;
                this.hardPointHolders[_loc_2].hardpointname.text = "Hardpoint " + _loc_2;
                this.hardPointHolders[_loc_2].scaleX = 0.75;
                this.hardPointHolders[_loc_2].scaleY = 0.75;
                this.hardPointHolders[_loc_2].name = _loc_2;
                this.hardPointHolders[_loc_2].addEventListener(MouseEvent.MOUSE_DOWN, this.func_SelectedHardpointDrag);
                this.hardPointHolders[_loc_2].addEventListener(MouseEvent.MOUSE_UP, this.func_SelectedHardpointReleased);
                this.shipblueprint.addChild(this.hardPointHolders[_loc_2]);
                _loc_2 = _loc_2 + 1;
            }
            this.shipblueprint.addChild(this.sellingHardPointBox);
            this.sellingHardPointBox.x = 250;
            this.sellingHardPointBox.y = 0;
            this.sellingHardPointBox.scaleX = 0.75;
            this.sellingHardPointBox.scaleY = 0.75;
            this.sellingHardPointBox.name = -1;
            this.sellingHardPointBox.addEventListener(MouseEvent.MOUSE_UP, this.func_SelectedHardpointReleased);
            return;
        }// end function

        public function func_SpecialSelling(event:MouseEvent) : void
        {
            var _loc_2:* = Number(event.target.parent.parent.name);
            trace(event.target.parent.parent.name);
            this.buyspecial.visible = true;
            this.buyingwindowaction = this.SpecialListHolder[_loc_2].action.actionToDo.text;
            trace(this.buyingwindowaction);
            this.selectedSpot = _loc_2;
            var _loc_3:* = this.playershipstatus[11][1][this.selectedSpot][0];
            var _loc_4:* = this.specialshipitems[_loc_3][0];
            if (this.buyingwindowaction == "SELL")
            {
                this.buyspecial.question.text = "Sell: " + _loc_4 + " \r For" + this.specialshipitems[_loc_3][6] / 2;
            }
            else
            {
                this.buyspecial.question.text = "Drop: " + _loc_4 + "? ";
            }
            this.func_playRegularClick();
            return;
        }// end function

        public function func_InitSpecialsDisplay()
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            this.SpecialsDisplayed = new Array();
            var _loc_1:* = 0;
            while (_loc_1 < this.availableitems.length)
            {
                
                _loc_2 = _loc_1;
                this.SpecialsDisplayed[_loc_1] = new Object();
                this.SpecialsDisplayed[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
                this.SpecialsDisplayed[_loc_1].DisplayBox.itemname.text = this.availableitems[_loc_1][0];
                this.SpecialsDisplayed[_loc_1].DisplayBox.itemcost.text = "Cost: " + this.availableitems[_loc_1][6];
                this.SpecialsDisplayed[_loc_1].DisplayBox.name = this.availableitems[_loc_1][50];
                _loc_3 = "";
                if (this.availableitems[_loc_1][5] == "FLARE")
                {
                    _loc_3 = _loc_3 + ("Energy Drain: " + this.availableitems[_loc_1][2] + " \r");
                    _loc_3 = _loc_3 + ("Reload :" + this.availableitems[_loc_1][4] / 1000 + "/s \r");
                    _loc_3 = _loc_3 + ("Life Time: " + this.availableitems[_loc_1][3] / 1000 + "/s \r");
                    _loc_3 = _loc_3 + ("%Chance :" + this.availableitems[_loc_1][10] * 100 + "% \r");
                }
                else if (this.availableitems[_loc_1][5] == "MINES")
                {
                    _loc_3 = _loc_3 + ("Reload :" + this.availableitems[_loc_1][4] / 1000 + "/s \r");
                    _loc_3 = _loc_3 + ("Life Time: " + this.availableitems[_loc_1][3] / 1000 + "/s \r");
                    _loc_3 = _loc_3 + ("Damage :" + this.availableitems[_loc_1][12] + " \r");
                    _loc_3 = _loc_3 + "Not Effective on Ai";
                }
                else if (this.availableitems[_loc_1][5] == "DETECTOR" || this.availableitems[_loc_1][5] == "STEALTH" || this.availableitems[_loc_1][5] == "CLOAK")
                {
                    _loc_3 = _loc_3 + ("Energy Drain: " + this.availableitems[_loc_1][2] + " \r");
                    if (this.availableitems[_loc_1][5] == "DETECTOR")
                    {
                        _loc_3 = _loc_3 + ("Detect Hide Levels: \r " + this.availableitems[_loc_1][10] + " and under \r ");
                        _loc_3 = _loc_3 + ("Interval: " + this.availableitems[_loc_1][11] / 1000 + "/s");
                    }
                    else
                    {
                        _loc_3 = _loc_3 + ("Hide Level: " + this.availableitems[_loc_1][10] + "\r");
                    }
                    if (this.availableitems[_loc_1][5] == "CLOAK")
                    {
                        _loc_3 = _loc_3 + ("C/D: " + this.availableitems[_loc_1][11] / 1000 + "/s");
                    }
                }
                else if (this.availableitems[_loc_1][5] == "PULSAR")
                {
                    _loc_3 = _loc_3 + ("Energy Need: " + this.availableitems[_loc_1][2] + " \r");
                    _loc_3 = _loc_3 + ("Shutdown Time: " + this.availableitems[_loc_1][11] / 1000 + " s\r");
                    _loc_3 = _loc_3 + ("Radius :" + this.availableitems[_loc_1][12] + " \r");
                    _loc_3 = _loc_3 + ("QTY: " + this.availableitems[_loc_1][1]);
                }
                else if (this.availableitems[_loc_1][5] == "HSHIELD")
                {
                    _loc_3 = _loc_3 + ("Energy Need: " + this.availableitems[_loc_1][2] + " \r");
                    _loc_3 = _loc_3 + ("ReCharge: " + this.availableitems[_loc_1][4] / 1000 + " s\r");
                    _loc_3 = _loc_3 + ("Duration: " + this.availableitems[_loc_1][3] / 1000 + "/s \r");
                    _loc_3 = _loc_3 + ("Damage :" + this.availableitems[_loc_1][12] + "/s \r");
                }
                else if (this.availableitems[_loc_1][5] == "INVULNSHIELD")
                {
                    _loc_3 = _loc_3 + ("Energy Need: " + this.availableitems[_loc_1][2] + " \r");
                    _loc_3 = _loc_3 + ("ReCharge: " + this.availableitems[_loc_1][4] / 1000 + " s\r");
                    _loc_3 = _loc_3 + ("Duration :" + this.availableitems[_loc_1][3] + "/s \r");
                }
                else if (this.availableitems[_loc_1][5] == "RECHARGESHIELD")
                {
                    _loc_3 = _loc_3 + ("Energy Conversion: \r 1 Energy:" + this.availableitems[_loc_1][2] + " Shield \r");
                    _loc_3 = _loc_3 + ("ReCharge :" + this.availableitems[_loc_1][4] / 1000 + "/s \r");
                }
                else if (this.availableitems[_loc_1][5] == "RECHARGESTRUCT")
                {
                    _loc_3 = _loc_3 + ("Energy Conversion: \r 1 Energy:" + this.availableitems[_loc_1][2] + " Struct \r");
                    _loc_3 = _loc_3 + ("ReCharge :" + this.availableitems[_loc_1][4] / 1000 + "/s \r");
                }
                else if (this.availableitems[_loc_1][5] == "RAPIDMISSILE")
                {
                    _loc_3 = _loc_3 + ("Energy Needed:" + this.availableitems[_loc_1][2] + "\r");
                    _loc_3 = _loc_3 + ("Reload: " + (1 - this.availableitems[_loc_1][11]) * 100 + "% Faster\r");
                    _loc_3 = _loc_3 + "Dumbfire Missiles\rOnly";
                }
                else if (this.availableitems[_loc_1][5] == "JUMPDRIVE")
                {
                    _loc_3 = _loc_3 + ("Energy:" + this.availableitems[_loc_1][2] + "\r");
                    _loc_3 = _loc_3 + ("Reload: " + this.availableitems[_loc_1][4] / 1000 + "s\r");
                    _loc_3 = _loc_3 + "Set Location\rOn Map";
                }
                else if (this.availableitems[_loc_1][5] == "WINGMAN")
                {
                    _loc_3 = _loc_3 + ("Energy:" + this.availableitems[_loc_1][2] + "\r");
                    _loc_3 = _loc_3 + ("Reload: " + this.availableitems[_loc_1][4] / 1000 + "s\r");
                    _loc_3 = _loc_3 + "Max Wingmen\rCarriers:3\rOthers:1";
                }
                this.SpecialsDisplayed[_loc_1].DisplayBox.itemspecs.text = _loc_3;
                this.SpecialsDisplayed[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN, this.func_SpecialBuying);
                this.DisplayScreen.func_addItem(this.SpecialsDisplayed[_loc_1].DisplayBox);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_drawMissileDisplays()
        {
            this.func_ClearBlueBrint();
            this.func_drawMissilePointBrackets();
            this.func_MissileHolderImages();
            this.func_DisplayBankInfo(this.currentlySeelctedMIssileBank);
            return;
        }// end function

        public function func_cancelQuestion(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_drawSpecialsDisplays();
            return;
        }// end function

        public function func_SpecialBuying(event:MouseEvent) : void
        {
            var _loc_2:* = Number(event.target.parent.name);
            this.buyingwindowaction = "BUY";
            this.selectedSpot = _loc_2;
            var _loc_3:* = this.specialshipitems[this.selectedSpot][6];
            var _loc_4:* = this.specialshipitems[this.selectedSpot][0];
            if (this.playershipstatus[11][1].length >= this.shiptype[this.playershiptype][3][7])
            {
            }
            else if (_loc_3 <= this.playershipstatus[3][1])
            {
                this.buyspecial.visible = true;
                this.buyspecial.question.text = "Buy: " + _loc_4 + " \r For" + _loc_3;
                this.func_playRegularClick();
            }
            return;
        }// end function

        public function func_drawEnergyCapImages()
        {
            var _loc_2:* = null;
            this.EnergyCapDataHolders = new Array();
            var _loc_1:* = 0;
            while (_loc_1 < this.numberOfEnergyCapPoints)
            {
                
                _loc_2 = getDefinitionByName("hardwarescreengunbracketenergycap0") as Class;
                this.EnergyCapDataHolders[_loc_1] = new _loc_2 as MovieClip;
                this.EnergyCapDataHolders[_loc_1].x = 0;
                this.EnergyCapDataHolders[_loc_1].y = 0;
                this.EnergyCapDataHolders[_loc_1].scaleX = 0.75;
                this.EnergyCapDataHolders[_loc_1].scaleY = 0.75;
                this.EnergyCapDataHolders[_loc_1].itemname.text = this.energycapacitors[this.playershipstatus[1][5]][1];
                this.EnergyCapDataHolders[_loc_1].name = _loc_1;
                this.shipblueprint.addChild(this.EnergyCapDataHolders[_loc_1]);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_MissileHolderImages()
        {
            var _loc_2:* = null;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            this.MissileDataHolders = new Array();
            var _loc_1:* = 0;
            while (_loc_1 < this.numberOfMissilepoints)
            {
                
                _loc_2 = getDefinitionByName("hardwarescreenmissilebankicon") as Class;
                this.MissileDataHolders[_loc_1] = new _loc_2 as MovieClip;
                this.MissileDataHolders[_loc_1].x = this.shiptype[this.playershiptype][7][_loc_1][2] * this.missilelocationmultiplier;
                this.MissileDataHolders[_loc_1].y = this.shiptype[this.playershiptype][7][_loc_1][3] * this.missilelocationmultiplier;
                this.MissileDataHolders[_loc_1].scaleX = 0.75;
                this.MissileDataHolders[_loc_1].scaleY = 0.75;
                this.MissileDataHolders[_loc_1].itemname.text = "Bank " + _loc_1;
                this.MissileDataHolders[_loc_1].name = _loc_1;
                _loc_3 = 0;
                _loc_4 = _loc_1;
                _loc_5 = 0;
                while (_loc_5 < this.missile.length)
                {
                    
                    if (this.playershipstatus[7][_loc_4][4][_loc_5] > 0)
                    {
                        _loc_3 = _loc_3 + this.playershipstatus[7][_loc_4][4][_loc_5];
                    }
                    _loc_5 = _loc_5 + 1;
                }
                this.shipblueprint.addChild(this.MissileDataHolders[_loc_1]);
                _loc_6 = this.playershipstatus[7][_loc_4][5];
                this.MissileDataHolders[_loc_1].qty.text = "Missiles\r(" + _loc_3 + "/" + _loc_6 + ")";
                this.MissileDataHolders[_loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_MissileBankSelected);
                this.shipblueprint.addChild(this.MissileDataHolders[_loc_1]);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_GunsSelectionMade(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_GunsSelectionWasMade();
            return;
        }// end function

        public function func_drawMissilePointBrackets()
        {
            this.MissilePointHolders = new Array();
            var _loc_1:* = 0;
            while (_loc_1 < this.numberOfMissilepoints)
            {
                
                this.MissilePointHolders[_loc_1] = new this.hardPointBox() as MovieClip;
                this.MissilePointHolders[_loc_1].x = this.shiptype[this.playershiptype][7][_loc_1][2] * this.missilelocationmultiplier;
                this.MissilePointHolders[_loc_1].y = this.shiptype[this.playershiptype][7][_loc_1][3] * this.missilelocationmultiplier;
                this.MissilePointHolders[_loc_1].maxMissiles = this.playershipstatus[7][_loc_1][5];
                this.MissilePointHolders[_loc_1].hardpointname.text = "Bank " + _loc_1;
                this.MissilePointHolders[_loc_1].scaleX = 0.75;
                this.MissilePointHolders[_loc_1].scaleY = 0.75;
                this.MissilePointHolders[_loc_1].name = _loc_1;
                this.shipblueprint.addChild(this.MissilePointHolders[_loc_1]);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_TurretsSelectionWasMade(event:MouseEvent) : void
        {
            if (this.shiptype[this.playershiptype][5].length > 0)
            {
                this.func_playRegularClick();
                this.func_ResetButtonsToOff();
                this.turrets.gotoAndStop(1);
                gotoAndStop("Turrets");
            }
            return;
        }// end function

        public function func_BuyEnergyGen(event:MouseEvent) : void
        {
            this.EnergyGenTypeBeingBought = Number(event.target.parent.name);
            var _loc_2:* = this.energygenerators[this.EnergyGenTypeBeingBought][2] - this.currentvalueofEnergygenerator;
            if (_loc_2 <= this.playershipstatus[3][1])
            {
                this.buyenergygen.question.text = "Buy: " + this.energygenerators[this.EnergyGenTypeBeingBought][1] + " \r";
                this.buyenergygen.question.text = this.buyenergygen.question.text + ("For: " + _loc_2);
                this.buyenergygen.visible = true;
                this.func_playRegularClick();
            }
            else
            {
                this.buyenergygen.visible = false;
            }
            return;
        }// end function

        public function func_acceptGunPurchase(event:MouseEvent) : void
        {
            var _loc_2:* = 0;
            while (_loc_2 < this.playershipstatus[0].length)
            {
                
                if (this.playershipstatus[0][_loc_2][0] == "none")
                {
                    this.playershipstatus[0][_loc_2][0] = this.gunBeingBought;
                    this.playershipstatus[3][1] = this.playershipstatus[3][1] - this.guntype[this.gunBeingBought][5];
                    break;
                }
                _loc_2 = _loc_2 + 1;
            }
            this.buyweapon.visible = false;
            this.func_playRegularClick();
            this.func_drawGunDisplays();
            return;
        }// end function

        public function func_cancelTurretSale(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_drawTurretDisplays();
            return;
        }// end function

        public function func_cancelEnergyCapPurchase(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_drawEnergyCapDisplays();
            return;
        }// end function

        public function func_BuyMissiles(event:MouseEvent) : void
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            this.missileTypeBeingBought = Number(event.target.parent.name);
            if (this.currentlySeelctedMIssileBank >= 0)
            {
                _loc_2 = this.missile[this.missileTypeBeingBought][5];
                if (_loc_2 <= this.playershipstatus[3][1])
                {
                    _loc_3 = 0;
                    _loc_4 = this.MissilePointHolders[this.currentlySeelctedMIssileBank].maxMissiles;
                    _loc_3 = 0;
                    while (_loc_3 < this.missile.length)
                    {
                        
                        if (this.playershipstatus[7][this.currentlySeelctedMIssileBank][4][_loc_3] > 0)
                        {
                            _loc_4 = _loc_4 - this.playershipstatus[7][this.currentlySeelctedMIssileBank][4][_loc_3];
                        }
                        _loc_3 = _loc_3 + 1;
                    }
                    if (_loc_4 > 0)
                    {
                        this.missilepurchasewindow.visible = true;
                        this.missilepurchasewindow.maxMissiles = _loc_4;
                        if (_loc_4 * _loc_2 > this.playershipstatus[3][1])
                        {
                            this.missilepurchasewindow.maxMissiles = Math.floor(this.playershipstatus[3][1] / _loc_2);
                        }
                        this.missilepurchasewindow.MissilePrice = _loc_2;
                        this.missilepurchasewindow.banknumberdisp.text = "Bank: " + this.currentlySeelctedMIssileBank;
                        this.missilepurchasewindow.missilecostdisp.text = this.missile[this.missileTypeBeingBought][6] + " at " + _loc_2 + "each";
                        this.missilepurchasewindow.maxpurchase.text = "Max Qty of: " + this.missilepurchasewindow.maxMissiles + " missiles";
                        this.missilepurchasewindow.quantity.text = this.missilepurchasewindow.maxMissiles;
                        this.func_playRegularClick();
                    }
                }
            }
            return;
        }// end function

        public function func_drawTurretBrackets()
        {
            this.TurrethardPointHolders = new Array();
            var _loc_1:* = this.shiptype[this.playershiptype][3][4];
            var _loc_2:* = 0;
            while (_loc_2 < this.numberOfTurrets)
            {
                
                this.TurrethardPointHolders[_loc_2] = new this.hardPointBox() as MovieClip;
                this.TurrethardPointHolders[_loc_2].x = this.shiptype[this.playershiptype][5][_loc_2][0] * _loc_1;
                this.TurrethardPointHolders[_loc_2].y = this.shiptype[this.playershiptype][5][_loc_2][1] * _loc_1;
                this.TurrethardPointHolders[_loc_2].hardpointname.text = "Turret " + _loc_2;
                this.TurrethardPointHolders[_loc_2].scaleX = 0.75;
                this.TurrethardPointHolders[_loc_2].scaleY = 0.75;
                this.TurrethardPointHolders[_loc_2].name = _loc_2;
                this.TurrethardPointHolders[_loc_2].addEventListener(MouseEvent.MOUSE_DOWN, this.func_SelectedTurretDrag);
                this.TurrethardPointHolders[_loc_2].addEventListener(MouseEvent.MOUSE_UP, this.func_SelectedTurretReleased);
                this.shipblueprint.addChild(this.TurrethardPointHolders[_loc_2]);
                _loc_2 = _loc_2 + 1;
            }
            this.shipblueprint.addChild(this.sellingTurretPointBox);
            this.sellingTurretPointBox.x = 250;
            this.sellingTurretPointBox.y = 0;
            this.sellingTurretPointBox.scaleX = 0.75;
            this.sellingTurretPointBox.scaleY = 0.75;
            this.sellingTurretPointBox.name = -1;
            this.sellingTurretPointBox.addEventListener(MouseEvent.MOUSE_UP, this.func_SelectedTurretReleased);
            return;
        }// end function

        public function func_drawEnergyGenDisplays()
        {
            this.buyenergygen.visible = false;
            this.DisplayScreen.func_clearAllItems();
            this.func_InitEnergyGensDisplay();
            this.func_ClearBlueBrint();
            this.func_drawEnergyGenBrackets();
            this.func_drawEnergyGenImages();
            return;
        }// end function

        public function func_BuyNewlTurret(event:MouseEvent) : void
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            this.TurretBeingBought = Number(event.target.parent.name);
            if (this.guntype[this.TurretBeingBought][5] * this.turretCostModifier <= this.playershipstatus[3][1])
            {
                _loc_2 = 0;
                _loc_3 = false;
                _loc_4 = -1;
                while (_loc_2 < this.playershipstatus[8].length)
                {
                    
                    if (this.playershipstatus[8][_loc_2][0] == "none")
                    {
                        _loc_3 = true;
                        _loc_4 = _loc_2;
                        break;
                    }
                    _loc_2 = _loc_2 + 1;
                }
                if (_loc_3 == true)
                {
                    this.buyweapon.visible = true;
                    this.buyweapon.question.text = "Buy: " + this.guntype[this.TurretBeingBought][6] + " \r" + "For: " + this.guntype[this.TurretBeingBought][5] * this.turretCostModifier;
                    this.func_playRegularClick();
                    ;
                }
            }
            return;
        }// end function

        public function func_BuyNewWeapon(event:MouseEvent) : void
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            this.gunBeingBought = Number(event.target.parent.name);
            if (this.guntype[this.gunBeingBought][5] <= this.playershipstatus[3][1])
            {
                _loc_2 = 0;
                _loc_3 = false;
                _loc_4 = -1;
                while (_loc_2 < this.playershipstatus[0].length)
                {
                    
                    if (this.playershipstatus[0][_loc_2][0] == "none")
                    {
                        _loc_3 = true;
                        _loc_4 = _loc_2;
                        break;
                    }
                    _loc_2 = _loc_2 + 1;
                }
                if (_loc_3 == true)
                {
                    this.buyweapon.visible = true;
                    this.buyweapon.question.text = "Buy: " + this.guntype[this.gunBeingBought][6] + " \r" + "For: " + this.guntype[this.gunBeingBought][5];
                    this.func_playRegularClick();
                    ;
                }
            }
            return;
        }// end function

        public function func_ShieldSelectionMade(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_ResetButtonsToOff();
            this.shieldgen.gotoAndStop(1);
            gotoAndStop("shieldgenerators");
            return;
        }// end function

        public function func_SelectedTurretReleased(event:MouseEvent) : void
        {
            var TemTurretsHolder:*;
            var event:* = event;
            var gunReleasedon:*;
            try
            {
                gunReleasedon = Number(event.target.parent.name);
                if (this.currentlyDraggingTurret != -1)
                {
                    if (!isNaN(gunReleasedon))
                    {
                        trace(gunReleasedon);
                        if (gunReleasedon == -1)
                        {
                            if (!isNaN(this.playershipstatus[8][this.currentlyDraggingTurret][0]))
                            {
                                this.sellweapon.visible = true;
                                this.sellweapon.question.text = "Sell: " + this.guntype[this.playershipstatus[8][this.currentlyDraggingTurret][0]][6] + " \r" + "For: " + Math.round(this.guntype[this.playershipstatus[8][this.currentlyDraggingTurret][0]][5] / 6) * this.turretCostModifier;
                                this.TurretsHolder[this.currentlyDraggingTurret].stopDrag();
                                this.func_playRegularClick();
                            }
                            else
                            {
                                gunReleasedon = 0;
                            }
                        }
                        else if (this.currentlyDraggingTurret != gunReleasedon)
                        {
                            TemTurretsHolder = this.playershipstatus[8][this.currentlyDraggingTurret];
                            this.playershipstatus[8][this.currentlyDraggingTurret] = this.playershipstatus[8][gunReleasedon];
                            this.playershipstatus[8][gunReleasedon] = TemTurretsHolder;
                            this.func_playRegularClick();
                        }
                    }
                }
                if (gunReleasedon != -1)
                {
                    this.func_drawTurretDisplays();
                }
            }
            catch (error:Error)
            {
            }
            return;
        }// end function

        public function func_acceptSale(event:MouseEvent) : void
        {
            this.playershipstatus[3][1] = this.playershipstatus[3][1] + Math.round(this.guntype[this.playershipstatus[0][this.currentlyDraggingGun][0]][5] / 6);
            this.playershipstatus[0][this.currentlyDraggingGun][0] = "none";
            this.func_playRegularClick();
            this.func_drawGunDisplays();
            return;
        }// end function

        public function func_cancelTurretPurchase(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.buyweapon.visible = false;
            return;
        }// end function

        public function func_acceptTurretPurchase(event:MouseEvent) : void
        {
            var _loc_2:* = 0;
            while (_loc_2 < this.playershipstatus[8].length)
            {
                
                if (this.playershipstatus[8][_loc_2][0] == "none")
                {
                    this.playershipstatus[8][_loc_2][0] = this.TurretBeingBought;
                    this.playershipstatus[3][1] = this.playershipstatus[3][1] - this.guntype[this.TurretBeingBought][5] * this.turretCostModifier;
                    break;
                }
                _loc_2 = _loc_2 + 1;
            }
            this.buyweapon.visible = false;
            this.func_playRegularClick();
            this.func_drawTurretDisplays();
            return;
        }// end function

        public function func_acceptQuestion(event:MouseEvent) : void
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            if (this.buyingwindowaction == "BUY")
            {
                _loc_2 = this.specialshipitems[this.selectedSpot][6];
                _loc_3 = this.specialshipitems[this.selectedSpot][1];
                this.playershipstatus[3][1] = this.playershipstatus[3][1] - _loc_2;
                if (this.playershipstatus[11][1].length == 0)
                {
                    this.playershipstatus[11][1] = new Array();
                }
                _loc_4 = this.playershipstatus[11][1].length;
                this.playershipstatus[11][1][_loc_4] = new Array();
                this.playershipstatus[11][1][_loc_4][0] = this.selectedSpot;
                this.playershipstatus[11][1][_loc_4][1] = _loc_3;
            }
            else if (this.buyingwindowaction == "SELL")
            {
                _loc_5 = this.playershipstatus[11][1][this.selectedSpot][0];
                _loc_6 = this.specialshipitems[_loc_5][6] / 2;
                this.playershipstatus[3][1] = this.playershipstatus[3][1] + _loc_6;
                this.playershipstatus[11][1].splice(this.selectedSpot, 1);
            }
            else
            {
                this.playershipstatus[11][1].splice(this.selectedSpot, 1);
            }
            this.func_playRegularClick();
            this.func_drawSpecialsDisplays();
            return;
        }// end function

        public function func_InitEnergyGensDisplay()
        {
            var _loc_3:* = undefined;
            this.currentvalueofEnergygenerator = Math.round(this.energygenerators[this.playershipstatus[1][0]][2] / 6);
            this.EnergyGensDisplayed = new Array();
            var _loc_1:* = 0;
            var _loc_2:* = this.shiptype[this.playershiptype][6][2] + 1;
            while (_loc_1 < _loc_2)
            {
                
                _loc_3 = _loc_1;
                this.EnergyGensDisplayed[_loc_1] = new Object();
                this.EnergyGensDisplayed[_loc_1].genNumber = _loc_3;
                this.EnergyGensDisplayed[_loc_1].DisplayBox = new this.DisplayBox() as MovieClip;
                this.EnergyGensDisplayed[_loc_1].DisplayBox.itemname.text = this.energygenerators[_loc_3][1];
                if (this.playershipstatus[1][0] == _loc_3)
                {
                    this.EnergyGensDisplayed[_loc_1].DisplayBox.itemcost.text = "Current";
                }
                else
                {
                    this.EnergyGensDisplayed[_loc_1].DisplayBox.itemcost.text = "Cost:" + (this.energygenerators[_loc_3][2] - this.currentvalueofEnergygenerator);
                    this.EnergyGensDisplayed[_loc_1].DisplayBox.addEventListener(MouseEvent.MOUSE_DOWN, this.func_BuyEnergyGen);
                }
                this.EnergyGensDisplayed[_loc_1].DisplayBox.name = _loc_3;
                this.EnergyGensDisplayed[_loc_1].DisplayBox.itemspecs.text = "Recharge: " + this.energygenerators[_loc_3][0] + "/sec \r";
                this.DisplayScreen.func_addItem(this.EnergyGensDisplayed[_loc_1].DisplayBox);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_drawGunDisplays()
        {
            this.func_ClearBlueBrint();
            this.func_gunHolderImages();
            this.func_drawHardpointBrackets();
            this.currentlyDraggingGun = -1;
            this.sellweapon.visible = false;
            return;
        }// end function

        public function func_EnergyGenSelectionMade(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_ResetButtonsToOff();
            this.energygen.gotoAndStop(1);
            gotoAndStop("EnergyGenerators");
            return;
        }// end function

        public function func_drawSpecialsOnShip()
        {
            var _loc_1:* = null;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            this.SpecialListHolder = new Array();
            this.specialsDisplay.titledisp.text = "No. of Specials (" + this.playershipstatus[11][1].length + "/" + this.shiptype[this.playershiptype][3][7] + ")";
            _loc_1 = getDefinitionByName("hardwarescreenspecialsinfoitemdisp") as Class;
            var _loc_2:* = 0;
            _loc_2 = 0;
            while (_loc_2 < this.playershipstatus[11][1].length)
            {
                
                this.SpecialListHolder[_loc_2] = new _loc_1 as MovieClip;
                _loc_3 = this.playershipstatus[11][1][_loc_2][0];
                _loc_4 = this.specialshipitems[_loc_3][0];
                _loc_5 = this.playershipstatus[11][1][_loc_2][1];
                this.SpecialListHolder[_loc_2].x = 0;
                this.SpecialListHolder[_loc_2].y = this.SpecialListHolder[_loc_2].height * _loc_2;
                this.SpecialListHolder[_loc_2].name = _loc_2;
                this.SpecialListHolder[_loc_2].label.text = _loc_2 + ": " + _loc_4;
                if (_loc_5 >= 0)
                {
                    this.SpecialListHolder[_loc_2].label.text = " (" + _loc_5 + ")";
                    this.SpecialListHolder[_loc_2].action.actionToDo.text = "DROP";
                }
                else
                {
                    this.SpecialListHolder[_loc_2].action.actionToDo.text = "SELL";
                }
                this.SpecialListHolder[_loc_2].action.addEventListener(MouseEvent.MOUSE_DOWN, this.func_SpecialSelling);
                this.specialsDisplay.addChild(this.SpecialListHolder[_loc_2]);
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

    }
}
