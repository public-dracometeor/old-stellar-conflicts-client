﻿package sc346_fla
{
    import flash.display.*;
    import flash.events.*;
    import flash.media.*;
    import flash.net.*;
    import flash.system.*;
    import flash.text.*;
    import flash.utils.*;

    dynamic public class MainTimeline extends flash.display.MovieClip
    {
        public var totalLatChecks:Object;
        public var isspacekeypressed:Object;
        public var isPlayerChatting:Object;
        public var updateFrom:Object;
        public var credDisplayTimer:Timer;
        public var playerfunds:Object;
        public var baseshieldgendrain:Object;
        public var shieldgenerators:Object;
        public var systemchattextcolor:Object;
        public var PlayerStatDisp:MovieClip;
        public var controlledserverclose:Object;
        public var starbasestayhostiletime:Object;
        public var playerrotationdegredation:Object;
        public var isplayeremp:Object;
        public var currentip:Object;
        public var hasClockBeenSet:Object;
        public var SpecialsTimer:Timer;
        public var HalfBackgroundWidth:Object;
        public var mov_structureWarning:MovieClip;
        public var guntype:Object;
        public var isshiftkeypressed:Object;
        public var LastMissileHeard:Object;
        public var energygenerators:Object;
        public var colorNumber:Object;
        public var dockingTipText:TextField;
        public var playerShipSpeedRatio:Object;
        public var datatosend:Object;
        public var refreshchatdisplay:Object;
        public var currentStarFrame:Object;
        public var maxdockingvelocity:Object;
        public var currenthelpframedisplayed:Object;
        public var RapidMissilesOn:Object;
        public var starbaselocation:Object;
        public var publicteams:Object;
        public var lastshipcoordinatex:Object;
        public var currentplayershotsfired:Object;
        public var lastpingcheck:Object;
        public var GamePingTimer:TextField;
        public var lastshipcoordinatey:Object;
        public var shipTargetDisplay:Object;
        public var gamedisplayarea:MovieClip;
        public var framestobuffer:Object;
        public var missile:Object;
        public var playerempend:Object;
        public var squadbaseinfo:Object;
        public var shipYmovement:Object;
        public var NavigationImageToAttach:Class;
        public var extraplayerships:Object;
        public var loginmovie:MovieClip;
        public var gameTutorial:MovieClip;
        public var NavigationTimer:Timer;
        public var playerEnteringCol:Object;
        public var doublegameareawidth:Object;
        public var stafchattextcolor:Object;
        public var currentport:Object;
        public var staffhelpchattextcolor:Object;
        public var timetillusercanjump:Object;
        public var gameplaystatus:Object;
        public var afterburnerinuse:Object;
        public var NeededLatFails:Object;
        public var savingGameTextBox:TextField;
        public var timetillnexClocktcheck:Object;
        public var badwords:Object;
        public var AnglePlayerShipFacing:Object;
        public var shipositiondelay:Object;
        public var shipRelationIDDisplay:Object;
        public var TargetDisplay:MovieClip;
        public var OnlinePLayerList:MovieClip;
        public var lastMeteorTime:Object;
        public var gameOptionsScreen:MovieClip;
        public var MissileTimer:Timer;
        public var inGameSoundButton:MovieClip;
        public var teamdeathmatch:Object;
        public var MissionRangeFromNavpoint:Object;
        public var shieldrechargerate:Object;
        public var specialsingame:Specialsingame;
        public var CurenntClockCheckNumber:Object;
        public var hangarWindow:MovieClip;
        public var MusicVolumeAdjust:SoundTransform;
        public var schange:Object;
        public var shiptimeoutime:Object;
        public var myLoadVars:Object;
        public var sectorinformation:Object;
        public var bountychanging:Object;
        public var playersquadbaseslvl:Object;
        public var playerSpecialsSettings:Object;
        public var shipDockingImage:Object;
        public var isplayerdisrupt:Object;
        public var maxMeteors:Object;
        public var shipcoordinatex:Object;
        public var shipcoordinatey:Object;
        public var relogInGame:Object;
        public var chatDisplay:chatdialogue;
        public var shipXmovement:Object;
        public var playershipafterburnerspeed:Object;
        public var creditBar:MovieClip;
        public var currentotherplayshot:Object;
        public var specialshipitems:Object;
        public var missileShotBuuferData:Object;
        public var disruptendsat:Object;
        public var teambasetypes:Object;
        public var inGameHelp:MovieClip;
        public var gamebackground:Object;
        public var isdownkeypressed:Object;
        public var playershipfacing:Object;
        public var keypressdelay:Object;
        public var teamchattextcolor:Object;
        public var RightKey:Object;
        public var HalfBackgroundMaxHeight:Object;
        public var playershipstatus:Object;
        public var isgamerunningfromremote:Object;
        public var currZoomFactor:Object;
        public var lastStaticSound:Object;
        public var lastturning:Object;
        public var shipJumpImage:Object;
        public var playersshiptype:Object;
        public var pingintervalcheck:Object;
        public var scoreDISP:MovieClip;
        public var gunfirekey:Object;
        public var loc:Object;
        public var lastTimerChatFocused:Object;
        public var dockkey:Object;
        public var totalsmallships:Object;
        public var tradeGoodsScreen:MovieClip;
        public var othermissilefire:Object;
        public var BackwardsKey:Object;
        public var shiptype:Object;
        public var doublegameareaheight:Object;
        public var missilekey:Object;
        public var teambases:Object;
        public var NavigationImage:MovieClip;
        public var currentothermissileshot:Object;
        public var TargetShipKey:Object;
        public var MissileSeekDelay:Object;
        public var squadbasesalreadyloaded:Object;
        public var PlayersShipShieldImage:MovieClip;
        public var RadarBlot:Object;
        public var totalstars:Object;
        public var squadbasetype:Object;
        public var isleftkeypressed:Object;
        public var LastDockScreen:Object;
        public var gameSounds:Object;
        public var EnemyBaseTextColor:Object;
        public var regularchattextcolor:Object;
        public var isupkeypressed:Object;
        public var inGameHelpButton:SimpleButton;
        public var AvailableListDisplay:Object;
        public var reEnterButton:SimpleButton;
        public var gunShotBufferData:Object;
        public var gameMap:ingamemap;
        public var maxextraships:Object;
        public var gameSettingScreen:MovieClip;
        public var mysocket:Object;
        public var speedratio:Object;
        public var gameerrordoneby:Object;
        public var squadchattextcolor:Object;
        public var missiontoactualscoremodifier:Object;
        public var currentPIpacket:Object;
        public var NeutralBasetextcolor:Object;
        public var lastMissileSeek:Object;
        public var halfgameareaheight:Object;
        public var halfgameareawidth:Object;
        public var InfoResendDelay:Object;
        public var otherplayerdockedon:Object;
        public var tradegoods:Object;
        public var playerjustloggedin:Object;
        public var playershipvelocity:Object;
        public var shipDeadImage:Object;
        public var spacekeyjustpressed:Object;
        public var extrafundsmultiplier:Object;
        public var backgroundstar:Object;
        public var othershipbuffer:Object;
        public var remoteupdate:Object;
        public var versionno:Object;
        public var playersSessionScoreStart:Object;
        public var EnergyPerRapid:Object;
        public var updateStarInterval:Object;
        public var HeaderColor:Object;
        public var keywaspressed:Object;
        public var timebannedfor:Object;
        public var LeftKey:Object;
        public var xwidthofasector:Object;
        public var squadbasedockedat:Object;
        public var lastplayerssavedinfo:Object;
        public var isdkeypressed:Object;
        public var playerrankings:Object;
        public var energydrainedbyshieldgenatfull:Object;
        public var maxenergy:Object;
        public var playersCurrentMissions:Object;
        public var RapidMissileRateChange:Object;
        public var DockingTipMessage:Timer;
        public var starbasepricechanges:Object;
        public var curreKeyUpFrame:Object;
        public var pulsarseldammodier:Object;
        public var maxshieldstrength:Object;
        public var timePlayerCanExit:Object;
        public var starbaseLevel:Object;
        public var gameError:MovieClip;
        public var shipHealthBar:Object;
        public var sectormapitems:Object;
        public var gameerror:Object;
        public var privatechattextcolor:Object;
        public var mySndCh:SoundChannel;
        public var playersquadbases:Object;
        public var playershotsfired:Object;
        public var ForwardKey:Object;
        public var justenteredgame:Object;
        public var playershipmaxvelocity:Object;
        public var curentGametime:Object;
        public var lastdamagetobase:Object;
        public var isrightkeypressed:Object;
        public var currentOnlineListdisplay:Object;
        public var playershipacceleration:Object;
        public var arrayZoomFactors:Object;
        public var turretCrosshairs:MovieClip;
        public var ExitGameButton:SimpleButton;
        public var MeteorHolderImage:Class;
        public var gameNewsBox:aimessages2;
        public var MissionCheckTimer:Timer;
        public var timeintervalcheck:Object;
        public var lastkeyPress:Object;
        public var ClockCheckstodo:Object;
        public var targetinfo:Object;
        public var playersworthtobtymodifire:Object;
        public var deathmenu:MovieClip;
        public var shiplag:Object;
        public var ShipHardwareScreen:MovieClip;
        public var MeteorMapLocaterImage:Class;
        public var DeathMatchTimer:Timer;
        public var gamechatinfo:Object;
        public var secondlastturning:Object;
        public var BackgroundMaxHeight:Object;
        public var counterForBaseLives:Object;
        public var playerdiruptend:Object;
        public var EnterChatKey:Object;
        public var TypingInTextField:Object;
        public var basesstartat:Object;
        public var iscontrolkeypressed:Object;
        public var squadwarinfo:Object;
        public var gamesetting:Object;
        public var playersFakeVelocity:Object;
        public var IsSocketConnected:Object;
        public var BackgroundMaxWidth:Object;
        public var gameRadar:MovieClip;
        public var specialItemno:Object;
        public var missionsavailable:Object;
        public var arenachattextcolor:Object;
        public var clocktimediff:Object;
        public var pulsarsinzone:Object;
        public var ClockIntervalCheckTime:Object;
        public var turretcontrol:MovieClip;
        public var tb:Object;
        public var isplayeraguest:Object;
        public var othergunfire:Object;
        public var energyrechargerate:Object;
        public var latencyTest:Object;
        public var MochiAdds:Object;
        public var inGameMapButton:SimpleButton;
        public var NextShipTimeResend:Object;
        public var playersexitdocktimewait:Object;
        public var currenttimechangeratio:Object;
        public var squadBaseDockedScreen:MovieClip;
        public var playershiprotating:Object;
        public var ywidthofasector:Object;
        public var PlayersShipImage:MovieClip;
        public var missileBankWindow:MovieClip;
        public var gunSoundDelay:Object;
        public var playershiprotation:Object;
        public var playersdestination:Object;
        public var currentonlineplayers:Object;
        public var playerscurrentextrashipno:Object;
        public var main_docked_screen:MovieClip;
        public var mission_award_multiplier:Object;
        public var shipNameTag:Object;
        public var playersmaxstructure:Object;
        public var playerBeingSeekedByMissile:Object;
        public var currentgamestatustext:Object;
        public var testData:MovieClip;
        public var ZonePlayerIsIn:Object;
        public var missions_Screen:MovieClip;
        public var MapTimer:Timer;
        public var energycapacitors:Object;
        public var MapKey:Object;
        public var baseidnumberstart:Object;
        public var PlayerInConquestZone:Object;
        public var SquadBaseIDAdjust:Object;
        public var MissionsDispText:TextField;
        public var FriendlyBasetextcolor:Object;
        public var teamdeathmatchinfo:Object;
        public var ClockCheckTimesReceived:Object;
        public var savestatus:Object;
        public var otherplayership:Object;
        public var isaracealteredZone:Object;
        public var totalcol:Object;
        public var afterburnerspeed:Object;
        public var TurretMouseDown:Object;
        public var afterburnerkey:Object;
        public var LastFrameTime:Object;
        public var replacewithchar:Object;
        public var nameColors:Object;
        public var lastplayerssavedinfosent:Object;
        public var gameMeteors:Object;
        public var LastKeyRelease:Object;

        public function MainTimeline()
        {
            addFrameScript(0, this.frame1, 3, this.frame4, 7, this.frame8, 10, this.frame11, 13, this.frame14, 16, this.frame17, 17, this.frame18, 25, this.frame26, 38, this.frame39, 59, this.frame60, 61, this.frame62, 72, this.frame73, 87, this.frame88, 101, this.frame102, 117, this.frame118, 128, this.frame129, 140, this.frame141, 154, this.frame155);
            return;
        }// end function

        public function firingbulletstartlocation(param1, param2, param3)
        {
            var _loc_4:* = Math.sqrt(param1 * param1 + param2 * param2);
            var _loc_5:* = Math.asin(param1 / _loc_4) / (Math.PI / 180);
            var _loc_6:* = 0;
            if (param1 >= 0)
            {
                if (param2 >= 0)
                {
                    _loc_6 = param3 + (90 - _loc_5);
                    param1 = _loc_4 * Math.cos(Math.PI / 180 * _loc_6);
                    param2 = _loc_4 * Math.sin(Math.PI / 180 * _loc_6);
                }
                if (param2 < 0)
                {
                    _loc_6 = param3 + (270 + _loc_5);
                    param1 = _loc_4 * Math.cos(Math.PI / 180 * _loc_6);
                    param2 = _loc_4 * Math.sin(Math.PI / 180 * _loc_6);
                }
            }
            if (param1 < 0)
            {
                if (param2 >= 0)
                {
                    _loc_6 = param3 + (90 - _loc_5);
                    param1 = _loc_4 * Math.cos(Math.PI / 180 * _loc_6);
                    param2 = _loc_4 * Math.sin(Math.PI / 180 * _loc_6);
                }
                if (param2 < 0)
                {
                    _loc_6 = param3 + (270 + _loc_5);
                    param1 = _loc_4 * Math.cos(Math.PI / 180 * _loc_6);
                    param2 = _loc_4 * Math.sin(Math.PI / 180 * _loc_6);
                }
            }
            var _loc_7:* = new Array();
            _loc_7[0] = param1;
            _loc_7[1] = param2;
            return _loc_7;
        }// end function

        public function ChangeSquadBaseLevel(param1, param2)
        {
            var _loc_3:* = undefined;
            _loc_3 = 0;
            while (_loc_3 < this.playersquadbases.length)
            {
                
                if (this.playersquadbases[_loc_3] != null)
                {
                    if (this.playersquadbases[_loc_3][0] == param1)
                    {
                        this.playersquadbases[_loc_3][1] = Number(param2);
                        this.setSquadBaseImage(param1);
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            return;
        }// end function

        public function func_SubmitChat()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            if (this.chatDisplay.visible)
            {
                _loc_1 = this.chatDisplay.chatInput.text;
                _loc_2 = "";
                if (_loc_1 != "")
                {
                    _loc_1 = this.func_checkoutgoingcharacters(_loc_1);
                    _loc_3 = _loc_1.toUpperCase();
                    trace("sending chat:" + _loc_1);
                    _loc_4 = "";
                    _loc_5 = "";
                    if (_loc_3 == "?SOUND=ON")
                    {
                    }
                    else if (_loc_3 == "?SOUND=OFF")
                    {
                    }
                    else if (_loc_3 == "?KFLAG")
                    {
                        this.func_enterintochat("Not Enabled Yet", this.systemchattextcolor);
                    }
                    else if (_loc_3.substr(0, 7) == "?METEOR")
                    {
                        _loc_3 = "MET`CREATE`~";
                        this.mysocket.send(_loc_3);
                    }
                    else if (_loc_3.substr(0, 5) == "?HELP")
                    {
                        _loc_4 = _loc_1.substr(6);
                        if (_loc_4 != "")
                        {
                            _loc_5 = "CHT~CH`" + this.playershipstatus[3][0] + "`STAFFH`" + _loc_4 + "~";
                            this.mysocket.send(_loc_5);
                            this.func_enterintochat("Sent Message To Staff : " + _loc_4, this.systemchattextcolor);
                        }
                    }
                    else if (_loc_3.substr(0, 8) == "?GETRANK")
                    {
                    }
                    else if (_loc_3.substr(0, 3) == "/W ")
                    {
                        _loc_6 = 3;
                        while (_loc_6 < _loc_1.length)
                        {
                            
                            if (_loc_1.charAt(_loc_6) != " ")
                            {
                                _loc_9 = _loc_6;
                                break;
                            }
                            _loc_6 = _loc_6 + 1;
                        }
                        _loc_6 = _loc_9 + 1;
                        while (_loc_6 < _loc_1.length)
                        {
                            
                            if (_loc_1.charAt(_loc_6) == " ")
                            {
                                _loc_10 = _loc_6;
                                break;
                            }
                            _loc_6 = _loc_6 + 1;
                        }
                        _loc_7 = null;
                        _loc_8 = _loc_1.substr(_loc_9, _loc_10 - _loc_9);
                        _loc_4 = _loc_1.substr((_loc_10 + 1));
                        if (_loc_4.length > 0)
                        {
                            _loc_3 = "CHT~CH`" + this.playershipstatus[3][0] + "`PM`" + _loc_4 + "`" + _loc_8.toUpperCase() + "~";
                            if (this.gamechatinfo[2][2] == false)
                            {
                                this.mysocket.send(_loc_3);
                                if (_loc_7 != this.playershipstatus[3][0])
                                {
                                }
                            }
                            else
                            {
                                this.func_enterintochat(" You Are Muted ", this.systemchattextcolor);
                            }
                        }
                    }
                    else if (_loc_1.substr(0, 7).toUpperCase() == "?IGNORE")
                    {
                        _loc_2 = _loc_1.substr(8).toUpperCase();
                        this.func_ignorelist(_loc_2);
                    }
                    else if (_loc_1.substr(0, 8).toUpperCase() == "?CHEATER")
                    {
                    }
                    else if (_loc_3.substr(0, 2) == "//" && this.playershipstatus[5][2] != "N/A")
                    {
                        _loc_3 = "CHT~CH`" + this.playershipstatus[3][0] + "`TM`" + _loc_1.substr(2) + "~";
                        if (this.gamechatinfo[2][2] == false)
                        {
                            this.mysocket.send(_loc_3);
                        }
                        else
                        {
                            this.func_enterintochat(" You Are Muted ", this.systemchattextcolor);
                        }
                    }
                    else if (_loc_1.substr(0, 1) == " ".substr(0, 1) && (this.playershipstatus[5][12] == "MOD" || this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "ADMIN"))
                    {
                        _loc_3 = "CHT~CH`" + this.playershipstatus[3][0] + "`STF`" + _loc_1.substr(1) + "~";
                        this.mysocket.send(_loc_3);
                    }
                    else if (_loc_1.charAt(0) == ";" && this.playershipstatus[5][10] != "NONE")
                    {
                        _loc_3 = "CHT~CH`" + this.playershipstatus[3][0] + "`SM`" + _loc_1.substr(1) + "~";
                        this.mysocket.send(_loc_3);
                    }
                    else if (_loc_1.substr(0, 7).toUpperCase() == "/*FLEET")
                    {
                        _loc_11 = _loc_1.substr(8).toUpperCase();
                        this.mysocket.send("FLEET`MOVE`" + _loc_11 + "`~");
                        trace("SEnt Fleet");
                    }
                    else if (_loc_1.substr(0, 2) == "/*" && (this.playershipstatus[5][12] == "MOD" || this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "ADMIN"))
                    {
                        this.func_admincommands(_loc_1);
                    }
                    else
                    {
                        _loc_3 = "CHT~CH`" + this.playershipstatus[3][0] + "`M`" + this.badwordfiltering(_loc_1) + "~";
                        if (this.gamechatinfo[2][2] == false)
                        {
                            this.mysocket.send(_loc_3);
                        }
                        else
                        {
                            this.func_enterintochat(" You Are Muted ", this.systemchattextcolor);
                        }
                    }
                }
                this.chatDisplay.chatInput.text = "";
                stage.focus = stage;
            }
            return;
        }// end function

        public function func_addEnergyGenintocliet(param1)
        {
            var _loc_2:* = this.energygenerators.length;
            this.energygenerators[_loc_2] = new Array();
            this.energygenerators[_loc_2][0] = Math.floor(Number(param1[1]));
            this.energygenerators[_loc_2][1] = "Level " + (_loc_2 + 1);
            this.energygenerators[_loc_2][2] = Math.floor(Number(param1[2]));
            return;
        }// end function

        public function func_stealthplayership(param1, param2, param3)
        {
            this.playershipstatus[5][15] = "S" + param2;
            this.playerSpecialsSettings.isStealthed = true;
            this.playerSpecialsSettings.StealthEnergy = param1;
            this.playerSpecialsSettings.StealthLocation = param3;
            this.func_StealthTheRadar();
            return;
        }// end function

        public function func_MeteorsOnGameFrame(event:Event)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            _loc_2 = getTimer();
            _loc_3 = _loc_2 - this.lastMeteorTime;
            this.lastMeteorTime = _loc_2;
            _loc_4 = 0;
            while (_loc_4 < this.maxMeteors)
            {
                
                if (this.gameMeteors[_loc_4] != null)
                {
                    this.gameMeteors[_loc_4].xposition = this.gameMeteors[_loc_4].xposition + this.gameMeteors[_loc_4].xspeed * _loc_3;
                    this.gameMeteors[_loc_4].yposition = this.gameMeteors[_loc_4].yposition + this.gameMeteors[_loc_4].yspeed * _loc_3;
                    this.gameMeteors[_loc_4].mapMarker.x = this.gameMeteors[_loc_4].xposition * this.gameMap.xratio + this.gameMap.gridxindent + this.gameMap.xsecwidth;
                    this.gameMeteors[_loc_4].mapMarker.y = this.gameMeteors[_loc_4].yposition * this.gameMap.yratio + this.gameMap.gridyindent + this.gameMap.ysecwidth;
                    this.gameMeteors[_loc_4].meteorImage.x = this.gameMeteors[_loc_4].xposition;
                    this.gameMeteors[_loc_4].meteorImage.y = this.gameMeteors[_loc_4].yposition;
                    if (this.gameMeteors[_loc_4].HitTime <= _loc_2)
                    {
                        this.func_removeMeteorFromGame(_loc_4);
                    }
                }
                _loc_4 = _loc_4 + 1;
            }
            return;
        }// end function

        public function GameKeyListenerKeyRelease(event:KeyboardEvent) : void
        {
            var _loc_2:* = false;
            var _loc_3:* = event.keyCode;
            if (_loc_3 == this.EnterChatKey)
            {
                this.func_EnableChatPress();
            }
            else if (this.isPlayerChatting)
            {
                this.func_KeyReleaseWhileChatting();
            }
            else
            {
                this.lastkeyPress = getTimer();
                if (_loc_3 == this.gamesetting.accelkey)
                {
                    this.isupkeypressed = _loc_2;
                    this.keywaspressed = true;
                }
                else if (_loc_3 == this.gamesetting.turnleftkey)
                {
                    this.isleftkeypressed = _loc_2;
                    this.keywaspressed = true;
                }
                else if (_loc_3 == this.gamesetting.turnrightkey)
                {
                    this.isrightkeypressed = _loc_2;
                    this.keywaspressed = true;
                }
                else if (_loc_3 == this.gamesetting.deaccelkey)
                {
                    this.isdownkeypressed = _loc_2;
                    this.keywaspressed = true;
                }
                else if (_loc_3 == this.gamesetting.afterburnerskey)
                {
                    this.isshiftkeypressed = _loc_2;
                    this.keywaspressed = true;
                }
                else if (_loc_3 == this.gamesetting.gunskey)
                {
                    this.iscontrolkeypressed = _loc_2;
                }
                else if (_loc_3 == this.gamesetting.missilekey)
                {
                    this.isspacekeypressed = _loc_2;
                }
                else if (_loc_3 == this.gamesetting.dockkey)
                {
                    this.isdkeypressed = _loc_2;
                }
                else if (_loc_3 == this.gamesetting.MapKey)
                {
                    if (!this.TypingInTextField)
                    {
                        this.func_TriggerGameMap();
                    }
                }
                else if (_loc_3 == this.gamesetting.targeterkey)
                {
                }
                else if (_loc_3 == this.gamesetting.OnlineListKey)
                {
                    if (!this.TypingInTextField)
                    {
                        this.fund_OnlineListToggled();
                    }
                }
                else if (_loc_3 == this.gamesetting.HelpKey)
                {
                    if (!this.TypingInTextField)
                    {
                        this.func_TriggerGameHelp();
                    }
                }
            }
            return;
        }// end function

        function frame11()
        {
            trace("DO I GET CALLED, YO?!");
        
            this.gameTutorial.visible = false;
            this.gameNewsBox.visible = false;
            this.gameMap.visible = false;
            this.inGameMapButton.visible = false;
            this.MapTimer = new Timer(100);
            this.MapTimer.addEventListener(TimerEvent.TIMER, this.MapTimerHandler);
            this.playersdestination = new Array();
            this.playersdestination[0] = -1;
            this.inGameMapButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_InGameMap_Click);
            this.gameMap.minimizeButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_InGameMap_Click);
            this.inGameHelp.visible = false;
            this.inGameHelpButton.visible = false;
            this.inGameHelpButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_InGameHelp_Click);
            this.inGameHelp.minimizeButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_InGameHelp_Click);
            this.OnlinePLayerList.onlinePlayersListing.addEventListener(FocusEvent.FOCUS_IN, this.StageReFocusInHandler);
            this.OnlinePLayerList.onlinePlayersListing.text = "";
            this.OnlinePLayerList.visible = true;
            this.currentonlineplayers = new Array();
            this.currentOnlineListdisplay = 0;
            this.AvailableListDisplay = new Array();
            this.AvailableListDisplay[0] = "s/b";
            this.AvailableListDisplay[1] = "k/d";
            this.AvailableListDisplay[2] = "both";
            this.colorNumber = 0;
            this.HeaderColor = "#00FF00";
            this.nameColors = new Array();
            this.nameColors[0] = "#00FFFF";
            this.nameColors[1] = "#99FF00";
            this.nameColors[2] = "#CDFFFF";
            this.totalcol = this.nameColors.length;
            this.gameplaystatus = new Array();
            this.gameplaystatus[1] = new Array();
            this.pingintervalcheck = 20000;
            this.lastpingcheck = getTimer() + 10000;
            this.shiplag = 240;
            this.remoteupdate = false;
            this.GamePingTimer.text = "";
            addEventListener(Event.ENTER_FRAME, this.pingTimerScript);
            this.hasClockBeenSet = false;
            this.ClockIntervalCheckTime = 2000;
            this.timetillnexClocktcheck = getTimer() + 400;
            this.CurenntClockCheckNumber = 0;
            this.ClockCheckstodo = 2;
            this.ClockCheckTimesReceived = new Array();
            this.clocktimediff = 0;
            this.GamePingTimer.addEventListener(FocusEvent.FOCUS_IN, this.StageReFocusInHandler);
            this.inGameSoundButton.visible = false;
            this.inGameSoundButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_InGameSound_Click);
            this.gameSounds = new Object();
            this.gunSoundDelay = 75;
            this.LastMissileHeard = 0;
            this.gameSounds.errorClick = new errorSound();
            this.gameSounds.regularClick = new mainClickSound();
            this.gameSounds.shipExplosion = new shipExplosionSound();
            this.gameSounds.MissileFire = new missilefiresound();
            this.gameSounds.connectingSound = new ConnetingSound();
            this.gameSounds.login_but_pressSound = new loggin_button_pressed();
            this.gameSounds.login_but_popsSound = new login_but_pops();
            this.gameSounds.openingTitleSound = new openingSoundWave();
            //this.gameSounds.MusicLoop0 = new trance_sound();
            this.lastStaticSound = getTimer();
            this.gameSounds.RadioStatic = new RadioStaticSound();
            this.MusicVolumeAdjust = new SoundTransform();
            this.MusicVolumeAdjust.volume = 0.5;
            this.latencyTest = new Array();
            this.totalLatChecks = 8;
            this.NeededLatFails = 5;
            this.specialshipitems = new Array();
            this.pulsarseldammodier = 1;
            this.specialItemno = 0;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Flare 1";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 200;
            this.specialshipitems[this.specialItemno][3] = 10000;
            this.specialshipitems[this.specialItemno][4] = 3000;
            this.specialshipitems[this.specialItemno][5] = "FLARE";
            this.specialshipitems[this.specialItemno][6] = 100000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "specialflare1";
            this.specialshipitems[this.specialItemno][10] = 30 / 100;
            this.specialshipitems[this.specialItemno][11] = 1000;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Flare 2";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 400;
            this.specialshipitems[this.specialItemno][3] = 10000;
            this.specialshipitems[this.specialItemno][4] = 4000;
            this.specialshipitems[this.specialItemno][5] = "FLARE";
            this.specialshipitems[this.specialItemno][6] = 280000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "specialflare2";
            this.specialshipitems[this.specialItemno][10] = 45 / 100;
            this.specialshipitems[this.specialItemno][11] = 1500;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Flare 3";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 600;
            this.specialshipitems[this.specialItemno][3] = 10000;
            this.specialshipitems[this.specialItemno][4] = 5000;
            this.specialshipitems[this.specialItemno][5] = "FLARE";
            this.specialshipitems[this.specialItemno][6] = 750000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "specialflare3";
            this.specialshipitems[this.specialItemno][10] = 60 / 100;
            this.specialshipitems[this.specialItemno][11] = 2000;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Detector 1";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 100;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "DETECTOR";
            this.specialshipitems[this.specialItemno][6] = 350000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "";
            this.specialshipitems[this.specialItemno][10] = 1;
            this.specialshipitems[this.specialItemno][11] = 6000;
            this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
            this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Detector 2";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 220;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "DETECTOR";
            this.specialshipitems[this.specialItemno][6] = 750000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "";
            this.specialshipitems[this.specialItemno][10] = 2;
            this.specialshipitems[this.specialItemno][11] = 5500;
            this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
            this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Detector 3";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 325;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "DETECTOR";
            this.specialshipitems[this.specialItemno][6] = 2150000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "";
            this.specialshipitems[this.specialItemno][10] = 3;
            this.specialshipitems[this.specialItemno][11] = 5000;
            this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
            this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Stealth 1";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 75;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "STEALTH";
            this.specialshipitems[this.specialItemno][6] = 250000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "";
            this.specialshipitems[this.specialItemno][10] = 1;
            this.specialshipitems[this.specialItemno][11] = 5000;
            this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
            this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Stealth 2";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 175;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "STEALTH";
            this.specialshipitems[this.specialItemno][6] = 1250000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "";
            this.specialshipitems[this.specialItemno][10] = 2;
            this.specialshipitems[this.specialItemno][11] = 7500;
            this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
            this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Stealth 3";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 250;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "STEALTH";
            this.specialshipitems[this.specialItemno][6] = 2500000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "";
            this.specialshipitems[this.specialItemno][10] = 3;
            this.specialshipitems[this.specialItemno][11] = 10000;
            this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
            this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Cloak+Stealth 1";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 150;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "CLOAK";
            this.specialshipitems[this.specialItemno][6] = 500000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "";
            this.specialshipitems[this.specialItemno][10] = 1;
            this.specialshipitems[this.specialItemno][11] = 5000;
            this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
            this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Cloak+Stealth 2";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 350;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "CLOAK";
            this.specialshipitems[this.specialItemno][6] = 1750000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "";
            this.specialshipitems[this.specialItemno][10] = 2;
            this.specialshipitems[this.specialItemno][11] = 7500;
            this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
            this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Cloak+Stealth 3";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 650;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "CLOAK";
            this.specialshipitems[this.specialItemno][6] = 4200000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "";
            this.specialshipitems[this.specialItemno][10] = 3;
            this.specialshipitems[this.specialItemno][11] = 10000;
            this.specialshipitems[this.specialItemno][4] = this.specialshipitems[this.specialItemno][11];
            this.specialshipitems[this.specialItemno][3] = this.specialshipitems[this.specialItemno][11];
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Pulsar-1";
            this.specialshipitems[this.specialItemno][1] = 3;
            this.specialshipitems[this.specialItemno][2] = 400;
            this.specialshipitems[this.specialItemno][3] = 1000;
            this.specialshipitems[this.specialItemno][4] = 5000;
            this.specialshipitems[this.specialItemno][5] = "PULSAR";
            this.specialshipitems[this.specialItemno][6] = 50000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "pulsar1";
            this.specialshipitems[this.specialItemno][10] = 95 / 100;
            this.specialshipitems[this.specialItemno][11] = 4000;
            this.specialshipitems[this.specialItemno][12] = 150;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Pulsar-2";
            this.specialshipitems[this.specialItemno][1] = 2;
            this.specialshipitems[this.specialItemno][2] = 600;
            this.specialshipitems[this.specialItemno][3] = 1500;
            this.specialshipitems[this.specialItemno][4] = 7000;
            this.specialshipitems[this.specialItemno][5] = "PULSAR";
            this.specialshipitems[this.specialItemno][6] = 125000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "pulsar2";
            this.specialshipitems[this.specialItemno][10] = 95 / 100;
            this.specialshipitems[this.specialItemno][11] = 7000;
            this.specialshipitems[this.specialItemno][12] = 225;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Pulsar-3";
            this.specialshipitems[this.specialItemno][1] = 1;
            this.specialshipitems[this.specialItemno][2] = 750;
            this.specialshipitems[this.specialItemno][3] = 2000;
            this.specialshipitems[this.specialItemno][4] = 15000;
            this.specialshipitems[this.specialItemno][5] = "PULSAR";
            this.specialshipitems[this.specialItemno][6] = 250000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "pulsar3";
            this.specialshipitems[this.specialItemno][10] = 95 / 100;
            this.specialshipitems[this.specialItemno][11] = 9500;
            this.specialshipitems[this.specialItemno][12] = 300;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Re-Shield-1";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 5;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = 15000;
            this.specialshipitems[this.specialItemno][5] = "RECHARGESHIELD";
            this.specialshipitems[this.specialItemno][6] = 650000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "reshield1";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0;
            this.specialshipitems[this.specialItemno][12] = 0;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Re-Shield-2";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 12;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = 25000;
            this.specialshipitems[this.specialItemno][5] = "RECHARGESHIELD";
            this.specialshipitems[this.specialItemno][6] = 1250000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "reshield2";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0;
            this.specialshipitems[this.specialItemno][12] = 0;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Re-Shield-3";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 20;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = 50000;
            this.specialshipitems[this.specialItemno][5] = "RECHARGESHIELD";
            this.specialshipitems[this.specialItemno][6] = 2500000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "reshield3";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0;
            this.specialshipitems[this.specialItemno][12] = 0;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Re-Struct-1";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 5;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = 25000;
            this.specialshipitems[this.specialItemno][5] = "RECHARGESTRUCT";
            this.specialshipitems[this.specialItemno][6] = 1250000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "restruct1";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0;
            this.specialshipitems[this.specialItemno][12] = 0;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Re-Struct-2";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 12;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = 35000;
            this.specialshipitems[this.specialItemno][5] = "RECHARGESTRUCT";
            this.specialshipitems[this.specialItemno][6] = 2750000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "restruct2";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0;
            this.specialshipitems[this.specialItemno][12] = 0;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Re-Struct-3";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 20;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = 50000;
            this.specialshipitems[this.specialItemno][5] = "RECHARGESTRUCT";
            this.specialshipitems[this.specialItemno][6] = 5890000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "restruct3";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0;
            this.specialshipitems[this.specialItemno][12] = 0;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Mine-1";
            this.specialshipitems[this.specialItemno][1] = 15;
            this.specialshipitems[this.specialItemno][2] = 0;
            this.specialshipitems[this.specialItemno][3] = 5000;
            this.specialshipitems[this.specialItemno][4] = 3000;
            this.specialshipitems[this.specialItemno][5] = "MINES";
            this.specialshipitems[this.specialItemno][6] = 50000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "mine1";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 2000;
            this.specialshipitems[this.specialItemno][12] = 5000;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Mine-2";
            this.specialshipitems[this.specialItemno][1] = 10;
            this.specialshipitems[this.specialItemno][2] = 0;
            this.specialshipitems[this.specialItemno][3] = 5000;
            this.specialshipitems[this.specialItemno][4] = 5500;
            this.specialshipitems[this.specialItemno][5] = "MINES";
            this.specialshipitems[this.specialItemno][6] = 95000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "mine2";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 2000;
            this.specialshipitems[this.specialItemno][12] = 10000;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Mine-3";
            this.specialshipitems[this.specialItemno][1] = 5;
            this.specialshipitems[this.specialItemno][2] = 0;
            this.specialshipitems[this.specialItemno][3] = 5000;
            this.specialshipitems[this.specialItemno][4] = 8000;
            this.specialshipitems[this.specialItemno][5] = "MINES";
            this.specialshipitems[this.specialItemno][6] = 135000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "mine3";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 2000;
            this.specialshipitems[this.specialItemno][12] = 25000;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "HardShield";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 1000;
            this.specialshipitems[this.specialItemno][3] = 7000;
            this.specialshipitems[this.specialItemno][4] = 14000;
            this.specialshipitems[this.specialItemno][5] = "HSHIELD";
            this.specialshipitems[this.specialItemno][6] = 535000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "hardshield1";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0;
            this.specialshipitems[this.specialItemno][12] = 25000;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "InvulnShield";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 1000;
            this.specialshipitems[this.specialItemno][3] = 7000;
            this.specialshipitems[this.specialItemno][4] = 30000;
            this.specialshipitems[this.specialItemno][5] = "INVULNSHIELD";
            this.specialshipitems[this.specialItemno][6] = 535000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "invulnshield1";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0;
            this.specialshipitems[this.specialItemno][12] = 0;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Rap. Missile 1";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 300;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "RAPIDMISSILE";
            this.specialshipitems[this.specialItemno][6] = 750000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "rapmis1";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0.55;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Rap. Missile 2";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 425;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "RAPIDMISSILE";
            this.specialshipitems[this.specialItemno][6] = 2750000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "rapmis2";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0.35;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Rap. Missile 3";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 550;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = -1;
            this.specialshipitems[this.specialItemno][5] = "RAPIDMISSILE";
            this.specialshipitems[this.specialItemno][6] = 4750000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "rapmis3";
            this.specialshipitems[this.specialItemno][10] = 100 / 100;
            this.specialshipitems[this.specialItemno][11] = 0.15;
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Jump Drive";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 1200;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = 70000;
            this.specialshipitems[this.specialItemno][5] = "JUMPDRIVE";
            this.specialshipitems[this.specialItemno][6] = 1500000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "Jump Drive";
            var _loc_1:* = this;
            var _loc_2:* = this.specialItemno + 1;
            _loc_1.specialItemno = _loc_2;
            this.specialshipitems[this.specialItemno] = new Array();
            this.specialshipitems[this.specialItemno][0] = "Wing Man";
            this.specialshipitems[this.specialItemno][1] = -1;
            this.specialshipitems[this.specialItemno][2] = 1200;
            this.specialshipitems[this.specialItemno][3] = -1;
            this.specialshipitems[this.specialItemno][4] = 10000;
            this.specialshipitems[this.specialItemno][5] = "WINGMAN";
            this.specialshipitems[this.specialItemno][6] = 100000;
            this.specialshipitems[this.specialItemno][7] = this.specialItemno;
            this.specialshipitems[this.specialItemno][8] = "Wing Man";
            this.shipDockingImage = getDefinitionByName("shiptypedock") as Class;
            this.shipDeadImage = getDefinitionByName("shiptypedead") as Class;
            this.shipJumpImage = getDefinitionByName("shiptypejump") as Class;
            this.shipNameTag = getDefinitionByName("shipnametag") as Class;
            this.shipHealthBar = getDefinitionByName("shiphealthbar") as Class;
            this.shipRelationIDDisplay = getDefinitionByName("othershipiderDisplay") as Class;
            this.shipTargetDisplay = getDefinitionByName("shiptargetedDisplay") as Class;
            this.shiptype = new Array();
            this.totalsmallships = 16;
            this.guntype = new Array();
            this.shieldgenerators = new Array();
            this.missile = new Array();
            this.basesstartat = 4000;
            this.baseidnumberstart = 4000;
            this.isaracealteredZone = false;
            this.versionno = "2.00.205b";
            this.squadbasedockedat = null;
            this.otherplayerdockedon = null;
            this.lastdamagetobase = -55;
            this.playerrankings = new Array();
            this.playersSessionScoreStart = 0;
            this.starbasepricechanges = new Array();
            this.keypressdelay = 0;
            this.shipositiondelay = 220;
            this.missiontoactualscoremodifier = 10;
            this.publicteams = 20;
            this.lastplayerssavedinfo = "";
            this.isplayeremp = false;
            this.playerempend = 0;
            this.playerdiruptend = 0;
            this.isplayerdisrupt = false;
            this.isplayeraguest = false;
            this.timetillusercanjump = 0;
            this.playersworthtobtymodifire = 1000;
            this.tradegoods = new Array();
            this.energygenerators = new Array();
            this.energycapacitors = new Array();
            this.PlayerInConquestZone = false;
            this.playerfunds = 0;
            this.extraplayerships = new Array();
            this.maxextraships = 5;
            this.playerscurrentextrashipno = 0;
            this.playershipstatus = new Array();
            this.playershipstatus[1] = new Array();
            this.playershipstatus[2] = new Array();
            this.playershipstatus[3] = new Array();
            this.playershipstatus[4] = new Array();
            this.playershipstatus[4][1] = new Array();
            this.playershipstatus[5] = new Array();
            this.playershipstatus[5][10] = "NONE";
            this.playershipstatus[5][4] = "";
            this.playershipstatus[7] = new Array();
            this.playershipstatus[8] = new Array();
            this.playershipstatus[10] = new Array();
            this.playershipstatus[10][0] = 0;
            this.playershipstatus[10][1] = "Single";
            this.playershipstatus[11] = new Array();
            this.playershipstatus[11][1] = new Array();
            this.playershipstatus[5][20] = true;
            this.playershipstatus[5][21] = getTimer() + 7000;
            if (this.playershipstatus[6] == null)
            {
                this.playershipstatus[6] = new Array();
            }
            this.playershipstatus[6][0] = 0;
            this.playershipstatus[6][1] = 0;
            this.NeutralBasetextcolor = "#33FFFF";
            this.FriendlyBasetextcolor = "#ffff00";
            this.EnemyBaseTextColor = "#dd3333";
            this.starbasestayhostiletime = 15000;
            this.teamdeathmatch = false;
            this.sectormapitems = new Array();
            this.starbaselocation = new Array();
            this.teambases = new Array();
            this.teambasetypes = new Array();
            this.playersquadbases = new Array();
            this.starbaseLevel = 8000;
            this.pulsarsinzone = false;
            this.sectorinformation = new Array();
            this.sectorinformation[1] = new Array();
            this.sectorinformation[1][0] = 1000;
            this.sectorinformation[1][1] = 1000;
            this.gamesetting = new Object();
            this.gamesetting.scrollingbckgrnd = true;
            this.gamesetting.showbckgrnd = true;
            this.gamesetting.totalstars = 20;
            this.gamesetting.MapKey = 77;
            this.gamesetting.OnlineListKey = 79;
            this.gamesetting.HelpKey = 72;
            this.gamesetting.chatTextSize = "M";
            this.gamesetting.MusicVolume = 0.1;
            this.gamesetting.IngameVolume = 0.4;
            this.gamesetting.MenuVolume = 0.65;
            this.gamesetting.GameTips = true;
            this.gamesetting.MenuChannelSound = new SoundChannel();
            this.gamesetting.MenuSoundTransform = new SoundTransform();
            this.gamesetting.IngameChannelSound = new SoundChannel();
            this.gamesetting.IngameSoundTransform = new SoundTransform();
            this.func_refreshMenuVolumeSetting();
            this.func_refreshIngameVolumeSetting();
            this.gamesetting.newdefault = new Object();
            this.gamesetting.newdefault.accelkey = 87;
            this.gamesetting.newdefault.deaccelkey = 83;
            this.gamesetting.newdefault.turnleftkey = 65;
            this.gamesetting.newdefault.turnrightkey = 68;
            this.gamesetting.newdefault.missilekey = 76;
            this.gamesetting.newdefault.afterburnerskey = 74;
            this.gamesetting.newdefault.dockkey = 88;
            this.gamesetting.newdefault.gunskey = 75;
            this.gamesetting.newdefault.targeterkey = 84;
            this.gamesetting.newdefault.zoominkey = 188;
            this.gamesetting.newdefault.zoomoutkey = 190;
            this.gamesetting.olddefault = new Object();
            this.gamesetting.olddefault.accelkey = 38;
            this.gamesetting.olddefault.deaccelkey = 40;
            this.gamesetting.olddefault.turnleftkey = 37;
            this.gamesetting.olddefault.turnrightkey = 39;
            this.gamesetting.olddefault.missilekey = 32;
            this.gamesetting.olddefault.afterburnerskey = 16;
            this.gamesetting.olddefault.dockkey = 68;
            this.gamesetting.olddefault.gunskey = 17;
            this.gamesetting.olddefault.targeterkey = 84;
            this.gamesetting.olddefault.zoominkey = 188;
            this.gamesetting.olddefault.zoomoutkey = 190;
            this.func_setToNewDefaultGameKeys();
            this.targetinfo = new Array();
            this.targetinfo[0] = "None";
            this.shipcoordinatex = 0;
            this.shipcoordinatey = 0;
            this.playerjustloggedin = true;
            this.maxdockingvelocity = 40;
            this.playershiprotating = 0;
            this.halfgameareawidth = Math.round(1000 / 2);
            this.halfgameareaheight = Math.round(700 / 2);
            this.playershotsfired = new Array();
            this.playerBeingSeekedByMissile = false;
            this.currenttimechangeratio = 0;
            this.xwidthofasector = 1000;
            this.ywidthofasector = 1000;
            this.secondlastturning = 0;
            this.lastturning = 0;
            this.currentplayershotsfired = 0;
            this.currenthelpframedisplayed = 0;
            this.disruptendsat = 0;
            this.isupkeypressed = false;
            this.isdownkeypressed = false;
            this.isleftkeypressed = false;
            this.isrightkeypressed = false;
            this.iscontrolkeypressed = false;
            this.isdkeypressed = false;
            this.isshiftkeypressed = false;
            this.isspacekeypressed = false;
            this.keywaspressed = false;
            this.playershipafterburnerspeed = 120;
            this.playerrotationdegredation = 0.25;
            this.afterburnerinuse = false;
            this.spacekeyjustpressed = false;
            this.AnglePlayerShipFacing = 0;
            this.afterburnerspeed = 0;
            this.playersFakeVelocity = 0;
            this.shipXmovement = 0;
            this.shipYmovement = 0;
            this.playerShipSpeedRatio = 0;
            this.currentPIpacket = 0;
            this.gunShotBufferData = "";
            this.missileShotBuuferData = "";
            this.lastshipcoordinatex = 0;
            this.lastshipcoordinatey = 0;
            this.NextShipTimeResend = 0;
            this.InfoResendDelay = 5000;
            this.curreKeyUpFrame = 4;
            this.updateFrom = 8;
            this.currentotherplayshot = 0;
            this.currentothermissileshot = 0;
            this.lastMissileSeek = getTimer();
            this.MissileSeekDelay = 500;
            this.lastkeyPress = getTimer();
            this.LastKeyRelease = getTimer();
            stage.addEventListener(KeyboardEvent.KEY_DOWN, this.GameKeyListenerKeyPress);
            stage.addEventListener(KeyboardEvent.KEY_UP, this.GameKeyListenerKeyRelease);
            this.isPlayerChatting = false;
            this.ForwardKey = 87;
            this.LeftKey = 65;
            this.BackwardsKey = 83;
            this.RightKey = 68;
            this.EnterChatKey = 13;
            this.afterburnerkey = 74;
            this.gunfirekey = 75;
            this.missilekey = 76;
            this.dockkey = 88;
            this.MapKey = 77;
            this.TargetShipKey = 84;
            this.credDisplayTimer = new Timer(500);
            this.credDisplayTimer.removeEventListener(TimerEvent.TIMER, this.credDisplayTimerHandler);
            this.credDisplayTimer.addEventListener(TimerEvent.TIMER, this.credDisplayTimerHandler);
            this.credDisplayTimer.start();
            this.DockingTipMessage = new Timer(500);
            this.DockingTipMessage.addEventListener(TimerEvent.TIMER, this.DockingTipTimerHandler);
            this.RadarBlot = getDefinitionByName("radarblot") as Class;
            this.MeteorMapLocaterImage = getDefinitionByName("ingamemapmeteormarker") as Class;
            this.MeteorHolderImage = getDefinitionByName("meteoriteImage") as Class;
            this.gameMeteors = new Array();
            this.maxMeteors = 0;
            addEventListener(Event.ENTER_FRAME, this.func_MeteorsOnGameFrame);
            this.lastMeteorTime = getTimer();
            this.SquadBaseIDAdjust = 6000;
            this.playersquadbaseslvl = 0;
            trace("Loading Squad Bases: \r");
            this.squadbasesalreadyloaded = false;
            this.squadbaseinfo = new Array();
            this.squadbasetype = 0;
            this.missionsavailable = new Array();
            this.playersCurrentMissions = new Array();
            this.MissionCheckTimer = new Timer(1000);
            this.MissionCheckTimer.addEventListener(TimerEvent.TIMER, this.MissioTimerHandler);
            this.MissionCheckTimer.start();
            this.MissionRangeFromNavpoint = 300;
            this.TypingInTextField = false;
            this.teamdeathmatchinfo = new Array();
            this.teamdeathmatchinfo[0] = 300000;
            this.teamdeathmatchinfo[1] = 100000;
            this.teambasetypes[0] = new Array();
            this.teambasetypes[0][0] = 300000;
            this.teambasetypes[0][1] = 800;
            this.teambasetypes[0][2] = 10000;
            this.teambasetypes[0][3] = 300000;
            this.teambasetypes[0][4] = 20000;
            this.squadwarinfo = new Array();
            this.squadwarinfo[0] = false;
            this.squadwarinfo[1] = new Array();
            this.savestatus = "Save Game";
            this.lastplayerssavedinfo = "";
            this.lastplayerssavedinfosent = "";
            return;
        }// end function

        function frame14()
        {
            this.chatDisplay.visible = true;
            this.regularchattextcolor = "#33FFFF";
            this.privatechattextcolor = "#33ff33";
            this.systemchattextcolor = "#33ff33";
            this.teamchattextcolor = "#ffff00";
            this.squadchattextcolor = "#FFCB65";
            this.arenachattextcolor = "#ff99ff";
            this.staffhelpchattextcolor = "#cccc66";
            this.stafchattextcolor = "#cccc66";
            this.playerEnteringCol = "#33ff33";
            this.refreshchatdisplay = false;
            this.gamechatinfo = new Array();
            this.gamechatinfo[1] = new Array();
            this.gamechatinfo[2] = new Array();
            this.gamechatinfo[2][2] = false;
            this.gamechatinfo[2][1] = 30;
            this.gamechatinfo[3] = new Array();
            this.gamechatinfo[3][1] = false;
            this.gamechatinfo[6] = new Array();
            this.gamechatinfo[7] = new Array();
            this.gamechatinfo[7][0] = new Array();
            this.func_buildBlankChat();
            this.func_RefreshChat();
            this.func_enterintochat("Welcome To Stellar Conflicts!", this.arenachattextcolor);
            this.func_enterintochat("?help followed by a message will send a message to any online moderator.", this.arenachattextcolor);
            this.chatDisplay.visible = false;
            this.chatDisplay.textoutline.gotoAndStop(1);
            this.chatDisplay.chatInput.text = "";
            this.lastTimerChatFocused = getTimer();
            this.chatDisplay.chatInput.addEventListener(FocusEvent.FOCUS_IN, this.chatfocusInHandler);
            this.chatDisplay.sendButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_SendChat_Click);
            this.chatDisplay.chatInput.addEventListener(FocusEvent.FOCUS_OUT, this.chatfocusOutHandler);
            this.enterbadwords();
            this.replacewithchar = new Array();
            this.loc = this.replacewithchar.length;
            this.replacewithchar[this.loc] = new Array();
            this.replacewithchar[this.loc][0] = "&apos;";
            this.replacewithchar[this.loc][1] = "\'";
            this.loc = this.replacewithchar.length;
            this.replacewithchar[this.loc] = new Array();
            this.replacewithchar[this.loc][0] = "&quot;";
            this.replacewithchar[this.loc][1] = "\"";
            this.loc = this.replacewithchar.length;
            this.replacewithchar[this.loc] = new Array();
            this.replacewithchar[this.loc][0] = "&gt;";
            this.replacewithchar[this.loc][1] = ">";
            this.loc = this.replacewithchar.length;
            this.replacewithchar[this.loc] = new Array();
            this.replacewithchar[this.loc][0] = "&lt;";
            this.replacewithchar[this.loc][1] = "<";
            this.loc = this.replacewithchar.length;
            this.replacewithchar[this.loc] = new Array();
            this.replacewithchar[this.loc][0] = "&#126;";
            this.replacewithchar[this.loc][1] = "~";
            this.loc = this.replacewithchar.length;
            this.replacewithchar[this.loc] = new Array();
            this.replacewithchar[this.loc][0] = "&#96;";
            this.replacewithchar[this.loc][1] = "`";
            this.chatDisplay.miscOutPut.addEventListener(FocusEvent.FOCUS_IN, this.StageReFocusInHandler);
            this.chatDisplay.textdisplay.addEventListener(FocusEvent.FOCUS_IN, this.StageReFocusInHandler);
            this.chatDisplay.textdisplay.addEventListener(FocusEvent.FOCUS_OUT, this.ChatWindowFocusOut);
            return;
        }// end function

        function frame17()
        {
            this.gameSettingScreen.addEventListener(MouseEvent.MOUSE_UP, this.func_GameSettingScreenPressed);
            this.gameSettingScreen.gameSounds = this.gameSounds;
            stop();
            //Security.loadPolicyFile("xmlsocket://24.183.128.150:843");
            try
            {
            }
            catch (error:Error)
            {
                trace("Error: " + error);
            }
            this.IsSocketConnected = false;
            this.ZonePlayerIsIn = "200";
            this.timebannedfor = 0;
            this.controlledserverclose = false;
            this.gameerror = "hostclosedconnection";
            this.gameerrordoneby = "";
            this.framestobuffer = 4;
            this.othershipbuffer = this.framestobuffer;
            this.shiptimeoutime = 10000;
            this.timeintervalcheck = getTimer();
            this.otherplayership = new Array();
            this.currentotherplayshot = 0;
            this.othergunfire = new Array();
            this.othermissilefire = new Array();
            this.justenteredgame = true;
            this.extrafundsmultiplier = 1000;
            this.mysocket = new XMLSocket();
            this.currentgamestatustext = "";
            this.currentip = "";
            this.currentport = 2151;
            //if (this.isgamerunningfromremote == true)
            //{
            //    this.currentip = "127.0.0.1";
            //    this.ClockCheckstodo = 2;
            //    this.testData.textt.text = "connecting";
            //    this.mysocket.connect(this.currentip, this.currentport);
            //}
            //else
            //{
                this.ClockCheckstodo = 12;
                this.currentip = "24.183.128.150";
                this.mysocket.connect(this.currentip, this.currentport);
            //}   
            this.mysocket.addEventListener(DataEvent.DATA, this.ReceivedData);
            this.mysocket.addEventListener(Event.CONNECT, this.onConnect);
            this.relogInGame = false;
            this.mysocket.addEventListener(Event.CLOSE, this.onClose);
            this.mysocket.addEventListener(IOErrorEvent.IO_ERROR, this.onSocketError);
            return;
        }// end function

        function frame18()
        {
            this.loginmovie.mysocket = this.mysocket;
            this.loginmovie.playershipstatus = this.playershipstatus;
            this.loginmovie.PlayerInConquestZone = this.PlayerInConquestZone;
            this.loginmovie.gameSounds = this.gameSounds;
            this.loginmovie.gamesetting = this.gamesetting;
            stop();
            return;
        }// end function

        public function SpecialsTimerHandler(event:TimerEvent) : void
        {
            var event:* = event;
            try
            {
                this.func_updateSpecialsDisplay();
            }
            catch (error:Error)
            {
            }
            return;
        }// end function

        public function func_setHeadingLocation()
        {
            if (this.playersdestination[0] >= this.SquadBaseIDAdjust)
            {
                if (this.playersquadbases[this.playersdestination[0] - this.SquadBaseIDAdjust] != null)
                {
                    this.playersdestination[1] = this.playersquadbases[this.playersdestination[0] - this.SquadBaseIDAdjust][2];
                    this.playersdestination[2] = this.playersquadbases[this.playersdestination[0] - this.SquadBaseIDAdjust][3];
                    this.playersdestination[4] = this.playersquadbases[this.playersdestination[0] - this.SquadBaseIDAdjust][0];
                }
                else
                {
                    this.playersdestination[0] = -1;
                }
            }
            else if (this.playersdestination[0] > -1)
            {
                this.playersdestination[1] = this.sectormapitems[this.playersdestination[0]][1];
                this.playersdestination[2] = this.sectormapitems[this.playersdestination[0]][2];
                this.playersdestination[4] = this.sectormapitems[this.playersdestination[0]][0];
            }
            else
            {
                this.gameMap.fun_resetDestinatedSelected();
            }
            return;
        }// end function

        function frame26()
        {
            this.turretcontrol.auto.addEventListener(MouseEvent.MOUSE_DOWN, this.autoTurretsSelected);
            this.turretcontrol.manual.addEventListener(MouseEvent.MOUSE_DOWN, this.manualTurretsSelected);
            this.turretcontrol.off.addEventListener(MouseEvent.MOUSE_DOWN, this.offTurretsSelected);
            this.turretCrosshairs.visible = false;
            this.TurretMouseDown = false;
            if (this.playershipstatus[8].length > 0)
            {
                this.func_setTurrets("auto");
            }
            else
            {
                this.func_setTurrets("off");
            }
            this.targetinfo[4] = this.playershipstatus[3][0];
            this.turretCrosshairs.addEventListener(MouseEvent.MOUSE_DOWN, this.setMouseIsDown);
            this.turretCrosshairs.addEventListener(MouseEvent.MOUSE_UP, this.setMouseIsUp);
            stop();
            this.RapidMissilesOn = false;
            this.RapidMissileRateChange = 0;
            this.EnergyPerRapid = 0;
            this.playerSpecialsSettings.isCloaked = false;
            this.playerSpecialsSettings.CloakEnergy = 0;
            this.playerSpecialsSettings.CloakLocation = 0;
            this.playerSpecialsSettings.isStealthed = false;
            this.playerSpecialsSettings.StealthEnergy = 0;
            this.playerSpecialsSettings.StealthLocation = 0;
            this.func_resetSpecials();
            this.func_displayallspecials();
            this.SpecialsTimer = new Timer(500);
            this.SpecialsTimer.addEventListener(TimerEvent.TIMER, this.SpecialsTimerHandler);
            this.SpecialsTimer.start();
            stop();
            this.NavigationTimer = new Timer(350);
            this.NavigationTimer.addEventListener(TimerEvent.TIMER, this.NavigationTimerHandler);
            this.NavigationTimer.start();
            this.TargetDisplay.visible = false;
            this.targetinfo[0] = "";
            this.targetinfo[4] = 99999999999999;
            this.func_MissionsInformationUpdate();
            this.mov_structureWarning.visible = false;
            this.dockingTipText.text = "";
            if (this.gamesetting.GameTips)
            {
                this.DockingTipMessage.start();
            }
            this.missileBankWindow.fireStyle.addEventListener(MouseEvent.MOUSE_DOWN, this.MissileFireStyleChanged);
            if (this.playershipstatus[10][0] > this.playershipstatus[7].length)
            {
                this.playershipstatus[10][0] = 0;
            }
            this.missileBankWindow.func_buildDisplay(this.playershipstatus[7], this.playershipstatus[10][0], this.missile);
            this.missileBankWindow.func_RefreshDisplay(this.playershipstatus[7], this.playershipstatus[10][0], this.playershipstatus[10][1]);
            this.MissileTimer = new Timer(350);
            this.MissileTimer.addEventListener(TimerEvent.TIMER, this.MissileTimerHandler);
            this.MissileTimer.start();
            this.setRadarScale();
            this.gameTutorial.gotoAndStop("playingTut");
            stage.focus = stage;
            this.func_redrawGameSoundsButton();
            stage.focus = stage;
            this.speedratio = 0;
            this.playersshiptype = this.playershipstatus[5][0];
            this.totalstars = 30;
            this.backgroundstar = new Array();
            this.doublegameareawidth = 1600;
            this.doublegameareaheight = 1000;
            this.BackgroundMaxWidth = 1000 * this.sectorinformation[0][0];
            this.HalfBackgroundWidth = this.BackgroundMaxWidth / 2;
            this.BackgroundMaxHeight = 1000 * this.sectorinformation[0][1];
            this.HalfBackgroundMaxHeight = this.BackgroundMaxHeight / 2;
            this.otherplayership = new Array();
            this.othergunfire = new Array();
            this.playershotsfired = new Array();
            this.arrayZoomFactors = new Array();
            this.arrayZoomFactors[0] = 100;
            this.arrayZoomFactors[1] = 90;
            this.arrayZoomFactors[2] = 80;
            this.arrayZoomFactors[3] = 70;
            this.arrayZoomFactors[4] = 60;
            this.arrayZoomFactors[5] = 55;
            this.currZoomFactor = 0;
            this.currentStarFrame = 100;
            this.updateStarInterval = 0;
            this.curentGametime = getTimer();
            this.LastFrameTime = getTimer();
            if (!this.gamesetting.showbckgrnd)
            {
                this.gamedisplayarea.backgroundImage.visible = false;
            }
            this.func_InitPlayerAfterDock();
            addEventListener(Event.ENTER_FRAME, this.func_OnGameFrame);
            this.gamebackground = new MovieClip();
            this.gamedisplayarea.addChild(this.gamebackground);
            this.makebackgroundstars();
            this.func_loadSectorItemsIntoBackgroup();
            this.loadAllBasesintoBackGround();
            this.func_addAllMeteorBackgroundimages();
            this.NavigationImageToAttach = getDefinitionByName("NavigationDirectorImage") as Class;
            this.NavigationImage = new this.NavigationImageToAttach() as MovieClip;
            this.gamedisplayarea.addChild(this.NavigationImage);
            this.NavigationImage.visible = false;
            this.PlayersShipImage = new this.shiptype[this.playersshiptype][10] as MovieClip;
            this.PlayersShipImage.name = "PlayersShipImage";
            this.gamedisplayarea.addChild(this.PlayersShipImage);
            this.PlayersShipShieldImage = new this.shiptype[this.playersshiptype][11] as MovieClip;
            this.gamedisplayarea.addChild(this.PlayersShipShieldImage);
            this.PlayersShipShieldImage.gotoAndStop(4);
            this.PlayersShipImage.gotoAndStop(1);
            this.PlayersShipImage.x = 0;
            this.PlayersShipImage.y = 0;
            return;
        }// end function

        public function playGameMusic()
        {
            // FUCK YOU WITH A RAKE.
            //this.mySndCh = this.gameSounds.MusicLoop0.play();
            //this.mySndCh.addEventListener(Event.SOUND_COMPLETE, this.loopGameMusic);
            //this.MusicVolumeAdjust.volume = this.gamesetting.MusicVolume;
            //this.mySndCh.soundTransform = this.MusicVolumeAdjust;
            return;
        }// end function

        public function PlayerControlsScript(param1)
        {
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            if (this.isplayerdisrupt)
            {
                if (getTimer() > this.playerdiruptend)
                {
                    this.isplayerdisrupt = false;
                }
            }
            this.playerShipSpeedRatio = this.playershipvelocity / this.playershipmaxvelocity;
            var _loc_2:* = "";
            var _loc_3:* = this.playershipstatus[5][0];
            var _loc_4:* = this.shiptype[_loc_3][3][2];
            this.secondlastturning = 0;
            this.lastturning = 0;
            var _loc_5:* = this.sectorinformation[1][0];
            var _loc_6:* = this.sectorinformation[1][1];
            var _loc_7:* = 0;
            var _loc_8:* = getTimer();
            var _loc_9:* = new Array();
            if (this.playershipstatus[5][4] == "alive")
            {
                if (this.iscontrolkeypressed)
                {
                    this.func_runFireScript(_loc_8);
                }
                if (this.isspacekeypressed)
                {
                    this.func_runMissileFireScript(_loc_8);
                }
                var _loc_12:* = this;
                var _loc_13:* = this.curreKeyUpFrame - 1;
                _loc_12.curreKeyUpFrame = _loc_13;
                if (this.curreKeyUpFrame == 0)
                {
                    this.curreKeyUpFrame = 1;
                    if (this.isshiftkeypressed == true)
                    {
                        this.playersFakeVelocity = this.playersFakeVelocity + this.playershipacceleration * param1 * 2;
                        if (this.playersFakeVelocity >= this.playershipmaxvelocity)
                        {
                            this.playersFakeVelocity = this.playershipafterburnerspeed;
                        }
                    }
                    if (!this.isshiftkeypressed)
                    {
                        if (this.isupkeypressed)
                        {
                            this.playersFakeVelocity = this.playersFakeVelocity + this.playershipacceleration * param1;
                            if (this.playersFakeVelocity > this.playershipmaxvelocity)
                            {
                                this.playersFakeVelocity = this.playershipmaxvelocity;
                            }
                        }
                        else if (this.isdownkeypressed)
                        {
                            if (this.playersFakeVelocity > 0 && this.playersFakeVelocity < this.playershipacceleration * param1)
                            {
                                this.playersFakeVelocity = 0;
                            }
                            if (this.playersFakeVelocity > 0)
                            {
                                this.playersFakeVelocity = this.playersFakeVelocity - this.playershipacceleration * param1;
                            }
                        }
                        if (this.playersFakeVelocity > this.playershipmaxvelocity)
                        {
                            this.playersFakeVelocity = this.playershipmaxvelocity;
                        }
                    }
                    this.playershipstatus[6][0] = Math.ceil(this.shipcoordinatex / _loc_5);
                    this.playershipstatus[6][1] = Math.ceil(this.shipcoordinatey / _loc_6);
                    _loc_10 = this.playershipstatus[6][0] + "`" + this.playershipstatus[6][1];
                    _loc_11 = this.playershipstatus[6][2] + "`" + this.playershipstatus[6][3];
                    if (_loc_11 != _loc_10 || this.keywaspressed == true || this.NextShipTimeResend < getTimer())
                    {
                        this.playershipstatus[6][2] = this.playershipstatus[6][0];
                        this.playershipstatus[6][3] = this.playershipstatus[6][1];
                        this.keywaspressed = false;
                        _loc_2 = this.func_GetPlayerSEndOutData(_loc_10, _loc_11, getTimer());
                        this.NextShipTimeResend = getTimer() + this.InfoResendDelay;
                        this.curreKeyUpFrame = 4;
                    }
                    this.playershiprotating = 0;
                    if (this.isrightkeypressed == true)
                    {
                        this.playershiprotating = this.playershiprotating + this.playershiprotation;
                    }
                    if (this.isleftkeypressed == true)
                    {
                        this.playershiprotating = this.playershiprotating - this.playershiprotation;
                    }
                }
                this.playerShipSpeedRatio = this.playersFakeVelocity / this.playershipmaxvelocity;
                if (this.playershiprotating != 0)
                {
                    this.PlayersShipImage.rotation = this.PlayersShipImage.rotation + this.playershiprotating * param1;
                }
            }
            if (this.isdkeypressed)
            {
                this.func_trytoDockTheShip();
            }
            return _loc_2;
        }// end function

        public function func_KillMissionsUpdate()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = undefined;
            _loc_1 = 0;
            while (_loc_1 < this.playersCurrentMissions.length)
            {
                
                if (this.playersCurrentMissions[_loc_1].MissionType == "Kill")
                {
                    var _loc_3:* = this.playersCurrentMissions[_loc_1];
                    var _loc_4:* = _loc_3.ShipsLeftToKill - 1;
                    _loc_3.ShipsLeftToKill = _loc_4;
                    if (_loc_3.ShipsLeftToKill <= 0)
                    {
                        _loc_2 = Number(_loc_3.FundsReward);
                        if (!isNaN(_loc_2))
                        {
                            this.playershipstatus[3][1] = this.playershipstatus[3][1] + _loc_2;
                            this.func_enterintochat("Completed: " + _loc_3.MissionName + " for " + _loc_2, this.privatechattextcolor);
                            this.func_MissionsInformationUpdate();
                            this.playersCurrentMissions.splice(_loc_1, 1);
                            _loc_1 = _loc_1 - 1;
                        }
                    }
                    else
                    {
                        this.func_enterintochat("Updated: " + _loc_3.MissionName + ", Kills left: " + _loc_3.ShipsLeftToKill, this.privatechattextcolor);
                        this.func_MissionsInformationUpdate();
                    }
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_TriggerGameHelp()
        {
            if (this.playershipstatus[5][4] != "")
            {
                if (this.inGameHelp.visible)
                {
                    this.inGameHelp.visible = false;
                }
                else
                {
                    this.inGameHelp.visible = true;
                    if (this.playershipstatus[5][12] == "MOD" || this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "ADMIN")
                    {
                        this.inGameHelp.butt9.visible = true;
                    }
                    else
                    {
                        this.inGameHelp.butt9.visible = false;
                    }
                }
                this.func_RefocusToStage();
            }
            return;
        }// end function

        function frame39()
        {
            this.deathmenu.shiptype = this.shiptype;
            this.deathmenu.playershipstatus = this.playershipstatus;
            this.playershipstatus[2][5] = 0;
            this.playershipstatus[5][4] = "dead";
            this.playershipvelocity = 0;
            this.func_setTurrets("off");
            this.bountychanging = Number(this.playershipstatus[5][3] + this.playershipstatus[5][8]);
            if (this.bountychanging < 1 || this.bountychanging > 100000000 || isNaN(this.bountychanging))
            {
                this.playershipstatus[5][3] = 0;
                this.playershipstatus[5][8] = 0;
            }
            if (this.playershipstatus[3][0] == this.playershipstatus[5][5])
            {
                this.schange = (this.playershipstatus[5][3] + this.playershipstatus[5][8]) * -1;
            }
            else
            {
                this.schange = this.playershipstatus[5][3] + this.playershipstatus[5][8];
            }
            if (this.playershipstatus[3][0] == this.playershipstatus[5][5])
            {
                this.tb = 0;
            }
            else
            {
                this.tb = this.playershipstatus[5][3] + this.playershipstatus[5][8];
            }
            this.datatosend = "TC" + "`" + this.playershipstatus[3][0] + "`SD`" + this.playershipstatus[5][5] + "`" + this.tb + "`" + "0`0`0`0`" + "~";
            trace(this.datatosend);
            this.mysocket.send(this.datatosend);
            this.func_shipExplosionSound(this.shipcoordinatex, this.shipcoordinatey);
            this.playershipstatus[5][3] = 0;
            this.playershipstatus[5][8] = 0;
            this.gamedisplayarea.removeChild(this.PlayersShipImage);
            stop();
            return;
        }// end function

        public function enterbadwords()
        {
            this.badwords = new Array();
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "fuck";
            this.badwords[this.loc][1] = "f**k";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "shit";
            this.badwords[this.loc][1] = "s**t";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "asshole";
            this.badwords[this.loc][1] = "a**h*le";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "nigger";
            this.badwords[this.loc][1] = "ni**er";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "whore";
            this.badwords[this.loc][1] = "wh**e";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "cunt";
            this.badwords[this.loc][1] = "c*nt";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "cock";
            this.badwords[this.loc][1] = "c*ck";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "pussy";
            this.badwords[this.loc][1] = "pu**y";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "fag";
            this.badwords[this.loc][1] = "f*g";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "bitch";
            this.badwords[this.loc][1] = "b**ch";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "jackass";
            this.badwords[this.loc][1] = "j**k*ss";
            this.loc = this.badwords.length;
            this.badwords[this.loc] = new Array();
            this.badwords[this.loc][0] = "fuk";
            this.badwords[this.loc][1] = "f*k";
            return;
        }// end function

        public function MissioTimerHandler(event:TimerEvent) : void
        {
            this.func_checkReconAndPatrolStatus(this.shipcoordinatex, this.shipcoordinatey);
            return;
        }// end function

        public function addSingleMeteorBackgroundImage(param1)
        {
            var mN:* = param1;
            try
            {
                if (this.gameMeteors[mN] != null)
                {
                    this.gamebackground.addChild(this.gameMeteors[mN].meteorImage);
                }
            }
            catch (error:Error)
            {
                trace("Failed Add");
            }
            return;
        }// end function

        public function func_playRegularClick()
        {
            if (this.gamesetting.soundvolume)
            {
                this.gamesetting.MenuChannelSound = this.gameSounds.regularClick.play();
                this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
            }
            return;
        }// end function

        public function chatfocusOutHandler(event:FocusEvent) : void
        {
            this.isPlayerChatting = false;
            stage.focus = this;
            this.chatDisplay.textoutline.gotoAndStop(1);
            return;
        }// end function

        public function saveplayerextraships()
        {
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_1:* = "";
            _loc_1 = _loc_1 + ("NO" + this.playerscurrentextrashipno + "~");
            var _loc_2:* = 0;
            while (_loc_2 < this.extraplayerships.length)
            {
                
                if (this.extraplayerships[_loc_2] != null)
                {
                    if (this.extraplayerships[_loc_2][0] != null)
                    {
                        _loc_1 = _loc_1 + ("SH" + _loc_2 + "`");
                        _loc_1 = _loc_1 + ("ST" + this.extraplayerships[_loc_2][0] + "`");
                        _loc_1 = _loc_1 + ("SG" + this.extraplayerships[_loc_2][1] + "`");
                        _loc_1 = _loc_1 + ("EG" + this.extraplayerships[_loc_2][2] + "`");
                        _loc_1 = _loc_1 + ("EC" + this.extraplayerships[_loc_2][3] + "`");
                        _loc_3 = 0;
                        while (_loc_3 < this.shiptype[this.extraplayerships[_loc_2][0]][2].length)
                        {
                            
                            _loc_1 = _loc_1 + ("HP" + _loc_3 + "G" + this.extraplayerships[_loc_2][4][_loc_3] + "`");
                            _loc_3 = _loc_3 + 1;
                        }
                        _loc_4 = 0;
                        while (_loc_4 < this.shiptype[this.extraplayerships[_loc_2][0]][5].length)
                        {
                            
                            _loc_1 = _loc_1 + ("TT" + _loc_4 + "G" + this.extraplayerships[_loc_2][5][_loc_4] + "`");
                            _loc_4 = _loc_4 + 1;
                        }
                        _loc_5 = 0;
                        while (_loc_5 < this.extraplayerships[_loc_2][11][1].length)
                        {
                            
                            _loc_1 = _loc_1 + ("SP" + this.extraplayerships[_loc_2][11][1][_loc_5][0] + "Q" + this.extraplayerships[_loc_2][11][1][_loc_5][1] + "`");
                            _loc_5 = _loc_5 + 1;
                        }
                        _loc_1 = _loc_1 + "~";
                    }
                }
                _loc_2 = _loc_2 + 1;
            }
            return _loc_1;
        }// end function

        public function func_removeMeteorFromGame(param1)
        {
            var mN:* = param1;
            if (this.gameMeteors[mN] != null)
            {
                try
                {
                    this.gameMap.removeChild(this.gameMeteors[mN].mapMarker);
                }
                catch (error:Error)
                {
                    try
                    {
						this.gamebackground.removeChild(this.gameMeteors[mN].meteorImage);
                    }
					catch (error:Error)
					{
					}
                }
                catch (error:Error)
                {
                }
                this.gameMeteors[mN] = null;
            }
            return;
        }// end function

        public function func_ignoreplayer(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            _loc_2 = false;
            _loc_3 = 0;
            while (_loc_3 < this.gamechatinfo[6].length)
            {
                
                if (param1 == this.gamechatinfo[6][_loc_3])
                {
                    _loc_2 = true;
                    break;
                }
                _loc_3 = _loc_3 + 1;
            }
            return _loc_2;
        }// end function

        public function func_PingRadar()
        {
            this.gameRadar.radarPing.play();
            return;
        }// end function

        public function func_GameOptions_Click(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.gameTutorial.visible = false;
            gotoAndStop("Options");
            return;
        }// end function

        public function func_ReEnterGame(event:MouseEvent) : void
        {
            this.relogInGame = true;
            this.mysocket.close();
            gotoAndPlay(1);
            return;
        }// end function

        function frame60()
        {
            gotoAndStop("maingameplaying");
            return;
        }// end function

        function frame62()
        {
            stage.focus = stage;
            if (this.ZonePlayerIsIn == "100")
            {
                this.gameTutorial.visible = true;
                this.gameTutorial.gotoAndStop(1);
            }
            this.func_checkMusicSettings();
            this.reEnterButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_ReEnterGame);
            this.ExitGameButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_ExitTheGame);
            this.inGameSoundButton.visible = false;
            this.inGameHelpButton.visible = true;
            this.inGameMapButton.visible = true;
            this.saveplayersgame();
            try
            {
                removeEventListener(Event.ENTER_FRAME, this.func_OnGameFrame);
            }
            catch (error:Error)
            {
                trace("Game Event is not loaded");
            }
            try
            {
            }
            catch (error:Error)
            {
                trace("Game Timer is not loaded");
            }
            this.chatDisplay.visible = true;
            this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.visible = false;
            this.creditBar.creditsToDisp = this.playershipstatus[3][1];
            this.playersexitdocktimewait = 0;
            this.timePlayerCanExit = getTimer() + this.playersexitdocktimewait;
            this.playershipstatus[5][4] = "docking";
            this.main_docked_screen.dockedbackground.docked_image_holder.base_exit_button.alpha = 0;
            this.main_docked_screen.dockedbackground.docked_image_holder.base_exit_button.addEventListener(MouseEvent.MOUSE_OVER, this.exitMouseOver);
            this.main_docked_screen.dockedbackground.docked_image_holder.base_exit_button.addEventListener(MouseEvent.MOUSE_OUT, this.buttonMouseOut);
            this.main_docked_screen.dockedbackground.docked_image_holder.base_exit_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_ExitBaseButton_Click);
            this.main_docked_screen.dockedbackground.docked_image_holder.base_hardware_button.alpha = 0;
            this.main_docked_screen.dockedbackground.docked_image_holder.base_hardware_button.addEventListener(MouseEvent.MOUSE_OVER, this.hardwareMouseOver);
            this.main_docked_screen.dockedbackground.docked_image_holder.base_hardware_button.addEventListener(MouseEvent.MOUSE_OUT, this.buttonMouseOut);
            this.main_docked_screen.dockedbackground.docked_image_holder.base_hardware_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_HardwareBaseButton_Click);
            this.main_docked_screen.dockedbackground.docked_image_holder.hangarButton.alpha = 0;
            this.main_docked_screen.dockedbackground.docked_image_holder.hangarButton.addEventListener(MouseEvent.MOUSE_OVER, this.hangarMouseOver);
            this.main_docked_screen.dockedbackground.docked_image_holder.hangarButton.addEventListener(MouseEvent.MOUSE_OUT, this.buttonMouseOut);
            this.main_docked_screen.dockedbackground.docked_image_holder.hangarButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_HangarButton_Click);
            this.main_docked_screen.dockedbackground.docked_image_holder.game_options_button.alpha = 0;
            this.main_docked_screen.dockedbackground.docked_image_holder.game_options_button.addEventListener(MouseEvent.MOUSE_OVER, this.optionsMouseOver);
            this.main_docked_screen.dockedbackground.docked_image_holder.game_options_button.addEventListener(MouseEvent.MOUSE_OUT, this.buttonMouseOut);
            this.main_docked_screen.dockedbackground.docked_image_holder.game_options_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_GameOptions_Click);
            this.main_docked_screen.dockedbackground.docked_image_holder.trageGoodsSelector.alpha = 0;
            this.main_docked_screen.dockedbackground.docked_image_holder.trageGoodsSelector.addEventListener(MouseEvent.MOUSE_OVER, this.tradegoodsMouseOver);
            this.main_docked_screen.dockedbackground.docked_image_holder.trageGoodsSelector.addEventListener(MouseEvent.MOUSE_OUT, this.buttonMouseOut);
            this.main_docked_screen.dockedbackground.docked_image_holder.trageGoodsSelector.addEventListener(MouseEvent.MOUSE_DOWN, this.tradegoods_Click);
            this.main_docked_screen.dockedbackground.docked_image_holder.game_missions_button.alpha = 0;
            this.main_docked_screen.dockedbackground.docked_image_holder.game_missions_button.addEventListener(MouseEvent.MOUSE_OVER, this.MissionsMouseOver);
            this.main_docked_screen.dockedbackground.docked_image_holder.game_missions_button.addEventListener(MouseEvent.MOUSE_OUT, this.buttonMouseOut);
            this.main_docked_screen.dockedbackground.docked_image_holder.game_missions_button.addEventListener(MouseEvent.MOUSE_DOWN, this.Missions_Click);
            this.starbaseexitposition();
            if (!this.teamdeathmatch)
            {
                this.func_GenerateMissions(this.shipcoordinatex, this.shipcoordinatey);
                this.func_checkCargoMissionsFinish(this.playershipstatus[4][0]);
            }
            else
            {
                this.main_docked_screen.dockedbackground.docked_image_holder.trageGoodsSelector.visible = false;
                this.func_GenerateMissions(this.shipcoordinatex, this.shipcoordinatey);
            }
            this.mysocket.send("PI`CLEAR`~");
            this.func_setSelectedIconMessage("");
            this.counterForBaseLives = 0;
            this.DeathMatchTimer = new Timer(1000);
            this.DeathMatchTimer.addEventListener(TimerEvent.TIMER, this.DeathMatchTimerHandler);
            this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.addEventListener(MouseEvent.MOUSE_DOWN, this.func_DeahtMatchInfoClick);
            if (this.teamdeathmatch == true)
            {
                trace("Isd DeathMatch Zone");
                this.DeathMatchTimer.start();
                this.sendnewdmgame();
            }
            stop();
            this.LastDockScreen = "main";
            return;
        }// end function

        public function func_fireMissileSound(param1, param2, param3)
        {
            var _loc_4:* = undefined;
            if (this.gamesetting.CombatSounds)
            {
                if (Math.abs(this.shipcoordinatex - param2) < 800)
                {
                    if (Math.abs(this.shipcoordinatey - param3) < 800)
                    {
                        _loc_4 = getTimer();
                        if (this.LastMissileHeard < _loc_4)
                        {
                            this.gamesetting.IngameChannelSound = this.gameSounds.MissileFire.play();
                            this.gamesetting.IngameChannelSound.soundTransform = this.gamesetting.IngameSoundTransform;
                            this.LastMissileHeard = _loc_4 + this.gunSoundDelay;
                        }
                    }
                }
            }
            return;
        }// end function

        public function func_getPlayersName(param1)
        {
            var _loc_2:* = 0;
            while (_loc_2 < this.currentonlineplayers.length)
            {
                
                if (param1 == this.currentonlineplayers[_loc_2][0])
                {
                    return this.currentonlineplayers[_loc_2][1];
                }
                _loc_2 = _loc_2 + 1;
            }
            return "";
        }// end function

        public function othergunfiremovement(param1)
        {
            var playerwhoshothitid:*;
            var gunshothitplayertype:*;
            var raceOfHitter:*;
            var shielddamagedone:*;
            var structdamagedone:*;
            var piercingdamagedone:*;
            var zzj:*;
            var dataupdated:*;
            var TimeChangeRatio:* = param1;
            var curenttime:* = getTimer();
            var cc:*;
            var playersx:* = this.shipcoordinatex;
            var playersy:* = this.shipcoordinatey;
            var halfplayerswidth:* = Math.round(this.PlayersShipImage.width / 2);
            var halfplayersheight:* = Math.round(this.PlayersShipImage.height / 2);
            cc = 0;
            while (cc < this.othergunfire.length)
            {
                
                this.othergunfire[cc][1] = this.othergunfire[cc][1] + this.othergunfire[cc][3] * TimeChangeRatio;
                this.othergunfire[cc][2] = this.othergunfire[cc][2] + this.othergunfire[cc][4] * TimeChangeRatio;
                try
                {
                    this.othergunfire[cc][16].x = this.othergunfire[cc][1] - this.shipcoordinatex;
                    this.othergunfire[cc][16].y = this.othergunfire[cc][2] - this.shipcoordinatey;
                }
                catch (error:Error)
                {
                    trace("Error moving other player gunfire: " + error);
                }
                if (this.othergunfire[cc][5] < curenttime)
                {
                    try
                    {
                        this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                    }
                    catch (error:Error)
                    {
                        trace("Error removing otherplayershot - timeout: " + error);
                    }
                    this.othergunfire.splice(cc, 1);
                    cc = (cc - 1);
                }
                else if (this.othergunfire[cc][14] == true || this.playershipstatus[5][4] != "alive")
                {
                }
                else if (this.myLineHittest(this.shipcoordinatex, this.shipcoordinatey, Math.round(this.PlayersShipImage.width / 2) + this.othergunfire[cc][11], this.othergunfire[cc][18], this.othergunfire[cc][19], this.othergunfire[cc][1], this.othergunfire[cc][2]))
                {
                    try
                    {
                        this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                    }
                    catch (error:Error)
                    {
                        trace("Error removing otherplayershot - playerhit: " + error);
                    }
                    playerwhoshothitid = this.othergunfire[cc][0];
                    gunshothitplayertype = this.othergunfire[cc][7];
                    shielddamagedone = this.guntype[gunshothitplayertype][4];
                    structdamagedone = this.guntype[gunshothitplayertype][7];
                    piercingdamagedone = this.guntype[gunshothitplayertype][8];
                    if (!this.playershipstatus[5][20])
                    {
                        this.playershipstatus[2][1] = this.playershipstatus[2][1] - shielddamagedone;
                        if (piercingdamagedone > 0)
                        {
                            this.playershipstatus[2][5] = this.playershipstatus[2][5] - piercingdamagedone;
                        }
                    }
                    if (this.playershipstatus[2][1] < 0)
                    {
                        this.playershipstatus[2][5] = this.playershipstatus[2][5] - structdamagedone;
                        this.playershipstatus[2][1] = 0;
                    }
                    this.func_playerGotHit();
                    this.gunShotBufferData = this.gunShotBufferData + ("GH" + "`" + this.playershipstatus[3][0] + "`");
                    this.gunShotBufferData = this.gunShotBufferData + (this.othergunfire[cc][0] + "`");
                    this.gunShotBufferData = this.gunShotBufferData + (this.othergunfire[cc][9] + "`SH`" + Math.round(this.playershipstatus[2][1]));
                    this.gunShotBufferData = this.gunShotBufferData + ("`ST`" + Math.round(this.playershipstatus[2][5]) + "~");
                    this.othergunfire.splice(cc, 1);
                    if (this.playershipstatus[2][5] <= 0 && this.playershipstatus[5][4] == "alive")
                    {
                        this.playershipstatus[5][4] = "dead";
                        this.playershipstatus[5][5] = playerwhoshothitid;
                        zzj = 0;
                        dataupdated = false;
                        while (zzj < this.currentonlineplayers.length && dataupdated == false)
                        {
                            
                            if (playerwhoshothitid == this.currentonlineplayers[zzj][0])
                            {
                                dataupdated = true;
                                this.playershipstatus[5][7] = this.currentonlineplayers[zzj][1];
                            }
                            zzj = (zzj + 1);
                        }
                        this.playershipstatus[2][5] = 0;
                        gotoAndPlay("playerdeath");
                    }
                    cc = (cc - 1);
                }
                else
                {
                    this.othergunfire[cc][18] = this.othergunfire[cc][1];
                    this.othergunfire[cc][19] = this.othergunfire[cc][2];
                }
                cc = (cc + 1);
            }
            return;
        }// end function

        public function DisplayOnlinePlayersListDetails()
        {
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            var _loc_15:* = undefined;
            var _loc_1:* = "";
            var _loc_2:* = 0;
            var _loc_3:* = new Array();
            var _loc_4:* = 0;
            while (_loc_4 < this.currentonlineplayers.length)
            {
                
                if (this.currentonlineplayers[_loc_4][0] >= 0)
                {
                    _loc_8 = -1;
                    if (isNaN(Number(this.currentonlineplayers[_loc_4][4])) || Number(this.currentonlineplayers[_loc_4][4] == -1))
                    {
                        _loc_8 = -1;
                    }
                    else
                    {
                        _loc_8 = Number(this.currentonlineplayers[_loc_4][4]);
                    }
                    _loc_9 = false;
                    _loc_10 = 0;
                    while (_loc_10 < _loc_3.length)
                    {
                        
                        if (_loc_3[_loc_10][0] == _loc_8)
                        {
                            _loc_11 = _loc_3[_loc_10][1].length;
                            _loc_3[_loc_10][1][_loc_11] = new Array();
                            _loc_3[_loc_10][1][_loc_11][0] = this.currentonlineplayers[_loc_4][1];
                            if (this.currentonlineplayers[_loc_4][10].length > 0)
                            {
                                _loc_3[_loc_10][1][_loc_11][0] = "(" + this.currentonlineplayers[_loc_4][10] + ") " + _loc_3[_loc_10][1][_loc_11][0];
                            }
                            _loc_3[_loc_10][1][_loc_11][1] = this.currentonlineplayers[_loc_4][5];
                            _loc_3[_loc_10][1][_loc_11][2] = this.currentonlineplayers[_loc_4][6];
                            _loc_3[_loc_10][1][_loc_11][3] = this.currentonlineplayers[_loc_4][7];
                            _loc_3[_loc_10][1][_loc_11][4] = this.currentonlineplayers[_loc_4][3];
                            _loc_3[_loc_10][1][_loc_11][5] = this.currentonlineplayers[_loc_4][9];
                            _loc_9 = true;
                            break;
                        }
                        _loc_10 = _loc_10 + 1;
                    }
                    if (_loc_9 == false)
                    {
                        _loc_12 = _loc_3.length;
                        _loc_3[_loc_12] = new Array();
                        _loc_3[_loc_12][0] = _loc_8;
                        _loc_3[_loc_12][1] = new Array();
                        _loc_3[_loc_12][1][0] = new Array();
                        _loc_3[_loc_12][1][0][0] = this.currentonlineplayers[_loc_4][1];
                        if (this.currentonlineplayers[_loc_4][10].length > 0)
                        {
                            _loc_3[_loc_12][1][0][0] = "(" + this.currentonlineplayers[_loc_4][10] + ") " + _loc_3[_loc_12][1][0][0];
                        }
                        _loc_3[_loc_12][1][0][1] = this.currentonlineplayers[_loc_4][5];
                        _loc_3[_loc_12][1][0][2] = this.currentonlineplayers[_loc_4][6];
                        _loc_3[_loc_12][1][0][3] = this.currentonlineplayers[_loc_4][7];
                        _loc_3[_loc_12][1][0][4] = this.currentonlineplayers[_loc_4][3];
                        _loc_3[_loc_10][1][0][5] = this.currentonlineplayers[_loc_4][9];
                    }
                }
                _loc_4 = _loc_4 + 1;
            }
            var _loc_5:* = 1;
            while (_loc_5 < _loc_3.length)
            {
                
                if (_loc_3[_loc_5][0] < _loc_3[(_loc_5 - 1)][0])
                {
                    _loc_13 = _loc_3[_loc_5];
                    _loc_3[_loc_5] = _loc_3[(_loc_5 - 1)];
                    _loc_3[(_loc_5 - 1)] = _loc_13;
                    _loc_5 = 0;
                }
                _loc_5 = _loc_5 + 1;
            }
            var _loc_6:* = "";
            var _loc_7:* = 0;
            while (_loc_7 < _loc_3.length)
            {
                
                if (_loc_3[_loc_7][0] < 0)
                {
                    _loc_6 = _loc_6 + ("<font color=\"" + this.HeaderColor + "\">Teamless players</FONT>\r");
                }
                else
                {
                    _loc_6 = _loc_6 + ("<font color=\"" + this.HeaderColor + "\">Team: " + _loc_3[_loc_7][0] + "</FONT>\r");
                }
                _loc_14 = 0;
                _loc_15 = 0;
                while (_loc_15 < _loc_3[_loc_7][1].length)
                {
                    
                    _loc_6 = _loc_6 + ("<font color=\"" + this.nameColors[_loc_14] + "\">" + _loc_3[_loc_7][1][_loc_15][0] + "   Score:" + _loc_3[_loc_7][1][_loc_15][1] + "   K:" + _loc_3[_loc_7][1][_loc_15][2] + "   D:" + _loc_3[_loc_7][1][_loc_15][3] + "   BTY:" + _loc_3[_loc_7][1][_loc_15][4] + "   Squad: " + _loc_3[_loc_7][1][_loc_15][5] + "</FONT>\r");
                    if (++_loc_14 >= this.totalcol)
                    {
                        _loc_14 = 0;
                    }
                    _loc_15 = _loc_15 + 1;
                }
                _loc_6 = _loc_6 + "\r";
                _loc_7 = _loc_7 + 1;
            }
            this.OnlinePLayerList.onlineListDetailsone.htmlText = _loc_6;
            return;
        }// end function

        function frame73()
        {
            this.ShipHardwareScreen.guntype = this.guntype;
            this.ShipHardwareScreen.shieldgenerators = this.shieldgenerators;
            this.ShipHardwareScreen.missile = this.missile;
            this.ShipHardwareScreen.shiptype = this.shiptype;
            this.ShipHardwareScreen.energycapacitors = this.energycapacitors;
            this.ShipHardwareScreen.energygenerators = this.energygenerators;
            this.ShipHardwareScreen.specialshipitems = this.specialshipitems;
            this.ShipHardwareScreen.playershipstatus = this.playershipstatus;
            this.ShipHardwareScreen.gameSounds = this.gameSounds;
            this.ShipHardwareScreen.gamesetting = this.gamesetting;
            this.ShipHardwareScreen.exit_but.addEventListener(MouseEvent.MOUSE_DOWN, this.func_exitHardWareScreen);
            return;
        }// end function

        public function func_SeekTheOtherPlayersMissile(param1)
        {
            var _loc_10:* = undefined;
            var _loc_2:* = false;
            var _loc_3:* = this.shipcoordinatex;
            var _loc_4:* = this.shipcoordinatey;
            var _loc_5:* = this.othermissilefire[param1][1];
            var _loc_6:* = this.othermissilefire[param1][2];
            if (_loc_2)
            {
                _loc_3 = 0;
                _loc_4 = 0;
            }
            else
            {
                _loc_3 = this.shipcoordinatex;
                _loc_4 = this.shipcoordinatey;
            }
            var _loc_7:* = Math.round(Math.sqrt((_loc_3 - this.xposition) * (_loc_3 - this.xposition) + (_loc_4 - this.yposition) * (_loc_4 - this.yposition)));
            var _loc_8:* = Math.atan2(_loc_3 - _loc_5, _loc_6 - _loc_4) * (180 / Math.PI);
            _loc_8 = Math.atan2(_loc_3 - _loc_5, _loc_6 - _loc_4) * (180 / Math.PI) - this.othermissilefire[param1][6];
            trace(_loc_8);
            var _loc_9:* = "";
            if (_loc_8 > 360)
            {
                _loc_8 = _loc_8 - 360;
            }
            if (_loc_8 < 0)
            {
                _loc_8 = _loc_8 + 360;
            }
            if (_loc_8 < 0)
            {
                _loc_8 = _loc_8 + 360;
            }
            if (_loc_8 < 10 || _loc_8 >= 350)
            {
                if (_loc_7 < 30)
                {
                }
            }
            else if (_loc_8 <= 180)
            {
                _loc_9 = _loc_9 + "R";
            }
            else
            {
                _loc_9 = _loc_9 + "L";
            }
            if (_loc_9 != this.othermissilefire[param1][15])
            {
                this.othermissilefire[param1][15] = _loc_9;
                _loc_10 = "MI" + "`" + this.othermissilefire[param1][0] + "`" + Math.round(_loc_5) + "`" + Math.round(_loc_6) + "`" + Math.round(this.othermissilefire[param1][8]) + "`" + Math.round(this.othermissilefire[param1][6]) + "`" + "`" + this.othermissilefire[param1][9] + "`" + this.othermissilefire[param1][15] + "~";
                this.gunShotBufferData = this.gunShotBufferData + _loc_10;
            }
            return;
        }// end function

        public function func_GenerateMissions(param1, param2)
        {
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            _loc_3 = 10;
            _loc_4 = 9000;
            this.missionsavailable = new Array();
            _loc_5 = 0;
            _loc_6 = 0;
            _loc_7 = -1;
            _loc_8 = -1;
            trace("1");
            _loc_10 = new Array();
            _loc_6 = 0;
            while (_loc_6 < this.sectormapitems.length)
            {
                
                if (this.sectormapitems[_loc_6] != null)
                {
                    if (this.sectormapitems[_loc_6][0].substr(0, 2) == "NP")
                    {
                        _loc_10[_loc_10.length] = _loc_6;
                    }
                }
                _loc_6 = _loc_6 + 1;
            }
            _loc_6 = 0;
            while (_loc_6 < _loc_10.length)
            {
                
                _loc_5 = this.missionsavailable.length;
                this.missionsavailable[_loc_5] = new Object();
                this.missionsavailable[_loc_5].MissionType = "Recon";
                this.missionsavailable[_loc_5].MissionName = "Recon at " + this.sectormapitems[_loc_10[_loc_6]][0];
                this.missionsavailable[_loc_5].Locations = new Array();
                this.missionsavailable[_loc_5].Locations[0] = new Object();
                this.missionsavailable[_loc_5].Locations[0].MarkerName = this.sectormapitems[_loc_10[_loc_6]][0];
                this.missionsavailable[_loc_5].Locations[0].Xcoord = this.sectormapitems[_loc_10[_loc_6]][1];
                this.missionsavailable[_loc_5].Locations[0].Ycoord = this.sectormapitems[_loc_10[_loc_6]][2];
                _loc_9 = Math.sqrt((param1 - this.missionsavailable[_loc_5].Locations[0].Xcoord) * (param1 - this.missionsavailable[_loc_5].Locations[0].Xcoord) + (param2 - this.missionsavailable[_loc_5].Locations[0].Ycoord) * (param2 - this.missionsavailable[_loc_5].Locations[0].Ycoord));
                this.missionsavailable[_loc_5].FundsReward = Math.floor(_loc_9 * _loc_3);
                _loc_6 = _loc_6 + 1;
            }
            if (_loc_10.length > 3)
            {
                _loc_6 = 0;
                while (_loc_6 < _loc_10.length)
                {
                    
                    _loc_7 = -1;
                    _loc_8 = -1;
                    _loc_5 = this.missionsavailable.length;
                    this.missionsavailable[_loc_5] = new Object();
                    this.missionsavailable[_loc_5].MissionType = "Patrol";
                    this.missionsavailable[_loc_5].MissionName = "Patrol ";
                    this.missionsavailable[_loc_5].Locations = new Array();
                    this.missionsavailable[_loc_5].Locations[0] = new Object();
                    this.missionsavailable[_loc_5].Locations[0].MarkerName = this.sectormapitems[_loc_10[_loc_6]][0];
                    this.missionsavailable[_loc_5].Locations[0].Xcoord = this.sectormapitems[_loc_10[_loc_6]][1];
                    this.missionsavailable[_loc_5].Locations[0].Ycoord = this.sectormapitems[_loc_10[_loc_6]][2];
                    _loc_9 = Math.sqrt((param1 - this.missionsavailable[_loc_5].Locations[0].Xcoord) * (param1 - this.missionsavailable[_loc_5].Locations[0].Xcoord) + (param2 - this.missionsavailable[_loc_5].Locations[0].Ycoord) * (param2 - this.missionsavailable[_loc_5].Locations[0].Ycoord));
                    this.missionsavailable[_loc_5].FundsReward = _loc_9 * _loc_3;
                    this.missionsavailable[_loc_5].MissionName = this.missionsavailable[_loc_5].MissionName + (this.sectormapitems[_loc_10[_loc_6]][0] + " ");
                    while (_loc_7 == -1)
                    {
                        
                        _loc_7 = Math.round(Math.random() * (_loc_10.length - 1));
                        if (_loc_7 != _loc_6)
                        {
                            this.missionsavailable[_loc_5].Locations[1] = new Object();
                            this.missionsavailable[_loc_5].Locations[1].MarkerName = this.sectormapitems[_loc_10[_loc_7]][0];
                            this.missionsavailable[_loc_5].Locations[1].Xcoord = this.sectormapitems[_loc_10[_loc_7]][1];
                            this.missionsavailable[_loc_5].Locations[1].Ycoord = this.sectormapitems[_loc_10[_loc_7]][2];
                            _loc_9 = Math.sqrt((this.missionsavailable[_loc_5].Locations[0].Xcoord - this.missionsavailable[_loc_5].Locations[1].Xcoord) * (this.missionsavailable[_loc_5].Locations[0].Xcoord - this.missionsavailable[_loc_5].Locations[1].Xcoord) + (this.missionsavailable[_loc_5].Locations[0].Ycoord - this.missionsavailable[_loc_5].Locations[1].Ycoord) * (this.missionsavailable[_loc_5].Locations[0].Ycoord - this.missionsavailable[_loc_5].Locations[1].Ycoord));
                            this.missionsavailable[_loc_5].FundsReward = this.missionsavailable[_loc_5].FundsReward + _loc_9 * _loc_3;
                            this.missionsavailable[_loc_5].MissionName = this.missionsavailable[_loc_5].MissionName + (this.sectormapitems[_loc_10[_loc_7]][0] + " ");
                            while (_loc_8 == -1)
                            {
                                
                                _loc_8 = Math.round(Math.random() * (_loc_10.length - 1));
                                if (_loc_8 != _loc_6 && _loc_8 != _loc_7)
                                {
                                    this.missionsavailable[_loc_5].Locations[2] = new Object();
                                    this.missionsavailable[_loc_5].Locations[2].MarkerName = this.sectormapitems[_loc_10[_loc_8]][0];
                                    this.missionsavailable[_loc_5].Locations[2].Xcoord = this.sectormapitems[_loc_10[_loc_8]][1];
                                    this.missionsavailable[_loc_5].Locations[2].Ycoord = this.sectormapitems[_loc_10[_loc_8]][2];
                                    _loc_9 = Math.sqrt((this.missionsavailable[_loc_5].Locations[1].Xcoord - this.missionsavailable[_loc_5].Locations[2].Xcoord) * (this.missionsavailable[_loc_5].Locations[1].Xcoord - this.missionsavailable[_loc_5].Locations[2].Xcoord) + (this.missionsavailable[_loc_5].Locations[1].Ycoord - this.missionsavailable[_loc_5].Locations[2].Ycoord) * (this.missionsavailable[_loc_5].Locations[1].Ycoord - this.missionsavailable[_loc_5].Locations[2].Ycoord));
                                    this.missionsavailable[_loc_5].FundsReward = this.missionsavailable[_loc_5].FundsReward + _loc_9 * _loc_3;
                                    this.missionsavailable[_loc_5].MissionName = this.missionsavailable[_loc_5].MissionName + this.sectormapitems[_loc_10[_loc_8]][0];
                                    continue;
                                }
                                _loc_8 = -1;
                            }
                            continue;
                        }
                        _loc_7 = -1;
                    }
                    this.missionsavailable[_loc_5].FundsReward = Math.floor(this.missionsavailable[_loc_5].FundsReward);
                    _loc_6 = _loc_6 + 1;
                }
            }
            _loc_6 = 0;
            while (_loc_6 < this.starbaselocation.length)
            {
                
                if (this.starbaselocation[_loc_6] != null)
                {
                    if (this.starbaselocation[_loc_6][0] != this.playershipstatus[4][0] && 1 == 1)
                    {
                        _loc_5 = this.missionsavailable.length;
                        this.missionsavailable[_loc_5] = new Object();
                        this.missionsavailable[_loc_5].MissionType = "Cargo";
                        this.missionsavailable[_loc_5].MissionName = "Deliver Cargo to " + this.starbaselocation[_loc_6][0];
                        this.missionsavailable[_loc_5].Locations = new Array();
                        this.missionsavailable[_loc_5].Locations[0] = new Object();
                        this.missionsavailable[_loc_5].Locations[0].MarkerName = this.starbaselocation[_loc_6][0];
                        this.missionsavailable[_loc_5].Locations[0].Xcoord = this.starbaselocation[_loc_6][1];
                        this.missionsavailable[_loc_5].Locations[0].Ycoord = this.starbaselocation[_loc_6][2];
                        _loc_9 = Math.sqrt((param1 - this.missionsavailable[_loc_5].Locations[0].Xcoord) * (param1 - this.missionsavailable[_loc_5].Locations[0].Xcoord) + (param2 - this.missionsavailable[_loc_5].Locations[0].Ycoord) * (param2 - this.missionsavailable[_loc_5].Locations[0].Ycoord));
                        this.missionsavailable[_loc_5].FundsReward = Math.floor(_loc_9 * _loc_3);
                    }
                }
                _loc_6 = _loc_6 + 1;
            }
            _loc_6 = 0;
            while (_loc_6 < 4)
            {
                
                _loc_5 = this.missionsavailable.length;
                this.missionsavailable[_loc_5] = new Object();
                this.missionsavailable[_loc_5].MissionType = "Kill";
                this.missionsavailable[_loc_5].MissionName = "Kill " + (_loc_6 + 3) + " Ships";
                this.missionsavailable[_loc_5].Locations = new Array();
                this.missionsavailable[_loc_5].ShipsLeftToKill = _loc_6 + 3;
                this.missionsavailable[_loc_5].FundsReward = Math.floor((_loc_6 + 3) * _loc_4);
                _loc_6 = _loc_6 + 1;
            }
            return;
        }// end function

        function frame88()
        {
            this.LastDockScreen = "hangar";
            this.hangarWindow.playerscurrentextrashipno = this.playerscurrentextrashipno;
            this.hangarWindow.extraplayerships = this.extraplayerships;
            this.hangarWindow.guntype = this.guntype;
            this.hangarWindow.shieldgenerators = this.shieldgenerators;
            this.hangarWindow.missile = this.missile;
            this.hangarWindow.shiptype = this.shiptype;
            this.hangarWindow.energycapacitors = this.energycapacitors;
            this.hangarWindow.energygenerators = this.energygenerators;
            this.hangarWindow.specialshipitems = this.specialshipitems;
            this.hangarWindow.playershipstatus = this.playershipstatus;
            this.hangarWindow.maxShipsToOwn = this.maxextraships;
            this.hangarWindow.gameSounds = this.gameSounds;
            this.hangarWindow.gamesetting = this.gamesetting;
            this.OnlinePLayerList.visible = false;
            this.hangarWindow.exit_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_exitHangarScreen);
            return;
        }// end function

        public function func_funcDeathMatchProcc(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            _loc_2 = 0;
            if (param1[1] == "BL")
            {
                _loc_2 = 2;
                while (_loc_2 < param1.length)
                {
                    
                    this.teambases[_loc_2 - 2][10] = Number(param1[_loc_2]);
                    if (this.teambases[_loc_2 - 2][10] < 1)
                    {
                        this.teambases[_loc_2 - 2][10] = 1;
                    }
                    this.func_updateTameBaseHealthBars(_loc_2 - 2);
                    _loc_2 = _loc_2 + 1;
                }
            }
            else if (param1[1] == "BH")
            {
                this.teambases[Number(param1[2])][10] = Number(param1[3]);
                this.func_updateTameBaseHealthBars(Number(param1[2]));
            }
            else if (param1[1] == "NG")
            {
                _loc_2 = 2;
                while (_loc_2 < param1.length)
                {
                    
                    this.teambases[_loc_2 - 2][10] = Number(param1[_loc_2]);
                    this.teambases[_loc_2 - 2][11] = this.teambasetypes[this.teambases[_loc_2 - 2][3]][2];
                    if (this.teambases[_loc_2 - 2][10] < 1)
                    {
                        this.teambases[_loc_2 - 2][10] = 1;
                    }
                    _loc_2 = _loc_2 + 1;
                }
            }
            else if (param1[1] == "EG")
            {
                if (this.teamdeathmatch == true)
                {
                    _loc_3 = Number(param1[3]);
                    _loc_4 = Number(param1[2]);
                    _loc_5 = this.teambases[_loc_4][0].substr(2);
                    if (this.squadwarinfo[0] == true)
                    {
                        _loc_5 = this.squadwarinfo[1][_loc_4];
                    }
                    this.teambases[_loc_4][10] = 0;
                    _loc_6 = "None";
                    _loc_7 = "No One";
                    _loc_8 = "None";
                    _loc_2 = 0;
                    while (_loc_2 < this.currentonlineplayers.length)
                    {
                        
                        if (this.currentonlineplayers[_loc_2][0] == _loc_3)
                        {
                            _loc_6 = Number(this.currentonlineplayers[_loc_2][4]);
                            _loc_7 = this.currentonlineplayers[_loc_2][1];
                            _loc_8 = this.teambases[_loc_6][0].substr(2);
                            if (this.squadwarinfo[0] == true)
                            {
                                _loc_8 = this.squadwarinfo[1][_loc_6];
                            }
                        }
                        _loc_2 = _loc_2 + 1;
                    }
                    _loc_9 = int(param1[4]);
                    _loc_10 = int(param1[5]);
                    _loc_11 = "";
                    _loc_11 = "HOST: Player " + _loc_7 + " of Team " + _loc_8 + " has destroyed " + _loc_5 + "\'s Base";
                    this.func_enterintochat(_loc_11, this.systemchattextcolor);
                    _loc_11 = "HOST: Reward for Team " + _loc_8 + " is " + _loc_9 + " Funds and " + _loc_10 + " Score";
                    this.func_enterintochat(_loc_11, this.systemchattextcolor);
                    if (this.playershipstatus[5][2] == _loc_6)
                    {
                        this.playershipstatus[5][9] = this.playershipstatus[5][9] + Number(_loc_10);
                        this.playershipstatus[3][1] = this.playershipstatus[3][1] + Number(_loc_9);
                    }
                    _loc_2 = 0;
                    while (_loc_2 < this.currentonlineplayers.length)
                    {
                        
                        if (this.currentonlineplayers[_loc_2][0] >= 0)
                        {
                            if (this.currentonlineplayers[_loc_2][4] == _loc_6)
                            {
                                this.currentonlineplayers[_loc_2][5] = this.currentonlineplayers[_loc_2][5] + Number(_loc_10);
                            }
                        }
                        _loc_2 = _loc_2 + 1;
                    }
                    this.playershipstatus[5][2] = "N/A";
                    _loc_2 = 0;
                    while (_loc_2 < this.currentonlineplayers.length)
                    {
                        
                        if (this.currentonlineplayers[_loc_2][0] >= 0)
                        {
                            this.currentonlineplayers[_loc_2][4] = "N/A";
                        }
                        _loc_2 = _loc_2 + 1;
                    }
                    trace("3");
                    if (this.squadwarinfo[0] == true)
                    {
                        this.squadwarinfo[0] = false;
                        this.squadwarinfo[2] = false;
                    }
                    trace("4");
                    if (this.playershipstatus[5][4] != "docking")
                    {
                        gotoAndStop("dockedscreen");
                    }
                }
            }
            return;
        }// end function

        public function func_NormalRadar()
        {
            this.gameRadar.gotoAndStop(1);
            return;
        }// end function

        public function ReceivedData(param1) : void
        {
            this.dataprocess(param1.data);
            return;
        }// end function

        public function func_trytoDockTheShip()
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_9:* = undefined;
            var _loc_1:* = null;
            var _loc_7:* = 0;
            while (_loc_7 < this.starbaselocation.length)
            {
                
                if (this.starbaselocation[_loc_7] != null)
                {
                    if (this.starbaselocation[_loc_7][5] == "ACTIVE" || Number(this.starbaselocation[_loc_7][5]) < getTimer())
                    {
                        _loc_2 = this.starbaselocation[_loc_7][1];
                        _loc_3 = this.starbaselocation[_loc_7][2];
                        _loc_4 = this.starbaselocation[_loc_7][4];
                        _loc_5 = _loc_2 - this.shipcoordinatex;
                        _loc_6 = _loc_3 - this.shipcoordinatey;
                        if (_loc_5 * _loc_5 + _loc_6 * _loc_6 <= _loc_4 * _loc_4)
                        {
                            if (this.starbaselocation[_loc_7][0].substr(0, 2) == "SB")
                            {
                            }
                            else if (this.starbaselocation[_loc_7][0].substr(0, 2) == "PL")
                            {
                            }
                            if (this.playershipvelocity > this.maxdockingvelocity)
                            {
                            }
                            else if (this.starbaselocation[_loc_7][9] > getTimer())
                            {
                            }
                            else
                            {
                                this.otherplayerdockedon = null;
                                _loc_9 = this.starbaselocation[_loc_7][16];
                                _loc_9 = Number(_loc_9);
                                if (_loc_9 > -1)
                                {
                                    if (_loc_9 == Number(this.playershipstatus[5][2]))
                                    {
                                        this.playershipstatus[4][0] = this.starbaselocation[_loc_7][0];
                                        gotoAndStop("dockedscreen");
                                    }
                                }
                                else
                                {
                                    this.playershipstatus[4][0] = this.starbaselocation[_loc_7][0];
                                    gotoAndStop("dockedscreen");
                                }
                            }
                        }
                    }
                }
                _loc_7 = _loc_7 + 1;
            }
            var _loc_8:* = 0;
            while (_loc_8 < this.playersquadbases.length)
            {
                
                if (this.playersquadbases[_loc_8] != null)
                {
                    if (this.playersquadbases[_loc_8][0] == this.playershipstatus[5][10])
                    {
                        _loc_2 = this.playersquadbases[_loc_8][2];
                        _loc_3 = this.playersquadbases[_loc_8][3];
                        _loc_4 = 100;
                        _loc_5 = _loc_2 - this.shipcoordinatex;
                        _loc_6 = _loc_3 - this.shipcoordinatey;
                        if (_loc_5 * _loc_5 + _loc_6 * _loc_6 <= _loc_4 * _loc_4)
                        {
                            if (this.playershipvelocity > this.maxdockingvelocity)
                            {
                            }
                            else
                            {
                                gotoAndStop("squadBaseDock");
                            }
                        }
                    }
                }
                _loc_8 = _loc_8 + 1;
            }
            if (this.teamdeathmatch == true)
            {
                _loc_8 = 0;
                while (_loc_8 < this.teambases.length)
                {
                    
                    if (this.teambases[_loc_8] != null)
                    {
                        _loc_2 = this.teambases[_loc_8][1];
                        _loc_3 = this.teambases[_loc_8][2];
                        _loc_4 = this.teambases[_loc_8][4];
                        _loc_5 = Number(_loc_2) - Number(this.shipcoordinatex);
                        _loc_6 = Number(_loc_3) - Number(this.shipcoordinatey);
                        if (Math.sqrt(_loc_5 * _loc_5 + _loc_6 * _loc_6) <= _loc_4)
                        {
                            if (_loc_8 != this.playershipstatus[5][2])
                            {
                            }
                            else if (this.playershipvelocity > this.maxdockingvelocity)
                            {
                            }
                            else
                            {
                                gotoAndStop("dockedscreen");
                            }
                            break;
                        }
                    }
                    _loc_8 = _loc_8 + 1;
                }
            }
            return;
        }// end function

        function frame102()
        {
            this.gameOptionsScreen.initialGameSettings = this.gamesetting;
            this.gameOptionsScreen.sectorinformation = this.sectorinformation;
            this.gameOptionsScreen.sectormapitems = this.sectormapitems;
            this.gameOptionsScreen.playershipstatus = this.playershipstatus;
            this.gameOptionsScreen.mysocket = this.mysocket;
            this.gameOptionsScreen.gameSounds = this.gameSounds;
            this.gameOptionsScreen.isplayeraguest = this.isplayeraguest;
            this.gameOptionsScreen.exit_button.gotoAndStop(1);
            this.gameOptionsScreen.exit_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_exitOptionsScreen);
            return;
        }// end function

        public function func_InitializeGameMap()
        {
            this.gameMap.sectorinformation = this.sectorinformation;
            this.gameMap.sectormapitems = this.sectormapitems;
            this.gameMap.starbaselocation = this.starbaselocation;
            this.gameMap.func_initMap();
            return;
        }// end function

        public function func_MoveBackGroundStars()
        {
            var _loc_1:* = undefined;
            _loc_1 = 0;
            while (_loc_1 < this.gamesetting.totalstars)
            {
                
                this.backgroundstar[_loc_1][0] = this.backgroundstar[_loc_1][0] + (-this.shipXmovement) / this.backgroundstar[_loc_1][3];
                this.backgroundstar[_loc_1][1] = this.backgroundstar[_loc_1][1] + (-this.shipYmovement) / this.backgroundstar[_loc_1][3];
                if (this.backgroundstar[_loc_1][0] < -500)
                {
                    this.backgroundstar[_loc_1][0] = 500;
                    this.backgroundstar[_loc_1][1] = Math.random() * 600 - 300;
                }
                if (this.backgroundstar[_loc_1][0] > 500)
                {
                    this.backgroundstar[_loc_1][0] = -500;
                    this.backgroundstar[_loc_1][1] = Math.random() * 600 - 300;
                }
                if (this.backgroundstar[_loc_1][1] > 300)
                {
                    this.backgroundstar[_loc_1][0] = Math.random() * 800 - 400;
                    this.backgroundstar[_loc_1][1] = -300;
                }
                if (this.backgroundstar[_loc_1][1] < -300)
                {
                    this.backgroundstar[_loc_1][0] = Math.random() * 800 - 400;
                    this.backgroundstar[_loc_1][1] = 300;
                }
                this.backgroundstar[_loc_1][2].x = this.backgroundstar[_loc_1][0];
                this.backgroundstar[_loc_1][2].y = this.backgroundstar[_loc_1][1];
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_setToNewDefaultGameKeys()
        {
            this.gamesetting.accelkey = this.gamesetting.newdefault.accelkey;
            this.gamesetting.deaccelkey = this.gamesetting.newdefault.deaccelkey;
            this.gamesetting.turnleftkey = this.gamesetting.newdefault.turnleftkey;
            this.gamesetting.turnrightkey = this.gamesetting.newdefault.turnrightkey;
            this.gamesetting.missilekey = this.gamesetting.newdefault.missilekey;
            this.gamesetting.afterburnerskey = this.gamesetting.newdefault.afterburnerskey;
            this.gamesetting.dockkey = this.gamesetting.newdefault.dockkey;
            this.gamesetting.gunskey = this.gamesetting.newdefault.gunskey;
            this.gamesetting.targeterkey = this.gamesetting.newdefault.targeterkey;
            return;
        }// end function

        public function initServices(param1)
        {
            if (param1 == "MOCHI")
            {
                this.MochiAdds = true;
            }
            return;
        }// end function

        public function LogIntoZone()
        {
            var _loc_1:* = undefined;
            this.currentonlineplayers = new Array();
            
            _loc_1 = "ZONELOGIN`" + this.playershipstatus[3][0] + "`" + this.playershipstatus[3][2] + "`" + this.playershipstatus[5][0] + "`" + this.playershipstatus[5][3] + "`" + this.playershipstatus[5][2] + "`" + this.playershipstatus[5][9] + "`" + this.playershipstatus[5][10] + "`" + this.playershipstatus[3][3] + "~";
            this.mysocket.send(_loc_1);
            return;
        }// end function

        public function func_MissionsInformationUpdate()
        {
            var DisplayedText:*;
            var i:*;
            this.MissionsDispText.visible = false;
            DisplayedText = "";
            i = 0;
            while (i < this.playersCurrentMissions.length)
            {
                
                if (this.playersCurrentMissions[i] != null)
                {
                    DisplayedText = DisplayedText + ("\r" + this.playersCurrentMissions[i].MissionName + "\r");
                    if (this.playersCurrentMissions[i].MissionType == "Kill")
                    {
                        if (this.playersCurrentMissions[i].ShipsLeftToKill > 0)
                        {
                            DisplayedText = DisplayedText + (" Kills Left: " + this.playersCurrentMissions[i].ShipsLeftToKill);
                        }
                        else
                        {
                            DisplayedText = DisplayedText + " Completed";
                        }
                    }
                    else if (this.playersCurrentMissions[i].MissionType == "Recon" || this.playersCurrentMissions[i].MissionType == "Patrol")
                    {
                        if (this.playersCurrentMissions[i].Locations.length > 0)
                        {
                            DisplayedText = DisplayedText + (" Go to " + this.playersCurrentMissions[i].Locations[0].MarkerName);
                        }
                        else
                        {
                            DisplayedText = DisplayedText + " Completed";
                        }
                    }
                    else if (this.playersCurrentMissions[i].MissionType == "Cargo")
                    {
                        DisplayedText = DisplayedText + " In Progress";
                    }
                }
                i = (i + 1);
            }
            try
            {
                if (DisplayedText != "MISSIONS")
                {
                    this.MissionsDispText.visible = true;
                    this.MissionsDispText.text = DisplayedText;
                }
            }
            catch (error:Error)
            {
            }
            return;
        }// end function

        function frame118()
        {
            this.tradeGoodsScreen.tradegoods = this.tradegoods;
            this.tradeGoodsScreen.playershipstatus = this.playershipstatus;
            this.tradeGoodsScreen.mysocket = this.mysocket;
            this.tradeGoodsScreen.shiptype = this.shiptype;
            this.tradeGoodsScreen.gameSounds = this.gameSounds;
            this.tradeGoodsScreen.gamesetting = this.gamesetting;
            this.tradeGoodsScreen.exit_button.gotoAndStop(1);
            this.tradeGoodsScreen.exit_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_exitTradeGoodsScreen);
            return;
        }// end function

        public function func_fireGunSound(param1, param2, param3)
        {
            var _loc_4:* = undefined;
            if (this.gamesetting.CombatSounds)
            {
                if (Math.abs(this.shipcoordinatex - param2) < 800)
                {
                    if (Math.abs(this.shipcoordinatey - param3) < 800)
                    {
                        _loc_4 = getTimer();
                        if (this.guntype[Number(param1)][11] < _loc_4)
                        {
                            this.gamesetting.IngameChannelSound = this.guntype[Number(param1)][9].play();
                            this.gamesetting.IngameChannelSound.soundTransform = this.gamesetting.IngameSoundTransform;
                            this.guntype[Number(param1)][11] = _loc_4 + this.gunSoundDelay;
                        }
                    }
                }
            }
            return;
        }// end function

        public function func_setToOldGameKeys()
        {
            this.gamesetting.accelkey = this.gamesetting.olddefault.accelkey;
            this.gamesetting.deaccelkey = this.gamesetting.olddefault.deaccelkey;
            this.gamesetting.turnleftkey = this.gamesetting.olddefault.turnleftkey;
            this.gamesetting.turnrightkey = this.gamesetting.olddefault.turnrightkey;
            this.gamesetting.missilekey = this.gamesetting.olddefault.missilekey;
            this.gamesetting.afterburnerskey = this.gamesetting.olddefault.afterburnerskey;
            this.gamesetting.dockkey = this.gamesetting.olddefault.dockkey;
            this.gamesetting.gunskey = this.gamesetting.olddefault.gunskey;
            this.gamesetting.targeterkey = this.gamesetting.olddefault.targeterkey;
            return;
        }// end function

        public function func_IncomingSeekerInfo(param1)
        {
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            trace(param1);
            var _loc_2:* = Number(param1[1]);
            var _loc_3:* = Number(param1[7]);
            var _loc_4:* = Number(param1[2]);
            var _loc_5:* = Number(param1[3]);
            var _loc_6:* = param1[8];
            var _loc_7:* = Number(param1[4]);
            var _loc_8:* = Number(param1[5]);
            if (Number(this.playershipstatus[3][0]) == Number(_loc_2))
            {
                _loc_9 = 0;
                while (_loc_9 < this.playershotsfired)
                {
                    
                    if (this.playershotsfired[_loc_9][0] == _loc_3)
                    {
                        break;
                    }
                    _loc_9 = _loc_9 + 1;
                }
            }
            else
            {
                _loc_10 = 0;
                while (_loc_10 < this.othermissilefire.length)
                {
                    
                    if (this.othermissilefire[_loc_10][9] == _loc_3)
                    {
                        if (_loc_2 == this.othermissilefire[_loc_10][0])
                        {
                            this.othermissilefire[_loc_10][15] = _loc_6;
                            break;
                        }
                    }
                    _loc_10 = _loc_10 + 1;
                }
            }
            return;
        }// end function

        public function func_resetFlightKeys()
        {
            this.isupkeypressed = false;
            this.isdownkeypressed = false;
            this.isleftkeypressed = false;
            this.isrightkeypressed = false;
            this.iscontrolkeypressed = false;
            this.isdkeypressed = false;
            this.isshiftkeypressed = false;
            this.isspacekeypressed = false;
            this.afterburnerinuse = false;
            this.spacekeyjustpressed = false;
            this.iscontrolkeypressed = false;
            this.keywaspressed = true;
            return;
        }// end function

        public function badwordfiltering(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            _loc_2 = String(param1.toLowerCase());
            _loc_3 = 0;
            while (_loc_3 < param1.length)
            {
                
                _loc_4 = 0;
                while (_loc_4 < this.badwords.length)
                {
                    
                    if (_loc_2.substr(_loc_3, this.badwords[_loc_4][0].length) == this.badwords[_loc_4][0])
                    {
                        _loc_5 = param1.substr(0, _loc_3);
                        _loc_6 = param1.substr(_loc_3 + this.badwords[_loc_4][0].length);
                        param1 = _loc_5 + this.badwords[_loc_4][1] + _loc_6;
                        break;
                    }
                    _loc_4 = _loc_4 + 1;
                }
                _loc_3 = _loc_3 + 1;
            }
            return param1;
        }// end function

        public function func_RemoveSquadBaseMarker(param1)
        {
            this.gameMap.playersquadbases = this.playersquadbases;
            this.gameMap.func_RemoveSquadBase(param1);
            return;
        }// end function

        public function func_addEnergyCapintocliet(param1)
        {
            var _loc_2:* = this.energycapacitors.length;
            this.energycapacitors[_loc_2] = new Array();
            this.energycapacitors[_loc_2][0] = Math.floor(Number(param1[1]));
            this.energycapacitors[_loc_2][1] = "Level " + (_loc_2 + 1);
            this.energycapacitors[_loc_2][2] = Math.floor(Number(param1[2]));
            return;
        }// end function

        public function func_addgunintocliet(param1)
        {
            var _loc_2:* = int(param1[1]);
            this.guntype[_loc_2] = new Array();
            this.guntype[_loc_2][0] = Number(param1[2]);
            this.guntype[_loc_2][1] = Number(param1[3]) / 1000;
            this.guntype[_loc_2][2] = Number(param1[4]) / 1000;
            this.guntype[_loc_2][3] = Number(param1[5]);
            this.guntype[_loc_2][4] = Number(param1[6]);
            this.guntype[_loc_2][5] = Number(param1[7]);
            this.guntype[_loc_2][6] = param1[8];
            this.guntype[_loc_2][7] = Number(param1[9]);
            this.guntype[_loc_2][8] = Number(param1[10]);
            var _loc_3:* = getDefinitionByName("guntype" + _loc_2 + "sound") as Class;
            this.guntype[_loc_2][9] = new _loc_3;
            this.guntype[_loc_2][10] = getDefinitionByName("guntype" + _loc_2 + "fire") as Class;
            this.guntype[_loc_2][11] = 0;
            return;
        }// end function

        function frame141()
        {
            this.inGameSoundButton.visible = false;
            this.saveplayersgame();
            this.playershipstatus[5][4] = "docking";
            this.mysocket.send("PI`CLEAR`~");
            try
            {
                removeEventListener(Event.ENTER_FRAME, this.func_OnGameFrame);
            }
            catch (error:Error)
            {
                trace("Game Event is not loaded");
            }
            this.squadBaseDockedScreen.guntype = this.guntype;
            this.squadBaseDockedScreen.shieldgenerators = this.shieldgenerators;
            this.squadBaseDockedScreen.playershipstatus = this.playershipstatus;
            this.squadBaseDockedScreen.gameSounds = this.gameSounds;
            this.squadBaseDockedScreen.gamesetting = this.gamesetting;
            this.squadBaseDockedScreen.mysocket = this.mysocket;
            this.squadBaseDockedScreen.squadbaseinfo = this.squadbaseinfo;
            this.squadBaseDockedScreen.exit_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_ExitSquadBaseButton_Click);
            return;
        }// end function

        function frame129()
        {
            this.missions_Screen.playersCurrentMissions = this.playersCurrentMissions;
            this.missions_Screen.missionsavailable = this.missionsavailable;
            this.missions_Screen.gamesetting = this.gamesetting;
            this.missions_Screen.playershipstatus = this.playershipstatus;
            this.missions_Screen.mysocket = this.mysocket;
            this.missions_Screen.gameSounds = this.gameSounds;
            this.missions_Screen.exit_button.gotoAndStop(1);
            this.missions_Screen.exit_button.addEventListener(MouseEvent.MOUSE_DOWN, this.missions_ScreenExit);
            return;
        }// end function

        public function func_loadSquadBasesTypes(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            _loc_2 = param1;
            _loc_3 = 1;
            this.squadbaseinfo = new Array();
            while (_loc_3 < (_loc_2.length - 1))
            {
                
                _loc_4 = _loc_2[_loc_3].split("`");
                _loc_5 = Number(_loc_4[0]);
                this.squadbaseinfo[_loc_5] = new Object();
                this.squadbaseinfo[_loc_5].MaxStructure = Number(_loc_4[1]);
                this.squadbaseinfo[_loc_5].MaxShieldGen = Number(_loc_4[2]);
                this.squadbaseinfo[_loc_5].MaxGun = Number(_loc_4[3]);
                this.squadbaseinfo[_loc_5].ShieldGenAlteration = 1;
                this.squadbaseinfo[_loc_5].upgradeToCost = Number(_loc_4[7]);
                this.squadbaseinfo[_loc_5].HourlyIncome = Number(_loc_4[8]);
                _loc_3 = _loc_3 + 1;
            }
            return;
        }// end function

        public function func_RefocusToStage()
        {
            stage.focus = stage;
            return;
        }// end function

        function frame155()
        {
            this.chatDisplay.visible = false;
            this.gameError.reloadButton.visible = false;
            this.gameError.reloadButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_GameErrorReload);
            this.func_DisplayError(this.gameerror);
            stop();
            return;
        }// end function

        public function credDisplayTimerHandler(event:TimerEvent) : void
        {
            var event:* = event;
            try
            {
                this.creditBar.func_displayCredits(Math.floor(this.playershipstatus[3][1]));
                if (this.playershipstatus[3][1] < 0)
                {
                    this.playershipstatus[3][1] = Math.abs(this.playershipstatus[3][1]);
                }
                this.scoreDISP.func_displayScore(this.playershipstatus[5][9]);
            }
            catch (error:Error)
            {
            }
            return;
        }// end function

        public function func_GameSettingScreenPressed(event:MouseEvent) : void
        {
            if (this.gameSettingScreen.isFinished == true)
            {
                nextFrame();
            }
            return;
        }// end function

        public function func_checkothercharacters(param1)
        {
            return param1;
        }// end function

        public function hardwareMouseOver(event:MouseEvent) : void
        {
            this.func_setSelectedIconMessage("ShipHardware");
            return;
        }// end function

        public function onConnect(param1) : void
        {
            this.testData.textt.text = "connected";
            trace("Trace : Connection Made");
            this.IsSocketConnected = true;
            this.gameSettingScreen.gotoAndStop(2);
            return;
        }// end function

        public function func_DockOtherShip(param1)
        {
            var currentplayerid:* = param1;
            var jjjj:*;
            var TotalOthers:* = this.otherplayership.length;
            while (jjjj < TotalOthers)
            {
                
                if (currentplayerid == this.otherplayership[jjjj][0])
                {
                    this.otherplayership[jjjj][4] = 0;
                    this.otherplayership[jjjj][5][0] = 0;
                    this.func_RemoveRadarDot(this.otherplayership[jjjj][59]);
                    try
                    {
                        this.gamedisplayarea.removeChild(this.otherplayership[jjjj][21]);
                    }
                    catch (error:Error)
                    {
                        trace("Error: " + error);
                    }
                    this.otherplayership[jjjj][21] = new this.shipDockingImage() as MovieClip;
                    this.gamedisplayarea.addChild(this.otherplayership[jjjj][21]);
                    this.otherplayership[jjjj][21].x = -999999;
                    this.otherplayership[jjjj][6] = getTimer() + 2000;
                    this.otherplayership[jjjj][15] = "docking";
                    break;
                }
                jjjj = (jjjj + 1);
            }
            return;
        }// end function

        public function func_InGameSound_Click(event:MouseEvent) : void
        {
            if (!this.gamesetting.soundvolume)
            {
                this.gamesetting.soundvolume = true;
                this.gamesetting.gameMusic = true;
            }
            else
            {
                this.gamesetting.soundvolume = false;
                this.gamesetting.gameMusic = false;
                this.gamesetting.CombatSounds = false;
            }
            this.func_redrawGameSoundsButton();
            this.func_checkMusicSettings();
            this.func_RefocusToStage();
            return;
        }// end function

        public function loadplayerextraships(param1)
        {
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            var _loc_15:* = undefined;
            var _loc_2:* = param1.split("~");
            var _loc_3:* = 0;
            trace("EXTRA IINFO" + param1);
            trace("1");
            this.extraplayerships = new Array();
            _loc_3 = 0;
            while (_loc_3 < this.maxextraships)
            {
                
                this.extraplayerships[_loc_3] = new Array();
                this.extraplayerships[_loc_3][0] = null;
                _loc_3 = _loc_3 + 1;
            }
            var _loc_4:* = Number(_loc_2[0].substr(2));
            _loc_3 = 0;
            while ((_loc_3 + 1) < (_loc_2.length - 1))
            {
                
                if (_loc_2[(_loc_3 + 1)].substr(0, 2) == "SH")
                {
                    _loc_5 = _loc_2[(_loc_3 + 1)].split("`");
                    if (_loc_5[1].substr(0, 2) == "ST")
                    {
                        _loc_6 = Number(_loc_5[0].substr("2"));
                        this.extraplayerships[_loc_6] = new Array();
                        this.extraplayerships[_loc_6][0] = Number(_loc_5[1].substr("2"));
                        this.extraplayerships[_loc_6][1] = Number(_loc_5[2].substr("2"));
                        this.extraplayerships[_loc_6][2] = Number(_loc_5[3].substr("2"));
                        this.extraplayerships[_loc_6][3] = Number(_loc_5[4].substr("2"));
                        _loc_7 = 5;
                        this.extraplayerships[_loc_6][4] = new Array();
                        _loc_8 = 0;
                        while (_loc_8 < this.shiptype[this.extraplayerships[_loc_6][0]][2].length)
                        {
                            
                            this.extraplayerships[_loc_6][4][_loc_8] = "none";
                            _loc_8 = _loc_8 + 1;
                        }
                        this.extraplayerships[_loc_6][5] = new Array();
                        _loc_9 = 0;
                        this.extraplayerships[_loc_6][5] = new Array();
                        while (_loc_9 < this.shiptype[this.extraplayerships[_loc_6][0]][5].length)
                        {
                            
                            this.extraplayerships[_loc_6][5][_loc_9] = "none";
                            _loc_9 = _loc_9 + 1;
                        }
                        _loc_10 = 0;
                        this.extraplayerships[_loc_6][11] = new Array();
                        this.extraplayerships[_loc_6][11][1] = new Array();
                        _loc_11 = "";
                        while (_loc_7 < _loc_5.length)
                        {
                            
                            if (_loc_5[_loc_7] == null)
                            {
                                break;
                            }
                            if (_loc_5[_loc_7].substr(0, 2) == "HP")
                            {
                                trace("4-" + _loc_7 + "\'" + _loc_5.length);
                                _loc_11 = _loc_5[_loc_7].split("G");
                                _loc_12 = _loc_11[0].substr("2");
                                if (isNaN(_loc_11[1]))
                                {
                                    _loc_11[1] = "none";
                                }
                                else if (Number(_loc_11[1]) < 0)
                                {
                                    _loc_11[1] = "none";
                                }
                                this.extraplayerships[_loc_6][4][_loc_12] = _loc_11[1];
                            }
                            else if (_loc_5[_loc_7].substr(0, 2) == "TT")
                            {
                                _loc_11 = _loc_5[_loc_7].split("G");
                                _loc_9 = _loc_11[0].substr("2");
                                if (isNaN(_loc_11[1]))
                                {
                                    _loc_11[1] = "none";
                                }
                                else if (Number(_loc_11[1]) < 0)
                                {
                                    _loc_11[1] = "none";
                                }
                                this.extraplayerships[_loc_6][5][_loc_9] = _loc_11[1];
                            }
                            else if (_loc_5[_loc_7].substr(0, 2) == "SP")
                            {
                                _loc_13 = _loc_5[_loc_7].split("Q");
                                _loc_14 = Number(_loc_13[0].substr("2"));
                                _loc_15 = Number(_loc_13[1]);
                                this.extraplayerships[_loc_6][11][1][_loc_10] = new Array();
                                this.extraplayerships[_loc_6][11][1][_loc_10][0] = _loc_14;
                                this.extraplayerships[_loc_6][11][1][_loc_10][1] = _loc_15;
                                _loc_10 = _loc_10 + 1;
                            }
                            _loc_7 = _loc_7 + 1;
                        }
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            if (isNaN(Number(_loc_4)))
            {
                _loc_4 = 0;
            }
            if (Number(_loc_4) >= this.maxextraships)
            {
                _loc_4 = 0;
            }
            if (this.extraplayerships[_loc_4][0] == null)
            {
                _loc_3 = 0;
                while (_loc_3 < this.maxextraships)
                {
                    
                    if (this.extraplayerships[_loc_3][0] != null)
                    {
                        _loc_4 = _loc_3;
                        break;
                    }
                    _loc_3 = _loc_3 + 1;
                }
            }
            this.changetonewship(_loc_4);
            return;
        }// end function

        public function hangarMouseOver(event:MouseEvent) : void
        {
            this.func_setSelectedIconMessage("hangar");
            return;
        }// end function

        public function func_RefreshOtherShipsImages()
        {
            var _loc_1:* = 0;
            while (_loc_1 < this.otherplayership.length)
            {
                
                if (this.playershipstatus[3][0] != this.otherplayership[_loc_1][0])
                {
                    if (this.otherplayership[_loc_1][36] > 1)
                    {
                        if (this.otherplayership[_loc_1][0] >= 0)
                        {
                            trace(this.otherplayership[_loc_1][36] + "~" + (this.otherplayership[_loc_1][37] - this.otherplayership[_loc_1][1]));
                        }
                        this.otherplayership[_loc_1][21].x = this.otherplayership[_loc_1][37] - this.shipcoordinatex;
                        this.otherplayership[_loc_1][21].y = this.otherplayership[_loc_1][38] - this.shipcoordinatey;
                    }
                    else
                    {
                        this.otherplayership[_loc_1][21].x = this.otherplayership[_loc_1][1] - this.shipcoordinatex;
                        this.otherplayership[_loc_1][21].y = this.otherplayership[_loc_1][2] - this.shipcoordinatey;
                    }
                    this.otherplayership[_loc_1][21].rotation = Math.round(this.otherplayership[_loc_1][3]);
                    if (this.otherplayership[_loc_1][52] != null)
                    {
                        this.otherplayership[_loc_1][52].rotation = -this.otherplayership[_loc_1][21].rotation;
                    }
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function RemoveSquadBaseToGameBackGround(param1)
        {
            var ImageLink:* = param1;
            try
            {
                this.gamebackground.removeChild(ImageLink);
            }
            catch (error:Error)
            {
                trace("Error removing squadbase: ");
            }
            return;
        }// end function

        public function func_removePlayersShotFromGlobalID(param1)
        {
            var _loc_2:* = 0;
            while (_loc_2 < this.playershotsfired.length)
            {
                
                if (param1 == this.playershotsfired[_loc_2][0])
                {
                    this.func_removePlayersShot(_loc_2);
                    break;
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_AddChatter(param1)
        {
            this.chatDisplay.func_addMiscChatter(param1);
            return;
        }// end function

        public function func_SendChat_Click(event:MouseEvent) : void
        {
            this.func_SubmitChat();
            stage.focus = stage;
            return;
        }// end function

        public function func_fire_a_gunshot(param1, param2, param3, param4, param5, param6, param7, param8)
        {
            var _loc_9:* = this.playershotsfired.length;
            if (this.playershotsfired.length < 1)
            {
                this.playershotsfired = new Array();
                _loc_9 = 0;
            }
            if (this.currentplayershotsfired > 998)
            {
                this.currentplayershotsfired = 0;
            }
            var _loc_10:* = this.currentplayershotsfired;
            var _loc_13:* = this;
            var _loc_14:* = this.currentplayershotsfired + 1;
            _loc_13.currentplayershotsfired = _loc_14;
            this.playershotsfired[_loc_9] = new Array();
            this.playershotsfired[_loc_9][0] = _loc_10;
            this.playershotsfired[_loc_9][1] = Math.round(param2);
            this.playershotsfired[_loc_9][2] = Math.round(param3);
            this.playershotsfired[_loc_9][3] = Math.round(param4);
            this.playershotsfired[_loc_9][4] = Math.round(param5);
            this.playershotsfired[_loc_9][5] = param8 + this.guntype[param1][1] * 1000;
            this.playershotsfired[_loc_9][6] = param1;
            this.playershotsfired[_loc_9][7] = param6;
            this.playershotsfired[_loc_9][8] = param8 + this.guntype[param1][2] * 1000;
            this.playershotsfired[_loc_9][9] = new this.guntype[param1][10] as MovieClip;
            this.gamedisplayarea.addChild(this.playershotsfired[_loc_9][9]);
            this.playershotsfired[_loc_9][9].x = this.playershotsfired[_loc_9][1] - this.shipcoordinatex;
            this.playershotsfired[_loc_9][9].y = this.playershotsfired[_loc_9][2] - this.shipcoordinatey;
            this.playershotsfired[_loc_9][9].rotation = param6;
            this.playershotsfired[_loc_9][10] = true;
            this.playershotsfired[_loc_9][13] = "GUNS";
            var _loc_11:* = this.func_globalTimeStamp(param8);
            var _loc_12:* = "GF" + "`" + this.playershipstatus[3][0] + "`" + Math.round(param2) + "`" + Math.round(param3) + "`" + Math.round(param2 + param4 * this.shipositiondelay * 0.001) + "`" + Math.round(param3 + param5 * this.shipositiondelay * 0.001) + "`" + Math.round(param7) + "`" + Math.round(param6) + "`" + this.playershotsfired[_loc_9][6] + "`" + _loc_10 + "`" + _loc_11 + "~";
            return "GF" + "`" + this.playershipstatus[3][0] + "`" + Math.round(param2) + "`" + Math.round(param3) + "`" + Math.round(param2 + param4 * this.shipositiondelay * 0.001) + "`" + Math.round(param3 + param5 * this.shipositiondelay * 0.001) + "`" + Math.round(param7) + "`" + Math.round(param6) + "`" + this.playershotsfired[_loc_9][6] + "`" + _loc_10 + "`" + _loc_11 + "~";
        }// end function

        public function func_initalizeStatDisplay()
        {
            var _loc_1:* = this.playershipstatus[1][0];
            this.energyrechargerate = this.energygenerators[_loc_1][0];
            var _loc_2:* = this.playershipstatus[1][5];
            this.maxenergy = this.energycapacitors[_loc_2][0];
            var _loc_3:* = this.playershipstatus[2][0];
            this.energydrainedbyshieldgenatfull = this.shieldgenerators[_loc_3][3];
            this.shieldrechargerate = this.shieldgenerators[_loc_3][1];
            this.maxshieldstrength = this.shieldgenerators[_loc_3][0];
            this.baseshieldgendrain = this.shieldgenerators[_loc_3][2];
            this.isplayeremp = false;
            this.playerempend = 0;
            this.PlayerStatDisp.energy = this.maxenergy;
            this.PlayerStatDisp.maxenergy = this.maxenergy;
            this.PlayerStatDisp.shield = this.shieldgenerators[this.playershipstatus[2][0]][0];
            this.PlayerStatDisp.maxshieldstrength = this.maxshieldstrength;
            this.PlayerStatDisp.structure = this.shiptype[this.playershipstatus[5][0]][3][3];
            this.PlayerStatDisp.playersmaxstructure = this.shiptype[this.playershipstatus[5][0]][3][3];
            this.PlayerStatDisp.maxvelocity = this.playershipmaxvelocity;
            return;
        }// end function

        public function func_StealthTheRadar()
        {
            this.gameRadar.gotoAndStop(2);
            return;
        }// end function

        public function missions_ScreenExit(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            gotoAndStop("dockedscreen");
            return;
        }// end function

        public function myLineHittest(param1, param2, param3, param4, param5, param6, param7)
        {
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            var _loc_15:* = undefined;
            var _loc_16:* = undefined;
            param3 = param3 * 1.2;
            var _loc_8:* = 180 - Math.atan2(param4 - param6, param5 - param7) / (Math.PI / 180);
            var _loc_9:* = param4 - param1;
            var _loc_10:* = param5 - param2;
            var _loc_11:* = 180 - Math.atan2(_loc_9, _loc_10) / (Math.PI / 180);
            var _loc_12:* = 180 - Math.atan2(_loc_9, _loc_10) / (Math.PI / 180) - _loc_8;
            if (180 - Math.atan2(_loc_9, _loc_10) / (Math.PI / 180) - _loc_8 < -180)
            {
                _loc_12 = _loc_12 + 360;
            }
            else if (_loc_12 > 180)
            {
                _loc_12 = _loc_12 - 360;
            }
            if (Math.abs(_loc_12) < 90)
            {
                _loc_13 = Math.sqrt((param4 - param6) * (param4 - param6) + (param5 - param7) * (param5 - param7));
                _loc_14 = Math.sqrt(_loc_9 * _loc_9 + _loc_10 * _loc_10);
                _loc_15 = Math.abs(Math.cos(_loc_12 * Math.PI / 180) * _loc_14);
                if (_loc_15 > _loc_13)
                {
                    if (Math.sqrt((param6 - param1) * (param6 - param1) + (param7 - param2) * (param7 - param2)) < param3)
                    {
                        return true;
                    }
                    return false;
                }
                else
                {
                    _loc_16 = Math.abs(Math.sin(_loc_12 * Math.PI / 180) * _loc_14);
                    if (_loc_16 < param3)
                    {
                        return true;
                    }
                    return false;
                }
            }
            else
            {
                if (Math.sqrt((param4 - param1) * (param4 - param1) + (param5 - param2) * (param5 - param2)) < param3)
                {
                    return true;
                }
                return false;
            }
        }// end function

        public function func_ProcessClockTimer(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            if (this.ClockCheckTimesReceived.length <= this.ClockCheckstodo)
            {
                _loc_2 = Number(param1[1]);
                this.ClockCheckTimesReceived[this.ClockCheckTimesReceived.length] = _loc_2;
                this.loginmovie.mov_login.logindisplay.currentstatus.text = this.loginmovie.mov_login.logindisplay.currentstatus.text + (this.ClockCheckstodo - this.ClockCheckTimesReceived.length + ".");
                if (this.ClockCheckTimesReceived.length == this.ClockCheckstodo)
                {
                    removeEventListener(Event.ENTER_FRAME, this.func_ClockSynchronizeScript);
                    this.loginmovie.mov_login.logindisplay.currentstatus.text = this.loginmovie.mov_login.logindisplay.currentstatus.text + "\r";
                    _loc_3 = 0;
                    _loc_4 = 0;
                    while (_loc_4 < this.ClockCheckstodo)
                    {
                        
                        _loc_3 = _loc_3 + this.ClockCheckTimesReceived[_loc_4];
                        _loc_4 = _loc_4 + 1;
                    }
                    this.clocktimediff = Math.round(_loc_3 / this.ClockCheckstodo);
                    this.loginmovie.mov_login.logindisplay.currentstatus.text = this.loginmovie.mov_login.logindisplay.currentstatus.text + "\r\rEntering Zone";
                    this.LogIntoZone();
                }
                else
                {
                    this.timetillnexClocktcheck = getTimer() + 200;
                }
            }
            return;
        }// end function

        public function func_turnoffstealth()
        {
            var _loc_1:* = undefined;
            _loc_1 = this.playerSpecialsSettings.StealthLocation;
            if (this.playershipstatus[5][15].charAt(0) == "S")
            {
                this.playershipstatus[5][15] = "";
                this.func_NormalRadar();
                this.playershipstatus[11][2][(_loc_1 - 1)][0] = 0;
                this.specialsingame["sp" + _loc_1].specialbutton.gotoAndStop("OFF");
                this.playerSpecialsSettings.isStealthed = false;
            }
            return;
        }// end function

        public function func_MoveBackground()
        {
            this.gamedisplayarea.backgroundImage.x = (this.shipcoordinatex - this.HalfBackgroundWidth) / -30;
            this.gamedisplayarea.backgroundImage.y = (this.shipcoordinatey - this.HalfBackgroundMaxHeight) / -30;
            return;
        }// end function

        public function func_GameErrorReload(event:MouseEvent) : void
        {
            gotoAndPlay(1);
            return;
        }// end function

        public function func_addTradeGoodIntoClient(param1)
        {
            var _loc_2:* = Number(param1[1]);
            this.tradegoods[_loc_2] = new Object();
            this.tradegoods[_loc_2].GoodsName = param1[2];
            this.tradegoods[_loc_2].BaseBuyingFor = param1[3];
            this.tradegoods[_loc_2].BaseSellingFor = param1[4];
            return;
        }// end function

        public function setSquadBaseImage(param1)
        {
            var _loc_2:* = undefined;
            _loc_2 = 0;
            while (_loc_2 < this.playersquadbases.length)
            {
                
                if (this.playersquadbases[_loc_2] != null)
                {
                    if (this.playersquadbases[_loc_2][0] == param1)
                    {
                        this.playersquadbases[_loc_2][12].gotoAndStop("type" + this.playersquadbases[_loc_2][1]);
                        this.playersquadbases[_loc_2][12].x = this.playersquadbases[_loc_2][2];
                        this.playersquadbases[_loc_2][12].y = this.playersquadbases[_loc_2][3];
                        this.func_setSquadbaseLabel(_loc_2);
                        this.playersquadbases[_loc_2][20] = this.squadbaseinfo[this.playersquadbases[_loc_2][1]].MaxStructure;
                        this.playersquadbases[_loc_2][14].y = this.playersquadbases[_loc_2][12].baseiddisp.y + (-this.playersquadbases[_loc_2][12].baseiddisp.height);
                    }
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function onSocketError(event:IOErrorEvent) : void
        {
            trace("Socket ioErrorHandler: " + event);
            this.gameerror = "failedtologin";
            gotoAndStop("gameclose");
            return;
        }// end function

        public function func_reportSavedProgress(param1)
        {
            var ProgressText:* = param1;
            try
            {
                this.savingGameTextBox.text = ProgressText;
            }
            catch (error:Error)
            {
            }
            return;
        }// end function

        public function saveplayersgame()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            this.func_reportSavedProgress("");
            if (this.savestatus == "Save Game")
            {
                _loc_1 = "";
                _loc_1 = _loc_1 + ("PI" + this.playershipstatus[3][0]);
                _loc_1 = _loc_1 + ("`ST" + this.playershipstatus[5][0]);
                _loc_1 = _loc_1 + ("`SG" + this.playershipstatus[2][0]);
                _loc_1 = _loc_1 + ("`EG" + this.playershipstatus[1][0]);
                _loc_1 = _loc_1 + ("`EC" + this.playershipstatus[1][5]);
                var _loc_10:* = _loc_1 + ("`CR" + this.playershipstatus[3][1]);
                _loc_1 = _loc_1 + ("`CR" + this.playershipstatus[3][1]);
                _loc_1 = _loc_1 + _loc_10;
                _loc_1 = _loc_1 + ("`SE" + this.playershipstatus[5][1]);
                _loc_1 = _loc_1 + ("`" + this.playershipstatus[4][0]);
                _loc_2 = 0;
                while (_loc_2 < this.playershipstatus[0].length)
                {
                    
                    _loc_1 = _loc_1 + ("`HP" + _loc_2 + "G");
                    _loc_1 = _loc_1 + this.playershipstatus[0][_loc_2][0];
                    _loc_2 = _loc_2 + 1;
                }
                _loc_3 = 0;
                while (_loc_3 < this.playershipstatus[8].length)
                {
                    
                    _loc_1 = _loc_1 + ("`TT" + _loc_3 + "G");
                    _loc_1 = _loc_1 + this.playershipstatus[8][_loc_3][0];
                    _loc_3 = _loc_3 + 1;
                }
                _loc_4 = 0;
                while (_loc_4 < this.playershipstatus[4][1].length)
                {
                    
                    if (this.playershipstatus[4][1][_loc_4] > 0)
                    {
                        _loc_1 = _loc_1 + ("`CO" + _loc_4 + "A");
                        _loc_1 = _loc_1 + this.playershipstatus[4][1][_loc_4];
                    }
                    _loc_4 = _loc_4 + 1;
                }
                _loc_5 = 0;
                while (_loc_5 < this.playershipstatus[11][1].length)
                {
                    
                    _loc_1 = _loc_1 + ("`SP" + this.playershipstatus[11][1][_loc_5][0] + "Q");
                    _loc_1 = _loc_1 + this.playershipstatus[11][1][_loc_5][1];
                    _loc_5 = _loc_5 + 1;
                }
                _loc_6 = 0;
                while (_loc_6 < this.playershipstatus[7].length)
                {
                    
                    _loc_7 = 0;
                    while (_loc_7 < this.playershipstatus[7][_loc_6][4].length)
                    {
                        
                        if (this.playershipstatus[7][_loc_6][4][_loc_7] > 0)
                        {
                            _loc_1 = _loc_1 + ("`MB" + _loc_6 + "T" + _loc_7 + "Q" + this.playershipstatus[7][_loc_6][4][_loc_7]);
                        }
                        _loc_7 = _loc_7 + 1;
                    }
                    _loc_6 = _loc_6 + 1;
                }
                _loc_1 = _loc_1 + "`~";
                if (this.isplayeraguest == false)
                {
                    _loc_8 = this.saveplayerextraships();
                    if (this.lastplayerssavedinfosent == "info=" + _loc_1 + "&score=" + this.playershipstatus[5][9] + "&shipsdata=" + _loc_8)
                    {
                        this.savestatus = "Save Game";
                    }
                    else
                    {
                        this.lastplayerssavedinfosent = "info=" + _loc_1 + "&score=" + this.playershipstatus[5][9] + "&shipsdata=" + _loc_8;
                        _loc_9 = "SAVEG`~:" + this.playershipstatus[3][2] + ":" + this.playershipstatus[3][3] + ":" + _loc_1 + ":" + this.playershipstatus[5][9] + ":" + _loc_8 + ":" + this.playershipstatus[3][1] + ":" + this.playershipstatus[5][19] + ":";
                        this.mysocket.send(_loc_9);
                        if (this.isgamerunningfromremote == false)
                        {
                        }
                    }
                }
            }
            return;
        }// end function

        public function func_AddRadarDot(param1, param2)
        {
            if (param2 == "BASE")
            {
                param1.gotoAndStop("basedot");
            }
            else if (param2 == "NP")
            {
                param1.gotoAndStop("navpoint");
            }
            else if (param2 == "HOSTILE")
            {
                param1.gotoAndStop("enemydot");
            }
            else if (param2 == "FREIND")
            {
                param1.gotoAndStop("friendlydot");
            }
            this.gameRadar.radarScreen.addChild(param1);
            return;
        }// end function

        public function func_turnofshipcloak()
        {
            var _loc_1:* = undefined;
            if (this.playershipstatus[5][15].charAt(0) == "C")
            {
                _loc_1 = this.playerSpecialsSettings.CloakLocation;
                this.playerSpecialsSettings.isCloaked = false;
                this.playershipstatus[5][15] = "";
                this.keywaspressed = true;
                this.PlayersShipImage.alpha = 1;
                this.playershipstatus[11][2][(_loc_1 - 1)][0] = getTimer() + 5000;
                this.specialsingame["sp" + _loc_1].specialbutton.gotoAndStop("RELOAD");
                this.specialsingame["sp" + _loc_1].specialinfodata.text = "";
                this.func_NormalRadar();
            }
            return;
        }// end function

        public function loopGameMusic(event:Event)
        {
            //if (this.mySndCh != null)
            //{
            //    this.mySndCh.removeEventListener(Event.SOUND_COMPLETE, this.loopGameMusic);
            //    this.playGameMusic();
            //}
            return;
        }// end function

        public function func_ProcessGunHit(param1)
        {
            var currentnumber:*;
            var basethatgothit:*;
            var shotdamage:*;
            var cc:*;
            var gunshottype:*;
            var meteor:*;
            var aiwhogothit:*;
            var qz:*;
            var zzzz:*;
            var currentthread:* = param1;
            var playerwhogothit:* = currentthread[1];
            var shooterofbullet:* = currentthread[2];
            var shooternumberfire:* = Number(currentthread[3]);
            if (playerwhogothit == "SQ")
            {
                basethatgothit = String(currentthread[4]);
                shotdamage = 0;
                if (this.playershipstatus[3][0] != shooterofbullet)
                {
                    cc = 0;
                    while (cc < this.othergunfire.length)
                    {
                        
                        if (this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing otherplayershot - gunhit: " + error);
                            }
                            gunshottype = this.othergunfire[cc][7];
                            shotdamage = this.guntype[gunshottype][4];
                            this.othergunfire.splice(cc, 1);
                            cc = 0;
                        }
                        cc = (cc + 1);
                    }
                }
            }
            else if (playerwhogothit >= 4000)
            {
                if (playerwhogothit < 5000)
                {
                    basethatgothit = playerwhogothit - 4000;
                    basethatgothit = this.teambases[basethatgothit][0];
                    shotdamage = 0;
                    if (this.playershipstatus[3][0] != shooterofbullet)
                    {
                        cc = 0;
                        while (cc < this.othergunfire.length)
                        {
                            
                            if (this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                            {
                                try
                                {
                                    this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                                }
                                catch (error:Error)
                                {
                                    trace("Error removing otherplayershot - basehit: " + error);
                                }
                                this.othergunfire.splice(cc, 1);
                                break;
                            }
                            cc = (cc + 1);
                        }
                    }
                    else if (this.playershipstatus[3][0] == shooterofbullet)
                    {
                        currentnumber = 0;
                        while (currentnumber < this.playershotsfired.length)
                        {
                            
                            if (this.playershotsfired[currentnumber][0] == shooternumberfire)
                            {
                                try
                                {
                                    this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                                }
                                catch (error:Error)
                                {
                                    trace("Error removing playershot - gunhit: " + error);
                                }
                                this.playershotsfired.splice(currentnumber, 1);
                                break;
                            }
                            currentnumber = (currentnumber + 1);
                        }
                    }
                }
                else if (playerwhogothit < 6000)
                {
                    if (this.playershipstatus[3][0] != shooterofbullet)
                    {
                        cc = 0;
                        while (cc < this.othergunfire.length)
                        {
                            
                            if (this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                            {
                                try
                                {
                                    this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                                }
                                catch (error:Error)
                                {
                                    trace("Error removing otherplayershot - basehit: " + error);
                                }
                                this.othergunfire.splice(cc, 1);
                                break;
                            }
                            cc = (cc + 1);
                        }
                    }
                    else if (this.playershipstatus[3][0] == shooterofbullet)
                    {
                        currentnumber = 0;
                        while (currentnumber < this.playershotsfired.length)
                        {
                            
                            if (this.playershotsfired[currentnumber][0] == shooternumberfire)
                            {
                                try
                                {
                                    this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                                }
                                catch (error:Error)
                                {
                                    trace("Error removing playershot - gunhit: " + error);
                                }
                                this.playershotsfired.splice(currentnumber, 1);
                                break;
                            }
                            currentnumber = (currentnumber + 1);
                        }
                    }
                }
                else if (playerwhogothit < 7000)
                {
                    basethatgothit = playerwhogothit - 7000;
                    shotdamage = 0;
                    if (this.playershipstatus[3][0] != shooterofbullet)
                    {
                        cc = 0;
                        while (cc < this.othergunfire.length)
                        {
                            
                            if (this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                            {
                                try
                                {
                                    this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                                }
                                catch (error:Error)
                                {
                                    trace("Error removing otherplayershot - basehit: " + error);
                                }
                                this.othergunfire.splice(cc, 1);
                                break;
                            }
                            cc = (cc + 1);
                        }
                    }
                    else if (this.playershipstatus[3][0] == shooterofbullet)
                    {
                        currentnumber = 0;
                        while (currentnumber < this.playershotsfired.length)
                        {
                            
                            if (this.playershotsfired[currentnumber][0] == shooternumberfire)
                            {
                                try
                                {
                                    this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                                }
                                catch (error:Error)
                                {
                                    trace("Error removing playershot - gunhit: " + error);
                                }
                                this.playershotsfired.splice(currentnumber, 1);
                                break;
                            }
                            currentnumber = (currentnumber + 1);
                        }
                    }
                }
                else if (playerwhogothit < this.starbaseLevel + 1000)
                {
                    basethatgothit = playerwhogothit - this.starbaseLevel;
                    shotdamage = 0;
                    if (this.playershipstatus[3][0] != shooterofbullet)
                    {
                        cc = 0;
                        while (cc < this.othergunfire.length)
                        {
                            
                            if (this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                            {
                                try
                                {
                                    this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                                }
                                catch (error:Error)
                                {
                                    trace("Error removing otherplayershot - basehit: " + error);
                                }
                                this.othergunfire.splice(cc, 1);
                                break;
                            }
                            cc = (cc + 1);
                        }
                    }
                    else if (this.playershipstatus[3][0] == shooterofbullet)
                    {
                        currentnumber = 0;
                        while (currentnumber < this.playershotsfired.length)
                        {
                            
                            if (this.playershotsfired[currentnumber][0] == shooternumberfire)
                            {
                                try
                                {
                                    this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                                }
                                catch (error:Error)
                                {
                                    trace("Error removing playershot - gunhit: " + error);
                                }
                                this.playershotsfired.splice(currentnumber, 1);
                                break;
                            }
                            currentnumber = (currentnumber + 1);
                        }
                    }
                }
            }
            else if (playerwhogothit == "MET")
            {
                meteor = String(currentthread[4]);
                shotdamage = 0;
                if (this.playershipstatus[3][0] != shooterofbullet)
                {
                    cc = 0;
                    while (cc < this.othergunfire.length)
                    {
                        
                        if (this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing otherplayershot - meteor hit: " + error);
                            }
                            gunshottype = this.othergunfire[cc][7];
                            shotdamage = this.guntype[gunshottype][4];
                            this.othergunfire.splice(cc, 1);
                            cc = 0;
                        }
                        cc = (cc + 1);
                    }
                }
            }
            else
            {
                if (playerwhogothit == "AI")
                {
                    var _loc_3:* = currentthread[2];
                    aiwhogothit = currentthread[2];
                    playerwhogothit = _loc_3;
                    shooterofbullet = currentthread[3];
                    shooternumberfire = Number(currentthread[4]);
                }
                else
                {
                    qz = 0;
                    while (qz < this.otherplayership.length)
                    {
                        
                        if (playerwhogothit == this.otherplayership[qz][0])
                        {
                            this.otherplayership[qz][57].gotoAndPlay(1);
                            if (this.otherplayership[qz][21].alpha < 0.9)
                            {
                                this.otherplayership[qz][21].alpha = 0.9;
                            }
                            break;
                        }
                        qz = (qz + 1);
                    }
                }
                if (this.playershipstatus[3][0] != shooterofbullet)
                {
                    cc = 0;
                    while (cc < this.othergunfire.length)
                    {
                        
                        if (this.othergunfire[cc][0] == shooterofbullet && this.othergunfire[cc][9] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.othergunfire[cc][16]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing otherplayershot - meteor AIhit: " + error);
                            }
                            this.othergunfire.splice(cc, 1);
                            cc = 0;
                        }
                        cc = (cc + 1);
                    }
                }
                else if (this.playershipstatus[3][0] == shooterofbullet)
                {
                    zzzz = 0;
                    while (zzzz < this.playershotsfired.length)
                    {
                        
                        if (this.playershotsfired[zzzz][0] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.playershotsfired[zzzz][9]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing playershot - AI hit: " + error);
                            }
                            this.playershotsfired.splice(zzzz, 1);
                            break;
                        }
                        zzzz = (zzzz + 1);
                    }
                }
            }
            return;
        }// end function

        public function func_OnlinePlayersMinimize_Click(event:MouseEvent) : void
        {
            this.fund_OnlineListToggled();
            return;
        }// end function

        public function func_addshieldgenintocliet(param1)
        {
            var _loc_2:* = this.shieldgenerators.length;
            this.shieldgenerators[_loc_2] = new Array();
            this.shieldgenerators[_loc_2][0] = Number(param1[1]);
            this.shieldgenerators[_loc_2][1] = Number(param1[2]);
            this.shieldgenerators[_loc_2][2] = Number(param1[3]);
            this.shieldgenerators[_loc_2][3] = Number(param1[4]);
            this.shieldgenerators[_loc_2][4] = "Level " + (_loc_2 + 1);
            this.shieldgenerators[_loc_2][5] = Number(param1[5]);
            return;
        }// end function

        public function func_LatencyTimeUpd(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            this.latencyTest[this.latencyTest.length] = param1;
            if (this.latencyTest.length > this.totalLatChecks)
            {
                this.latencyTest.splice(0, 1);
                _loc_2 = 0;
                _loc_3 = 0;
                _loc_4 = -900;
                _loc_5 = 0;
                _loc_6 = 900;
                _loc_2 = 0;
                while (_loc_2 < this.totalLatChecks)
                {
                    
                    if (this.latencyTest[_loc_2] < -50 && this.latencyTest[_loc_2] > -500)
                    {
                        _loc_3 = _loc_3 + 1;
                        if (this.latencyTest[_loc_2] > _loc_4)
                        {
                            _loc_4 = this.latencyTest[_loc_2];
                        }
                    }
                    else if (this.latencyTest[_loc_2] > 160)
                    {
                        _loc_5 = _loc_5 + 1;
                        if (this.latencyTest[_loc_2] < _loc_6)
                        {
                            _loc_6 = this.latencyTest[_loc_2];
                        }
                    }
                    _loc_2 = _loc_2 + 1;
                }
                if (_loc_3 >= this.NeededLatFails)
                {
                    this.shiplag = this.shiplag + 60;
                    if (this.shiplag > 500)
                    {
                        this.shiplag = 500;
                    }
                    this.latencyTest = new Array();
                    _loc_7 = "Net lag, Game Delay set to: " + this.shiplag + "ms";
                    if (this.shiplag == 500)
                    {
                        _loc_7 = _loc_7 + " (Max Amount)";
                    }
                }
                else if (_loc_5 >= this.totalLatChecks)
                {
                    this.shiplag = this.shiplag - 60;
                    if (this.shiplag < 100)
                    {
                        this.shiplag = 100;
                    }
                    this.latencyTest = new Array();
                }
            }
            return;
        }// end function

        public function func_isplayeronmap()
        {
            var _loc_1:* = false;
            var _loc_2:* = this.playershipstatus[6][0];
            var _loc_3:* = this.playershipstatus[6][1];
            if (_loc_2 >= 0 && _loc_2 <= this.sectorinformation[0][0])
            {
                if (_loc_3 >= 0 && _loc_3 <= this.sectorinformation[0][1])
                {
                    _loc_1 = true;
                }
            }
            return _loc_1;
        }// end function

        public function func_Missilewasfired(param1)
        {
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            param1.splice(4, 2);
            var _loc_2:* = Number(param1[6]);
            if (this.othermissilefire.length < 1)
            {
                this.othermissilefire = new Array();
            }
            var _loc_11:* = this;
            var _loc_12:* = this.currentothermissileshot + 1;
            _loc_11.currentothermissileshot = _loc_12;
            if (this.currentothermissileshot >= 1000)
            {
                this.currentothermissileshot = 1;
            }
            var _loc_3:* = String(getTimer() + this.clocktimediff);
            _loc_3 = Number(_loc_3.substr(_loc_3.length - 4));
            var _loc_4:* = _loc_3 - Number(param1[8]);
            if (_loc_3 - Number(param1[8]) < -1000)
            {
                _loc_4 = _loc_4 + 10000;
            }
            else if (_loc_4 > 10000)
            {
                _loc_4 = _loc_4 - 10000;
            }
            var _loc_5:* = this.othermissilefire.length;
            this.othermissilefire[_loc_5] = new Array();
            this.othermissilefire[_loc_5][0] = param1[1];
            this.othermissilefire[_loc_5][1] = Number(param1[2]);
            this.othermissilefire[_loc_5][2] = Number(param1[3]);
            var _loc_6:* = int(param1[4]);
            this.othermissilefire[_loc_5][6] = int(param1[5]);
            var _loc_7:* = Number(param1[5]);
            var _loc_8:* = this.MovingObjectWithThrust(_loc_7, _loc_6);
            this.othermissilefire[_loc_5][3] = _loc_8[0];
            this.othermissilefire[_loc_5][4] = _loc_8[1];
            this.othermissilefire[_loc_5][1] = this.othermissilefire[_loc_5][1] + this.othermissilefire[_loc_5][3] * _loc_4 * 0.001;
            this.othermissilefire[_loc_5][2] = this.othermissilefire[_loc_5][2] + this.othermissilefire[_loc_5][4] * _loc_4 * 0.001;
            this.othermissilefire[_loc_5][5] = getTimer() + this.missile[int(param1[6])][2];
            this.othermissilefire[_loc_5][7] = int(param1[6]);
            this.othermissilefire[_loc_5][8] = _loc_6;
            this.othermissilefire[_loc_5][9] = int(param1[7]);
            this.othermissilefire[_loc_5][10] = "other";
            this.othermissilefire[_loc_5][14] = false;
            if (this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] != -1)
            {
                _loc_9 = this.othermissilefire[_loc_5][0];
                _loc_10 = 0;
                while (_loc_10 < this.currentonlineplayers.length)
                {
                    
                    if (this.currentonlineplayers[_loc_10][0] == _loc_9)
                    {
                        if (this.currentonlineplayers[_loc_10][4] == this.playershipstatus[5][2])
                        {
                            this.othermissilefire[_loc_5][14] = true;
                        }
                    }
                    _loc_10 = _loc_10 + 1;
                }
            }
            this.othermissilefire[_loc_5][16] = new this.missile[this.othermissilefire[_loc_5][7]][10] as MovieClip;
            this.gamedisplayarea.addChild(this.othermissilefire[_loc_5][16]);
            this.othermissilefire[_loc_5][16].x = this.othermissilefire[_loc_5][1] - this.shipcoordinatex;
            this.othermissilefire[_loc_5][16].y = this.othermissilefire[_loc_5][2] - this.shipcoordinatey;
            this.othermissilefire[_loc_5][16].rotation = this.othermissilefire[_loc_5][6];
            this.othermissilefire[_loc_5][16].gotoAndStop(1);
            this.othermissilefire[_loc_5][11] = this.othermissilefire[_loc_5][16].width / 2;
            this.othermissilefire[_loc_5][12] = this.othermissilefire[_loc_5][16].height / 2;
            this.othermissilefire[_loc_5][18] = Number(param1[2]);
            this.othermissilefire[_loc_5][19] = Number(param1[3]);
            this.func_fireMissileSound(this.othermissilefire[_loc_5][7], this.othermissilefire[_loc_5][1], this.othermissilefire[_loc_5][2]);
            return;
        }// end function

        public function savecurrenttoextraship(param1)
        {
            this.extraplayerships[param1][0] = this.playershipstatus[5][0];
            var _loc_2:* = this.playershipstatus[5][0];
            this.extraplayerships[param1][4] = new Array();
            var _loc_3:* = 0;
            while (_loc_3 < this.shiptype[_loc_2][2].length)
            {
                
                this.extraplayerships[param1][4][_loc_3] = this.playershipstatus[0][_loc_3][0];
                _loc_3 = _loc_3 + 1;
            }
            var _loc_4:* = 0;
            this.extraplayerships[param1][5] = new Array();
            while (_loc_4 < this.shiptype[_loc_2][5].length)
            {
                
                this.extraplayerships[param1][5][_loc_4] = this.playershipstatus[8][_loc_4][0];
                _loc_4 = _loc_4 + 1;
            }
            var _loc_5:* = 0;
            this.extraplayerships[param1][11] = new Array();
            this.extraplayerships[param1][11][1] = new Array();
            while (_loc_5 < this.playershipstatus[11][1].length)
            {
                
                this.extraplayerships[param1][11][1][_loc_5] = new Array();
                this.extraplayerships[param1][11][1][_loc_5][0] = this.playershipstatus[11][1][_loc_5][0];
                this.extraplayerships[param1][11][1][_loc_5][1] = this.playershipstatus[11][1][_loc_5][1];
                _loc_5 = _loc_5 + 1;
            }
            this.extraplayerships[param1][1] = this.playershipstatus[2][0];
            this.extraplayerships[param1][2] = this.playershipstatus[1][0];
            this.extraplayerships[param1][3] = this.playershipstatus[1][5];
            return;
        }// end function

        public function onClose(param1) : void
        {
            this.testData.textt.text = "closed";
            this.IsSocketConnected = false;
            trace("Trace : Connection Close");
            if (this.relogInGame)
            {
                gotoAndPlay(1);
            }
            else
            {
                gotoAndStop("gameclose");
            }
            return;
        }// end function

        public function func_loadSquadBasesData(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            _loc_2 = param1;
            _loc_3 = 1;
            this.playersquadbases = new Array();
            while (_loc_3 < (_loc_2.length - 1))
            {
                
                _loc_4 = _loc_2[_loc_3].split("`");
                this.add_SquadBaseToGame(_loc_4[0], _loc_4[1], _loc_4[2], _loc_4[3], _loc_4[6], _loc_4[11]);
                _loc_3 = _loc_3 + 1;
            }
            trace("Loaded Squad BAses:" + this.playersquadbases.length);
            return;
        }// end function

        public function func_TurretControlScript(param1)
        {
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_2:* = "";
            _loc_3 = 0;
            _loc_4 = 0;
            _loc_5 = 0;
            _loc_6 = 0;
            if (this.playershipstatus[9] == "MANUAL")
            {
                this.turretCrosshairs.x = mouseX;
                this.turretCrosshairs.y = mouseY;
                _loc_4 = mouseX - this.gamedisplayarea.x;
                _loc_5 = mouseY - this.gamedisplayarea.y;
                _loc_6 = Math.sqrt(_loc_4 * _loc_4 + _loc_5 * _loc_5);
                if (_loc_6 < 300)
                {
                    this.turretCrosshairs.visible = true;
                    if (this.TurretMouseDown)
                    {
                        this.turretCrosshairs.gotoAndStop(2);
                        _loc_3 = 180 - Math.atan2(_loc_4, _loc_5) / (Math.PI / 180);
                        this.func_fireTurretsAt(_loc_3, param1);
                    }
                    else
                    {
                        this.turretCrosshairs.gotoAndStop(1);
                    }
                }
                else
                {
                    this.turretCrosshairs.visible = false;
                    this.TurretMouseDown = false;
                }
            }
            if (this.playershipstatus[9] == "AUTO")
            {
                this.func_SelectAutoTarget();
                if (this.targetinfo[4] < 4000)
                {
                    if (this.targetinfo[4] != this.playershipstatus[3][0])
                    {
                        _loc_7 = this.targetinfo[5];
                        if (this.otherplayership.length > _loc_7)
                        {
                            if (this.otherplayership[_loc_7][0] != this.playershipstatus[3][0])
                            {
                                _loc_4 = this.otherplayership[_loc_7][21].x;
                                _loc_5 = this.otherplayership[_loc_7][21].y;
                                _loc_3 = 180 - Math.atan2(_loc_4, _loc_5) / (Math.PI / 180) + Math.random() * 5 - 2.5;
                                this.func_fireTurretsAt(_loc_3, param1);
                            }
                        }
                    }
                }
            }
            return;
        }// end function

        public function func_HangarButton_Click(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.gameTutorial.visible = false;
            gotoAndStop("Hangar");
            return;
        }// end function

        public function add_SquadBaseToGame(param1, param2, param3, param4, param5, param6)
        {
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            if (this.playersquadbases.length < 1)
            {
                this.playersquadbases = new Array();
            }
            _loc_7 = Number(param5) - this.SquadBaseIDAdjust;
            this.playersquadbases[_loc_7] = new Array();
            this.playersquadbases[_loc_7][0] = param1;
            this.playersquadbases[_loc_7][1] = Number(param2);
            this.playersquadbases[_loc_7][2] = Number(param3);
            this.playersquadbases[_loc_7][3] = Number(param4);
            this.playersquadbases[_loc_7][4] = Math.ceil(Number(param3) / this.sectorinformation[1][0]);
            this.playersquadbases[_loc_7][5] = Math.ceil(Number(param4) / this.sectorinformation[1][1]);
            this.playersquadbases[_loc_7][9] = Number(param6);
            this.playersquadbases[_loc_7][10] = false;
            _loc_8 = getDefinitionByName("squadbasetypes") as Class;
            this.playersquadbases[_loc_7][12] = new _loc_8 as MovieClip;
            this.playersquadbases[_loc_7][13] = new this.RadarBlot() as MovieClip;
            this.playersquadbases[_loc_7][14] = new shiphealthbar() as MovieClip;
            this.playersquadbases[_loc_7][12].addChild(this.playersquadbases[_loc_7][14]);
            this.playersquadbases[_loc_7][20] = 1000;
            this.playersquadbases[_loc_7][22] = 1000;
            trace(this.playersquadbases[_loc_7]);
            this.func_drawSquadbaseMarker(param1);
            this.setSquadBaseImage(param1);
            this.addSquadBaseToGameBackGround(this.playersquadbases[_loc_7][12]);
            this.playersquadbases[_loc_7][14].func_setLifeSettings(1, 0);
            return;
        }// end function

        public function func_writeNewstoScreen(param1, param2)
        {
            this.gameNewsBox.timeUntilHide = getTimer() + 7000;
            this.gameNewsBox.DisplayingMessage.text = param2;
            this.gameNewsBox.gotoAndPlay(1);
            this.gameNewsBox.visible = true;
            this.gameNewsBox.NewsPictureWindow.gotoAndPlay(2);
            this.gameNewsBox.NewsPictureWindow.PictureToShow = param1;
            if (this.playershipstatus[5][4] == "alive")
            {
                this.gameNewsBox.textBackGround.alpha = 0.3;
            }
            else
            {
                this.gameNewsBox.textBackGround.alpha = 0.9;
            }
            this.func_playStaticSound();
            return;
        }// end function

        public function func_lifestats(param1)
        {
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_2:* = int(param1[1]);
            var _loc_3:* = Number(param1[2]);
            var _loc_4:* = Number(param1[3]);
            if (_loc_2 >= 4000)
            {
                if (_loc_2 < 5000)
                {
                    _loc_5 = _loc_2 - 4000;
                    this.teambases[_loc_5][10] = _loc_3;
                    this.teambases[_loc_5][11] = _loc_4;
                    this.func_updateTameBaseHealthBars(_loc_5);
                }
                else if (_loc_2 < this.SquadBaseIDAdjust + 1000)
                {
                    _loc_5 = _loc_2 - this.SquadBaseIDAdjust;
                    if (this.playersquadbases[_loc_5] != null)
                    {
                        this.playersquadbases[_loc_5][14].func_setLifeSettings(_loc_3 / this.playersquadbases[_loc_5][20], _loc_4 / this.playersquadbases[_loc_5][22]);
                    }
                }
                else if (_loc_2 < this.starbaseLevel + 1000)
                {
                    _loc_6 = _loc_2 - this.starbaseLevel;
                    trace("running life" + _loc_6);
                    if (this.starbaselocation[_loc_6] != null)
                    {
                        trace("adjusting life");
                        this.starbaselocation[_loc_6][11] = _loc_3;
                        this.starbaselocation[_loc_6][14] = _loc_4;
                        this.starbaselocation[_loc_6][23].func_setLifeSettings(_loc_3 / this.starbaselocation[_loc_6][12], _loc_4 / this.starbaselocation[_loc_6][15]);
                    }
                }
            }
            else
            {
                _loc_7 = this.otherplayership.length;
                _loc_8 = 0;
                while (_loc_8 < _loc_7)
                {
                    
                    if (this.otherplayership[_loc_8][0] == _loc_2)
                    {
                        this.otherplayership[_loc_8][41] = _loc_4;
                        this.otherplayership[_loc_8][40] = _loc_3;
                        break;
                    }
                    _loc_8 = _loc_8 + 1;
                }
            }
            return;
        }// end function

        public function func_refreshCurrentOnlineList()
        {
            this.OnlinePLayerList.visible = true;
            this.DisplayOnlinePlayersList();
            return;
        }// end function

        public function stopGameMusic(event:Event)
        {
            this.mySndCh.removeEventListener(Event.SOUND_COMPLETE, this.loopGameMusic);
            return;
        }// end function

        public function ProcessOtherShipKeys(param1)
        {
            var _loc_3:* = undefined;
            var _loc_2:* = 0;
            while (_loc_2 < this.otherplayership.length)
            {
                
                _loc_3 = 0;
                while (_loc_3 < this.otherplayership[_loc_2][30].length)
                {
                    
                    if (this.otherplayership[_loc_2][30][_loc_3] == null)
                    {
                        break;
                    }
                    else if (this.otherplayership[_loc_2][30][(_loc_3 + 1)] != null && this.otherplayership[_loc_2][30][(_loc_3 + 1)][9] < param1)
                    {
                        this.otherplayership[_loc_2][30].splice(_loc_3, 1);
                        _loc_3 = _loc_3 - 1;
                    }
                    else if (this.otherplayership[_loc_2][30][_loc_3][9] < param1)
                    {
                        this.otherplayership[_loc_2][3] = this.otherplayership[_loc_2][30][_loc_3][2];
                        if (this.otherplayership[_loc_2][36] > 0)
                        {
                            this.otherplayership[_loc_2][36] = 7;
                        }
                        else
                        {
                            this.otherplayership[_loc_2][36] = 7;
                        }
                        this.otherplayership[_loc_2][1] = this.otherplayership[_loc_2][30][_loc_3][0];
                        this.otherplayership[_loc_2][2] = this.otherplayership[_loc_2][30][_loc_3][1];
                        this.otherplayership[_loc_2][4] = this.otherplayership[_loc_2][30][_loc_3][3];
                        this.otherplayership[_loc_2][5] = new Array();
                        this.otherplayership[_loc_2][5][0] = this.otherplayership[_loc_2][30][_loc_3][6][0];
                        this.otherplayership[_loc_2][5][1] = this.otherplayership[_loc_2][30][_loc_3][6][1];
                        this.otherplayership[_loc_2][5][8] = this.otherplayership[_loc_2][30][_loc_3][8];
                        this.otherplayership[_loc_2][5][9] = this.otherplayership[_loc_2][30][_loc_3][9];
                        this.otherplayership[_loc_2][30].splice(_loc_3, 1);
                        _loc_3 = _loc_3 - 1;
                    }
                    else
                    {
                        break;
                    }
                    _loc_3 = _loc_3 + 1;
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_starbaseSetBty(param1)
        {
            var _loc_2:* = Number(param1[2]);
            _loc_2 = _loc_2 - this.starbaseLevel;
            var _loc_3:* = Number(param1[3]);
            if (this.starbaselocation[_loc_2] != null)
            {
                this.starbaselocation[_loc_2][19] = Number(_loc_3);
                this.func_RedrawStarbaseNameTag(_loc_2);
            }
            return;
        }// end function

        public function dataprocess(param1)
        {
            var newinfo:*;
            var timetorunit:*;
            var curroute:*;
            var currentthread:*;
            var currentplayerid:*;
            var sctt:*;
            var scjj:*;
            var tmjj:*;
            var scorejj:*;
            var bountyOfKill:*;
            var fundsworth:*;
            var KillerOfShip:*;
            var jj:*;
            var dataupdated:*;
            var NameOfKiller:*;
            var OnlineLocOfKiller:*;
            var KilledIDNo:*;
            var NameOfDeceased:*;
            var LocOfDeceased:*;
            var outputmessage:*;
            var bcjj:*;
            var Noobdatatosend:*;
            var datatosend:*;
            var incomeFromBase:*;
            var baseidname:*;
            var killersid:*;
            var bountyWorth:*;
            var PlayersNewBty:*;
            var PlayersNewScore:*;
            var SQBfundsworth:*;
            var NameOfBaseKiller:*;
            var abb:*;
            var SQBDoutputmessage:*;
            var fundstoadd:*;
            var myLoadVars:* = param1;
			//trace("Message Recieved : " + myLoadVars);
			
            if (myLoadVars != "")
            {
                if (myLoadVars.substr(0, 4) != "PING")
                {
                }
            }
            newinfo = myLoadVars.split("~");
            timetorunit = (newinfo.length - 1);
            curroute = 0;
            while (curroute < timetorunit)
            {
                
                currentthread = newinfo[curroute].split("`");
				trace("RECV'D: " + myLoadVars);
				//trace("Parsing sector: " + newinfo[curroute]);
				//trace("Parsing subsectior: " + currentthread[0]);
                if (currentthread[0] == "ACCT")
                {
                    if (currentthread[1] == "PILOAD")
                    {
                        this.isplayeraguest = this.loginmovie.mov_login.isAGuest;
                        this.loadplayerdata(myLoadVars);
                        this.loginmovie.mov_login.playershipstatus = this.playershipstatus;
                        this.loginmovie.mov_login.func_loginaccepted();
                        break;
                    }
                    else if (currentthread[1] == "PINOLOAD")
                    {
                        this.loginmovie.mov_login.func_failedLogin();
                    }
                    else if (currentthread[1] == "nameexists")
                    {
                        this.loginmovie.mov_login.Incoming_Creation_Data(currentthread[1]);
                    }
                    else if (currentthread[1] == "namecreated")
                    {
                        this.loginmovie.mov_login.Incoming_Creation_Data(currentthread[1]);
                    }
                    else if (currentthread[1] == "SQUADCREATING")
                    {
                        this.gameOptionsScreen.func_receivedsquadcreation(currentthread[2]);
                    }
                    else if (currentthread[1] == "SQUADJOINED")
                    {
                        this.gameOptionsScreen.func_receivedsquadjoinage(currentthread[2]);
                    }
                    else if (currentthread[1] == "SQUADLEAVE")
                    {
                        this.gameOptionsScreen.func_receivedsquadleaving(currentthread[2]);
                    }
                    else if (currentthread[1] == "SQUADKICKED")
                    {
                        this.playershipstatus[5][10] = "NONE";
                        try
                        {
                            this.gameOptionsScreen.mainSquadDisplay();
                        }
                        catch (error:Error)
                        {
                            trace("Squad Was Disolved while not at squad screen");
                        }
                    }
                    else if (currentthread[1] == "SQUADDESTROYED")
                    {
                        this.playershipstatus[5][10] = "NONE";
                        this.playershipstatus[5][11] = false;
                        this.playershipstatus[5][13] = "";
                        try
                        {
                            this.gameOptionsScreen.mainSquadDisplay();
                        }
                        catch (error:Error)
                        {
                            trace("Squad Was Disolved while not at squad screen");
                        }
                    }
                    else if (currentthread[1] == "SQUADBINFO")
                    {
                        try
                        {
                            this.gameOptionsScreen.func_incomingSquadBaseInfo(currentthread);
                        }
                        catch (error:Error)
                        {
                            trace("base detail info failed");
                        }
                    }
                    else if (currentthread[1] == "SQUADPREFINFO")
                    {
                        try
                        {
                            this.gameOptionsScreen.func_preferences_membersofsquad(newinfo[curroute]);
                        }
                        catch (error:Error)
                        {
                            trace(currentthread);
                            trace("base pref info failed");
                        }
                    }
                }
                else if (currentthread[0] == "JZONE")
                {
                    this.ZonePlayerIsIn = currentthread[1];
                    this.loginmovie.mov_login.func_zoneconnectionbeenmade();
                    this.mysocket.send("LOADZONESYSTEM`~");
                    this.loginmovie.mov_login.logindisplay.currentstatus.text = "Loading System Information";
                }
                else if (currentthread[0] == "PI" && !isNaN(currentthread[1]))
                {
                    this.otherPilotProcess(currentthread);
                }
                else if (currentthread[0] == "GAMEINFO")
                {
					trace("test?")
                    this.func_processgameinfo(myLoadVars);
                }
                else if (currentthread[0] == "MI")
                {
                    this.func_IncomingSeekerInfo(currentthread);
                }
                else if (currentthread[0] == "MET")
                {
                    trace("rec data:" + currentthread);
                    this.func_IncomingMeteorInfo(currentthread);
                }
                else if (currentthread[0] == "MF")
                {
                    trace("MF DATA:" + currentthread);
                    this.func_Missilewasfired(currentthread);
                }
                else if (currentthread[0] == "GF")
                {
                    try
                    {
                        this.func_AddOtherGunshot(currentthread);
                    }
                    catch (error:Error)
                    {
                    }
                }
                else if (currentthread[0] == "GH")
                {
                    this.func_ProcessGunHit(currentthread);
                }
                else if (currentthread[0] == "STATS")
                {
                    this.func_statuscheck(currentthread);
                }
                else if (currentthread[0] == "SH")
                {
                    this.func_bringinspecial(currentthread);
                }
                else if (currentthread[0] == "MH")
                {
                    this.func_processMissileHit(currentthread);
                }
                else if (currentthread[0] == "DM")
                {
                    this.func_funcDeathMatchProcc(currentthread);
                }
                else if (currentthread[0] == "CH")
                {
                    this.func_IncomingChat(currentthread);
                }
                else if (currentthread[0] == "TO")
                {
                    this.func_LogOutPlayer(currentthread[1]);
                }
                else if (currentthread[0] == "TC")
                {
                    currentplayerid = currentthread[1];
                    if (currentthread[2] == "DK")
                    {
                        if (currentplayerid == this.playershipstatus[3][0])
                        {
                        }
                        else
                        {
                            this.func_DockOtherShip(currentplayerid);
                        }
                    }
                    if (currentthread[2] == "SQC")
                    {
                        sctt = 0;
                        while (sctt < this.currentonlineplayers.length)
                        {
                            
                            if (currentplayerid == this.currentonlineplayers[sctt][0])
                            {
                                this.currentonlineplayers[sctt][9] = currentthread[3];
                                this.func_refreshCurrentOnlineList();
                                break;
                            }
                            sctt = (sctt + 1);
                        }
                        if (this.playershipstatus[3][0] == currentplayerid)
                        {
                            this.playershipstatus[5][10] = currentthread[3];
                        }
                    }
                    else if (currentthread[2] == "SC")
                    {
                        currentthread[3] = Number(currentthread[3]);
                        if (isNaN(currentthread[3]))
                        {
                            currentthread[3] = 0;
                        }
                        scjj = 0;
                        while (scjj < this.currentonlineplayers.length)
                        {
                            
                            if (currentplayerid == this.currentonlineplayers[scjj][0])
                            {
                                this.currentonlineplayers[scjj][2] = Number(currentthread[3]);
                                this.currentonlineplayers[scjj][3] = Number(currentthread[4]);
                                break;
                            }
                            scjj = (scjj + 1);
                        }
                        if (Number(this.playershipstatus[3][0]) == currentthread[3])
                        {
                            this.playershipstatus[5][8] = Number(currentthread[4]);
                        }
                    }
                    if (currentthread[2] == "TM")
                    {
                        if (currentthread[3] == "-1")
                        {
                            currentthread[3] = "N/A";
                        }
                        tmjj = 0;
                        while (tmjj < this.currentonlineplayers.length)
                        {
                            
                            if (currentplayerid == this.currentonlineplayers[tmjj][0])
                            {
                                this.currentonlineplayers[tmjj][4] = currentthread[3];
                                break;
                            }
                            tmjj = (tmjj + 1);
                        }
                        if (this.playershipstatus[3][0] == currentthread[1])
                        {
                            this.playershipstatus[5][2] = currentthread[3];
                            this.func_refreshBaseTeamDisplays(this.playershipstatus[5][2]);
                        }
                        this.func_refreshCurrentOnlineList();
                    }
                    else if (currentthread[2] == "SCORE")
                    {
                        currentthread[3] = Number(currentthread[3]);
                        if (isNaN(currentthread[3]) || Number(currentthread[3] < 0))
                        {
                            currentthread[3] = 0;
                        }
                        scorejj = 0;
                        while (scorejj < this.currentonlineplayers.length)
                        {
                            
                            if (currentplayerid == this.currentonlineplayers[scorejj][0])
                            {
                                this.currentonlineplayers[scorejj][5] = Number(this.currentonlineplayers[scorejj][5]) + Number(currentthread[3]);
                                break;
                            }
                            scorejj = (scorejj + 1);
                        }
                        if (this.playershipstatus[3][0] == currentthread[1])
                        {
                            this.playershipstatus[5][9] = Number(this.playershipstatus[5][9]) + Number(currentthread[3]);
                            this.playersSessionScoreStart = this.playersSessionScoreStart + Number(currentthread[3]);
                        }
                        this.func_refreshCurrentOnlineList();
                    }
                    if (currentthread[2] == "SD")
                    {
                        this.func_KillOpponentsShip(currentplayerid);
                        bountyOfKill = Number(currentthread[4]);
                        fundsworth = Math.floor(bountyOfKill * this.extrafundsmultiplier);
                        KillerOfShip = currentthread[3];
                        if (this.playershipstatus[3][0] == KillerOfShip)
                        {
                            this.func_KillMissionsUpdate();
                            this.playershipstatus[3][1] = this.playershipstatus[3][1] + fundsworth;
                            this.playershipstatus[5][8] = Number(currentthread[6]);
                            this.playershipstatus[5][9] = Number(currentthread[7]);
                        }
                        jj = 0;
                        dataupdated = false;
                        NameOfKiller = "";
                        while (jj < this.currentonlineplayers.length)
                        {
                            
                            if (KillerOfShip == String(this.currentonlineplayers[jj][0]))
                            {
                                this.currentonlineplayers[jj][3] = Number(currentthread[6]);
                                this.currentonlineplayers[jj][5] = Number(currentthread[7]);
                                NameOfKiller = this.currentonlineplayers[jj][1];
                                OnlineLocOfKiller = jj;
                                break;
                            }
                            jj = (jj + 1);
                        }
                        jj = 0;
                        dataupdated = true;
                        KilledIDNo = String(currentplayerid);
                        if (this.playershipstatus[3][0] == KilledIDNo)
                        {
                            this.playershipstatus[5][8] = Number(currentthread[5]);
                        }
                        NameOfDeceased = "";
                        while (jj < this.currentonlineplayers.length)
                        {
                            
                            if (KilledIDNo == String(this.currentonlineplayers[jj][0]))
                            {
                                this.currentonlineplayers[jj][3] = Number(currentthread[5]);
                                NameOfDeceased = this.currentonlineplayers[jj][1];
                                LocOfDeceased = jj;
                                break;
                            }
                            jj = (jj + 1);
                        }
                        outputmessage = NameOfDeceased + " (" + bountyOfKill + " BTY, " + fundsworth + " Credits) killed by " + NameOfKiller;
                        if (Number(currentplayerid) >= 0 && Number(currentplayerid) < 1000 && Number(KillerOfShip) >= 0 && Number(KillerOfShip) < 1000)
                        {
                            this.currentonlineplayers[OnlineLocOfKiller][6] = Number(this.currentonlineplayers[OnlineLocOfKiller][6]) + 1;
                            this.currentonlineplayers[LocOfDeceased][7] = Number(this.currentonlineplayers[LocOfDeceased][7]) + 1;
                        }
                        this.func_refreshOtherPlayerDisplayedLabel(KillerOfShip);
                        this.func_refreshCurrentOnlineList();
                        if (Number(currentplayerid) >= 0 && Number(currentplayerid) < 1000 || Number(KillerOfShip) >= 0 && Number(KillerOfShip) < 1000)
                        {
                            this.func_AddChatter(outputmessage);
                        }
                    }
                    else if (currentthread[2] == "BC")
                    {
                        bcjj = 0;
                        while (jj < this.currentonlineplayers.length)
                        {
                            
                            if (currentplayerid == this.currentonlineplayers[bcjj][0])
                            {
                                this.currentonlineplayers[bcjj][3] = Number(currentthread[3]);
                                break;
                            }
                            bcjj = (bcjj + 1);
                        }
                        if (this.playershipstatus[3][0] == currentplayerid)
                        {
                            this.playershipstatus[5][8] = Number(currentthread[3]) - Number(this.playershipstatus[5][3]);
                        }
                        this.func_refreshCurrentOnlineList();
                    }
                }
                else if (currentthread[0] == "LFE")
                {
                    this.func_lifestats(currentthread);
                }
                else if (currentthread[0] == "PING")
                {
                    this.func_PingTimer_lastpingCame(Number(currentthread[1]));
                }
                else if (currentthread[0] == "LISTING")
                {
                    this.loginmovie.mov_login.logindisplay.func_incominglisting(currentthread);
                }
                else if (currentthread[0] == "TI" && currentthread[1] != this.playershipstatus[3][0])
                {
                    this.func_otherPLayerLogin(currentthread);
                }
                else if (currentthread[0] == "LI")
                {
                    this.playershipstatus[3][0] = currentthread[1];
                    this.playershipstatus[5][19] = currentthread[2];
                    if (this.ZonePlayerIsIn == "100")
                    {
                        this.playershipstatus[5][2] = "0";
                        Noobdatatosend = "TC" + "`" + this.playershipstatus[3][0] + "`TM`" + this.playershipstatus[5][2] + "~";
                        this.mysocket.send(Noobdatatosend);
                    }
                    gotoAndStop("dockedscreen");
                    if (Number(this.playershipstatus[5][1]) < 300)
                    {
                        datatosend = "TC" + "`" + this.playershipstatus[3][0] + "`TM`" + this.playershipstatus[5][2] + "~";
                        this.mysocket.send(datatosend);
                    }
                    this.mysocket.send("INITTRADE`LOAD`~");
                    trace("loggedin");
                }
                else if (currentthread[0] == "SGAME")
                {
                    this.func_postsaveresults(currentthread);
                }
                else if (currentthread[0] == "SECTORLOADING")
                {
                    this.func_LoadZonesSystemMap(myLoadVars);
                    this.mysocket.send("SQUADB`TYPES`~");
                }
                else if (currentthread[0] == "TRG")
                {
                    if (currentthread[1] == "MAP")
                    {
                        try
                        {
                            this.gameMap.parseInTradegoods(newinfo[curroute]);
                        }
                        catch (error:Error)
                        {
                            trace("Error: Can\'t Parse Trade Goods into Map");
                        }
                    }
                    else
                    {
                        try
                        {
                            this.tradeGoodsScreen.parseInTradegoods(newinfo[curroute]);
                        }
                        catch (error:Error)
                        {
                            trace("Error: Can\'t Parse Trade Goods into Base");
                        }
                    }
                }
                else if (currentthread[0] == "SQUADBASESLOAD")
                {
                    this.func_loadSquadBasesData(newinfo);
                    this.loginmovie.mov_login.logindisplay.currentstatus.text = this.loginmovie.mov_login.logindisplay.currentstatus.text + "Squad Bases Loaded\r";
                    this.func_InitializeClockChecks();
                }
                else if (currentthread[0] == "SQBTYPES")
                {
                    trace("SQB TYPES:" + newinfo);
                    this.func_loadSquadBasesTypes(newinfo);
                    this.mysocket.send("LOADSYSTEMSQUADBASES`~");
                    this.loginmovie.mov_login.logindisplay.currentstatus.text = this.loginmovie.mov_login.logindisplay.currentstatus.text + "\rLoading Squad Bases\r";
                }
                else if (currentthread[0] == "CLOCK")
                {
                    this.func_ProcessClockTimer(currentthread);
                }
                else if (currentthread[0] == "NEWS")
                {
                    if (currentthread[1] == "MES")
                    {
                        trace(currentthread[2]);
                        this.func_writeNewstoScreen("NEWS", currentthread[2]);
                    }
                }
                else if (currentthread[0] == "SQB")
                {
                    if (currentthread[1] == "SB")
                    {
                        if (currentthread[2] == "DESTROYED")
                        {
                            this.remove_SquadBaseFrom(currentthread[3]);
                        }
                        if (currentthread[2] == "CREATED")
                        {
                            if (this.playersquadbases.length < 1)
                            {
                                this.playersquadbases = new Array();
                            }
                            this.add_SquadBaseToGame(currentthread[3], currentthread[4], currentthread[5], currentthread[6], currentthread[9], currentthread[14]);
                        }
                    }
                    if (currentthread[1] == "STATS")
                    {
                        try
                        {
                            this.squadBaseDockedScreen.parseInBaseStats(currentthread);
                        }
                        catch (error:Error)
                        {
                            trace("Error: Can\'t Parse Squad Base Details");
                        }
                    }
                    if (currentthread[1] == "INCOME")
                    {
                        incomeFromBase = Number(currentthread[2]);
                        if (!isNaN(incomeFromBase))
                        {
                            incomeFromBase = Math.floor(incomeFromBase);
                            if (incomeFromBase > 0)
                            {
                                this.playershipstatus[3][1] = this.playershipstatus[3][1] + incomeFromBase;
                                this.func_enterintochat("HOST: Squadbase Generated " + incomeFromBase + " Credits.", this.squadchattextcolor);
                            }
                        }
                    }
                    if (currentthread[1] == "UPGL")
                    {
                        trace("recieved base upgrades:" + currentthread);
                        this.ChangeSquadBaseLevel(currentthread[2], currentthread[3]);
                    }
                    if (currentthread[1] == "BTY")
                    {
                        this.ChangeSquadBTY(currentthread[2], currentthread[3]);
                    }
                    if (currentthread[1] == "SQUADBD")
                    {
                        baseidname = String(currentthread[2]);
                        killersid = Number(currentthread[3]);
                        bountyWorth = Number(currentthread[4]);
                        PlayersNewBty = Number(currentthread[5]);
                        PlayersNewScore = Number(currentthread[6]);
                        SQBfundsworth = Math.floor(bountyWorth * this.extrafundsmultiplier);
                        if (this.playershipstatus[3][0] == killersid)
                        {
                            this.playershipstatus[3][1] = this.playershipstatus[3][1] + SQBfundsworth;
                            this.playershipstatus[5][8] = PlayersNewBty;
                            this.playershipstatus[5][9] = PlayersNewScore;
                        }
                        NameOfBaseKiller = "";
                        abb = 0;
                        while (abb < this.currentonlineplayers.length)
                        {
                            
                            if (killersid == Number(this.currentonlineplayers[abb][0]))
                            {
                                this.currentonlineplayers[abb][3] = PlayersNewBty;
                                this.currentonlineplayers[abb][5] = PlayersNewScore;
                                NameOfBaseKiller = this.currentonlineplayers[abb][1];
                                trace("updated");
                                break;
                            }
                            abb = (abb + 1);
                        }
                        SQBDoutputmessage = NameOfBaseKiller + " destroyed Squad Base " + baseidname + " (" + bountyWorth + " BTY, " + SQBfundsworth + ")";
                        this.func_refreshOtherPlayerDisplayedLabel(killersid);
                        this.func_refreshCurrentOnlineList();
                        this.func_AddChatter(SQBDoutputmessage);
                        trace("Base Killed" + baseidname);
                    }
                }
                else if (currentthread[0] == "STB")
                {
                    if (currentthread[1] == "DEST")
                    {
                        this.func_StarbaseDestroyed(currentthread);
                    }
                    else if (currentthread[1] == "RELOAD")
                    {
                        currentthread = currentthread.slice(2);
                        trace(currentthread);
                        this.func_StarbaseLoadingData(currentthread);
                        this.func_refreshBaseTeamDisplays(this.playershipstatus[5][2]);
                    }
                    else if (currentthread[1] == "BTY")
                    {
                        this.func_starbaseSetBty(currentthread);
                    }
                }
                else if (currentthread[0] == "ADMIN")
                {
                    if (currentthread[1] == "MUTE")
                    {
                        if (this.gamechatinfo[2][2] == true)
                        {
                            this.gamechatinfo[2][2] = false;
                            this.func_enterintochat("HOST: You Are No Longer Muted", this.systemchattextcolor);
                        }
                        else
                        {
                            this.gamechatinfo[2][2] = true;
                            this.gamechatinfo[2][3] = getTimer() + 1000 * 60 * 15;
                            this.func_enterintochat("HOST: Muted for " + Math.ceil(this.gamechatinfo[2][4] / 60000) + " minuites by " + currentthread[2], this.systemchattextcolor);
                        }
                    }
                    if (currentthread[1] == "TRANSFER")
                    {
                        fundstoadd = Number(currentthread[3]);
                        if (!isNaN(fundstoadd))
                        {
                            this.playershipstatus[3][1] = this.playershipstatus[3][1] + Number(fundstoadd);
                        }
                    }
                }
                else if (currentthread[0] == "ERROR")
                {
                    trace("GOT KICK");
                    if (currentthread[1] == "TOOMANYUSERS")
                    {
                        this.gameerror = "toomanyplayers";
                        this.controlledserverclose = true;
                        gotoAndStop("gameclose");
                    }
                    else if (currentthread[1] == "TOOMANYIPUSERS")
                    {
                        this.gameerror = "toomanyipplayers";
                        this.controlledserverclose = true;
                        gotoAndStop("gameclose");
                    }
                    if (currentthread[1] == "KICKED")
                    {
                        this.controlledserverclose = true;
                        this.gameerror = "kicked";
                        this.gameerrordoneby = currentthread[2];
                        gotoAndStop("gameclose");
                    }
                    if (currentthread[1] == "SHUTDOWN")
                    {
                        this.controlledserverclose = true;
                        this.gameerror = "servershutdown";
                        gotoAndStop("gameclose");
                    }
                    if (currentthread[1] == "NAMEINUSE")
                    {
                        this.controlledserverclose = true;
                        this.gameerror = "nameinuse";
                        gotoAndStop("gameclose");
                    }
                    if (currentthread[1] == "SPAM")
                    {
                        this.controlledserverclose = true;
                        this.gameerror = "FLOODING";
                        gotoAndStop("gameclose");
                    }
                    if (currentthread[1] == "BANNED")
                    {
                        this.controlledserverclose = true;
                        this.gameerror = "banned";
                        this.timebannedfor = currentthread[2];
                        this.gameerrordoneby = currentthread[3];
                        gotoAndStop("gameclose");
                    }
                }
                curroute = (curroute + 1);
            }
            return;
        }// end function

        public function func_ExitBaseButton_Click(event:MouseEvent) : void
        {
            if (1 == 1)
            {
                this.func_playRegularClick();
                this.starbaseexitposition();
                this.playerjustloggedin = false;
                if (this.teamdeathmatch)
                {
                    if (this.squadwarinfo[0] == true && this.squadwarinfo[2] != true)
                    {
                        trace("Squad War is waiting to start");
                    }
                    else if (this.playershipstatus[5][2] != Number(this.playershipstatus[5][2]) && this.squadwarinfo[2] == true)
                    {
                        trace("You are not on one of the squads");
                    }
                    else if (this.playershipstatus[5][2] != Number(this.playershipstatus[5][2]))
                    {
                        trace("Need to Choose / Select a Team First");
                    }
                    else if (this.timePlayerCanExit < getTimer())
                    {
                        if (!isNaN(this.playershipstatus[5][2]) && this.playershipstatus[5][2] >= 0 && this.playershipstatus[5][2] < this.teambases.length)
                        {
                            gotoAndStop("maingameplaying");
                        }
                    }
                    else
                    {
                        trace("Need to wait ");
                    }
                }
                else
                {
                    gotoAndStop("maingameplaying");
                }
            }
            return;
        }// end function

        public function exitMouseOver(event:MouseEvent) : void
        {
            this.func_setSelectedIconMessage("exitDock");
            return;
        }// end function

        public function Missions_Click(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.gameTutorial.visible = false;
            gotoAndStop("Missions");
            return;
        }// end function

        public function func_Special_Selected(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            _loc_2 = param1 - 1;
            _loc_3 = this.playershipstatus[11][1][_loc_2][0];
            _loc_4 = getTimer();
            if (_loc_3 != null)
            {
                if (this.specialshipitems[_loc_3][5] == "FLARE")
                {
                    _loc_5 = this.specialshipitems[_loc_3][2];
                    if (this.playershipstatus[1][1] >= _loc_5 && this.playershipstatus[11][2][_loc_2][0] < _loc_4)
                    {
                        this.playershipstatus[1][1] = this.playershipstatus[1][1] - _loc_5;
                        this.playershipstatus[11][2][_loc_2][0] = _loc_4 + this.specialshipitems[_loc_3][4];
                        this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
                        _loc_6 = Math.round(this.shipcoordinatex);
                        _loc_7 = Math.round(this.shipcoordinatey);
                        trace("flare at:" + _loc_6 + "`" + _loc_7);
                    }
                }
                else if (this.specialshipitems[_loc_3][5] == "DETECTOR")
                {
                    if (this.playershipstatus[11][2][_loc_2][0] == 0)
                    {
                        this.specialsingame["sp" + param1].specialbutton.gotoAndStop("ON");
                        this.playershipstatus[11][2][_loc_2][0] = this.specialshipitems[_loc_3][11] + _loc_4;
                        this.specialsingame["sp" + param1].specialinfodata.text = "ON";
                        this.func_detectorPing(this.specialshipitems[_loc_3][10]);
                    }
                    else
                    {
                        this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
                    }
                }
                else if (this.specialshipitems[_loc_3][5] == "STEALTH")
                {
                    if (this.playershipstatus[11][2][_loc_2][0] == 0)
                    {
                        if (!this.playerSpecialsSettings.isStealthed && !this.playerSpecialsSettings.isCloaked)
                        {
                            this.specialsingame["sp" + param1].specialbutton.gotoAndStop("ON");
                            this.playershipstatus[11][2][_loc_2][0] = -1;
                            this.specialsingame["sp" + param1].specialinfodata.text = "ON";
                            this.func_stealthplayership(100, this.specialshipitems[_loc_3][10], param1);
                        }
                    }
                    else
                    {
                        this.func_turnoffstealth();
                    }
                }
                else if (this.specialshipitems[_loc_3][5] == "CLOAK")
                {
                    if (this.playershipstatus[11][2][_loc_2][0] == 0)
                    {
                        if (!this.playerSpecialsSettings.isStealthed && !this.playerSpecialsSettings.isCloaked)
                        {
                            this.specialsingame["sp" + param1].specialbutton.gotoAndStop("ON");
                            this.playershipstatus[11][2][_loc_2][0] = -1;
                            this.specialsingame["sp" + param1].specialinfodata.text = "ON";
                            this.func_cloakplayership(100, this.specialshipitems[_loc_3][10], param1);
                        }
                    }
                    else
                    {
                        this.func_turnofshipcloak();
                    }
                }
                else if (this.specialshipitems[_loc_3][5] == "RAPIDMISSILE")
                {
                    if (this.playershipstatus[11][2][_loc_2][0] == 0)
                    {
                        this.RapidMissilesOn = true;
                        this.RapidMissileRateChange = this.specialshipitems[_loc_3][11];
                        this.EnergyPerRapid = this.specialshipitems[_loc_3][2];
                        this.specialsingame["sp" + param1].specialbutton.gotoAndStop("ON");
                        this.playershipstatus[11][2][_loc_2][0] = -1;
                        this.specialsingame["sp" + param1].specialinfodata.text = "ON";
                    }
                    else
                    {
                        this.RapidMissilesOn = false;
                        this.specialsingame["sp" + param1].specialbutton.gotoAndStop("OFF");
                        this.specialsingame["sp" + param1].specialinfodata.text = "";
                        this.playershipstatus[11][2][_loc_2][0] = 0;
                    }
                }
                else if (this.specialshipitems[_loc_3][5] == "RECHARGESHIELD")
                {
                    _loc_5 = Math.floor(this.playershipstatus[1][1]);
                    if (this.playershipstatus[1][1] >= _loc_5 && this.playershipstatus[11][2][_loc_2][0] == 0)
                    {
                        this.playershipstatus[1][1] = this.playershipstatus[1][1] - _loc_5;
                        this.playershipstatus[11][2][_loc_2][0] = _loc_4 + this.specialshipitems[_loc_3][4];
                        this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
                        _loc_8 = _loc_5 * this.specialshipitems[_loc_3][2];
                        _loc_9 = this.playershipstatus[2][1];
                        if (_loc_8 + _loc_9 > this.PlayerStatDisp.maxshieldstrength)
                        {
                            _loc_8 = Math.floor(this.PlayerStatDisp.maxshieldstrength - _loc_9);
                        }
                        this.playershipstatus[2][1] = this.playershipstatus[2][1] + _loc_8;
                        this.gunShotBufferData = this.gunShotBufferData + ("SH" + "`" + this.playershipstatus[3][0] + "`" + _loc_3 + "`" + Math.floor(this.playershipstatus[2][1]) + "~");
                    }
                }
                else if (this.specialshipitems[_loc_3][5] == "RECHARGESTRUCT")
                {
                    _loc_5 = Math.floor(this.playershipstatus[1][1]);
                    if (this.playershipstatus[1][1] >= _loc_5 && this.playershipstatus[11][2][_loc_2][0] == 0)
                    {
                        if (this.PlayerStatDisp.playersmaxstructure != null)
                        {
                            this.playershipstatus[1][1] = this.playershipstatus[1][1] - _loc_5;
                            this.playershipstatus[11][2][_loc_2][0] = _loc_4 + this.specialshipitems[_loc_3][4];
                            this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
                            _loc_10 = _loc_5 * this.specialshipitems[_loc_3][2];
                            this.specialsingame["sp" + param1].specialinfodata.text = "USED";
                            _loc_11 = this.playershipstatus[2][5];
                            if (_loc_11 + _loc_10 > this.PlayerStatDisp.playersmaxstructure)
                            {
                                _loc_10 = Math.floor(this.PlayerStatDisp.playersmaxstructure - _loc_11);
                            }
                            this.playershipstatus[2][5] = this.playershipstatus[2][5] + _loc_10;
                            this.gunShotBufferData = this.gunShotBufferData + ("SH" + "`" + this.playershipstatus[3][0] + "`" + _loc_3 + "`" + Math.floor(this.playershipstatus[2][5]) + "~");
                        }
                    }
                }
                else if (this.specialshipitems[_loc_3][5] == "MINES")
                {
                    _loc_5 = this.specialshipitems[_loc_3][2];
                    if (this.playershipstatus[5][15].charAt(0) == "C")
                    {
                    }
                    else if (this.playershipstatus[11][1][_loc_2][1] < 1)
                    {
                    }
                    else if (this.playershipstatus[11][1][_loc_2][1] > 0)
                    {
                        var _loc_15:* = this.playershipstatus[11][1][_loc_2];
                        var _loc_16:* = 1;
                        var _loc_17:* = _loc_15[1] - 1;
                        _loc_15[_loc_16] = _loc_17;
                        this.playershipstatus[11][2][_loc_2][0] = _loc_4 + this.specialshipitems[_loc_3][4];
                        this.specialsingame["sp" + param1].specialbutton.gotoAndStop("RELOAD");
                        this.func_displayspecials(param1);
                        _loc_12 = Math.round(this.shipcoordinatex);
                        _loc_13 = Math.round(this.shipcoordinatey);
                        var _loc_15:* = this;
                        var _loc_16:* = this.currentplayershotsfired + 1;
                        _loc_15.currentplayershotsfired = _loc_16;
                        if (this.currentplayershotsfired > 999)
                        {
                            this.currentplayershotsfired = 0;
                        }
                        _loc_14 = this.playershipstatus[3][0] + "a" + this.currentplayershotsfired;
                    }
                }
            }
            return;
        }// end function

        public function ChangeSquadBTY(param1, param2)
        {
            var _loc_3:* = undefined;
            _loc_3 = Number(param1) - this.SquadBaseIDAdjust;
            if (this.playersquadbases[_loc_3] != null)
            {
                this.playersquadbases[_loc_3][9] = Number(param2);
                this.func_setSquadbaseLabel(_loc_3);
            }
            return;
        }// end function

        public function otherPilotProcess(param1)
        {
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            var _loc_15:* = undefined;
            var _loc_16:* = undefined;
            var _loc_17:* = undefined;
            var _loc_18:* = undefined;
            var _loc_19:* = undefined;
            var _loc_2:* = getTimer();
            var _loc_3:* = this.otherplayership.length;
            var _loc_4:* = param1[1];
            var _loc_5:* = 0;
            var _loc_6:* = false;
            while (_loc_5 < this.otherplayership.length && _loc_6 == false)
            {
                
                if (_loc_4 == this.otherplayership[_loc_5][0])
                {
                    if (this.otherplayership[_loc_5][15] != "alive")
                    {
                        this.otherplayership[_loc_5][6] = 0;
                        _loc_5 = 9999999999;
                    }
                    else
                    {
                        _loc_7 = Number(param1[9]);
                        _loc_3 = _loc_5;
                        _loc_6 = true;
                        if (_loc_7 > this.otherplayership[_loc_3][51] || _loc_7 < 15 && this.otherplayership[_loc_3][51] > 60 || isNaN(_loc_7))
                        {
                            this.otherplayership[_loc_3][51] = _loc_7;
                            this.otherplayership[_loc_3][0] = param1[1];
                            _loc_8 = int(param1[2]);
                            _loc_9 = this.otherplayership[_loc_3][30].length;
                            this.otherplayership[_loc_3][30][_loc_9] = new Array();
                            this.otherplayership[_loc_3][30][_loc_9][0] = _loc_8;
                            _loc_10 = int(param1[3]);
                            this.otherplayership[_loc_3][30][_loc_9][1] = int(_loc_10);
                            this.otherplayership[_loc_3][30][_loc_9][2] = int(param1[4]);
                            this.otherplayership[_loc_3][30][_loc_9][3] = Number(param1[5]);
                            this.otherplayership[_loc_3][30][_loc_9][6] = new Array();
                            this.otherplayership[_loc_3][30][_loc_9][6][0] = 0;
                            this.otherplayership[_loc_3][30][_loc_9][6][1] = 0;
                            _loc_18 = 0;
                            while (_loc_18 < param1[6].length)
                            {
                                
                                if (param1[6].charAt(_loc_18) == "S")
                                {
                                    this.otherplayership[_loc_3][30][_loc_9][6][0] = "S";
                                }
                                else if (param1[6].charAt(_loc_18) == "U")
                                {
                                    this.otherplayership[_loc_3][30][_loc_9][6][0] = this.otherplayership[_loc_3][30][_loc_9][6][0] + this.shiptype[this.otherplayership[_loc_3][11]][3][0];
                                }
                                else if (param1[6].charAt(_loc_18) == "D")
                                {
                                    this.otherplayership[_loc_3][30][_loc_9][6][0] = this.otherplayership[_loc_3][30][_loc_9][6][0] - this.shiptype[this.otherplayership[_loc_3][11]][3][0];
                                }
                                else if (param1[6].charAt(_loc_18) == "L")
                                {
                                    this.otherplayership[_loc_3][30][_loc_9][6][1] = this.otherplayership[_loc_3][30][_loc_9][6][1] - this.shiptype[this.otherplayership[_loc_3][11]][3][2];
                                }
                                else if (param1[6].charAt(_loc_18) == "R")
                                {
                                    this.otherplayership[_loc_3][30][_loc_9][6][1] = this.otherplayership[_loc_3][30][_loc_9][6][1] + this.shiptype[this.otherplayership[_loc_3][11]][3][2];
                                }
                                _loc_18 = _loc_18 + 1;
                            }
                            if (this.otherplayership[_loc_3][16] != param1[7])
                            {
                                _loc_15 = String(param1[7]);
                                if (_loc_15 == "C" && this.otherplayership[_loc_3][16] != "D")
                                {
                                }
                                else if (_loc_15 == "S" && this.otherplayership[_loc_3][16] != "D")
                                {
                                }
                                else if (_loc_15 != "S" && _loc_15 != "C")
                                {
                                }
                                this.otherplayership[_loc_3][16] = _loc_15;
                            }
                            _loc_11 = String(getTimer() + this.clocktimediff);
                            _loc_11 = int(_loc_11.substr(_loc_11.length - 4));
                            _loc_12 = _loc_11 - int(param1[8]);
                            if (_loc_12 < -1500)
                            {
                                _loc_12 = _loc_12 + 10000;
                            }
                            _loc_13 = this.shiplag - _loc_12 + getTimer();
                            if (this.shiplag - _loc_12 < -60 && this.shiplag - _loc_12 > -150)
                            {
                            }
                            this.func_LatencyTimeUpd(this.shiplag - _loc_12);
                            this.otherplayership[_loc_3][30][_loc_9][9] = _loc_13;
                            this.otherplayership[_loc_3][30][_loc_9][8] = Number(param1[9]);
                            _loc_14 = 0;
                            while (_loc_14 < (this.otherplayership[_loc_3][30].length - 1))
                            {
                                
                                if (this.otherplayership[_loc_3][30][_loc_14][9] > this.otherplayership[_loc_3][30][_loc_9][9])
                                {
                                    this.otherplayership[_loc_3][30].splice(_loc_9, 1);
                                    _loc_9 = _loc_9 - 1;
                                }
                                _loc_14 = _loc_14 + 1;
                            }
                            this.otherplayership[_loc_3][6] = _loc_2 + this.shiptimeoutime;
                        }
                    }
                }
                _loc_5 = _loc_5 + 1;
            }
            if (_loc_6 == false)
            {
                _loc_3 = _loc_5;
                _loc_3 = this.otherplayership.length;
                this.otherplayership[_loc_3] = new Array();
                this.otherplayership[_loc_3][0] = param1[1];
                this.otherplayership[_loc_3][1] = int(param1[2]);
                this.otherplayership[_loc_3][2] = int(param1[3]);
                this.otherplayership[_loc_3][3] = int(param1[4]);
                this.otherplayership[_loc_3][4] = int(param1[5]);
                this.otherplayership[_loc_3][5] = new Array();
                this.otherplayership[_loc_3][5][0] = int(0);
                this.otherplayership[_loc_3][5][1] = int(0);
                this.otherplayership[_loc_3][6] = _loc_2 + this.shiptimeoutime;
                this.otherplayership[_loc_3][7] = _loc_2 + this.shiptimeoutime * 2;
                this.otherplayership[_loc_3][30] = new Array();
                _loc_16 = 0;
                _loc_17 = false;
                while (_loc_16 < this.currentonlineplayers.length && _loc_17 == false)
                {
                    
                    if (this.otherplayership[_loc_3][0] == this.currentonlineplayers[_loc_16][0])
                    {
                        _loc_17 = true;
                        this.otherplayership[_loc_3][10] = this.currentonlineplayers[_loc_16][1];
                        this.otherplayership[_loc_3][11] = this.currentonlineplayers[_loc_16][2];
                        this.otherplayership[_loc_3][12] = this.currentonlineplayers[_loc_16][3];
                        this.otherplayership[_loc_3][13] = this.currentonlineplayers[_loc_16][4];
                        this.otherplayership[_loc_3][14] = this.currentonlineplayers[_loc_16][8];
                        this.otherplayership[_loc_3][19] = false;
                    }
                    _loc_16 = _loc_16 + 1;
                }
                if (_loc_17 == false)
                {
                    this.otherplayership.splice(_loc_3, 1);
                }
                else
                {
                    this.otherplayership[_loc_3][15] = "alive";
                    _loc_18 = 0;
                    while (_loc_18 < param1[6].length)
                    {
                        
                        if (param1[6].charAt(_loc_18) == "S")
                        {
                            this.otherplayership[_loc_3][5][0] = "S";
                        }
                        else if (param1[6].charAt(_loc_18) == "U")
                        {
                            this.otherplayership[_loc_3][5][0] = this.otherplayership[_loc_3][5][0] + this.shiptype[this.otherplayership[_loc_3][11]][3][0];
                        }
                        else if (param1[6].charAt(_loc_18) == "D")
                        {
                            this.otherplayership[_loc_3][5][0] = this.otherplayership[_loc_3][5][0] - this.shiptype[this.otherplayership[_loc_3][11]][3][0];
                        }
                        else if (param1[6].charAt(_loc_18) == "L")
                        {
                            this.otherplayership[_loc_3][5][1] = this.otherplayership[_loc_3][5][1] - this.shiptype[this.otherplayership[_loc_3][11]][3][2];
                        }
                        else if (param1[6].charAt(_loc_18) == "R")
                        {
                            this.otherplayership[_loc_3][5][1] = this.otherplayership[_loc_3][5][1] + this.shiptype[this.otherplayership[_loc_3][11]][3][2];
                        }
                        _loc_18 = _loc_18 + 1;
                    }
                    this.otherplayership[_loc_3][5][9] = getTimer();
                    this.otherplayership[_loc_3][5][8] = -1;
                    this.otherplayership[_loc_3][16] = String(param1[7]);
                    this.otherplayership[_loc_3][21] = new this.shiptype[this.otherplayership[_loc_3][11]][10] as MovieClip;
                    if (this.playershipstatus[3][0] != Number(param1[1]))
                    {
                        this.gamedisplayarea.addChild(this.otherplayership[_loc_3][21]);
                        this.otherplayership[_loc_3][21].x = this.otherplayership[_loc_3][1] - this.shipcoordinatex;
                        this.otherplayership[_loc_3][21].y = this.otherplayership[_loc_3][2] - this.shipcoordinatey;
                        this.otherplayership[_loc_3][21].rotation = this.otherplayership[_loc_3][2];
                        this.otherplayership[_loc_3][21].gotoAndStop(1);
                        _loc_19 = "";
                        if (this.otherplayership[_loc_3][13] == "N/A" || this.otherplayership[_loc_3][13] == -1 || this.otherplayership[_loc_3][13] != this.playershipstatus[5][2])
                        {
                            _loc_19 = "enemy";
                        }
                        else
                        {
                            _loc_19 = "friend";
                        }
                        this.otherplayership[_loc_3][52] = new MovieClip();
                        this.otherplayership[_loc_3][53] = new shiphealthbar() as MovieClip;
                        this.otherplayership[_loc_3][53].y = -this.otherplayership[_loc_3][21].width * 0.8;
                        this.otherplayership[_loc_3][52].addChild(this.otherplayership[_loc_3][53]);
                        this.otherplayership[_loc_3][54] = new shipnametag() as MovieClip;
                        this.otherplayership[_loc_3][54].y = this.otherplayership[_loc_3][53].y;
                        this.otherplayership[_loc_3][52].addChild(this.otherplayership[_loc_3][54]);
                        this.otherplayership[_loc_3][58] = new this.shipTargetDisplay() as MovieClip;
                        this.otherplayership[_loc_3][58].height = this.otherplayership[_loc_3][21].width;
                        this.otherplayership[_loc_3][58].width = this.otherplayership[_loc_3][21].width;
                        this.otherplayership[_loc_3][58].visible = false;
                        this.otherplayership[_loc_3][52].addChild(this.otherplayership[_loc_3][58]);
                        this.otherplayership[_loc_3][55] = new othershipiderDisplay() as MovieClip;
                        this.otherplayership[_loc_3][59] = new this.RadarBlot() as MovieClip;
                        if (_loc_19 == "friend")
                        {
                            this.otherplayership[_loc_3][54].gotoAndStop(2);
                            this.otherplayership[_loc_3][55].gotoAndStop("teammate");
                            this.func_AddRadarDot(this.otherplayership[_loc_3][59], "FREIND");
                        }
                        else
                        {
                            this.otherplayership[_loc_3][54].gotoAndStop(1);
                            this.otherplayership[_loc_3][55].gotoAndStop("enemy");
                            this.func_AddRadarDot(this.otherplayership[_loc_3][59], "HOSTILE");
                        }
                        this.otherplayership[_loc_3][55].height = this.otherplayership[_loc_3][21].width;
                        this.otherplayership[_loc_3][55].width = this.otherplayership[_loc_3][21].width;
                        this.otherplayership[_loc_3][52].addChild(this.otherplayership[_loc_3][55]);
                        this.func_refreshOtherPlayerDisplayedLabel(this.otherplayership[_loc_3][0]);
                        this.otherplayership[_loc_3][56] = _loc_19;
                        this.otherplayership[_loc_3][57] = new this.shiptype[this.otherplayership[_loc_3][11]][11] as MovieClip;
                        this.otherplayership[_loc_3][57].gotoAndStop(4);
                        this.otherplayership[_loc_3][21].addChild(this.otherplayership[_loc_3][57]);
                        this.otherplayership[_loc_3][21].addChild(this.otherplayership[_loc_3][52]);
                        this.otherplayership[_loc_3][52].rotation = -this.otherplayership[_loc_3][3];
                    }
                    this.otherplayership[_loc_3][35] = 0;
                    this.otherplayership[_loc_3][36] = 0;
                    this.otherplayership[_loc_3][37] = -9999;
                    this.otherplayership[_loc_3][38] = -9999;
                    this.otherplayership[_loc_3][39] = -9999;
                    this.otherplayership[_loc_3][40] = 0;
                    this.otherplayership[_loc_3][41] = null;
                    this.otherplayership[_loc_3][42] = null;
                    this.otherplayership[_loc_3][43] = null;
                    this.otherplayership[_loc_3][44] = null;
                    this.otherplayership[_loc_3][46] = null;
                    this.otherplayership[_loc_3][47] = 0;
                    this.otherplayership[_loc_3][51] = -1;
                    this.func_RequestStats(this.otherplayership[_loc_3][0]);
                }
            }
            return;
        }// end function

        public function func_ClockSynchronizeScript(event:Event)
        {
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_2:* = getTimer();
            if (_loc_2 > this.timetillnexClocktcheck)
            {
                this.loginmovie.mov_login.logindisplay.currentstatus.text = this.loginmovie.mov_login.logindisplay.currentstatus.text + ".";
                this.timetillnexClocktcheck = _loc_2 + this.ClockIntervalCheckTime;
                var _loc_5:* = this;
                var _loc_6:* = this.CurenntClockCheckNumber + 1;
                _loc_5.CurenntClockCheckNumber = _loc_6;
                _loc_3 = String(_loc_2);
                _loc_3 = _loc_3.substr(_loc_3.length - 4);
                _loc_4 = "CLOCK`" + _loc_3 + "~";
                this.mysocket.send(_loc_4);
            }
            return;
        }// end function

        public function func_setSelectedIconMessage(param1)
        {
            if (param1 == "")
            {
                this.main_docked_screen.current_Selection.text = "Roll Over an Icon";
            }
            else if (param1 == "hangar")
            {
                this.main_docked_screen.current_Selection.text = "Your Hangar \r Buy + Sell Ships";
            }
            else if (param1 == "options")
            {
                this.main_docked_screen.current_Selection.text = "Game Settings \r Squad Settings";
            }
            else if (param1 == "ShipHardware")
            {
                this.main_docked_screen.current_Selection.text = "Equip your Ship";
            }
            else if (param1 == "exitDock")
            {
                this.main_docked_screen.current_Selection.text = "Leave Dock";
            }
            else if (param1 == "tradegoods")
            {
                this.main_docked_screen.current_Selection.text = "Buy/Sell Trade Goods";
            }
            else if (param1 == "missions")
            {
                this.main_docked_screen.current_Selection.text = "Missions";
            }
            return;
        }// end function

        public function DockingTipTimerHandler(event:TimerEvent) : void
        {
            var event:* = event;
            try
            {
                this.dockingTipText.text = this.func_getDockingTipMessage();
            }
            catch (error:Error)
            {
                DockingTipMessage.stop();
            }
            return;
        }// end function

        public function initializemissilebanks()
        {
            var _loc_3:* = undefined;
            this.playershipstatus[10][0] = 0;
            this.playershipstatus[7] = new Array();
            var _loc_1:* = this.playershipstatus[5][0];
            var _loc_2:* = 0;
            
            while (_loc_2 < this.shiptype[_loc_1][7].length)
            {
                
                this.playershipstatus[7][_loc_2] = new Array();
                this.playershipstatus[7][_loc_2][0] = 0;
                this.playershipstatus[7][_loc_2][1] = 0;
                this.playershipstatus[7][_loc_2][2] = this.shiptype[_loc_1][7][_loc_2][2];
                this.playershipstatus[7][_loc_2][3] = this.shiptype[_loc_1][7][_loc_2][3];
                this.playershipstatus[7][_loc_2][4] = new Array();
                _loc_3 = 0;
                while (_loc_3 < this.missile.length)
                {
                    
                    this.playershipstatus[7][_loc_2][4][_loc_3] = 0;
                    _loc_3 = _loc_3 + 1;
                }
                this.playershipstatus[7][_loc_2][5] = this.shiptype[_loc_1][7][_loc_2][5];
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_addAllMeteorBackgroundimages()
        {
            var _loc_1:* = 0;
            while (_loc_1 < this.maxMeteors)
            {
                
                if (this.gameMeteors[_loc_1] != null)
                {
                    this.addSingleMeteorBackgroundImage(_loc_1);
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_ExitTheGame(event:MouseEvent) : void
        {
            this.gameerror = "endplayersgame";
            this.mysocket.close();
            gotoAndPlay("gameclose");
            return;
        }// end function

        public function func_playStaticSound()
        {
            if (this.gamesetting.CombatSounds)
            {
                if (this.lastStaticSound < getTimer())
                {
                    this.gamesetting.IngameChannelSound = this.gameSounds.RadioStatic.play();
                    this.gamesetting.IngameChannelSound.soundTransform = this.gamesetting.IngameSoundTransform;
                }
                this.lastStaticSound = getTimer() + 500;
            }
            return;
        }// end function

        public function func_GetPlayerSEndOutData(param1, param2, param3)
        {
            var _loc_4:* = "PI`" + param2 + "`" + param1 + "~";
            _loc_4 = "PI`" + param2 + "`" + param1 + "~" + ("PI" + "`" + this.playershipstatus[3][0] + "`");
            _loc_4 = _loc_4 + (Math.round(this.shipcoordinatex) + "`");
            _loc_4 = _loc_4 + (Math.round(this.shipcoordinatey) + "`");
            _loc_4 = _loc_4 + (Math.round(this.PlayersShipImage.rotation) + "`");
            _loc_4 = _loc_4 + (Math.round(this.playershipvelocity) + "`");
            _loc_4 = _loc_4 + (this.currentkeyedshipmovement() + "`");
            _loc_4 = _loc_4 + (this.playershipstatus[5][15] + "`");
            _loc_4 = _loc_4 + (this.func_globalTimeStamp(param3) + "`");
            _loc_4 = _loc_4 + (this.currentPIpacket + "~");
            var _loc_5:* = this;
            var _loc_6:* = this.currentPIpacket + 1;
            _loc_5.currentPIpacket = _loc_6;
            if (this.currentPIpacket > 99)
            {
                this.currentPIpacket = 0;
            }
            return _loc_4;
        }// end function

        public function func_setSquadbaseLabel(param1)
        {
            this.playersquadbases[param1][12].baseiddisp.text = this.playersquadbases[param1][0] + " (" + this.playersquadbases[param1][9] + ")";
            return;
        }// end function

        public function starbaseexitposition()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            trace("Conq Zone:" + this.PlayerInConquestZone);
            _loc_1 = 0;
            _loc_2 = false;
            _loc_3 = 0;
            if (this.teamdeathmatch)
            {
                trace(this.playershipstatus[5][2]);
                this.shipcoordinatex = 0;
                this.shipcoordinatey = 0;
                if (!isNaN(this.playershipstatus[5][2]) && this.playershipstatus[5][2] >= 0)
                {
                    if (this.playershipstatus[5][2] < this.teambases.length)
                    {
                        _loc_4 = Math.round(this.teambases[Number(this.playershipstatus[5][2])][4] / 5 * 4);
                        this.shipcoordinatex = this.teambases[this.playershipstatus[5][2]][1] + _loc_4 * 2 * Math.random() - _loc_4;
                        this.shipcoordinatey = this.teambases[this.playershipstatus[5][2]][2] + _loc_4 * 2 * Math.random() - _loc_4;
                    }
                }
            }
            else if (this.PlayerInConquestZone)
            {
                _loc_1 = 0;
                _loc_2 = false;
                _loc_3 = 0;
                while (_loc_1 < this.starbaselocation.length && _loc_2 == false)
                {
                    
                    if (this.starbaselocation[_loc_1][0] == this.playershipstatus[4][0])
                    {
                        if (this.starbaselocation[_loc_1][16] == "-2" || this.starbaselocation[_loc_1][16] == this.playershipstatus[5][2])
                        {
                            _loc_2 = true;
                            _loc_3 = Math.round(this.starbaselocation[_loc_1][4] / 5 * 4);
                            this.shipcoordinatex = this.starbaselocation[_loc_1][1] + _loc_3 * 2 * Math.random() - _loc_3;
                            this.shipcoordinatey = this.starbaselocation[_loc_1][2] + _loc_3 * 2 * Math.random() - _loc_3;
                        }
                    }
                    _loc_1 = _loc_1 + 1;
                }
                if (!_loc_2)
                {
                    trace("Looking fo rdoc loc");
                    _loc_1 = 0;
                    while (_loc_1 < this.starbaselocation.length && _loc_2 == false)
                    {
                        
                        if (this.starbaselocation[_loc_1][0].substr(0, 2) == "PL")
                        {
                            if (this.starbaselocation[_loc_1][16] == this.playershipstatus[5][2])
                            {
                                _loc_3 = Math.round(this.starbaselocation[_loc_1][4] / 5 * 4);
                                this.shipcoordinatex = this.starbaselocation[_loc_1][1] + _loc_3 * 2 * Math.random() - _loc_3;
                                this.shipcoordinatey = this.starbaselocation[_loc_1][2] + _loc_3 * 2 * Math.random() - _loc_3;
                            }
                        }
                        _loc_1 = _loc_1 + 1;
                    }
                }
                if (this.shipcoordinatex < 0 || this.shipcoordinatey < 0)
                {
                    this.shipcoordinatex = 0;
                    this.shipcoordinatey = 0;
                }
            }
            else
            {
                this.playershipstatus[5][2] = "SB112";
                _loc_1 = 0;
                _loc_2 = false;
                _loc_3 = 0;
                while (_loc_1 < this.starbaselocation.length && _loc_2 == false)
                {
                    
                    if (this.starbaselocation[_loc_1][0] == this.playershipstatus[4][0])
                    {
                        _loc_2 = true;
                        _loc_3 = Math.round(this.starbaselocation[_loc_1][4] / 5 * 4);
                        this.shipcoordinatex = this.starbaselocation[_loc_1][1] + _loc_3 * 2 * Math.random() - _loc_3;
                        this.shipcoordinatey = this.starbaselocation[_loc_1][2] + _loc_3 * 2 * Math.random() - _loc_3;
                    }
                    _loc_1 = _loc_1 + 1;
                }
                if (!_loc_2)
                {
                    _loc_1 = Math.round(Math.random() * this.starbaselocation.length - 1);
                    if (this.starbaselocation[_loc_1] != null)
                    {
                        this.playershipstatus[4][0] = this.starbaselocation[_loc_1][0];
                        _loc_3 = Math.round(this.starbaselocation[_loc_1][4] / 5 * 4);
                        this.shipcoordinatex = this.starbaselocation[_loc_1][1] + _loc_3 * 2 * Math.random() - _loc_3;
                        this.shipcoordinatey = this.starbaselocation[_loc_1][2] + _loc_3 * 2 * Math.random() - _loc_3;
                    }
                }
                if (this.shipcoordinatex < 0 || this.shipcoordinatey < 0)
                {
                    this.shipcoordinatex = 0;
                    this.shipcoordinatey = 0;
                }
            }
            return;
        }// end function

        public function func_checkForShipChange()
        {
            var _loc_2:* = undefined;
            var _loc_1:* = this.playershipstatus[5][0];
            this.playershipstatus = this.hangarWindow.playershipstatus;
            if (this.hangarWindow.playerscurrentextrashipno != this.playerscurrentextrashipno || this.playershipstatus[5][0] != this.hangarWindow.extraplayerships[this.playerscurrentextrashipno][0])
            {
                if (this.playershipstatus[5][0] == this.hangarWindow.extraplayerships[this.playerscurrentextrashipno][0])
                {
                    this.savecurrenttoextraship(this.playerscurrentextrashipno);
                }
                this.extraplayerships = this.hangarWindow.extraplayerships;
                this.playerscurrentextrashipno = this.hangarWindow.playerscurrentextrashipno;
                this.changetonewship(this.playerscurrentextrashipno);
                this.initializemissilebanks();
                _loc_2 = "TC" + "`";
                _loc_2 = _loc_2 + this.playershipstatus[3][0];
                _loc_2 = _loc_2 + ("`SC`" + this.playershipstatus[5][0] + "`0~");
                this.mysocket.send(_loc_2);
            }
            return;
        }// end function

        public function func_RemoveRadarDot(param1)
        {
            var ChildName:* = param1;
            try
            {
                this.gameRadar.radarScreen.removeChild(ChildName);
            }
            catch (error:Error)
            {
                trace("Raddar Remove Error: " + error);
            }
            return;
        }// end function

        public function func_MoveGunShots(param1, param2)
        {
            var currenttimechangeratio:* = param1;
            var curentGametime:* = param2;
            var currentnumber:*;
            while (currentnumber < this.playershotsfired.length)
            {
                
                if (this.playershotsfired[currentnumber][10])
                {
                    this.playershotsfired[currentnumber][10] = false;
                }
                else if (this.playershotsfired[currentnumber][5] < curentGametime)
                {
                    this.func_removePlayersShot(currentnumber);
                    currentnumber = (currentnumber - 1);
                }
                else
                {
                    this.playershotsfired[currentnumber][1] = this.playershotsfired[currentnumber][1] + this.playershotsfired[currentnumber][3] * currenttimechangeratio;
                    this.playershotsfired[currentnumber][2] = this.playershotsfired[currentnumber][2] + this.playershotsfired[currentnumber][4] * currenttimechangeratio;
                    try
                    {
                        this.playershotsfired[currentnumber][9].x = this.playershotsfired[currentnumber][1] - this.shipcoordinatex;
                        this.playershotsfired[currentnumber][9].y = this.playershotsfired[currentnumber][2] - this.shipcoordinatey;
                    }
                    catch (error:Error)
                    {
                        trace("Error moving player gunfire: " + error);
                    }
                }
                currentnumber = (currentnumber + 1);
            }
            this.othergunfiremovement(currenttimechangeratio);
            return;
        }// end function

        public function func_otherPLayerLogin(param1)
        {
            var _loc_2:* = param1[1];
            var _loc_3:* = String(param1[2]);
            var _loc_4:* = false;
            var _loc_5:* = 0;
            while (_loc_5 < this.currentonlineplayers.length)
            {
                
                if (String(this.currentonlineplayers[_loc_5][0]) == String(_loc_2) || String(this.currentonlineplayers[_loc_5][1]) == String(_loc_3))
                {
                    this.currentonlineplayers.splice(_loc_5, 1);
                    _loc_5 = _loc_5 - 1;
                }
                _loc_5 = _loc_5 + 1;
            }
            var _loc_6:* = this.currentonlineplayers.length;
            this.currentonlineplayers[_loc_6] = new Array();
            this.currentonlineplayers[_loc_6][0] = param1[1];
            this.currentonlineplayers[_loc_6][1] = param1[2];
            this.currentonlineplayers[_loc_6][2] = param1[3];
            this.currentonlineplayers[_loc_6][3] = param1[4];
            this.currentonlineplayers[_loc_6][4] = param1[5];
            if (isNaN(this.currentonlineplayers[_loc_6][4]))
            {
                this.currentonlineplayers[_loc_6][4] = -1;
            }
            this.currentonlineplayers[_loc_6][5] = int(param1[6]);
            this.currentonlineplayers[_loc_6][6] = int(param1[7]);
            this.currentonlineplayers[_loc_6][7] = int(param1[8]);
            this.currentonlineplayers[_loc_6][8] = int(param1[9]);
            this.currentonlineplayers[_loc_6][9] = param1[10];
            this.currentonlineplayers[_loc_6][10] = param1[11];
            if (param1[2] == this.playershipstatus[3][2] && this.playershipstatus[3][2] == this.playershipstatus[3][0])
            {
                this.playershipstatus[3][0] = param1[1];
            }
            this.func_refreshCurrentOnlineList();
            if (this.playershipstatus[3][0] != null)
            {
                this.func_enterintochat(param1[2] + " has entered", this.playerEnteringCol);
                this.func_AddChatter(param1[2] + " has entered");
            }
            return;
        }// end function

        public function func_DisplayError(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            if (param1 == "failedtologin")
            {
                this.gameError.errormessage.text = " Failed to Login \r" + "Probable Reasons For This - \r" + "1. Server is down, or could not connect to server \r" + "2. You are not connected to the internet or information cannot  \r" + "Pass through a firewall\r" + "3. Failed Login  \r\r" + "Post on the Forums if this continues to happen  \r";
            }
            else if (param1 == "hostclosedconnection")
            {
                this.gameError.errormessage.text = " Connection Has Closed \r" + "You Have lost the connection to the server \r" + "Probable Reasons For This - \r" + "1. Server shut down \r" + "2. Your connection to the internet was broken \r" + "3. Server Full, come back later";
            }
            else if (param1 == "toomanyplayers")
            {
                this.gameError.errormessage.text = " Too Many Players \r" + "Probable Reasons For This - \r" + "1. There are too many players, Try again later \r";
            }
            else if (param1 == "servershutdown")
            {
                this.gameError.errormessage.text = " The server was shut down \r" + "Probable Reasons For This - \r" + "1. An Admin needed to restart the server. \r" + "It will take a few minuites for the server to reload \r" + "You need to close this window and try reloading form the Homepage.";
            }
            else if (param1 == "nameinuse")
            {
                this.gameError.errormessage.text = " Your Name is Already in Use \r" + "Probable Reasons For This - \r" + "1. Your name is already in use in this zone. \r" + "It will take a few minuites for you to time out if disconnected  \r";
            }
            else if (param1 == "kicked")
            {
                this.gameError.errormessage.text = " You were booted by " + this.gameerrordoneby + "\r" + "Probable Reasons For This - \r" + "1. You Have been a very naughty person \r" + "2. Game Error \r";
            }
            else if (param1 == "toomanyipplayers")
            {
                this.gameError.errormessage.text = " Too Many Players using this Internet Connection  \r" + "Probable Reasons For This - \r" + "1. Too many players on your network \r" + "2. Too many names logged in \r";
            }
            else if (param1 == "banned")
            {
                _loc_2 = Number(this.timebannedfor);
                _loc_3 = _loc_2 * 60;
                _loc_4 = Math.floor(_loc_3 / 60 / 60 / 24);
                _loc_3 = _loc_3 - _loc_4 * 60 * 60 * 24;
                _loc_5 = Math.floor(_loc_3 / 60 / 60);
                _loc_3 = _loc_3 - _loc_5 * 60 * 60;
                _loc_6 = Math.floor(_loc_3 / 60);
                _loc_3 = _loc_3 - _loc_6 * 60;
                _loc_7 = _loc_3;
                _loc_2 = _loc_4 + " Days, " + _loc_5 + " Hours, " + _loc_6 + " Minutes";
                this.gameError.errormessage.text = " Your Have Been Banned for " + _loc_2 + " by " + this.gameerrordoneby + "\r" + "Probable Reasons For This - \r" + "1. You are probably not welcomed here. \r" + "2. This Point of Access is banned. \r" + "3. In case of an error you can post in the forum. \r";
            }
            else if (param1 == "fpsstopped")
            {
                this.gameError.errormessage.text = " Your Frame Rate is Too Slow! \r" + "Probable Reasons For This - \r" + "1. You computer was running to slow \r" + "2. You tried to cheat \r";
            }
            else if (param1 == "savefailure")
            {
                this.gameError.errormessage.text = " Your Game Failed to Save! \r" + "Probable Reasons For This - \r" + "1. Your Save Transmission Failed \r" + "2. You are Playing With This Account in Another Zone at the Same Time \r" + "\r" + "If this continues you may end up being banned, /r and you will have to contact the webmaster to become unbanned.";
            }
            else if (param1 == "FLOODING")
            {
                this.gameError.errormessage.text = " Your Must Stop Flooding \r" + "Probable Reasons For This - \r" + "1. Your Are Flooding the Chat \r" + "\r" + "If this continues you may end up being reported and being banned.";
            }
            else if (param1 == "endplayersgame")
            {
                this.gameError.errormessage.text = " You have ended your game \r" + "Thanks for playing!.";
            }
            else
            {
                this.gameError.errormessage.text = " Unknown Error Occured";
            }
            return;
        }// end function

        public function func_specialsinfo(param1, param2, param3, param4)
        {
            if (param2 == "RELOAD")
            {
                this.specialsingame["sp" + param4].specialbutton.reloadtime = param3;
                this.func_displayspecials(param4);
                this.specialsingame["sp" + param4].specialbutton.gotoAndStop("RELOAD");
                this.specialsingame["sp" + param4].specialinfodata.text = "RELOAD";
            }
            if (param2 == "FAILED")
            {
                this.specialsingame["sp" + param4].specialbutton.reloadtime = param3;
                this.specialsingame["sp" + param4].specialinfodata.text = "FAILED";
                this.func_displayspecials(param4);
                this.specialsingame["sp" + param4].specialbutton.gotoAndStop("RELOAD");
            }
            if (param2 == "ON")
            {
                this.specialsingame["sp" + param4].specialbutton.gotoAndStop("ON");
                this.specialsingame["sp" + param4].specialinfodata.text = "ON";
                this.func_displayspecials(param4);
            }
            if (param2 == "OFF")
            {
                this.func_displayspecials(param4);
                this.specialsingame["sp" + param4].specialbutton.gotoAndStop("OFF");
                this.specialsingame["sp" + param4].specialinfodata.text = "";
            }
            return;
        }// end function

        public function func_InGameMap_Click(event:MouseEvent) : void
        {
            this.func_TriggerGameMap();
            return;
        }// end function

        public function setMouseIsDown(event:MouseEvent) : void
        {
            this.TurretMouseDown = true;
            return;
        }// end function

        public function func_UpdateMusicVolume()
        {
            //this.MusicVolumeAdjust.volume = this.gamesetting.MusicVolume;
            //this.mySndCh.soundTransform = this.MusicVolumeAdjust;
            return;
        }// end function

        public function func_loadSectorItemsIntoBackgroup()
        {
            var teambasename:*;
            var basename:*;
            var extracount:*;
            var ii:*;
            while (ii < this.sectormapitems.length)
            {
                
                if (this.sectormapitems[ii] != null)
                {
                    if (this.sectormapitems[ii][0].substr(0, 2) == "TB")
                    {
                        teambasename = this.sectormapitems[ii][0];
                        extracount = 0;
                        while (extracount < this.teambases.length)
                        {
                            
                            if (this.teambases[extracount][0] == teambasename)
                            {
                                this.gamebackground.addChild(this.teambases[extracount][21]);
                                this.teambases[extracount][21].x = this.sectormapitems[ii][1];
                                this.teambases[extracount][21].y = this.sectormapitems[ii][2];
                                try
                                {
                                    this.teambases[extracount][21].gotoAndStop((extracount + 1));
                                }
                                catch (error:Error)
                                {
                                    trace("TeamBaseFrameNotThere");
                                }
                                this.teambases[extracount][21].baseiddisp.text = teambasename.substr(2);
                                this.func_AddRadarDot(this.teambases[extracount][22], "BASE");
                                this.teambases[extracount][22].x = this.sectormapitems[ii][1];
                                this.teambases[extracount][22].y = this.sectormapitems[ii][2];
                                this.func_updateTameBaseHealthBars(extracount);
                            }
                            extracount = (extracount + 1);
                        }
                    }
                    if (this.sectormapitems[ii][0].substr(0, 2) == "SB" || this.sectormapitems[ii][0].substr(0, 2) == "PL")
                    {
                        basename = this.sectormapitems[ii][0];
                        extracount = 0;
                        while (extracount < this.starbaselocation.length)
                        {
                            
                            if (this.starbaselocation[extracount] != null)
                            {
                                if (this.starbaselocation[extracount][0] == basename)
                                {
                                    this.gamebackground.addChild(this.starbaselocation[extracount][21]);
                                    this.starbaselocation[extracount][21].x = this.sectormapitems[ii][1];
                                    this.starbaselocation[extracount][21].y = this.sectormapitems[ii][2];
                                    this.starbaselocation[extracount][22].x = this.sectormapitems[ii][1];
                                    this.starbaselocation[extracount][22].y = this.sectormapitems[ii][2];
                                    this.func_AddRadarDot(this.starbaselocation[extracount][22], "BASE");
                                }
                            }
                            extracount = (extracount + 1);
                        }
                    }
                    if (this.sectormapitems[ii][0].substr(0, 2) == "NP")
                    {
                        this.gamebackground.addChild(this.sectormapitems[ii][21]);
                        this.sectormapitems[ii][21].x = this.sectormapitems[ii][1];
                        this.sectormapitems[ii][21].y = this.sectormapitems[ii][2];
                        this.func_AddRadarDot(this.sectormapitems[ii][22], "NP");
                        this.sectormapitems[ii][22].x = this.sectormapitems[ii][1];
                        this.sectormapitems[ii][22].y = this.sectormapitems[ii][2];
                    }
                }
                ii = (ii + 1);
            }
            return;
        }// end function

        public function makebackgroundstars()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = null;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            this.backgroundstar = new Array();
            _loc_1 = 0;
            while (_loc_1 < this.gamesetting.totalstars)
            {
                
                this.backgroundstar[_loc_1] = new Array(4);
                this.backgroundstar[_loc_1][0] = Math.random() * this.doublegameareawidth - Math.round(this.doublegameareawidth / 2);
                this.backgroundstar[_loc_1][1] = Math.random() * this.doublegameareaheight - Math.round(this.doublegameareaheight / 2);
                _loc_2 = getDefinitionByName("backgroundstar") as Class;
                this.backgroundstar[_loc_1][2] = new _loc_2 as MovieClip;
                this.gamedisplayarea.addChild(this.backgroundstar[_loc_1][2]);
                this.backgroundstar[_loc_1][2].x = this.backgroundstar[_loc_1][0];
                this.backgroundstar[_loc_1][2].y = this.backgroundstar[_loc_1][1];
                _loc_3 = Math.round(Math.random() * 3) + 1;
                if (_loc_3 != 1)
                {
                    this.backgroundstar[_loc_1][2].gotoAndStop(1);
                }
                else
                {
                    this.backgroundstar[_loc_1][2].gotoAndStop(1);
                }
                _loc_4 = 5 - _loc_3;
                if (_loc_4 < 2)
                {
                    _loc_4 = 2;
                }
                this.backgroundstar[_loc_1][2].scaleX = _loc_4;
                this.backgroundstar[_loc_1][2].scaleY = _loc_4;
                this.backgroundstar[_loc_1][3] = _loc_3;
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_getPlayersIngameLabel(param1)
        {
            var _loc_2:* = "";
            var _loc_3:* = 0;
            while (_loc_3 < this.currentonlineplayers.length)
            {
                
                if (param1 == this.currentonlineplayers[_loc_3][0])
                {
                    if (this.currentonlineplayers[_loc_3][9] != "NONE")
                    {
                        _loc_2 = this.currentonlineplayers[_loc_3][1] + " (" + this.currentonlineplayers[_loc_3][3] + ") ";
                        _loc_2 = _loc_2 + ("\r<" + this.currentonlineplayers[_loc_3][9] + ">");
                    }
                    else
                    {
                        _loc_2 = "\r" + this.currentonlineplayers[_loc_3][1] + " (" + this.currentonlineplayers[_loc_3][3] + ")";
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            return _loc_2;
        }// end function

        public function func_KeyReleaseWhileChatting()
        {
            if (this.chatDisplay.chatInput.text == "::")
            {
                if (this.gamechatinfo[5] != "")
                {
                    this.chatDisplay.chatInput.text = "/w " + this.gamechatinfo[5] + " ";
                }
            }
            else if (this.chatDisplay.chatInput.text.length > 120)
            {
                this.chatDisplay.chatInput.text = this.chatDisplay.chatInput.text.substr(0, 120);
            }
            return;
        }// end function

        function frame1()
        {
            //Security.allowDomain("*");
            //Security.loadPolicyFile("xmlsocket://24.183.128.150:843");
            this.isgamerunningfromremote = false;
            this.playerSpecialsSettings = new Object();
            this.playerSpecialsSettings.isCloaked = false;
            this.playerSpecialsSettings.CloakEnergy = 0;
            this.playerSpecialsSettings.CloakLocation = 0;
            this.playerSpecialsSettings.isStealthed = false;
            this.playerSpecialsSettings.StealthEnergy = 0;
            this.playerSpecialsSettings.StealthLocation = 0;
            return;
        }// end function

        function frame4()
        {
            if (framesLoaded == totalFrames)
            {
                play();
            }
            else
            {
                gotoAndPlay(2);
            }
            return;
        }// end function

        public function func_StarbaseDestroyed(param1)
        {
            var DestroyingRace:*;
            var BaseKillerId:*;
            var BaseReward:*;
            var PlayersNewBty:*;
            var fundsReward:*;
            var BaseKillerName:*;
            var jt:*;
            var tempImageHolder:*;
            var STBDoutputmessage:*;
            var currentthread:* = param1;
            var numberDestroyed:* = Number(currentthread[2]);
            if (!isNaN(numberDestroyed))
            {
                numberDestroyed = numberDestroyed - this.starbaseLevel;
                if (this.starbaselocation[numberDestroyed] != null)
                {
                    DestroyingRace = currentthread[3];
                    BaseKillerId = currentthread[4];
                    BaseReward = Number(currentthread[5]);
                    PlayersNewBty = Number(currentthread[6]);
                    fundsReward = BaseReward * this.extrafundsmultiplier;
                    BaseKillerName = "";
                    jt = 0;
                    while (jt < this.currentonlineplayers.length)
                    {
                        
                        if (this.currentonlineplayers[jt][0] == BaseKillerId)
                        {
                            BaseKillerName = this.currentonlineplayers[jt][1];
                            if (Number(this.currentonlineplayers[jt][0]) >= 0)
                            {
                                this.currentonlineplayers[jt][3] = PlayersNewBty;
                            }
                        }
                        if (this.currentonlineplayers[jt] != null)
                        {
                            if (this.currentonlineplayers[jt][4] == DestroyingRace)
                            {
                                if (Number(this.currentonlineplayers[jt][0]) >= 0)
                                {
                                    this.currentonlineplayers[jt][5] = this.currentonlineplayers[jt][5] + Number(BaseReward);
                                }
                            }
                        }
                        jt = (jt + 1);
                    }
                    if (this.playershipstatus[5][2] == DestroyingRace)
                    {
                        this.playershipstatus[5][9] = this.playershipstatus[5][9] + Number(BaseReward);
                        this.playershipstatus[3][1] = this.playershipstatus[3][1] + fundsReward;
                    }
                    if (this.playershipstatus[3][0] == BaseKillerId)
                    {
                        this.playershipstatus[5][8] = PlayersNewBty;
                    }
                    try
                    {
                        this.gamebackground.removeChild(this.starbaselocation[numberDestroyed][21]);
                    }
                    catch (error:Error)
                    {
                        trace("Starbase Not Removed on Death");
                    }
                    this.starbaselocation[numberDestroyed][5] = getTimer() + 600000;
                    tempImageHolder = getDefinitionByName("starbaseexplosion") as Class;
                    this.starbaselocation[numberDestroyed][21] = new tempImageHolder as MovieClip;
                    this.starbaselocation[numberDestroyed][21].x = this.starbaselocation[numberDestroyed][1];
                    this.starbaselocation[numberDestroyed][21].y = this.starbaselocation[numberDestroyed][2];
                    try
                    {
                        this.gamebackground.addChild(this.starbaselocation[numberDestroyed][21]);
                    }
                    catch (error:Error)
                    {
                        trace("Starbase Not Added on Death Image");
                    }
                    STBDoutputmessage = BaseKillerName + " Captured " + this.starbaselocation[numberDestroyed][0] + " (" + BaseReward + " BTY, " + fundsReward + ")";
                    this.func_refreshOtherPlayerDisplayedLabel(BaseKillerId);
                    this.func_refreshCurrentOnlineList();
                    this.func_AddChatter(STBDoutputmessage);
                    this.func_enterintochat(this.starbaselocation[numberDestroyed][0] + " has been captured!", this.systemchattextcolor);
                }
            }
            return;
        }// end function

        public function MissileTimerHandler(event:TimerEvent) : void
        {
            var event:* = event;
            try
            {
                this.playershipstatus[10][0] = this.missileBankWindow.func_RefreshDisplay(this.playershipstatus[7], this.playershipstatus[10][0], this.playershipstatus[10][1]);
            }
            catch (error:Error)
            {
                MissileTimer.stop();
            }
            return;
        }// end function

        public function func_OnGameFrame(event:Event)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            this.curentGametime = getTimer();
            _loc_2 = (this.curentGametime - this.LastFrameTime) * 0.001;
            this.LastFrameTime = this.curentGametime;
            _loc_2 = _loc_2;
            this.Proccess_Energies(_loc_2);
            _loc_3 = this.gunShotBufferData;
            this.func_MoveGunShots(_loc_2, this.curentGametime);
            this.othermissilefiremovement(_loc_2);
            if (_loc_3 != this.gunShotBufferData)
            {
                this.gunShotBufferData = this.gunShotBufferData + ("LFE`" + this.playershipstatus[3][0] + "`" + Math.ceil(this.playershipstatus[2][5]) + "`" + Math.ceil(this.playershipstatus[2][1]) + "`~");
            }
            this.func_RunOtherSips(_loc_2);
            this.shipXmovement = this.shipcoordinatex - this.lastshipcoordinatex;
            this.shipYmovement = this.shipcoordinatey - this.lastshipcoordinatey;
            this.lastshipcoordinatex = this.shipcoordinatex;
            this.lastshipcoordinatey = this.shipcoordinatey;
            this.func_MoveBackGroundStars();
            if (this.gamesetting.showbckgrnd)
            {
                if (this.gamesetting.scrollingbckgrnd)
                {
                    this.func_MoveBackground();
                }
            }
            this.gamebackground.x = -this.shipcoordinatex;
            this.gamebackground.y = -this.shipcoordinatey;
            this.func_UpdateRadarPosition(this.shipcoordinatex, this.shipcoordinatey);
            _loc_4 = "";
            _loc_4 = this.PlayerControlsScript(_loc_2);
            this.func_TurretControlScript(this.curentGametime);
            if (_loc_4 != "")
            {
                _loc_4 = _loc_4 + this.gunShotBufferData;
                this.mysocket.send(_loc_4);
                _loc_4 = "";
                this.gunShotBufferData = "";
            }
            else if (this.gunShotBufferData != "")
            {
                _loc_5 = "PI`FALSE`~" + this.gunShotBufferData;
                this.gunShotBufferData = "";
                this.mysocket.send(_loc_5);
            }
            this.PlayersShipShieldImage.rotation = this.PlayersShipImage.rotation;
            return;
        }// end function

        public function StageReFocusInHandler(event:FocusEvent) : void
        {
            this.func_RefocusToStage();
            return;
        }// end function

        function frame8()
        {
            if (this.MochiAdds)
            {
            }
            return;
        }// end function

        public function DisplayOnlinePlayersList()
        {
            var _loc_1:* = "";
            var _loc_2:* = 0;
            var _loc_3:* = 0;
            while (_loc_3 < this.currentonlineplayers.length)
            {
                
                if (this.currentonlineplayers[_loc_3][0] >= 0)
                {
                    _loc_2 = _loc_2 + 1;
                }
                _loc_3 = _loc_3 + 1;
            }
            _loc_1 = "";
            _loc_1 = _loc_1 + ("( O )Online System Players: " + _loc_2 + " (" + (this.currentonlineplayers.length - _loc_2) + ")  ");
            this.OnlinePLayerList.onlinePlayersListing.text = _loc_1;
            if (this.OnlinePLayerList.currentFrame == 2)
            {
                this.DisplayOnlinePlayersListDetails();
            }
            return;
        }// end function

        public function func_admincommands(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            var _loc_15:* = undefined;
            var _loc_16:* = undefined;
            var _loc_17:* = undefined;
            var _loc_18:* = undefined;
            var _loc_19:* = undefined;
            var _loc_20:* = undefined;
            _loc_2 = param1.toUpperCase();
            _loc_3 = "";
            _loc_4 = 0;
            _loc_5 = 0;
            _loc_6 = 0;
            if (_loc_2.substr(0, 6) == "/*KICK")
            {
                _loc_4 = this.func_namelocation(_loc_2.substr(7));
                if (_loc_4 != null)
                {
                    _loc_2 = "ADMIN`KICK`" + _loc_4 + "~";
                    this.mysocket.send(_loc_2);
                }
            }
            else if (_loc_2.substr(0, 6) == "/*NEWS")
            {
                if (this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "ADMIN")
                {
                    this.datatosend = "ADMIN`NEWS`MES`" + param1.substr(7) + "~";
                    this.mysocket.send(this.datatosend);
                }
            }
            else if (_loc_2.substr(0, 14) == "/*IPUSEDBYNAME")
            {
                _loc_3 = _loc_2.substr(15);
                if (_loc_3 != null)
                {
                    _loc_2 = "ADMIN`IPUSEDBYNAME`" + _loc_3 + "~";
                    this.mysocket.send(_loc_2);
                }
            }
            else if (_loc_2.substr(0, 15) == "/*NAMESUSEDBYIP")
            {
                _loc_3 = _loc_2.substr(16);
                if (_loc_3 != null)
                {
                    _loc_2 = "ADMIN`NAMESUSEDBYIP`" + _loc_3 + "~";
                    this.mysocket.send(_loc_2);
                }
            }
            else if (_loc_2.substr(0, 6) == "/*PING")
            {
                _loc_2 = "PING`" + getTimer() + "~";
                this.mysocket.send(_loc_2);
            }
            else if (_loc_2.substr(0, 13) == "/*DELETESQUAD")
            {
                _loc_3 = _loc_2.substr(14);
                if (_loc_3 != null)
                {
                    _loc_2 = "ADMIN`DELETESQUAD`" + _loc_3 + "~";
                    this.mysocket.send(_loc_2);
                }
            }
            else if (_loc_2.substr(0, 11) == "/*SQUADINFO")
            {
                _loc_3 = _loc_2.substr(12);
                if (_loc_3 != null)
                {
                    _loc_2 = "ADMIN`SQUADINFO`" + _loc_3 + "~";
                    this.mysocket.send(_loc_2);
                }
            }
            else if (_loc_2.substr(0, 6) == "/*MUTE")
            {
                _loc_4 = this.func_namelocation(_loc_2.substr(7));
                if (_loc_4 != null)
                {
                    _loc_2 = "ADMIN`MUTE`" + _loc_4 + "~";
                    this.mysocket.send(_loc_2);
                }
            }
            else if (_loc_2.substr(0, 7) == "/*ARENA")
            {
                _loc_15 = param1.substr(8);
                _loc_2 = "CHT~CH`" + this.playershipstatus[3][0] + "`AM`" + _loc_15 + "`" + "~";
                if (this.gamechatinfo[2][2] == false)
                {
                    this.mysocket.send(_loc_2);
                }
                else
                {
                    this.func_enterintochat(" You Are Muted ", this.systemchattextcolor);
                }
            }
            else if (_loc_2 == "/*STARTTIMER")
            {
                _loc_2 = "ADMIN`STARTTIMER`~";
                this.mysocket.send(_loc_2);
                this.func_enterintochat(" Started Timer ", this.systemchattextcolor);
            }
            else if (_loc_2 == "/*STOPTIMER")
            {
                _loc_2 = "ADMIN`STOPTIMER`~";
                this.mysocket.send(_loc_2);
                this.func_enterintochat(" Stopped Timer ", this.systemchattextcolor);
            }
            else if (_loc_2.substr(0, 11) == "/*ALLARENAS")
            {
                _loc_15 = param1.substr(12);
                _loc_2 = "CHT~CH`" + this.playershipstatus[3][0] + "`SVR`" + _loc_15 + "`" + "~";
                if (this.gamechatinfo[2][2] == false)
                {
                    this.mysocket.send(_loc_2);
                }
                else
                {
                    this.func_enterintochat(" You Are Muted ", this.systemchattextcolor);
                }
            }
            else if (_loc_2.substr(0, 7) == "/*FUNDS")
            {
                _loc_7 = 8;
                _loc_5 = 8;
                while (_loc_5 < _loc_2.length)
                {
                    
                    if (_loc_2.charAt(_loc_5) == " ")
                    {
                        _loc_6 = _loc_5 + 1;
                        _loc_8 = _loc_5;
                        break;
                    }
                    _loc_5 = _loc_5 + 1;
                }
                _loc_16 = Number(_loc_2.substr(_loc_7, _loc_8 - _loc_7));
                _loc_4 = this.func_namelocation(_loc_2.substr(_loc_6));
                if (_loc_4 != null && !isNaN(_loc_16))
                {
                    _loc_2 = "ADMIN`FUNDS`" + _loc_16 + "`" + _loc_4 + "~";
                    this.mysocket.send(_loc_2);
                }
            }
            else if (_loc_2.substr(0, 7) == "/*SCORE")
            {
                if (this.playershipstatus[5][12] == "ADMIN")
                {
                    _loc_17 = 8;
                    _loc_5 = 8;
                    while (_loc_5 < _loc_2.length)
                    {
                        
                        if (_loc_2.charAt(_loc_5) == " ")
                        {
                            _loc_6 = _loc_5 + 1;
                            _loc_19 = _loc_5;
                            break;
                        }
                        _loc_5 = _loc_5 + 1;
                    }
                    _loc_18 = Number(_loc_2.substr(_loc_17, _loc_19 - _loc_17));
                    _loc_4 = this.func_namelocation(_loc_2.substr(_loc_6));
                    if (_loc_4 != null && !isNaN(_loc_18))
                    {
                        _loc_2 = "ADMIN`" + _loc_4 + "`SCORE`" + _loc_18 + "~";
                        this.mysocket.send(_loc_2);
                    }
                }
            }
            else if (_loc_2.substr(0, 14) == "/*CREATEPIRATE")
            {
                if (this.playershipstatus[5][12] == "ADMIN")
                {
                }
            }
            else if (_loc_2.substr(0, 7) == "/*PRICE")
            {
            }
            else if (_loc_2.substr(0, 4) == "/*IP")
            {
                _loc_4 = this.func_namelocation(_loc_2.substr(5));
                if (_loc_4 != null)
                {
                    _loc_2 = "ADMIN`IP`" + _loc_4 + "~";
                    this.mysocket.send(_loc_2);
                }
            }
            else if (_loc_2.substr(0, 5) == "/*BAN")
            {
                if (this.playershipstatus[5][12] == "ADMIN" || this.playershipstatus[5][12] == "SMOD" || this.playershipstatus[5][12] == "MOD")
                {
                    _loc_17 = 6;
                    _loc_5 = 6;
                    while (_loc_5 < _loc_2.length)
                    {
                        
                        if (_loc_2.charAt(_loc_5) == " ")
                        {
                            _loc_6 = _loc_5 + 1;
                            _loc_19 = _loc_5;
                            break;
                        }
                        _loc_5 = _loc_5 + 1;
                    }
                    _loc_20 = Number(_loc_2.substr(_loc_17, _loc_19 - _loc_17));
                    _loc_4 = this.func_namelocation(_loc_2.substr(_loc_6));
                    if (_loc_4 != null && !isNaN(_loc_20))
                    {
                        _loc_2 = "ADMIN`" + _loc_4 + "`BAN`" + _loc_20 + "~";
                        this.mysocket.send(_loc_2);
                    }
                }
            }
            else if (_loc_2.substr(0, 7) == "/*UNBAN")
            {
                if (this.playershipstatus[5][12] == "ADMIN")
                {
                    _loc_6 = 8;
                    _loc_13 = _loc_2.substr(_loc_6);
                    _loc_2 = "ADMIN`UNBAN`" + _loc_13 + "`~";
                    this.mysocket.send(_loc_2);
                }
            }
            else if (_loc_2.substr(0, 6) == "/*INFO")
            {
                _loc_14 = _loc_2.substr(7);
                if (this.playershipstatus[5][12] == "ADMIN")
                {
                    this.datatosend = "ADMIN`INFO`" + _loc_14 + "`~";
                    this.mysocket.send(this.datatosend);
                }
            }
            else if (_loc_2.substr(0, 9) == "/*PROMOTE")
            {
                _loc_14 = _loc_2.substr(10);
                if (this.playershipstatus[5][12] == "ADMIN")
                {
                    this.datatosend = "ADMIN`PROMOTE`" + _loc_14 + "`~";
                    this.mysocket.send(this.datatosend);
                }
            }
            else if (_loc_2.substr(0, 8) == "/*DEMOTE")
            {
                _loc_14 = _loc_2.substr(9);
                if (this.playershipstatus[5][12] == "ADMIN")
                {
                    this.datatosend = "ADMIN`DEMOTE`" + _loc_14 + "`~";
                    this.mysocket.send(this.datatosend);
                }
            }
            else if (_loc_2.substr(0, 14) == "/*CREATEAIBASE")
            {
                if (this.playershipstatus[5][12] != "")
                {
                    this.datatosend = "CREATEAIBASE`~";
                    this.mysocket.send(this.datatosend);
                }
            }
            return;
        }// end function

        public function manualTurretsSelected(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_setTurrets("manual");
            return;
        }// end function

        public function NavigationTimerHandler(event:TimerEvent) : void
        {
            var event:* = event;
            try
            {
                this.func_updateNavigationDisplay();
            }
            catch (error:Error)
            {
                NavigationTimer.stop();
            }
            return;
        }// end function

        public function func_StarbaseLoadingData(param1)
        {
            var _loc_3:* = undefined;
            var _loc_2:* = Number(param1[9]) - this.starbaseLevel;
            if (this.starbaselocation[_loc_2] == null)
            {
                this.starbaselocation[_loc_2] = new Array();
            }
            this.starbaselocation[_loc_2][0] = param1[0];
            this.starbaselocation[_loc_2][1] = Number(param1[1].substr("1"));
            this.starbaselocation[_loc_2][2] = Number(param1[2].substr("1"));
            this.starbaselocation[_loc_2][3] = int(param1[3].substr("1"));
            this.starbaselocation[_loc_2][4] = int(param1[4].substr("1"));
            this.starbaselocation[_loc_2][9] = 0;
            this.starbaselocation[_loc_2][11] = Number(param1[6]);
            this.starbaselocation[_loc_2][12] = Number(param1[7]);
            this.starbaselocation[_loc_2][13] = 400;
            this.starbaselocation[_loc_2][14] = 25000;
            this.starbaselocation[_loc_2][15] = 250000;
            this.starbaselocation[_loc_2][16] = param1[10];
            this.starbaselocation[_loc_2][19] = Number(param1[11]);
            if (this.starbaselocation[_loc_2][22] == null)
            {
                this.starbaselocation[_loc_2][22] = new this.RadarBlot() as MovieClip;
            }
            this.func_StarbaseDraw(_loc_2);
            this.starbaselocation[_loc_2][5] = Number(param1[5]) * 1000 + getTimer();
            if (Number(param1[5]) == 0)
            {
                this.starbaselocation[_loc_2][5] = "ACTIVE";
            }
            else
            {
                _loc_3 = new Array();
                _loc_3[2] = _loc_2 + this.starbaseLevel;
                this.func_StarbaseDestroyed(_loc_3);
            }
            return;
        }// end function

        public function disruptplayersengines()
        {
            this.isupkeypressed = false;
            this.isdownkeypressed = true;
            this.isshiftkeypressed = false;
            this.keywaspressed = true;
            return;
        }// end function

        public function currentkeyedshipmovement()
        {
            var _loc_2:* = undefined;
            var _loc_1:* = "";
            if (this.isrightkeypressed == true)
            {
                _loc_1 = _loc_1 + "R";
            }
            if (this.isleftkeypressed == true)
            {
                _loc_1 = _loc_1 + "L";
            }
            if (_loc_1 == "RL")
            {
                _loc_1 = "";
            }
            if (this.isshiftkeypressed == true)
            {
                _loc_1 = _loc_1 + "S";
            }
            else
            {
                _loc_2 = "";
                if (this.isupkeypressed == true)
                {
                    _loc_2 = _loc_2 + "U";
                }
                if (this.isdownkeypressed == true)
                {
                    _loc_2 = _loc_2 + "D";
                }
                if (_loc_2 != "UD")
                {
                    _loc_1 = _loc_1 + _loc_2;
                }
            }
            return _loc_1;
        }// end function

        public function func_namelocation(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            _loc_2 = null;
            param1 = param1.toUpperCase();
            _loc_3 = 0;
            while (_loc_3 < this.currentonlineplayers.length)
            {
                
                if (param1 == this.currentonlineplayers[_loc_3][1].toUpperCase())
                {
                    _loc_2 = this.currentonlineplayers[_loc_3][0];
                    break;
                }
                _loc_3 = _loc_3 + 1;
            }
            return _loc_2;
        }// end function

        public function addSquadBaseToGameBackGround(param1)
        {
            var imageLink:* = param1;
            trace("added SquadBasetobackground");
            try
            {
                this.gamebackground.addChild(imageLink);
                trace(imageLink.x);
                trace(imageLink.y);
            }
            catch (error:Error)
            {
                trace("Error adding squadbase: ");
            }
            return;
        }// end function

        public function func_cloakplayership(param1, param2, param3)
        {
            this.playerSpecialsSettings.isCloaked = true;
            this.playerSpecialsSettings.CloakEnergy = param1;
            this.playerSpecialsSettings.CloakLocation = param3;
            this.playershipstatus[5][15] = "C" + param2;
            this.PlayersShipImage.alpha = 0.5;
            this.keywaspressed = true;
            this.func_StealthTheRadar();
            return;
        }// end function

        public function ChatWindowFocusOut(event:FocusEvent) : void
        {
            this.func_RefreshChat();
            return;
        }// end function

        public function GameKeyListenerKeyPress(event:KeyboardEvent) : void
        {
            var e:* = event;
            var OutComeReturn:*;
            var keypressed:* = e.keyCode;
            if (this.isPlayerChatting)
            {
            }
            else
            {
                this.chatDisplay.chatInput.text = "";
                if (keypressed == this.gamesetting.accelkey)
                {
                    if (this.isupkeypressed != OutComeReturn)
                    {
                        if (!this.isplayerdisrupt)
                        {
                            this.isupkeypressed = OutComeReturn;
                            this.keywaspressed = true;
                        }
                    }
                }
                else if (keypressed == this.gamesetting.turnleftkey)
                {
                    if (this.isleftkeypressed != OutComeReturn)
                    {
                        this.isleftkeypressed = OutComeReturn;
                        this.keywaspressed = true;
                    }
                }
                else if (keypressed == this.gamesetting.turnrightkey)
                {
                    if (this.isrightkeypressed != OutComeReturn)
                    {
                        this.isrightkeypressed = OutComeReturn;
                        this.keywaspressed = true;
                    }
                }
                else if (keypressed == this.gamesetting.deaccelkey)
                {
                    if (this.isdownkeypressed != OutComeReturn)
                    {
                        this.isdownkeypressed = OutComeReturn;
                        this.keywaspressed = true;
                    }
                }
                else if (keypressed == this.gamesetting.afterburnerskey)
                {
                    if (this.isshiftkeypressed != OutComeReturn)
                    {
                        if (!this.isplayerdisrupt)
                        {
                            this.isshiftkeypressed = OutComeReturn;
                            this.keywaspressed = true;
                        }
                    }
                }
                else if (keypressed == this.gamesetting.gunskey)
                {
                    this.iscontrolkeypressed = OutComeReturn;
                }
                else if (keypressed == this.gamesetting.missilekey)
                {
                    this.isspacekeypressed = OutComeReturn;
                }
                else if (keypressed == this.gamesetting.dockkey)
                {
                    this.isdkeypressed = OutComeReturn;
                }
                else if (Number(keypressed) >= 49 && Number(keypressed) <= 57)
                {
                    try
                    {
                        this.func_Special_Selected(Number(keypressed) - 48);
                    }
                    catch (error:Error)
                    {
                    }
                }
            }
            return;
        }// end function

        public function func_RequestStats(param1)
        {
            var _loc_2:* = undefined;
            if (this.playershipstatus[3][0] != param1)
            {
                _loc_2 = "STATS`TGT`GET`" + this.playershipstatus[3][0] + "`" + param1 + "`~";
                this.mysocket.send(_loc_2);
            }
            return;
        }// end function

        public function func_InitializeClockChecks()
        {
            this.loginmovie.mov_login.logindisplay.currentstatus.text = this.loginmovie.mov_login.logindisplay.currentstatus.text + "\r Synchronizing with server \r ";
            addEventListener(Event.ENTER_FRAME, this.func_ClockSynchronizeScript);
            return;
        }// end function

        public function deathsgamerewards()
        {
            return 0;
        }// end function

        public function func_globalTimeStamp(param1)
        {
            var _loc_2:* = undefined;
            if (param1 < 1)
            {
                _loc_2 = String(getTimer() + this.clocktimediff);
            }
            else
            {
                _loc_2 = String(param1 + this.clocktimediff);
            }
            _loc_2 = Number(_loc_2.substr(_loc_2.length - 4));
            if (_loc_2 > 10000)
            {
                _loc_2 = _loc_2 - 10000;
            }
            return String(_loc_2);
        }// end function

        public function func_RedrawStarbaseNameTag(param1)
        {
            var _loc_2:* = this.NeutralBasetextcolor;
            var _loc_3:* = "";
            if (this.starbaselocation[param1][16] == "-2")
            {
                _loc_2 = this.NeutralBasetextcolor;
            }
            else if (this.starbaselocation[param1][16] == this.playershipstatus[5][2])
            {
                _loc_2 = this.FriendlyBasetextcolor;
                _loc_3 = " (" + this.starbaselocation[param1][19] + ")";
            }
            else
            {
                _loc_2 = this.EnemyBaseTextColor;
                _loc_3 = " (" + this.starbaselocation[param1][19] + ")";
            }
            var _loc_4:* = this.starbaselocation[param1][0];
            if (_loc_4.substr(0, 2) == "PL")
            {
                _loc_4 = _loc_4.substr(2);
                _loc_3 = "";
            }
            this.starbaselocation[param1][21].BaseLabel.htmlText = "<font color=\"" + _loc_2 + "\">" + _loc_4 + _loc_3 + "</FONT>\r";
            return;
        }// end function

        public function displaystats()
        {
            var _loc_1:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.visible = true;
            if (this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] >= 0 && this.playershipstatus[5][2] < this.teambases.length)
            {
                this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.textinfo.text = "CHANGE TEAM";
            }
            else
            {
                this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.textinfo.text = "GO ON TEAM";
            }
            _loc_1 = "";
            var _loc_2:* = "";
            _loc_1 = _loc_1 + ("Teams: " + this.teambases.length + "\r");
            _loc_3 = new Array();
            _loc_4 = new Array();
            _loc_5 = 0;
            while (_loc_5 < this.teambases.length)
            {
                
                _loc_3[_loc_5] = 0;
                _loc_4[_loc_5] = 0;
                _loc_5 = _loc_5 + 1;
            }
            _loc_5 = 0;
            while (_loc_5 < this.currentonlineplayers.length)
            {
                
                if (this.currentonlineplayers[_loc_5][4] != "N/A" && this.currentonlineplayers[_loc_5][4] >= 0 && this.currentonlineplayers[_loc_5][4] < this.teambases.length)
                {
                    if (this.currentonlineplayers[_loc_5][0] >= 0)
                    {
                        (_loc_3[this.currentonlineplayers[_loc_5][4]] + 1);
                    }
                    else
                    {
                        (_loc_4[this.currentonlineplayers[_loc_5][4]] + 1);
                    }
                }
                _loc_5 = _loc_5 + 1;
            }
            if (this.squadwarinfo[0] == true)
            {
                this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.visible = false;
                _loc_1 = _loc_1 + "Team / Squad Size / Base Structure\r";
                _loc_5 = 0;
                while (_loc_5 < this.teambases.length)
                {
                    
                    _loc_1 = _loc_1 + (" " + this.teambases[_loc_5][0].substr(2) + "  /  " + this.squadwarinfo[1][_loc_5] + ": " + _loc_3[_loc_5] + "  /  " + this.teambases[_loc_5][10] + "\r");
                    _loc_5 = _loc_5 + 1;
                }
                if (this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] >= 0)
                {
                    this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.yourteam.text = "Your Team: " + this.teambases[this.playershipstatus[5][2]][0].substr(2) + "  /  " + this.squadwarinfo[1][this.playershipstatus[5][2]] + "\r";
                }
                else
                {
                    this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.yourteam.text = "Your Team: N/A";
                }
            }
            else
            {
                this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.butt.visible = true;
                _loc_1 = _loc_1 + "Team Size / Base Structure\r";
                _loc_5 = 0;
                while (_loc_5 < this.teambases.length)
                {
                    
                    _loc_1 = _loc_1 + (" " + this.teambases[_loc_5][0].substr(2) + ": " + _loc_3[_loc_5] + " (" + _loc_4[_loc_5] + " ai)  " + this.teambases[_loc_5][10] + "\r");
                    _loc_5 = _loc_5 + 1;
                }
                if (this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] >= 0 && this.playershipstatus[5][2] < this.teambases.length)
                {
                    this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.yourteam.text = "Your Team: " + this.teambases[this.playershipstatus[5][2]][0].substr(2) + "\r";
                }
                else
                {
                    this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.yourteam.text = "Your Team: N/A";
                }
            }
            this.main_docked_screen.dockedbackground.docked_image_holder.zoneinterface.information.text = _loc_1;
            return;
        }// end function

        public function loadsquadbases()
        {
            this.mysocket.send("LOADSYSTEMSQUADBASES`~");
            return;
        }// end function

        public function loadAllBasesintoBackGround()
        {
            var _loc_1:* = undefined;
            _loc_1 = 0;
            while (_loc_1 < this.playersquadbases.length)
            {
                
                if (this.playersquadbases[_loc_1] != null)
                {
                    if (this.playersquadbases[_loc_1][12] != null)
                    {
                        this.addSquadBaseToGameBackGround(this.playersquadbases[_loc_1][12]);
                    }
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_InGameHelp_Click(event:MouseEvent) : void
        {
            this.func_TriggerGameHelp();
            return;
        }// end function

        public function func_updateSpecialsDisplay()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            _loc_1 = getTimer();
            _loc_2 = 1;
            while (_loc_2 < (this.playershipstatus[11][1].length + 1))
            {
                
                _loc_3 = _loc_2 - 1;
                if (this.playershipstatus[11][2][_loc_3][0] == -1)
                {
                }
                else if (this.playershipstatus[11][2][_loc_3][0] == 0)
                {
                    this.specialsingame["sp" + _loc_2].specialinfodata.text = "";
                }
                else if (this.playershipstatus[11][2][_loc_3][0] > _loc_1)
                {
                    this.specialsingame["sp" + _loc_2].specialinfodata.text = Math.ceil((this.playershipstatus[11][2][_loc_3][0] - _loc_1) / 1000);
                }
                else
                {
                    _loc_4 = this.specialshipitems[this.playershipstatus[11][1][_loc_3][0]][5];
                    if (_loc_4 == "FLARE" || _loc_4 == "RECHARGESHIELD" || _loc_4 == "RECHARGESTRUCT" || _loc_4 == "MINES" || _loc_4 == "CLOAK" || _loc_4 == "STEALTH")
                    {
                        this.specialsingame["sp" + _loc_2].specialinfodata.text = "";
                        this.specialsingame["sp" + _loc_2].specialbutton.gotoAndStop("OFF");
                        this.playershipstatus[11][2][_loc_3][0] = 0;
                    }
                    if (_loc_4 == "DETECTOR")
                    {
                        if (this.specialsingame["sp" + _loc_2].specialbutton.setting == "RELOAD")
                        {
                            this.playershipstatus[11][2][_loc_3][0] = 0;
                            this.specialsingame["sp" + _loc_2].specialinfodata.text = "";
                            this.specialsingame["sp" + _loc_2].specialbutton.gotoAndStop("OFF");
                        }
                        else
                        {
                            this.playershipstatus[11][2][_loc_3][0] = this.specialshipitems[this.playershipstatus[11][1][_loc_3][0]][11] + _loc_1;
                            this.specialsingame["sp" + _loc_2].specialinfodata.text = "SCAN";
                            this.func_detectorPing(this.specialshipitems[this.playershipstatus[11][1][_loc_3][0]][10]);
                        }
                    }
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_fireTurretsAt(param1, param2)
        {
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            _loc_3 = "";
            _loc_4 = 0;
            while (_loc_4 < this.playershipstatus[8].length)
            {
                
                if (this.playershipstatus[8][_loc_4][1] <= param2 && this.playershipstatus[1][1] > this.playershipstatus[8][_loc_4][5] && this.playershipstatus[8][_loc_4][0] != "none" && this.playershipstatus[8][_loc_4][4] == "ON")
                {
                    if (this.playershipstatus[5][15] == "C")
                    {
                    }
                    this.playershipstatus[1][1] = this.playershipstatus[1][1] - this.playershipstatus[8][_loc_4][5];
                    _loc_5 = this.playershipstatus[8][_loc_4][0];
                    _loc_6 = this.playershipstatus[8][_loc_4][7];
                    _loc_7 = this.PlayersShipImage.rotation;
                    _loc_8 = _loc_6;
                    _loc_7 = param1;
                    _loc_9 = this.MovingObjectWithThrust(_loc_7, _loc_8);
                    _loc_10 = _loc_9[0];
                    _loc_11 = _loc_9[1];
                    _loc_12 = this.playershipstatus[8][_loc_4][2];
                    _loc_13 = this.playershipstatus[8][_loc_4][3];
                    _loc_14 = this.firingbulletstartlocation(_loc_12, _loc_13, this.PlayersShipImage.rotation);
                    _loc_12 = _loc_14[0] + this.shipcoordinatex;
                    _loc_13 = _loc_14[1] + this.shipcoordinatey;
                    this.playershipstatus[8][_loc_4][1] = getTimer() + this.guntype[_loc_5][2] * 1000;
                    _loc_3 = _loc_3 + this.func_fire_a_gunshot(_loc_5, _loc_12, _loc_13, _loc_10, _loc_11, _loc_7, _loc_8, param2);
                    this.func_fireGunSound(_loc_5, _loc_12, _loc_13);
                }
                _loc_4 = _loc_4 + 1;
            }
            this.gunShotBufferData = this.gunShotBufferData + _loc_3;
            return;
        }// end function

        public function func_runFireScript(param1)
        {
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_2:* = "";
            var _loc_3:* = 0;
            while (_loc_3 < this.playershipstatus[0].length)
            {
                
                if (this.playershipstatus[0][_loc_3][1] <= param1 && this.playershipstatus[1][1] > this.playershipstatus[0][_loc_3][5] && this.playershipstatus[0][_loc_3][0] != "none" && this.playershipstatus[0][_loc_3][4] == "ON")
                {
                    if (this.playerSpecialsSettings.isCloaked)
                    {
                        this.func_turnofshipcloak();
                    }
                    this.playershipstatus[1][1] = this.playershipstatus[1][1] - this.playershipstatus[0][_loc_3][5];
                    _loc_4 = this.playershipstatus[0][_loc_3][0];
                    _loc_5 = this.playershipstatus[0][_loc_3][7];
                    _loc_6 = this.PlayersShipImage.rotation;
                    _loc_7 = this.playershipvelocity + _loc_5;
                    _loc_8 = this.MovingObjectWithThrust(_loc_6, _loc_7);
                    _loc_9 = _loc_8[0];
                    _loc_10 = _loc_8[1];
                    _loc_11 = this.playershipstatus[0][_loc_3][2];
                    _loc_12 = this.playershipstatus[0][_loc_3][3];
                    _loc_13 = this.firingbulletstartlocation(_loc_11, _loc_12, _loc_6);
                    _loc_11 = _loc_13[0] + this.shipcoordinatex;
                    _loc_12 = _loc_13[1] + this.shipcoordinatey;
                    this.playershipstatus[0][_loc_3][1] = getTimer() + this.guntype[_loc_4][2] * 1000;
                    _loc_2 = _loc_2 + this.func_fire_a_gunshot(_loc_4, _loc_11, _loc_12, _loc_9, _loc_10, _loc_6, _loc_7, param1);
                    this.func_fireGunSound(_loc_4, _loc_11, _loc_12);
                }
                _loc_3 = _loc_3 + 1;
            }
            this.gunShotBufferData = this.gunShotBufferData + _loc_2;
            return;
        }// end function

        public function func_checkoutgoingcharacters(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            _loc_2 = 0;
            while (_loc_2 < param1.length)
            {
                
                _loc_3 = 0;
                while (_loc_3 < this.replacewithchar.length)
                {
                    
                    if (param1.substr(_loc_2, this.replacewithchar[_loc_3][1].length) == this.replacewithchar[_loc_3][1])
                    {
                        trace("FOUND:" + this.replacewithchar[_loc_3][1]);
                        _loc_4 = param1.substr(0, _loc_2);
                        _loc_5 = param1.substr(_loc_2 + this.replacewithchar[_loc_3][1].length);
                        param1 = _loc_4 + this.replacewithchar[_loc_3][0] + _loc_5;
                        _loc_3 = 999;
                    }
                    _loc_3 = _loc_3 + 1;
                }
                _loc_2 = _loc_2 + 1;
            }
            return param1;
        }// end function

        public function setMouseIsUp(event:MouseEvent) : void
        {
            this.TurretMouseDown = false;
            return;
        }// end function

        public function func_RunOtherSips(param1)
        {
            var _loc_2:* = getTimer();
            this.ProcessOtherShipKeys(_loc_2);
            this.func_moveAnothership(param1);
            this.func_RefreshOtherShipsImages();
            return;
        }// end function

        public function func_bringinspecial(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            trace("special:" + param1);
            _loc_2 = param1[1];
            _loc_3 = param1[2];
            _loc_4 = this.specialshipitems[_loc_3][5];
            if (_loc_4 == "RECHARGESHIELD")
            {
                _loc_5 = 0;
                while (_loc_5 < this.otherplayership.length)
                {
                    
                    if (_loc_2 == this.otherplayership[_loc_5][0])
                    {
                        this.otherplayership[_loc_5][41] = Number(param1[3]);
                        break;
                    }
                    _loc_5 = _loc_5 + 1;
                }
            }
            else if (_loc_4 == "RECHARGESTRUCT")
            {
                _loc_5 = 0;
                while (_loc_5 < this.otherplayership.length)
                {
                    
                    if (_loc_2 == this.otherplayership[_loc_5][0])
                    {
                        this.otherplayership[_loc_5][40] = Number(param1[3]);
                        break;
                    }
                    _loc_5 = _loc_5 + 1;
                }
            }
            return;
        }// end function

        public function fund_OnlineListToggled()
        {
            if (this.currentonlineplayers.length > 0)
            {
                this.OnlinePLayerList.playershipstatus = this.playershipstatus;
                this.OnlinePLayerList.mysocket = this.mysocket;
                trace(this.OnlinePLayerList.currentFrame);
                if (this.OnlinePLayerList.currentFrame == 1)
                {
                    this.OnlinePLayerList.gotoAndStop(2);
                    this.OnlinePLayerList.minimizeButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_OnlinePlayersMinimize_Click);
                    if (this.playershipstatus[5][4] == "alive" || this.teamdeathmatch || this.ZonePlayerIsIn == "100")
                    {
                        this.OnlinePLayerList.changeWindow.gotoAndStop(2);
                    }
                    else
                    {
                        this.OnlinePLayerList.changeWindow.gotoAndStop(1);
                    }
                }
                else if (this.OnlinePLayerList.currentFrame == 2)
                {
                    this.OnlinePLayerList.gotoAndStop(1);
                }
                this.func_RefocusToStage();
            }
            this.func_refreshCurrentOnlineList();
            return;
        }// end function

        public function func_checkCargoMissionsFinish(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            _loc_2 = 0;
            while (_loc_2 < this.playersCurrentMissions.length)
            {
                
                if (this.playersCurrentMissions[_loc_2].MissionType == "Cargo")
                {
                    if (param1 == this.playersCurrentMissions[_loc_2].Locations[0].MarkerName)
                    {
                        _loc_3 = Number(this.playersCurrentMissions[_loc_2].FundsReward);
                        if (!isNaN(_loc_3))
                        {
                            this.playershipstatus[3][1] = this.playershipstatus[3][1] + _loc_3;
                            this.func_enterintochat("Completed: " + this.playersCurrentMissions[_loc_2].MissionName + " for " + _loc_3, this.privatechattextcolor);
                            this.playersCurrentMissions.splice(_loc_2, 1);
                            _loc_2 = _loc_2 - 1;
                        }
                    }
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_shipExplosionSound(param1, param2)
        {
            if (this.gamesetting.CombatSounds)
            {
                if (Math.abs(this.shipcoordinatex - param1) < 800)
                {
                    if (Math.abs(this.shipcoordinatey - param2) < 800)
                    {
                        this.gamesetting.IngameChannelSound = this.gameSounds.shipExplosion.play();
                        this.gamesetting.IngameChannelSound.soundTransform = this.gamesetting.IngameSoundTransform;
                    }
                }
            }
            return;
        }// end function

        public function buttonMouseOut(event:MouseEvent) : void
        {
            this.func_setSelectedIconMessage("");
            return;
        }// end function

        public function func_StarbaseDraw(param1)
        {
            var tempImageHolder:*;
            var sbLoc:* = param1;
            if (this.starbaselocation[sbLoc] != null)
            {
                if (this.starbaselocation[sbLoc][21] != null)
                {
                    try
                    {
                        this.gamebackground.removeChild(this.starbaselocation[sbLoc][21]);
                    }
                    catch (error:Error)
                    {
                        trace("Starbase Not Removed on Death");
                    }
                }
                this.starbaselocation[sbLoc][5] = "ACTIVE";
                if (this.starbaselocation[sbLoc][0].substr(0, 2) == "SB")
                {
                    tempImageHolder = getDefinitionByName("starbasetype" + this.starbaselocation[sbLoc][3]) as Class;
                    this.starbaselocation[sbLoc][21] = new tempImageHolder as MovieClip;
                }
                else
                {
                    tempImageHolder = getDefinitionByName("planettype" + this.starbaselocation[sbLoc][3]) as Class;
                    this.starbaselocation[sbLoc][21] = new tempImageHolder as MovieClip;
                }
                this.starbaselocation[sbLoc][21].x = this.starbaselocation[sbLoc][1];
                this.starbaselocation[sbLoc][21].y = this.starbaselocation[sbLoc][2];
                this.starbaselocation[sbLoc][23] = new shiphealthbar() as MovieClip;
                this.starbaselocation[sbLoc][21].addChild(this.starbaselocation[sbLoc][23]);
                this.starbaselocation[sbLoc][23].func_setLifeSettings(1, 0);
                this.starbaselocation[sbLoc][23].y = -30;
                try
                {
                    this.gamebackground.addChild(this.starbaselocation[sbLoc][21]);
                }
                catch (error:Error)
                {
                    trace("Starbase Image Not Added to game background (may not be at proper screen)");
                }
                this.func_RedrawStarbaseNameTag(sbLoc);
            }
            return;
        }// end function

        public function chatfocusInHandler(event:FocusEvent) : void
        {
            stage.focus = this.chatDisplay.chatInput;
            this.isPlayerChatting = true;
            this.chatDisplay.textoutline.gotoAndStop(2);
            this.func_resetFlightKeys();
            return;
        }// end function

        public function func_updateNavigationDisplay()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = undefined;
            if (this.playersdestination[0] == "N")
            {
                this.NavigationImage.visible = false;
            }
            else if (this.playersdestination[0] >= 0)
            {
                _loc_1 = Math.round(Math.sqrt(Math.pow(this.shipcoordinatex - this.playersdestination[1], 2) + Math.pow(this.shipcoordinatey - this.playersdestination[2], 2)));
                if (_loc_1 > 300)
                {
                    this.NavigationImage.visible = true;
                    _loc_2 = 180 - Math.atan2(this.playersdestination[1] - this.shipcoordinatex, this.playersdestination[2] - this.shipcoordinatey) / (Math.PI / 180);
                    this.NavigationImage.rotation = _loc_2;
                    this.NavigationImage.navText.rotation = -this.NavigationImage.rotation;
                    this.NavigationImage.navText.directionInfo.text = this.playersdestination[4] + "\r" + _loc_1;
                }
                else
                {
                    this.NavigationImage.visible = false;
                }
            }
            else
            {
                this.NavigationImage.visible = false;
            }
            return;
        }// end function

        public function func_ignorelist(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            _loc_2 = true;
            _loc_3 = 0;
            while (_loc_3 < this.gamechatinfo[6].length)
            {
                
                if (param1 == this.gamechatinfo[6][_loc_3])
                {
                    _loc_2 = false;
                    this.gamechatinfo[6].splice(_loc_3, 1);
                    this.func_enterintochat(" Removing From Ignore List: " + param1, this.systemchattextcolor);
                    break;
                }
                _loc_3 = _loc_3 + 1;
            }
            if (_loc_2 == true)
            {
                _loc_4 = this.gamechatinfo[6].length;
                this.gamechatinfo[6][_loc_4] = param1;
                this.func_enterintochat(" Adding To Ignore List: " + param1, this.systemchattextcolor);
            }
            return;
        }// end function

        public function func_checkReconAndPatrolStatus(param1, param2)
        {
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            _loc_3 = 0;
            while (_loc_3 < this.playersCurrentMissions.length)
            {
                
                if (this.playersCurrentMissions[_loc_3].MissionType == "Recon" || this.playersCurrentMissions[_loc_3].MissionType == "Patrol")
                {
                    _loc_4 = this.playersCurrentMissions[_loc_3].Locations[0].Xcoord;
                    _loc_5 = this.playersCurrentMissions[_loc_3].Locations[0].Ycoord;
                    _loc_6 = this.playersCurrentMissions[_loc_3].Locations[0].MarkerName;
                    _loc_7 = Math.sqrt((_loc_4 - param1) * (_loc_4 - param1) + (_loc_5 - param2) * (_loc_5 - param2));
                    if (_loc_7 < this.MissionRangeFromNavpoint)
                    {
                        if (this.playersCurrentMissions[_loc_3].Locations.length < 2)
                        {
                            _loc_8 = Number(this.playersCurrentMissions[_loc_3].FundsReward);
                            if (!isNaN(_loc_8))
                            {
                                this.playershipstatus[3][1] = this.playershipstatus[3][1] + _loc_8;
                                this.func_enterintochat("Completed: " + this.playersCurrentMissions[_loc_3].MissionName + " for " + _loc_8, this.privatechattextcolor);
                                this.playersCurrentMissions[_loc_3].Locations.splice(0, 1);
                                this.func_MissionsInformationUpdate();
                                this.playersCurrentMissions.splice(_loc_3, 1);
                                _loc_3 = _loc_3 - 1;
                            }
                        }
                        else
                        {
                            this.playersCurrentMissions[_loc_3].Locations.splice(0, 1);
                            this.func_MissionsInformationUpdate();
                            this.func_enterintochat("Updated: " + this.playersCurrentMissions[_loc_3].MissionName + " proceed to " + this.playersCurrentMissions[_loc_3].Locations[0].MarkerName, this.privatechattextcolor);
                        }
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            return;
        }// end function

        public function MovingObjectWithThrust(param1, param2)
        {
            var _loc_3:* = new Array();
            param1 = param1 * (Math.PI / 180);
            if (param1 == 0)
            {
                _loc_3[1] = -param2;
                _loc_3[0] = 0;
            }
            if (param2 != 0)
            {
                _loc_3[1] = -param2 * Math.cos(param1);
                _loc_3[0] = param2 * Math.sin(param1);
            }
            else
            {
                _loc_3[0] = 0;
                _loc_3[1] = 0;
            }
            return _loc_3;
        }// end function

        public function func_EnableChatPress()
        {
            if (this.chatDisplay.visible)
            {
                if (!this.isPlayerChatting)
                {
                    this.isPlayerChatting = true;
                    this.func_resetFlightKeys();
                    stage.focus = this.chatDisplay.chatInput;
                }
                else
                {
                    this.func_SubmitChat();
                    this.isPlayerChatting = false;
                    stage.focus = stage;
                    this.chatDisplay.chatInput.text = "";
                }
            }
            return;
        }// end function

        public function func_UpdateRadarPosition(param1, param2)
        {
            this.gameRadar.radarScreen.x = (-param1) * 0.05;
            this.gameRadar.radarScreen.y = (-param2) * 0.05;
            return;
        }// end function

        public function UpdatePing()
        {
            var _loc_2:* = undefined;
            var _loc_1:* = getTimer();
            if (_loc_1 > this.lastpingcheck - this.pingintervalcheck + 1600)
            {
                _loc_2 = "PING`" + _loc_1 + "~";
                this.mysocket.send(_loc_2);
                this.remoteupdate = true;
                this.lastpingcheck = this.lastpingcheck + this.pingintervalcheck;
            }
            return;
        }// end function

        public function func_initializePlayersship()
        {
            var _loc_4:* = undefined;
            this.playershipstatus[5][27] = false;
            var _loc_1:* = this.playershipstatus[5][0];
            this.playershipstatus[5][4] = "alive";
            this.playershiprotation = this.shiptype[_loc_1][3][2];
            this.playershipmaxvelocity = this.shiptype[_loc_1][3][1];
            this.playershipafterburnerspeed = this.shiptype[_loc_1][3][6];
            this.playershipacceleration = this.shiptype[_loc_1][3][0];
            this.playershipvelocity = 0;
            var _loc_2:* = 0;
            while (_loc_2 < this.playershipstatus[0].length)
            {
                
                _loc_4 = this.playershipstatus[0][_loc_2][0];
                if (isNaN(_loc_4))
                {
                    _loc_4 = 0;
                }
                this.playershipstatus[0][_loc_2][1] = 0;
                this.playershipstatus[0][_loc_2][2] = this.shiptype[this.playershipstatus[5][0]][2][_loc_2][0];
                this.playershipstatus[0][_loc_2][3] = this.shiptype[this.playershipstatus[5][0]][2][_loc_2][1];
                this.playershipstatus[0][_loc_2][4] = "ON";
                this.playershipstatus[0][_loc_2][5] = this.guntype[_loc_4][3];
                this.playershipstatus[0][_loc_2][6] = this.guntype[_loc_4][2] * 1000;
                this.playershipstatus[0][_loc_2][7] = Math.round(this.guntype[_loc_4][0]);
                this.playershipstatus[0][_loc_2][8] = this.guntype[_loc_4][1] * 1000;
                _loc_2 = _loc_2 + 1;
            }
            var _loc_3:* = 0;
            while (_loc_3 < this.playershipstatus[8].length)
            {
                
                _loc_4 = this.playershipstatus[8][_loc_3][0];
                if (isNaN(_loc_4))
                {
                    _loc_4 = 0;
                }
                this.playershipstatus[8][_loc_3][1] = 0;
                this.playershipstatus[8][_loc_3][2] = this.shiptype[this.playershipstatus[5][0]][5][_loc_3][0];
                this.playershipstatus[8][_loc_3][3] = this.shiptype[this.playershipstatus[5][0]][5][_loc_3][1];
                this.playershipstatus[8][_loc_3][4] = "ON";
                this.playershipstatus[8][_loc_3][5] = this.guntype[_loc_4][3];
                this.playershipstatus[8][_loc_3][6] = this.guntype[_loc_4][2] * 1000;
                this.playershipstatus[8][_loc_3][7] = Math.round(this.guntype[_loc_4][0]);
                this.playershipstatus[8][_loc_3][8] = this.guntype[_loc_4][1] * 1000;
                _loc_3 = _loc_3 + 1;
            }
            this.playershipstatus[1][1] = this.energycapacitors[this.playershipstatus[1][5]][0];
            this.playershipstatus[2][1] = this.shieldgenerators[this.playershipstatus[2][0]][0];
            this.playersmaxstructure = this.shiptype[this.playershipstatus[5][0]][3][3];
            this.playershipstatus[2][5] = this.playersmaxstructure;
            this.playershipstatus[2][6] = 0;
            this.playershipstatus[2][2] = "FULL";
            return;
        }// end function

        public function ChatFinishListenerKeyRelease(event:KeyboardEvent) : void
        {
            var _loc_2:* = undefined;
            if (this.isPlayerChatting)
            {
                _loc_2 = event.keyCode;
                if (_loc_2 == this.EnterChatKey)
                {
                    this.func_EnableChatPress();
                }
                else if (this.isPlayerChatting)
                {
                    this.func_KeyReleaseWhileChatting();
                }
            }
            else
            {
                stage.focus = stage;
            }
            return;
        }// end function

        public function optionsMouseOver(event:MouseEvent) : void
        {
            this.func_setSelectedIconMessage("options");
            return;
        }// end function

        public function func_displayspecials(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            _loc_2 = param1 - 1;
            _loc_3 = this.playershipstatus[11][1][_loc_2][0];
            this.specialsingame["sp" + param1].specialdisplay.text = this.specialshipitems[_loc_3][0];
            if (this.playershipstatus[11][1][_loc_2][1] >= 0)
            {
                this.specialsingame["sp" + param1].specialdisplay.text = this.specialsingame["sp" + param1].specialdisplay.text + (" (" + this.playershipstatus[11][1][_loc_2][1] + ")");
            }
            return;
        }// end function

        public function func_LoadZonesSystemMap(param1)
        {
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_2:* = param1.split("~");
            var _loc_3:* = 0;
            var _loc_4:* = "";
            this.sectormapitems = new Array();
            this.starbaselocation = new Array();
            this.teambases = new Array();
            while (_loc_3 < (_loc_2.length - 1))
            {
                
                if (_loc_2[_loc_3].substr(0, 2) == "SB" || _loc_2[_loc_3].substr(0, 2) == "PL")
                {
                    this.sectormapitems[_loc_3] = new Array();
                    _loc_4 = _loc_2[_loc_3].split("`");
                    _loc_6 = Number(_loc_4[9]) - this.starbaseLevel;
                    this.sectormapitems[_loc_3][0] = _loc_4[0];
                    this.sectormapitems[_loc_3][1] = Number(_loc_4[1].substr("1"));
                    this.sectormapitems[_loc_3][2] = Number(_loc_4[2].substr("1"));
                    this.sectormapitems[_loc_3][3] = int(_loc_4[3].substr("1"));
                    this.sectormapitems[_loc_3][6] = int(_loc_4[4].substr("1"));
                    this.sectormapitems[_loc_3][9] = 0;
                    this.sectormapitems[_loc_3][4] = "OFF";
                    this.sectormapitems[_loc_3][10] = Math.ceil(Number(this.sectormapitems[_loc_3][1]) / this.sectorinformation[1][0]);
                    this.sectormapitems[_loc_3][11] = Math.ceil(Number(this.sectormapitems[_loc_3][2]) / this.sectorinformation[1][1]);
                    this.func_StarbaseLoadingData(_loc_4);
                }
                if (_loc_2[_loc_3].substr(0, 2) == "TB")
                {
                    this.teamdeathmatch = true;
                    this.sectormapitems[_loc_3] = new Array();
                    _loc_4 = _loc_2[_loc_3].split("`");
                    _loc_7 = this.teambases.length;
                    this.teambases[_loc_7] = new Array();
                    this.teambases[_loc_7][0] = _loc_4[0];
                    this.sectormapitems[_loc_3][0] = _loc_4[0];
                    this.sectormapitems[_loc_3][1] = int(_loc_4[1].substr("1"));
                    this.teambases[_loc_7][1] = Number(_loc_4[1].substr("1"));
                    this.sectormapitems[_loc_3][2] = int(_loc_4[2].substr("1"));
                    this.teambases[_loc_7][2] = Number(_loc_4[2].substr("1"));
                    this.sectormapitems[_loc_3][3] = int(_loc_4[3].substr("1"));
                    this.teambases[_loc_7][3] = int(_loc_4[3].substr("1"));
                    this.teambases[_loc_7][4] = Number(_loc_4[4].substr("1"));
                    this.sectormapitems[_loc_3][6] = int(_loc_4[4].substr("1"));
                    this.teambases[_loc_7][5] = _loc_4[5];
                    this.teambases[_loc_7][9] = 0;
                    this.sectormapitems[_loc_3][9] = 0;
                    this.teambases[_loc_7][10] = 0;
                    this.teambases[_loc_7][11] = 0;
                    this.sectormapitems[_loc_3][4] = "OFF";
                    this.sectormapitems[_loc_3][10] = Math.ceil(Number(this.sectormapitems[_loc_3][1]) / this.sectorinformation[1][0]);
                    this.sectormapitems[_loc_3][11] = Math.ceil(Number(this.sectormapitems[_loc_3][2]) / this.sectorinformation[1][1]);
                    this.sectormapitems[_loc_3][15] = int(_loc_4[6]);
                    this.teambases[_loc_7][15] = int(_loc_4[6]);
                    this.sectormapitems[_loc_3][17] = int(_loc_4[7]);
                    this.teambases[_loc_7][17] = int(_loc_4[7]);
                    _loc_5 = getDefinitionByName("teambasetype" + this.teambases[_loc_7][3]) as Class;
                    this.teambases[_loc_7][21] = new _loc_5 as MovieClip;
                    this.teambases[_loc_7][22] = new this.RadarBlot() as MovieClip;
                    this.teambases[_loc_7][23] = new shiphealthbar() as MovieClip;
                    this.teambases[_loc_7][21].addChild(this.teambases[_loc_7][23]);
                    this.teambases[_loc_7][23].y = -63;
                }
                if (_loc_2[_loc_3].substr(0, 2) == "AS")
                {
                    this.sectormapitems[_loc_3] = new Array();
                    _loc_4 = _loc_2[_loc_3].split("`");
                    this.sectormapitems[_loc_3][0] = _loc_4[0];
                    this.sectormapitems[_loc_3][1] = int(_loc_4[1].substr("1"));
                    this.sectormapitems[_loc_3][2] = int(_loc_4[2].substr("1"));
                    this.sectormapitems[_loc_3][3] = int(_loc_4[3].substr("1"));
                    this.sectormapitems[_loc_3][4] = "OFF";
                    this.sectormapitems[_loc_3][10] = Math.ceil(Number(this.sectormapitems[_loc_3][1]) / this.sectorinformation[1][0]);
                    this.sectormapitems[_loc_3][11] = Math.ceil(Number(this.sectormapitems[_loc_3][2]) / this.sectorinformation[1][1]);
                    this.sectormapitems[_loc_3][22] = new this.RadarBlot() as MovieClip;
                }
                if (_loc_2[_loc_3].substr(0, 2) == "NP")
                {
                    this.sectormapitems[_loc_3] = new Array();
                    _loc_4 = _loc_2[_loc_3].split("`");
                    this.sectormapitems[_loc_3][0] = _loc_4[0];
                    this.sectormapitems[_loc_3][1] = Number(_loc_4[1].substr("1"));
                    this.sectormapitems[_loc_3][2] = Number(_loc_4[2].substr("1"));
                    this.sectormapitems[_loc_3][3] = Number(_loc_4[3].substr("1"));
                    this.sectormapitems[_loc_3][4] = "OFF";
                    _loc_8 = Number(_loc_4[4]);
                    if (isNaN(_loc_8))
                    {
                        this.sectormapitems[_loc_3][5] = _loc_4[4];
                    }
                    else
                    {
                        this.sectormapitems[_loc_3][5] = "Jump Point to System: " + _loc_4[4];
                        this.sectormapitems[_loc_3][9] = _loc_8;
                    }
                    this.sectormapitems[_loc_3][10] = Math.ceil(Number(this.sectormapitems[_loc_3][1]) / this.sectorinformation[1][0]);
                    this.sectormapitems[_loc_3][11] = Math.ceil(Number(this.sectormapitems[_loc_3][2]) / this.sectorinformation[1][1]);
                    _loc_5 = getDefinitionByName("navpointtype0") as Class;
                    this.sectormapitems[_loc_3][21] = new _loc_5 as MovieClip;
                    this.sectormapitems[_loc_3][22] = new this.RadarBlot() as MovieClip;
                }
                if (_loc_2[_loc_3].substr(0, 2) == "SI")
                {
                    _loc_4 = _loc_2[_loc_3].split("`");
                    if (_loc_4[1].substr(0, 2) == "XL")
                    {
                        trace("Zone:" + _loc_4[0]);
                        _loc_9 = _loc_4[0].substr("2");
                        trace("ZoneNo:" + _loc_9);
                        if (_loc_9.substr(0, 1) == "6")
                        {
                            this.PlayerInConquestZone = true;
                        }
                        this.sectorinformation = new Array();
                        this.sectorinformation[0] = new Array();
                        this.sectorinformation[0][0] = Number(_loc_4[1].substr("2"));
                        this.sectorinformation[0][1] = Number(_loc_4[2].substr("2"));
                        this.sectorinformation[1] = new Array();
                        this.sectorinformation[1][0] = Number(_loc_4[3].substr("2"));
                        this.sectorinformation[1][1] = Number(_loc_4[4].substr("2"));
                        this.isaracealteredZone = false;
                        _loc_10 = _loc_4[6].split(",");
                        _loc_11 = 0;
                        while (_loc_11 < _loc_10.length)
                        {
                            
                            if (_loc_10[_loc_11] == "PULSAR")
                            {
                                this.pulsarsinzone = true;
                            }
                            if (_loc_10[_loc_11] == "RACES")
                            {
                                this.isaracealteredZone = true;
                            }
                            _loc_11 = _loc_11 + 1;
                        }
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            if (this.isaracealteredZone == false)
            {
            }
            this.loginmovie.mov_login.logindisplay.currentstatus.text = this.loginmovie.mov_login.logindisplay.currentstatus.text + "\rBuilding Map";
            this.func_InitializeGameMap();
            this.loginmovie.mov_login.logindisplay.currentstatus.text = this.loginmovie.mov_login.logindisplay.currentstatus.text + "\rSystem Loaded\r";
            return;
        }// end function

        public function func_TriggerGameMap()
        {
            if (this.sectorinformation[0] != null)
            {
                if (this.gameMap.visible)
                {
                    this.MapTimer.stop();
                    this.gameMap.visible = false;
                }
                else
                {
                    this.gameMap.mysocket = this.mysocket;
                    this.gameMap.tradegoods = this.tradegoods;
                    this.gameMap.currentRolledOverTarget = "";
                    this.gameMap.destinationInfoWin.DispInfo.htmlText = "Roll over a target to retrieve its information";
                    this.playersdestination[0] = this.gameMap.func_PlayerMapLocation(this.shipcoordinatex, this.shipcoordinatey, this.AnglePlayerShipFacing);
                    this.func_setHeadingLocation();
                    this.MapTimer.start();
                    this.gameMap.visible = true;
                }
                this.func_RefocusToStage();
            }
            return;
        }// end function

        public function func_refreshMenuVolumeSetting()
        {
            this.gamesetting.MenuSoundTransform.volume = this.gamesetting.MenuVolume;
            this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
            return;
        }// end function

        public function MissileFireStyleChanged(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            if (this.playershipstatus[10][1] != "Single")
            {
                this.playershipstatus[10][1] = "Single";
            }
            else
            {
                this.playershipstatus[10][1] = "All";
            }
            var _loc_2:* = this.missileBankWindow.func_RefreshDisplay(this.playershipstatus[7], this.playershipstatus[10][0], this.playershipstatus[10][1]);
            stage.focus = stage;
            return;
        }// end function

        public function func_drawSquadbaseMarker(param1)
        {
            this.gameMap.SquadBaseIDAdjust = this.SquadBaseIDAdjust;
            this.gameMap.playersquadbases = this.playersquadbases;
            this.gameMap.func_drawSquadbase(param1);
            return;
        }// end function

        public function func_enterintochat(param1, param2)
        {
            var _loc_3:* = undefined;
            _loc_3 = new Array();
            _loc_3[0] = param1;
            _loc_3[1] = param2;
            this.gamechatinfo[1].splice(0, 0, _loc_3);
            this.gamechatinfo[1].splice(30, 1);
            this.func_RefreshChat();
            return;
        }// end function

        public function func_IncomingChat(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            trace("chat in:" + param1);
            if (param1[2] == "M" || param1[2] == "PM" || param1[2] == "TM" || param1[2] == "SM" || param1[2] == "AM" || param1[2] == "STF" || param1[2] == "HLP")
            {
                _loc_2 = false;
                _loc_4 = param1[1];
                if (_loc_4.toUpperCase() == "HOST")
                {
                    if (param1[2] == "STF" || param1[2] == "HLP")
                    {
                        _loc_3 = this.func_checkothercharacters(param1[3]);
                    }
                    else
                    {
                        _loc_3 = "HOST: " + this.func_checkothercharacters(param1[3]);
                    }
                }
                else if (param1[2] == "PM")
                {
                    _loc_3 = param1[1] + ": " + this.func_checkothercharacters(param1[3]);
                    this.gamechatinfo[5] = param1[1];
                }
                else if (param1[2] == "SM")
                {
                    _loc_4 = param1[1];
                    _loc_3 = _loc_4 + ": " + this.func_checkothercharacters(param1[3]);
                }
                else
                {
                    _loc_5 = 0;
                    while (_loc_5 < this.currentonlineplayers.length)
                    {
                        
                        if (param1[1] == this.currentonlineplayers[_loc_5][0])
                        {
                            _loc_4 = this.currentonlineplayers[_loc_5][1];
                            if (param1[2] == "PM")
                            {
                                this.gamechatinfo[5] = _loc_4;
                            }
                            if (param1[2] != "AM")
                            {
                                _loc_2 = this.func_ignoreplayer(_loc_4);
                            }
                            else
                            {
                                _loc_2 = false;
                            }
                            if (_loc_2 != true)
                            {
                                _loc_3 = _loc_4 + ": " + this.func_checkothercharacters(param1[3]);
                            }
                            break;
                        }
                        _loc_5 = _loc_5 + 1;
                    }
                }
                if (_loc_2 != true)
                {
                    _loc_6 = this.regularchattextcolor;
                    if (param1[2] == "M")
                    {
                        _loc_6 = this.regularchattextcolor;
                    }
                    if (param1[2] == "PM")
                    {
                        _loc_6 = this.privatechattextcolor;
                    }
                    if (param1[2] == "TM")
                    {
                        _loc_6 = this.teamchattextcolor;
                    }
                    if (param1[2] == "SM")
                    {
                        _loc_6 = this.squadchattextcolor;
                    }
                    if (param1[2] == "AM")
                    {
                        _loc_6 = this.arenachattextcolor;
                    }
                    if (param1[2] == "STF")
                    {
                        _loc_6 = this.stafchattextcolor;
                    }
                    if (param1[2] == "HLP")
                    {
                        _loc_6 = this.staffhelpchattextcolor;
                    }
                    this.func_enterintochat(_loc_3, _loc_6);
                }
            }
            else if (param1[2] == "WM")
            {
                _loc_3 = "HOST: Your Base Has Been Attacked";
                _loc_6 = this.squadchattextcolor;
                this.func_enterintochat(_loc_3, _loc_6);
            }
            return;
        }// end function

        public function MissionsMouseOver(event:MouseEvent) : void
        {
            this.func_setSelectedIconMessage("missions");
            return;
        }// end function

        public function func_SelectAutoTarget()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            _loc_1 = 200;
            _loc_2 = 0;
            _loc_3 = -1;
            _loc_4 = _loc_1;
            if (this.targetinfo[4] != this.playershipstatus[3][0])
            {
                if (this.targetinfo[4] < 4000)
                {
                    _loc_2 = 0;
                    while (_loc_2 < this.otherplayership.length)
                    {
                        
                        if (this.otherplayership[_loc_2][0] == this.targetinfo[4])
                        {
                            if (this.otherplayership[_loc_2][16].length < 2)
                            {
                                _loc_5 = Math.abs(this.otherplayership[_loc_2][1] - this.shipcoordinatex);
                                _loc_6 = Math.abs(this.otherplayership[_loc_2][2] - this.shipcoordinatey);
                                _loc_4 = Math.sqrt(_loc_5 * _loc_5 + _loc_6 * _loc_6);
                                if (_loc_4 < _loc_1)
                                {
                                    this.targetinfo[5] = _loc_2;
                                    _loc_3 = _loc_2;
                                    break;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        _loc_2 = _loc_2 + 1;
                    }
                }
            }
            if (_loc_3 == -1)
            {
                this.targetinfo[4] = this.playershipstatus[3][0];
                _loc_4 = _loc_1;
            }
            _loc_2 = 0;
            while (_loc_2 < this.otherplayership.length)
            {
                
                if (this.otherplayership[_loc_2][0] != this.playershipstatus[3][0])
                {
                    if (this.otherplayership[_loc_2][56] != "friend")
                    {
                        if (this.otherplayership[_loc_2][16].length < 2)
                        {
                            _loc_7 = Math.abs(this.otherplayership[_loc_2][1] - this.shipcoordinatex);
                            _loc_8 = Math.abs(this.otherplayership[_loc_2][2] - this.shipcoordinatey);
                            _loc_9 = Math.sqrt(_loc_7 * _loc_7 + _loc_8 * _loc_8);
                            if (_loc_9 < _loc_1)
                            {
                                if (_loc_9 < _loc_4)
                                {
                                    _loc_4 = _loc_9;
                                    this.targetinfo[4] = this.otherplayership[_loc_2][0];
                                    this.targetinfo[5] = _loc_2;
                                }
                            }
                        }
                    }
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_KillOpponentsShip(param1)
        {
            var currentplayerid:* = param1;
            var jjjj:*;
            var TotalOthers:* = this.otherplayership.length;
            while (jjjj < TotalOthers)
            {
                
                if (currentplayerid == this.otherplayership[jjjj][0])
                {
                    this.func_RemoveRadarDot(this.otherplayership[jjjj][59]);
                    try
                    {
                        this.gamedisplayarea.removeChild(this.otherplayership[jjjj][21]);
                    }
                    catch (error:Error)
                    {
                        trace("Kill Ship Error: " + error);
                    }
                    this.otherplayership[jjjj][21] = new this.shipDeadImage() as MovieClip;
                    this.gamedisplayarea.addChild(this.otherplayership[jjjj][21]);
                    this.otherplayership[jjjj][21].x = -999999;
                    this.otherplayership[jjjj][6] = getTimer() + 2200;
                    this.otherplayership[jjjj][15] = "dead";
                    this.func_shipExplosionSound(this.otherplayership[jjjj][1], this.otherplayership[jjjj][2]);
                    break;
                }
                jjjj = (jjjj + 1);
            }
            return;
        }// end function

        public function func_dm_getBaseLives()
        {
            this.mysocket.send("DM`BL`~");
            return;
        }// end function

        public function func_refreshOtherPlayerDisplayedLabel(param1)
        {
            var KillerOfShip:* = param1;
            var i:*;
            while (i < this.otherplayership.length)
            {
                
                if (this.otherplayership[i][0] == KillerOfShip)
                {
                    try
                    {
                        this.otherplayership[i][54].shipLabel.text = this.func_getPlayersIngameLabel(this.otherplayership[i][0]);
                    }
                    catch (error:Error)
                    {
                        trace("couldnt update players label");
                    }
                }
                i = (i + 1);
            }
            return;
        }// end function

        public function func_redrawGameSoundsButton()
        {
            this.inGameSoundButton.visible = true;
            if (!this.gamesetting.soundvolume)
            {
                this.inGameSoundButton.gotoAndStop(2);
            }
            else
            {
                this.inGameSoundButton.gotoAndStop(1);
            }
            return;
        }// end function

        public function func_checkMusicSettings()
        {
            //if (this.gamesetting.gameMusic)
            //{
            //    if (this.mySndCh == null)
            //    {
            //        this.playGameMusic();
            //    }
            //    this.func_UpdateMusicVolume();
            //}
            //else if (this.mySndCh != null)
            //{
            //    this.mySndCh.removeEventListener(Event.SOUND_COMPLETE, this.loopGameMusic);
            //    this.mySndCh.stop();
            //    this.mySndCh = null;
            //}
            //return;
        }// end function

        public function func_LogOutPlayer(param1)
        {
            var _loc_2:* = 0;
            var _loc_3:* = "";
            while (_loc_2 < this.currentonlineplayers.length)
            {
                
                if (param1 == this.currentonlineplayers[_loc_2][0])
                {
                    _loc_3 = this.currentonlineplayers[_loc_2][1];
                    this.currentonlineplayers.splice(_loc_2, 1);
                }
                _loc_2 = _loc_2 + 1;
            }
            this.func_refreshCurrentOnlineList();
            this.func_enterintochat(_loc_3 + " has left", this.playerEnteringCol);
            this.func_AddChatter(_loc_3 + " has left");
            return;
        }// end function

        public function func_exitHardWareScreen(event:MouseEvent) : void
        {
            this.playershipstatus = this.ShipHardwareScreen.playershipstatus;
            this.OnlinePLayerList.visible = true;
            this.savecurrenttoextraship(this.playerscurrentextrashipno);
            this.func_playRegularClick();
            if (this.LastDockScreen == "main")
            {
                gotoAndStop("dockedscreen");
            }
            else
            {
                gotoAndStop("Hangar");
            }
            return;
        }// end function

        public function DeathMatchTimerHandler(event:TimerEvent) : void
        {
            var event:* = event;
            try
            {
                this.displaystats();
                this.func_dm_getBaseLives();
                var _loc_3:* = this;
                var _loc_4:* = this.counterForBaseLives + 1;
                _loc_3.counterForBaseLives = _loc_4;
                if (this.counterForBaseLives > 10)
                {
                    this.func_dm_getBaseLives();
                    this.counterForBaseLives = 0;
                }
            }
            catch (error:Error)
            {
                DeathMatchTimer.stop();
            }
            return;
        }// end function

        public function func_IncomingMeteorInfo(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            var _loc_15:* = undefined;
            if (param1[1] == "CR")
            {
                _loc_2 = Number(param1[2]);
                this.gameMeteors[_loc_2] = new Object();
                this.gameMeteors[_loc_2].heading = Number(param1[3]);
                this.gameMeteors[_loc_2].targetNo = Number(param1[4]);
                this.gameMeteors[_loc_2].xposition = Number(param1[5]);
                this.gameMeteors[_loc_2].yposition = Number(param1[6]);
                this.gameMeteors[_loc_2].velocity = Number(param1[7]);
                this.gameMeteors[_loc_2].TimeUntillHit = Number(param1[8]);
                this.gameMeteors[_loc_2].HitTime = Number(param1[8]) + getTimer();
                this.gameMeteors[_loc_2].mapMarker = new this.MeteorMapLocaterImage() as MovieClip;
                this.gameMeteors[_loc_2].mapMarker.x = -999999999;
                this.gameMeteors[_loc_2].mapMarker.y = -999999999;
                this.gameMeteors[_loc_2].mapMarker.rotation = this.gameMeteors[_loc_2].heading;
                this.gameMeteors[_loc_2].meteorImage = new this.MeteorHolderImage() as MovieClip;
                this.gameMeteors[_loc_2].meteorImage.x = this.gameMeteors[_loc_2].xposition;
                this.gameMeteors[_loc_2].meteorImage.y = this.gameMeteors[_loc_2].yposition;
                this.gameMeteors[_loc_2].lifeBar = new shiphealthbar() as MovieClip;
                this.gameMeteors[_loc_2].lifeBar.y = -this.gameMeteors[_loc_2].meteorImage.width * 0.8;
                this.gameMeteors[_loc_2].meteorImage.addChild(this.gameMeteors[_loc_2].lifeBar);
                _loc_3 = this.MovingObjectWithThrust(this.gameMeteors[_loc_2].heading, this.gameMeteors[_loc_2].velocity);
                this.gameMeteors[_loc_2].xspeed = _loc_3[0] / 1000;
                this.gameMeteors[_loc_2].yspeed = _loc_3[1] / 1000;
                this.gameMap.addChild(this.gameMeteors[_loc_2].mapMarker);
                this.addSingleMeteorBackgroundImage(_loc_2);
                this.maxMeteors = this.gameMeteors.length;
                this.gameMeteors[_loc_2].lifeBar.func_setLifeSettings(1 / 1, 0 / 1);
                this.func_writeNewstoScreen("NEWS", "A Meteor has spawned and will collide in " + Math.floor(this.gameMeteors[_loc_2].TimeUntillHit / 1000) + "seconds, check your map for its location!");
            }
            else if (param1[1] == "HT")
            {
            }
            else if (param1[1] == "DST")
            {
                _loc_4 = Number(param1[2]);
                _loc_5 = Number(param1[3]);
                _loc_6 = Number(param1[4]);
                _loc_7 = Number(param1[5]);
                this.func_removeMeteorFromGame(_loc_4);
                if (_loc_5 == this.playershipstatus[3][0])
                {
                    this.playershipstatus[3][1] = this.playershipstatus[3][1] + _loc_7;
                    this.playershipstatus[5][9] = this.playershipstatus[5][9] + Number(_loc_6);
                }
                _loc_8 = 0;
                _loc_9 = false;
                _loc_10 = "AI";
                while (_loc_8 < this.currentonlineplayers.length)
                {
                    
                    if (_loc_5 == String(this.currentonlineplayers[_loc_8][0]))
                    {
                        this.currentonlineplayers[_loc_8][5] = this.currentonlineplayers[_loc_8][5] + Number(_loc_6);
                        _loc_10 = this.currentonlineplayers[_loc_8][1];
                        break;
                    }
                    _loc_8 = _loc_8 + 1;
                }
                _loc_12 = "A meteor was destroyed by " + _loc_10 + " for " + _loc_7 + " credits and " + _loc_6 + " score.";
                this.func_writeNewstoScreen("NEWS", _loc_12);
            }
            else if (param1[1] == "LFE")
            {
                _loc_13 = Number(param1[2]);
                _loc_14 = Number(param1[3]);
                _loc_15 = Number(param1[4]);
                this.gameMeteors[_loc_13].lifeBar.func_setLifeSettings(_loc_14 / _loc_15, 0 / 1);
            }
            return;
        }// end function

        public function tradegoodsMouseOver(event:MouseEvent) : void
        {
            this.func_setSelectedIconMessage("tradegoods");
            return;
        }// end function

        public function func_HardwareBaseButton_Click(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.gameTutorial.visible = false;
            gotoAndStop("ShipHardware");
            return;
        }// end function

        public function func_displayallspecials()
        {
            var _loc_1:* = undefined;
            _loc_1 = 1;
            while (_loc_1 < 10)
            {
                
                if (_loc_1 > this.playershipstatus[11][1].length)
                {
                    this.specialsingame["sp" + _loc_1].visible = false;
                }
                else
                {
                    this.specialsingame["sp" + _loc_1].specialinfodata.text = "";
                    this.func_displayspecials(_loc_1);
                    this.specialsingame["sp" + _loc_1].specialbutton.button.text = _loc_1;
                    this.specialsingame["sp" + _loc_1].visible = true;
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_updateTameBaseHealthBars(param1)
        {
            var baseNumber:* = param1;
            try
            {
                this.teambases[baseNumber][23].func_setLifeSettings(this.teambases[baseNumber][10] / this.teambases[baseNumber][17], 0);
            }
            catch (error:Error)
            {
            }
            return;
        }// end function

        public function func_exitHangarScreen(event:MouseEvent) : void
        {
            this.OnlinePLayerList.visible = true;
            this.func_playRegularClick();
            gotoAndStop("dockedscreen");
            return;
        }// end function

        public function func_buildBlankChat()
        {
            var _loc_1:* = undefined;
            _loc_1 = this.gamechatinfo[2][1];
            while (_loc_1 >= 0)
            {
                
                this.gamechatinfo[1][_loc_1] = new Array();
                this.gamechatinfo[1][_loc_1][0] = "";
                this.gamechatinfo[1][_loc_1][1] = this.teamchattextcolor;
                _loc_1 = _loc_1 - 1;
            }
            return;
        }// end function

        public function func_AddOtherGunshot(param1)
        {
            var _loc_9:* = undefined;
            if (this.othergunfire.length < 1)
            {
                this.othergunfire = new Array();
            }
            var _loc_10:* = this;
            var _loc_11:* = this.currentotherplayshot + 1;
            _loc_10.currentotherplayshot = _loc_11;
            if (this.currentotherplayshot >= 500)
            {
                this.currentotherplayshot = 1;
            }
            var _loc_2:* = this.othergunfire.length;
            this.othergunfire[_loc_2] = new Array();
            this.othergunfire[_loc_2][0] = param1[1];
            param1.splice(4, 2);
            if (this.othergunfire[_loc_2][0].substr(0, 2) == "AI")
            {
                this.othergunfire[_loc_2][0].splice(2, 1);
            }
            this.othergunfire[_loc_2][1] = Number(param1[2]);
            this.othergunfire[_loc_2][2] = Number(param1[3]);
            var _loc_3:* = int(param1[4]);
            this.othergunfire[_loc_2][6] = int(param1[5]);
            var _loc_4:* = this.othergunfire[_loc_2][6];
            var _loc_5:* = this.MovingObjectWithThrust(_loc_4, _loc_3);
            this.othergunfire[_loc_2][3] = _loc_5[0];
            this.othergunfire[_loc_2][4] = _loc_5[1];
            this.othergunfire[_loc_2][7] = int(param1[6]);
            this.othergunfire[_loc_2][8] = this.currentotherplayshot;
            this.othergunfire[_loc_2][9] = int(param1[7]);
            this.othergunfire[_loc_2][10] = "other";
            this.othergunfire[_loc_2][14] = false;
            var _loc_6:* = this.othergunfire[_loc_2][0];
            if (Number(_loc_6 >= this.SquadBaseIDAdjust))
            {
                if (this.playersquadbases[_loc_6 - this.SquadBaseIDAdjust] != null)
                {
                    if (this.playersquadbases[_loc_6 - this.SquadBaseIDAdjust][0] == this.playershipstatus[5][10])
                    {
                        this.othergunfire[_loc_2][14] = true;
                    }
                }
            }
            else if (this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] != -1)
            {
                _loc_9 = 0;
                while (_loc_9 < this.currentonlineplayers.length)
                {
                    
                    if (this.currentonlineplayers[_loc_9][0] == _loc_6)
                    {
                        if (this.currentonlineplayers[_loc_9][4] == this.playershipstatus[5][2])
                        {
                            this.othergunfire[_loc_2][14] = true;
                            break;
                        }
                    }
                    _loc_9 = _loc_9 + 1;
                }
            }
            this.othergunfire[_loc_2][5] = getTimer() + this.guntype[this.othergunfire[_loc_2][7]][1] * 1000;
            var _loc_7:* = String(getTimer() + this.clocktimediff);
            _loc_7 = Number(_loc_7.substr(_loc_7.length - 4));
            var _loc_8:* = _loc_7 - Number(param1[8]);
            if (_loc_7 - Number(param1[8]) < -1000)
            {
                _loc_8 = _loc_8 + 10000;
            }
            else if (_loc_8 > 10000)
            {
                _loc_8 = _loc_8 - 10000;
            }
            _loc_8 = _loc_8 / 1000;
            this.othergunfire[_loc_2][1] = this.othergunfire[_loc_2][1] + this.othergunfire[_loc_2][3] * _loc_8;
            this.othergunfire[_loc_2][2] = this.othergunfire[_loc_2][2] + this.othergunfire[_loc_2][4] * _loc_8;
            this.othergunfire[_loc_2][16] = new this.guntype[this.othergunfire[_loc_2][7]][10] as MovieClip;
            this.gamedisplayarea.addChild(this.othergunfire[_loc_2][16]);
            this.othergunfire[_loc_2][16].x = this.othergunfire[_loc_2][1] - this.shipcoordinatex;
            this.othergunfire[_loc_2][16].y = this.othergunfire[_loc_2][2] - this.shipcoordinatey;
            this.othergunfire[_loc_2][16].rotation = this.othergunfire[_loc_2][6];
            this.othergunfire[_loc_2][18] = Number(param1[2]);
            this.othergunfire[_loc_2][19] = Number(param1[3]);
            this.othergunfire[_loc_2][11] = this.othergunfire[_loc_2][16].width / 2;
            this.othergunfire[_loc_2][12] = this.othergunfire[_loc_2][16].height / 2;
            this.func_fireGunSound(this.othergunfire[_loc_2][7], this.othergunfire[_loc_2][1], this.othergunfire[_loc_2][2]);
            return;
        }// end function

        public function Proccess_Energies(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            if (this.playershipstatus[5][4] == "dead")
            {
                this.playershipstatus[2][1] = 0;
                this.playershipstatus[1][1] = 0;
                this.playershipstatus[2][5] = 0;
            }
            else
            {
                if (this.playershipstatus[2][5] / this.shiptype[this.playershipstatus[5][0]][3][3] < 0.5)
                {
                    this.mov_structureWarning.visible = true;
                }
                else
                {
                    this.mov_structureWarning.visible = false;
                }
                _loc_2 = this.playershipstatus[1][1];
                _loc_3 = this.playershipstatus[2][1];
                if (this.isplayeremp)
                {
                    _loc_2 = 0;
                    if (this.playerempend < getTimer())
                    {
                        this.isplayeremp = false;
                    }
                }
                else
                {
                    if (isNaN(_loc_3))
                    {
                        _loc_3 = 0;
                    }
                    if (_loc_3 < 0)
                    {
                        _loc_3 = 0;
                    }
                    _loc_3 = _loc_3 + this.shieldrechargerate * param1;
                    if (this.playershipstatus[2][2] == "OFF")
                    {
                        _loc_3 = 0;
                    }
                    else if (this.playershipstatus[2][2] == "HALF")
                    {
                        if (_loc_3 > Math.round(this.maxshieldstrength / 2))
                        {
                            _loc_3 = Math.round(this.maxshieldstrength / 2);
                        }
                    }
                    else if (_loc_3 > this.maxshieldstrength)
                    {
                        _loc_3 = this.maxshieldstrength;
                    }
                    _loc_2 = _loc_2 + this.energyrechargerate * param1;
                    if (this.playershipstatus[2][2] != "OFF")
                    {
                        _loc_2 = _loc_2 - this.baseshieldgendrain * param1;
                        _loc_2 = _loc_2 - _loc_3 / this.maxshieldstrength * (this.energydrainedbyshieldgenatfull * param1);
                    }
                    if (_loc_2 < 0)
                    {
                        _loc_2 = 0;
                    }
                    if (_loc_2 > this.maxenergy)
                    {
                        _loc_2 = this.maxenergy;
                    }
                    this.playershipstatus[1][1] = _loc_2;
                }
            }
            this.playershipstatus[1][1] = _loc_2;
            this.playershipstatus[2][1] = _loc_3;
            this.PlayerStatDisp.func_displaystats(_loc_2, _loc_3, this.playershipstatus[2][5], this.playershipvelocity);
            return;
        }// end function

        public function func_getDockingTipMessage()
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_1:* = null;
            _loc_7 = 0;
            while (_loc_7 < this.starbaselocation.length)
            {
                
                if (this.starbaselocation[_loc_7] != null)
                {
                    if (this.starbaselocation[_loc_7][5] == "ACTIVE" || Number(this.starbaselocation[_loc_7][5]) < getTimer())
                    {
                        _loc_2 = this.starbaselocation[_loc_7][1];
                        _loc_3 = this.starbaselocation[_loc_7][2];
                        _loc_4 = this.starbaselocation[_loc_7][4];
                        _loc_5 = _loc_2 - this.shipcoordinatex;
                        _loc_6 = _loc_3 - this.shipcoordinatey;
                        if (_loc_5 * _loc_5 + _loc_6 * _loc_6 <= _loc_4 * _loc_4)
                        {
                            if (this.playershipvelocity > this.maxdockingvelocity)
                            {
                                return "Slow down if you wish to dock";
                            }
                            if (this.starbaselocation[_loc_7][9] > getTimer())
                            {
                            }
                            else
                            {
                                return "To Dock, Press X";
                            }
                        }
                    }
                }
                _loc_7 = _loc_7 + 1;
            }
            _loc_8 = 0;
            while (_loc_8 < this.playersquadbases.length)
            {
                
                if (this.playersquadbases[_loc_8] != null)
                {
                    if (this.playersquadbases[_loc_8][0] == this.playershipstatus[5][10])
                    {
                        _loc_2 = this.playersquadbases[_loc_8][2];
                        _loc_3 = this.playersquadbases[_loc_8][3];
                        _loc_4 = 100;
                        _loc_5 = _loc_2 - this.shipcoordinatex;
                        _loc_6 = _loc_3 - this.shipcoordinatey;
                        if (_loc_5 * _loc_5 + _loc_6 * _loc_6 <= _loc_4 * _loc_4)
                        {
                            if (this.playershipvelocity > this.maxdockingvelocity)
                            {
                                return "Slow down if you wish to dock";
                            }
                            return "Press X to Dock";
                        }
                    }
                }
                _loc_8 = _loc_8 + 1;
            }
            if (this.teamdeathmatch == true)
            {
                _loc_8 = 0;
                while (_loc_8 < this.teambases.length)
                {
                    
                    if (this.teambases[_loc_8] != null)
                    {
                        _loc_2 = this.teambases[_loc_8][1];
                        _loc_3 = this.teambases[_loc_8][2];
                        _loc_4 = this.teambases[_loc_8][4];
                        _loc_5 = Number(_loc_2) - Number(this.shipcoordinatex);
                        _loc_6 = Number(_loc_3) - Number(this.shipcoordinatey);
                        if (Math.sqrt(_loc_5 * _loc_5 + _loc_6 * _loc_6) <= _loc_4)
                        {
                            if (_loc_8 != this.playershipstatus[5][2])
                            {
                            }
                            else
                            {
                                if (this.playershipvelocity > this.maxdockingvelocity)
                                {
                                    return "Slow down if you wish to dock";
                                }
                                return "Press X to Dock";
                            }
                            break;
                        }
                    }
                    _loc_8 = _loc_8 + 1;
                }
            }
            return "";
        }// end function

        public function func_resetSpecials()
        {
            var _loc_1:* = undefined;
            this.playershipstatus[11][2] = new Array();
            _loc_1 = 0;
            while (_loc_1 < this.playershipstatus[11][1].length)
            {
                
                this.playershipstatus[11][2][_loc_1] = new Array();
                this.playershipstatus[11][2][_loc_1][0] = 0;
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function sendnewdmgame()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            _loc_1 = "";
            _loc_2 = 0;
            while (_loc_2 < this.teambases.length)
            {
                
                _loc_1 = _loc_1 + ("`" + this.teambasetypes[this.teambases[_loc_2][3]][0]);
                _loc_2 = _loc_2 + 1;
            }
            _loc_3 = "DM" + "`NG" + _loc_1 + "~";
            this.mysocket.send(_loc_3);
            return;
        }// end function

        public function func_setTurrets(param1)
        {
            this.turretcontrol.auto.gotoAndStop(2);
            this.turretcontrol.manual.gotoAndStop(2);
            this.turretcontrol.off.gotoAndStop(2);
            this.turretCrosshairs.visible = false;
            if (param1 == "auto")
            {
                this.playershipstatus[9] = "AUTO";
                this.turretcontrol.auto.gotoAndStop(1);
            }
            else if (param1 == "manual")
            {
                this.playershipstatus[9] = "MANUAL";
                this.turretcontrol.manual.gotoAndStop(1);
                this.turretCrosshairs.visible = true;
                this.turretCrosshairs.gotoAndStop(1);
            }
            else
            {
                this.playershipstatus[9] = "OFF";
                this.turretcontrol.off.gotoAndStop(1);
            }
            stage.focus = stage;
            return;
        }// end function

        public function func_InitPlayerAfterDock()
        {
            this.gunShotBufferData = "";
            this.missileShotBuuferData = "";
            this.currentplayershotsfired = 0;
            this.currenthelpframedisplayed = 0;
            this.playershipstatus[5][20] = false;
            this.playershotsfired = new Array();
            this.playerBeingSeekedByMissile = false;
            this.playershipstatus[6][0] = Math.ceil(this.shipcoordinatex / this.xwidthofasector);
            this.playershipstatus[6][1] = Math.ceil(this.shipcoordinatey / this.ywidthofasector);
            this.isupkeypressed = false;
            this.isdownkeypressed = false;
            this.isleftkeypressed = false;
            this.isrightkeypressed = false;
            this.iscontrolkeypressed = false;
            this.isdkeypressed = false;
            this.isshiftkeypressed = false;
            this.isspacekeypressed = false;
            this.keywaspressed = false;
            this.currenttimechangeratio = 0;
            this.afterburnerinuse = false;
            this.spacekeyjustpressed = false;
            this.iscontrolkeypressed = false;
            this.AnglePlayerShipFacing = 0;
            this.playershipvelocity = 0;
            this.NextShipTimeResend = getTimer() + this.InfoResendDelay - 500;
            this.playershipstatus[5][4] = "alive";
            this.playershipstatus[5][15] = "";
            this.playerShipSpeedRatio = 0;
            this.shipXmovement = 0;
            this.shipYmovement = 0;
            this.playershotsfired = new Array();
            this.func_initializePlayersship();
            this.func_initalizeStatDisplay();
            var _loc_3:* = Math.ceil(this.shipcoordinatex / this.xwidthofasector);
            this.playershipstatus[6][0] = Math.ceil(this.shipcoordinatex / this.xwidthofasector);
            this.playershipstatus[6][2] = _loc_3;
            var _loc_3:* = Math.ceil(this.shipcoordinatey / this.ywidthofasector);
            this.playershipstatus[6][1] = Math.ceil(this.shipcoordinatey / this.ywidthofasector);
            this.playershipstatus[6][3] = _loc_3;
            var _loc_1:* = this.playershipstatus[6][0] + "`" + this.playershipstatus[6][1];
            var _loc_2:* = "NEW`";
            this.lastshipcoordinatex = this.shipcoordinatex;
            this.lastshipcoordinatey = this.shipcoordinatey;
            this.playersFakeVelocity = 0;
            return;
        }// end function

        public function func_sendOutStats(param1)
        {
            var _loc_2:* = "STATS`TGT`INFO`";
            _loc_2 = _loc_2 + (this.playershipstatus[3][0] + "`" + param1 + "`");
            _loc_2 = _loc_2 + (Math.round(this.shieldrechargerate) + "`");
            _loc_2 = _loc_2 + (Math.round(this.maxshieldstrength) + "`");
            _loc_2 = _loc_2 + (Math.round(this.PlayerStatDisp.playersmaxstructure) + "`");
            _loc_2 = _loc_2 + (Math.round(this.playershipstatus[2][1]) + "`");
            _loc_2 = _loc_2 + (Math.round(this.playershipstatus[2][5]) + "`");
            _loc_2 = _loc_2 + "~";
            this.mysocket.send(_loc_2);
            return;
        }// end function

        public function func_exitTradeGoodsScreen(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            gotoAndStop("dockedscreen");
            return;
        }// end function

        public function deathfgamerewards()
        {
            return 0;
        }// end function

        public function func_playerGotHit()
        {
            this.PlayersShipShieldImage.gotoAndPlay(1);
            return;
        }// end function

        public function func_DeahtMatchInfoClick(event:MouseEvent) : void
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            _loc_2 = new Array();
            _loc_3 = new Array();
            _loc_4 = 0;
            while (_loc_4 < this.teambases.length)
            {
                
                _loc_2[_loc_4] = 0;
                _loc_3[_loc_4] = 0;
                _loc_4 = _loc_4 + 1;
            }
            _loc_4 = 0;
            while (_loc_4 < this.currentonlineplayers.length)
            {
                
                if (this.currentonlineplayers[_loc_4][4] != "N/A" && this.currentonlineplayers[_loc_4][4] >= 0 && this.currentonlineplayers[_loc_4][4] < this.teambases.length)
                {
                    if (this.currentonlineplayers[_loc_4][0] >= 0)
                    {
                        (_loc_2[this.currentonlineplayers[_loc_4][4]] + 1);
                    }
                    else
                    {
                        (_loc_3[this.currentonlineplayers[_loc_4][4]] + 1);
                    }
                }
                _loc_4 = _loc_4 + 1;
            }
            _loc_5 = -1;
            _loc_6 = -1;
            if (this.playershipstatus[5][2] != "N/A" && this.playershipstatus[5][2] >= 0)
            {
                _loc_6 = -1;
                if (_loc_2[0] < _loc_2[1])
                {
                    _loc_6 = 0;
                }
                else if (_loc_2[0] > _loc_2[1])
                {
                    _loc_6 = 1;
                }
                else if (Number(this.playershipstatus[5][2]) > 1)
                {
                    _loc_6 = Math.round(Math.random());
                }
                if (_loc_6 != this.playershipstatus[5][2] && _loc_6 > -1)
                {
                    _loc_5 = _loc_6;
                }
                else
                {
                    this.func_enterintochat(" Can\'t Uneven Teams", this.systemchattextcolor);
                }
            }
            else
            {
                _loc_6 = 0;
                if (_loc_2[0] == _loc_2[1])
                {
                    _loc_6 = Math.round(Math.random() * (this.teambases.length - 1));
                }
                else if (_loc_2[0] > _loc_2[1])
                {
                    _loc_6 = 1;
                }
                else
                {
                    _loc_6 = 0;
                }
                _loc_5 = _loc_6;
            }
            if (_loc_5 >= 0)
            {
                this.datatosend = "TC" + "`" + this.playershipstatus[3][0] + "`TM`" + _loc_5 + "~";
                this.mysocket.send(this.datatosend);
                this.func_enterintochat(" Team Changing to : " + this.teambases[_loc_5][0].substr(2), this.systemchattextcolor);
            }
            return;
        }// end function

        public function changetonewship(param1)
        {
            this.playerscurrentextrashipno = param1;
            var _loc_2:* = this.extraplayerships[param1][0];
			if (_loc_2 == null) {
				_loc_2 = 0;
			}
            if (this.playershipstatus[5][0] == _loc_2)
            {
            }
            else
            {
                this.playershipstatus[5][0] = _loc_2;
                this.playershipstatus[4][1] = new Array();
                this.initializemissilebanks();
            }
            this.playershipstatus[5][27] = false;
            this.playershiprotation = this.shiptype[_loc_2][3][2];
            this.playershipacceleration = this.shiptype[_loc_2][3][0];
            this.playershipstatus[2][4] = this.shiptype[_loc_2][3][3];
            this.playershipstatus[0] = new Array();
            var _loc_3:* = 0;
            while (_loc_3 < this.shiptype[_loc_2][2].length)
            {
                
                this.playershipstatus[0][_loc_3] = new Array();
                this.playershipstatus[0][_loc_3][0] = this.extraplayerships[param1][4][_loc_3];
                this.playershipstatus[0][_loc_3][1] = 0;
                this.playershipstatus[0][_loc_3][2] = this.shiptype[_loc_2][2][_loc_3][0];
                this.playershipstatus[0][_loc_3][3] = this.shiptype[_loc_2][2][_loc_3][1];
                _loc_3 = _loc_3 + 1;
            }
            var _loc_4:* = 0;
            this.playershipstatus[8] = new Array();
            while (_loc_4 < this.shiptype[_loc_2][5].length)
            {
                
                this.playershipstatus[8][_loc_4] = new Array();
                this.playershipstatus[8][_loc_4][0] = this.extraplayerships[param1][5][_loc_4];
                _loc_4 = _loc_4 + 1;
            }
            this.playershipstatus[11] = new Array();
            this.playershipstatus[11][0] = new Array();
            this.playershipstatus[11][0][0] = 0;
            this.playershipstatus[11][0][1] = 0;
            this.playershipstatus[11][1] = new Array();
            this.playershipstatus[11][2] = new Array();
            if (this.extraplayerships[param1][11] == null)
            {
                this.extraplayerships[param1][11] = new Array();
            }
            if (this.extraplayerships[param1][11][1] == null)
            {
                this.extraplayerships[param1][11][1] = new Array();
            }
            var _loc_5:* = 0;
            while (_loc_5 < this.extraplayerships[param1][11][1].length)
            {
                
                this.playershipstatus[11][1][_loc_5] = new Array();
                this.playershipstatus[11][1][_loc_5][0] = this.extraplayerships[param1][11][1][_loc_5][0];
                this.playershipstatus[11][1][_loc_5][1] = this.extraplayerships[param1][11][1][_loc_5][1];
                _loc_5 = _loc_5 + 1;
            }
            this.playershipstatus[2][0] = this.extraplayerships[param1][1];
            this.playershipstatus[1][0] = this.extraplayerships[param1][2];
            this.playershipstatus[1][5] = this.extraplayerships[param1][3];
            return;
        }// end function

        public function func_statuscheck(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            if (param1[1] == "TGT")
            {
                if (param1[2] == "GET")
                {
                    this.func_sendOutStats(param1[3]);
                }
                else if (param1[2] == "INFO")
                {
                    _loc_2 = Number(param1[3]);
                    if (this.playershipstatus[3][0] != _loc_2)
                    {
                        _loc_3 = 0;
                        while (_loc_3 < this.otherplayership.length)
                        {
                            
                            if (this.otherplayership[_loc_3][0] == _loc_2)
                            {
                                this.otherplayership[_loc_3][40] = Number(param1[8]);
                                this.otherplayership[_loc_3][41] = Number(param1[7]);
                                this.otherplayership[_loc_3][42] = Number(param1[4]);
                                this.otherplayership[_loc_3][43] = Number(param1[5]);
                                this.otherplayership[_loc_3][47] = Number(param1[6]);
                            }
                            _loc_3 = _loc_3 + 1;
                        }
                    }
                }
            }
            return;
        }// end function

        public function func_refreshBaseTeamDisplays(param1)
        {
            this.gameMap.starbaselocation = this.starbaselocation;
            this.gameMap.func_refreshBaseTeamDisplays(param1);
            return;
        }// end function

        public function func_fire_a_missile(param1)
        {
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            var _loc_15:* = undefined;
            var _loc_16:* = undefined;
            var _loc_17:* = undefined;
            var _loc_18:* = undefined;
            var _loc_19:* = undefined;
            var _loc_20:* = undefined;
            var _loc_2:* = param1;
            var _loc_3:* = this.playershipstatus[7][_loc_2][0];
            if (this.playershipstatus[7][_loc_2][4][_loc_3] > 0)
            {
                _loc_4 = getTimer();
                if (this.playerSpecialsSettings.isCloaked)
                {
                    this.func_turnofshipcloak();
                }
                _loc_5 = true;
                if (this.RapidMissilesOn)
                {
                    if (this.playershipstatus[1][1] > this.EnergyPerRapid)
                    {
                        this.playershipstatus[1][1] = this.playershipstatus[1][1] - this.EnergyPerRapid;
                        _loc_6 = _loc_4 + this.missile[_loc_3][3] * this.RapidMissileRateChange;
                    }
                    else
                    {
                        _loc_5 = false;
                    }
                }
                else
                {
                    _loc_6 = _loc_4 + this.missile[_loc_3][3];
                }
                _loc_7 = this.playershotsfired.length;
                if (_loc_7 < 1)
                {
                    this.playershotsfired = new Array();
                    _loc_7 = 0;
                }
                _loc_8 = _loc_3;
                _loc_9 = this.missile[_loc_3][0];
                _loc_10 = this.PlayersShipImage.rotation;
                _loc_11 = _loc_9 + this.playershipvelocity;
                _loc_12 = this.MovingObjectWithThrust(_loc_10, _loc_11);
                _loc_13 = _loc_12[0];
                _loc_14 = _loc_12[1];
                _loc_15 = String(getTimer() + this.clocktimediff);
                _loc_15 = Number(_loc_15.substr(_loc_15.length - 4));
                if (_loc_15 > 10000)
                {
                    _loc_15 = _loc_15 - 10000;
                }
                if (this.currentplayershotsfired > 999)
                {
                    this.currentplayershotsfired = 0;
                }
                _loc_16 = this.playershipstatus[7][_loc_2][2];
                _loc_17 = this.playershipstatus[7][_loc_2][3];
                _loc_18 = this.firingbulletstartlocation(_loc_16, _loc_17, _loc_10);
                _loc_16 = _loc_18[0] + this.shipcoordinatex;
                _loc_17 = _loc_18[1] + this.shipcoordinatey;
                if (_loc_5)
                {
                    var _loc_21:* = this.playershipstatus[7][_loc_2][4];
                    var _loc_22:* = _loc_3;
                    var _loc_23:* = _loc_21[_loc_3] - 1;
                    _loc_21[_loc_22] = _loc_23;
                    if (this.currentplayershotsfired > 998)
                    {
                        this.currentplayershotsfired = 0;
                    }
                    _loc_19 = this.currentplayershotsfired;
                    var _loc_21:* = this;
                    var _loc_22:* = this.currentplayershotsfired + 1;
                    _loc_21.currentplayershotsfired = _loc_22;
                    this.playershotsfired[_loc_7] = new Array();
                    this.playershotsfired[_loc_7][0] = _loc_19;
                    this.playershotsfired[_loc_7][1] = Math.round(_loc_16);
                    this.playershotsfired[_loc_7][2] = Math.round(_loc_17);
                    this.playershotsfired[_loc_7][3] = _loc_13;
                    this.playershotsfired[_loc_7][4] = _loc_14;
                    this.playershotsfired[_loc_7][5] = _loc_4 + this.missile[_loc_3][2];
                    this.playershotsfired[_loc_7][6] = _loc_3;
                    this.playershotsfired[_loc_7][7] = _loc_10;
                    this.playershotsfired[_loc_7][8] = _loc_4 + this.missile[_loc_3][3];
                    this.playershotsfired[_loc_7][9] = new this.missile[_loc_3][10] as MovieClip;
                    this.gamedisplayarea.addChild(this.playershotsfired[_loc_7][9]);
                    this.playershotsfired[_loc_7][9].x = this.playershotsfired[_loc_7][1] - this.shipcoordinatex;
                    this.playershotsfired[_loc_7][9].y = this.playershotsfired[_loc_7][2] - this.shipcoordinatey;
                    this.playershotsfired[_loc_7][9].rotation = this.playershotsfired[_loc_7][7];
                    this.playershotsfired[_loc_7][9].gotoAndStop(1);
                    _loc_20 = "MF" + "`" + this.playershipstatus[3][0] + "`";
                    _loc_20 = _loc_20 + (this.playershotsfired[_loc_7][1] + "`");
                    _loc_20 = _loc_20 + (this.playershotsfired[_loc_7][2] + "`");
                    _loc_20 = _loc_20 + (Math.round(this.playershotsfired[_loc_7][1] + _loc_13 * this.shipositiondelay * 0.001) + "`");
                    _loc_20 = _loc_20 + (Math.round(this.playershotsfired[_loc_7][2] + _loc_14 * this.shipositiondelay * 0.001) + "`");
                    _loc_20 = _loc_20 + (Math.round(_loc_11) + "`");
                    _loc_20 = _loc_20 + (Math.round(_loc_10 * 100) / 100 + "`");
                    _loc_20 = _loc_20 + (_loc_3 + "`");
                    _loc_20 = _loc_20 + (_loc_19 + "`");
                    _loc_20 = _loc_20 + _loc_15;
                    _loc_20 = _loc_20 + "~";
                    this.gunShotBufferData = this.gunShotBufferData + _loc_20;
                    this.func_fireMissileSound(_loc_3, _loc_16, _loc_17);
                    this.playershotsfired[_loc_7][13] = "MISSILE";
                    this.playershotsfired[_loc_7][14] = this.missile[_loc_3][1];
                    this.playershotsfired[_loc_7][15] = "";
                    this.playershipstatus[7][_loc_2][1] = _loc_6;
                }
            }
            return;
        }// end function

        public function tradegoods_Click(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.gameTutorial.visible = false;
            gotoAndStop("TradeGoods");
            return;
        }// end function

        public function func_PingTimer_lastpingCame(param1)
        {
            var _loc_2:* = getTimer();
            this.gameplaystatus[1][0] = _loc_2 - param1;
            var _loc_3:* = "Latency: " + this.gameplaystatus[1][0] + "ms Current";
            this.lastpingcheck = _loc_2 + this.pingintervalcheck;
            this.GamePingTimer.text = _loc_3;
            return;
        }// end function

       public function loadplayerdata(param1)
        {
            var subdata:* = undefined;
            var nest_iter:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_14:* = undefined;
            var _loc_15:* = undefined;
            var _loc_16:* = undefined;
            var _loc_17:* = undefined;
            var _loc_18:* = undefined;
            var extra_ships:* = "";
            var split_data:* = param1.split("~");
            var iter:* = 0;
			
            while (iter < (split_data.length - 1))
            {
				trace("Call this~!");
                if (split_data[iter].substr(0, 2) == "PI")
                {
                    
                    
                    
                    subdata = split_data[iter].split("`");
					trace("Maybe call this~!" + subdata[1]);
                    if (subdata[1].substr(0, 2) == "ST")
                    {
						trace("Hope I made it this far~!");
                        this.playershipstatus[5][0] = int(subdata[1].substr("2"));
                        this.playershipstatus[2][0] = int(subdata[2].substr("2"));
                        this.playershipstatus[1][0] = int(subdata[3].substr("2"));
                        this.playershipstatus[1][5] = int(subdata[4].substr("2"));
                        this.playershipstatus[3][1] = Number(subdata[5].substr("2"));
                        this.playershipstatus[5][1] = subdata[6].substr("2");
                        this.playershipstatus[4][0] = subdata[7];
                        nest_iter = 0;
                        this.playershipstatus[0] = new Array();
                        this.playershipstatus[8] = new Array();
                        this.playershipstatus[11][1] = new Array();
                        _loc_7 = 0;
                        this.initializemissilebanks();
                        while (nest_iter < (subdata.length - 1))
                        {
                            
                            _loc_8 = nest_iter;
                            if (subdata[nest_iter].substr(0, 2) == "HP")
                            {
                                _loc_9 = subdata[nest_iter].split("G");
                                _loc_10 = _loc_9[0].substr("2");
                                this.playershipstatus[0][_loc_10] = new Array();
                                if (isNaN(_loc_9[1]))
                                {
                                    _loc_9[1] = "none";
                                }
                                else if (Number(_loc_9[1]) < 0)
                                {
                                    _loc_9[1] = "none";
                                }
                                this.playershipstatus[0][_loc_10][0] = _loc_9[1];
                                nest_iter = nest_iter + 1;
                                continue;
                            }
                            if (subdata[nest_iter].substr(0, 2) == "TT")
                            {
                                _loc_9 = subdata[nest_iter].split("G");
                                _loc_11 = _loc_9[0].substr("2");
                                this.playershipstatus[8][_loc_11] = new Array();
                                if (isNaN(_loc_9[1]))
                                {
                                    _loc_9[1] = "none";
                                }
                                else if (Number(_loc_9[1]) < 0)
                                {
                                    _loc_9[1] = "none";
                                }
                                this.playershipstatus[8][_loc_11][0] = _loc_9[1];
                                nest_iter = nest_iter + 1;
                                continue;
                            }
                            if (subdata[nest_iter].substr(0, 2) == "CO")
                            {
                                _loc_12 = subdata[nest_iter].split("A");
                                _loc_13 = _loc_12[0].substr("2");
                                this.playershipstatus[4][1][_loc_13] = int(_loc_12[1]);
                                nest_iter = nest_iter + 1;
                                continue;
                            }
                            if (subdata[nest_iter].substr(0, 2) == "SP")
                            {
                                _loc_12 = subdata[nest_iter].split("Q");
                                trace(_loc_12);
                                _loc_7 = this.playershipstatus[11][1].length;
                                this.playershipstatus[11][1][_loc_7] = new Array();
                                this.playershipstatus[11][1][_loc_7][0] = Number(_loc_12[0].substr("2"));
                                this.playershipstatus[11][1][_loc_7][1] = Number(_loc_12[1]);
                                nest_iter = nest_iter + 1;
                                continue;
                            }
                            if (subdata[nest_iter].substr(0, 2) == "MB")
                            {
                                trace(" Missile Found ");
                                _loc_14 = subdata[nest_iter].split("T");
                                _loc_15 = Number(_loc_14[0].substr(2));
                                _loc_16 = _loc_14[1].split("Q");
                                _loc_17 = Number(_loc_16[0]);
                                _loc_18 = Number(_loc_16[1]);
                                trace(" Missile Found :" + _loc_17 + "~" + this.missile.length);
                                if (_loc_17 < this.missile.length)
                                {
                                    if (this.missile[_loc_17] != null)
                                    {
                                        if (this.playershipstatus[7][_loc_15] != null)
                                        {
                                            this.playershipstatus[7][_loc_15][4][_loc_17] = _loc_18;
                                            if (this.playershipstatus[7][_loc_15][4][_loc_17] > this.playershipstatus[7][_loc_15][5])
                                            {
                                            }
                                        }
                                    }
                                }
                                nest_iter = nest_iter + 1;
                                continue;
                            }
                            nest_iter = nest_iter + 1;
                        }
                    }
                }
                if (split_data[iter].substr(0, 5) == "score")
                {
                    subdata = split_data[iter].split("`");
                    this.playershipstatus[5][9] = Number(subdata[1]);
                    if (isNaN(this.playershipstatus[5][9]))
                    {
                        this.playershipstatus[5][9] = Number(0);
                    }
                    this.playersSessionScoreStart = this.playershipstatus[5][9];
                }
                if (split_data[iter].substr(0, 2) == "NO")
                {
                    extra_ships = extra_ships + (split_data[iter] + "~");
                }
                if (split_data[iter].substr(0, 2) == "SH")
                {
                    extra_ships = extra_ships + (split_data[iter] + "~");
                }
                if (split_data[iter].substr(0, 3) == "bty")
                {
                    subdata = split_data[iter].split("`");
                    this.playershipstatus[5][8] = Number(subdata[1]);
                    if (this.playershipstatus[5][8] < 0)
                    {
                        this.playershipstatus[5][8] = 0;
                    }
                }
                if (split_data[iter].substr(0, 5) == "squad")
                {
                    trace("squad arguments passed");
                    subdata = split_data[iter].split("`");
                    this.playershipstatus[5][10] = subdata[1];
                    this.playershipstatus[5][13] = subdata[3];
                    this.playershipstatus[5][11] = false;
                    if (subdata[2] == this.playershipstatus[3][2])
                    {
                        this.playershipstatus[5][11] = true;
                    }
                    trace("squad owner:" + subdata[2] + " ` " + this.playershipstatus[5][11] + " ` " + this.playershipstatus[3][2]);
                }
                if (split_data[iter].substr(0, 4) == "fund")
                {
                    subdata = split_data[iter].split("`");
                    this.playerfunds = Number(subdata[1]);
                    if (this.playerfunds != 0)
                    {
                        this.playershipstatus[3][1] = this.playerfunds;
                    }
                }
                if (split_data[iter].substr(0, 2) == "AD")
                {
                    subdata = split_data[iter].split("`");
                    this.playershipstatus[5][12] = subdata[1].toUpperCase();
                }
                if (split_data[iter].substr(0, 3) == "SAF")
                {
                    subdata = split_data[iter].split("`");
                    this.playershipstatus[5][25] = "YES";
                }
                if (split_data[iter].substr(0, 6) == "BANNED")
                {
                    subdata = split_data[iter].split("`");
                    this.gameerror = "banned";
                    this.timebannedfor = subdata[1];
                    gotoAndStop("gameclose");
                }
                if (split_data[iter].substr(0, 2) == "EM")
                {
                    subdata = split_data[iter].split("`");
                    this.playershipstatus[3][5] = String(subdata[1]);
                }
                if (split_data[iter].substr(0, 4) == "RACE")
                {
                    subdata = split_data[iter].split("`");
                    this.playershipstatus[5][26] = int(subdata[1]);
                }
                iter = iter + 1;
            }
            this.loadplayerextraships(extra_ships);
            return;
        }// end function 

        public function myhittest(param1, param2, param3, param4, param5, param6)
        {
            if (param3 - param5 > param1)
            {
                return false;
            }
            if (param3 + param5 < -param1)
            {
                return false;
            }
            if (param4 - param6 > param2)
            {
                return false;
            }
            if (param4 + param6 < -param2)
            {
                return false;
            }
            return true;
        }// end function

        public function func_exitOptionsScreen(event:MouseEvent) : void
        {
            Object(root).removeEventListener(KeyboardEvent.KEY_DOWN, this.gameOptionsScreen.OptionsKeyListener);
            this.func_playRegularClick();
            this.func_RefreshChat();
            gotoAndStop("dockedscreen");
            return;
        }// end function

        public function autoTurretsSelected(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_setTurrets("auto");
            return;
        }// end function

        public function func_processgameinfo(param1)
        {
            var _loc_2:* = param1.split("~");
            var _loc_3:* = 1;
            while (_loc_3 < (_loc_2.length - 1))
            {
                
                if (_loc_2[_loc_3].substr(0, 4) == "SHIP")
                {
                    this.func_addshipintoclient(_loc_2[_loc_3].split("`"));
                }
                else if (_loc_2[_loc_3].substr(0, 6) == "GNTYPE")
                {
                    this.func_addgunintocliet(_loc_2[_loc_3].split("`"));
                }
                else if (_loc_2[_loc_3].substr(0, 7) == "SHLDGEN")
                {
                    this.func_addshieldgenintocliet(_loc_2[_loc_3].split("`"));
                }
                else if (_loc_2[_loc_3].substr(0, 3) == "CGO")
                {
                    this.func_addTradeGoodIntoClient(_loc_2[_loc_3].split("`"));
                }
                else if (_loc_2[_loc_3].substr(0, 5) == "ENCAP")
                {
                    this.func_addEnergyCapintocliet(_loc_2[_loc_3].split("`"));
                }
                else if (_loc_2[_loc_3].substr(0, 5) == "ENGEN")
                {
                    this.func_addEnergyGenintocliet(_loc_2[_loc_3].split("`"));
                }
                else if (_loc_2[_loc_3].substr(0, 6) == "MSTYPE")
                {
                    this.func_addMissileTypeintocliet(_loc_2[_loc_3].split("`"));
                }
                _loc_3 = _loc_3 + 1;
            }
            this.loginmovie.gotoAndPlay("start");
            return;
        }// end function

        public function func_addshipintoclient(param1)
        {
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_2:* = int(param1[1]);
            this.shiptype[_loc_2] = new Array();
            this.shiptype[_loc_2][0] = param1[2];
            this.shiptype[_loc_2][1] = Number(param1[3]);
            this.shiptype[_loc_2][2] = new Array();
            var _loc_3:* = param1[23].split(",");
            var _loc_4:* = 0;
            while (_loc_4 < (_loc_3.length - 1))
            {
                
                _loc_7 = _loc_3[_loc_4].split("=");
                this.shiptype[_loc_2][2][_loc_4] = new Array();
                this.shiptype[_loc_2][2][_loc_4][0] = Number(_loc_7[0]);
                this.shiptype[_loc_2][2][_loc_4][1] = Number(_loc_7[1]);
                _loc_4 = _loc_4 + 1;
            }
            this.shiptype[_loc_2][3] = new Array();
            this.shiptype[_loc_2][3][0] = Number(param1[4]);
            this.shiptype[_loc_2][3][1] = Number(param1[5]);
            this.shiptype[_loc_2][3][2] = Number(param1[6]);
            this.shiptype[_loc_2][3][3] = Number(param1[7]);
            this.shiptype[_loc_2][3][4] = Number(param1[8]);
            this.shiptype[_loc_2][3][5] = Number(param1[9]) / 100;
            this.shiptype[_loc_2][3][6] = Number(param1[10]);
            this.shiptype[_loc_2][3][7] = Number(param1[11]);
            if (param1[12] == "true")
            {
                this.shiptype[_loc_2][3][8] = true;
            }
            else
            {
                this.shiptype[_loc_2][3][8] = false;
            }
            if (param1[13] == "true")
            {
                this.shiptype[_loc_2][3][10] = true;
            }
            else
            {
                this.shiptype[_loc_2][3][10] = false;
            }
            if (param1[14] == "true")
            {
                this.shiptype[_loc_2][3][11] = true;
            }
            else
            {
                this.shiptype[_loc_2][3][11] = false;
            }
            if (param1[15] == "true")
            {
                this.shiptype[_loc_2][3][12] = true;
            }
            else
            {
                this.shiptype[_loc_2][3][12] = false;
            }
            if (param1[16] == "true")
            {
                this.shiptype[_loc_2][3][13] = true;
            }
            else
            {
                this.shiptype[_loc_2][3][13] = false;
            }
            this.shiptype[_loc_2][4] = Number(param1[17]);
            this.shiptype[_loc_2][5] = new Array();
            var _loc_5:* = param1[25].split(",");
            _loc_4 = 0;
            while (_loc_4 < (_loc_5.length - 1))
            {
                
                _loc_8 = _loc_5[_loc_4].split("=");
                this.shiptype[_loc_2][5][_loc_4] = new Array();
                this.shiptype[_loc_2][5][_loc_4][0] = Number(_loc_8[0]);
                this.shiptype[_loc_2][5][_loc_4][1] = Number(_loc_8[1]);
                _loc_4 = _loc_4 + 1;
            }
            this.shiptype[_loc_2][6] = new Array();
            this.shiptype[_loc_2][6][0] = Number(param1[18]);
            this.shiptype[_loc_2][6][1] = Number(param1[19]);
            this.shiptype[_loc_2][6][2] = Number(param1[20]);
            this.shiptype[_loc_2][6][3] = Number(param1[21]);
            this.shiptype[_loc_2][6][4] = Number(param1[22]);
            this.shiptype[_loc_2][7] = new Array();
            var _loc_6:* = param1[24].split(",");
            _loc_4 = 0;
            while (_loc_4 < (_loc_6.length - 1))
            {
                
                _loc_9 = _loc_6[_loc_4].split("=");
                this.shiptype[_loc_2][7][_loc_4] = new Array();
                this.shiptype[_loc_2][7][_loc_4][2] = Number(_loc_9[0]);
                this.shiptype[_loc_2][7][_loc_4][3] = Number(_loc_9[1]);
                this.shiptype[_loc_2][7][_loc_4][5] = Number(_loc_9[2]);
                _loc_4 = _loc_4 + 1;
            }
            this.shiptype[_loc_2][10] = getDefinitionByName("shiptype" + _loc_2) as Class;
            this.shiptype[_loc_2][11] = getDefinitionByName("shiptype" + _loc_2 + "shield") as Class;
            this.shiptype[_loc_2][9] = Number(param1[25]);
            return;
        }// end function

        public function pingTimerScript(event:Event)
        {
            var _loc_3:* = undefined;
            var _loc_2:* = getTimer();
            if (this.IsSocketConnected)
            {
                if (_loc_2 > this.lastpingcheck)
                {
                    this.lastpingcheck = this.lastpingcheck + this.pingintervalcheck;
                    _loc_3 = "PING`" + _loc_2 + "~";
                    this.mysocket.send(_loc_3);
                    this.remoteupdate = false;
                }
            }
            return;
        }// end function

        public function MapTimerHandler(event:TimerEvent) : void
        {
            this.playersdestination[0] = this.gameMap.func_PlayerMapLocation(this.shipcoordinatex, this.shipcoordinatey, this.AnglePlayerShipFacing);
            this.func_setHeadingLocation();
            return;
        }// end function

        public function func_ExitSquadBaseButton_Click(event:MouseEvent) : void
        {
            if (1 == 1)
            {
                this.func_playRegularClick();
                gotoAndStop("maingameplaying");
            }
            return;
        }// end function

        public function func_processMissileHit(param1)
        {
            var cc:*;
            var currentnumber:*;
            var basethatgothit:*;
            var shotdamage:*;
            var qz:*;
            var currentthread:* = param1;
            trace(currentthread);
            var playerwhogothit:* = currentthread[1];
            var shooterofbullet:* = currentthread[2];
            var shooternumberfire:* = currentthread[3];
            if (playerwhogothit == "SQ")
            {
            }
            else if (Number(playerwhogothit) < 5000 && Number(playerwhogothit) > 3000)
            {
                basethatgothit = playerwhogothit - 4000;
                basethatgothit = this.teambases[basethatgothit][0];
                shotdamage = 0;
                if (this.playershipstatus[3][0] != shooterofbullet)
                {
                    cc = 0;
                    while (cc < this.othermissilefire.length)
                    {
                        
                        if (this.othermissilefire[cc][0] == shooterofbullet && this.othermissilefire[cc][9] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing otherplayershot - basehit: " + error);
                            }
                            this.othermissilefire.splice(cc, 1);
                            break;
                        }
                        cc = (cc + 1);
                    }
                }
                else if (this.playershipstatus[3][0] == shooterofbullet)
                {
                    trace("Hit Base Detected");
                    currentnumber = 0;
                    while (currentnumber < this.playershotsfired.length)
                    {
                        
                        if (this.playershotsfired[currentnumber][0] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing playershot - gunhit: " + error);
                            }
                            this.playershotsfired.splice(currentnumber, 1);
                            trace("Removed Shot");
                            break;
                        }
                        currentnumber = (currentnumber + 1);
                    }
                }
            }
            else if (Number(playerwhogothit) < 6000 && Number(playerwhogothit) >= 5000)
            {
                basethatgothit = playerwhogothit - 4000;
                basethatgothit = this.teambases[basethatgothit][0];
                shotdamage = 0;
                if (this.playershipstatus[3][0] != shooterofbullet)
                {
                    cc = 0;
                    while (cc < this.othermissilefire.length)
                    {
                        
                        if (this.othermissilefire[cc][0] == shooterofbullet && this.othermissilefire[cc][9] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing otherplayershot - basehit: " + error);
                            }
                            this.othermissilefire.splice(cc, 1);
                            break;
                        }
                        cc = (cc + 1);
                    }
                }
                else if (this.playershipstatus[3][0] == shooterofbullet)
                {
                    trace("Hit Base Detected");
                    currentnumber = 0;
                    while (currentnumber < this.playershotsfired.length)
                    {
                        
                        if (this.playershotsfired[currentnumber][0] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing playershot - gunhit: " + error);
                            }
                            this.playershotsfired.splice(currentnumber, 1);
                            trace("Removed Shot");
                            break;
                        }
                        currentnumber = (currentnumber + 1);
                    }
                }
            }
            else if (Number(playerwhogothit) < 8000 && Number(playerwhogothit) >= 7000)
            {
                basethatgothit = playerwhogothit - 7000;
                shotdamage = 0;
                if (this.playershipstatus[3][0] != shooterofbullet)
                {
                    cc = 0;
                    while (cc < this.othermissilefire.length)
                    {
                        
                        if (this.othermissilefire[cc][0] == shooterofbullet && this.othermissilefire[cc][9] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing otherplayershot - basehit: " + error);
                            }
                            this.othermissilefire.splice(cc, 1);
                            break;
                        }
                        cc = (cc + 1);
                    }
                }
                else if (this.playershipstatus[3][0] == shooterofbullet)
                {
                    trace("Hit Base Detected");
                    currentnumber = 0;
                    while (currentnumber < this.playershotsfired.length)
                    {
                        
                        if (this.playershotsfired[currentnumber][0] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing playershot - gunhit: " + error);
                            }
                            this.playershotsfired.splice(currentnumber, 1);
                            trace("Removed Shot");
                            break;
                        }
                        currentnumber = (currentnumber + 1);
                    }
                }
            }
            else if (Number(playerwhogothit) < this.starbaseLevel + 1000 && Number(playerwhogothit) >= this.starbaseLevel)
            {
                basethatgothit = playerwhogothit - this.starbaseLevel;
                shotdamage = 0;
                if (this.playershipstatus[3][0] != shooterofbullet)
                {
                    cc = 0;
                    while (cc < this.othermissilefire.length)
                    {
                        
                        if (this.othermissilefire[cc][0] == shooterofbullet && this.othermissilefire[cc][9] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing otherplayershot - basehit: " + error);
                            }
                            this.othermissilefire.splice(cc, 1);
                            break;
                        }
                        cc = (cc + 1);
                    }
                }
                else if (this.playershipstatus[3][0] == shooterofbullet)
                {
                    trace("Hit Base Detected");
                    currentnumber = 0;
                    while (currentnumber < this.playershotsfired.length)
                    {
                        
                        if (this.playershotsfired[currentnumber][0] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.playershotsfired[currentnumber][9]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing playershot - gunhit: " + error);
                            }
                            this.playershotsfired.splice(currentnumber, 1);
                            trace("Removed Shot");
                            break;
                        }
                        currentnumber = (currentnumber + 1);
                    }
                }
            }
            else if (playerwhogothit == "MET")
            {
            }
            else
            {
                if (playerwhogothit == "AI")
                {
                    shooterofbullet = currentthread[3];
                    shooternumberfire = Number(currentthread[4]);
                }
                else
                {
                    qz = 0;
                    while (qz < this.otherplayership.length)
                    {
                        
                        if (playerwhogothit == this.otherplayership[qz][0])
                        {
                            this.otherplayership[qz][57].gotoAndPlay(1);
                            if (this.otherplayership[qz][21].alpha < 0.9)
                            {
                                this.otherplayership[qz][21].alpha = 0.9;
                            }
                            break;
                        }
                        qz = (qz + 1);
                    }
                }
                if (this.playershipstatus[3][0] != shooterofbullet)
                {
                    cc = 0;
                    while (cc < this.othermissilefire.length)
                    {
                        
                        if (this.othermissilefire[cc][0] == shooterofbullet && this.othermissilefire[cc][9] == shooternumberfire)
                        {
                            try
                            {
                                this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                            }
                            catch (error:Error)
                            {
                                trace("Error removing otherplayershot - MIssileHitonBase: " + error);
                            }
                            this.othermissilefire.splice(cc, 1);
                            break;
                        }
                        cc = (cc + 1);
                    }
                }
                if (this.playershipstatus[3][0] == shooterofbullet)
                {
                    this.func_removePlayersShotFromGlobalID(shooternumberfire);
                }
            }
            return;
        }// end function

        public function func_moveAnothership(param1)
        {
            var _loc_2:* = 0;
            var _loc_3:* = getTimer();
            _loc_2 = 0;
            while (_loc_2 < this.otherplayership.length)
            {
                
                this.func_moveanothership(param1, _loc_2, _loc_3);
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_addMissileTypeintocliet(param1)
        {
            var _loc_2:* = Number(param1[1]);
            this.missile[_loc_2] = new Array();
            this.missile[_loc_2][0] = Number(param1[3]);
            this.missile[_loc_2][1] = Number(param1[8]);
            this.missile[_loc_2][2] = Number(param1[4]);
            this.missile[_loc_2][3] = Number(param1[5]);
            this.missile[_loc_2][4] = Number(param1[6]);
            this.missile[_loc_2][5] = Number(param1[7]);
            this.missile[_loc_2][6] = param1[2];
            this.missile[_loc_2][7] = param1[10];
            this.missile[_loc_2][8] = param1[9];
            this.missile[_loc_2][9] = Number(param1[11]);
            this.missile[_loc_2][10] = getDefinitionByName("missile" + _loc_2 + "fire") as Class;
            this.missile[_loc_2][11] = 0;
            return;
        }// end function

        public function func_runMissileFireScript(param1)
        {
            var _loc_2:* = 0;
            while (_loc_2 < this.playershipstatus[7].length)
            {
                
                if (this.playershipstatus[7][_loc_2] != null)
                {
                    if (this.playershipstatus[7][_loc_2][1] <= param1)
                    {
                        if (this.playershipstatus[10][1] != "Single" || this.playershipstatus[10][0] == _loc_2)
                        {
                            this.func_fire_a_missile(_loc_2);
                        }
                    }
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function func_RefreshChat()
        {
            var _loc_1:* = undefined;
            var _loc_2:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            _loc_1 = 9;
            if (this.gamesetting.chatTextSize == "M")
            {
                _loc_1 = 11;
            }
            else if (this.gamesetting.chatTextSize == "L")
            {
                _loc_1 = 12;
            }
            _loc_2 = this.chatDisplay.textdisplay.scrollV;
            var _loc_3:* = this.chatDisplay.textdisplay.numLines;
            _loc_4 = true;
            if (_loc_2 == this.chatDisplay.textdisplay.maxScrollV)
            {
                _loc_4 = true;
            }
            _loc_5 = "";
            _loc_6 = this.gamechatinfo[2][1];
            while (_loc_6 >= 0)
            {
                
                _loc_5 = _loc_5 + ("<font size=\"" + _loc_1 + "\" color=\"" + this.gamechatinfo[1][_loc_6][1] + "\">" + this.gamechatinfo[1][_loc_6][0] + "</FONT>\r");
                _loc_6 = _loc_6 - 1;
            }
            this.chatDisplay.textdisplay.htmlText = _loc_5;
            if (_loc_4)
            {
                this.chatDisplay.textdisplay.scrollV = this.chatDisplay.textdisplay.maxScrollV;
            }
            else
            {
                this.chatDisplay.textdisplay.scrollV = _loc_2;
            }
            return;
        }// end function

        public function ChangeZoom(param1)
        {
            if (param1 == "in")
            {
                var _loc_2:* = this;
                var _loc_3:* = this.currZoomFactor - 1;
                _loc_2.currZoomFactor = _loc_3;
                if (this.currZoomFactor < 0)
                {
                    this.currZoomFactor = 0;
                }
            }
            else
            {
                var _loc_2:* = this;
                var _loc_3:* = this.currZoomFactor + 1;
                _loc_2.currZoomFactor = _loc_3;
                if (this.currZoomFactor >= this.arrayZoomFactors.length)
                {
                    this.currZoomFactor = this.arrayZoomFactors.length - 1;
                }
            }
            return;
        }// end function

        public function func_postsaveresults(param1)
        {
            var _loc_2:* = undefined;
            _loc_2 = param1;
            this.func_reportSavedProgress("Game Saved");
            if (_loc_2[1] == "savesuccess")
            {
                this.playershipstatus[5][19] = _loc_2[2];
            }
            else
            {
                this.playershipstatus[5][19] = _loc_2[2];
            }
            return;
        }// end function

        public function offTurretsSelected(event:MouseEvent) : void
        {
            this.func_playRegularClick();
            this.func_setTurrets("off");
            return;
        }// end function

        public function func_moveanothership(param1, param2, param3)
        {
            var playersCurRot:*;
            var PlayerSpeedRatio:*;
            var thetimeelapsed:* = param1;
            var shipno:* = param2;
            var CurrentTime:* = param3;
            var timeelapsed:* = thetimeelapsed;
            var zz:* = shipno;
            timeelapsed = (CurrentTime - this.otherplayership[zz][5][9]) / 1000;
            if (timeelapsed > 0.9)
            {
                timeelapsed = 0;
            }
            this.otherplayership[zz][5][9] = CurrentTime;
            var otherplayershtype:* = this.otherplayership[zz][11];
            var otherplayersmaxvelocity:* = this.shiptype[otherplayershtype][3][1];
            var otherplayersafterburner:* = this.shiptype[otherplayershtype][3][6];
            var velocity:*;
            if (this.otherplayership[zz][5][0] == "S")
            {
                var _loc_5:* = this.otherplayership[zz][4] + this.shiptype[this.otherplayership[zz][11]][3][0] * timeelapsed * 3;
                this.otherplayership[zz][4] = this.otherplayership[zz][4] + this.shiptype[this.otherplayership[zz][11]][3][0] * timeelapsed * 3;
                velocity = _loc_5;
                if (velocity > otherplayersafterburner)
                {
                    var _loc_5:* = otherplayersafterburner;
                    this.otherplayership[zz][4] = otherplayersafterburner;
                    velocity = _loc_5;
                }
            }
            else if (isNaN(Number(this.otherplayership[zz][5][0])))
            {
                velocity = 0;
            }
            else
            {
                this.otherplayership[zz][5][0] = Number(this.otherplayership[zz][5][0]);
                this.otherplayership[zz][4] = this.otherplayership[zz][4] + this.otherplayership[zz][5][0] * timeelapsed;
                if (this.otherplayership[zz][4] > otherplayersmaxvelocity)
                {
                    this.otherplayership[zz][4] = this.otherplayership[zz][4] - this.shiptype[this.otherplayership[zz][11]][3][0] * timeelapsed * 2;
                    if (this.otherplayership[zz][4] < otherplayersmaxvelocity)
                    {
                        this.otherplayership[zz][4] = otherplayersmaxvelocity;
                    }
                }
                velocity = this.otherplayership[zz][4];
                if (velocity < 0)
                {
                    velocity = 0;
                    this.otherplayership[zz][4] = 0;
                }
            }
            var otherplayershiprotating:* = this.otherplayership[zz][5][1] * timeelapsed;
            this.otherplayership[zz][3] = this.otherplayership[zz][3] + otherplayershiprotating;
            if (this.playershipstatus[3][0] == this.otherplayership[zz][0])
            {
                playersCurRot = this.PlayersShipImage.rotation;
                if (playersCurRot < 0)
                {
                    playersCurRot = playersCurRot + 360;
                }
                if (Math.abs(playersCurRot - this.otherplayership[zz][3]) > 90)
                {
                }
            }
            if (isNaN(this.otherplayership[zz][0]))
            {
            }
            if (this.otherplayership[zz][3] > 360)
            {
                this.otherplayership[zz][3] = this.otherplayership[zz][3] - 360;
            }
            if (this.otherplayership[zz][3] < 0)
            {
                this.otherplayership[zz][3] = this.otherplayership[zz][3] + 360;
            }
            var relativefacing:* = this.otherplayership[zz][3];
            velocity = velocity * timeelapsed;
            var ShipMoved:* = this.MovingObjectWithThrust(relativefacing, velocity);
            this.otherplayership[zz][1] = this.otherplayership[zz][1] + ShipMoved[0];
            this.otherplayership[zz][2] = this.otherplayership[zz][2] + ShipMoved[1];
            if (this.otherplayership[zz][36] > 1)
            {
                this.otherplayership[zz][37] = this.otherplayership[zz][37] + ShipMoved[0];
                this.otherplayership[zz][38] = this.otherplayership[zz][38] + ShipMoved[1];
                this.otherplayership[zz][37] = this.otherplayership[zz][37] + (this.otherplayership[zz][1] - this.otherplayership[zz][37]) / this.otherplayership[zz][36];
                this.otherplayership[zz][38] = this.otherplayership[zz][38] + (this.otherplayership[zz][2] - this.otherplayership[zz][38]) / this.otherplayership[zz][36];
                var _loc_5:* = this.otherplayership[zz];
                var _loc_6:* = 36;
                var _loc_7:* = _loc_5[36] - 1;
                _loc_5[_loc_6] = _loc_7;
            }
            else
            {
                _loc_5[37] = _loc_5[1];
                _loc_5[38] = _loc_5[2];
            }
            if (this.playershipstatus[3][0] != _loc_5[0])
            {
                try
                {
                    _loc_5[21].setSpeedRatio(_loc_5[4] / otherplayersmaxvelocity);
                }
                catch (error:Error)
                {
                }
                this.otherplayership[zz][59].x = this.otherplayership[zz][1];
                this.otherplayership[zz][59].y = this.otherplayership[zz][2];
            }
            else
            {
                if (this.playershipstatus[5][4] != "dead")
                {
                    this.AnglePlayerShipFacing = this.otherplayership[zz][3];
                    if (this.otherplayership[zz][36] > 0)
                    {
                        this.shipcoordinatex = this.otherplayership[zz][37];
                        this.shipcoordinatey = this.otherplayership[zz][38];
                    }
                    else
                    {
                        this.shipcoordinatex = this.otherplayership[zz][1];
                        this.shipcoordinatey = this.otherplayership[zz][2];
                    }
                    this.playershipvelocity = this.otherplayership[zz][4];
                }
                else
                {
                    this.playershipvelocity = 0;
                }
                try
                {
                    PlayerSpeedRatio = this.otherplayership[zz][4] / otherplayersmaxvelocity;
                    if (PlayerSpeedRatio > 1)
                    {
                        if (this.isshiftkeypressed)
                        {
                            this.PlayersShipImage.setSpeedRatio(this.otherplayership[zz][4] / otherplayersmaxvelocity);
                        }
                        else
                        {
                            this.PlayersShipImage.setSpeedRatio(1);
                        }
                    }
                    else
                    {
                        this.PlayersShipImage.setSpeedRatio(this.otherplayership[zz][4] / otherplayersmaxvelocity);
                    }
                }
                catch (error:Error)
                {
                    trace("Could Not setPlayers Engine Ratio:" + error);
                }
            }
            if (this.playershipstatus[3][0] != this.otherplayership[zz][0])
            {
                if (this.otherplayership[zz][47] > 0)
                {
                    this.otherplayership[zz][41] = this.otherplayership[zz][41] + timeelapsed * this.otherplayership[zz][42];
                    if (this.otherplayership[zz][41] > this.otherplayership[zz][43])
                    {
                        this.otherplayership[zz][41] = this.otherplayership[zz][43];
                    }
                    try
                    {
                        this.otherplayership[zz][53].func_setLifeSettings(this.otherplayership[zz][40] / this.otherplayership[zz][47], this.otherplayership[zz][41] / this.otherplayership[zz][43]);
                    }
                    catch (error:Error)
                    {
                    }
                }
                else
                {
                    this.otherplayership[zz][53].visible = false;
                }
                if (this.otherplayership[zz][16].length > 1)
                {
                    this.otherplayership[zz][59].alpha = this.otherplayership[zz][59].alpha - 0.007;
                    if (this.otherplayership[zz][56] == "friend")
                    {
                        if (this.otherplayership[zz][59].alpha < 0.5)
                        {
                            this.otherplayership[zz][59].alpha = 0.5;
                        }
                    }
                    else if (this.otherplayership[zz][59].alpha < 0)
                    {
                        this.otherplayership[zz][59].alpha = 0;
                    }
                    if (this.otherplayership[zz][16].charAt(0) == "C")
                    {
                        this.otherplayership[zz][21].alpha = this.otherplayership[zz][21].alpha - 0.007;
                        if (this.otherplayership[zz][56] == "friend")
                        {
                            if (this.otherplayership[zz][21].alpha < 0.5)
                            {
                                this.otherplayership[zz][21].alpha = 0.5;
                            }
                        }
                        else if (this.otherplayership[zz][21].alpha < 0)
                        {
                            this.otherplayership[zz][21].alpha = 0;
                        }
                    }
                    else
                    {
                        this.otherplayership[zz][21].alpha = 1;
                    }
                }
                else
                {
                    this.otherplayership[zz][59].alpha = this.otherplayership[zz][59].alpha + 0.08;
                    this.otherplayership[zz][21].alpha = this.otherplayership[zz][21].alpha + 0.08;
                    if (this.otherplayership[zz][59].alpha > 1)
                    {
                        this.otherplayership[zz][59].alpha = 1;
                    }
                    if (this.otherplayership[zz][21].alpha > 1)
                    {
                        this.otherplayership[zz][21].alpha = 1;
                    }
                }
            }
            if (this.otherplayership[zz][58] != null)
            {
                if (this.targetinfo[4] == this.otherplayership[zz][0])
                {
                    this.otherplayership[zz][58].visible = true;
                }
                else
                {
                    this.otherplayership[zz][58].visible = false;
                }
            }
            if (CurrentTime > this.otherplayership[zz][6])
            {
                this.func_RemoveRadarDot(this.otherplayership[zz][59]);
                try
                {
                    this.gamedisplayarea.removeChild(this.otherplayership[zz][21]);
                }
                catch (error:Error)
                {
                    trace("Error: " + error);
                }
                this.otherplayership.splice(zz, 1);
            }
            return;
        }// end function

        public function remove_SquadBaseFrom(param1)
        {
            var _loc_2:* = undefined;
            _loc_2 = 0;
            while (_loc_2 < this.playersquadbases.length)
            {
                
                if (this.playersquadbases[_loc_2] != null)
                {
                    if (this.playersquadbases[_loc_2][0] == param1)
                    {
                        this.gameMap.func_RemoveSquadBase(this.playersquadbases[_loc_2][11]);
                        this.RemoveSquadBaseToGameBackGround(this.playersquadbases[_loc_2][12]);
                        this.playersquadbases.splice(_loc_2, 1);
                    }
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function setRadarScale()
        {
            this.gameRadar.radarScreen.scaleX = 0.05;
            this.gameRadar.radarScreen.scaleY = 0.05;
            return;
        }// end function

        public function func_removePlayersShot(param1)
        {
            var thecurrentnumber:* = param1;
            try
            {
                this.gamedisplayarea.removeChild(this.playershotsfired[thecurrentnumber][9]);
            }
            catch (error:Error)
            {
                trace("Error removing playershot: " + error);
            }
            this.playershotsfired.splice(thecurrentnumber, 1);
            return;
        }// end function

        public function func_detectorPing(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            this.func_PingRadar();
            _loc_2 = 0;
            while (_loc_2 < this.otherplayership.length)
            {
                
                if (this.otherplayership[_loc_2][16].length > 1)
                {
                    _loc_3 = this.otherplayership[_loc_2][16].charAt(0);
                    _loc_4 = Number(this.otherplayership[_loc_2][16].charAt(1));
                    if (_loc_4 <= param1)
                    {
                        if (_loc_3 == "C")
                        {
                            this.otherplayership[_loc_2][21].alpha = 0.95;
                        }
                        this.otherplayership[_loc_2][59].alpha = 1;
                    }
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function othermissilefiremovement(param1)
        {
            var playersx:*;
            var playersy:*;
            var halfplayerswidth:*;
            var halfplayersheight:*;
            var playerwhoshothitid:*;
            var gunshothitplayertype:*;
            var empendsat:*;
            var zzj:*;
            var dataupdated:*;
            var timeChangeRatio:* = param1;
            var curenttime:* = getTimer();
            var MakeMissileSeek:*;
            var cc:*;
            if (this.othermissilefire.length > 0)
            {
                playersx = this.shipcoordinatex;
                playersy = this.shipcoordinatey;
                halfplayerswidth = this.PlayersShipImage.width / 2;
                halfplayersheight = this.PlayersShipImage.height / 2;
            }
            while (cc < this.othermissilefire.length)
            {
                
                this.othermissilefire[cc][1] = this.othermissilefire[cc][1] + this.othermissilefire[cc][3] * timeChangeRatio;
                this.othermissilefire[cc][2] = this.othermissilefire[cc][2] + this.othermissilefire[cc][4] * timeChangeRatio;
                try
                {
                    this.othermissilefire[cc][16].x = this.othermissilefire[cc][1] - this.shipcoordinatex;
                    this.othermissilefire[cc][16].y = this.othermissilefire[cc][2] - this.shipcoordinatey;
                    this.othermissilefire[cc][16].rotation = this.othermissilefire[cc][6];
                }
                catch (error:Error)
                {
                    trace("Error moving other player MIssilefire: " + error);
                }
                if (this.othermissilefire[cc][5] < curenttime)
                {
                    try
                    {
                        this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                    }
                    catch (error:Error)
                    {
                        trace("Error removing otherplayermissile timeout: " + error);
                    }
                    this.othermissilefire.splice(cc, 1);
                    cc = (cc - 1);
                }
                else if (this.othermissilefire[cc][14] == true || this.playershipstatus[5][4] != "alive")
                {
                }
                else if (this.myLineHittest(this.shipcoordinatex, this.shipcoordinatey, Math.round(this.PlayersShipImage.width / 2) + this.othermissilefire[cc][11], this.othermissilefire[cc][18], this.othermissilefire[cc][19], this.othermissilefire[cc][1], this.othermissilefire[cc][2]) && this.playershipstatus[5][4] == "alive")
                {
                    try
                    {
                        this.gamedisplayarea.removeChild(this.othermissilefire[cc][16]);
                    }
                    catch (error:Error)
                    {
                        trace("Error removing otherplayermissile hitship: " + error);
                    }
                    playerwhoshothitid = this.othermissilefire[cc][0];
                    gunshothitplayertype = this.othermissilefire[cc][7];
                    if (this.playershipstatus[5][20] != true)
                    {
                        if (this.missile[gunshothitplayertype][8] == "EMP")
                        {
                            empendsat = getTimer() + this.missile[gunshothitplayertype][9];
                            if (this.playerempend < empendsat)
                            {
                                this.playerempend = empendsat;
                                this.isplayeremp = true;
                            }
                        }
                        else if (this.missile[gunshothitplayertype][8] == "DISRUPTER")
                        {
                            this.disruptendsat = getTimer() + this.missile[gunshothitplayertype][9];
                            if (this.playerdiruptend < this.disruptendsat)
                            {
                                this.disruptplayersengines();
                                this.playerdiruptend = this.disruptendsat;
                                this.isplayerdisrupt = true;
                            }
                        }
                        else if (this.missile[gunshothitplayertype][8] == "PIERCE")
                        {
                            this.playershipstatus[2][5] = this.playershipstatus[2][5] - this.missile[gunshothitplayertype][4];
                        }
                        else
                        {
                            this.playershipstatus[2][1] = this.playershipstatus[2][1] - this.missile[gunshothitplayertype][4];
                        }
                        if (this.playershipstatus[2][1] < 0)
                        {
                            this.playershipstatus[2][5] = this.playershipstatus[2][5] + this.playershipstatus[2][1];
                            this.playershipstatus[2][1] = 0;
                        }
                    }
                    this.gunShotBufferData = this.gunShotBufferData + ("MH" + "`" + this.playershipstatus[3][0] + "`" + this.othermissilefire[cc][0] + "`" + this.othermissilefire[cc][9] + "`~");
                    this.func_playerGotHit();
                    if (this.playershipstatus[2][5] <= 0 && this.playershipstatus[5][4] == "alive")
                    {
                        this.playershipstatus[5][4] = "dead";
                        this.playershipstatus[5][5] = playerwhoshothitid;
                        zzj = 0;
                        dataupdated = false;
                        while (zzj < this.currentonlineplayers.length && dataupdated == false)
                        {
                            
                            if (playerwhoshothitid == this.currentonlineplayers[zzj][0])
                            {
                                dataupdated = true;
                                this.playershipstatus[5][7] = this.currentonlineplayers[zzj][1];
                            }
                            zzj = (zzj + 1);
                        }
                        this.playershipstatus[2][5] = 0;
                        gotoAndPlay("playerdeath");
                    }
                    this.othermissilefire.splice(cc, 1);
                    cc = (cc - 1);
                }
                cc = (cc + 1);
            }
            return;
        }// end function

        public function func_refreshIngameVolumeSetting()
        {
            this.gamesetting.IngameSoundTransform.volume = this.gamesetting.IngameVolume;
            this.gamesetting.IngameChannelSound.soundTransform = this.gamesetting.IngameSoundTransform;
            return;
        }// end function

    }
}
