﻿package sc346_fla
{
    import flash.display.*;

    dynamic public class regcapshipengine_180 extends flash.display.MovieClip
    {

        public function regcapshipengine_180()
        {
            addFrameScript(0, this.frame1, 1, this.frame2);
            return;
        }// end function

        function frame1()
        {
            return;
        }// end function

        function frame2()
        {
            stop();
            return;
        }// end function

        public function setSpeedRatio(param1)
        {
            var _loc_2:* = Math.ceil(param1 * 10 / 2) + 2;
            if (_loc_2 > 8)
            {
                _loc_2 = 8;
            }
            this.gotoAndStop(_loc_2);
            return;
        }// end function

    }
}
