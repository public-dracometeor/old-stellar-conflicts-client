﻿package sc346_fla
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;

    dynamic public class onlineplayersListing_4 extends flash.display.MovieClip
    {
        public var changeWindow:MovieClip;
        public var onlinePlayersListing:TextField;
        public var teamNo:TextField;
        public var playershipstatus:Object;
        public var minimizeButton:SimpleButton;
        public var onlineListDetailsone:TextField;
        public var mysocket:Object;

        public function onlineplayersListing_4()
        {
            addFrameScript(0, this.frame1, 1, this.frame2);
            return;
        }// end function

        public function func_ChangeToTeam(event:MouseEvent) : void
        {
            var _loc_3:* = undefined;
            var _loc_2:* = Number(this.teamNo.text);
            if (!isNaN(_loc_2))
            {
                _loc_2 = Math.round(_loc_2);
                if (_loc_2 < 1)
                {
                    _loc_2 = 0;
                }
                _loc_3 = "TC`" + this.playershipstatus[3][0] + "`TM`" + _loc_2 + "`~";
                this.mysocket.send(_loc_3);
                this.teamNo.text = "Change Sent";
            }
            return;
        }// end function

        function frame1()
        {
            stop();
            return;
        }// end function

        public function func_ChangeToSoloPlayer(event:MouseEvent) : void
        {
            var _loc_2:* = "TC`" + this.playershipstatus[3][0] + "`TM`-1`~";
            this.mysocket.send(_loc_2);
            this.teamNo.text = "Change Sent";
            return;
        }// end function

        function frame2()
        {
            this.changeWindow.changeTeam.addEventListener(MouseEvent.MOUSE_DOWN, this.func_ChangeToTeam);
            this.changeWindow.SoloPlayer.addEventListener(MouseEvent.MOUSE_DOWN, this.func_ChangeToSoloPlayer);
            stop();
            return;
        }// end function

    }
}
