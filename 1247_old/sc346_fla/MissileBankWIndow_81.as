﻿package sc346_fla
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;

    dynamic public class MissileBankWIndow_81 extends flash.display.MovieClip
    {
        public var playersMissileBanks:Object;
        public var BankHolderImage:Class;
        public var firingStyle:Object;
        public var totalMissileTypes:Object;
        public var fireStyle:MovieClip;
        public var SingleFireBank:Object;
        public var BankDisplays:Object;
        public var holderDisplay:bankDisplayInfo;
        public var changingBankMissileType:Object;
        public var missile:Object;

        public function MissileBankWIndow_81()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        public function func_searchForNextMissile(param1, param2)
        {
            var _loc_3:* = 0;
            _loc_3 = param2 + 1;
            while (_loc_3 != param2)
            {
                
                if (_loc_3 >= this.totalMissileTypes)
                {
                    _loc_3 = -1;
                }
                else if (this.playersMissileBanks[param1][4][_loc_3] > 0)
                {
                    this.playersMissileBanks[param1][0] = _loc_3;
                    break;
                }
                _loc_3 = _loc_3 + 1;
            }
            return;
        }// end function

        public function func_buildDisplay(param1, param2, param3)
        {
            this.BankHolderImage = getDefinitionByName("bankDisplayInfo") as Class;
            this.playersMissileBanks = param1;
            this.SingleFireBank = param2;
            this.missile = param3;
            this.totalMissileTypes = this.missile.length;
            this.BankDisplays = new Array();
            this.changingBankMissileType = new Array();
            var _loc_4:* = 0;
            _loc_4 = 0;
            while (_loc_4 < this.playersMissileBanks.length)
            {
                
                this.changingBankMissileType[_loc_4] = false;
                this.BankDisplays[_loc_4] = new Object();
                this.BankDisplays[_loc_4] = new this.BankHolderImage() as MovieClip;
                this.BankDisplays[_loc_4].x = 0;
                this.BankDisplays[_loc_4].y = this.holderDisplay.height * _loc_4 * 0.5;
                this.BankDisplays[_loc_4].name = _loc_4;
                this.BankDisplays[_loc_4].changeButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_MissileTypeChange);
                this.BankDisplays[_loc_4].missilereadiness.addEventListener(MouseEvent.MOUSE_DOWN, this.func_MissileSingleBankChange);
                addChild(this.BankDisplays[_loc_4]);
                _loc_4 = _loc_4 + 1;
            }
            return;
        }// end function

        public function func_MissileSingleBankChange(event:MouseEvent) : void
        {
            var _loc_2:* = Number(event.target.parent.name);
            this.SingleFireBank = _loc_2;
            stage.focus = stage;
            return;
        }// end function

        public function func_RefreshDisplay(param1, param2, param3)
        {
            var _loc_6:* = undefined;
            var _loc_4:* = getTimer();
            if (this.SingleFireBank != param2)
            {
                param2 = this.SingleFireBank;
            }
            this.playersMissileBanks = param1;
            if (param3 == "Single")
            {
                this.fireStyle.gotoAndStop(1);
            }
            else
            {
                this.fireStyle.gotoAndStop(2);
            }
            var _loc_5:* = 0;
            _loc_5 = 0;
            while (_loc_5 < this.playersMissileBanks.length)
            {
                
                if (this.playersMissileBanks[_loc_5][4][this.playersMissileBanks[_loc_5][0]] < 1 || this.changingBankMissileType[_loc_5])
                {
                    this.changingBankMissileType[_loc_5] = false;
                    this.func_searchForNextMissile(_loc_5, this.playersMissileBanks[_loc_5][0]);
                }
                _loc_6 = param1[_loc_5][4][this.playersMissileBanks[_loc_5][0]];
                this.BankDisplays[_loc_5].bankdisplay.text = _loc_5 + ": " + this.missile[this.playersMissileBanks[_loc_5][0]][7] + "(" + _loc_6 + ")";
                this.BankDisplays[_loc_5].missilereadiness.gotoAndStop("ready");
                if (_loc_6 < 1)
                {
                    this.BankDisplays[_loc_5].missilereadiness.gotoAndStop("noammo");
                }
                else if (this.playersMissileBanks[_loc_5][1] > _loc_4)
                {
                    this.BankDisplays[_loc_5].missilereadiness.gotoAndStop("reload");
                }
                else if (param3 == "Single")
                {
                    if (param2 != _loc_5)
                    {
                        this.BankDisplays[_loc_5].missilereadiness.gotoAndStop("noton");
                    }
                }
                _loc_5 = _loc_5 + 1;
            }
            return param2;
        }// end function

        function frame1()
        {
            this.holderDisplay.visible = false;
            stop();
            return;
        }// end function

        public function func_MissileTypeChange(event:MouseEvent) : void
        {
            var _loc_2:* = Number(event.target.parent.name);
            trace(_loc_2);
            trace(this.changingBankMissileType);
            trace(this.changingBankMissileType[_loc_2]);
            this.changingBankMissileType[_loc_2] = true;
            stage.focus = stage;
            return;
        }// end function

    }
}
