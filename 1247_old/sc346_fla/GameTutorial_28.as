﻿package sc346_fla
{
    import flash.display.*;
    import flash.events.*;

    dynamic public class GameTutorial_28 extends flash.display.MovieClip
    {
        public var goBackButton:MovieClip;
        public var hasBeenClosed:Object;
        public var ReentryButton:MovieClip;
        public var ContinueButton:MovieClip;

        public function GameTutorial_28()
        {
            addFrameScript(0, this.frame1, 1, this.frame2, 7, this.frame8, 8, this.frame9, 9, this.frame10, 10, this.frame11, 13, this.frame14, 14, this.frame15, 15, this.frame16);
            return;
        }// end function

        function frame8()
        {
            this.ContinueButton.visible = true;
            return;
        }// end function

        function frame1()
        {
            this.ContinueButton.actionlabel.text = "Continue";
            this.goBackButton.actionlabel.text = "Go Back";
            this.ContinueButton.addEventListener(MouseEvent.MOUSE_DOWN, this.nextFrameClick);
            this.goBackButton.addEventListener(MouseEvent.MOUSE_DOWN, this.prevFrameClick);
            this.ContinueButton.gotoAndStop(1);
            this.goBackButton.gotoAndStop(1);
            if (this.hasBeenClosed)
            {
                this.gotoAndStop("closed");
            }
            stop();
            this.goBackButton.visible = false;
            return;
        }// end function

        function frame2()
        {
            this.goBackButton.visible = true;
            return;
        }// end function

        public function reEntryClick(event:MouseEvent) : void
        {
            Object(root).relogInGame = true;
            Object(root).mysocket.close();
            Object(root).gotoAndPlay(1);
            return;
        }// end function

        function frame9()
        {
            this.ContinueButton.visible = false;
            return;
        }// end function

        function frame11()
        {
            this.goBackButton.visible = true;
            return;
        }// end function

        public function prevFrameClick(event:MouseEvent) : void
        {
            prevFrame();
            stop();
            return;
        }// end function

        function frame14()
        {
            this.ContinueButton.actionlabel.text = "Continue";
            return;
        }// end function

        function frame10()
        {
            this.ContinueButton.actionlabel.text = "Continue";
            this.goBackButton.actionlabel.text = "Go Back";
            this.ContinueButton.addEventListener(MouseEvent.MOUSE_DOWN, this.nextFrameClick);
            this.goBackButton.addEventListener(MouseEvent.MOUSE_DOWN, this.prevFrameClick);
            this.ContinueButton.gotoAndStop(1);
            this.goBackButton.gotoAndStop(1);
            if (this.hasBeenClosed)
            {
                this.gotoAndStop("closed");
            }
            this.hasBeenClosed = true;
            stop();
            this.goBackButton.visible = false;
            this.ContinueButton.visible = true;
            return;
        }// end function

        function frame16()
        {
            this.hasBeenClosed = true;
            this.ReentryButton.actionlabel.text = "Re-Enter Game";
            this.ReentryButton.gotoAndStop(1);
            this.ReentryButton.addEventListener(MouseEvent.MOUSE_DOWN, this.reEntryClick);
            stop();
            return;
        }// end function

        public function nextFrameClick(event:MouseEvent) : void
        {
            nextFrame();
            stop();
            return;
        }// end function

        function frame15()
        {
            this.ContinueButton.actionlabel.text = "Finish";
            return;
        }// end function

    }
}
