﻿package sc346_fla
{
    import flash.display.*;

    dynamic public class loginscreen_50 extends flash.display.MovieClip
    {
        public var gamesetting:Object;
        public var counter:Object;
        public var playershipstatus:Object;
        public var PlayerInConquestZone:Object;
        public var gameSounds:Object;
        public var mov_login:MovieClip;
        public var mysocket:Object;

        public function loginscreen_50()
        {
            addFrameScript(0, this.frame1, 1, this.frame2, 2, this.frame3, 15, this.frame16, 64, this.frame65);
            return;
        }// end function

        public function func_playOpeningTitleSound()
        {
            if (this.gamesetting.soundvolume)
            {
                this.gamesetting.MenuChannelSound = this.gameSounds.openingTitleSound.play();
                this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
            }
            return;
        }// end function

        function frame3()
        {
            var _loc_1:* = this;
            var _loc_2:* = this.counter + 1;
            _loc_1.counter = _loc_2;
            gotoAndPlay(2);
            return;
        }// end function

        function frame65()
        {
            this.mov_login.mysocket = this.mysocket;
            this.mov_login.playershipstatus = this.playershipstatus;
            this.mov_login.PlayerInConquestZone = this.PlayerInConquestZone;
            this.mov_login.gameSounds = this.gameSounds;
            this.mov_login.gamesetting = this.gamesetting;
            stop();
            return;
        }// end function

        function frame1()
        {
            this.counter = 101;
            this.scaleX = 0.6;
            this.scaleY = 0.6;
            return;
        }// end function

        function frame2()
        {
            if (this.counter > 100)
            {
                this.mysocket.send("GAMEINFO`~");
                this.counter = 0;
            }
            return;
        }// end function

        function frame16()
        {
            this.func_playOpeningTitleSound();
            return;
        }// end function

        public function func_playRegularClick()
        {
            if (this.gamesetting.soundvolume)
            {
                this.gamesetting.MenuChannelSound = this.gameSounds.regularClick.play();
                this.gamesetting.MenuChannelSound.soundTransform = this.gamesetting.MenuSoundTransform;
            }
            return;
        }// end function

    }
}
