﻿package sc346_fla
{
    import flash.display.*;
    import flash.events.*;

    dynamic public class SelectionWindow_117 extends flash.display.MovieClip
    {
        public var sliderscale:Object;
        public var initialSlideBarY:Object;
        public var MaxBarY:Object;
        public var leftSideSlide:Object;
        public var slider:MovieClip;
        public var risghtSideSLide:Object;
        public var SlidingBar:Object;
        public var screen:MovieClip;
        public var mouseXoffset:Object;
        public var MinBarX:Object;
        public var screenWidth:Object;
        public var originalSLiderWidth:Object;
        public var SliderToScreenDIfference:Object;

        public function SelectionWindow_117()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        public function StartDrag(event:MouseEvent) : void
        {
            this.SlidingBar = true;
            this.mouseXoffset = mouseX - this.leftSideSlide - (this.slider.sliderBar.x - this.leftSideSlide);
            return;
        }// end function

        public function func_addItem(param1)
        {
            var _loc_2:* = this.screen.width;
            this.screen.addChild(param1);
            param1.x = _loc_2;
            this.func_calcSlideBarScale();
            this.screen.x = 0;
            return;
        }// end function

        function frame1()
        {
            this.screenWidth = 545;
            this.leftSideSlide = 15;
            this.risghtSideSLide = 550 - 15;
            this.slider.sliderBar.scaleX = 1;
            this.sliderscale = 1;
            this.SlidingBar = true;
            this.initialSlideBarY = this.slider.sliderBar.y;
            this.originalSLiderWidth = this.slider.sliderBar.width;
            this.mouseXoffset = 0;
            this.SliderToScreenDIfference = this.originalSLiderWidth / this.screenWidth;
            this.MinBarX = this.leftSideSlide;
            this.func_calcSlideBarScale();
            this.slider.sliderBar.addEventListener(MouseEvent.MOUSE_DOWN, this.StartDrag);
            Object(root).addEventListener(MouseEvent.MOUSE_UP, this.StopSliderDrag);
            addEventListener(Event.ENTER_FRAME, this.func_MoveSLider);
            return;
        }// end function

        public function func_clearAllItems()
        {
            while (this.screen.numChildren > 0)
            {
                
                this.screen.removeChildAt(0);
            }
            this.func_calcSlideBarScale();
            return;
        }// end function

        public function func_calcSlideBarScale()
        {
            if (this.screen.width > 0)
            {
                this.sliderscale = this.screenWidth / this.screen.width;
            }
            else
            {
                this.sliderscale = 1;
                this.slider.visible = false;
            }
            if (this.sliderscale > 1)
            {
                this.sliderscale = 1;
                this.slider.visible = false;
            }
            if (this.sliderscale < 1)
            {
                this.slider.sliderBar.scaleX = this.sliderscale;
                this.slider.sliderBar.x = this.leftSideSlide;
                this.MaxBarY = this.risghtSideSLide - this.originalSLiderWidth * this.sliderscale;
                this.slider.visible = true;
            }
            else
            {
                this.slider.alpha = 50;
            }
            return;
        }// end function

        public function func_MoveSLider(event:Event)
        {
            if (this.SlidingBar)
            {
                this.slider.sliderBar.y = this.initialSlideBarY;
                this.slider.sliderBar.x = mouseX - this.mouseXoffset;
                if (this.slider.sliderBar.x < this.leftSideSlide)
                {
                    this.slider.sliderBar.x = this.leftSideSlide;
                }
                else if (this.slider.sliderBar.x > this.MaxBarY)
                {
                    this.slider.sliderBar.x = this.MaxBarY;
                }
                this.screen.x = (-(this.slider.sliderBar.x - this.leftSideSlide)) / (this.sliderscale * this.SliderToScreenDIfference) + 1;
            }
            return;
        }// end function

        public function StopSliderDrag(event:MouseEvent) : void
        {
            this.SlidingBar = false;
            return;
        }// end function

    }
}
