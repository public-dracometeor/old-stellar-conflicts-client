﻿package sc346_fla
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;

    dynamic public class Missions_Screen_137 extends flash.display.MovieClip
    {
        public var gamesettings:Object;
        public var missionDetails:TextField;
        public var mysocket:Object;
        public var CurrentSpotSelected:Object;
        public var p0:MovieClip;
        public var p1:MovieClip;
        public var p2:MovieClip;
        public var missionLabel:TextField;
        public var missionsavailable:Object;
        public var m10:MovieClip;
        public var m11:MovieClip;
        public var missionsForSelection:Object;
        public var CurrentActionDoing:Object;
        public var playershipstatus:Object;
        public var m0:MovieClip;
        public var m1:MovieClip;
        public var m2:MovieClip;
        public var m4:MovieClip;
        public var m5:MovieClip;
        public var m6:MovieClip;
        public var m7:MovieClip;
        public var maxMissions:Object;
        public var m9:MovieClip;
        public var m3:MovieClip;
        public var maxAvailMissionsButts:Object;
        public var m8:MovieClip;
        public var exit_button:MovieClip;
        public var playersCurrentMissions:Object;
        public var gameSounds:Object;
        public var maxAvailMissions:Object;
        public var interaction_button:MovieClip;

        public function Missions_Screen_137()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        function frame1()
        {
            this.missionLabel.text = "";
            this.missionDetails.text = "";
            this.maxAvailMissions = 12;
            this.maxAvailMissionsButts = 12;
            this.maxMissions = 3;
            this.missionsForSelection = new Array();
            this.func_setButtonsListeners();
            this.selectAvailableMissions();
            this.func_DisplayButtons();
            this.interaction_button.gotoAndStop(1);
            this.interaction_button.addEventListener(MouseEvent.MOUSE_DOWN, this.func_InteractionClick);
            stop();
            this.exit_button.actionlabel.text = "Exit";
            return;
        }// end function

        public function selectAvailableMissions()
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_1:* = false;
            if (this.maxAvailMissions > this.missionsavailable.length)
            {
                this.maxAvailMissions = this.missionsavailable.length;
            }
            while (this.missionsForSelection.length < this.maxAvailMissions)
            {
                
                _loc_2 = Math.round(Math.random() * (this.missionsavailable.length - 1));
                _loc_3 = false;
                _loc_4 = 0;
                while (_loc_4 < this.missionsForSelection.length)
                {
                    
                    if (this.missionsavailable[_loc_2].MissionName == this.missionsForSelection[_loc_4].MissionName)
                    {
                        _loc_3 = true;
                        break;
                    }
                    _loc_4 = _loc_4 + 1;
                }
                if (!_loc_3)
                {
                    _loc_5 = this.missionsForSelection.length;
                    this.missionsForSelection[_loc_5] = new Object();
                    this.missionsForSelection[_loc_5] = this.missionsavailable[_loc_2];
                }
            }
            return;
        }// end function

        public function func_playRegularClick()
        {
            if (this.gamesettings.soundvolume)
            {
                this.gamesettings.MenuChannelSound = this.gameSounds.regularClick.play();
                this.gamesettings.MenuChannelSound.soundTransform = this.gamesettings.MenuSoundTransform;
            }
            return;
        }// end function

        public function func_setButtonsListeners()
        {
            var _loc_1:* = 0;
            _loc_1 = 0;
            while (_loc_1 < this.maxAvailMissionsButts)
            {
                
                this["m" + _loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_ListItemClick);
                _loc_1 = _loc_1 + 1;
            }
            _loc_1 = 0;
            while (_loc_1 < this.maxMissions)
            {
                
                this["p" + _loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_ListItemClick);
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function diplaySelectedMissionInfo(param1)
        {
            var _loc_2:* = "";
            var _loc_3:* = 0;
            if (param1.MissionType == "Recon")
            {
                _loc_2 = "Recon Mission\r";
                _loc_2 = _loc_2 + (param1.MissionName + "\r");
                _loc_2 = _loc_2 + ("Reward: " + param1.FundsReward);
                this.missionLabel.text = _loc_2;
                _loc_2 = "In this mission you will need to preform a recon mission";
                _loc_2 = _loc_2 + " by approaching the destination mentioned. Doing this will complete your mission.";
                this.missionDetails.text = _loc_2;
            }
            else if (param1.MissionType == "Patrol")
            {
                _loc_2 = "Patrol Mission\r";
                _loc_2 = _loc_2 + (param1.MissionName + "\r");
                _loc_2 = _loc_2 + ("Reward: " + param1.FundsReward);
                this.missionLabel.text = _loc_2;
                _loc_2 = "In this mission you will need to run recon at the listed points, in their listed order.";
                _loc_2 = _loc_2 + " You need to approach each destination (in order) and your mission will be completed.";
                this.missionDetails.text = _loc_2;
            }
            else if (param1.MissionType == "Cargo")
            {
                _loc_2 = "Express Cargo Mission\r";
                _loc_2 = _loc_2 + (param1.MissionName + "\r");
                _loc_2 = _loc_2 + ("Reward: " + param1.FundsReward);
                this.missionLabel.text = _loc_2;
                _loc_2 = "In this mission you will need to deliver special cargo to the listed destination.";
                _loc_2 = _loc_2 + " You need to dock at the destination provided and your mission will be completed.";
                this.missionDetails.text = _loc_2;
            }
            else if (param1.MissionType == "Kill")
            {
                _loc_2 = "Kill Your Enemies!\r";
                _loc_2 = _loc_2 + (param1.MissionName + "\r");
                _loc_2 = _loc_2 + ("Reward: " + param1.FundsReward);
                this.missionLabel.text = _loc_2;
                _loc_2 = "To complete this mission, simply kill the required number of enemies.";
                this.missionDetails.text = _loc_2;
            }
            return;
        }// end function

        public function func_DisplayButtons()
        {
            var _loc_1:* = 0;
            _loc_1 = 0;
            while (_loc_1 < this.maxAvailMissionsButts)
            {
                
                if (_loc_1 >= this.missionsForSelection.length)
                {
                    this["m" + _loc_1].visible = false;
                }
                else
                {
                    this["m" + _loc_1].visible = true;
                    this["m" + _loc_1].gotoAndStop(1);
                    this["m" + _loc_1].itemText.text = this.missionsForSelection[_loc_1].MissionName;
                }
                _loc_1 = _loc_1 + 1;
            }
            _loc_1 = 0;
            while (_loc_1 < this.maxMissions)
            {
                
                if (_loc_1 >= this.playersCurrentMissions.length)
                {
                    this["p" + _loc_1].visible = false;
                }
                else
                {
                    this["p" + _loc_1].visible = true;
                    this["p" + _loc_1].gotoAndStop(1);
                    this["p" + _loc_1].itemText.text = this.playersCurrentMissions[_loc_1].MissionName;
                }
                _loc_1 = _loc_1 + 1;
            }
            this.missionLabel.text = "Select A Mission";
            this.missionDetails.text = "";
            this.interaction_button.visible = false;
            return;
        }// end function

        public function func_InteractionClick(event:MouseEvent) : void
        {
            var _loc_2:* = undefined;
            if (this.CurrentActionDoing == "m")
            {
                _loc_2 = this.playersCurrentMissions.length;
                this.playersCurrentMissions[_loc_2] = new Object();
                this.playersCurrentMissions[_loc_2] = this.missionsForSelection[this.CurrentSpotSelected];
                this.missionsForSelection.splice(this.CurrentSpotSelected, 1);
            }
            else if (this.CurrentActionDoing == "p")
            {
                this.playersCurrentMissions.splice(this.CurrentSpotSelected, 1);
            }
            this.func_DisplayButtons();
            return;
        }// end function

        public function func_ListItemClick(event:MouseEvent) : void
        {
            this.func_DisplayButtons();
            var _loc_2:* = String(event.target.parent.name);
            var _loc_3:* = _loc_2.substr(0, 1);
            var _loc_4:* = Number(_loc_2.substr(1));
            this.CurrentSpotSelected = _loc_4;
            this.CurrentActionDoing = _loc_3;
            if (_loc_3 == "m")
            {
                this["m" + this.CurrentSpotSelected].gotoAndStop(2);
                this.diplaySelectedMissionInfo(this.missionsForSelection[this.CurrentSpotSelected]);
                if (this.playersCurrentMissions.length < this.maxMissions)
                {
                    this.interaction_button.visible = true;
                    this.interaction_button.actionlabel.text = "Take Mission";
                }
            }
            else if (_loc_3 == "p")
            {
                this["p" + this.CurrentSpotSelected].gotoAndStop(2);
                this.diplaySelectedMissionInfo(this.playersCurrentMissions[this.CurrentSpotSelected]);
                this.interaction_button.visible = true;
                this.interaction_button.actionlabel.text = "Drop Mission";
            }
            return;
        }// end function

    }
}
