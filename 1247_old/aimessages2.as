﻿package 
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import flash.utils.*;

    dynamic public class aimessages2 extends flash.display.MovieClip
    {
        public var textBackGround:MovieClip;
        public var DisplayingMessage:TextField;
        public var timeUntilHide:Object;
        public var cancel_button:SimpleButton;
        public var NewsPictureWindow:MovieClip;

        public function aimessages2()
        {
            addFrameScript(0, this.frame1, 2, this.frame3);
            return;
        }// end function

        function frame1()
        {
            this.cancel_button.addEventListener(MouseEvent.MOUSE_DOWN, function (event:MouseEvent) : void
            {
                gotoAndStop(1);
                return;
            }// end function
            );
            this.visible = false;
            stop();
            return;
        }// end function

        function frame3()
        {
            if (this.timeUntilHide > getTimer())
            {
                gotoAndPlay(2);
            }
            else
            {
                gotoAndStop(1);
            }
            return;
        }// end function

    }
}
