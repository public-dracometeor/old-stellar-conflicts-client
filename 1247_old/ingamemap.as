﻿package 
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import flash.utils.*;

    dynamic public class ingamemap extends flash.display.MovieClip
    {
        public var sectorinformation:Object;
        public var MapGridLabelImage:Class;
        public var MapIsVisible:Object;
        public var destination:TextField;
        public var lastTeamPositionSent:Object;
        public var gridxindent:Object;
        public var mysocket:Object;
        public var currentRolledOverTarget:Object;
        public var playersdestination:Object;
        public var PlayerLocater:Object;
        public var DesiredHeading:Object;
        public var yratio:Object;
        public var settingjumplocation:Object;
        public var purchasableitems:Object;
        public var SectorSquare:Object;
        public var sectormapitems:Object;
        public var gamemapscripting:MovieClip;
        public var clearSettingButton:SimpleButton;
        public var gridyindent:Object;
        public var destinationInfoWin:MovieClip;
        public var SquadBaseImages:Object;
        public var playersquadbases:Object;
        public var yrange:Object;
        public var minimizeButton:SimpleButton;
        public var tradegoods:Object;
        public var PlayerLocaterImage:Class;
        public var gridsize_x:Object;
        public var gridsize_y:Object;
        public var ingamexwidthofasec:Object;
        public var xsecwidth:Object;
        public var ysecwidth:Object;
        public var SectorItemImages:Object;
        public var xratio:Object;
        public var MapGridXLabels:Object;
        public var SquadBaseIDAdjust:Object;
        public var sellableitems:Object;
        public var noofysectors:Object;
        public var noofxsectors:Object;
        public var StarBaseImages:Object;
        public var xrange:Object;
        public var MapGridYLabels:Object;
        public var starbaselocation:Object;
        public var lastTeamPositionSentinterval:Object;
        public var SectorSquareImage:Class;
        public var ingameywidthofasec:Object;

        public function ingamemap()
        {
            addFrameScript(0, this.frame1);
            return;
        }// end function

        public function func_SetHeadingDirection(event:MouseEvent) : void
        {
            this.DesiredHeading = Number(event.target.parent.name);
            this.destination.text = this.currentRolledOverTarget;
            this.clearSettingButton.visible = true;
            return;
        }// end function

        public function func_RemoveSquadBase(param1)
        {
            this.removeChild(param1);
            return;
        }// end function

        public function func_refreshBaseTeamDisplays(param1)
        {
            var _loc_2:* = undefined;
            var _loc_3:* = undefined;
            _loc_2 = 0;
            while (_loc_2 < this.starbaselocation.length)
            {
                
                if (this.starbaselocation[_loc_2] != null)
                {
                    _loc_3 = this.starbaselocation[_loc_2][16];
                    _loc_3 = Number(_loc_3);
                    if (_loc_3 > -1)
                    {
                        if (_loc_3 == Number(param1))
                        {
                            this.StarBaseImages[_loc_2].gotoAndStop("friendly");
                        }
                        else
                        {
                            this.StarBaseImages[_loc_2].gotoAndStop("hostile");
                        }
                    }
                    else
                    {
                        this.StarBaseImages[_loc_2].gotoAndStop("neutral");
                    }
                    if (this.starbaselocation[_loc_2][0].substr(0, 2) == "PL")
                    {
                        this.StarBaseImages[_loc_2].label.text = this.starbaselocation[_loc_2][0].substr(2);
                    }
                    else
                    {
                        this.StarBaseImages[_loc_2].label.text = this.starbaselocation[_loc_2][0];
                    }
                    this.StarBaseImages[_loc_2].label.y = this.ysecwidth / 2;
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        function frame1()
        {
            this.MapIsVisible = false;
            this.StarBaseImages = new Array();
            this.SectorItemImages = new Array();
            this.SquadBaseImages = new Array();
            this.DesiredHeading = -1;
            this.fun_resetDestinatedSelected();
            this.MapGridLabelImage = getDefinitionByName("ingamemapgridlabel") as Class;
            this.MapGridXLabels = new Array();
            this.MapGridYLabels = new Array();
            this.SectorSquareImage = getDefinitionByName("ingamemapsecsquare") as Class;
            this.PlayerLocaterImage = getDefinitionByName("ingamemapplayership") as Class;
            this.PlayerLocater = new this.PlayerLocaterImage() as MovieClip;
            this.addChild(this.PlayerLocater);
            this.SectorSquare = new Array();
            this.lastTeamPositionSent = 0;
            this.lastTeamPositionSentinterval = 1500;
            this.settingjumplocation = false;
            this.gridxindent = 30;
            this.gridyindent = 50;
            this.gridsize_x = 600;
            this.gridsize_y = 450;
            this.xrange = 0;
            this.yrange = 0;
            this.clearSettingButton.addEventListener(MouseEvent.MOUSE_DOWN, this.func_ClearHeadingDirection);
            this.currentRolledOverTarget = "";
            this.purchasableitems = new Array();
            this.sellableitems = new Array();
            stop();
            return;
        }// end function

        public function fun_resetDestinatedSelected()
        {
            this.destination.text = "NOT SELECTED";
            this.clearSettingButton.visible = false;
            return;
        }// end function

        public function ObjectMousedOver(event:MouseEvent) : void
        {
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            trace(event.target.name);
            var _loc_2:* = Number(event.target.name);
            trace(_loc_2);
            var _loc_3:* = 0;
            if (_loc_2 >= this.SquadBaseIDAdjust)
            {
                _loc_3 = _loc_2 - this.SquadBaseIDAdjust;
                if (this.playersquadbases[_loc_3] != null)
                {
                    _loc_2 = this.playersquadbases[_loc_3][0];
                    this.destinationInfoWin.DispInfo.htmlText = "Squad Base:\r " + _loc_2 + "\r\r";
                    this.destinationInfoWin.DispInfo.htmlText = this.destinationInfoWin.DispInfo.htmlText + "Click Location to set Heading";
                }
            }
            else if (_loc_2 > -1)
            {
                _loc_2 = this.sectormapitems[_loc_2][0];
                if (_loc_2.substr(0, 2) == "NP")
                {
                    this.destinationInfoWin.DispInfo.htmlText = "Nav Point\r " + _loc_2 + "\r\r";
                    this.destinationInfoWin.DispInfo.htmlText = this.destinationInfoWin.DispInfo.htmlText + "Click Location to set Heading";
                }
                if (_loc_2.substr(0, 2) == "SB" || _loc_2.substr(0, 2) == "PL")
                {
                    _loc_4 = "Starbase";
                    _loc_5 = _loc_2;
                    if (_loc_5.substr(0, 2) == "PL")
                    {
                        _loc_4 = "Planet";
                        _loc_5 = _loc_5.substr(2);
                    }
                    if (this.currentRolledOverTarget != _loc_2)
                    {
                        this.destinationInfoWin.DispInfo.htmlText = _loc_4 + "\r " + _loc_5 + "\r\rLoading Trade Information\r\r";
                        this.destinationInfoWin.DispInfo.htmlText = this.destinationInfoWin.DispInfo.htmlText + "Click Location to set Heading";
                        this.mysocket.send("TRG`INFO`MAP`" + _loc_2 + "`~");
                    }
                    else
                    {
                        trace("ran");
                    }
                }
                if (_loc_2.substr(0, 2) == "TB")
                {
                    this.destinationInfoWin.DispInfo.htmlText = "Team Base\r " + _loc_2.substr(2) + "\r\r";
                    this.destinationInfoWin.DispInfo.htmlText = this.destinationInfoWin.DispInfo.htmlText + "Click Location to set Heading";
                }
            }
            this.currentRolledOverTarget = _loc_2;
            return;
        }// end function

        public function drawgrid()
        {
            var _loc_3:* = undefined;
            var _loc_4:* = undefined;
            var _loc_1:* = 0;
            while (_loc_1 <= this.noofxsectors)
            {
                
                _loc_3 = this.xsecwidth * _loc_1 + this.gridxindent + this.xsecwidth / 2;
                _loc_4 = this.gridyindent - 5;
                this.MapGridXLabels[_loc_1] = new this.MapGridLabelImage() as MovieClip;
                this.MapGridXLabels[_loc_1].x = _loc_3;
                this.MapGridXLabels[_loc_1].y = _loc_4;
                this.addChild(this.MapGridXLabels[_loc_1]);
                this.MapGridXLabels[_loc_1].GridLabel.text = _loc_1 + 1;
                _loc_1 = _loc_1 + 1;
            }
            var _loc_2:* = 0;
            while (_loc_2 <= this.noofxsectors)
            {
                
                _loc_4 = this.ysecwidth * _loc_2 + this.gridyindent + this.ysecwidth / 2;
                _loc_3 = this.gridxindent - 15;
                this.MapGridYLabels[_loc_2] = new this.MapGridLabelImage() as MovieClip;
                this.MapGridYLabels[_loc_2].x = _loc_3;
                this.MapGridYLabels[_loc_2].y = _loc_4;
                this.addChild(this.MapGridYLabels[_loc_2]);
                this.MapGridYLabels[_loc_2].GridLabel.text = _loc_2 + 1;
                _loc_2 = _loc_2 + 1;
            }
            _loc_1 = 0;
            while (_loc_1 <= this.noofxsectors)
            {
                
                _loc_3 = this.xsecwidth * _loc_1 + this.gridxindent;
                this.SectorSquare[_loc_1] = new Array();
                _loc_2 = 0;
                while (_loc_2 <= this.noofysectors)
                {
                    
                    _loc_4 = this.ysecwidth * _loc_2 + this.gridyindent;
                    this.SectorSquare[_loc_1][_loc_2] = new this.SectorSquareImage() as MovieClip;
                    this.SectorSquare[_loc_1][_loc_2].x = _loc_3;
                    this.SectorSquare[_loc_1][_loc_2].y = _loc_4;
                    this.SectorSquare[_loc_1][_loc_2].width = this.xsecwidth;
                    this.SectorSquare[_loc_1][_loc_2].height = this.ysecwidth;
                    this.addChild(this.SectorSquare[_loc_1][_loc_2]);
                    _loc_2 = _loc_2 + 1;
                }
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

        public function func_setupthewaypoints()
        {
            var _loc_1:* = undefined;
            var _loc_3:* = undefined;
            var _loc_4:* = null;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            this.StarBaseImages = new Array();
            this.SectorItemImages = new Array();
            this.SquadBaseImages = new Array();
            var _loc_2:* = getDefinitionByName("ingamemapstarbasemarker") as Class;
            _loc_3 = 0;
            while (_loc_3 < this.starbaselocation.length)
            {
                
                if (this.starbaselocation[_loc_3] != null)
                {
                    this.StarBaseImages[_loc_3] = new _loc_2 as MovieClip;
                    this.StarBaseImages[_loc_3].x = this.starbaselocation[_loc_3][1] * this.xratio + this.gridxindent + this.xsecwidth;
                    this.StarBaseImages[_loc_3].y = this.starbaselocation[_loc_3][2] * this.yratio + this.gridyindent + this.ysecwidth;
                    this.addChild(this.StarBaseImages[_loc_3]);
                    if (this.starbaselocation[_loc_3][0].substr(0, 2) == "PL")
                    {
                        _loc_4 = getDefinitionByName("planettype" + this.starbaselocation[_loc_3][3] + "image") as Class;
                        this.StarBaseImages[_loc_3].label.text = this.starbaselocation[_loc_3][0].substr(2);
                    }
                    else
                    {
                        _loc_4 = getDefinitionByName("starbasetype" + this.starbaselocation[_loc_3][3] + "image") as Class;
                        this.StarBaseImages[_loc_3].label.text = this.starbaselocation[_loc_3][0];
                    }
                    _loc_5 = new _loc_4 as MovieClip;
                    _loc_5.width = this.xsecwidth * 0.75;
                    _loc_5.height = this.ysecwidth * 0.8;
                    _loc_5.name = _loc_5;
                    this.StarBaseImages[_loc_3].addChild(_loc_5);
                    _loc_6 = 0;
                    while (_loc_6 < this.sectormapitems.length)
                    {
                        
                        if (this.sectormapitems[_loc_6] != null)
                        {
                            if (this.sectormapitems[_loc_6][0] == this.starbaselocation[_loc_3][0])
                            {
                                this.StarBaseImages[_loc_3].name = _loc_6;
                                this.StarBaseImages[_loc_3].addEventListener(MouseEvent.MOUSE_DOWN, this.func_SetHeadingDirection);
                                this.StarBaseImages[_loc_3].addEventListener(MouseEvent.ROLL_OVER, this.ObjectMousedOver, false, 0, true);
                            }
                        }
                        _loc_6 = _loc_6 + 1;
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            _loc_2 = getDefinitionByName("ingamemapnavpoint") as Class;
            _loc_3 = 0;
            while (_loc_3 < this.sectormapitems.length)
            {
                
                if (this.sectormapitems[_loc_3] != null)
                {
                    if (this.sectormapitems[_loc_3][0].substr(0, 2) == "TB")
                    {
                        _loc_1 = this.SectorItemImages.length;
                        this.SectorItemImages[_loc_1] = new _loc_2 as MovieClip;
                        this.SectorItemImages[_loc_1].x = this.sectormapitems[_loc_3][1] * this.xratio + this.gridxindent + this.xsecwidth;
                        this.SectorItemImages[_loc_1].y = this.sectormapitems[_loc_3][2] * this.yratio + this.gridyindent + this.ysecwidth;
                        this.SectorItemImages[_loc_1].label.text = this.sectormapitems[_loc_3][0];
                        this.addChild(this.SectorItemImages[_loc_1]);
                        this.SectorItemImages[_loc_1].name = _loc_3;
                        this.SectorItemImages[_loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_SetHeadingDirection);
                        this.SectorItemImages[_loc_1].addEventListener(MouseEvent.ROLL_OVER, this.ObjectMousedOver, false, 0, true);
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            _loc_2 = getDefinitionByName("ingamemapnavpoint") as Class;
            _loc_3 = 0;
            while (_loc_3 < this.sectormapitems.length)
            {
                
                if (this.sectormapitems[_loc_3] != null)
                {
                    if (this.sectormapitems[_loc_3][0].substr(0, 2) == "NP")
                    {
                        _loc_1 = this.SectorItemImages.length;
                        this.SectorItemImages[_loc_1] = new _loc_2 as MovieClip;
                        this.SectorItemImages[_loc_1].x = this.sectormapitems[_loc_3][1] * this.xratio + this.gridxindent + this.xsecwidth;
                        this.SectorItemImages[_loc_1].y = this.sectormapitems[_loc_3][2] * this.yratio + this.gridyindent + this.ysecwidth;
                        this.SectorItemImages[_loc_1].label.text = this.sectormapitems[_loc_3][0];
                        this.addChild(this.SectorItemImages[_loc_1]);
                        this.SectorItemImages[_loc_1].name = _loc_3;
                        this.SectorItemImages[_loc_1].addEventListener(MouseEvent.MOUSE_DOWN, this.func_SetHeadingDirection);
                        this.SectorItemImages[_loc_1].addEventListener(MouseEvent.ROLL_OVER, this.ObjectMousedOver, false, 0, true);
                    }
                }
                _loc_3 = _loc_3 + 1;
            }
            return;
        }// end function

        public function func_ClearHeadingDirection(event:MouseEvent) : void
        {
            this.DesiredHeading = -1;
            return;
        }// end function

        public function func_initMap()
        {
            trace(this.sectorinformation[0][0]);
            this.noofxsectors = this.sectorinformation[0][0];
            this.noofysectors = this.sectorinformation[0][1];
            this.xsecwidth = this.gridsize_x / (this.noofxsectors + 1);
            this.ysecwidth = this.gridsize_y / (this.noofysectors + 1);
            this.ingamexwidthofasec = this.sectorinformation[1][0];
            this.ingameywidthofasec = this.sectorinformation[1][1];
            this.xratio = this.xsecwidth / this.ingamexwidthofasec;
            this.yratio = this.ysecwidth / this.ingameywidthofasec;
            this.drawgrid();
            this.func_setupthewaypoints();
            this.addChild(this.PlayerLocater);
            return;
        }// end function

        public function func_PlayerMapLocation(param1, param2, param3)
        {
            this.PlayerLocater.x = param1 * this.xratio + this.gridxindent + this.xsecwidth;
            this.PlayerLocater.y = param2 * this.yratio + this.gridyindent + this.ysecwidth;
            this.PlayerLocater.rotation = param3;
            return this.DesiredHeading;
        }// end function

        public function func_DisplaytradeGoods()
        {
            var _loc_4:* = undefined;
            var _loc_1:* = this.currentRolledOverTarget;
            var _loc_2:* = "Starbase";
            var _loc_3:* = _loc_1;
            if (_loc_3.substr(0, 2) == "PL")
            {
                _loc_2 = "Planet";
                _loc_3 = _loc_3.substr(2);
            }
            this.destinationInfoWin.DispInfo.htmlText = _loc_2 + "\r " + _loc_3 + "\r";
            this.destinationInfoWin.DispInfo.htmlText = this.destinationInfoWin.DispInfo.htmlText + "Buying\r";
            _loc_4 = 0;
            while (_loc_4 < this.sellableitems.length)
            {
                
                this.destinationInfoWin.DispInfo.htmlText = this.destinationInfoWin.DispInfo.htmlText + (" " + this.tradegoods[this.sellableitems[_loc_4][0]].GoodsName + " (" + this.sellableitems[_loc_4][1] + ")\r");
                _loc_4 = _loc_4 + 1;
            }
            this.destinationInfoWin.DispInfo.htmlText = this.destinationInfoWin.DispInfo.htmlText + "Selling\r";
            _loc_4 = 0;
            _loc_4 = 0;
            while (_loc_4 < this.purchasableitems.length)
            {
                
                this.destinationInfoWin.DispInfo.htmlText = this.destinationInfoWin.DispInfo.htmlText + (" " + this.tradegoods[this.purchasableitems[_loc_4][0]].GoodsName + " (" + this.purchasableitems[_loc_4][1] + ")\r");
                _loc_4 = _loc_4 + 1;
            }
            return;
        }// end function

        public function parseInTradegoods(param1)
        {
            var _loc_5:* = undefined;
            var _loc_2:* = param1.split("`");
            var _loc_3:* = 3;
            var _loc_4:* = 0;
            if (_loc_2[2] == this.currentRolledOverTarget)
            {
                this.purchasableitems = new Array();
                this.sellableitems = new Array();
                while (_loc_3 < (_loc_2.length - 1))
                {
                    
                    _loc_5 = _loc_2[_loc_3].split(":");
                    if (_loc_5[1] == "S")
                    {
                        _loc_4 = this.purchasableitems.length;
                        this.purchasableitems[_loc_4] = new Array();
                        this.purchasableitems[_loc_4][0] = Number(_loc_5[0]);
                        this.purchasableitems[_loc_4][1] = Number(_loc_5[2]);
                    }
                    if (_loc_5[1] == "B")
                    {
                        _loc_4 = this.sellableitems.length;
                        this.sellableitems[_loc_4] = new Array();
                        this.sellableitems[_loc_4][0] = Number(_loc_5[0]);
                        this.sellableitems[_loc_4][1] = Number(_loc_5[2]);
                    }
                    _loc_3 = _loc_3 + 1;
                }
                this.func_DisplaytradeGoods();
            }
            else
            {
                trace("wrong base loading:" + _loc_2[2]);
            }
            return;
        }// end function

        public function func_drawSquadbase(param1)
        {
            var _loc_2:* = 0;
            var _loc_3:* = getDefinitionByName("ingamemapsquadbase") as Class;
            _loc_2 = 0;
            while (_loc_2 < this.playersquadbases.length)
            {
                
                if (this.playersquadbases[_loc_2] != null)
                {
                    if (this.playersquadbases[_loc_2][0] == param1)
                    {
                        this.playersquadbases[_loc_2][11] = new _loc_3 as MovieClip;
                        this.playersquadbases[_loc_2][11].x = this.playersquadbases[_loc_2][2] * this.xratio + this.gridxindent + this.xsecwidth;
                        this.playersquadbases[_loc_2][11].y = this.playersquadbases[_loc_2][3] * this.yratio + this.gridyindent + this.ysecwidth;
                        this.playersquadbases[_loc_2][11].label.text = param1;
                        this.playersquadbases[_loc_2][11].name = _loc_2 + this.SquadBaseIDAdjust;
                        this.playersquadbases[_loc_2][11].addEventListener(MouseEvent.ROLL_OVER, this.ObjectMousedOver, false, 0, true);
                        this.playersquadbases[_loc_2][11].addEventListener(MouseEvent.MOUSE_DOWN, this.func_SetHeadingDirection);
                        this.addChild(this.playersquadbases[_loc_2][11]);
                    }
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

    }
}
